package service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import pojo.Book;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public interface BookService {
    Boolean save(Book book);
    Boolean update(Book book);

    Boolean delete(Integer id);

    Book getById(Integer id);

    List<Book> getAll();

    IPage<Book> getPage(int currentPage, int pageSize);
}
