package top.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.pojo.Book;


public interface IBookService extends IService<Book> {
}
