package top.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import top.mapper.BookMapper;
import top.pojo.Book;


@Service
public class BookServiceImpl extends ServiceImpl<BookMapper, Book> implements IBookService {
}
