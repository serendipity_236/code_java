package top.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;
import top.pojo.Book;
import top.service.IBookService;


@SpringBootTest
@Transactional
//@Rollback(value = false)
public class BookServiceTest {
    @Autowired
    private IBookService bookService;
    @Test
    public void testSave() {
        Book book = new Book();
        book.setName("springboot");
        book.setType("springboot");
        book.setDescription("springboot");
        bookService.save(book);
    }
}
