import java.io.StringReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Test {
    // 二分查找应用
    // 二分查找代码实现
    // 1.基础班
    private int binarySearch(int[] array,int target){
        int i = 0,j = array.length - 1;
        while(i <= j){
            int mid = (i + j) >>> 1;
            if(array[mid] < target){
                i = mid + 1;
            }else if(array[mid] > target){
                j = mid - 1;
            }else{
                return mid;
            }
        }
        return -1;
    }

    // 2.改进版
    private int binarySearch2(int[] array,int target){
        // j为不参与比较的
        int i = 0,j = array.length;
        while(i < j){
            int mid = (i + j) >>> 1;
            if(array[mid] < target){
                i = mid + 1;
            }else if(array[mid] > target){
                j = mid;
            }else{
                return mid;
            }
        }
        return -1;
    }

    // 3.平衡版
    private int binarySearch3(int[] array,int target){
        // j为不参与比较的
        int i = 0,j = array.length;
        while((i + 1) < j){
            int mid = (i + j) >>> 1;
            if(array[mid] <= target){
                i = mid;
            }else{
                j = mid;
            }
        }
        if(array[i] == target){
            return i;
        }
        return -1;
    }

    // 3.找到插入的位置  leftMost的应用
    private int binarySearch4(int[] array,int target){
        int i = 0,j = array.length - 1;
        while(i <= j){
            int mid = (i + j) >>> 1;
            if(array[mid] < target){
                i = mid + 1;
            }else{
                j = mid - 1;
            }
        }
        return i;
    }

    // 4.在升序数组中查找元素的第一个和最后一个位置
    private int[] binarySearch5(int[] array,int target){
        int left = leftMost(array,target);
        if(left == -1){
            return new int[]{-1,-1};
        }else{
            return new int[]{left,rightMost(array,target)};
        }
    }

    public int leftMost(int[] array,int target){
        int i = 0,j = array.length - 1;
        int candidate = -1;
        while(i <= j){
            int mid = (i + j) >>> 1;
            if(array[mid] < target){
                i = mid + 1;
            }else if(array[mid] > target){
                j = mid - 1;
            }else{
                candidate = mid;
                j = mid - 1;
            }
        }
        return candidate;
    }

    public int rightMost(int[] array,int target){
        int i = 0,j = array.length - 1;
        int candidate = -1;
        while(i <= j){
            int mid = (i + j) >>> 1;
            if(array[mid] < target){
                i = mid + 1;
            }else if(array[mid] > target){
                j = mid - 1;
            }else{
                candidate = mid;
                i = mid + 1;
            }
        }
        return candidate;
    }

    // 6
    // 力扣2446给你两个字符串数组event1和event2，表示发生在同一天的两个闭区间时间段事件，其中：
    // event1 = [startTime1, endTime1] 且
    // event2 = [startTime2, endTime2]
    // 事件的时间为有效的 24 小时制且按HH:MM格式给出。
    // 当两个事件存在某个非空的交集时（即，某些时刻是两个事件都包含的），则认为出现 冲突。
    // 如果两个事件之间存在冲突，返回true；否则，返回false 。
    public boolean haveConflict6(String[] event1, String[] event2) {
        return !(event1[1].compareTo(event2[0]) < 0 || event2[1].compareTo(event1[0]) < 0);
    }

    // 7
    // 力扣4寻找两个正序数组的中位数
    // 给定两个大小分别为 m 和 n 的正序（从小到大）数组nums1和nums2。请你找出并返回这两个正序数组的中位数 。
    //算法的时间复杂度应该为 O(log (m+n))
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int length1 = nums1.length, length2 = nums2.length;
        int totalLength = length1 + length2;
        if (totalLength % 2 == 1) {
            int midIndex = totalLength / 2;
            return getKthElement(nums1, nums2, midIndex + 1);
        } else {
            int midIndex1 = totalLength / 2 - 1, midIndex2 = totalLength / 2;
            return (getKthElement(nums1, nums2, midIndex1 + 1) + getKthElement(nums1, nums2, midIndex2 + 1)) / 2.0;
        }
    }

    public int getKthElement(int[] nums1, int[] nums2, int k) {
        /* 主要思路：要找到第 k (k>1) 小的元素，那么就取 pivot1 = nums1[k/2-1] 和 pivot2 = nums2[k/2-1] 进行比较
         * 这里的 "/" 表示整除
         * nums1 中小于等于 pivot1 的元素有 nums1[0 .. k/2-2] 共计 k/2-1 个
         * nums2 中小于等于 pivot2 的元素有 nums2[0 .. k/2-2] 共计 k/2-1 个
         * 取 pivot = min(pivot1, pivot2)，两个数组中小于等于 pivot 的元素共计不会超过 (k/2-1) + (k/2-1) <= k-2 个
         * 这样 pivot 本身最大也只能是第 k-1 小的元素
         * 如果 pivot = pivot1，那么 nums1[0 .. k/2-1] 都不可能是第 k 小的元素。把这些元素全部 "删除"，剩下的作为新的 nums1 数组
         * 如果 pivot = pivot2，那么 nums2[0 .. k/2-1] 都不可能是第 k 小的元素。把这些元素全部 "删除"，剩下的作为新的 nums2 数组
         * 由于我们 "删除" 了一些元素（这些元素都比第 k 小的元素要小），因此需要修改 k 的值，减去删除的数的个数
         */

        int length1 = nums1.length, length2 = nums2.length;
        int index1 = 0, index2 = 0;

        while (true) {
            // 边界情况
            if (index1 == length1) {
                return nums2[index2 + k - 1];
            }
            if (index2 == length2) {
                return nums1[index1 + k - 1];
            }
            if (k == 1) {
                return Math.min(nums1[index1], nums2[index2]);
            }

            // 正常情况
            int half = k / 2;
            int newIndex1 = Math.min(index1 + half, length1) - 1;
            int newIndex2 = Math.min(index2 + half, length2) - 1;
            int pivot1 = nums1[newIndex1], pivot2 = nums2[newIndex2];
            if (pivot1 <= pivot2) {
                k -= (newIndex1 - index1 + 1);
                index1 = newIndex1 + 1;
            } else {
                k -= (newIndex2 - index2 + 1);
                index2 = newIndex2 + 1;
            }
        }
    }
    // 原理：划分数组
    // 该方法要对中位数的定义有理解
    // 中位数可以把一个数组分成两个部分
    // 当数组的长度为奇数时候，中位数有一个，
    // 当数组的长度为偶数时候，中位数有两个，一个是左边的最大值，一个是右边的最小值
    // 当两个有序数组的长度和为奇数时候，左边数组的长度=右边数组长度+1
    // 当两个有序数组的长度和为奇数时候，左边数组的长度=右边数组长度
    // 分割线的条件：
    // 1.分割线左边的两个数组总个数要大于等于分割线右边的两个数组总个数
    // 2.第一个数组分割线左边的最大值要小于等于第二个数组分割线右边边的最小值，而第二个数组分割线左边的最大值要小于等于第一个数组分割线右边的最小值

    public double findMedianSortedArrays2(int[] nums1, int[] nums2) {
        if (nums1.length > nums2.length) {
            return findMedianSortedArrays2(nums2, nums1);
        }

        int m = nums1.length;
        int n = nums2.length;
        int left = 0, right = m;
        // median1：前一部分的最大值
        // median2：后一部分的最小值
        int median1 = 0, median2 = 0;

        while (left <= right) {
            // 前一部分包含 nums1[0 .. i-1] 和 nums2[0 .. j-1]
            // 后一部分包含 nums1[i .. m-1] 和 nums2[j .. n-1]
            int i = (left + right) / 2;
            int j = (m + n + 1) / 2 - i;

            // nums_im1, nums_i, nums_jm1, nums_j 分别表示 nums1[i-1], nums1[i], nums2[j-1], nums2[j]
            int nums_im1 = (i == 0 ? Integer.MIN_VALUE : nums1[i - 1]);
            int nums_i = (i == m ? Integer.MAX_VALUE : nums1[i]);
            int nums_jm1 = (j == 0 ? Integer.MIN_VALUE : nums2[j - 1]);
            int nums_j = (j == n ? Integer.MAX_VALUE : nums2[j]);

            if (nums_im1 <= nums_j) {
                median1 = Math.max(nums_im1, nums_jm1);
                median2 = Math.min(nums_i, nums_j);
                left = i + 1;
            } else {
                right = i - 1;
            }
        }

        return (m + n) % 2 == 0 ? (median1 + median2) / 2.0 : median1;
    }

    // 8搜索二维矩阵
    public boolean searchMatrix(int[][] matrix, int target) {
        int m = matrix.length, n = matrix[0].length;
        int low = 0, high = m * n - 1;
        while (low <= high) {
            int mid = (high - low) / 2 + low;
            int x = matrix[mid / n][mid % n];
            if (x < target) {
                low = mid + 1;
            } else if (x > target) {
                high = mid - 1;
            } else {
                return true;
            }
        }
        return false;
    }

    // 9搜索旋转排序数组
    public int search(int[] nums, int target) {
        int n = nums.length;
        if (n == 0) {
            return -1;
        }
        if (n == 1) {
            return nums[0] == target ? 0 : -1;
        }
        int l = 0, r = n - 1;
        while (l <= r) {
            int mid = (l + r) / 2;
            if (nums[mid] == target) {
                return mid;
            }
            if (nums[0] <= nums[mid]) {
                if (nums[0] <= target && target < nums[mid]) {
                    r = mid - 1;
                } else {
                    l = mid + 1;
                }
            } else {
                if (nums[mid] < target && target <= nums[n - 1]) {
                    l = mid + 1;
                } else {
                    r = mid - 1;
                }
            }
        }
        return -1;
    }


    //数组的题
    //15. 三数之和
    public List<List<Integer>> threeSum(int[] nums) {
        int n = nums.length;
        Arrays.sort(nums);
        List<List<Integer>> ans = new ArrayList<List<Integer>>();
        // 枚举 a
        for (int first = 0; first < n; ++first) {
            // 需要和上一次枚举的数不相同
            if (first > 0 && nums[first] == nums[first - 1]) {
                continue;
            }
            // c 对应的指针初始指向数组的最右端
            int third = n - 1;
            int target = -nums[first];
            // 枚举 b
            for (int second = first + 1; second < n; ++second) {
                // 需要和上一次枚举的数不相同
                if (second > first + 1 && nums[second] == nums[second - 1]) {
                    continue;
                }
                // 需要保证 b 的指针在 c 的指针的左侧
                while (second < third && nums[second] + nums[third] > target) {
                    --third;
                }
                // 如果指针重合，随着 b 后续的增加
                // 就不会有满足 a+b+c=0 并且 b<c 的 c 了，可以退出循环
                if (second == third) {
                    break;
                }
                if (nums[second] + nums[third] == target) {
                    List<Integer> list = new ArrayList<Integer>();
                    list.add(nums[first]);
                    list.add(nums[second]);
                    list.add(nums[third]);
                    ans.add(list);
                }
            }
        }
        return ans;
    }
}