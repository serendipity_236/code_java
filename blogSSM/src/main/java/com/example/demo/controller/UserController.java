package com.example.demo.controller;

import com.example.demo.common.AjaxResult;
import com.example.demo.common.ApplicationVariable;
import com.example.demo.common.PasswordUtils;
import com.example.demo.common.UserSessionUtils;
import com.example.demo.entity.Userinfo;
import com.example.demo.entity.vo.UserinfoVO;
import com.example.demo.service.ArticleService;
import com.example.demo.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/user")
public class UserController {

    @Resource
    private UserService userService;

    @Resource
    private ArticleService articleService;

    @RequestMapping("/reg")
    public AjaxResult register(Userinfo userinfo) {
        //非空校验和参数有效性校验(防止通过第三方发起的非法请求)
        //StringUtils.hasLength()返回值是boolean类型，如果参数不是空字符串返回true，反之false
        if (userinfo == null || !StringUtils.hasLength(userinfo.getUsername())
                || !StringUtils.hasLength(userinfo.getPassword())) {
            return AjaxResult.fail(-1, "非法参数");
        }
        //密码加密
        userinfo.setPassword(PasswordUtils.encrypt(userinfo.getPassword()));

        return AjaxResult.success(userService.register(userinfo));
    }

    //参数名和Ajax请求中data的属性名保持一致
    @RequestMapping("/login")
    public AjaxResult login(HttpServletRequest request, String username, String password) {
        //System.out.println(username);
        if (username == null || password == null) {
            return AjaxResult.fail(-1, "非法参数", null);
        }
        Userinfo userinfo = userService.getUserByName(username);
        if (userinfo != null && userinfo.getId() > 0) {
            //有效用户
//            if (userinfo.getPassword().equals(password)) {
            if (PasswordUtils.verify(password, userinfo.getPassword())) {
                //用户名和密码都正确
                //隐藏敏感信息
                userinfo.setPassword("");
                //存储到session中
                HttpSession session = request.getSession();
                session.setAttribute(ApplicationVariable.User_Session_key, userinfo);
                return AjaxResult.success(userinfo);
            }
        }
        return AjaxResult.success(0, "非法用户", null);
    }

    @RequestMapping("/showinfo")
    public AjaxResult showInfo(HttpServletRequest request) {
        UserinfoVO userinfoVO = new UserinfoVO();
        //得到当前登录用户(从Session中获取)
        Userinfo userinfo = UserSessionUtils.getSessionUser(request);
        if (userinfo == null) {
            return AjaxResult.fail(-1, "非法请求");
        }
        //使用spring提供的深拷贝方法
        BeanUtils.copyProperties(userinfo, userinfoVO);
        //得到用户发表文章总数
        int totalNum = articleService.getTotalNumById(userinfo.getId());
        userinfoVO.setTotalNum(totalNum);
        return AjaxResult.success(userinfoVO);
    }

    @RequestMapping("/logout")
    public AjaxResult logout(HttpSession session) {
        session.removeAttribute(ApplicationVariable.User_Session_key);
        return AjaxResult.success(1);
    }

    @RequestMapping("/getuserbyid")
    public AjaxResult getUserById(Integer uid) {
        if (uid == null || uid <= 0) {
            return AjaxResult.fail(-1, "非法参数请求");
        }
        Userinfo userinfo = userService.getUserById(uid);
        if (userinfo == null || userinfo.getId() <= 0) {
            return AjaxResult.fail(-1, "非法用户");
        }
        //去除敏感信息
        userinfo.setPassword("");
        UserinfoVO userinfoVO = new UserinfoVO();
        BeanUtils.copyProperties(userinfo, userinfoVO);
        userinfoVO.setTotalNum(articleService.getTotalNumById(userinfoVO.getId()));
        return AjaxResult.success(userinfoVO);
    }
}
