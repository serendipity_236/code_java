package com.example.demo.controller;

import com.example.demo.common.AjaxResult;
import com.example.demo.common.UserSessionUtils;
import com.example.demo.entity.Articleinfo;
import com.example.demo.entity.Userinfo;
import com.example.demo.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

@RequestMapping("/art")
@RestController
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    @RequestMapping("/mylists")
    //查询个人文章列表
    public AjaxResult getAllMyArticle(HttpServletRequest request) {
        Userinfo userinfo = UserSessionUtils.getSessionUser(request);
        if (userinfo == null) {
            return AjaxResult.fail(-1, "非法用户");
        }
        List<Articleinfo> lists = articleService.getAllMyArticle(userinfo.getId());
        return AjaxResult.success(lists);
    }

    @RequestMapping("/delete")
    public AjaxResult delete(HttpServletRequest request, Integer articleId) {
        if (articleId == null || articleId <= 0) {
            return AjaxResult.fail(-1, "参数异常，非法请求");
        }
        Userinfo userinfo = UserSessionUtils.getSessionUser(request);
        if (userinfo == null) {
            return AjaxResult.fail(-2, "您并未登录");
        }
        Integer result = articleService.delete(articleId, userinfo.getId());
        return AjaxResult.success(result);
    }

    @RequestMapping("/detail")
    public AjaxResult getDetail(Integer articleId) {
        if (articleId == null || articleId <= 0) {
            return AjaxResult.fail(-1, "非法参数");
        }
        return AjaxResult.success(articleService.getDetail(articleId));
    }

    @RequestMapping("/increment-rcount")
    public AjaxResult incrementRcount(Integer id) {
        if (id == null || id <= 0) {
            return AjaxResult.fail(-1, "未知错误");
        }
        return AjaxResult.success(articleService.incrementRcount(id));
    }

    @RequestMapping("/add")
    public AjaxResult add(HttpServletRequest request, Articleinfo articleinfo) {
        if (articleinfo == null || !StringUtils.hasLength(articleinfo.getTitle())
                || !StringUtils.hasLength(articleinfo.getContent())) {
            return AjaxResult.fail(-1, "非法参数");
        }
        Userinfo userinfo = UserSessionUtils.getSessionUser(request);
        if (userinfo == null || userinfo.getId() < 0) {
            return AjaxResult.fail(-1, "无效用户");
        }

        articleinfo.setUid(userinfo.getId());
        return AjaxResult.success(articleService.add(articleinfo));
    }

    @RequestMapping("/update")
    public AjaxResult update(HttpServletRequest request, Articleinfo articleinfo) {
        if (articleinfo == null || !StringUtils.hasLength(articleinfo.getTitle())
                || !StringUtils.hasLength(articleinfo.getContent()) || articleinfo.getId() == null) {
            return AjaxResult.fail(-1, "非法参数");
        }
        Userinfo userinfo = UserSessionUtils.getSessionUser(request);
        if (userinfo == null || userinfo.getId() == null) {
            return AjaxResult.fail(-2, "无效登录");
        }
        articleinfo.setUid(userinfo.getId());
        articleinfo.setUpdatetime(LocalDateTime.now());
        return AjaxResult.success(articleService.update(articleinfo));
    }

    @RequestMapping("/listbypage")
    public AjaxResult getListsByPage(Integer pageIndex, Integer limitSize) {
        //  参数校验
        if (pageIndex == null || pageIndex <= 1) {
            pageIndex = 1;
        }
        if (limitSize == null || limitSize <= 1) {
            limitSize = 2;
        }
        int offset = (pageIndex - 1) * limitSize;
        //  文章列表数据
        List<Articleinfo> lists = articleService.getListsByPage(limitSize, offset);
        //  列表总共几页
        //  1.总共几条数据
        int totalEntry = articleService.getTotalEntry();
        //  2.求总页数
        double pageVirtualCount = totalEntry / (limitSize * 1.0);
//        if (totalEntry % limitSize == 0)
//            pageCount = totalEntry % limitSize;
//        else
//            pageCount = totalEntry / limitSize + 1;
        int pageRealCount = (int) Math.ceil(pageVirtualCount);
        HashMap<String, Object> map = new HashMap<>();
        map.put("list", lists);
        map.put("pageCount", pageRealCount);
        return AjaxResult.success(map);
    }
}