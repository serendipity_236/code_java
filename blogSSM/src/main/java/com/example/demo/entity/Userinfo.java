package com.example.demo.entity;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Userinfo {
    //使用Integer兼容性更好，可以接受的值的类型更多，可以是数值类型也可以是null
    private Integer id;
    private String username;
    private String password;
    private String photo;
    //LocalDateTime给前端返回是一个时间戳，要对时间进行格式化，
    //例如SimpleDateFormat类、@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private LocalDateTime createtime;
    private LocalDateTime updatetime;
    private Integer state;//预留字段
}
