package com.example.demo.mapper;

import com.example.demo.entity.Userinfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface UserMapper {

    //用户注册功能
    public int register(Userinfo user);

    //用户登录功能(根据用户有查询用户对象)  数据库中给用户名设置唯一键值
    public Userinfo getUserByName(@Param("username") String username);

    //根据用户Id查询用户
    public Userinfo getUserById(@Param("id") Integer uid);
}
