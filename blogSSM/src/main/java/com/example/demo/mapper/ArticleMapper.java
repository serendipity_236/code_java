package com.example.demo.mapper;

import com.example.demo.entity.Articleinfo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface ArticleMapper {

    //根据用户id查询该用户的全部文章数量
    int getTotalNumById(@Param("uid") Integer id);

    List<Articleinfo> getAllMyArticle(@Param("uid") Integer id);

    int delete(@Param("id") Integer articleId, @Param("uid") Integer uid);

    Articleinfo getDetail(@Param("id") Integer articleId);

    int incrementRcount(@Param("id") Integer id);

    int add(Articleinfo articleinfo);

    int update(Articleinfo articleinfo);

    /**
     * 分页功能 controller把算好的偏移量给sql查询
     *
     * @param limitSize 每页面最多有几条记录显示
     * @param offset    偏移量
     * @return 返回当前页面的文章记录
     */
    List<Articleinfo> getListsByPage(@Param("limitSize") Integer limitSize,
                                     @Param("offset") Integer offset);

    int getTotalEntry();
}
