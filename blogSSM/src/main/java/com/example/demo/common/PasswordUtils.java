package com.example.demo.common;

import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import java.nio.charset.StandardCharsets;
import java.util.UUID;

/**
 * 密码的工具类
 */
public class PasswordUtils {

    /**
     * 加盐加密
     *
     * @param password 明文密码
     * @return 返回通过加盐加密的密码
     */
    public static String encrypt(String password) {
        //32位盐
        String salt = UUID.randomUUID().toString().replaceAll("-", "");
        //使用加盐和明文密码产生加密密码
        String saltPassword = DigestUtils.md5DigestAsHex((salt + password).getBytes(StandardCharsets.UTF_8));
        //最终密码：盐值+"*"+加密密码  规范密码
        return salt + "$" + saltPassword;
    }

    /**
     * 加盐加密的重载
     *
     * @param password 明文密码
     * @param salt     固定盐值
     * @return 返回在数据库存放的密码
     */
    public static String encrypt(String password, String salt) {
        //使用加盐和明文密码产生加密密码
        String saltPassword = DigestUtils.md5DigestAsHex((salt + password).getBytes(StandardCharsets.UTF_8));
        //最终密码：盐值+"*"+加密密码  规范密码
        return salt + "$" + saltPassword;
    }

    /**
     * 验证密码
     *
     * @param inputPassword    用户输入的密码
     * @param finalSqlPassword 数据库中的最终密码
     * @return 判断使用用户密码进行加密后的密码是否和数据库中最终密码是否相同
     */
    public static boolean verify(String inputPassword, String finalSqlPassword) {
        if (StringUtils.hasLength(inputPassword) && StringUtils.hasLength(finalSqlPassword)
                && finalSqlPassword.length() == 65) {
            //得到盐值
            String salt = finalSqlPassword.split("\\$")[0];
            //使用得到的盐值加密得到密码
            String finalPassword = encrypt(inputPassword, salt);
            //判断使用得到的盐值产生的密码是否和finalSqlPassword密码相同
            return finalSqlPassword.equals(finalPassword);
        }
        return false;
    }
}
