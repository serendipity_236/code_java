package com.example.demo.common;

import com.example.demo.entity.Userinfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * 当前登录用户相关操作
 */
public class UserSessionUtils {

    /**
     * 得到当前登录用户
     *
     * @param request
     * @return
     */
    public static Userinfo getSessionUser(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session != null &&
                session.getAttribute(ApplicationVariable.User_Session_key) != null) {
            return (Userinfo) session.getAttribute(ApplicationVariable.User_Session_key);
        }
        return null;
    }
}
