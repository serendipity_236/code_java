package com.example.demo.configure;

import com.example.demo.common.ApplicationVariable;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 登录的拦截器
 */

public class LoginInterceptor implements HandlerInterceptor {

    /**
     * true-->用户已经登录
     * false-->跳转到登陆页面
     *
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession(false);
        if (session != null && session.getAttribute(ApplicationVariable.User_Session_key) != null) {
            //已登录
            return true;
        }
        //未登录
        response.sendRedirect("/login.html");
        return false;

    }
}
