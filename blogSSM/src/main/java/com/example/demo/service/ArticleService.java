package com.example.demo.service;

import com.example.demo.entity.Articleinfo;
import com.example.demo.mapper.ArticleMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ArticleService {

    @Resource
    private ArticleMapper articleMapper;

    public int getTotalNumById(Integer id) {
        return articleMapper.getTotalNumById(id);
    }

    public List<Articleinfo> getAllMyArticle(Integer id) {
        return articleMapper.getAllMyArticle(id);
    }

    public int delete(Integer articleId, Integer uid) {
        return articleMapper.delete(articleId, uid);
    }

    public Articleinfo getDetail(Integer articleId) {
        return articleMapper.getDetail(articleId);
    }

    public int incrementRcount(Integer id) {
        return articleMapper.incrementRcount(id);
    }

    public int add(Articleinfo articleinfo) {
        return articleMapper.add(articleinfo);
    }

    public int update(Articleinfo articleinfo) {
        return articleMapper.update(articleinfo);
    }

    public List<Articleinfo> getListsByPage(Integer limitSize, Integer offset) {
        return articleMapper.getListsByPage(limitSize, offset);
    }

    public int getTotalEntry() {
        return articleMapper.getTotalEntry();
    }
}
