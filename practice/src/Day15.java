import java.util.HashSet;
import java.util.Set;

public class Day15 {

    // https://leetcode.cn/problems/nge-tou-zi-de-dian-shu-lcof/
    // 剑指 Offer 60. n个骰子的点数
    // 动态规划解决问题
    // dp数组定义：dp[i][j] 当骰子个数为i时，点数为j时的组合
    // dp关系式：dp[i][j] = dp[i - 1][j - 1] + dp[i - 1][j - 2]+dp[i - 1][j - 3]+
    // dp[i - 1][j - 4]+dp[i - 1][j - 5]+dp[i - 1][j - 6]
    // dp初始化：dp[1][1~6] = 1
    public double[] dicesProbability(int n) {
        int[][] dp = new int[n + 1][6 * n + 1];

        // 初始化
        for (int i = 1; i <= 6; i++) {
            dp[1][i] = 1;
        }
        // dp
        // 骰子个数
        for (int i = 2; i <= n; i++) {
            // i个骰子的点数组合
            for (int j = i; j <= 6 * i ; j++) {
                // 实现dp状态转移方程
                for (int k = 1; k <= 6 ; k++) {
                    if(j < k) break;
                    dp[i][j] += dp[i - 1][j - k];
                }
            }
        }

        double[] result = new double[5 * n + 1];
        int index = 0;
        double total = Math.pow(6,n);
        for (int i = n; i <= 6 * n; i++) {
            result[index++] = dp[n][i] / total;
        }
        return result;
    }

    // https://leetcode.cn/problems/bu-ke-pai-zhong-de-shun-zi-lcof/
    // 剑指 Offer 61. 扑克牌中的顺子
    // 集合方法解决
    public boolean isStraight(int[] nums) {
        // 如果要成为一个顺子，满足的条件是
        // 1.除大小王以外不能有重复的牌
        // 2.牌中最大值减去最小值的差不能超过5，大小王除外
        // 判断是否有有重复的牌
        Set<Integer> set = new HashSet<>();
        int max = -1;
        int min = 7777;
        for (int num : nums) {
            if (num == 0)
                continue;
            if (set.contains(num))
                return false;
            set.add(num);
            max = Math.max(num, max);
            min = Math.min(num, min);
        }

        return max - min < 5;
    }

    // https://leetcode.cn/problems/yuan-quan-zhong-zui-hou-sheng-xia-de-shu-zi-lcof/
    // 剑指 Offer 62. 圆圈中最后剩下的数字
    // 关键点找出删除第k个节点之后，其他节点新的序号
    // f(n,m) = (f(n - 1,m) + m) % n;
    public int lastRemaining(int n, int m) {
        if(n == 0){
            return n;
        }
        //return (lastRemaining(n - 1,m) + m) % n;
        // 优化
        int res = 0;
        for (int i = 1; i <= n; i++) {
            res = (res + m) % i;
        }
        return res;
    }

    // https://leetcode.cn/problems/gu-piao-de-zui-da-li-run-lcof/
    // 剑指 Offer 63. 股票的最大利润
    public int maxProfit(int[] prices) {
        int min = Integer.MAX_VALUE;
        int max = 0;
        for (int price : prices){
            if(price < min){
                min = price;
            }else {
                max = Math.max(max,price - min);
            }
        }
        return max;
    }

    // https://leetcode.cn/problems/qiu-12n-lcof/
    // 剑指 Offer 64. 求1+2+…+n
    int sum;
    public int sumNums(int n) {
        boolean flag = n >= 1 && sumNums(n - 1) < 1;
        sum += n;
        return sum;
    }

    // https://leetcode.cn/problems/bu-yong-jia-jian-cheng-chu-zuo-jia-fa-lcof/
    // 剑指 Offer 65. 不用加减乘除做加法
    public int add(int a, int b) {
        while (b != 0) {
            int carry = (a & b) << 1;
            a = a ^ b;
            b = carry;
        }
        return a;
    }

    // https://leetcode.cn/problems/gou-jian-cheng-ji-shu-zu-lcof/solutions/208840/mian-shi-ti-66-gou-jian-cheng-ji-shu-zu-biao-ge-fe/
    // 剑指 Offer 66. 构建乘积数组
    public int[] constructArr(int[] a) {
        int len = a.length;
        if(len == 0)
            return new int[0];
        int[] b = new int[len];
        b[0] = 1;
        int tmp = 1;
        for(int i = 1; i < len; i++) {
            b[i] = b[i - 1] * a[i - 1];
        }
        for(int i = len - 2; i >= 0; i--) {
            tmp *= a[i + 1];
            b[i] *= tmp;
        }
        return b;
    }

     public class TreeNode { int val;
      TreeNode left;
      TreeNode right;
      TreeNode(int x) {
          val = x;
      }
    }

    // https://leetcode.cn/problems/er-cha-sou-suo-shu-de-zui-jin-gong-gong-zu-xian-lcof/
    // 剑指 Offer 68 - I. 二叉搜索树的最近公共祖先
    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
        while(root != null){
            if(root.val > p.val && root.val > q.val){
                root = root.left;
            }else if(root.val < p.val && root.val < q.val){
                root = root.right;
            }else {
                // 如果两个节点没有在同一边就说明已经找到了最近公共祖先节点
                return root;
            }
        }
        return null;
    }
}
