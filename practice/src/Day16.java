import java.util.*;

public class Day16 {
    // https://leetcode.cn/problems/climbing-stairs/
    // 70. 爬楼梯
    private Map<Integer,Integer> map = new HashMap<>();
    public int climbStairs(int n) {
        // 解法1直接循环实现
//        if(n == 1)
//            return 1;
//        if(n == 2)
//            return 2;
//        return climbStairs(n - 1) + climbStairs(n - 2);
        // 解法2存储每一次结算过的值
//        if(n == 1)
//            return 1;
//        if(n == 2)
//            return 2;
        // 查询本次n阶台阶是否已经计算过
//        if(map.get(n) != null){
//            return map.get(n);
//        }else {
//            int tmp = climbStairs(n - 1) + climbStairs(n - 2);
//            map.put(n,tmp);
//            return tmp;
        // 解法3从下往上计算
        int a = 1;// 一个台阶
        int b = 2;// 两个台阶
        int c = 0;
        if(n <= 2){
            return n;
        }
        for (int i = 3; i <= n; i++) {
            c = a + b;
            a = b;
            b = c;
        }
        return c;
    }

    // https://leetcode.cn/problems/two-sum/description/
    // 1. 两数之和
    public int[] twoSum(int[] nums, int target) {
        // key为值，value为元素下标
        Map<Integer,Integer> map = new HashMap<>(nums.length);
        int[] result = new int[2];
        for (int i = 0; i < nums.length; i++) {
            int need = target - nums[i];
            Integer index = map.get(need);
            if(index != null){
                result[0] = i;
                result[1] = index;
                break;
            }else {
                map.put(nums[i],i);
            }
        }
        return result;
    }

    // https://leetcode.cn/problems/merge-sorted-array/description/
    // 88. 合并两个有序数组
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        // 解法1 双指针正序处理
//        int[] newArray = new int[m+n];
//        // nums1下标，nums2下标
//        int i=0,j=0;
//        int k = 0;
//        int ret;
//        while(i+j<m+n){
//            // i等于m代表nums1数组取完，取nums2数组即可
//            if(i==m){
//                ret=nums2[j++];
//            // j等于n代表nums2数组取完，取nums1数组即可
//            }else if(j==n){
//                ret=nums1[i++];
//            // 比较nums1和nums2中i和j下标的值，取小值放入新数组中
//            }else if(nums1[i]>nums2[j]){
//                ret=nums2[j++];
//            }else{
//                ret=nums1[i++];
//            }
//            newArray[k++]=ret;
//        }
//        for (int l = 0; l < m+n; l++) {
//            nums1[l]=newArray[l];
//        }

        // 解法2 双指针倒序处理
        int nums1Index = m - 1;
        int nums2Index = n - 1;
        int index = m + n - 1;
        while(index >= 0){
            // 代表nums1数组已经取完，取nums2数组即可
            if(nums1Index < 0){
                nums1[index--] = nums2[nums2Index--];
            // 代表nums2数组已经取完，直接结束循环，因为nums1之前的数值已经处于合并后正确的位置
            } else if (nums2Index < 0) {
                break;
            }else if (nums1[nums1Index] > nums2[nums2Index]){
                nums1[index--] = nums1[nums1Index--];
            }else {
                nums1[index--] = nums2[nums2Index--];
            }
        }
    }

    // https://leetcode.cn/problems/move-zeroes/
    // 283. 移动零
    public void moveZeroes(int[] nums) {
        if(nums == null){
            return;
        }
        // 代表最后一个不为0的数字的后一个位置,也就是说index指向的位置之后包括自己的这些位置都是0
        int index = 0;
        for (int i = 0; i < nums.length; i++) {
            if(nums[i] != 0){
                nums[index++] = nums[i];
            }
        }
        for (int i = index; i < nums.length; i++) {
            nums[i] = 0;
        }
    }

    // https://leetcode.cn/problems/find-all-numbers-disappeared-in-an-array/
    // 448. 找到所有数组中消失的数字
    public List<Integer> findDisappearedNumbers(int[] nums) {
        List<Integer> result = new ArrayList<>();
        int n = nums.length;

        // 没出现的数字不会对对应下标(数字-1)位置的数字进行处理
        for (int num : nums){
            int x = (num - 1) % n;
            nums[x] += n;
        }

        for (int i = 0; i < nums.length; i++) {
            // 没有进行处理的数字就说明该下标没有对应的数字(下标+1)
            if(nums[i] <= n){
                result.add(i + 1);
            }
        }

        return result;
    }

    // https://leetcode.cn/problems/que-shi-de-shu-zi-lcof/
    // 剑指 Offer 53 - II. 0～n-1中缺失的数字
    public int missingNumber(int[] nums) {
        int n = nums.length + 1;
        for (int i = 0; i < n - 1; i++) {
            // 数组中的元素对应下标 不对应直接返回 因为数组是有序的
            // 例如nums[0] = 0 nums[1] = 1
            // 利用数组元素是否和下标相同判断当前位置是否出现该出现的元素
            if (nums[i] != i) {
                return i;
            }
        }
        return n - 1;
    }

    // https://leetcode.cn/problems/merge-two-sorted-lists/
    // 21. 合并两个有序链表
    public static class ListNode {
        int val;
        ListNode next;
        ListNode() {}
        ListNode(int val) { this.val = val; }
        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }
    }
    // 双指针解决
    public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        ListNode ret = new ListNode(-1);
        ListNode temp = ret;
        while(list1 != null && list2 != null){
            if(list1.val < list2.val){
                temp.next = list1;
                list1=list1.next;
            }else{
                temp.next = list2;
                list2 = list2.next;
            }
            temp = temp.next;

        }
        //1.list1为空
        if(list1!=null){
            temp.next = list1;
        }
        //2.list2为空
        if(list2!=null){
            temp.next = list2;
        }
        return ret.next;
    }

    // 递归解决
    public ListNode mergeTwoLists2(ListNode list1, ListNode list2) {
        if(list1 == null){
            return list2;
        }
        if(list2 == null){
            return list1;
        }
        if(list1.val < list2.val){
            list1.next = mergeTwoLists2(list1.next,list2);
            return list1;
        }
        list2.next = mergeTwoLists2(list1, list2.next);

        return list2;
    }

    // https://leetcode.cn/problems/remove-duplicates-from-sorted-list/
    // 83. 删除排序链表中的重复元素
    public ListNode deleteDuplicates(ListNode head) {
        if(head == null){
            return null;
        }
        ListNode currentNode = head;
        while(currentNode.next != null){
            if(currentNode.val == currentNode.next.val){
                currentNode.next = currentNode.next.next;
            }else {
                currentNode = currentNode.next;
            }
        }
        return head;
    }

    // https://leetcode.cn/problems/linked-list-cycle/
    // 141. 环形链表
    // 哈希法
    public boolean hasCycle1(ListNode head) {
        Set<ListNode> seen = new HashSet<ListNode>();
        while (head != null) {
            if (!seen.add(head)) {
                return true;
            }
            head = head.next;
        }
        return false;
    }

    // 快慢指针方法
    public boolean hasCycle(ListNode head) {
        if(head == null){
            return false;
        }
        ListNode slowNode = head;
        ListNode fastNode = head;
        while(slowNode.next != null && fastNode.next.next != null){
            slowNode = slowNode.next;
            fastNode = fastNode.next.next;
            if(slowNode == fastNode){
                return true;
            }
        }
        return false;
    }

    // https://leetcode.cn/problems/linked-list-cycle-ii/
    // 142. 环形链表 II
    // 快慢指针解法
    public ListNode detectCycle(ListNode head) {
        if(head == null){
            return null;
        }

        ListNode fast = head;
        ListNode slow = head;
        boolean loopExist = false;
        while(fast.next != null && fast.next.next != null){
            fast = fast.next.next;
            slow = slow.next;
            // 判断是否为环
            if(fast == slow)
            {
                loopExist = true;
                break;
            }
        }

        if(loopExist){
            fast = head;
            while(fast != slow){
                fast = fast.next;
                slow = slow.next;
            }
            return slow;
        }
        return null;
    }

    // https://leetcode.cn/problems/intersection-of-two-linked-lists/
    // 160. 相交链表
    // 哈希法
    public ListNode getIntersectionNode1(ListNode headA, ListNode headB) {
        Set<ListNode> visit = new HashSet<ListNode>();
        ListNode temp = headA;
        while (temp != null) {
            visit.add(temp);
            temp = temp.next;
        }
        temp = headB;
        while (temp != null) {
            // 在链表a中查看是否存在相同的节点
            if (visit.contains(temp)) {
                return temp;
            }
            temp = temp.next;
        }
        //没有相交结点
        return null;
    }

    // 快慢指针原理  也可也比较长度，让长度先走差值步数,然后再一起走
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        if(headA == null || headB == null){
            return null;
        }
        ListNode pA = headA;
        ListNode pB = headB;
        while(pA != pB){
            pA = pA == null ? headB : pA.next;
            pB = pB == null ? headA : pB.next;
        }

        return pA;
    }

    // https://leetcode.cn/problems/reverse-linked-list/
    // 206. 反转链表
    public ListNode reverseList(ListNode head) {
        ListNode prevNode = null;
        ListNode currNode = head;
        while (currNode != null) {
            ListNode next = currNode.next;
            currNode.next = prevNode;
            prevNode = currNode;
            currNode = next;
        }
        return prevNode;
    }

    // https://leetcode.cn/problems/palindrome-linked-list/description/
    // 234. 回文链表
    // 思路：找到回文连链表中前半段结束的位置，开始截断，比较前段和后段的值是否相同
    public boolean isPalindrome(ListNode head) {
        ListNode fastNode = head;
        ListNode slowNode = head;
        //    偶数个节点            奇数个节点
        // 例如 1-2-3-3-2-1 要找到第2个3的位置
        while(fastNode != null && fastNode.next != null){
            fastNode = fastNode.next.next;
            slowNode = slowNode.next;
        }
        // 如果是奇数个节点
        // 例如 1-2-4-2-1 要找到4后面的2的位置
        if(fastNode != null){
            slowNode = slowNode.next;
        }
        // 反转链表
        slowNode = reverse(slowNode);
        fastNode = head;

        while(slowNode != null){
            if(slowNode.val != fastNode.val){
                return false;
            }
            slowNode = slowNode.next;
            fastNode = fastNode.next;
        }

        return true;
    }

    // 反转链表
    private ListNode reverse(ListNode head) {
        ListNode preNode = null;
        ListNode currentNode = head;
        while(currentNode != null){
            ListNode next = currentNode.next;
            currentNode.next = preNode;
            preNode = currentNode;
            currentNode = next;
        }
        return preNode;
    }

    // https://leetcode.cn/problems/middle-of-the-linked-list/
    // 876. 链表的中间结点
    public ListNode middleNode(ListNode head) {
        ListNode fast = head;
        ListNode slow = head;
        while(fast!=null&&fast.next!=null){
            fast = fast.next.next;
            slow = slow.next;
        }
        return slow;
    }

    // https://leetcode.cn/problems/lian-biao-zhong-dao-shu-di-kge-jie-dian-lcof/
    // 剑指 Offer 22. 链表中倒数第k个节点
    public ListNode getKthFromEnd(ListNode head, int k) {
        if (head == null){
            return null;
        }
        ListNode low = head;
        ListNode fast = head;
        int count = 0;
        // 快的先走k步
        while(count != k){
            if(fast == null){
                return null;
            }
            fast = fast.next;
            count++;
        }
        while(fast!= null){
            fast = fast.next;
            low = low.next;
        }
        return low;
    }

    // https://leetcode.cn/problems/implement-queue-using-stacks/
    // 232. 用栈实现队列
    Stack<Integer> inStack;
    Stack<Integer> outStack;
    public Day16() {
        inStack = new Stack<>();
        outStack = new Stack<>();
    }

    public void push(int x) {
        inStack.push(x);
    }

    public int pop() {
        if(outStack.isEmpty()){
            // 把输入栈中的元素压入输出栈
            inToOut();
        }
        return outStack.pop();
    }

    public int peek() {
        if(outStack.isEmpty()){
            // 把输入栈中的元素压入输出栈
            inToOut();
        }
        return outStack.peek();
    }

    public boolean empty() {
        return inStack.isEmpty() && outStack.isEmpty();
    }

    private void inToOut() {
        while(!inStack.isEmpty()){
            outStack.push(inStack.pop());
        }
    }

    // https://leetcode.cn/problems/decode-string/
    // 394. 字符串解码
    public String decodeString(String s) {
        Stack<String> resStack = new Stack<>(); //记录字符
        Stack<Integer> counStack = new Stack<>(); //记录左括号前的数字
        // 拼接[]之间的字符成为一个整体
        // 初始化为空
        String res = "";
        // 字符串下标
        int index = 0;
        while(index < s.length()){
            char cur = s.charAt(index);
            if(Character.isDigit(cur)){
                // 处理数字
                StringBuilder ret = new StringBuilder();
                while(Character.isDigit(s.charAt(index))){
                    ret.append(s.charAt(index));
                    index++;
                }
                counStack.push(Integer.parseInt(ret.toString()));
            } else if(cur == '['){
                // 左括号[
                resStack.push(res);
                // 拼接下一个[]之间的字符
                res = "";
                index++;
            } else if (cur == ']') {
                // 右括号]
                // 拼接最里面[]的字符串到临近外层[]去
                StringBuilder ret = new StringBuilder(resStack.pop());
                int repeatTimes = counStack.pop();
                for (int i = 0; i < repeatTimes; i++) {
                    ret.append(res);
                }
                res = ret.toString();
                index++;
            }else {
                // 普通字符
                res += s.charAt(index);
                index++;
            }
        }
        return res;
    }
}
