import java.util.*;

public class Day9 {

    // 剑指 Offer 35. 复杂链表的复制
    // https://leetcode.cn/problems/fu-za-lian-biao-de-fu-zhi-lcof/
    static class Node {
        int val;
        Node next;
        Node random;
        Node left;
        Node right;

        public Node(int val) {
            this.val = val;
            this.next = null;
            this.random = null;
        }

        public Node(int _val,Node _left,Node _right) {
            val = _val;
            left = _left;
            right = _right;
        }
    }
    public Node copyRandomList(Node head) {
        if(head == null){
            return null;
        }

        // 复制链表的节点
        Node cur = head;
        while(cur != null){
            Node next  = cur.next;
            cur.next = new Node(cur.val);
            cur.next.next = next;
            cur = next;
        }

        // 复制链表的随机节点  随机节点可能为空
        cur = head;
        Node newHead;
        while(cur != null){
            newHead = cur.next;
            newHead.random = cur.random == null ? null : cur.random.next;
            cur = cur.next.next;
        }
        // 拆分链表的节点
        newHead = head.next;
        cur = head;
        Node curNewHead = head.next;
        while(cur != null){
            cur.next = cur.next.next;
            cur = curNewHead.next;
            curNewHead.next = cur == null ? null : cur.next;
            curNewHead = curNewHead.next;
        }
        return newHead;
    }

    // 剑指 Offer 36. 二叉搜索树与双向链表
    // https://leetcode.cn/problems/er-cha-sou-suo-shu-yu-shuang-xiang-lian-biao-lcof/
    public Node treeToDoublyList(Node root) {
        if(root == null){
            return null;
        }

        Queue<Node> queue = new LinkedList<>();

        // 中序遍历，把遍历的节点存入队列
        inOrder(root,queue);

        Node head = queue.poll();
        Node pre = head;
        // 取出队列的元素
        while(!queue.isEmpty()){
            Node cur = queue.poll();
            pre.right = cur;
            cur.left = pre;
            pre = cur;
        }

        pre.right = head;
        head.left = pre;

        return  head;
    }

    private void inOrder(Node root, Queue<Node> queue) {
        if(root == null){
            return;
        }
        inOrder(root.left,queue);
        queue.add(root);
        inOrder(root.right,queue);
    }

    // 剑指 Offer 37. 序列化二叉树
    // https://leetcode.cn/problems/xu-lie-hua-er-cha-shu-lcof/
    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode(int x) {
            val = x;
        }
    }
    // 层次遍历二叉树，为null的子节点也要保存进去
    public String serialize(TreeNode root) {
        if(root == null){
            return "";
        }
        StringBuilder sb = new StringBuilder();
        Queue<TreeNode> queue = new LinkedList<>();
        queue.add(root);
        TreeNode tmp;
        while(!queue.isEmpty()){
            tmp = queue.poll();
            if(tmp != null){
                sb.append(tmp.val).append(",");
                queue.add(tmp.left);
                queue.add(tmp.right);
            }else{
                sb.append("null,");
            }
        }
        return sb.toString();
    }

    public TreeNode deserialize(String data) {
        if(data == null || data.length() <= 0 ){
            return null;
        }
        String[] strings = data.split(",");
        Queue<TreeNode> queue = new LinkedList<>();
        TreeNode root = new TreeNode(Integer.parseInt(strings[0]));
        queue.add(root);
        TreeNode tmp;
        int i = 1;
        while(!queue.isEmpty()){
            tmp = queue.poll();
            if(!strings[i].equals("null")){
                TreeNode left = new TreeNode(Integer.parseInt(strings[i]));
                tmp.left = left;
                queue.add(left);
            }
            i++;
            if(!strings[i].equals("null")){
                TreeNode right = new TreeNode(Integer.parseInt(strings[i]));
                tmp.right = right;
                queue.add(right);
            }
            i++;

        }
        return root;
    }

    // 剑指 Offer 38. 字符串的排列
    // https://leetcode.cn/problems/zi-fu-chuan-de-pai-lie-lcof/
    // 全排列问题，构造n叉树解决,但是要进行剪枝
    List<String> res;
    boolean[] visited;
    StringBuilder sb;
    public String[] permutation(String s) {
        this.res = new ArrayList<>();
        char[] chars = s.toCharArray();
        this.visited = new boolean[chars.length];
        sb = new StringBuilder();
        Arrays.sort(chars);
        bfs(chars,0);
        String[] results = new String[res.size()];
        for (int i = 0; i < res.size(); i++) {
            results[i] = res.get(i);
        }
        return results;
    }

    private void bfs(char[] chars, int k) {
        if(chars.length == k){
            res.add(sb.toString());
            return;
        }
        for (int i = 0; i < chars.length; i++) {
            // 剪枝，当当前字符和前一个字符相等，并且前一个字符没有被遍历过，代表是重复的遍历，直接跳过即可
            if(i > 0 && !visited[i - 1] && (chars[i] == chars[i - 1])){
                continue;
            }
            if (!visited[i]) {
                visited[i] = true;
                sb.append(chars[i]);
                bfs(chars,k + 1);
                sb.deleteCharAt(sb.length() - 1);
                visited[i] = false;
            }
        }
    }

    // 剑指 Offer 39. 数组中出现次数超过一半的数字
    // https://leetcode.cn/problems/shu-zu-zhong-chu-xian-ci-shu-chao-guo-yi-ban-de-shu-zi-lcof/
    // 方法1 哈希法
    public int majorityElement1(int[] nums) {
        Map<Integer, Integer> map = new HashMap<>();
        int n = nums.length / 2;
        for(int num : nums) {
            map.put(num, map.getOrDefault(num, 0) + 1);
            if(map.get(num) > n) {
                return num;
            }
        }
        return 0;
    }

    // 方法2 排序法
    public int majorityElement2(int[] nums) {
        Arrays.sort(nums);
        return nums[nums.length / 2];
    }

    // 方法3 摩尔投票法
    // 利用众数的个数一定是大于等于数组长度的一半的性质，进行抵消
    public int majorityElement(int[] nums) {
        int mostValue = nums[0];
        int sum = 1;
        for (int i = 1; i < nums.length; i++) {
            if(sum == 0){
                mostValue = nums[i];
                sum = 1;
            }else {
                if (mostValue == nums[i]) {
                    sum++;
                } else {
                    sum--;
                }
            }
        }
        return mostValue;
    }
}
