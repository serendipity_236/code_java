import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

public class Day21 {


    // 基础
    // 求0-1范围中
    // 对任意出现的数的概率从x，调整为x2
    public static double xToX2(){
        return Math.max(Math.random(),Math.random());
    }

    // 从a-b等概率出现，实现c-d等概率出现
    // 已知f函数等概率出现1-5，要求模拟使用f函数使1-7也等概率出现的g函数
    // f函数如下
    // 步骤等概率得到0~1，然后在做到0~(7-1)等概率出现，最后结果加+1就是1~7等概率出现
    // 已知a~b等概率出现，要得到c~d等概率,先得到0~1等概率，然后再得到0~(d-c)(看需要几个二进制位可以可以包括d-c,如果出现的数字比d-c大，就重做，直到小于d-c)
    // 然后结果再加c，就得到c~d等概率出现
    public static int f(){
        return (int) (Math.random() * 5) + 1;
    }
    // 使用f函数等概率返回0-1
    public static int f2(){
        int ans = 0;
        do {
            ans = f();
        }while (ans == 3);
        return ans < 3 ? 0 : 1;
    }
    // 得到000~111等概率  0~7等概率返回
    public static int f3(){
        return f2() << 2 + f2() << 1 + f2();
    }
    // 实现0~6等概率返回
    public static int f4(){
        int ans = 0;
        do {
            ans = f3();
        }while (ans == 7);
        return ans;
    }
    // 实现1~7等概率返回
    public static int g(){
        return f4() + 1;
    }

    //从01不等概率出现随机到01等概率出现
    // 已知函数
    public static int x(){
        return Math.random() < 0.84 ? 0 : 1;
    }
    // 转换为等概率函数 00和11重做 01返回0，10返回0,实现等概率
    public static int y(){
        int ans = 0;
        do {
            ans = x();
        }while (ans == x());
        return ans;
    }

    // 对数器,用于调试代码的正确性
    public static int[] generateRandomArray(int maxSize, int maxValue) {
        // Math.random() [0,1)
        // Math.random() * N [0,N)
        // (int)(Math.random() * N) [0, N-1]
        int[] arr = new int[(int) ((maxSize + 1) * Math.random())];
        for (int i = 0; i < arr.length; i++) {
            // [-? , +?]
            arr[i] = (int) ((maxValue + 1) * Math.random()) - (int) (maxValue * Math.random());
        }
        return arr;
    }

    // 有序数组中找到>=num的最左位置
    public static int mostLeftNoLessNumIndex(int[] arr, int num) {
        if (arr == null || arr.length == 0) {
            return -1;
        }
        int L = 0;
        int R = arr.length - 1;
        int ans = -1;
        while (L <= R) {
            int mid = (L + R) / 2;
            if (arr[mid] >= num) {
                ans = mid;
                R = mid - 1;
            } else {
                L = mid + 1;
            }
        }
        return ans;
    }
    // 有序数组中找到<=num的最右位置
    public static int nearestIndex(int[] arr, int value) {
        int L = 0;
        int R = arr.length - 1;
        int ans = -1; // 记录最右的对号
        while (L <= R) {
            int mid = L + ((R - L) >> 1);
            if (arr[mid] <= value) {
                ans = mid;
                L = mid + 1;
            } else {
                R = mid - 1;
            }
        }
        return ans;
    }

    // 局部最小值问题 待搜索数组中，每两个相邻的元素一定不相等
    public static int oneMinIndex(int[] arr) {
        if (arr == null || arr.length == 0) {
            return -1;
        }
        int N = arr.length;
        if (N == 1) {
            return 0;
        }
        // 判断开头
        if (arr[0] < arr[1]) {
            return 0;
        }
        // 判断结尾
        if (arr[N - 1] < arr[N - 2]) {
            return N - 1;
        }
        int L = 0;
        int R = N - 1;
        // L...R 肯定有局部最小
        while (L < R - 1) {
            int mid = (L + R) / 2;
            if (arr[mid] < arr[mid - 1] && arr[mid] < arr[mid + 1]) {
                return mid;
            } else {
                if (arr[mid] > arr[mid - 1]) {
                    R = mid - 1;
                } else {
                    L = mid + 1;
                }
            }
        }
        return arr[L] < arr[R] ? L : R;
    }
    // 生成随机数组，且相邻数不相等
    public static int[] randomArray(int maxLen, int maxValue) {
        int len = (int) (Math.random() * maxLen);
        int[] arr = new int[len];
        if (len > 0) {
            arr[0] = (int) (Math.random() * maxValue);
            for (int i = 1; i < len; i++) {
                do {
                    arr[i] = (int) (Math.random() * maxValue);
                } while (arr[i] == arr[i - 1]);
            }
        }
        return arr;
    }

    private static class DoubleNode {
        DoubleNode last;
        DoubleNode next;
        int val;
        public DoubleNode(){

        }
        public DoubleNode(int val){
            this.val = val;
        }
    }

    // 双端链表的逆转
    public static DoubleNode reverseDoubleList(DoubleNode head){
        DoubleNode pre = null;
        DoubleNode next= null;
        while(head != null){
            next = head.next;
            head.next = pre;
            head.last = next;
            pre = head;
            head = next;
        }
        return pre;
    }

    // 单链表实现栈和队列
    public static class Node<V> {
        public V value;
        public Node<V> next;
        public Node<V> last;

        public Node(V v) {
            value = v;
            next = null;
        }
    }

    public static class MyQueue<V> {
        private Node<V> head;
        private Node<V> tail;
        private int size;

        public MyQueue() {
            head = null;
            tail = null;
            size = 0;
        }

        public boolean isEmpty() {
            return size == 0;
        }

        public int size() {
            return size;
        }

        public void offer(V value) {
            // 尾插法
            Node<V> cur = new Node<V>(value);
            // 尾部为空说明当前没有节点
            if (tail == null) {
                head = cur;
            } else {
                tail.next = cur;
            }
            tail = cur;
            size++;
        }

        public V poll() {
            // 头出法
            V ans = null;
            if (head != null) {
                ans = head.value;
                head = head.next;
                size--;
            }
            if (head == null) {
                tail = null;
            }
            return ans;
        }

        public V peek() {
            V ans = null;
            if (head != null) {
                ans = head.value;
            }
            return ans;
        }

    }

    public static class MyStack<V> {
        private Node<V> head;
        private int size;

        public MyStack() {
            head = null;
            size = 0;
        }

        public boolean isEmpty() {
            return size == 0;
        }

        public int size() {
            return size;
        }

        public void push(V value) {
            // 头插法
            Node<V> cur = new Node<>(value);
            // 当前没有节点
            if (head == null) {
                head = cur;
            } else {
                cur.next = head;
                head = cur;
            }
            size++;
        }

        public V pop() {
            V ans = null;
            if (head != null) {
                ans = head.value;
                head = head.next;
                size--;
            }
            return ans;
        }

        public V peek() {
            return head != null ? head.value : null;
        }

    }
    // 对数器测试自定义队列和栈
    public static void testQueue() {
        MyQueue<Integer> myQueue = new MyQueue<>();
        Queue<Integer> test = new LinkedList<>();
        int testTime = 5000000;
        int maxValue = 200000000;
        System.out.println("测试开始！");
        for (int i = 0; i < testTime; i++) {
            if (myQueue.isEmpty() != test.isEmpty()) {
                System.out.println("Oops!");
            }
            if (myQueue.size() != test.size()) {
                System.out.println("Oops!");
            }
            double decide = Math.random();
            if (decide < 0.33) {
                int num = (int) (Math.random() * maxValue);
                myQueue.offer(num);
                test.offer(num);
            } else if (decide < 0.66) {
                if (!myQueue.isEmpty()) {
                    int num1 = myQueue.poll();
                    int num2 = test.poll();
                    if (num1 != num2) {
                        System.out.println("Oops!");
                    }
                }
            } else {
                if (!myQueue.isEmpty()) {
                    int num1 = myQueue.peek();
                    int num2 = test.peek();
                    if (num1 != num2) {
                        System.out.println("Oops!");
                    }
                }
            }
        }
        if (myQueue.size() != test.size()) {
            System.out.println("Oops!");
        }
        while (!myQueue.isEmpty()) {
            int num1 = myQueue.poll();
            int num2 = test.poll();
            if (num1 != num2) {
                System.out.println("Oops!");
            }
        }
        System.out.println("测试结束！");
    }

    public static void testStack() {
        MyStack<Integer> myStack = new MyStack<>();
        Stack<Integer> test = new Stack<>();
        int testTime = 5000000;
        int maxValue = 200000000;
        System.out.println("测试开始！");
        for (int i = 0; i < testTime; i++) {
            if (myStack.isEmpty() != test.isEmpty()) {
                System.out.println("Oops!");
            }
            if (myStack.size() != test.size()) {
                System.out.println("Oops!");
            }
            double decide = Math.random();
            if (decide < 0.33) {
                int num = (int) (Math.random() * maxValue);
                myStack.push(num);
                test.push(num);
            } else if (decide < 0.66) {
                if (!myStack.isEmpty()) {
                    int num1 = myStack.pop();
                    int num2 = test.pop();
                    if (num1 != num2) {
                        System.out.println("Oops!");
                    }
                }
            } else {
                if (!myStack.isEmpty()) {
                    int num1 = myStack.peek();
                    int num2 = test.peek();
                    if (num1 != num2) {
                        System.out.println("Oops!");
                    }
                }
            }
        }
        if (myStack.size() != test.size()) {
            System.out.println("Oops!");
        }
        while (!myStack.isEmpty()) {
            int num1 = myStack.pop();
            int num2 = test.pop();
            if (num1 != num2) {
                System.out.println("Oops!");
            }
        }
        System.out.println("测试结束！");
    }

    public static void main(String[] args) {
        testQueue();
        testStack();
    }

    // 双端链表实现双端队列
    public static class MyDeque<V> {
        private Node<V> head;
        private Node<V> tail;
        private int size;

        public MyDeque() {
            head = null;
            tail = null;
            size = 0;
        }

        public boolean isEmpty() {
            return size == 0;
        }

        public int size() {
            return size;
        }

        public void pushHead(V value) {
            Node<V> cur = new Node<>(value);
            // 当前位置没有节点
            if (head == null) {
                head = cur;
                tail = cur;
            } else {
                cur.next = head;
                head.last = cur;
                head = cur;
            }
            size++;
        }

        public void pushTail(V value) {
            Node<V> cur = new Node<>(value);
            if (head == null) {
                head = cur;
                tail = cur;
            } else {
                tail.next = cur;
                cur.last = tail;
                tail = cur;
            }
            size++;
        }

        public V pollHead() {
            V ans = null;
            if (head == null) {
                return null;
            }
            size--;
            ans = head.value;
            if (head == tail) {
                head = null;
                tail = null;
            } else {
                head = head.next;
                head.last = null;
            }
            return ans;
        }

        public V pollTail() {
            V ans = null;
            if (head == null) {
                return ans;
            }
            size--;
            ans = tail.value;
            if (head == tail) {
                head = null;
                tail = null;
            } else {
                tail = tail.last;
                tail.next = null;
            }
            return ans;
        }

        public V peekHead() {
            V ans = null;
            if (head != null) {
                ans = head.value;
            }
            return ans;
        }

        public V peekTail() {
            V ans = null;
            if (tail != null) {
                ans = tail.value;
            }
            return ans;
        }

    }

    // k个节点的组内逆序调整
    // https://leetcode.cn/problems/reverse-nodes-in-k-group/
    public static class ListNode {
        int val;
        ListNode next;
        ListNode() {}
        ListNode(int val) {
            this.val = val;
        }
        ListNode(int val, ListNode next) {
            this.val = val; this.next = next;
        }
    }

    public ListNode reverseKGroup(ListNode head, int k) {
        ListNode start = head;
        ListNode end = getKGroupEnd(start,k);
        // 链表长度不够k个
        if(end == null){
            return head;
        }
        // 新头部
        head = end;
        reverse(start,end);
        // 记录上一组逆序的结尾节点
        ListNode lastEnd = start;
        while(lastEnd.next != null){
            // 得到新一组的开头节点
            start = lastEnd.next;
            end = getKGroupEnd(start,k);
            if(end == null){
                return head;
            }
            reverse(start,end);
            lastEnd.next = end;
            lastEnd = start;
        }

        return head;
    }

    // 返回kg节点的结束位置
    public static ListNode getKGroupEnd(ListNode start,int k){
        while(--k != 0 && start != null){
            start = start.next;
        }
        return start;
    }
    // 反转链表
    public static void reverse(ListNode start,ListNode end){
        end = end.next;
        ListNode pre = null;
        ListNode next = null;
        ListNode cur = start;
        while(cur != end){
            next = cur.next;
            cur.next = pre;
            pre = cur;
            cur = next;
        }
        start.next = end;
    }

    // 两个链表相加
    public static ListNode addTwoNumbers(ListNode head1, ListNode head2) {

        int len1 = listLength(head1);
        int len2 = listLength(head2);

        ListNode l = len1 >= len2 ? head1 : head2;
        ListNode s = l == head1 ? head2 : head1;

        ListNode curL = l;
        ListNode curS = s;
        ListNode last = curL;
        // 记录进位
        int carry = 0;
        // 记录当前链表节点和
        int curNum = 0;

        // 先让短链表走完
        while (curS != null) {
            curNum = curL.val + curS.val + carry;
            curL.val = (curNum % 10);
            carry = curNum / 10;
            last = curL;
            curL = curL.next;
            curS = curS.next;
        }

        // 单独进行长链表
        while (curL != null) {
            curNum = curL.val + carry;
            curL.val = (curNum % 10);
            carry = curNum / 10;
            last = curL;
            curL = curL.next;
        }

        if (carry != 0) {
            last.next = new ListNode(1);
        }

        return l;
    }

    // 求链表长度
    public static int listLength(ListNode head) {
        int len = 0;
        while (head != null) {
            len++;
            head = head.next;
        }
        return len;
    }

    // 一个数组中只有两个数出现了奇数次，其他的数都出现了偶数次，求出现奇数次的两个数
    public static int[] getTwoOddNum(int[] nums){
        int eor = 0;
        for (int num : nums) {
            eor ^= num;
        }
        int firstNum = 0;
        // 提取出一个数最右侧的1
        int lastDifferentBit = eor & (~eor + 1);
        for (int num : nums) {
            if((lastDifferentBit & num) == 0){
                firstNum ^= num;
            }
        }
        int secondNum = eor ^ firstNum;
        return new int[]{firstNum,secondNum};
    }

    //

}
