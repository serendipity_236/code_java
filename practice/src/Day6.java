import java.util.Stack;

public class Day6 {

    // 剑指 Offer 27. 二叉树的镜像
    // https://leetcode.cn/problems/er-cha-shu-de-jing-xiang-lcof/
     public class TreeNode {
          int val;
          TreeNode left;
          TreeNode right;
          TreeNode(int x) {
              val = x; }
    }
    public TreeNode mirrorTree(TreeNode root) {
        if(root == null || (root.left == null && root.right == null)){
            return root;
        }
        TreeNode left = mirrorTree(root.left);
        TreeNode right = mirrorTree(root.right);

        root.left = right;
        root.right = left;

        return root;
    }

    // 剑指 Offer 28. 对称的二叉树
    // https://leetcode.cn/problems/dui-cheng-de-er-cha-shu-lcof/
    public boolean isSymmetric(TreeNode root) {
        if(root == null || (root.left == null && root.right == null)){
            return true;
        }
        return isMirrorTree(root.left,root.right);
    }

    private boolean isMirrorTree(TreeNode left, TreeNode right) {
         if(left == null && right == null){
             return true;
         }
         // 其中一个为null，就不是镜像二叉树
         if(left == null || right == null){
             return false;
         }
         // 两个当前子树的头节点的值不相同,也不是镜像二叉树
         if(left.val != right.val){
             return false;
         }
         return isMirrorTree(left.left,right.right) && isMirrorTree(left.right,right.left);
    }

    // 剑指 Offer 29. 顺时针打印矩阵
    // https://leetcode.cn/problems/shun-shi-zhen-da-yin-ju-zhen-lcof/
    public int[] spiralOrder(int[][] matrix) {
        if(matrix == null || matrix.length == 0 || matrix[0].length == 0){
            return new int[0];
        }
        int l = 0;
        int r = matrix[0].length - 1;
        int t = 0;
        int b = matrix.length - 1;
        int newLength = (r + 1) * (b + 1);
        int[] res = new int[newLength];
        int k = 0;
        while (true) {
            // 从左到右
            for (int i = t,j = l; j <= r; j++) {
                res[k++] = matrix[i][j];
            }
            t++;
            if(t > b){
                break;
            }
            // 从上到下
            for (int i = t,j = r; i <= b; i++) {
                res[k++] = matrix[i][j];
            }
            r--;
            if(l > r){
                break;
            }
            // 从右到左
            for (int i = b,j = r; j >= l ; j--) {
                res[k++] = matrix[i][j];
            }
            b--;
            if(t > b){
                break;
            }
            // 从下到上
            for (int i = b,j = l; i >= t ; i--) {
                res[k++] = matrix[i][j];
            }
            l++;
            if(l > r){
                break;
            }
        }
        return res;
    }

    // 剑指 Offer 30. 包含min函数的栈
    // https://leetcode.cn/problems/bao-han-minhan-shu-de-zhan-lcof/
    Stack<Integer> normalStack;
    Stack<Integer> minimumStack;
    public Day6() {
        this.normalStack = new Stack<>();
        this.minimumStack = new Stack<>();
    }

    public void push(int x) {
        normalStack.push(x);
        if(minimumStack.isEmpty() || minimumStack.peek() >= x){
            minimumStack.push(x);
        }
    }

    public void pop() {
        if(!normalStack.isEmpty()){
            // 这里要注意 当数值小于127的时候是比较的数值，但是超过了就是比较的对象了
            if(normalStack.peek().intValue() == minimumStack.peek().intValue()){
                normalStack.pop();
                minimumStack.pop();
            }else {
                normalStack.pop();
            }
        }
    }

    public int top() {
        return normalStack.peek();
    }

    public int min() {
        return minimumStack.peek();
    }

    // 剑指 Offer 31. 栈的压入、弹出序列
    // https://leetcode.cn/problems/zhan-de-ya-ru-dan-chu-xu-lie-lcof/
    public boolean validateStackSequences(int[] pushed, int[] popped) {
        if(pushed == null || pushed.length <= 0){
            return true;
        }
        Stack<Integer> stack = new Stack<>();
        for (int i = 0,j = 0; i < pushed.length; i++) {
            stack.push(pushed[i]);
            while(!stack.isEmpty() && (stack.peek() == popped[j])){
                stack.pop();
                j++;
            }
        }
        return stack.isEmpty();
    }
}
