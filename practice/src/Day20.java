import java.util.*;

public class Day20 {

    // https://leetcode.cn/problems/champagne-tower/
    // 799. 香槟塔
    // 模拟过程即可得出题解
    public double champagneTower(int poured, int query_row, int query_glass) {
        double[][] result = new double[101][101];
        result[0][0] = poured;
        for (int i = 0; i <= query_row; i++) {
            for (int j = 0; j <= i; j++) {
                double next = (result[i][j] - 1.0) / 2;
                if(next > 0){
                    result[i + 1][j] += next;
                    result[i + 1][j + 1] += next;
                }
            }
        }
        return Math.min(1.0,result[query_row][query_glass]);
    }

    // https://leetcode.cn/problems/tic-tac-toe-lcci/
    // 面试题 16.04. 井字游戏
    public String tictactoe(String[] board) {
        int length = board.length;
        int row = 0; // 横的和
        int column = 0; // 纵的和
        int left = 0; // 左斜线
        int right = 0; // 右斜线
        boolean flag = false; // 记录有没有空格

        for (int i = 0; i < length; i++) {

            // 重置
            row = 0; column = 0;

            for (int j = 0; j < length; j++) {

                row += board[i].charAt(j);
                column += board[j].charAt(i);

                if(board[i].charAt(j) == ' ')
                    flag = true;

            }

            // 横纵检查
            if (row == (int)'X' * length || column == (int)'X' * length)
                return "X";
            if (row == (int)'O' * length || column == (int)'O' * length)
                return "O";

            // 无需重置，因为一个棋盘中只可能出现一个斜线和反斜线都是一样的情况，统计完所有棋盘在比较
            // 两条斜线上的相加
            left = left + (int)board[i].charAt(i);
            right = right + (int)board[i].charAt(length - i - 1);

        }

        // 两条斜线检查
        if (left == (int)'X' * length || right == (int)'X' * length)
            return "X";
        if (left == (int)'O' * length || right == (int)'O' * length)
            return "O";

        if (flag)
            return "Pending";
        return "Draw";

    }

    // https://leetcode.cn/problems/find-the-index-of-the-first-occurrence-in-a-string/
    // 28. 找出字符串中第一个匹配项的下标
    // 暴力匹配
    public int strStr1(String haystack, String needle) {
        int n = haystack.length(), m = needle.length();
        for (int i = 0; i + m <= n; i++) {
            boolean flag = true;
            for (int j = 0; j < m; j++) {
                if (haystack.charAt(i + j) != needle.charAt(j)) {
                    flag = false;
                    break;
                }
            }
            if (flag) {
                return i;
            }
        }
        return -1;
    }

    // KMP算法
    public int strStr(String haystack, String needle){
        int[] next = new int[needle.length() + 1];
        getNext(needle.toCharArray(),next);
        return KMP(haystack.toCharArray(),needle.toCharArray(),next);
    }

    private int KMP(char[] str, char[] pattern, int[] next) {
        int i = 0;
        int j = 0;
        while(i < str.length && j < pattern.length){
            if(j == -1 || str[i] == pattern[j]){
                i++;
                j++;
            }
            else {
                j = next[j];
            }
        }
        if(j == pattern.length){
            return i - j;
        }else {
            return -1;
        }
    }

    public void getNext(char[] pattern,int[] next){
        next[0] = -1;
        int i = 0;
        int j = -1;
        while(i < pattern.length){
            if(j == -1){
                i++;
                j++;
            } else if (pattern[i] == pattern[j]) {
                i++;
                j++;
                next[i] = j;
            }else {
                j = next[j];
            }
        }
    }

    // https://leetcode.cn/problems/house-robber/
    // 198. 打家劫舍
    public int rob(int[] nums) {
        //return maxMoney(nums, nums.length - 1);
        return maxMoneyDp(nums);
    }

    // 递归 时间太长不推荐
    public int maxMoney(int[] moneys,int index){
        // 递归结束条件
        if(moneys == null || index < 0){
            return 0;
        }
        // 只有一个元素
        if(index == 0){
            return moneys[index];
        }
        return Math.max(maxMoney(moneys,index - 1),maxMoney(moneys,index - 2) + moneys[index]);
    }

    // 动态规划
    public int maxMoneyDp(int[] nums) {
        int length = nums.length;

        // 只有一个元素
        if(length == 1){
            return nums[0];
        }

        int[] dp = new int[length];
        dp[0] = nums[0];
        dp[1] = Math.max(nums[0],nums[1]);
        for (int i = 2; i < length; i++) {
            dp[i] = Math.max(dp[i - 1],dp[i - 2] + nums[i]);
        }
        return dp[length - 1];
    }

    // https://leetcode.cn/problems/house-robber-ii/
    // 213. 打家劫舍 II
    public int rob2(int[] nums) {
        int oneWay = maxMoney2Dp(nums,0, nums.length - 2);
        int twoWay = maxMoney2Dp(nums,1,nums.length - 1);
        return Math.max(oneWay,twoWay);
    }

    public int maxMoney2Dp(int[] nums,int start,int end) {
        int length = nums.length;

        // 只有一个元素
        if (length == 1) {
            return nums[0];
        }

        if(length == 2){
            return Math.max(nums[0],nums[1]);
        }

        int first = nums[start];
        int second = Math.max(nums[start],nums[start + 1]);

        for (int i = start + 2; i <= end; i++) {
            int tmp = second;
            second = Math.max(first + nums[i],second);
            first = tmp;
        }

        return second;
    }

    // https://leetcode.cn/problems/house-robber-iii/
    // 打家劫舍 III
    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode() {

        }
        TreeNode(int val) {
            this.val = val;
        }
        TreeNode(int val, TreeNode left, TreeNode right) {
          this.val = val;
          this.left = left;
          this.right = right;
        }
    }
    public int rob3(TreeNode root) {
        int[] result = dfs(root);
        return Math.max(result[0],result[1]);
    }

    private int[] dfs(TreeNode root) {
        // int[]{选中当前节点的最优解，不选当前节点的最优解}
        if(root == null){
            return new int[]{0,0};
        }

        int[] left = dfs(root.left);
        int[] right = dfs(root.right);
        // 选中当前节点，当前节点的两个子节点都不能选择
        int select = root.val + left[1] + right[1];
        // 没有选中当前节点，剩余的两个子节点要满足各自的最优解
        int noSelect = Math.max(left[0],left[1]) + Math.max(right[0],right[1]);

        return new int[]{select,noSelect};
    }

    // https://leetcode.cn/problems/dota2-senate/
    // 649. Dota2 参议院
    public String predictPartyVictory(String senate) {
        Queue<Integer> r = new LinkedList<>();
        Queue<Integer> d = new LinkedList<>();
        for (int i = 0; i < senate.length(); i++) {
            if(senate.charAt(i) == 'R'){
                r.offer(i);
            }else {
                d.offer(i);
            }
        }
        while(!r.isEmpty() && !d.isEmpty()){
            int rPoll = r.poll();
            int dPoll = d.poll();
            // 每次让下标小的先投票，投出另一方下标小的
            if(rPoll < dPoll){
                // 把对方投出之后，自己进入下一轮投票，投票位置是之前下标位置加上字符串长度
                r.offer(rPoll + senate.length());
            }else {
                d.offer(dPoll + senate.length());
            }
        }
         return d.isEmpty() ? "R": "D";
    }

    // https://leetcode.cn/problems/advantage-shuffle/
    // 870. 优势洗牌
    public int[] advantageCount(int[] nums1, int[] nums2) {
        int[] sort2 = nums2.clone();
        Arrays.sort(sort2);
        Arrays.sort(nums1);

        Map<Integer, Deque<Integer>> map = new HashMap<>();
        for (int num : nums2) {
            map.put(num,new LinkedList<>());
        }
        Deque<Integer> deque = new LinkedList<>();
        int index = 0;
        for (int num : nums1) {
            if(num > sort2[index]){
                map.get(sort2[index]).add(num);
                index++;
            }else {
                deque.add(num);
            }
        }
        int[] result = new int[nums1.length];
        for (int i = 0; i < nums2.length; i++) {
            if(map.get(nums2[i]).size() > 0){
                result[i] = map.get(nums2[i]).removeLast();
            }else {
                result[i] = deque.removeLast();
            }
        }
        return result;
    }
}
