import java.util.*;

public class Day11 {

    // 剑指 Offer 45. 把数组排成最小的数
    // https://leetcode.cn/problems/ba-shu-zu-pai-cheng-zui-xiao-de-shu-lcof/
    public String minNumber(int[] nums) {
        String[] strs = new String[nums.length];
        for(int i = 0; i < nums.length; i++)
            strs[i] = String.valueOf(nums[i]);
        quickSort(strs, 0, strs.length - 1);
        StringBuilder res = new StringBuilder();
        for(String s : strs)
            res.append(s);
        return res.toString();
    }
    void quickSort(String[] strs, int l, int r) {
        if(l >= r) return;
        int i = l, j = r;
        String tmp = strs[i];
        // 转换规则
        while(i < j) {
            while((strs[j] + strs[l]).compareTo(strs[l] + strs[j]) >= 0 && i < j) j--;
            while((strs[i] + strs[l]).compareTo(strs[l] + strs[i]) <= 0 && i < j) i++;
            tmp = strs[i];
            strs[i] = strs[j];
            strs[j] = tmp;
        }

        strs[i] = strs[l];
        strs[l] = tmp;
        quickSort(strs, l, i - 1);
        quickSort(strs, i + 1, r);
    }

    // 剑指 Offer 46. 把数字翻译成字符串
    // https://leetcode.cn/problems/ba-shu-zi-fan-yi-cheng-zi-fu-chuan-lcof/
    // 求多少种方案一般采用动态规划来做
    public int translateNum(int num) {
        if(num <= 9){
            return 1;
        }
        char[] array = String.valueOf(num).toCharArray();
        //int[] dp = new int[array.length + 1];

        // 第i个字符可能组合
        //dp[0] = 1;
        //dp[1] = 1;
        // 用斐波那契数列的优化思想进行优化
        int a = 1;
        int b = 1;
        int c = 0;
        for (int i = 2; i <= array.length; i++) {
            // 判断i个字符中后两个是否能构成组合一起翻译
            int tmp = 10 * (array[i - 2] - '0') + (array[i - 1] - '0');
            if(tmp >= 10 && tmp <= 25){
                // 加上dp[i - 1]是直接把第i个字符插入到之前i-1个字符的每个种类中
                // 加上dp[i - 2]是把i - 1和i - 2个字符进行组成成新的字符加入到i-2个字符的种类中
                //dp[i] = dp[i - 1] + dp[i - 2];
                c = a + b;
            }else {
                //dp[i] = dp[i - 1];
                // 单独组合就是加上之前的组合种类就可以
                c = b;
            }
            a = b;
            b = c;
        }
        //return dp[array.length];
        return c;
    }

    // 剑指 Offer 47. 礼物的最大价值
    // https://leetcode.cn/problems/li-wu-de-zui-da-jie-zhi-lcof/
    // 方式1一般求最大最小值采用dp的方法,并且二维方格一般用二维dp数组来做
    public int maxValue(int[][] grid) {
        int m = grid.length, n = grid[0].length;
        int[][] dp = new int[m][n];
        dp[0][0]= grid[0][0];
        // 初始化
        for (int i = 1; i < n; i++) {
            dp[0][i] += dp[0][i - 1] + grid[0][i];
        }
        for (int i = 1; i < m; i++) {
            dp[i][0] += dp[i - 1][0] + grid[i][0];
        }
        for (int i = 1; i < m; ++i) {
            for (int j = 1; j < n; ++j) {
                dp[i][j] += Math.max(dp[i][j - 1],dp[i - 1][j]) + grid[i][j];
            }
        }
        return dp[m - 1][n - 1];
    }

    // 方式2优化版
    public int maxValue2(int[][] grid) {
        int m = grid.length, n = grid[0].length;
        int[] dp = new int[n];
        dp[0]= grid[0][0];
        for (int i = 1; i < n; i++) {
            dp[i] = dp[i - 1] + grid[0][i];
        }
        for (int i = 1; i < m; i++) {
            dp[0] = dp[0] + grid[i][0];
            for (int j = 1; j < n; j++) {
                // 画图理解
                dp[j] = Math.max(dp[j],dp[j - 1]) + grid[i][j];
            }
        }
        return dp[n - 1];
    }

    // 剑指 Offer 48. 最长不含重复字符的子字符串
    // https://leetcode.cn/problems/zui-chang-bu-han-zhong-fu-zi-fu-de-zi-zi-fu-chuan-lcof/
    public int lengthOfLongestSubstring(String s) {
        if(s.length() <= 0){
            return 0;
        }
        // 记录字符和下标的映射
        Map<Character,Integer> map = new HashMap<>();
        int n = s.length();
        // 优化dp数组->单个变量
        //int[] dp = new int[n];
        //dp[0] = 1;
        int tmp = 1;
        map.put(s.charAt(0),0);
        int res = 1;
        for (int i = 1; i < n; i++) {
            if(!map.containsKey(s.charAt(i))){
                //dp[i] = dp[i - 1] + 1;
                tmp = tmp + 1; //没有跟新tmp之前tmp表示dp[i - 1];
            }else {
                int k = map.get(s.charAt(i));
                //dp[i] = i - k <= dp[i - 1] ? i - k : dp[i - 1] + 1;
                // 当前第i个字符a与之前重复的字符b之间相差的字符个数是否小于等于以字符b结尾的不重复字串的个数
                // 小于等于说明b也在子串中，因此跟新大小位i - k
                // 反之说明b不在子串中，把a加入进去
                tmp = i - k <= tmp ? i - k : tmp + 1;
            }
            //res = Math.max(res,dp[i]);
            res = Math.max(res,tmp);
            map.put(s.charAt(i),i);
        }
        return res;
    }

    // 剑指 Offer 49. 丑数
    // https://leetcode.cn/problems/chou-shu-lcof/
    // 方法1最小堆解决
    public int nthUglyNumber(int n) {
        int[] factors = {2, 3, 5};
        Set<Long> seen = new HashSet<>();
        PriorityQueue<Long> heap = new PriorityQueue<>();
        seen.add(1L);
        heap.offer(1L);
        int ugly = 0;
        for (int i = 0; i < n; i++) {
            long curr = heap.poll();
            ugly = (int) curr;
            for (int factor : factors) {
                long next = curr * factor;
                if (seen.add(next)) {
                    heap.offer(next);
                }
            }
        }
        return ugly;
    }

    // 方法2动态规划解决
    public int nthUglyNumber2(int n) {
        int a = 1,b = 1,c = 1;
        int[] dp = new int[n + 1];
        dp[1] = 1;
        for (int i = 2; i < dp.length ; i++) {
            dp[i] = Math.min(Math.min(dp[a] * 2,dp[b] * 3),dp[c] * 5);
            if(dp[i] == dp[a] * 2)
                a++;
            if(dp[i] == dp[b] * 3)
                b++;
            if(dp[i] == dp[c] * 5)
                c++;
        }
        return dp[n];
    }
}
