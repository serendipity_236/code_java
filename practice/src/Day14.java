import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Day14 {

    // https://leetcode.cn/problems/he-wei-sde-liang-ge-shu-zi-lcof/
    // 剑指 Offer 57. 和为s的两个数字
    // 可以使用二分查找，也可也使用双指针的形式
    // 双指针形式
    public int[] twoSum(int[] nums, int target) {
        if(nums == null || nums.length == 0){
            return new int[]{};
        }
        int left = 0;
        int right = nums.length - 1;
        int[] array = new int[2];
        while(left < right){
            if(nums[left] + nums[right] < target){
                left++;
            } else if (nums[left] + nums[right] > target) {
                right--;
            }else {
                array[0] = nums[left];
                array[1] = nums[right];
                return array;
            }
        }

        return new int[]{};
    }

    // https://leetcode.cn/problems/he-wei-sde-lian-xu-zheng-shu-xu-lie-lcof/
    // 剑指 Offer 57 - II. 和为s的连续正数序列
    // 滑动窗口解法
    public int[][] findContinuousSequence(int target) {
        // 保存结果的list集合
        List<int[]> result = new ArrayList<>();
        int sum = 1;
        int i = 1,j = 1;
        // 因为题目说了至少是两个序列组成，因此要小于一半
        while(i <= (target / 2)){
            // 扩大滑动窗口的范围
            if(sum < target){
                j++;
                sum += j;
            } else if (sum > target) {
                // 缩小滑动窗口的范围
                sum -= i;
                i++;
            }else {
                // 找到了符合条件的连续子序列，保存结果，用于返回结果
                int[] temp = new int[j - i + 1];
                int index = 0;
                for (int k = i; k <= j; k++) {
                    temp[index++] = k;
                }
                result.add(temp);
                sum -= i;
                i++;
                j++;
                sum += j;
            }
        }
        return result.toArray(new int[result.size()][]);
    }

    // https://leetcode.cn/problems/fan-zhuan-dan-ci-shun-xu-lcof/
    // 剑指 Offer 58 - I. 翻转单词顺序
    // 从后往前处理单词
    public String reverseWords(String s) {
        if(s == null || s.length() <= 0){
            return s;
        }
        s = s.trim();
        StringBuilder stringBuilder = new StringBuilder();
        int i = s.length() - 1;
        int j = i;
        while(i >= 0){
            // 找到空格
            while(i >= 0 && s.charAt(i) != ' ')
                i--;
            // 复制i+1到j区间之间的字符串到stringBuilder中去
            // substring截取的范围是左闭右开
            stringBuilder.append(s.substring(i + 1,j + 1) + " ");
            // 处理单词之间又多个空格的情况，即找到下一个单词的结尾处
            while(i >= 0 && s.charAt(i) == ' ')
                i--;
            j = i;
        }
        return stringBuilder.toString().trim();
    }

    // https://leetcode.cn/problems/zuo-xuan-zhuan-zi-fu-chuan-lcof/
    // 剑指 Offer 58 - II. 左旋转字符串
    public String reverseLeftWords(String s, int n) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = n; i < s.length(); i++) {
            stringBuilder.append(s.charAt(i));
        }
        for (int i = 0; i < n; i++) {
            stringBuilder.append(s.charAt(i));
        }
        return stringBuilder.toString();
    }

    // https://leetcode.cn/problems/hua-dong-chuang-kou-de-zui-da-zhi-lcof/
    // 剑指 Offer 59 - I. 滑动窗口的最大值
    public int[] maxSlidingWindow(int[] nums, int k) {
        if(nums == null || nums.length <= 1){
            return nums;
        }
        int[] result = new int[nums.length - k + 1];
        // 保存可以是最大元素的下标
        LinkedList<Integer> queue = new LinkedList<>();
        int index = 0;
        for (int i = 0; i < nums.length; i++) {
            while(!queue.isEmpty() && nums[queue.peekLast()] < nums[i])
                queue.pollLast();
            queue.add(i);
            // 判断当前队列的个数超过滑动窗口范围允许的个数最大值，如果超过要剔除不滑动窗口范围的下标位置
            if((queue.peekLast() - k) == queue.peek()){
                queue.poll();
            }
            if(i + 1 >= k){
                result[index++] = nums[queue.peek()];
            }
        }
        return result;
    }
}
