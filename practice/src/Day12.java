import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class Day12 {
    // 剑指 Offer 50. 第一个只出现一次的字符
    // https://leetcode.cn/problems/di-yi-ge-zhi-chu-xian-yi-ci-de-zi-fu-lcof/
    public char firstUniqChar(String s) {
        if(s == null || s.length() <= 0){
            return ' ';
        }
        // 当字符只出现一次就标记为true，反之出现一次以上就直接标记为false即可
        Map<Character,Boolean> map = new LinkedHashMap<>();
        for (int i = 0; i < s.length(); i++) {
            if(!map.containsKey(s.charAt(i))){
                map.put(s.charAt(i),true);
            }else {
                map.put(s.charAt(i),false);
            }
        }
        // 遍历哈希表
        for (Map.Entry<Character,Boolean> entry : map.entrySet()) {
            if(entry.getValue()){
               return entry.getKey();
            }
        }
        return ' ';
    }

    // 剑指 Offer 51. 数组中的逆序对
    // https://leetcode.cn/problems/shu-zu-zhong-de-ni-xu-dui-lcof/
    // 采用冒泡排序的方法去做，在冒泡排序中交换了几次就有几个逆序对
    // 采用归并排序
    public int reversePairs(int[] nums) {
        if(nums == null || nums.length == 0){
            return 0;
        }
        return mergerSort(nums,0,nums.length - 1);
    }

    private int mergerSort(int[] nums, int left, int right) {
        // 只有一个数字，不能构成逆序对
        if(left >= right){
            return 0;
        }

        int mid = left + (right - left) / 2;
        int x1 = mergerSort(nums,left,mid);
        int x2 = mergerSort(nums,mid + 1,right);
        int x3 = merge(nums,left,mid,mid + 1,right);

        return x1 + x2 + x3;
    }

    private int merge(int[] nums, int l1, int r1, int l2, int r2) {
        int[] tmp = new int[r2 - l1 + 1];
        int count = 0;
        int i = l1,j = l2;
        int k = 0;
        while(i <= r1 && j <= r2){
            if(nums[i] > nums[j]){
                count += (l2 - i);
                tmp[k++] = nums[j++];
            }else {
                tmp[k++] = nums[i++];
            }
        }
        while(i <= r1) {
            tmp[k++] = nums[i++];
        }
        while(j <= r2){
            tmp[k++] = nums[j++];
        }

        // 写回原数组
        k = 0;
        for (int l = l1; l <= r2; l++) {
            nums[l] = tmp[k++];
        }

        return count;
    }

    // 剑指 Offer 52. 两个链表的第一个公共节点
    // https://leetcode.cn/problems/liang-ge-lian-biao-de-di-yi-ge-gong-gong-jie-dian-lcof/
    // 做法1哈希法
    public static class ListNode {
        int val;
        ListNode next;
        ListNode(int x) {
            val = x;
            next = null;
        }
    }
    public ListNode getIntersectionNode1(ListNode headA, ListNode headB) {
        Set<ListNode> visited = new HashSet<ListNode>();
        ListNode temp = headA;
        while (temp != null) {
            visited.add(temp);
            temp = temp.next;
        }
        temp = headB;
        while (temp != null) {
            if (visited.contains(temp)) {
                return temp;
            }
            temp = temp.next;
        }
        return null;
    }

    // 做法2统计二者的长度，长的链表先走多出的部分，然后二者一起走，当指向同一个节点就是公共节点，反之为null就没有有
    // 做法3 双指针法
    ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        if(headA == null || headB == null){
            return null;
        }
        ListNode A = headA;
        ListNode B = headB;

        // 循环寻找 如果A不为null说明就找到了
        while(A != B){
            A = A == null ? headB : A.next;
            B = B == null ? headA : B.next;
        }
        return A;
    }

    // 剑指 Offer 53 - I. 在排序数组中查找数字 I
    // https://leetcode.cn/problems/zai-pai-xu-shu-zu-zhong-cha-zhao-shu-zi-lcof/
    // 二分查找模板的变形模板：
    // 1.寻找一个大于等于target的数的下标     2.寻找第一个等于target的数的下标
    // 3.寻找最后一个大于等于target的数的下标  4.寻找最后一个等于target的数的下标
    // 上面的模板都是二分查找的变形方法
    public int search(int[] nums, int target) {
        int left = binarySearchLeft(nums,target);
        int right = binarySearchRight(nums,target);
        if(left < 0 || right < 0)
            return 0;
        return  right - left + 1;
    }

    // 2.寻找第一个等于target的数的下标算法实现
    public int binarySearchLeft(int[] nums,double target){
       if(nums == null || nums.length <= 0){
           return -1;
       }
       int left = 0;
       int right = nums.length - 1;
       while(left < right){
           int mid = left + (right - left) / 2;
           if(nums[mid] >= target){
               right = mid;
           }else {
                left = mid + 1;
           }
       }
       if(nums[left] != target)
           return -1;
       return left;
    }

    // 4.寻找最后一个等于target的数的下标算法实现
    public int binarySearchRight(int[] nums,double target){
        if(nums == null || nums.length <= 0){
            return -1;
        }
        int left = 0;
        int right = nums.length - 1;
        while(left < right){
            int mid = left + (right - left) / 2;
            if(nums[mid] <= target){
                left = mid;
            }else {
                right  = mid - 1;
            }
        }
        if(nums[left] != target)
            return -1;
        return left;
    }

    // 剑指 Offer 53 - II. 0～n-1中缺失的数字
    // https://leetcode.cn/problems/que-shi-de-shu-zi-lcof/
    public int missingNumber(int[] nums) {
        // 二分搜索
        int l = 0;
        int r = nums.length - 1;
        while(l < r){
            int mid = (r - l) / 2 + l;
            if(nums[mid] == mid){
                l = mid + 1;
            }else {
                r = mid;
            }
        }
        // 如果是一个完整数组，那缺席的就是n,返回l + 1，反之就返回缺席的那个数字
        return nums[l] == l ? l + 1 : l;
    }
}
