import java.util.Stack;

public class Day2 {
    //剑指 Offer 09. 用两个栈实现队列
    // https://leetcode.cn/problems/yong-liang-ge-zhan-shi-xian-dui-lie-lcof/
    Stack<Integer> stack1;
    Stack<Integer> stack2;
    public Day2() {
        // 入
        stack1 = new Stack<>();
        // 出
        stack2 = new Stack<>();
    }

    public void appendTail(int value) {
        stack1.push(value);
    }

    public int deleteHead() {
       if(!stack2.isEmpty()) {
           return stack2.pop();
       }
       if(!stack1.isEmpty()){
            while(!stack1.isEmpty()){
                stack2.push(stack1.pop());
            }
            return stack2.pop();
       }
        return -1;
    }

    // 剑指 Offer 10- I. 斐波那契数列
    // https://leetcode.cn/problems/fei-bo-na-qi-shu-lie-lcof/
    // 解答1
    int[] array;
    public int fib1(int n) {
        array = new int[n + 1];
        for (int i = 0; i < n + 1; i++) {
            array[i] = -1;
        }
        return f1(n);
    }

    private int f1(int n) {
        if(n <= 1){
            return n;
        }
        // 计算过的不需再次计算
        if(array[n] != -1){
            return array[n];
        }

        // 保存已经计算过的值
        int sum = (f1(n - 1) + f1(n - 2)) % 1000000007;
        array[n] = sum;
        return sum;
    }

    // 剑指 Offer 10- I. 斐波那契数列
    // https://leetcode.cn/problems/fei-bo-na-qi-shu-lie-lcof/
    // 解答2

    public int fib(int n) {
        int a = 0;
        int b = 1;
        int c = -1;
        if(n < 2){
            return n;
        }
        int k = 2;
        while(k <= n){
            c = (a + b) % 1000000007;
            a = b;
            b = c;
            k++;
        }
        return b;
    }

    // 剑指 Offer 10- II. 青蛙跳台阶问题
    // https://leetcode.cn/problems/qing-wa-tiao-tai-jie-wen-ti-lcof/
    public int numWays(int n) {
        // 0号台阶一种跳法
        if(n < 2){
            return 1;
        }
        int a = 1;
        int b = 1;
        int c = -1;
        int k = 2;
        while(k <= n){
            c = (a + b) % 1000000007;
            a = b;
            b = c;
            k++;
        }
        return b;
    }

    // 剑指 Offer 11. 旋转数组的最小数字
    // https://leetcode.cn/problems/xuan-zhuan-shu-zu-de-zui-xiao-shu-zi-lcof/
    public int minArray(int[] numbers) {
        int l = 0;
        int r = numbers.length - 1;
        while(l < r){
            if(numbers[l] < numbers[r]){
                return numbers[l];
            }
            //int mid = (l + r) /2;
            int mid = l + (r - l) / 2;
            if(numbers[mid] > numbers[l]){
                l = mid + 1;
            }else if(numbers[mid] < numbers[l]){
                r = mid;
            }else {
                l++;
            }
        }
        return numbers[l];
    }

    // 剑指 Offer 12. 矩阵中的路径
    // https://leetcode.cn/problems/ju-zhen-zhong-de-lu-jing-lcof/
    int n;
    int m;
    int len;
    boolean [][] visited;
    public boolean exist(char[][] board, String word) {
        this.n = board.length;
        this.m = board[0].length;
        this.len = word.length();
        visited = new boolean[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if(dfs(board,i,j,word,0))
                    return true;
            }
        }
        return false;
    }

    private boolean dfs(char[][] board, int i, int j, String word,int k) {
        // 坐标越界
        if(i < 0 || i > n || j < 0 || j > m){
            return false;
        }
        // 该位置被访问过或者当前位位置和单词中进行匹配的字符不匹配
        if(visited[i][j] || board[i][j] != word.charAt(k)){
            return false;
        }

        if(k == len - 1){
            return true;
        }

        visited[i][j] = true;
        boolean res = (dfs(board,i + 1,j,word,k + 1) ||
                dfs(board,i - 1,j,word,k + 1) ||
                dfs(board,i,j + 1,word,k + 1) ||
                dfs(board,i,j - 1,word,k + 1));
        visited[i][j] = false;
        return res;
    }
}
