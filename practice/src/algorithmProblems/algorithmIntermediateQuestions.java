package algorithmProblems;

import java.util.*;

public class algorithmIntermediateQuestions {

    // 给定一个有序数组arr，从左到右依次表示X轴上从左往右点的位置
    // 给定一个正整数K，返回如果有一根长度为K的绳子，最多能盖住几个点
    // 绳子的边缘点碰到X轴上的点，也算盖住
    public static int maxPoint1(int[] arr, int L) {
        int res = 1;
        for (int i = 0; i < arr.length; i++) {
            int nearest = nearestIndex(arr, i, arr[i] - L);
            res = Math.max(res, i - nearest + 1);
        }
        return res;
    }

    public static int nearestIndex(int[] arr, int R, int value) {
        int L = 0;
        int index = R;
        while (L <= R) {
            int mid = L + ((R - L) >> 1);
            if (arr[mid] >= value) {
                index = mid;
                R = mid - 1;
            } else {
                L = mid + 1;
            }
        }
        return index;
    }

    public static int maxPoint2(int[] arr, int L) {
        int left = 0;
        int right = 0;
        int N = arr.length;
        int max = 0;
        while (left < N) {
            while (right < N && arr[right] - arr[left] <= L) {
                right++;
            }
            max = Math.max(max, right - left);
            left++;
        }
        return max;
    }

    // 小虎去买苹果，商店只提供两种类型的塑料袋，每种类型都有任意数量。
    // 1）能装下6个苹果的袋子
    // 2）能装下8个苹果的袋子
    // 小虎可以自由使用两种袋子来装苹果，但是小虎有强迫症，他要求自己使用的袋子数量必须最少，且使用的每个袋子必须装满。
    // 给定一个正整数N，返回至少使用多少袋子。如果N无法让使用的每个袋子必须装满，返回-1
    public static int minBags(int apple) {
        if (apple < 0) {
            return -1;
        }

        if ((apple & 1) != 0) { // 如果是奇数，返回-1
            return -1;
        }

        int bag8 = (apple >> 3);
        int rest = apple - (bag8 << 3);
        while (bag8 >= 0 && rest < 24) {
            // rest 个
            if (rest % 6 == 0) {
                return bag8 + (rest / 6);
            } else {
                bag8--;
                rest += 8;
            }
        }
        return -1;
    }

    public static int minBagAwesome(int apple) {
        if ((apple & 1) != 0) { // 如果是奇数，返回-1
            return -1;
        }
        if (apple < 18) {
            return apple == 0 ? 0 : (apple == 6 || apple == 8) ? 1
                    : (apple == 12 || apple == 14 || apple == 16) ? 2 : -1;
        }
        return (apple - 18) / 8 + 3;
    }

    // 给定一个正整数N，表示有N份青草统一堆放在仓库里有一只牛和一只羊，牛先吃，羊后吃，它俩轮流吃草不管是牛还是羊，
    // 每一轮能吃的草量必须是：1，4，16，64…(4的某次方)
    // 谁最先把草吃完，谁获胜假设牛和羊都绝顶聪明，都想赢，都会做出理性的决定根据唯一的参数N，返回谁会赢
    // 如果n份草，最终先手赢，返回"先手"
    // 如果n份草，最终后手赢，返回"后手"
    public static String winner1(int n) {
        //  0    1   2   3   4
        // 后手 先手 后手 先手 先手
        if (n < 5) {
            return (n == 0 || n == 2) ? "后手" : "先手";
        }
        int base = 1;
        while (base <= n) {
            if (winner1(n - base).equals("后手")) {
                return "先手";
            }
            if (base > n / 4) { // 防止base*4之后溢出
                break;
            }
            base *= 4;
        }
        return "后手";
    }

    public static String winner2(int n) {
        if (n % 5 == 0 || n % 5 == 2) {
            return "后手";
        } else {
            return "先手";
        }
    }

    // 牛牛有一些排成一行的正方形。每个正方形已经被染成红色或者绿色。牛牛现在可以选择任意一个正方形然后用这两种颜色的任意一种进行染色,
    // 这个正方形的颜色将会被覆盖。牛牛的目标是在完成染色之后,每个红色R都比每个绿色G距离最左侧近。牛牛想知道他最少需要涂染几个正方形。
    // 如样例所示: s = RGRGR
    // 我们涂染之后变成RRRGG满足要求了,涂染的个数为2,没有比这个更好的涂染方案。
    public static int minPaint(String s) {
        if (s == null || s.length() < 2) {
            return 0;
        }
        char[] chs = s.toCharArray();
        int[] right = new int[chs.length];
        // 统计0到i范围有多少个R 例如right[0]就是0~0有几个R
        right[chs.length - 1] = chs[chs.length - 1] == 'R' ? 1 : 0;
        for (int i = chs.length - 2; i >= 0; i--) {
            right[i] = right[i + 1] + chs[i] == 'R' ? 1 : 0;
        }

        int res = right[0];
        int left = 0;
        for (int i = 0; i < chs.length - 1; i++) {
            left += chs[i] == 'G' ? 1 : 0;
            res = Math.min(res, left + right[i + 1]);
        }
        res = Math.min(res, left + chs[chs.length - 1] == 'G' ? 1 : 0);
        return res;
    }

    // 给定一个N*N的矩阵matrix，只有0和1两种值，返回边框全是1的最大正方形的边长长度
    // N*N的矩阵，里面长方形的规模是N^4，里面正方形的规模是N^3
    // right数组代表right[i][j]位置都右边有多少个连续1，down数组代表down[i][j]下方有多少个连续的1
    public static void setOrderMap(int[][] m, int[][] right, int[][] down) {
        int r = m.length;
        int c = m[0].length;
        if (m[r - 1][c - 1] == 1) {
            right[r - 1][c - 1] = 1;
            down[r - 1][c - 1] = 1;
        }
        for (int i = r - 2; i != -1; i--) {
            if (m[i][c - 1] == 1) {
                right[i][c - 1] = 1;
                down[i][c - 1] = down[i + 1][c - 1] + 1;
            }
        }
        for (int i = c - 2; i != 1; i--) {
            if (m[r - 1][i] == 1) {
                right[r - 1][i] = right[i - 1][i + 1] + 1;
                down[r - 1][i] = 1;
            }
        }
        for (int i = r - 2; i != 1; i--) {
            for (int j = c - 2; j != 1; j--) {
                if (m[i][j] == 1) {
                    right[i][j] = right[i][j + 1] + 1;
                    down[i][j] = down[i + 1][j] + 1;
                }
            }
        }
    }

    public static int getMaxSize(int[][] m) {
        int[][] right = new int[m.length][m[0].length];
        int[][] down = new int[m.length][m[0].length];
        setOrderMap(m, right, down);
        for (int length = Math.min(m.length, m[0].length); length >= 1; length--) {
            //对于每个边长，看是否存在以该值作为边长的矩阵，满足四周全为1
            //因为要找最大边长，所以从大到小找
            if (hasSizeOfBorder(length, right, down)) {
                return length;
            }
        }
        return 0;
    }

    //该函数实现传入一个边长值，根据right与down矩阵，判断是否存在正方形满足四周全为1
    private static boolean hasSizeOfBorder(int length, int[][] right, int[][] down) {
        for (int row = 0; row + length - 1 <= right.length - 1; row++) {
            for (int col = 0; col + length - 1 <= right[0].length - 1; col++) {
                //row,col代表左上方的位置，要求左上方处下边最少有连续的length个1，右边最少有连续的length个1
                //[row + length - 1][col]代表左下角，要求该点右边最少有连续的length个1
                //[row][col + length - 1]代表右上角，要求该点下边最少有连续的length个1
                if (right[row][col] >= length && down[row][col] >= length
                        && right[row + length - 1][col] >= length
                        && down[row][col + length - 1] >= length) {
                    return true;
                }
            }
        }
        return false;
    }

    // 从a-b等概率出现，实现c-d等概率出现
    // 已知f函数等概率出现1-5，要求模拟使用f函数使1-7也等概率出现的g函数
    // f函数如下
    // 步骤等概率得到0~1，然后在做到0~(7-1)等概率出现，最后结果加+1就是1~7等概率出现
    // 已知a~b等概率出现，要得到c~d等概率,先得到0~1等概率，然后再得到0~(d-c)(看需要几个二进制位可以可以包括d-c,如果出现的数字比d-c大，就重做，直到小于d-c)
    // 然后结果再加c，就得到c~d等概率出现
    public static int f() {
        return (int) (Math.random() * 5) + 1;
    }

    // 使用f函数等概率返回0-1
    public static int f2() {
        int ans = 0;
        do {
            ans = f();
        } while (ans == 3);
        return ans < 3 ? 0 : 1;
    }

    // 得到000~111等概率  0~7等概率返回
    public static int f3() {
        return f2() << 2 + f2() << 1 + f2();
    }

    // 实现0~6等概率返回
    public static int f4() {
        int ans = 0;
        do {
            ans = f3();
        } while (ans == 7);
        return ans;
    }

    // 实现1~7等概率返回
    public static int g() {
        return f4() + 1;
    }

    //从01不等概率出现随机到01等概率出现
    // 已知函数
    public static int x() {
        return Math.random() < 0.84 ? 0 : 1;
    }

    // 转换为等概率函数 00和11重做 01返回0，10返回1,实现等概率
    // 01出现的概率是p(1-p) 10出现的概率是(1-p)*p 可以看到二者是等概率的
    public static int y() {
        int ans = 0;
        do {
            ans = x();
        } while (ans == x());
        return ans;
    }

    // 给定一个非负整数，代表二叉树的节点个数，返回能形成多少种不同的二叉树结构
    public static int numsOfTrees(int n) {
        if (n < 0) {
            return 0;
        }
        if (n == 0) {
            return 1;
        }
        if (n == 1) {
            return 1;
        }
        if (n == 2) {
            return 2;
        }
        int ans = 0;
        for (int leftNum = 0; leftNum < n; leftNum++) {
            int leftWays = numsOfTrees(leftNum);
            int rightWays = numsOfTrees(n - leftNum - 1);
            ans = leftWays * rightWays;
        }
        return ans;
    }

    public static int numsOfTreesDP(int n) {
        if (n < 0) {
            return 0;
        }
        if (n < 2) {
            return 1;
        }
        // i个节点可能组成的树
        int[] dp = new int[n + 1];
        dp[0] = 1;
        for (int i = 1; i <= n; i++) {// 当前一共有i个节点
            for (int j = 0; j <= i - 1; j++) {// 左侧的节点个为j 右侧的节点个数为i - j - 1
                dp[i] += dp[j] + dp[i - j - 1];
            }
        }
        return dp[n];
    }

    // 一个完整的括号字符串定义规则如下:
    // 1、空字符串是完整的。
    // 2、如果s是完整的字符串，那么(s)也是完整的。
    // 3、如果s和t是完整的字符串，将它们连接起来形成的st也是完整的。
    // 例如，"(()())", ""和"(())()"是完整的括号字符串，"())(", "()(" 和 ")"是不完整的括号字符串。
    // 牛牛有一个括号字符串s,现在需要在其中任意位置尽量少地添加括号,将其转化为一个完整的括号字符串。请问牛牛至少需要添加多少个括号。
    public static int needParentheses(String str) {
        int count = 0;
        int ans = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == '(') {
                count++;
            } else {
                if (count == 0) {
                    ans++;
                } else {
                    count--;
                }
            }
        }
        return count + ans;
    }


    // 给定一个数组arr，求差值为k的去重数字队
    public static List<List<Integer>> allPair(int[] arr, int k) {
        HashSet<Integer> set = new HashSet<>();
        for (int num : arr) {
            set.add(num);
        }
        List<List<Integer>> ans = new ArrayList<>();
        for (Integer num : set) {
            if (set.contains(num + k)) {
//                List<Integer> list = new ArrayList<>();
//                list.add(num,num + k);
//                ans.add(list);
                ans.add(Arrays.asList(num, num + k));
            }
        }
        return ans;
    }

    // 给一个包含n个整数元素的集合a，一个包含m个整数元素的集合b。
    // 定义magic操作为，从一个集合中取出元素放到另一个集合里，且过后每个集合的平均值都大于操作前。
    // 注意以下两点：
    // ①不可以把一个集合的元素取空，这样就没有平均值了
    // ②值为x的元素从集合b取出放入集合a，若集合a中已经有值为x的元素，
    // 则a的平均值不变（因为集合元素不会重复），b的平均值可能会改变（因为x被取出了）
    // 问最多可以进行少次 magic操作
    public static int maxMagic(int[] a, int[] b) {
        double sum1 = 0.0;
        for (int num : a) {
            sum1 += num;
        }
        double sum2 = 0.0;
        for (int num : b) {
            sum2 += num;
        }
        // 平均值相等
        if (avg(sum1, a.length) == avg(sum2, b.length)) {
            return 0;
        }
        // 平均值不相等
        int[] arrMore = null;
        int[] arrLess = null;
        double sumMore = 0.0;
        double sumLess = 0.0;
        if (avg(sum1, a.length) > avg(sum2, b.length)) {
            arrMore = a;
            sumMore = sum1;
            arrLess = b;
            sumLess = sum2;
        } else {
            arrMore = b;
            sumMore = sum2;
            arrLess = a;
            sumLess = sum1;
        }
        Arrays.sort(arrMore);

        // 保证在平均值较小的集合中不会重复出现
        HashSet<Integer> setLess = new HashSet<>();
        for (int less : arrLess) {
            setLess.add(less);
        }

        int moreSize = arrMore.length;
        int lessSize = arrLess.length;
        int ans = 0;
        for (int i = 0; i < arrMore.length; i++) {
            double cur = arrMore[i];
            // 判断当前元素是否小于平均值较大集合的平均值
            // 并且大于平均值较小集合的平均值
            // 并且在平均值较小集合中没出现
            if (cur < avg(sumMore, moreSize) && cur > avg(sumLess, lessSize)
                    && !setLess.contains(arrMore[i])) {
                sumMore -= cur;
                moreSize--;
                sumLess += cur;
                lessSize++;
                setLess.add(arrMore[i]);
                ans++;
            }
        }
        return ans;
    }

    private static double avg(double sum, int length) {
        return sum / length;
    }

    // 一个合法的括号匹配序列有以下定义:
    //1、空串""是一个合法的括号匹配序列
    //2、如果"X"和"Y"都是合法的括号匹配序列,"XY"也是一个合法的括号匹配序列
    //3、如果"X"是一个合法的括号匹配序列,那么"(X)"也是一个合法的括号匹配序列
    //4、每个合法的括号序列都可以由以上规则生成。
    //例如: "","()","()()","((()))"都是合法的括号序列
    //对于一个合法的括号序列我们又有以下定义它的深度:
    //1、空串""的深度是0
    //2、如果字符串"X"的深度是x,字符串"Y"的深度是y,那么字符串"XY"的深度为max(x,y) 3、如果"X"的深度是x,那么字符串"(X)"的深度是x+1
    //例如: "()()()"的深度是1,"((()))"的深度是3。牛牛现在给你一个合法的括号序列,需要你计算出其深度。
    public static int getDepth(String s) {
        int depth = 0; //记录当前深度
        int maxdepth = 0; //记录最大深度
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '(') {
                depth++;
                if (depth > maxdepth) {
                    maxdepth = depth;
                }
            } else {
                depth--;
            }
        }
        return maxdepth;
    }

    // 给定一个括号序列，请找到其中最长的有效括号子串
    public static int maxLength(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        char[] str = s.toCharArray();
        int[] dp = new int[str.length];
        int pre = 0;
        int res = 0;
        for (int i = 1; i < str.length; i++) {
            if (str[i] == ')') {
                pre = i - dp[i - 1] - 1;
                if (pre >= 0 && str[pre] == ')') {
                    dp[i] = dp[i - 1] + 2 + (pre > 0 ? dp[pre - 1] : 0);
                }
            }
            res = Math.max(res, dp[i]);
        }
        return res;
    }

    // 请编写一个程序， 对一个栈里的整型数据，按升序进行排序（即排序前，栈里的数据是无序的，排序后最大元素位于栈顶）,
    // 要求最多只能使用一个额外的栈存放临时数据，但不得将元素复制到别的数据结构中
    public static void sortByStack(Stack<Integer> stack) {
        Stack<Integer> help = new Stack<>();
        while (!stack.isEmpty()) {
            int temp = stack.pop();
            while (!help.isEmpty() && temp < help.peek()) {
                stack.push(help.pop());
            }
            help.push(temp);
        }

        while (!help.isEmpty()) {
            System.out.print(help.pop() + " ");
        }
    }

    // 将给定的数转换为字符串，原则如下：1对应 a，2对应b，…..26对应z，
    // 例如12258可以转换为"abbeh", "aveh", "abyh", "lbeh" and "lyh"，个数为5，编写一个函数，给出可以转换的不同字符串的个数。
    public static int number(String str) {
        if (str == null || str.length() == 0) {
            return 0;
        }
        return process(str.toCharArray(), 0);
    }

    // str[0..i-1]转化无需过问
    // str[i.....]去转化，返回有多少种转化方法
    public static int process(char[] str, int i) {
        if (i == str.length) {
            return 1;
        }
        // i没到最后，说明有字符
        if (str[i] == '0') { // 之前的决定有问题
            return 0;
        }
        // str[i] != '0'
        // 可能性一，i单转
        int ways = process(str, i + 1);
        if (i + 1 < str.length && (str[i] - '0') * 10 + str[i + 1] - '0' < 27) {
            ways += process(str, i + 2);
        }
        return ways;
    }

    // 从右往左的动态规划
    // 就是上面方法的动态规划版本
    // dp[i]表示：str[i...]有多少种转化方式
    public static int dp1(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        char[] str = s.toCharArray();
        int N = str.length;
        int[] dp = new int[N + 1];
        dp[N] = 1;
        for (int i = N - 1; i >= 0; i--) {
            if (str[i] != '0') {
                int ways = dp[i + 1];
                if (i + 1 < str.length && (str[i] - '0') * 10 + str[i + 1] - '0' < 27) {
                    ways += dp[i + 2];
                }
                dp[i] = ways;
            }
        }
        return dp[0];
    }

    // 二叉树每个结点都有一个int型权值，给定一棵二叉树，要求计算出从根结点到
    // 叶结点的所有路径中，权值和最大的值为多少。
    public static class Node {
        public int value;
        public Node left;
        public Node right;

        public Node(int value) {
            this.value = value;
        }
    }

    public static int maxSum = Integer.MAX_VALUE;

    public static int maxPath(Node head) {
        process(head, 0);
        return maxSum;
    }

    // pre代表走到当前节点上面所有节点权值之和
    public static void process(Node cur, int pre) {
        // 来到了叶子节点
        if (cur.left == null && cur.right == null) {
            maxSum = Math.max(maxSum, pre + cur.value);
        }
        if (cur.left != null) {
            process(cur.left, pre + cur.value);
        }
        if (cur.right != null) {
            process(cur.right, pre + cur.value);
        }
    }

    // 给定元素为非负整数的二维数组matrix，每行和每列都是从小到大有序的，在给定一个非负整数aim，判断aim是否在matrix数组中
    // 从右上角开始走，利用行和列有序去改变坐标可移动的范围
    // 变形：一个数组只由0和1组成，但是要满足以下条件
    // 每一行全部的0必须在1的左边(即0和0必须紧挨，不能01交替出现，同理1也是如此)
    // 每一行可以全为0也可也全为1
    // 同理也是从右上角开始遍历，遇到1就计算器++，遇到0就往下一行走，如果下一行的该位置为1，计算器++，然后找这行左边是否还有1，没有继续下一行，直接遍历到最后一行

    // 有n个打包机器从左到右一字排开，上方有一个自动装置会抓取一批物品到每个打包机上，放到机每个器上的这些物品数量有多有少，
    // 由于物品数量不相同，需要工人将每个机器上的物品进行移动从而到达物品数量相等才能打包。每个物品重量太大，每次只能搬一个物品进行移动；
    // 为了省力，只能在相邻的机器上移动。请计算在搬动最小次数的前提下，使每个机器上的物品数量相等。如果不能使每个机器上的物品相同，返回-1。
    // 例如
    // Input: [1,0,5]
    // Output: 3
    // [1,0,5] 表示有3个机器，每个机器上分别有1、0、5个物品，
    // 经过这些移动后：
    //  1st:    1     0 <-- 5    =>    1     1     4
    //  2nd:    1 <-- 1 <-- 4    =>    2     1     3
    //  3rd:    2     1 <-- 3    =>    2     2     2
    //  移动了3次，每个机器上的物品相等。
    // 此题和https://leetcode.cn/problems/super-washing-machines/这题类似
    public static int packingMachine(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        int size = arr.length;
        int sum = 0;
        for (int num : arr) {
            sum += num;
        }
        if (sum % size != 0) {
            return -1;
        }
        int avg = sum / size;
        int leftSum = 0;
        int ans = 0;
        for (int i = 0; i < arr.length; i++) {
            int leftNeed = leftSum - i * avg;
            int rightNeed = sum - leftSum - arr[i] - (size - i - 1) * avg;
            if (leftNeed < 0 && rightNeed < 0) {
                ans = Math.max(ans, Math.abs(leftNeed) + Math.abs(rightNeed));
            } else {
                ans = Math.max(ans, Math.max(Math.abs(leftNeed), Math.abs(rightNeed)));
            }
            leftSum += arr[i];
        }
        return ans;
    }

    // 用zigzag的方式打印矩阵，比如如下的矩阵
    // 0 1  2  3
    // 4 5  6  7
    // 8 9 10 11
    // 打印顺序0 1 4 8 5 2 3 6 9 10 7 11
    public static void printMatrixZigZag(int[][] matrix) {
        int tR = 0;
        int tC = 0;
        int dR = 0;
        int dC = 0;
        int endR = matrix.length - 1;
        int endC = matrix[0].length - 1;
        boolean fromUp = false;
        while (tR != endR + 1) {
            printLevel(matrix, tR, tC, dR, dC, fromUp);
            tR = tC == endC ? tR + 1 : tR;
            tC = tC == endC ? tC : tC + 1;
            dC = dR == endR ? dC + 1 : dC;
            dR = dR == endR ? dR : dR + 1;
            fromUp = !fromUp;
        }
        System.out.println();
    }

    public static void printLevel(int[][] m, int tR, int tC, int dR, int dC, boolean f) {
        if (f) {
            while (tR != dR + 1) {
                System.out.print(m[tR++][tC--] + " ");
            }
        } else {
            while (dR != tR - 1) {
                System.out.print(m[dR--][dC++] + " ");
            }
        }
    }

    // 用螺旋的方式打印矩阵，比如如下的矩阵
    // 0 1  2  3
    // 4 5  6  7
    // 8 9 10 11
    // 打印顺序0 1 2 3 7 11 10 9 8 4 5 6
    public static void spiralOrderPrint(int[][] matrix) {
        int tR = 0;
        int tC = 0;
        int dR = matrix.length - 1;
        int dC = matrix[0].length - 1;
        while (tR <= dR && tC <= dC) {
            printEdge(matrix, tR++, tC++, dR--, dC--);
        }
    }

    // tR和tC是左上角的行和列
    // dR和dc是右下角的行和列
    public static void printEdge(int[][] m, int tR, int tC, int dR, int dC) {
        if (tR == dR) {
            for (int i = tC; i <= dC; i++) {
                System.out.print(m[tR][i] + " ");
            }
        } else if (tC == dC) {
            for (int i = tR; i <= dR; i++) {
                System.out.print(m[i][tC] + " ");
            }
        } else {
            int curC = tC;
            int curR = tR;
            while (curC != dC) {
                System.out.print(m[tR][curC] + " ");
                curC++;
            }
            while (curR != dR) {
                System.out.print(m[curR][dC] + " ");
                curR++;
            }
            while (curC != tC) {
                System.out.print(m[dR][curC] + " ");
                curC--;
            }
            while (curR != tR) {
                System.out.print(m[curR][tC] + " ");
                curR--;
            }
        }
    }

    // 给定一个正方形矩阵，只用有限几个变量，实现矩阵中每个位置的顺时针旋转90°，比如如下矩阵
    // 0  1   2   3
    // 4  5   6   7
    // 8  9   10  11
    // 12 13  14  15
    // 调整为：
    // 12 8  4 0
    // 13 9  5 1
    // 14 10 6 2
    // 25 11 7 3
    public static void rotate(int[][] matrix) {
        // 左上角点
        int a = 0;
        int b = 0;
        // 右下角点
        int c = matrix.length - 1;
        int d = matrix[0].length - 1;
        while (a < c) {
            rotateEdge(matrix, a++, b++, c--, d--);
        }
    }

    public static void rotateEdge(int[][] m, int a, int b, int c, int d) {
        int tmp = 0;
        // 第i组第一个 a,b + i
        // 第i组第二个 a + i,d
        // 第i组第三个 c,d - i
        // 第i组第四个 c - i,b
        for (int i = 0; i < d - b; i++) {
            tmp = m[a][b + i];
            // 4->1
            m[a][b + i] = m[c - i][b];
            // 3->4
            m[c - i][b] = m[c][d - i];
            // 2->3
            m[c][d - i] = m[a + i][d];
            // 1->2
            m[a + i][d] = tmp;
        }
    }

    // 定义两个字符变量：s和m，再定义两种操作，
    // 第一种操作：
    // m=s；
    // s=s+s;
    // 第二种操作：
    // =s+m;
    // 假设初始化如下：
    // s=”a”; m=s;
    // 求最小的操作步骤数，可以将s拼接到长度等于n；
    public static int minOps(int n) {
        if (n < 2) {
            return 0;
        }
        if (isPrim(n)) {
            return n - 1;
        }
        // n不是质数
        int[] ans = divsSumAndCount(n);
        return ans[0] - ans[1];
    }

    private static boolean isPrim(int n) {
        for (int i = 2; i < n; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }

    private static int[] divsSumAndCount(int n) {
        int sum = 0;
        int count = 0;
        for (int i = 2; i <= n; i++) {
            while (n % i == 0) {
                sum += i;
                count++;
                n /= i;
            }
        }
        return new int[]{sum, count};
    }

    // 给定一个字符串类型的数组arr，求其中出现次数最多的前k个
    public static class StringNode {
        public String str;
        public int times;

        public StringNode(String str, int times) {
            this.str = str;
            this.times = times;
        }
    }

    public static void printTopK(String[] arr, int topK) {
        if (arr == null || arr.length == 0 || topK < 1) {
            return;
        }
        HashMap<String, Integer> map = new HashMap<>();
        for (String s : arr) {
            if (!map.containsKey(s)) {
                map.put(s, 0);
            }
            map.put(s, map.get(s) + 1);
        }
        topK = Math.min(arr.length, topK);
        PriorityQueue<StringNode> heap = new PriorityQueue<>((o1, o2) -> o1.times - o2.times);
        for (Map.Entry<String, Integer> set : map.entrySet()) {
            StringNode cur = new StringNode(set.getKey(), set.getValue());
            if (heap.size() < topK) {
                heap.add(cur);
            } else {
                if (heap.peek().times < cur.times) {
                    heap.poll();
                    heap.add(cur);
                }
            }
        }
        while (!heap.isEmpty()) {
            StringNode cur = heap.poll();
            System.out.print(cur.str + " ");
        }
    }

    // 设计一种结构，可以接收用户给的字符串，并且用户可以随时打印该结构中topK的字符串
    public static class TopKRecord {
        // 堆
        private StringNode[] heap;
        // 堆的大小
        private int heapSize;
        // 统计词频的map
        private HashMap<String, StringNode> strNodeMap;
        // 记录字符串在对上的位置，没有为-1
        private HashMap<StringNode, Integer> nodeIndexMap;

        public TopKRecord(int topK) {
            heap = new StringNode[topK];
            heapSize = 0;
            strNodeMap = new HashMap<>();
            nodeIndexMap = new HashMap<>();
        }

        // 添加字符串到当前结构
        public void add(String str) {
            // 当前str对应的节点对象
            StringNode cur = null;
            // 当前节点对象在堆上的位置，假设不在，置为-1
            int preIndex = -1;
            if (!strNodeMap.containsKey(str)) {// str第一次出现
                cur = new StringNode(str, 1);
                strNodeMap.put(str, cur);
                nodeIndexMap.put(cur, -1);
            } else {// 不是第一次出现
                cur = strNodeMap.get(str);
                cur.times++;
                preIndex = nodeIndexMap.get(cur);
            }
            if (preIndex == -1) {// 不在堆上
                if (heapSize == heap.length) {// 堆满
                    if (heap[0].times < cur.times) {
                        nodeIndexMap.put(heap[0], -1);
                        nodeIndexMap.put(cur, 0);
                        heap[0] = cur;
                        heapify(0, heapSize);
                    }
                } else {// 堆没满
                    nodeIndexMap.put(cur, heapSize);
                    heap[heapSize] = cur;
                    heapInsert(heapSize++);
                }
            } else {// 在堆上
                heapify(preIndex, heapSize);
            }
        }

        public void printTopK() {
            for (StringNode top : heap) {
                if (top == null) {
                    break;
                }
                System.out.println(top.str + " " + top.times);
            }
        }

        private void heapInsert(int index) {
            while (index != 0) {
                int parent = (index - 1) / 2;
                if (heap[index].times < heap[parent].times) {
                    swap(heap, index, parent);
                    index = parent;
                } else {
                    break;
                }
            }
        }

        private void heapify(int index, int size) {
            int l = 2 * index + 1;
            int r = 2 * index + 2;
            int smallestIndex = index;
            while (l < size) {
                if (heap[l].times < heap[r].times) {
                    smallestIndex = l;
                }
                if (r < size && heap[r].times < heap[smallestIndex].times) {
                    smallestIndex = r;
                }
                if (smallestIndex != index) {
                    swap(heap, index, smallestIndex);
                } else {
                    break;
                }
                index = smallestIndex;
                l = 2 * index + 1;
                r = 2 * index + 2;
            }
        }

        private void swap(StringNode[] heap, int index1, int index2) {
            nodeIndexMap.put(heap[index1], index2);
            nodeIndexMap.put(heap[index2], index1);
            StringNode temp = heap[index1];
            heap[index1] = heap[index2];
            heap[index2] = temp;
        }
    }

    // 实现一种狗猫队列的结构,要求如下：
    // 用户可以调用add方法将cat类或dog类的实例放入队列中;
    // 用户可以调用pollAll方法,将队列中所有的实例按照进队列的先后顺序依次弹出;
    // 用户可以调用 pollDog方法,将队列中dog类的实例按照进队列的先后顺序依次弹出；
    // 用户可以调用 pollCat方法,将队列中cat类的实例按照进队列的先后顺序依次弹出;
    // 用户可以调用isCatEmpty方法,检查队列中是否还有dog或cat的实例；
    // 用户可以调用 isDogEmpty方法,检查队列中是否有dog类的实例；
    // 用户可以调用 isCatEmpty方法,检查队列中是否有cat类的实例；
    // 要求以上所有方法的时间复杂度为O(1)
    public static class Pet {
        private String type;

        public Pet(String type) {
            this.type = type;
        }

        public String getType() {
            return this.type;
        }
    }

    public static class Dog extends Pet {
        public Dog() {
            super("Dog");
        }
    }

    public static class Cat extends Pet {
        public Cat() {
            super("Cat");
        }
    }

    public static class MyPet {
        private Pet pet;
        private int count;

        public MyPet(Pet pet, int count) {
            this.pet = pet;
            this.count = count;
        }

        public Pet getPet() {
            return pet;
        }

        public void setPet(Pet pet) {
            this.pet = pet;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }
    }

    Queue<MyPet> catQueue = new LinkedList<>();
    Queue<MyPet> dogQueue = new LinkedList<>();
    int count = 0;

    public void add(Pet pet) {
        count++;
        if (pet.getType().equals("Cat")) {
            catQueue.add(new MyPet(pet, count));
        } else if (pet.getType().equals("Dog")) {
            dogQueue.add(new MyPet(pet, count));
        } else {
            throw new RuntimeException("invalid input");
        }
    }

    public Pet pollAll() {
        if (!catQueue.isEmpty() && !dogQueue.isEmpty()) {
            if (catQueue.peek().getCount() < dogQueue.peek().getCount()) {
                return catQueue.poll().getPet();
            } else {
                return dogQueue.poll().getPet();
            }
        } else if (!catQueue.isEmpty()) {
            return catQueue.poll().getPet();
        } else if (!dogQueue.isEmpty()) {
            return dogQueue.poll().getPet();
        } else {
            throw new RuntimeException("Queue is Empty!");
        }
    }

    public Dog pollDog() {
        if (dogQueue.isEmpty()) {
            throw new RuntimeException("Queue is Empty!");
        }
        return (Dog) dogQueue.poll().getPet();
    }

    public Cat pollCat() {
        if (dogQueue.isEmpty()) {
            throw new RuntimeException("Queue is Empty!");
        }
        return (Cat) catQueue.poll().getPet();
    }

    public boolean isEmpty() {
        return catQueue.isEmpty() && dogQueue.isEmpty();
    }

    public boolean isDogEmpty() {
        return dogQueue.isEmpty();
    }

    public boolean isCatEmpty() {
        return catQueue.isEmpty();
    }

    // 实现一个特殊的栈，在实现栈的基本功能上，再实现返回栈中最小元素的操作
    // 要求pop、push、getMin操作的时间复杂度都是O(1)
    // 设计的栈类型可以使用现成的栈结构
    public static class MyStack1 {
        private Stack<Integer> stackData;
        private Stack<Integer> stackMin;

        public MyStack1() {
            this.stackData = new Stack<>();
            this.stackMin = new Stack<>();
        }

        public void push(int newNum) {
            // 处理min栈
            if (this.stackMin.isEmpty()) {
                this.stackMin.push(newNum);
            } else if (newNum <= this.getMin()) {
                this.stackMin.push(newNum);
            }

            // 处理data栈
            this.stackData.push(newNum);
        }

        public int pop() {
            if (this.stackData.isEmpty()) {
                throw new RuntimeException("Your stack is empty.");
            }

            // 弹出data栈的栈顶元素，如果此数和min栈的栈顶相等，min栈的栈顶也弹出
            int value = this.stackData.pop();
            if (value == this.getMin()) {
                this.stackMin.pop();
            }
            return value;
        }

        public int getMin() {
            if (this.stackMin.isEmpty()) {
                throw new RuntimeException("Your stack is empty.");
            }
            // 返回min栈的栈顶元素，但不弹出
            return this.stackMin.peek();
        }
    }

    // 栈实现队列

    public static class MyQueue<E> {

        private Stack<E> stack1 = new Stack<E>();
        private Stack<E> stack2 = new Stack<E>();

        // 入队
        public void offer(E e) {
            while (!stack2.isEmpty()) {
                stack1.push(stack2.pop());
            }
            stack1.push(e);
        }

        // 出队
        public E poll() {
            while (!stack1.isEmpty()) {
                stack2.push(stack1.pop());
            }
            return stack2.pop();
        }

        // 判断队列是否为空
        public boolean isEmpty() {
            return stack1.isEmpty() && stack2.isEmpty();
        }
    }

    // 队列实现栈
    public static class MyStack<E> {
        private Queue<E> queue1 = new LinkedList<E>();
        private Queue<E> queue2 = new LinkedList<E>();

        // 入栈(入不为空的栈)
        public void push(E e) {
            if (!queue1.isEmpty()) {
                queue1.offer(e);
            } else if (queue2.isEmpty()) {
                queue2.offer(e);
            } else {
                queue1.offer(e);
            }
        }

        // 出栈(出不为空的栈)
        public E pop() {
            if (queue1.isEmpty() && queue2.isEmpty()) {
                return null;
            }
            if (!queue1.isEmpty()) {
                int size = queue1.size() - 1;
                for (int i = 0; i < size; i++) {
                    queue2.offer(queue1.poll());
                }
                return queue1.poll();
            }
            int size = queue2.size() - 1;
            for (int i = 0; i < size; i++) {
                queue1.offer(queue2.poll());
            }
            return queue2.poll();
        }

        // 判断栈是否为空
        public boolean isEmpty() {
            return queue1.isEmpty() && queue2.isEmpty();
        }
    }

    // 给定一个整形数组arr，已知其中所有的值都是非负的，将这个数组看作一个容器，请返回容器能装多少水。
    // 例如3 1 2 5 2 4，返回5
    // https://leetcode.cn/problems/volume-of-histogram-lcci/
    public static int carryWaterMax(int[] arr) {
        int allWater = 0;
        int l = 0;
        int r = arr.length - 1;
        int leftMax = 0;
        int rightMax = 0;
        while (l < r) {
            leftMax = Math.max(leftMax, arr[l]);
            rightMax = Math.max(rightMax, arr[r]);
            if (leftMax < rightMax) {// 达到左边瓶颈
                allWater += leftMax - arr[l];
                l++;
            } else {// 达到右边瓶颈
                allWater += rightMax - arr[r];
                r--;
            }
        }
        return allWater;
    }

    // 给定一个数组arr长度为N，你可以把任意长度大于0且小于N的前缀作为左部分，剩下作为右部分
    // 但是每种划分下都有左部分的最大值和右部分的最大值,请返回最大的，左部分最大值减去右部分最大值的绝对值
    public static int maxAbsLeftAndRight(int[] arr) {
        int max = Integer.MIN_VALUE;
        for (int num : arr) {
            if (num > max) {
                max = num;
            }
        }
        return arr[0] > arr[arr.length - 1] ? max - arr[arr.length - 1] : max - arr[0];
    }

    // 如果一个字符串为str，把字符串str前面任意的部分挪到后面形成的字符叫做str的旋转词。
    // 比如A=”12345”,A的旋转词有”12345”,”23451”,”34512”,”45123”和”51234”。
    // 对于两个字符串A和B，请判断A和B是否互为旋转词。
    public static boolean isRotated(String a, String b) {
        if (a.length() != b.length()) {
            return false;
        }
        String doubleStr = a + a;
        // 判断b是否是doubleStr的子串
        return isSubStr(doubleStr, b) != -1;
    }

    public static int isSubStr(String a, String b) {
        char[] aa = a.toCharArray();
        char[] bb = b.toCharArray();
        int ai = 0;
        int bi = 0;
        int[] next = nextArr(b.toCharArray());
        while (ai < aa.length && bi < bb.length) {
            if (aa[ai] == bb[bi]) {
                ai++;
                bi++;
            } else if (next[bi] == -1) {
                ai++;
            } else {
                bi = next[bi];
            }
        }
        return bi == bb.length ? ai - bi : -1;
    }

    public static int[] nextArr(char[] b) {
        if (b.length == 1) {
            return new int[]{-1};
        }
        int[] arr = new int[b.length];
        arr[0] = -1;
        arr[1] = 0;
        int index = 2;
        int subLength = 0;
        while (index < arr.length) {
            if (b[index - 1] == b[subLength]) {
                subLength++;
                arr[index] = subLength;
                index++;
            } else if (subLength > 0) {
                subLength = arr[subLength];
            } else {
                arr[index] = 0;
                index++;
            }
        }
        return arr;
    }

    // 给定一个数组arr，arr[i]代表第i号咖啡机泡一杯咖啡的时间。
    // 给定一个正数N，表示 N个人等着咖啡机泡咖啡，每台咖啡机只能轮流泡咖啡。
    // 只有一台洗杯器，一次只能洗一个杯子，时间耗费a，洗完才能洗下一杯。
    // 每个咖啡杯也可以自己挥发干净，时间耗费b，咖啡杯可以并行挥发。
    // 假设所有人拿到咖啡之后立刻喝干净，返回从开始等到所有咖啡杯变干净的最短时间。
    // 验证的方法
    // 彻底的暴力
    // 很慢但是绝对正确
    // 以下为贪心+优良暴力
    public static class Machine {
        public int timePoint;
        public int workTime;

        public Machine(int t, int w) {
            timePoint = t;
            workTime = w;
        }
    }

    public static class MachineComparator implements Comparator<Machine> {
        @Override
        public int compare(Machine o1, Machine o2) {
            return (o1.timePoint + o1.workTime) - (o2.timePoint + o2.workTime);
        }
    }

    // 优良一点的暴力尝试的方法
    public static int minTime1(int[] arr, int n, int a, int b) {
        PriorityQueue<Machine> heap = new PriorityQueue<Machine>(new MachineComparator());
        for (int i = 0; i < arr.length; i++) {
            heap.add(new Machine(0, arr[i]));
        }
        int[] drinks = new int[n];
        for (int i = 0; i < n; i++) {
            Machine cur = heap.poll();
            cur.timePoint += cur.workTime;
            drinks[i] = cur.timePoint;
            heap.add(cur);
        }
        return bestTime(drinks, a, b, 0, 0);
    }

    // drinks 所有杯子可以开始洗的时间
    // wash 单杯洗干净的时间（串行）
    // air 挥发干净的时间(并行)
    // free 洗的机器什么时候可用
    // drinks[index.....]都变干净，最早的结束时间（返回）
    public static int bestTime(int[] drinks, int wash, int air, int index, int free) {
        if (index == drinks.length) {
            return 0;
        }
        // index号杯子 决定洗
        int selfClean1 = Math.max(drinks[index], free) + wash;
        int restClean1 = bestTime(drinks, wash, air, index + 1, selfClean1);
        int p1 = Math.max(selfClean1, restClean1);

        // index号杯子 决定挥发
        int selfClean2 = drinks[index] + air;
        int restClean2 = bestTime(drinks, wash, air, index + 1, free);
        int p2 = Math.max(selfClean2, restClean2);
        return Math.min(p1, p2);
    }

    // 贪心+优良尝试改成动态规划
    public static int minTime2(int[] arr, int n, int a, int b) {
        PriorityQueue<Machine> heap = new PriorityQueue<Machine>(new MachineComparator());
        for (int i = 0; i < arr.length; i++) {
            heap.add(new Machine(0, arr[i]));
        }
        int[] drinks = new int[n];
        for (int i = 0; i < n; i++) {
            Machine cur = heap.poll();
            cur.timePoint += cur.workTime;
            drinks[i] = cur.timePoint;
            heap.add(cur);
        }
        return bestTimeDp(drinks, a, b);
    }

    public static int bestTimeDp(int[] drinks, int wash, int air) {
        int N = drinks.length;
        int maxFree = 0;
        for (int drink : drinks) {
            maxFree = Math.max(maxFree, drink) + wash;
        }
        int[][] dp = new int[N + 1][maxFree + 1];
        for (int index = N - 1; index >= 0; index--) {
            for (int free = 0; free <= maxFree; free++) {
                int selfClean1 = Math.max(drinks[index], free) + wash;
                if (selfClean1 > maxFree) {
                    break; // 因为后面的也都不用填了
                }
                // index号杯子 决定洗
                int restClean1 = dp[index + 1][selfClean1];
                int p1 = Math.max(selfClean1, restClean1);
                // index号杯子 决定挥发
                int selfClean2 = drinks[index] + air;
                int restClean2 = dp[index + 1][free];
                int p2 = Math.max(selfClean2, restClean2);
                dp[index][free] = Math.min(p1, p2);
            }
        }
        return dp[0][0];
    }

    // 给定一个数组arr，如果通过调整可以做到arr中任意两个相邻的数字相乘是4的倍数，返回true，反之返回false
    public static boolean isMultiply4(int[] arr) {
        int odd = 0;// 奇数
        int even2 = 0;// 2的倍数，只有一个2的因子
        int even4 = 0;// 4的倍数
        for (int num : arr) {
            if ((num & 1) != 0) {
                odd++;
            } else {
                if (num % 4 == 0) {
                    even4++;
                } else {
                    even2++;
                }
            }
        }
        if (even2 == 0) {
            return even4 >= (odd - 1);
        } else {
            return even4 >= odd;
        }
    }

    // 斐波那契数列套路问题
    public static int f1(int n) {
        if (n < 1) {
            return 0;
        }
        if (n == 1 || n == 2) {
            return 1;
        }
        return f1(n - 1) + f1(n - 2);
    }

    public static int f2(int n) {
        if (n < 1) {
            return 0;
        }
        if (n == 1 || n == 2) {
            return 1;
        }
        int res = 1;
        int pre = 1;
        int tmp = 0;
        for (int i = 3; i <= n; i++) {
            tmp = res;
            res = res + pre;
            pre = tmp;
        }
        return res;
    }

    // O(logN)
    public static int f3(int n) {
        if (n < 1) {
            return 0;
        }
        if (n == 1 || n == 2) {
            return 1;
        }
        // [ 1 ,1 ]
        // [ 1, 0 ]
        int[][] base = {
                {1, 1},
                {1, 0}
        };
        int[][] res = matrixPower(base, n - 2);
        return res[0][0] + res[1][0];
    }

    public static int[][] matrixPower(int[][] m, int p) {
        int[][] res = new int[m.length][m[0].length];
        for (int i = 0; i < res.length; i++) {
            res[i][i] = 1;
        }
        // res = 矩阵中的1
        int[][] t = m;// 矩阵1次方
        for (; p != 0; p >>= 1) {
            if ((p & 1) != 0) {
                res = product(res, t);
            }
            t = product(t, t);
        }
        return res;
    }

    // 两个矩阵乘完之后的结果返回
    public static int[][] product(int[][] a, int[][] b) {
        int n = a.length;
        int m = b[0].length;
        int k = a[0].length; // a的列数同时也是b的行数
        int[][] ans = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                for (int c = 0; c < k; c++) {
                    ans[i][j] += a[i][c] * b[c][j];
                }
            }
        }
        return ans;
    }

    public static int s1(int n) {
        if (n < 1) {
            return 0;
        }
        if (n == 1 || n == 2) {
            return n;
        }
        return s1(n - 1) + s1(n - 2);
    }

    public static int s2(int n) {
        if (n < 1) {
            return 0;
        }
        if (n == 1 || n == 2) {
            return n;
        }
        int res = 2;
        int pre = 1;
        int tmp = 0;
        for (int i = 3; i <= n; i++) {
            tmp = res;
            res = res + pre;
            pre = tmp;
        }
        return res;
    }

    public static int s3(int n) {
        if (n < 1) {
            return 0;
        }
        if (n == 1 || n == 2) {
            return n;
        }
        int[][] base = {{1, 1}, {1, 0}};
        int[][] res = matrixPower(base, n - 2);
        return 2 * res[0][0] + res[1][0];
    }

    public static int c1(int n) {
        if (n < 1) {
            return 0;
        }
        if (n == 1 || n == 2 || n == 3) {
            return n;
        }
        return c1(n - 1) + c1(n - 3);
    }

    public static int c2(int n) {
        if (n < 1) {
            return 0;
        }
        if (n == 1 || n == 2 || n == 3) {
            return n;
        }
        int res = 3;
        int pre = 2;
        int prepre = 1;
        int tmp1 = 0;
        int tmp2 = 0;
        for (int i = 4; i <= n; i++) {
            tmp1 = res;
            tmp2 = pre;
            res = res + prepre;
            pre = tmp1;
            prepre = tmp2;
        }
        return res;
    }

    public static int c3(int n) {
        if (n < 1) {
            return 0;
        }
        if (n == 1 || n == 2 || n == 3) {
            return n;
        }
        int[][] base = {
                {1, 1, 0},
                {0, 0, 1},
                {1, 0, 0}};
        int[][] res = matrixPower(base, n - 3);
        return 3 * res[0][0] + 2 * res[1][0] + res[2][0];
    }


    // 字符串只由’0’和’1’两种字符构成，
    // 当字符串长度为1时，所有可能的字符串为"0"、“1”；
    // 当字符串长度为2时，所有可能的字符串为"00"、“01”、“10”、“11”；
    // 当字符串长度为3时，所有可能的字符串为"000"、“001”、“010”、“011”、“100”、
    // “101”、“110”、“111”
    // 如果某一个字符串中，只要是出现’0’的位置，左边就靠着’1’，这样的字符串叫作达标字符串。
    // 给定一个正数N，返回所有长度为N的字符串中，达标字符串的数量。
    // 比如，N=3，返回3，因为只有"101"、“110”、"111"达标。
    public static int getNum1(int n) {
        if (n < 1) {
            return 0;
        }
        return process(1, n);
    }

    public static int process(int i, int n) {
        if (i == n - 1) {
            return 2;
        }
        if (i == n) {
            return 1;
        }
        return process(i + 1, n) + process(i + 2, n);
    }

    public static int getNum2(int n) {
        if (n < 1) {
            return 0;
        }
        if (n == 1) {
            return 1;
        }
        int pre = 1;
        int cur = 1;
        int tmp = 0;
        for (int i = 2; i < n + 1; i++) {
            tmp = cur;
            cur += pre;
            pre = tmp;
        }
        return cur;
    }

    public static int getNum3(int n) {
        if (n < 1) {
            return 0;
        }
        if (n == 1 || n == 2) {
            return n;
        }
        int[][] base = {{1, 1}, {1, 0}};
        int[][] res = matrixPower(base, n - 2);
        return 2 * res[0][0] + res[1][0];
    }

    // 在迷迷糊糊的大草原上，小红捡到了n根木棍，第i根木棍的长度为i，小红现在很开心。
    // 想选出其中的三根木棍组成美丽的三角形。但是小明想捉弄小红，想去掉一些木棍，使得小红任意选三根木棍都不能组成三角形。
    // 请问小明最少去掉多少根木棍呢？
    // 给定N，返回至少去掉多少根？
    public static int removeStick(int[] arr) {
        int firstMax = 3;
        int secondMax = 2;
        int count = 3;
        for (int i = 4; i < arr.length; i++) {
            if (firstMax + secondMax <= arr[i]) {
                count++;
                secondMax = firstMax;
                firstMax = arr[i];
            }
        }
        return count;
    }

    // 牛牛准备参加学校组织的春游, 出发前牛牛准备往背包里装入一些零食, 牛牛的背包容量为w。
    // 牛牛家里一共有n袋零食, 第i袋零食体积为v[i]。
    // 牛牛想知道在总体积不超过背包容量的情况下,他一共有多少种零食放法(总体积为0也算一种放法)。
    public static long result = 1;

    public static void ways(long[] v, int w) {
        int n = v.length;
        long sum = 0;
        for (long l : v) {
            sum = sum + l;
        }
        if (sum <= w) {
            System.out.println((int) Math.pow(2, n));
        } else {
            state(v, 0, w, 0);
            System.out.println(result);
        }
    }

    public static void state(long[] v, int index, long w, long current) {
        // 没有东西可以放
        if (index == v.length)
            return;
        // 选择放入当前东西
        if (current + v[index] <= w) {
            result++;
            current = current + v[index];
            state(v, index + 1, w, current);
        }
        // 不放当前东西
        state(v, index + 1, w, current);
    }

    // 为了找到自己满意的工作，牛牛收集了每种工作的难度和报酬。牛牛选工作的标准是在难度不超过自身能力值的情况下，牛牛选择报酬最高的工作。
    // 在牛牛选定了自己的工作后，牛牛的小伙伴们来找牛牛帮忙选工作，牛牛依然使用自己的标准来帮助小伙伴们。请帮他们也选择
    public static class Job {
        public int money;// 薪资
        public int hardLevel;// 难度

        public Job(int money, int hardLevel) {
            this.money = money;
            this.hardLevel = hardLevel;
        }
    }

    // 给定一个Job类型的数组jobArr，表示所有的工作，，给定一个int类型的数组arr，表示小伙伴们的能力。
    // 返回int类型的数组，表示每一个小伙伴按照牛牛的标准所选择的工作所能获得的报酬
    public static class JobComparator implements Comparator<Job> {
        @Override
        public int compare(Job o1, Job o2) {
            return o1.hardLevel != o2.hardLevel ? o1.hardLevel - o2.hardLevel : o1.money - o2.money;
        }
    }

    public static int[] getMoneys(Job[] jobs, int[] abilities) {
        Arrays.sort(jobs, new JobComparator());
        TreeMap<Integer, Integer> map = new TreeMap<>();
        map.put(jobs[0].hardLevel, jobs[0].money);
        Job pre = jobs[0];
        for (int i = 1; i < jobs.length; i++) {
            if (jobs[i].hardLevel != pre.hardLevel && jobs[i].money > pre.money) {
                pre = jobs[i];
                map.put(pre.hardLevel, pre.money);
            }
        }
        int[] ans = new int[abilities.length];
        for (int i = 0; i < abilities.length; i++) {
            Integer key = map.floorKey(abilities[i]);
            ans[i] = (key != null ? map.get(key) : 0);
        }
        return ans;
    }

    // 给定一个字符串，如果该字符串符合人们日常书写一个整数的形式，返回int类型这个数字，如果不符合就返回-1或报错
    // 不符合的条件
    // 1.除了数字字符之外只能出现-号
    // 2.如果出现了-号，只能出现一次，并且后面必须跟着数字字符，并且该数字字符不能为0
    // 3.如果开头为0，后续不能有数字
    public static long convert(String str) {
        if (str == null || str.length() == 0) {
            return 0;
        }
        char[] s = str.toCharArray();
        if (!isValid(s)) {
            throw new RuntimeException("can not convert");
        } else {
            boolean negative = s[0] == '-';
            //return Long.parseLong(str);
            final int minQ = Integer.MIN_VALUE / 10;
            final int minR = Integer.MIN_VALUE % 10;
            int res = 0;
            int cur = 0;
            for (int i = negative ? 1 : 0; i < s.length; i++) {
                // 用负数表示是因为int类型，负数要比正数的范围多一个数字
                cur = '0' - s[i];
                // 溢出情况 向下溢出
                if (res < minQ || (res == minQ && cur < minR)) {
                    throw new RuntimeException("can not convert");
                }
                res = res * 10 + cur;
            }
            if (!negative && res == Integer.MIN_VALUE) {
                throw new RuntimeException("can not convert");
            }
            return negative ? res : -res;
        }
    }

    public static boolean isValid(char[] str) {
        if (str[0] != '-' && (str[0] < '0' || str[0] > '9')) {
            return false;
        }
        if (str[0] == '-' && (str.length == 1 || str[1] == '0')) {
            return false;
        }
        if (str[0] == '0' && str.length > 1) {
            return false;
        }
        for (int i = 1; i < str.length; i++) {
            if (str[i] < '0' || str[i] > '9') {
                return false;
            }
            if (str[i] == '-') {
                return false;
            }
        }
        return true;
    }

    // 给你一个字符串类型的数组arr，譬如:
    //  String[] arr = { "b\st", "d\", "a\d\e", "a\b\c" };
    //  把这些路径中蕴含的目录结构给打印出来，子目录直接列在父目录下面，并比父目录向右进两格，就像这样:
    //  a
    //    b
    //      c
    //    d
    //      e
    //  b
    //    cst
    //  d
    //  同一级的需要按字母顺序排列不能乱。  前缀树解决
    public static class FolderNode {
        public String name;
        public TreeMap<String, FolderNode> nexts;

        public FolderNode(String name) {
            this.name = name;
            nexts = new TreeMap<>();
        }
    }

    public static void print(String[] paths) {
        if (paths == null || paths.length == 0) {
            return;
        }
        // 构造前缀树
        FolderNode head = generateFolderTree(paths);
        // 打印前缀树
        printProcess(head, 0);
    }

    private static FolderNode generateFolderTree(String[] paths) {
        FolderNode head = new FolderNode("");
        for (String path : paths) {
            String[] split = path.split("\\\\");
            FolderNode cur = head;
            for (String s : split) {
                if (!cur.nexts.containsKey(s)) {
                    cur.nexts.put(s, new FolderNode(s));
                }
                cur = cur.nexts.get(s);
            }
        }
        return head;
    }

    private static void printProcess(FolderNode head, int level) {
        if (level != 0) {
            System.out.println(get2nSpace(level) + head.name);
        }
        for (FolderNode folderNode : head.nexts.values()) {
            printProcess(folderNode, level + 1);
        }
    }

    private static String get2nSpace(int level) {
        StringBuilder ans = new StringBuilder();
        for (int i = 1; i < level; i++) {
            ans.append("  ");
        }
        return ans.toString();
    }

    // 双向链表节点结构和二叉树节点结构是一样的，如果你把last认为是left，next认为是right的话。
    // 给定一个搜索二叉树的头节点head，请转化为一条有序的双向链表，并返回链表的头节点。
    public static class BSTNode {
        public int value;
        public BSTNode left;
        public BSTNode right;

        public BSTNode(int value) {
            this.value = value;
        }
    }

    public static BSTNode SBTConvertToOrderedList1(BSTNode head) {
        Queue<BSTNode> queue = new LinkedList<>();
        inOrderToQueue(head, queue);
        if (queue.isEmpty()) {
            return head;
        }
        head = queue.poll();
        BSTNode pre = head;
        pre.left = null;
        BSTNode cur = null;
        while (!queue.isEmpty()) {
            cur = queue.poll();
            pre.right = cur;
            cur.left = pre;
            pre = cur;
        }
        pre.right = null;
        return head;
    }

    private static void inOrderToQueue(BSTNode head, Queue<BSTNode> queue) {
        if (head == null) {
            return;
        }
        inOrderToQueue(head.left, queue);
        queue.add(head);
        inOrderToQueue(head.right, queue);
    }

    // 树形套路解决
    public static class Info {
        public BSTNode head;
        public BSTNode tail;

        public Info(BSTNode start, BSTNode end) {
            this.head = start;
            this.tail = end;
        }
    }

    public static BSTNode SBTConvertToOrderedList2(BSTNode head) {
        if (head == null) {
            return head;
        }
        return process(head).head;
    }

    // 返回x为头的整棵二叉搜索树，有序的方式连接好，并且返回整个双向链表的头节点和尾节点
    public static Info process(BSTNode x) {
        if (x == null) {
            return new Info(null, null);
        }
        Info leftHeadTail = process(x.left);
        Info rightHeadTail = process(x.right);
        if (leftHeadTail.tail != null) {
            leftHeadTail.tail.right = x;
        }
        x.left = leftHeadTail.tail;
        if (rightHeadTail.head != null) {
            rightHeadTail.head.left = x;
        }
        x.right = rightHeadTail.head;
        return new Info(leftHeadTail.head != null ? leftHeadTail.head : x,
                rightHeadTail.tail != null ? rightHeadTail.tail : x);
    }

    // 树形套路解决
    // 找到一棵二叉树中，最大的搜索二叉树，返回最大二叉搜索子树的节点个数
    public static class NeedInfo {
        public BSTNode maxBSTHead;
        public boolean isBST;
        public int max;
        public int min;
        public int maxBSTSize;

        public NeedInfo(BSTNode maxBSTHead, boolean isBST, int max, int min, int maxSize) {
            this.maxBSTHead = maxBSTHead;
            this.isBST = isBST;
            this.max = max;
            this.min = min;
            this.maxBSTSize = maxSize;
        }
    }

    public static NeedInfo f(BSTNode x) {
        if (x == null) {
            //return new NeedInfo(null,true,Integer.MAX_VALUE,
            //        Integer.MIN_VALUE,0);
            return null;
        }
        // 假设得到这些信息
        NeedInfo leftIfo = f(x.left);
        NeedInfo rightInfo = f(x.right);

        int max = x.value;
        int min = x.value;
        // 加工出这个五个信息
        if (leftIfo != null) {
            max = Math.max(max, leftIfo.max);
            min = Math.min(min, leftIfo.min);
        }

        if (rightInfo != null) {
            max = Math.max(max, rightInfo.max);
            min = Math.min(min, rightInfo.min);
        }

        int maxBSTSize = 0;
        BSTNode maxBSTHead = null;
        // 当前x不是二叉搜索子树的节点，假设左子树是二叉搜索树
        if (leftIfo != null) {
            maxBSTSize = leftIfo.maxBSTSize;
            maxBSTHead = leftIfo.maxBSTHead;
        }
        // 当前x不是二叉搜索子树的节点，假设右子树是二叉搜索树
        if (rightInfo != null && rightInfo.maxBSTSize > maxBSTSize) {
            maxBSTSize = rightInfo.maxBSTSize;
            maxBSTHead = rightInfo.maxBSTHead;
        }

        boolean isBST = false;
        // 当前x是二叉搜索子树的节点
        // 左子树加上当前x节点是二叉搜索树或者右子树加上当前x节点是二叉搜索树或者左子树和右子树都加上当前节点是二叉搜索树
        if (((leftIfo == null) || leftIfo.isBST) && ((rightInfo == null) || rightInfo.isBST)) {
            if (((leftIfo == null) || leftIfo.max < x.value) && ((rightInfo == null) || rightInfo.min < x.value)) {
                maxBSTHead = x;
                isBST = true;
                int leftSize = leftIfo == null ? 0 : leftIfo.maxBSTSize;
                int rightSize = rightInfo == null ? 0 : rightInfo.maxBSTSize;
                maxBSTSize = leftSize + rightSize + 1;

            }
        }
        return new NeedInfo(maxBSTHead, isBST, max, min, maxBSTSize);
    }

    // 为了保证招聘信息的质量问题，公司为每个职位设计了打分系统，打分可以为正数，也可以为负数，正数表示用户认可帖子质量，负数表示用户不认可帖子质量。
    // 打分的分数根据评价用户的等级大小不定，比如可以打1分，10分，30分，-10分等。假设数组A记录了一条帖子所有的打分记录，
    // 现在需要找出帖子曾经得到过最高的分数是多少，用于后续根据最高分数来确认需要对发帖用户做相应的惩罚或奖励。
    // 其中，最高分的定义为：用户所有打分记录中，连续打分数据之和的最大值即认为是帖子曾经获得的最高分。
    // 例如：帖子10001010近期打分记录为：[1,1,-1,-10,11,4,-6,9,20,-2]，那么该条帖子曾经达到过的最高分数为11+4+(-6)+9+20=38。
    // 请实现一段代码，输入为帖子近期的打分记录，输出为当前帖子得到的最高分数。
    // 子数组的最大累加和问题
    public static int maxSum(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }

        int max = Integer.MIN_VALUE;
        int cur = 0;
        for (int num : arr) {
            cur += num;
            max = Math.max(max, cur);
            cur = Math.max(cur, 0);
        }
        return max;
    }

    // 给定一个整型矩阵，返回子矩阵的最大累加和
    // 矩阵压缩技巧：求矩阵先看能否先求子数组，然后再看能否把子矩阵压缩
    public static int subMatrixMaxSum(int[][] arr) {
        if (arr == null || arr.length == 0 || arr[0].length == 0) {
            return 0;
        }
        int max = Integer.MIN_VALUE;
        int cur = 0;
        int[] s = null;
        for (int i = 0; i != arr.length; i++) {
            s = new int[arr[0].length];
            for (int j = i; j != arr.length; j++) {
                cur = 0;
                for (int k = 0; k != s.length; k++) {
                    s[k] += arr[j][k];
                    cur += s[k];
                    max = Math.max(max, cur);
                    cur = Math.max(cur, 0);
                }
            }
        }
        return max;
    }

    // 小Q正在给一条长度为n的道路设计路灯安置方案。
    // 为了让问题更简单,小Q把道路视为n个方格,需要照亮的地方用'.'表示, 不需要照亮的障碍物格子用'X'表示。
    // 小Q现在要在道路上设置一些路灯, 对于安置在pos位置的路灯, 这盏路灯可以照亮pos - 1, pos, pos + 1这三个位置。
    // 小Q希望能安置尽量少的路灯照亮所有'.'区域, 希望你能帮他计算一下最少需要多少盏路灯。
    // 输入描述
    // 输入的第一行包含一个正整数t(1 <= t <= 1000), 表示测试用例数
    // 接下来每两行一个测试数据, 第一行一个正整数n(1 <= n <= 1000),表示道路的长度。
    // 第二行一个字符串s表示道路的构造,只包含'.'和'X'。
    // 输出描述
    // 对于每个测试用例, 输出一个正整数表示最少需要多少盏路灯
    public static int minLight1(String road) {
        char[] str = road.toCharArray();
        int i = 0;
        int light = 0;
        while (i < str.length) {
            if (str[i] == 'X') {// i位置是x，不用放灯
                i++;
            } else {// i位置是点，考虑是否放灯
                light++;
                if (i + 1 == str.length) {
                    break;
                } else { // 有i位置 i+ 1 X .
                    if (str[i + 1] == 'X') {
                        i = i + 2;
                    } else {
                        i = i + 3;
                    }
                }
            }
        }
        return light;
    }

    // 更简洁的解法
    // 两个X之间，数一下.的数量，然后除以3，向上取整
    // 把灯数累加
    public static int minLight2(String road) {
        char[] str = road.toCharArray();
        int cur = 0;
        int light = 0;
        for (char c : str) {
            if (c == 'X') {
                light += (cur + 2) / 3;
                cur = 0;
            } else {
                cur++;
            }
        }
        light += (cur + 2) / 3;
        return light;
    }

    // 已知一棵二叉树没有重复节点，并且给定了这个棵树的中序遍历数组和先序遍历数组。请你返回这棵二叉树的后序遍历数组


    // 把一个数字用中文的形式表达出来
    // 直接接收1~9范围
    public static String num1To9(int num) {
        if (num < 1 || num > 9) {
            return "";
        }
        String[] names = {"一", "二", "三", "四", "五", "六", "七", "八", "九"};
        return names[num - 1];
    }

    // 直接接收1~99范围
    public static String num1To99(int num, boolean hasBai) {
        if (num < 1 || num > 99) {
            return "";
        }
        if (num < 10) {
            return num1To9(num);
        }
        int shi = num / 10;
        if (shi == 1 && !hasBai) {
            return "十" + num1To9(num % 10);
        } else {
            return num1To9(num / 10) + "十" + num1To9(num % 10);
        }
    }

    // 直接接收1~999范围
    public static String num1To999(int num) {
        if (num < 1 || num > 999) {
            return "";
        }
        if (num < 100) {
            return num1To99(num, false);
        }
        String res = num1To9(num / 100) + "百";
        int rest = num % 100;
        if (rest == 0) {
            return res;
        } else if (rest > 10) {
            res += num1To99(rest, true);
        } else {
            res += "零" + num1To9(rest);
        }
        return res;
    }

    // 直接接收1~9999范围
    public static String num1To9999(int num) {
        if (num < 1 || num > 9999) {
            return "";
        }
        if (num < 1000) {
            return num1To999(num);
        }
        String res = num1To9(num / 1000) + "千";
        int rest = num % 1000;
        if (rest == 0) {
            return res;
        } else if (rest >= 100) {
            res += num1To999(rest);
        } else {
            res += "零" + num1To99(rest, false);
        }
        return res;
    }

    // 直接接收1~99999范围
    public static String num1To99999999(int num) {
        if (num < 1 || num > 9999_9999) {
            return "";
        }
        int wan = num / 1_0000;
        int rest = num % 1_0000;
        if (wan == 0) {
            return num1To9999(rest);
        }
        String res = num1To9999(wan) + "万";
        if (rest == 0) {
            return res;
        } else if (rest >= 1000) {
            res += num1To9999(rest);
        } else {
            res += "零" + num1To999(rest);
        }
        return res;
    }

    public static String getNumChineseExpression(int num) {
        if (num == 0) {
            return "零";
        }
        String res = num < 0 ? "负" : "";
        int yi = Math.abs(num / 1_0000_0000);
        int rest = Math.abs(num % 1_0000_0000);
        if (yi == 0) {
            return res + num1To99999999(rest);
        }
        res += num1To9999(yi) + "亿";
        if (rest == 0) {
            return res;
        } else if (rest < 10000000) {
            res += "零" + num1To99999999(rest);
        } else {
            res += num1To99999999(rest);
        }
        return res;
    }

    // 把一个数字用英文的形式表达出来
    public static String num1To19(int num) {
        if (num < 1 || num > 19) {
            return "";
        }
        String[] names = {"One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve",
                "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen"};
        return names[num - 1];
    }

    public static String num1To99En(int num) {
        if (num < 1 || num > 99) {
            return "";
        }
        if (num < 20) {
            return num1To19(num);
        }
        int high = num / 10;
        String[] tyNames = {"Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety"};
        return tyNames[high - 2] + num1To19(num % 10);
    }

    public static String num1To999En(int num) {
        if (num < 1 || num > 999) {
            return "";
        }
        if (num < 100) {
            return num1To99En(num);
        }
        int high = num / 100;
        return num1To19(high) + "hundred" + num1To99En(num % 100);
    }

    public static String getNumEnglishExpression(int num) {
        if (num == 0) {
            return "zero";
        }
        String res = "";
        if (num < 0) {
            res = "Negative, ";
        }
        if (num == Integer.MIN_VALUE) {
            res += "Two Billion";
            long mod = -20_000_000_000L;
        }
        num = Math.abs(num);
        int high = 1_000_000_000;
        int highIndex = 0;
        String[] names = {"Billion", "Million", "Thousand", ""};
        while (num != 0) {
            int cur = num / high;
            num = num % high;
            if (cur != 0) {
                res += num1To999En(cur);
                res += names[highIndex] + ((num == 0) ? " " : ",");
            }
            high = high / 1000;
            highIndex++;
        }
        return res;
    }

    // 求完全二叉树的节点个数
    // 画图理解
    public static class CBTNode {
        public int value;
        public CBTNode left;
        public CBTNode right;

        public CBTNode(int value) {
            this.value = value;
        }
    }

    public static int getCBTTotalNum(CBTNode head) {
        if (head == null) {
            return 0;
        }
        return bs(head, 1, mostLeftLevel(head, 1));
    }

    // node在leve层，h是总深度
    // 以node为头节点的完全二叉树，节点个数是多少
    private static int bs(CBTNode node, int level, int h) {
        if (level == h) {
            return 1;
        }
        if (mostLeftLevel(node.right, level + 1) == h) {
            return 1 << (h - level) + bs(node.right, level + 1, h);
        } else {
            return 1 << (h - level + 1) + bs(node.left, level + 1, h);
        }
    }

    // 如果node在第level层
    // 以node为头节点的子树，最大深度是多少
    // node为头的子树一定是完全二叉树
    private static int mostLeftLevel(CBTNode node, int level) {
        while (node != null) {
            level++;
            node = node.left;
        }
        return level - 1;
    }

    // 最长递增子序列问题
    // 经典实现 时间复杂度O(N^2)
    public static int[] getLongestSubByDP(int[] arr) {
        int[] dp = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            dp[i] = 1;
            // 查看左边所有的数是否能构成递增序列
            for (int j = 0; j < i; j++) {
                if (arr[i] > arr[j]) {
                    dp[i] = Math.max(dp[i], dp[j] + 1);
                }
            }
        }
        return dp;
    }

    // 时间复杂度O(N*logN)
    public static int[] getLongestSubByDP2(int[] arr) {
        //初始化
        int[] dp = new int[arr.length];
        int[] ends = new int[arr.length];
        ends[0] = arr[0];
        dp[0] = 1;
        int right = 0;
        int l = 0;
        int r = 0;
        int m = 0;
        for (int i = 1; i < arr.length; i++) {
            l = 0;
            r = right;
            while (l <= r) {
                m = (l + r) / 2;
                if (arr[i] > ends[m]) {
                    l = m + 1;
                } else {
                    r = m - 1;
                }
            }
            right = Math.max(right, l);
            ends[l] = arr[i];
            dp[i] = l + 1;
        }
        return dp;
    }

    // 小Q得到一个神奇的数列: 1, 12, 123,…12345678910,1234567891011…。并且小Q对于能否被3整除这个性质很感兴趣。
    // 小Q现在希望你能帮他计算一下从数列的第l个到第r个(包含端点)有多少个数可以被3整除。
    // 输入描述:
    // 输入包括两个整数l和r(1 <= l <= r <= 1e9), 表示要求解的区间两端。
    // 输出描述:
    // 输出一个整数, 表示区间内能被3整除的数字个数。
    public static int is3(int l, int r) {
        int count = 0;
        for (int i = l; i <= r; i++) {
            int result = i * (i + 1) / 2;
            if (result % 3 == 0) {
                count++;
            }
        }
        return count;
    }

    // 给定一个整数数组A，长度为n，有 1 <= A[i] <= n，且对于[1,n]的整数，其中部分整数会重复出现而部分不会出现。
    // 实现算法找到[1,n]中所有未出现在A中的整数。
    // 提示：尝试实现O(n)的时间复杂度和O(1)的空间复杂度（返回值不计入空间复杂度）。
    // 输入描述：
    // 一行数字，全部为整数，空格分隔
    // A0 A1 A2 A3…
    // 输出描述：
    // 一行数字，全部为整数，空格分隔R0 R1 R2 R3…
    // 示例1:
    // 输入
    // 1 3 4 3
    // 输出
    // 2
    public static void printNoInArray(int[] arr) {
        if (arr == null || arr.length == 0) {
            return;
        }
        for (int num : arr) { //争取做到0下标位置放1，以此类推
            modify(num, arr);
        }
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != i + 1) {
                System.out.println(i + 1);
            }
        }
    }

    private static void modify(int num, int[] arr) {
        while (arr[num - 1] != num) {
            int temp = arr[num - 1];
            arr[num - 1] = num;
            num = temp;
        }
    }

    // CC 里面有一个土豪很喜欢一位女直播 kiki 唱歌，平时就经常给她点赞、送礼、私聊。
    // 最近 CC 直播平台在举行中秋之星直播唱歌比赛，假设一开始该女主播的初始人气值为 start，能够晋升下一轮人气需要刚好达到 end，
    // 土豪给主播增加人气可以采取一下三种方法：
    // 1.点赞；花费 x C币，人气 + 2
    // 2.送礼；花费 y C 币，人气 * 2
    // 3.私聊；花费 z C币，人气 - 2
    // 其中 end 远大于 start 且 end 为偶数，请写一个程序帮助土豪计算一下，最少花费多少 C 币就能帮助该主播 kiki 将人气刚好达到 end，从而能够晋级下一轮？
    // 限制 0 < x , y , z < = 10000 ; 0 < s t a r t , e n d < 1000000
    // 【例如】
    // 输入：start = 3，end = 100，x = 1，y = 2，z = 6
    // 输出：6
    //start偶数 end偶数 start<=end
    // 当递归条件不够，导致递归无法结束，要使用频繁解去加递归结束条件
    public static int minCoins(int add, int times, int del, int start, int end) {
        if (start > end) {
            return -1;
        }
        int limitPopularity = 2 * end;
        int limitCoin = ((end - start) / 2) * add;
        return dfs(0, start, add, times, del, end, limitPopularity, limitCoin);
    }

    /**
     * @param preMoney        之前已经花了多少钱 可变
     * @param cur             当前来到的人气	可变
     * @param add             点赞花费
     * @param times           送礼花费
     * @param del             私聊花费
     * @param aim             目标人气
     * @param limitPopularity 人气大到什么程度就不需要再尝试了 固定
     * @param limitCoin       已经使用的币大到什么程度就不需要再尝试了 固定
     * @return 返回最小币数
     */
    public static int dfs(int preMoney, int cur, int add, int times, int del,
                          int aim, int limitPopularity, int limitCoin) {
        //之前已经花的钱大于了最大钱数
        if (preMoney > limitCoin) {
            return Integer.MAX_VALUE;
        }
        //人气值小于0
        if (cur < 0) {
            return Integer.MAX_VALUE;
        }
        //当前人气值超过了目标人气值的2倍
        if (cur > limitPopularity) {
            return Integer.MAX_VALUE;
        }
        //当前人气值等于目标人气值
        if (cur == aim) {
            return preMoney;
        }
        int p1 = dfs(preMoney + add, cur + 2, add, times, del, aim, limitPopularity, limitCoin);
        int p2 = dfs(preMoney + times, cur * 2, add, times, del, aim, limitPopularity, limitCoin);
        int p3 = dfs(preMoney + del, cur - 2, add, times, del, aim, limitPopularity, limitCoin);
        return Math.min(Math.min(p1, p2), p3);
    }

    public static int minCoinsDp(int add, int times, int del, int start, int end) {
        if (start > end) {
            return -1;
        }
        int limitPopularity = 2 * end;
        int limitCoin = ((end - start) / 2) * add;
        int[][] dp = new int[limitCoin + 1][limitPopularity + 1];
        for (int pre = 0; pre <= limitCoin; pre++) {
            for (int aim = 0; aim <= limitPopularity; aim++) {
                if (start == aim) {
                    dp[pre][aim] = pre;
                } else {
                    dp[pre][aim] = Integer.MAX_VALUE;
                }
            }
        }
        for (int pre = limitCoin; pre >= 0; pre--) {
            for (int aim = 0; aim <= limitPopularity; aim++) {
                // 点赞
                if (aim - 2 >= 0 && pre + add <= limitCoin) {
                    dp[pre][aim] = Math.min(dp[pre][aim], dp[pre + add][aim - 2]);
                }
                // 私聊
                if (aim + 2 <= limitPopularity && pre + del <= limitCoin) {
                    dp[pre][aim] = Math.min(dp[pre][aim], dp[pre + del][aim + 2]);
                }
                // 送礼
                if ((aim & 1) == 0) {
                    if (aim / 2 >= 0 && pre + times < limitCoin) {
                        dp[pre][aim] = Math.min(dp[pre][aim], dp[pre + times][aim / 2]);
                    }
                }
            }
        }
        return dp[0][end];
    }

    // CC直播的运营部门组织了很多运行活动，每个活动需要花费一定的时间参与，主播每参加一个活动即可得到一定的奖励，参与活动可以从任意活动开始，
    // 但一旦开始，就需要将后续活动参加完毕（注意，最后一个活动必须参与），活动之间存在一定的依赖关系（不存在环的情况），，现在给出所有的活动时间与依赖关系，
    // 以及给出有限的时间，请帮主播计算在有限的时间内，可以获得的最大奖励以及需要的最少时长。
    // 活动 | 活动所需时间（天） | 获得奖励（C）
    // A     3                  2000
    // B     3                  4000
    // C     2                  2500
    // D     1                  1600
    // E     4                   3800
    // F     2                   2600
    // G     4                   4000
    // H     3                   3500
    // 如上图所示，给定有限时间为10天，可以获得的最大奖励为11700，需要的时长为9天，参加的活动为BDFH四个
    // 输入描述：
    // 第一行输入数据N与D，表示有N项活动，D表示给予的时长，0<N<=1000, 0 < D <= 10000,
    // 从第二行开始到N+1行，每行描述一个活动的信息，其中第一项表示当前活动需要花费多少时间，
    // 第二项表示可以获得的奖励a，之后有N项数据，表示当前活动与其他活动的依赖关系，1表示有依赖，0表示无依赖，每项使用空格分开。
    // 输出描述：
    // 输出两项数据A与T， 用空格分开，A表示所获得的最大奖励，T表示所需要的时长。
    // 输入：
    // 8 10
    // 3 2000 0 1 1 0 0 0 0 0
    // 3 4000 0 0 0 1 1 0 0 0
    // 2 2500 0 0 0 1 0 0 0 0
    // 1 1600 0 0 0 0 1 1 1 0
    // 4 3800 0 0 0 0 0 0 0 1
    // 2 2600 0 0 0 0 0 0 0 1
    // 4 4000 0 0 0 0 0 0 0 1
    // 3 3500 0 0 0 0 0 0 0 0
    // 输出
    // 11700 9
    //根据已知的信息，设置的节点信息内容有哪些
    public static class ActivityNode {
        int day;
        int money;
        LinkedList<ActivityNode> preNodes;  //指向该Node节点的上一级节点集合
        TreeMap<Integer, Integer> treeMap; //从最后一个活动到该节点活动的汇总

        public ActivityNode(int day, int money) {
            this.day = day;
            this.money = money;
            preNodes = new LinkedList<>();
            treeMap = new TreeMap<>();
        }

        public void put_treeMap(int day, int money) {
            treeMap.put(day, money);
            adjust();
        }

        public void adjust() {
            if (preNodes.isEmpty()) {  //当没有上级节点时候，需要对节点信息汇总进行调整，天数增加，钱也增加，不满足删除
                if (!treeMap.isEmpty()) {
                    int preMoney = Integer.MIN_VALUE;
                    Iterator<Map.Entry<Integer, Integer>> iterator = treeMap.entrySet().iterator();
                    while (iterator.hasNext()) {
                        Map.Entry<Integer, Integer> i = iterator.next();
                        Integer money = i.getValue();
                        if (money < preMoney) {
                            iterator.remove();
                            continue;
                        }
                        preMoney = money;
                    }
                }
            }
        }
    }

    //根据已知的信息，设置节点并返回最后一个节点
    //dayMoney  节点的信息     arr 节点的依赖关系
    public ActivityNode InfoChangeToNode(int[][] dayMoney, int[][] arr) {
        HashMap<Integer, ActivityNode> hashMap = new HashMap<>();
        for (int i = 0; i < dayMoney.length; i++) {
            ActivityNode node = new ActivityNode(dayMoney[i][0], dayMoney[i][1]);
            hashMap.put(i, node);
        }
        for (int i = 1; i < arr.length; i++) {
            for (int j = 0; j < i; j++) {
                if (arr[j][i] == 1) {
                    ActivityNode node = hashMap.get(i);
                    ActivityNode preNode = hashMap.get(j);
                    node.preNodes.add(preNode);
                }
            }
        }
        return hashMap.get(arr.length - 1);  //返回最后一个节点
    }

    //根据最后一个节点进行反向深度优先遍历，每一个节点的treeMap进行设置
    public void set(ActivityNode node) {
        setDayAndMoney(node, 0, 0);
    }

    public void setDayAndMoney(ActivityNode node, int preDay, int preMoney) {
        int day = node.day + preDay;
        int money = node.money + preMoney;
        node.put_treeMap(day, money);  //当前节点的treeMap信息添加
        for (ActivityNode preNode : node.preNodes) {
            setDayAndMoney(preNode, day, money);
        }
    }

    //主方法：获得活动获得的最大奖励
    public int[] getMaxMoney(int limitDay, int[][] dayMoney, int[][] arr) {
        if (dayMoney.length != arr.length || limitDay <= 0) {
            throw new RuntimeException("信息有误");
        }
        ActivityNode endNode = InfoChangeToNode(dayMoney, arr);
        // 从尾节点开始设置每个节点的天数和奖励信息
        set(endNode);
        //这样每个节点的treeMap就有信息
        ActivityNode head = new ActivityNode(0, 0);
        head.treeMap.put(endNode.day, endNode.money);//把尾节点的信息加入
        //深度优先遍历 dfs
        Stack<ActivityNode> stack = new Stack<>();
        HashSet<ActivityNode> hashSet = new HashSet<>();  //防止重复
        ActivityNode cur = endNode;
        stack.push(cur);
        hashSet.add(cur);
        while (!stack.isEmpty()) {
            ActivityNode temp = stack.pop();
            for (ActivityNode preNode : temp.preNodes) {
                if (!hashSet.contains(preNode)) {
                    stack.push(temp);
                    stack.push(preNode);
                    hashSet.add(preNode);
                    //对节点信息进行处理
                    TreeMap<Integer, Integer> preTreeMap = preNode.treeMap;
                    for (Integer i : preTreeMap.keySet()) {
                        head.put_treeMap(i, preTreeMap.get(i));
                    }
                    break;
                }
            }
        }
        for (Integer i : head.treeMap.keySet()) {
            System.out.println("day: " + i + ",money: " + head.treeMap.get(i));
        }

        //根据限制天数limitDay选择最大的活动奖励
        int[] res = new int[2];
        if (head.treeMap.firstKey() > limitDay) {
            res[0] = 0;
            res[1] = 0;
        } else if (head.treeMap.lastKey() < limitDay) {
            res[0] = head.treeMap.get(head.treeMap.lastKey());
            res[1] = head.treeMap.lastKey();
        } else {
            if (head.treeMap.containsKey(limitDay)) {
                res[0] = head.treeMap.get(limitDay);
                res[1] = limitDay;
            } else {
                //limitDay介于最小天数和最大天数之间，且不等于边界
                while (!head.treeMap.containsKey(--limitDay)) {
                } //一直向下找，找到最接近limitDay的那一组数据
                res[0] = head.treeMap.get(limitDay);
                res[1] = limitDay;
            }
        }
        return res;
    }

    // 给定一个只由 0(假)、1(真)、&(逻辑与)、|(逻辑或)和^(异或)五种字符组成的字符串express，再给定一个布尔值 desired。
    // 返回express能有多少种组合方式，可以达到desired的结果。
    //【举例】：
    // express="1^0|0|1"，desired=false
    // 只有 1^((0|0)|1)和 1^(0|(0|1))的组合可以得到 false，返回 2。
    // express="1"，desired=false无组合则可以得到false，返回0。
    // 每一个符号作为最后计算的范围尝试
    // 暴力递归尝试
    public static int getDesiredWays(String exp, boolean desired) {
        if (exp == null || exp.length() == 0) {
            return 0;
        }
        char[] express = exp.toCharArray();
        if (!isValid2(express)) {
            return 0;
        }
        return process(express, desired, 0, express.length - 1);
    }

    // express[l.....r]，返回组合是desired的方法数
    public static int process(char[] express, boolean desired, int l, int r) {
        if (l == r) {
            // 看期待的位置和当前位置是否是同一个boolean值
            if (express[l] == '1') {
                return desired ? 1 : 0;
            } else {
                return desired ? 0 : 1;
            }
        }
        int res = 0;
        if (desired) {
            for (int i = l + 1; i < r; i += 2) {
                switch (express[i]) {
                    case '&':
                        res += process(express, true, l, i - 1) * process(express, true, i + 1, r);
                        break;
                    case '|':
                        res += process(express, true, l, i - 1) * process(express, false, i + 1, r);
                        res += process(express, false, l, i - 1) * process(express, true, i + 1, r);
                        res += process(express, true, l, i - 1) * process(express, true, i + 1, r);
                        break;
                    case '^':
                        res += process(express, true, l, i - 1) * process(express, false, i + 1, r);
                        res += process(express, false, l, i - 1) * process(express, true, i + 1, r);
                        break;
                }
            }
        } else {
            for (int i = l + 1; i < r; i += 2) {
                switch (express[i]) {
                    case '&':
                        res += process(express, true, l, i - 1) * process(express, true, i + 1, r);
                        break;
                    case '|':
                        res += process(express, true, l, i - 1) * process(express, false, i + 1, r);
                        res += process(express, false, l, i - 1) * process(express, true, i + 1, r);
                        res += process(express, true, l, i - 1) * process(express, true, i + 1, r);
                        break;
                    case '^':
                        res += process(express, true, l, i - 1) * process(express, false, i + 1, r);
                        res += process(express, false, l, i - 1) * process(express, true, i + 1, r);
                        break;
                }
            }
        }
        return res;
    }

    public static boolean isValid2(char[] express) {
        if ((express.length & 1) == 0) {
            return false;
        }
        for (int i = 0; i < express.length; i += 2) {
            if (express[i] != '1' && express[i] != '0') {
                return false;
            }
        }
        for (int i = 1; i < express.length; i += 2) {
            if (express[i] != '&' && express[i] != '^' && express[i] != '|') {
                return false;
            }
        }
        return true;
    }

    // 动态规划
    public static int getDesiredWaysDp(String exp, boolean desired) {
        char[] express = exp.toCharArray();
        if (!isValid2(express)) {
            return 0;
        }
        int n = express.length;
        int[][] trueMap = new int[n][n];
        int[][] falseMap = new int[n][n];
        for (int i = 0; i < n; i += 2) {
            trueMap[i][i] = express[i] == '1' ? 1 : 0;
            falseMap[i][i] = express[i] == '0' ? 1 : 0;
        }
        // 画图理解
        for (int row = n - 2; row >= 0; row -= 2) {
            for (int col = row + 2; col < n; col += 2) {
                for (int i = row + 1; i < col; i += 2) {
                    switch (express[row]) {
                        case '&':
                            trueMap[row][col] += trueMap[row][i - 1] * trueMap[i + 1][col];
                            break;
                        case '|':
                            trueMap[row][col] += trueMap[row][i - 1] * trueMap[i + 1][col];
                            trueMap[row][col] += trueMap[row][i - 1] * trueMap[i + 1][col];
                            trueMap[row][col] += trueMap[row][i - 1] * trueMap[i + 1][col];
                            break;
                        case '^':
                            trueMap[row][col] += trueMap[row][i - 1] * trueMap[i + 1][col];
                            trueMap[row][col] += trueMap[row][i - 1] * trueMap[i + 1][col];
                            break;
                    }
                    switch (express[row]) {
                        case '&':
                            falseMap[row][col] += falseMap[row][i - 1] * falseMap[i + 1][col];
                            break;
                        case '|':
                            falseMap[row][col] += falseMap[row][i - 1] * falseMap[i + 1][col];
                            falseMap[row][col] += falseMap[row][i - 1] * falseMap[i + 1][col];
                            falseMap[row][col] += falseMap[row][i - 1] * falseMap[i + 1][col];
                            break;
                        case '^':
                            falseMap[row][col] += falseMap[row][i - 1] * falseMap[i + 1][col];
                            falseMap[row][col] += falseMap[row][i - 1] * falseMap[i + 1][col];
                            break;
                    }
                }
            }
        }
        return desired ? trueMap[0][n - 1] : falseMap[0][n - 1];
    }

    // 在一个字符串中找到没有重复字符子串中最长的长度。
    // 例如:
    // abcabcbb没有重复字符的最长子串是abc，长度为3
    // bbbbb，答案是b，长度为1
    // pwwkew，答案是wke，长度是3
    // 要求:答案必须是子串，"pwke" 是一个子字符序列但不是一个子字符串。
    public static int longestNoRepeatSubString(String str) {
        if (str == null || str.length() == 0) {
            return 0;
        }
        char[] chars = str.toCharArray();
        int[] map = new int[256];
        Arrays.fill(map, -1);
        int len = 0;
        int pre = -1;
        int cur = 0;
        for (int i = 0; i != chars.length; i++) {
            // 选出离当前位置最近的隔离点
            pre = Math.max(pre, map[chars[i]]);
            // 记录当前字符到最近隔离点的长度
            cur = i - pre;
            // 比较长度大小
            len = Math.max(len, cur);
            // 记录当前字符出现的位置
            map[chars[i]] = i;
        }
        return len;
    }

    // 给定两个字符串str1和str2，在给定三个整数ic,dc和rc,分别代表插入、删除和替换一个字符的代价，返回将str1编辑成str2的最小代价。
    // 比如，str1=”abc”，str2=”adc”，ic=5,dc=3,rc=2.从”abc”编辑成”adc”，把b替换成d是代价最小的，所以返回2.
    // 再比如，str1=”abc”,str2=”adc”,ic=5,dc=3,rc=100.从abc编辑成adc，先删除b，然后再插入d是代价最小的，所以返回8
    // 编辑距离问题
    public static int minEditDistance(String str1, String str2, int ic, int dc, int rc) {
        if (str1 == null || str2 == null) {
            return 0;
        }
        char[] chs1 = str1.toCharArray();
        char[] chs2 = str2.toCharArray();
        int row = chs1.length + 1;
        int col = chs2.length + 1;
        int[][] dp = new int[row][col];
        for (int i = 1; i < row; i++) {
            dp[i][0] = i * dc;
        }
        for (int i = 1; i < col; i++) {
            dp[0][i] = i * ic;
        }
        for (int i = 1; i < row; i++) {
            for (int j = 1; j < col; j++) {
                if (chs1[i - 1] == chs2[j - 1]) {
                    dp[i][j] = dp[i - 1][j - 1];
                } else {
                    dp[i][j] = dp[i - 1][j - 1] + rc;
                }
                dp[i][j] = Math.min(dp[i][j], dp[i - 1][j] + dc);
                dp[i][j] = Math.min(dp[i][j], dp[i][j - 1] + ic);
            }
        }
        return dp[row - 1][col - 1];
    }

    // 给定一个全是小写字母的字符串str，删除多余字符，使得每种字符只保留一个，并让最终结果字符串的字典序最小。
    //【举例】
    // str = "acbc"，删掉第一个'c'，得到"abc"，是所有结果字符串中字典序最小的。
    // str = "dbcacbca"，删掉第一个'b'、第一个'c'、第二个'c'、第二个'a'，得到"dabc"， 是所有结 果字符串中字典序最小的。
    public static String getMinOrderedByRemove(String str) {
        if (str == null || str.length() == 0) {
            return null;
        }
        int[] map = new int[256];
        int minIndex = 0;
        for (int i = 0; i < str.length(); i++) {
            map[str.charAt(i)]++;
        }
        for (int i = 0; i < str.length(); i++) {
            if (--map[str.charAt(i)] == 0) {
                break;
            } else {
                minIndex = str.charAt(minIndex) > str.charAt(i) ? i : minIndex;
            }
        }
        return String.valueOf(str.charAt(minIndex)) +
                getMinOrderedByRemove(str.substring(minIndex + 1)).
                        replaceAll(String.valueOf(str.charAt(minIndex)), "");
    }

    // 在数据加密和数据压缩中常需要对特殊的字符串进行编码。给定的字母表A由26个小写字母组成。
    // 该字母表产生的升序字符串中字母从左到右出现的次序与字母在字母表中出现的次序相同，且每个字符最多出现1次。
    // 例如，a,b,ab,bc,xyz等字符串都是升序字符串。现在对字母表中产生的所有长度不超过6的升序字符串，计算它在字典中的编码。
    // 输入描述：
    // 3
    // a
    // b
    // ab
    // 输出描述
    // 1
    // 2
    // 27
    // 必须以i号字符为头，长度的len的字符串有多少个
    public static int g(int i, int len) {
        int ans = 0;
        if (len == 1) {
            return 1;
        }
        for (int j = i + 1; j < 26; j++) {
            ans += g(j, len - 1);
        }
        return ans;
    }

    // 长度为len的字符串有多少个
    public static int f(int len) {
        int ans = 0;
        for (int i = 1; i <= 26; i++) {
            ans += g(i, len);
        }
        return ans;
    }

    public static int getStringKth(String str){
        char[] s = str.toCharArray();
        int sum = 0;
        int len = s.length;
        // 求出长度小于该字符串长度的所有可能
        for (int i = 1; i < len; i++) {
            sum+=f(i);
        }
        // 求出小于该字符串头节点的之前的那些字母，并且与该字符串长度相同的所有可能
        int first = s[0] - 'a' + 1;
        for (int i = 1; i < first; i++) {
            sum += g(i,len);
        }
        // 依次取定该字符串的每一个位置的字母，求所有可能
        // 自行举例子理解
        int pre = first;
        for (int i = 1; i < len ;i++) {
            int cur = s[i] - 'a' + 1;
            for (int j = pre + 1; j < cur; j++) {
                sum+=g(j,len - 1);
            }
            pre = cur;
        }
        return sum + 1;
    }
}
