package algorithmProblems;

import java.util.*;

public class interviewQuestionsTwo {
    public static class RetainTree {
        public static class Node {
            // 值
            public int value;
            // 是否保留
            public boolean retain;
            // 下级节点
            public List<Node> nexts;

            public Node(int v, boolean r) {
                value = v;
                retain = r;
                nexts = new ArrayList<>();
            }
        }

        // 给定一棵树的头节点head
        // 请按照题意，保留节点，没有保留的节点删掉
        // 换句话说就是 给一个二叉树，树中节点有黑有白，保留能到黑节点的路径上的所有节点(包括黑节点)，白节点就删除掉
        // 树调整完之后，返回头节点
        public static Node retain(Node x) {
            // 叶子节点是否保留
            if (x.nexts.isEmpty()) {
                return x.retain ? x : null;
            }
            // 非叶子节点
            // x下层有节点
            List<Node> newNexts = new ArrayList<>();
            for (Node next : x.nexts) {
                Node newNext = retain(next);
                if (newNext != null) {
                    newNexts.add(newNext);
                }
            }
            // x.nexts  老的链表，下级节点
            // newNexts 新的链表，只有保留的在里面
            if (!newNexts.isEmpty() || x.retain) {
                x.nexts = newNexts;
                return x;
            }
            return null;
        }

        // 先序打印
        public static void preOrderPrint(Node head) {
            System.out.println(head.value);
            for (Node next : head.nexts) {
                preOrderPrint(next);
            }
        }
    }

    // 来自美团面试
    // 求二叉树的最小深度
    // 测试链接：https://leetcode.cn/problems/minimum-depth-of-binary-tree/
    public static class MinimumDepthOfBinaryTree {
        public static class TreeNode {
            public int val;
            public TreeNode left;
            public TreeNode right;

            public TreeNode(int x) {
                val = x;
            }
        }

        // head为头的二叉树。返回最小深度
        public static int minDepth(TreeNode head) {
            if (head == null) {
                return 0;
            }
            return process(head);
        }

        private static int process(TreeNode head) {
            if (head.left == null && head.right == null) {
                return 1;
            }
            int leftHigh = Integer.MAX_VALUE;
            if (head.left != null) {
                leftHigh = process(head.left);
            }
            int rightHigh = Integer.MAX_VALUE;
            if (head.right != null) {
                rightHigh = process(head.right);
            }
            return Math.min(leftHigh, rightHigh) + 1;
        }
    }

    // 来自去哪网
    // 给定一个arr，里面的数字都是0~9，你可以随意使用里面的数字，也可以打乱顺序使用，请返回一个能被3整除的最大数字，用str的形式返回
    public static class Mod3Max {
        // 贪心的思路解法 :
        // 先得到数组的累加和，记为sum
        // 1) 如果sum%3==0，说明所有数从大到小拼起来就可以了
        // 2) 如果sum%3==1，说明多了一个余数1，
        // 只需要删掉一个最小的数(该数是%3==1的数);
        // 如果没有，只需要删掉两个最小的数(这两个数都是%3==2的数);
        // 3) 如果sum%3==2，说明多了一个余数2，
        // 只需要删掉一个最小的数(该数是%3==2的数);
        // 如果没有，只需要删掉两个最小的数(这两个数都是%3==1的数);
        // 如果上面都做不到，说明拼不成
        public static String max(int[] A) {
            if (A == null || A.length == 0) {
                return "";
            }
            int mod = 0;
            ArrayList<Integer> arr = new ArrayList<>();
            for (int num : A) {
                arr.add(num);
                mod += num;
                mod %= 3;
            }
            if ((mod == 1 || mod == 2) && !remove(arr, mod, 3 - mod)) {
                return "";
            }
            if (arr.isEmpty()) {
                return "";
            }
            arr.sort((a, b) -> b - a);
            if (arr.get(0) == 0) {
                return "0";
            }
            StringBuilder builder = new StringBuilder();
            for (int num : arr) {
                builder.append(num);
            }
            return builder.toString();
        }

        // 在arr中，要么删掉最小的一个、且%3之后余数是first的数
        // 如果做不到，删掉最小的两个、且%3之后余数是second的数
        // 如果能做到返回true，不能做到返回false
        public static boolean remove(ArrayList<Integer> arr, int first, int second) {
            if (arr.size() == 0) {
                return false;
            }
            arr.sort((a, b) -> compare(a, b, first, second));
            int size = arr.size();
            if (arr.get(size - 1) % 3 == first) {
                arr.remove(size - 1);
                return true;
            } else if (size > 1 && arr.get(size - 1) % 3 == second && arr.get(size - 2) % 3 == second) {
                arr.remove(size - 1);
                arr.remove(size - 2);
                return true;
            } else {
                return false;
            }
        }

        // a和b比较:
        // 如果余数一样，谁大谁放前面
        // 如果余数不一样，余数是0的放最前面、余数是s的放中间、余数是f的放最后
        public static int compare(int a, int b, int f, int s) {
            int ma = a % 3;
            int mb = b % 3;
            if (ma == mb) {
                return b - a;
            } else {
                if (ma == 0 || mb == 0) {
                    return ma == 0 ? -1 : 1;
                } else {
                    return ma == s ? -1 : 1;
                }
            }
        }
    }

    // 求二叉树直径
    // 测试链接：https://leetcode.cn/problems/diameter-of-binary-tree/
    public static class DiameterOfBinaryTree {
        public static class TreeNode {
            public int val;
            public TreeNode left;
            public TreeNode right;

            public TreeNode(int x) {
                val = x;
            }
        }

        public static class Info {
            public int diameter;
            public int height;

            public Info(int a, int b) {
                diameter = a;
                height = b;
            }
        }

        public static int diameterLength(TreeNode head) {
            return process(head).diameter;
        }

        public static Info process(TreeNode x) {
            if (x == null) {
                return new Info(0, 0);
            }
            Info leftInfo = process(x.left);
            Info rightInfo = process(x.right);
            // x节点的高度
            int height = Math.max(leftInfo.height, rightInfo.height) + 1;
            // 不包含x的最大直径
            int diameter = Math.max(leftInfo.diameter, rightInfo.diameter);
            // 比较经过x节点的最大直径和不经过x节点的最大直径
            diameter = Math.max(diameter, leftInfo.height + rightInfo.height);
            return new Info(diameter, height);
        }
    }

    // 来自微众
    // 人工智能岗
    // 一开始有21个球，甲和乙轮流拿球，甲先、乙后
    // 每个人在自己的回合，一定要拿不超过3个球，不能不拿
    // 最终谁的总球数为偶数，谁赢
    // 请问谁有必胜策略
    public static class WhoWin21Balls {
        // balls = 21
        // ball是奇数
        public static String win(int balls) {
            return process(0, balls, 0, 0);
        }

        // turn 谁的回合！
        // turn == 0 甲回合
        // turn == 1 乙回合
        // rest剩余球的数量
        // 之前，jiaBalls、yiBalls告诉你！
        // 当前，根据turn，知道是谁的回合！
        // 当前，还剩多少球，rest
        // 返回：谁会赢！
        public static String process(int turn, int rest, int jia, int yi) {
            if (rest == 0) {
                return (jia & 1) == 0 ? "甲" : "乙";
            }
            // rest > 0, 还剩下球！
            if (turn == 0) { // 甲的回合！
                // 甲，自己赢！甲赢！
                for (int pick = 1; pick <= Math.min(rest, 3); pick++) {
                    // pick 甲当前做的选择
                    if (process(1, rest - pick, jia + pick, yi).equals("甲")) {
                        return "甲";
                    }
                }
                return "乙";
            } else {
                for (int pick = 1; pick <= Math.min(rest, 3); pick++) {
                    // pick 甲当前做的选择
                    if (process(0, rest - pick, jia, yi + pick).equals("乙")) {
                        return "乙";
                    }
                }
                return "甲";
            }
        }

        // 我们补充一下设定，假设一开始的球数量不是21，是任意的正数
        // 如果最终两个人拿的都是偶数，认为无人获胜，平局
        // 如果最终两个人拿的都是奇数，认为无人获胜，平局
        // rest代表目前剩下多少球
        // cur == 0 代表目前是甲行动
        // cur == 1 代表目前是乙行动
        // first == 0 代表目前甲所选的球数，加起来是偶数
        // first == 1 代表目前甲所选的球数，加起来是奇数
        // second == 0 代表目前乙所选的球数，加起来是偶数
        // second == 1 代表目前乙所选的球数，加起来是奇数
        // 返回选完了rest个球，谁会赢，只会返回"甲"、"乙"、"平"
        // win1方法，就是彻底暴力的做所有尝试，并且返回最终的胜利者
        // 在甲的回合，甲会尝试所有的可能，以保证自己会赢，如果自己怎么都不会赢，那也要尽量平局，如果这个也不行，只能对方赢
        // 在乙的回合，乙会尝试所有的可能，以保证自己会赢，如果自己怎么都不会赢，那也要尽量平局，如果这个也不行，只能对方赢
        // 算法和数据结构体系学习班，视频39章节，牛羊吃草问题，就是类似这种递归
        public static String win1(int rest, int cur, int first, int second) {
            if (rest == 0) {
                if (first == 0 && second == 1) {
                    return "甲";
                }
                if (first == 1 && second == 0) {
                    return "乙";
                }
                return "平";
            }
            if (cur == 0) { // 甲行动
                String bestAns = "乙";
                for (int pick = 1; pick <= Math.min(3, rest); pick++) {
                    String curAns = win1(rest - pick, 1, first ^ (pick & 1), second);
                    if (curAns.equals("甲")) {
                        bestAns = "甲";
                        break;
                    }
                    if (curAns.equals("平")) {
                        bestAns = "平";
                    }
                }
                return bestAns;
            } else { // 乙行动
                String bestAns = "甲";
                for (int pick = 1; pick <= Math.min(3, rest); pick++) {
                    String curAns = win1(rest - pick, 0, first, second ^ (pick & 1));
                    if (curAns.equals("乙")) {
                        bestAns = "乙";
                        break;
                    }
                    if (curAns.equals("平")) {
                        bestAns = "平";
                    }
                }
                return bestAns;
            }
        }

        // 下面的win2方法，仅仅是把win1方法，做了记忆化搜索
        // 变成了动态规划
        public static String[][][][] dp = new String[5000][2][2][2];

        public static String win2(int rest, int cur, int first, int second) {
            if (rest == 0) {
                if (first == 0 && second == 1) {
                    return "甲";
                }
                if (first == 1 && second == 0) {
                    return "乙";
                }
                return "平";
            }
            if (dp[rest][cur][first][second] != null) {
                return dp[rest][cur][first][second];
            }
            if (cur == 0) { // 甲行动
                String bestAns = "乙";
                for (int pick = 1; pick <= Math.min(3, rest); pick++) {
                    String curAns = win2(rest - pick, 1, first ^ (pick & 1), second);
                    if (curAns.equals("甲")) {
                        bestAns = "甲";
                        break;
                    }
                    if (curAns.equals("平")) {
                        bestAns = "平";
                    }
                }
                dp[rest][cur][first][second] = bestAns;
                return bestAns;
            } else { // 乙行动
                String bestAns = "甲";
                for (int pick = 1; pick <= Math.min(3, rest); pick++) {
                    String curAns = win2(rest - pick, 0, first, second ^ (pick & 1));
                    if (curAns.equals("乙")) {
                        bestAns = "乙";
                        break;
                    }
                    if (curAns.equals("平")) {
                        bestAns = "平";
                    }
                }
                dp[rest][cur][first][second] = bestAns;
                return bestAns;
            }
        }
    }

    // 来自字节跳动
    // 给定一个数组arr，其中的值有可能正、负、0
    // 给定一个正数k
    // 返回累加和>=k的所有子数组中，最短的子数组长度
    // 本题测试链接 : https://leetcode.com/problems/shortest-subarray-with-sum-at-least-k/
    public static class ShortestSubarrayWithSumAtLeastK {

        public static int shortestSubarray1(int[] arr, int k) {
            if (arr == null || arr.length < 1) {
                return -1;
            }
            int n = arr.length + 1;
            long[] sum = new long[n];
            for (int i = 1; i < n; i++) {
                sum[i] = sum[i - 1] + arr[i - 1];
            }
            int[] stack = new int[n];
            int size = 1;
            int ans = Integer.MAX_VALUE;
            for (int i = 1; i < n; i++) {
                int mostRight = mostRight(sum, stack, size, sum[i] - k);
                ans = Math.min(ans, mostRight == -1 ? Integer.MAX_VALUE : (i - mostRight));
                while (size > 0 && sum[stack[size - 1]] >= sum[i]) {
                    size--;
                }
                stack[size++] = i;
            }
            return ans == Integer.MAX_VALUE ? -1 : ans;
        }

        public static int mostRight(long[] sum, int[] stack, int size, long aim) {
            int l = 0;
            int r = size - 1;
            int m = 0;
            int ans = -1;
            while (l <= r) {
                m = (l + r) / 2;
                if (sum[stack[m]] <= aim) {
                    ans = stack[m];
                    l = m + 1;
                } else {
                    r = m - 1;
                }
            }
            return ans;
        }

        public static int shortestSubarray2(int[] arr, int K) {
            int N = arr.length;
            long[] sum = new long[N + 1];
            for (int i = 0; i < N; i++) {
                sum[i + 1] = sum[i] + arr[i];
            }
            int ans = Integer.MAX_VALUE;
            int[] dq = new int[N + 1];
            int l = 0;
            int r = 0;
            for (int i = 0; i < N + 1; i++) {
                // 头部开始，符合条件的，从头部弹出！
                while (l != r && sum[i] - sum[dq[l]] >= K) {
                    ans = Math.min(ans, i - dq[l++]);
                }
                // 尾部开始，前缀和比当前的前缀和大于等于的，从尾部弹出！
                while (l != r && sum[dq[r - 1]] >= sum[i]) {
                    r--;
                }
                dq[r++] = i;
            }
            return ans != Integer.MAX_VALUE ? ans : -1;
        }
    }

    // 测试链接：https://leetcode.com/problems/powx-n/
    public static class PowXN {

        // 计算一个数x的n次方，n为正数，把n拆解为二进制的形式去算，第一次从1次方开始，后面以此类推2、4、8等等次方快速计算
        public static int pow(int a, int n) {
            int ans = 1;
            int t = a;
            while (n != 0) {
                if ((n & 1) != 0) {
                    ans *= t;
                }
                t *= t;
                n >>= 1;
            }
            return ans;
        }

        // x的n次方，n可能是负数
        public static double myPow(double x, int n) {
            if (n == 0) {
                return 1D;
            }
            int pow = Math.abs(n == Integer.MIN_VALUE ? n + 1 : n);
            double t = x;
            double ans = 1D;
            while (pow != 0) {
                if ((pow & 1) != 0) {
                    ans *= t;
                }
                pow >>= 1;
                t = t * t;
            }
            if (n == Integer.MIN_VALUE) {
                ans *= x;
            }
            return n < 0 ? (1D / ans) : ans;
        }
    }

    // 测试链接：https://leetcode.com/problems/sqrtx/
    public static class SqrtX {

        // x一定非负，输入可以保证
        public static int mySqrt(int x) {
            if (x == 0) {
                return 0;
            }
            if (x < 3) {
                return 1;
            }
            // x >= 3
            long ans = 1;
            long L = 1;
            long R = x;
            long M;
            while (L <= R) {
                M = (L + R) / 2;
                if (M * M <= x) {
                    ans = M;
                    L = M + 1;
                } else {
                    R = M - 1;
                }
            }
            return (int) ans;
        }
    }

    // 测试链接：https://leetcode.com/problems/set-matrix-zeroes/
    public static class SetMatrixZeroes {

        public static void setZeroes1(int[][] matrix) {
            boolean row0Zero = false;
            boolean col0Zero = false;
            int i = 0;
            int j = 0;
            for (i = 0; i < matrix[0].length; i++) {
                if (matrix[0][i] == 0) {
                    row0Zero = true;
                    break;
                }
            }
            for (i = 0; i < matrix.length; i++) {
                if (matrix[i][0] == 0) {
                    col0Zero = true;
                    break;
                }
            }
            for (i = 1; i < matrix.length; i++) {
                for (j = 1; j < matrix[0].length; j++) {
                    if (matrix[i][j] == 0) {
                        matrix[i][0] = 0;
                        matrix[0][j] = 0;
                    }
                }
            }
            for (i = 1; i < matrix.length; i++) {
                for (j = 1; j < matrix[0].length; j++) {
                    if (matrix[i][0] == 0 || matrix[0][j] == 0) {
                        matrix[i][j] = 0;
                    }
                }
            }
            if (row0Zero) {
                for (i = 0; i < matrix[0].length; i++) {
                    matrix[0][i] = 0;
                }
            }
            if (col0Zero) {
                for (i = 0; i < matrix.length; i++) {
                    matrix[i][0] = 0;
                }
            }
        }

        public static void setZeroes2(int[][] matrix) {
            boolean col0 = false;
            int i = 0;
            int j = 0;
            for (i = 0; i < matrix.length; i++) {
                for (j = 0; j < matrix[0].length; j++) {
                    if (matrix[i][j] == 0) {
                        matrix[i][0] = 0;
                        if (j == 0) {
                            col0 = true;
                        } else {
                            matrix[0][j] = 0;
                        }
                    }
                }
            }
            for (i = matrix.length - 1; i >= 0; i--) {
                for (j = 1; j < matrix[0].length; j++) {
                    if (matrix[i][0] == 0 || matrix[0][j] == 0) {
                        matrix[i][j] = 0;
                    }
                }
            }
            if (col0) {
                for (i = 0; i < matrix.length; i++) {
                    matrix[i][0] = 0;
                }
            }
        }
    }

    // 测试链接：https://leetcode.com/problems/maximum-product-subarray/
    public static class MaximumProductSubarray {

        public static double max(double[] arr) {
            if (arr == null || arr.length == 0) {
                return 0; // 报错！
            }
            int n = arr.length;
            // 上一步的最大
            double preMax = arr[0];
            // 上一步的最小
            double preMin = arr[0];
            double ans = arr[0];
            for (int i = 1; i < n; i++) {
                double p1 = arr[i];
                double p2 = arr[i] * preMax;
                double p3 = arr[i] * preMin;
                double curMax = Math.max(Math.max(p1, p2), p3);
                double curMin = Math.min(Math.min(p1, p2), p3);
                ans = Math.max(ans, curMax);
                preMax = curMax;
                preMin = curMin;
            }
            return ans;
        }

        public static int maxProduct(int[] nums) {
            int ans = nums[0];
            int min = nums[0];
            int max = nums[0];
            for (int i = 1; i < nums.length; i++) {
                int curMIn = Math.min(nums[i], Math.min(min * nums[i], max * nums[i]));
                int curMax = Math.max(nums[i], Math.max(min * nums[i], max * nums[i]));
                min = curMIn;
                max = curMax;
                ans = Math.max(ans, max);
            }
            return ans;
        }
    }

    // 测试链接：https://leetcode.com/problems/factorial-trailing-zeroes/
    public static class FactorialTrailingZeroes {

        public static int trailingZeroes(int n) {
            int ans = 0;
            while (n != 0) {
                n /= 5;
                ans += n;
            }
            return ans;
        }
    }

    // 测试链接：https://leetcode.com/problems/reverse-bits/
    public static class ReverseBits {
        public static int reverseBits(int n) {
            // n的高16位，和n的低16位，交换
            n = (n >>> 16) | (n << 16);
            // 给个例子，假设n二进制为：
            // 1011 0111 0011 1001 0011 1111 0110 1010
            // 解释一下，第一行，是把n左边16位，和n右边16位交换
            // n = (n >>> 16) | (n << 16);
            // 因为 n >>> 16 就是左边16位被移动到了右侧
            // 同时 n << 16  就是右边16位被移动到了左侧
            // 又 | 在了一起，所以，n变成了
            // 0011 1111 0110 1010 1011 0111 0011 1001
            n = ((n & 0xff00ff00) >>> 8) | ((n & 0x00ff00ff) << 8);
            // 第二行，
            // n = ((n & 0xff00ff00) >>> 8) | ((n & 0x00ff00ff) << 8);
            // (n & 0xff00ff00)
            // 这一句意思是，左侧开始算0~7位，保留；8~15位，全变0；16~23位，保留；24~31位，全变0
            // 0011 1111 0000 0000 1011 0111 0000 0000
            // (n & 0xff00ff00) >>> 8 这句就是上面的值，统一向右移动8位，变成：
            // 0000 0000 0011 1111 0000 0000 1011 0111
            // (n & 0x00ff00ff)
            // 这一句意思是，左侧开始算0~7位，全变0；8~15位，保留；16~23位，全变0；24~31位，保留
            // 0000 0000 0110 1010 0000 0000 0011 1001
            // (n & 0x00ff00ff) << 8 这句就是上面的值，统一向左移动8位，变成：
            // 0110 1010 0000 0000 0011 1001 0000 0000
            // 那么 ((n & 0xff00ff00) >>> 8) | ((n & 0x00ff00ff) << 8)
            // 什么效果？就是n的0~7位和8~15位交换了，16~23位和24~31位交换了
            // 0110 1010 0011 1111 0011 1001 1011 0111

            // 也就是说，整个过程是n的左16位，和右16位交换
            // n的左16位的内部，左8位和右8位交换；n的右16位的内部，左8位和右8位交换
            // 接下来的一行，其实是，从左边开始算，0~7位内部，左4和右4交换；8~15位，左4和右4交换；...
            // 接下来的一行，其实是，从左边开始算，0~3位内部，左2和右2交换；4~7位，左2和右2交换；...
            // 最后的一行，其实是，从左边开始算，0~1位内部，左1和右1交换；2~3位，左1和右1交换；...
            n = ((n & 0xf0f0f0f0) >>> 4) | ((n & 0x0f0f0f0f) << 4);
            n = ((n & 0xcccccccc) >>> 2) | ((n & 0x33333333) << 2);
            n = ((n & 0xaaaaaaaa) >>> 1) | ((n & 0x55555555) << 1);
            return n;
        }
    }

    // 测试链接：https://leetcode.com/problems/count-primes/
    public static class CountPrimes {

        public static int countPrimes(int n) {
            if (n < 3) {
                return 0;
            }
            // j已经不是素数了，f[j] = true;
            boolean[] f = new boolean[n];
            int count = n / 2; // 所有偶数都不要，还剩几个数
            // 跳过了1、2    3、5、7、
            for (int i = 3; i * i < n; i += 2) {
                if (f[i]) {
                    continue;
                }
                // 3 -> 3 * 3 = 9   3 * 5 = 15   3 * 7 = 21
                // 7 -> 7 * 7 = 49  7 * 9 = 63
                // 13 -> 13 * 13  13 * 15
                for (int j = i * i; j < n; j += 2 * i) {
                    if (!f[j]) {
                        --count;
                        f[j] = true;
                    }
                }
            }
            return count;
        }
    }

    // 给定一个数组arr，arr[i] = j，表示第i号试题的难度为j。给定一个非负数M。
    // 想出一张卷子，对于任何相邻的两道题目，前一题的难度不能超过后一题的难度+M。返回所有可能的卷子种数
    public static class ExaminationPaperWays {

        // 纯暴力方法，生成所有排列，一个一个验证
        public static int ways1(int[] arr, int m) {
            if (arr == null || arr.length == 0) {
                return 0;
            }
            return process(arr, 0, m);
        }

        public static int process(int[] arr, int index, int m) {
            if (index == arr.length) {
                for (int i = 1; i < index; i++) {
                    if (arr[i - 1] > arr[i] + m) {
                        return 0;
                    }
                }
                return 1;
            }
            int ans = 0;
            for (int i = index; i < arr.length; i++) {
                swap(arr, index, i);
                ans += process(arr, index + 1, m);
                swap(arr, index, i);
            }
            return ans;
        }

        public static void swap(int[] arr, int i, int j) {
            int tmp = arr[i];
            arr[i] = arr[j];
            arr[j] = tmp;
        }

        // 时间复杂度O(N * logN)
        // 从左往右的动态规划 + 范围上二分
        public static int ways2(int[] arr, int m) {
            if (arr == null || arr.length == 0) {
                return 0;
            }
            Arrays.sort(arr);
            int all = 1;
            for (int i = 1; i < arr.length; i++) {
                all = all * (num(arr, i - 1, arr[i] - m) + 1);
            }
            return all;
        }

        // arr[0..r]上返回>=t的数有几个, 二分的方法
        // 找到 >=t 最左的位置a, 然后返回r - a + 1就是个数
        public static int num(int[] arr, int r, int t) {
            int i = 0;
            int j = r;
            int m = 0;
            int a = r + 1;
            while (i <= j) {
                m = (i + j) / 2;
                if (arr[m] >= t) {
                    a = m;
                    j = m - 1;
                } else {
                    i = m + 1;
                }
            }
            return r - a + 1;
        }

        // 时间复杂度O(N * logV)
        // 从左往右的动态规划 + IndexTree
        public static int ways3(int[] arr, int m) {
            if (arr == null || arr.length == 0) {
                return 0;
            }
            int max = Integer.MIN_VALUE;
            int min = Integer.MAX_VALUE;
            for (int num : arr) {
                max = Math.max(max, num);
                min = Math.min(min, num);
            }
            IndexTree indexTree = new IndexTree(max - min + 2);
            Arrays.sort(arr);
            int a = 0;
            int b = 0;
            int all = 1;
            indexTree.add(arr[0] - min + 1, 1);
            for (int i = 1; i < arr.length; i++) {
                a = arr[i] - min + 1;
                b = i - (a - m - 1 >= 1 ? indexTree.sum(a - m - 1) : 0);
                all = all * (b + 1);
                indexTree.add(a, 1);
            }
            return all;
        }

        // 注意开始下标是1，不是0
        public static class IndexTree {

            private int[] tree;
            private int N;

            public IndexTree(int size) {
                N = size;
                tree = new int[N + 1];
            }

            public int sum(int index) {
                int ret = 0;
                while (index > 0) {
                    ret += tree[index];
                    index -= index & -index;
                }
                return ret;
            }

            public void add(int index, int d) {
                while (index <= N) {
                    tree[index] += d;
                    index += index & -index;
                }
            }
        }
    }

    public static class HouseRobberII {

        // arr 长度大于等于1
        // 测试链接：https://leetcode.com/problems/house-robber/
        public static int pickMaxSum(int[] arr) {
            int n = arr.length;
            // dp[i] : arr[0..i]范围上，随意选择，但是，任何两数不能相邻。得到的最大累加和是多少？
            int[] dp = new int[n];
            dp[0] = arr[0];
            dp[1] = Math.max(arr[0], arr[1]);
            for (int i = 2; i < n; i++) {
                int p1 = arr[i];
                int p2 = dp[i - 1];
                int p3 = arr[i] + dp[i - 2];
                dp[i] = Math.max(p1, Math.max(p2, p3));
            }
            return dp[n - 1];
        }

        // 测试链接：https://leetcode.com/problems/house-robber-ii/
        public static int rob(int[] nums) {
            if (nums == null || nums.length == 0) {
                return 0;
            }
            if (nums.length == 1) {
                return nums[0];
            }
            if (nums.length == 2) {
                return Math.max(nums[0], nums[1]);
            }
            int pre2 = nums[0];
            int pre1 = Math.max(nums[0], nums[1]);
            for (int i = 2; i < nums.length - 1; i++) {
                int tmp = Math.max(pre1, nums[i] + pre2);
                pre2 = pre1;
                pre1 = tmp;
            }
            int ans1 = pre1;
            pre2 = nums[1];
            pre1 = Math.max(nums[1], nums[2]);
            for (int i = 3; i < nums.length; i++) {
                int tmp = Math.max(pre1, nums[i] + pre2);
                pre2 = pre1;
                pre1 = tmp;
            }
            int ans2 = pre1;
            return Math.max(ans1, ans2);
        }
    }

    // 测试链接：https://leetcode.com/problems/house-robber-iii/
    public static class HouseRobberIII {

        public static class TreeNode {
            public int val;
            public TreeNode left;
            public TreeNode right;
        }

        public static int rob(TreeNode root) {
            Info info = process(root);
            return Math.max(info.no, info.yes);
        }

        public static class Info {
            public int no;
            public int yes;

            public Info(int n, int y) {
                no = n;
                yes = y;
            }
        }

        public static Info process(TreeNode x) {
            if (x == null) {
                return new Info(0, 0);
            }
            Info leftInfo = process(x.left);
            Info rightInfo = process(x.right);
            int no = Math.max(leftInfo.no, leftInfo.yes) + Math.max(rightInfo.no, rightInfo.yes);
            int yes = x.val + leftInfo.no + rightInfo.no;
            return new Info(no, yes);
        }
    }

    // 测试链接：https://leetcode.com/problems/find-the-celebrity/
    public static class FindTheCelebrity {

        // 提交时不要提交这个函数，因为默认系统会给你这个函数
        // knows方法，自己不认识自己
        public static boolean knows(int x, int i) {
            return true;
        }

        // 只提交下面的方法 0 ~ n-1
        public int findCelebrity(int n) {
            // 谁可能成为明星，谁就是cand
            int cand = 0;
            for (int i = 0; i < n; ++i) {
                if (knows(cand, i)) {
                    cand = i;
                }
            }
            // cand是什么？唯一可能是明星的人！
            // 下一步就是验证，它到底是不是明星
            // 1) cand是不是不认识所有的人 cand...（右侧cand都不认识）
            // 所以，只用验证 ....cand的左侧即可
            for (int i = 0; i < cand; ++i) {
                if (knows(cand, i)) {
                    return -1;
                }
            }
            // 2) 是不是所有的人都认识cand
            for (int i = 0; i < n; ++i) {
                if (!knows(i, cand)) {
                    return -1;
                }
            }
            return cand;
        }
    }

    // 测试链接：https://leetcode.com/problems/perfect-squares/
    public static class PerfectSquares {

        // 暴力解
        public static int numSquares1(int n) {
            int res = n, num = 2;
            while (num * num <= n) {
                int a = n / (num * num), b = n % (num * num);
                res = Math.min(res, a + numSquares1(b));
                num++;
            }
            return res;
        }

        // 1 : 1, 4, 9, 16, 25, 36, ...
        // 4 : 7, 15, 23, 28, 31, 39, 47, 55, 60, 63, 71, ...
        // 规律解
        // 规律一：个数不超过4
        // 规律二：出现1个的时候，显而易见
        // 规律三：任何数 % 8 == 7，一定是4个
        // 规律四：任何数消去4的因子之后，剩下rest，rest % 8 == 7，一定是4个
        public static int numSquares2(int n) {
            int rest = n;
            while (rest % 4 == 0) {
                rest /= 4;
            }
            if (rest % 8 == 7) {
                return 4;
            }
            int f = (int) Math.sqrt(n);
            if (f * f == n) {
                return 1;
            }
            for (int first = 1; first * first <= n; first++) {
                int second = (int) Math.sqrt(n - first * first);
                if (first * first + second * second == n) {
                    return 2;
                }
            }
            return 3;
        }

        // 数学解
        // 1）四平方和定理
        // 2）任何数消掉4的因子，结论不变
        public static int numSquares3(int n) {
            while (n % 4 == 0) {
                n /= 4;
            }
            if (n % 8 == 7) {
                return 4;
            }
            for (int a = 0; a * a <= n; ++a) {
                // a * a +  b * b = n
                int b = (int) Math.sqrt(n - a * a);
                if (a * a + b * b == n) {
                    return (a > 0 && b > 0) ? 2 : 1;
                }
            }
            return 3;
        }
    }

    // 有关这个游戏更有意思、更完整的内容：
    // https://www.bilibili.com/video/BV1rJ411n7ri
    // 测试链接：https://leetcode.com/problems/game-of-life/
    public static class GameOfLife {

        public static void gameOfLife(int[][] board) {
            int N = board.length;
            int M = board[0].length;
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < M; j++) {
                    int neighbors = neighbors(board, i, j);
                    if (neighbors == 3 || (board[i][j] == 1 && neighbors == 2)) {
                        board[i][j] |= 2;
                    }
                }
            }
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < M; j++) {
                    board[i][j] >>= 1;
                }
            }
        }

        // b[i][j] 这个位置的数，周围有几个1
        public static int neighbors(int[][] b, int i, int j) {
            return f(b, i - 1, j - 1)
                    + f(b, i - 1, j)
                    + f(b, i - 1, j + 1)
                    + f(b, i, j - 1)
                    + f(b, i, j + 1)
                    + f(b, i + 1, j - 1)
                    + f(b, i + 1, j)
                    + f(b, i + 1, j + 1);
        }

        // b[i][j] 上面有1，就返回1，上面不是1，就返回0
        public static int f(int[][] b, int i, int j) {
            return (i >= 0 && i < b.length && j >= 0 && j < b[0].length && (b[i][j] & 1) == 1) ? 1 : 0;
        }
    }

    // 测试链接：https://leetcode.com/problems/power-of-three/
    public static class PowerOfThree {

        // 如果一个数字是3的某次幂，那么这个数一定只含有3这个质数因子
        // 1162261467是int型范围内，最大的3的幂，它是3的19次方
        // 这个1162261467只含有3这个质数因子，如果n也是只含有3这个质数因子，那么
        // 1162261467 % n == 0
        // 反之如果1162261467 % n != 0 说明n一定含有其他因子
        public static boolean isPowerOfThree(int n) {
            return (n > 0 && 1162261467 % n == 0);
        }
    }

    // 测试链接：https://leetcode.com/problems/longest-substring-with-at-most-k-distinct-characters/
    // 求给一个字符串中最长子串，要求子串里面的字符种类不能超过k个，超过就是不达标，返回达标中最长的子串的长度
    public static class LongestSubstringWithAtMostKDistinctCharacters {

        public static int lengthOfLongestSubstringKDistinct(String s, int k) {
            if (s == null || s.length() == 0 || k < 1) {
                return 0;
            }
            char[] str = s.toCharArray();
            int N = str.length;
            int[] count = new int[256];
            int diff = 0;
            int R = 0;
            int ans = 0;
            for (int i = 0; i < N; i++) {
                // R 窗口的右边界
                while (R < N && (diff < k || (diff == k && count[str[R]] > 0))) {
                    diff += count[str[R]] == 0 ? 1 : 0;
                    count[str[R++]]++;
                }
                // R 来到违规的第一个位置
                ans = Math.max(ans, R - i);
                diff -= count[str[i]] == 1 ? 1 : 0;
                count[str[i]]--;
            }
            return ans;
        }
    }

    // 测试链接 : https://leetcode.com/problems/insert-delete-getrandom-o1/
    public static class InsertDeleteGetRandom {
        public static class RandomizedSet {

            private HashMap<Integer, Integer> keyIndexMap;
            private HashMap<Integer, Integer> indexKeyMap;
            private int size;

            public RandomizedSet() {
                keyIndexMap = new HashMap<Integer, Integer>();
                indexKeyMap = new HashMap<Integer, Integer>();
                size = 0;
            }

            public boolean insert(int val) {
                if (!keyIndexMap.containsKey(val)) {
                    keyIndexMap.put(val, size);
                    indexKeyMap.put(size++, val);
                    return true;
                }
                return false;
            }

            public boolean remove(int val) {
                // 整体思路是取出两个哈希表的最后一个元素，拿这个元素是覆盖要删除的元素，最后再删除掉多余的元素
                if (keyIndexMap.containsKey(val)) {
                    int deleteIndex = keyIndexMap.get(val);
                    int lastIndex = --size;
                    int lastKey = indexKeyMap.get(lastIndex);
                    keyIndexMap.put(lastKey, deleteIndex);
                    indexKeyMap.put(deleteIndex, lastKey);
                    keyIndexMap.remove(val);
                    indexKeyMap.remove(lastIndex);
                    return true;
                }
                return false;
            }

            public int getRandom() {
                if (size == 0) {
                    return -1;
                }
                int randomIndex = (int) (Math.random() * size);
                return indexKeyMap.get(randomIndex);
            }
        }
    }

    // 给定一个长度len，表示一共有几位
    // 所有字符都是小写(a~z)，可以生成长度为1，长度为2，
    // 长度为3...长度为len的所有字符串
    // 如果把所有字符串根据字典序排序，每个字符串都有所在的位置。
    // 给定一个字符串str，给定len，请返回str是总序列中的第几个
    // 比如len = 4，字典序的前几个字符串为:
    // a aa aaa aaaa aaab ... aaaz ... azzz b ba baa baaa ... bzzz c ...
    // a是这个序列中的第1个，bzzz是这个序列中的第36558个
    public static class StringKth {
        // 思路：
        // cdb，总共长度为7，请问cdb是第几个？
        // 第一位c :
        // 以a开头，剩下长度为(0~6)的所有可能性有几个
        // +
        // 以b开头，剩下长度为(0~6)的所有可能性有几个
        // +
        // 以c开头，剩下长度为(0)的所有可能性有几个
        // 第二位d :
        // +
        // 以ca开头的情况下，剩下长度为(0~5)的所有可能性有几个
        // +
        // 以cb开头的情况下，剩下长度为(0~5)的所有可能性有几个
        // +
        // 以cc开头的情况下，剩下长度为(0~5)的所有可能性有几个
        // +
        // 以cd开头的情况下，剩下长度为(0)的所有可能性有几个
        // 第三位b
        // +
        // 以cda开头的情况下，剩下长度为(0~4)的所有可能性有几个
        // +
        // 以cdb开头的情况下，剩下长度为(0)的所有可能性有几个
        public static int kth(String s, int len) {
            if (s == null || s.length() == 0 || s.length() > len) {
                return -1;
            }
            char[] num = s.toCharArray();
            int ans = 0;
            for (int i = 0, rest = len - 1; i < num.length; i++, rest--) {
                ans += (num[i] - 'a') * f(rest) + 1;
            }
            return ans;
        }

        // 不管以什么开头，剩下长度为(0~len)的所有可能性有几个
        public static int f(int len) {
            int ans = 1;
            for (int i = 1, base = 26; i <= len; i++, base *= 26) {
                ans += base;
            }
            return ans;
        }

        // 为了测试
        public static List<String> all(int len) {
            List<String> ans = new ArrayList<>();
            for (int i = 1; i <= len; i++) {
                char[] path = new char[i];
                process(path, 0, ans);
            }
            return ans;
        }

        // 为了测试
        public static void process(char[] path, int index, List<String> ans) {
            if (index == path.length) {
                ans.add(String.valueOf(path));
            } else {
                for (char c = 'a'; c <= 'z'; c++) {
                    path[index] = c;
                    process(path, index + 1, ans);
                }
            }
        }

        public static void main(String[] args) {
            int len = 4;
            // 暴力方法得到所有字符串
            List<String> ans = all(len);
            // 根据字典序排序，所有字符串都在其中
            ans.sort((a, b) -> a.compareTo(b));

            String test = "bzzz";
            // 根据我们的方法算出test是第几个？
            // 注意我们算出的第几个，是从1开始的
            // 而下标是从0开始的，所以变成index，还需要-1
            int index = kth(test, len) - 1;
            // 验证
            System.out.println(ans.get(index));
        }
    }

    // 来自小红书
    // [0,4,7] ： 0表示这里石头没有颜色，如果变红代价是4，如果变蓝代价是7
    // [1,X,X] ： 1表示这里石头已经是红，而且不能改颜色，所以后两个数X无意义
    // [2,X,X] ： 2表示这里石头已经是蓝，而且不能改颜色，所以后两个数X无意义
    // 颜色只可能是0、1、2，代价一定>=0
    // 给你一批这样的小数组，要求最后必须所有石头都有颜色，且红色和蓝色一样多，返回最小代价
    // 如果怎么都无法做到所有石头都有颜色、且红色和蓝色一样多，返回-1
    public static class MagicStone {

        public static int minCost(int[][] stones) {
            int n = stones.length;
            if ((n & 1) != 0) {
                return -1;
            }
            Arrays.sort(stones, (a, b) -> a[0] == 0 && b[0] == 0 ? (b[1] - b[2] - a[1] + a[2]) : (a[0] - b[0]));
            int zero = 0;
            int red = 0;
            int blue = 0;
            int cost = 0;
            for (int i = 0; i < n; i++) {
                if (stones[i][0] == 0) {
                    zero++;
                    cost += stones[i][1];
                } else if (stones[i][0] == 1) {
                    red++;
                } else {
                    blue++;
                }
            }
            if (red > (n >> 1) || blue > (n >> 1)) {
                return -1;
            }
            blue = zero - ((n >> 1) - red);
            for (int i = 0; i < blue; i++) {
                cost += stones[i][2] - stones[i][1];
            }
            return cost;
        }
    }

    // 来自京东
    // 把一个01字符串切成多个部分，要求每一部分的0和1比例一样，同时要求尽可能多的划分
    // 比如 : 01010101
    // 01 01 01 01 这是一种切法，0和1比例为 1 : 1
    // 0101 0101 也是一种切法，0和1比例为 1 : 1
    // 两种切法都符合要求，但是那么尽可能多的划分为第一种切法，部分数为4
    // 比如 : 00001111
    // 只有一种切法就是00001111整体作为一块，那么尽可能多的划分，部分数为1
    // 给定一个01字符串str，假设长度为N，要求返回一个长度为N的数组ans
    // 其中ans[i] = str[0...i]这个前缀串，要求每一部分的0和1比例一样，同时要求尽可能多的划分下，部分数是多少
    // 输入: str = "010100001"
    // 输出: ans = [1, 1, 1, 2, 1, 2, 1, 1, 3]
    public static class Ratio01Split {
        // 001010010100...
        public static int[] split(int[] arr) {

            // key : 分子
            // value : 属于key的分母表, 每一个分母，及其 分子/分母 这个比例，多少个前缀拥有
            HashMap<Integer, HashMap<Integer, Integer>> pre = new HashMap<>();
            int n = arr.length;
            int[] ans = new int[n];
            int zero = 0; // 0出现的次数
            int one = 0; // 1出现的次数
            for (int i = 0; i < n; i++) {
                if (arr[i] == 0) {
                    zero++;
                } else {
                    one++;
                }
                if (zero == 0 || one == 0) {
                    ans[i] = i + 1;
                } else { // 0和1，都有数量 -> 最简分数
                    int gcd = gcd(zero, one);
                    int a = zero / gcd;
                    int b = one / gcd;
                    // a / b 比例，之前有多少前缀拥有？ 3+1 4 5+1 6
                    if (!pre.containsKey(a)) {
                        pre.put(a, new HashMap<>());
                    }
                    if (!pre.get(a).containsKey(b)) {
                        pre.get(a).put(b, 1);
                    } else {
                        pre.get(a).put(b, pre.get(a).get(b) + 1);
                    }
                    ans[i] = pre.get(a).get(b);
                }
            }
            return ans;
        }

        public static int gcd(int m, int n) {
            return n == 0 ? m : gcd(n, m % n);
        }
    }

    // 来自腾讯
    // 给定一个数组arr，当拿走某个数a的时候，其他所有的数都+a
    // 请返回最终所有数都拿走的最大分数
    // 比如: [2,3,1]
    // 当拿走3时，获得3分，数组变成[5,4]
    // 当拿走5时，获得5分，数组变成[9]
    // 当拿走9时，获得9分，数组变成[]
    public static class PickAddMax {

        // 最优解
        public static int pick(int[] arr) {
            Arrays.sort(arr);
            int ans = 0;
            for (int i = arr.length - 1; i >= 0; i--) {
                ans = (ans << 1) + arr[i];
            }
            return ans;
        }

        // 纯暴力方法，为了测试
        public static int test(int[] arr) {
            if (arr.length == 1) {
                return arr[0];
            }
            int ans = 0;
            for (int i = 0; i < arr.length; i++) {
                int[] rest = removeAddOthers(arr, i);
                ans = Math.max(ans, arr[i] + test(rest));
            }
            return ans;
        }

        // 为了测试
        public static int[] removeAddOthers(int[] arr, int i) {
            int[] rest = new int[arr.length - 1];
            int ri = 0;
            for (int j = 0; j < i; j++) {
                rest[ri++] = arr[j] + arr[i];
            }
            for (int j = i + 1; j < arr.length; j++) {
                rest[ri++] = arr[j] + arr[i];
            }
            return rest;
        }
    }

    // 来自腾讯
    // 给定一个字符串str，和一个正数k
    // 返回长度为k的所有子序列中，字典序最大的子序列
    public static class MaxKLenSequence {

        public static String maxString(String s, int k) {
            if (k <= 0 || s.length() < k) {
                return "";
            }
            char[] str = s.toCharArray();
            int n = str.length;
            char[] stack = new char[n];
            int size = 0;
            for (int i = 0; i < n; i++) {
                while (size > 0 && stack[size - 1] < str[i] && size + n - i > k) {
                    size--;
                }
                if (size + n - i == k) {
                    return String.valueOf(stack, 0, size) + s.substring(i);
                }
                stack[size++] = str[i];
            }
            return String.valueOf(stack, 0, k);
        }

        // 暴力方法，为了测试
        public static String test(String str, int k) {
            if (k <= 0 || str.length() < k) {
                return "";
            }
            TreeSet<String> ans = new TreeSet<>();
            process(0, 0, str.toCharArray(), new char[k], ans);
            return ans.last();
        }

        // 为了测试
        public static void process(int si, int pi, char[] str, char[] path, TreeSet<String> ans) {
            if (si == str.length) {
                if (pi == path.length) {
                    ans.add(String.valueOf(path));
                }
            } else {
                process(si + 1, pi, str, path, ans);
                if (pi < path.length) {
                    path[pi] = str[si];
                    process(si + 1, pi + 1, str, path, ans);
                }
            }
        }
    }

    // 来自腾讯
    // 给定一个只由0和1组成的字符串S，假设下标从1开始，规定i位置的字符价值V[i]计算方式如下 :
    // 1) i == 1时，V[i] = 1
    // 2) i > 1时，如果S[i] != S[i-1]，V[i] = 1
    // 3) i > 1时，如果S[i] == S[i-1]，V[i] = V[i-1] + 1
    // 你可以随意删除S中的字符，返回整个S的最大价值
    // 字符串长度<=5000
    public static class ZeroOneAddValue {

        public static int max1(String s) {
            if (s == null || s.length() == 0) {
                return 0;
            }
            char[] str = s.toCharArray();
            int[] arr = new int[str.length];
            for (int i = 0; i < arr.length; i++) {
                arr[i] = str[i] == '0' ? 0 : 1;
            }
            return process1(arr, 0, 0, 0);
        }

        // 递归含义 :
        // 目前在arr[index...]上做选择, str[index...]的左边，最近的数字是lastNum
        // 并且lastNum所带的价值，已经拉高到baseValue
        // 返回在str[index...]上做选择，最终获得的最大价值
        // index -> 0 ~ 4999
        // lastNum -> 0 or 1
        // baseValue -> 1 ~ 5000
        // 5000 * 2 * 5000 -> 5 * 10^7(过!)
        public static int process1(int[] arr, int index, int lastNum, int baseValue) {
            if (index == arr.length) {
                return 0;
            }
            int curValue = lastNum == arr[index] ? (baseValue + 1) : 1;
            // 当前index位置的字符保留
            int next1 = process1(arr, index + 1, arr[index], curValue);
            // 当前index位置的字符不保留
            int next2 = process1(arr, index + 1, lastNum, baseValue);
            return Math.max(curValue + next1, next2);
        }
    }

    // 真实笔试，忘了哪个公司，但是绝对大厂
    // 一个子序列的消除规则如下:
    // 1) 在某一个子序列中，如果'1'的左边有'0'，那么这两个字符->"01"可以消除
    // 2) 在某一个子序列中，如果'3'的左边有'2'，那么这两个字符->"23"可以消除
    // 3) 当这个子序列的某个部分消除之后，认为其他字符会自动贴在一起，可以继续寻找消除的机会
    // 比如，某个子序列"0231"，先消除掉"23"，那么剩下的字符贴在一起变成"01"，继续消除就没有字符了
    // 如果某个子序列通过最优良的方式，可以都消掉，那么这样的子序列叫做“全消子序列”
    // 一个只由'0'、'1'、'2'、'3'四种字符组成的字符串str，可以生成很多子序列，返回“全消子序列”的最大长度
    // 字符串str长度 <= 200
    public static class ZeroOneTwoThreeDisappear {

        // str[L...R]上，都能消掉的子序列，最长是多少？
        public static int f(char[] str, int L, int R) {
            if (L >= R) {
                return 0;
            }
            if (L == R - 1) {
                return (str[L] == '0' && str[R] == '1') || (str[L] == '2' && str[R] == '3') ? 2 : 0;
            }
            // L...R 有若干个字符 > 2
            // str[L...R]上，都能消掉的子序列，最长是多少？
            // 可能性1，能消掉的子序列完全不考虑str[L]，最长是多少？
            int p1 = f(str, L + 1, R);
            if (str[L] == '1' || str[L] == '3') {
                return p1;
            }
            // str[L] =='0' 或者 '2'
            // '0' 去找 '1'
            // '2' 去找 '3'
            char find = str[L] == '0' ? '1' : '3';
            int p2 = 0;
            // L() ......
            for (int i = L + 1; i <= R; i++) {
                // L(0) ..... i(1) i+1....R
                if (str[i] == find) {
                    p2 = Math.max(p2, f(str, L + 1, i - 1) + 2 + f(str, i + 1, R));
                }
            }
            return Math.max(p1, p2);
        }

        public static int maxDisappear(String str) {
            if (str == null || str.length() == 0) {
                return 0;
            }
            return disappear(str.toCharArray(), 0, str.length() - 1);
        }

        // s[l..r]范围上，如题目所说的方式，最长的都能消掉的子序列长度
        public static int disappear(char[] s, int l, int r) {
            if (l >= r) {
                return 0;
            }
            if (l == r - 1) {
                return (s[l] == '0' && s[r] == '1') || (s[l] == '2' && s[r] == '3') ? 2 : 0;
            }
            int p1 = disappear(s, l + 1, r);
            if (s[l] == '1' || s[l] == '3') {
                return p1;
            }
            int p2 = 0;
            char find = s[l] == '0' ? '1' : '3';
            for (int i = l + 1; i <= r; i++) {
                if (s[i] == find) {
                    p2 = Math.max(p2, disappear(s, l + 1, i - 1) + 2 + disappear(s, i + 1, r));
                }
            }
            return Math.max(p1, p2);
        }
    }

    // 来自小红书
    // 一个无序数组长度为n，所有数字都不一样，并且值都在[0...n-1]范围上
    // 返回让这个无序数组变成有序数组的最小交换次数
    public static class MinSwapTimes {

        // 把数组进行离散化，方便进行下标循环怼
        public static void change(int[] arr) {
            int[] copy = Arrays.copyOf(arr, arr.length);
            Arrays.sort(copy);
            HashMap<Integer, Integer> map = new HashMap<>();
            for (int i = 0; i < copy.length; i++) {
                map.put(copy[i], i);
            }
            for (int i = 0; i < arr.length; i++) {
                arr[i] = map.get(arr[i]);
            }
            minSwap2(arr);
        }

        // 纯暴力，arr长度大一点都会超时
        // 但是绝对正确
        public static int minSwap1(int[] arr) {
            return process1(arr, 0);
        }

        // 让arr变有序，最少的交换次数是多少！返回
        // times, 之前已经做了多少次交换
        public static int process1(int[] arr, int times) {
            boolean sorted = true;
            for (int i = 1; i < arr.length; i++) {
                if (arr[i - 1] > arr[i]) {
                    sorted = false;
                    break;
                }
            }
            if (sorted) {
                return times;
            }
            // 数组现在是无序的状态！
            if (times >= arr.length - 1) {
                return Integer.MAX_VALUE;
            }
            int ans = Integer.MAX_VALUE;
            for (int i = 0; i < arr.length; i++) {
                for (int j = i + 1; j < arr.length; j++) {
                    swap(arr, i, j);
                    ans = Math.min(ans, process1(arr, times + 1));
                    swap(arr, i, j);
                }
            }
            return ans;
        }

        public static void swap(int[] arr, int i, int j) {
            int tmp = arr[i];
            arr[i] = arr[j];
            arr[j] = tmp;
        }

        // 已知arr中，只有0~n-1这些值，并且都出现1次
        public static int minSwap2(int[] arr) {
            int ans = 0;
            for (int i = 0; i < arr.length; i++) {
                while (i != arr[i]) {
                    swap(arr, i, arr[i]);
                    ans++;
                }
            }
            return ans;
        }
    }

    // 测试链接：https://leetcode.com/problems/next-permutation/
    public static class NextPermutation {

        public static void nextPermutation(int[] nums) {
            int N = nums.length;
            // 从右往左第一次降序的位置
            int firstLess = -1;
            for (int i = N - 2; i >= 0; i--) {
                if (nums[i] < nums[i + 1]) {
                    firstLess = i;
                    break;
                }
            }
            if (firstLess < 0) {
                reverse(nums, 0, N - 1);
            } else {
                int rightClosestMore = -1;
                // 找最靠右的、同时比nums[firstLess]大的数，位置在哪
                // 这里其实也可以用二分优化，但是这种优化无关紧要了
                for (int i = N - 1; i > firstLess; i--) {
                    if (nums[i] > nums[firstLess]) {
                        rightClosestMore = i;
                        break;
                    }
                }
                swap(nums, firstLess, rightClosestMore);
                reverse(nums, firstLess + 1, N - 1);
            }
        }

        public static void reverse(int[] nums, int L, int R) {
            while (L < R) {
                swap(nums, L++, R--);
            }
        }

        public static void swap(int[] nums, int i, int j) {
            int tmp = nums[i];
            nums[i] = nums[j];
            nums[j] = tmp;
        }
    }

    // 测试链接；https://leetcode.com/problems/best-meeting-point/
    public static class BestMeetingPoint {

        public static int minTotalDistance(int[][] grid) {
            int N = grid.length;
            int M = grid[0].length;
            int[] iOnes = new int[N];
            int[] jOnes = new int[M];
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < M; j++) {
                    if (grid[i][j] == 1) {
                        iOnes[i]++;
                        jOnes[j]++;
                    }
                }
            }
            int total = 0;
            int i = 0;
            int j = N - 1;
            int iRest = 0;
            int jRest = 0;
            while (i < j) {
                if (iOnes[i] + iRest <= iOnes[j] + jRest) {
                    total += iOnes[i] + iRest;
                    iRest += iOnes[i++];
                } else {
                    total += iOnes[j] + jRest;
                    jRest += iOnes[j--];
                }
            }
            i = 0;
            j = M - 1;
            iRest = 0;
            jRest = 0;
            while (i < j) {
                if (jOnes[i] + iRest <= jOnes[j] + jRest) {
                    total += jOnes[i] + iRest;
                    iRest += jOnes[i++];
                } else {
                    total += jOnes[j] + jRest;
                    jRest += jOnes[j--];
                }
            }
            return total;
        }
    }

    // 来自微软面试
    // 给定一个正数数组arr长度为n、正数x、正数y
    // 你的目标是让arr整体的累加和<=0
    // 你可以对数组中的数num执行以下三种操作中的一种，且每个数最多能执行一次操作 :
    // 1）不变
    // 2）可以选择让num变成0，承担x的代价
    // 3）可以选择让num变成-num，承担y的代价
    // 返回你达到目标的最小代价
    public static class SumNoPositiveMinCost {

        // 动态规划
        public static int minOpStep1(int[] arr, int x, int y) {
            int sum = 0;
            for (int num : arr) {
                sum += num;
            }
            return process1(arr, x, y, 0, sum);
        }

        // arr[i...]自由选择，每个位置的数可以执行三种操作中的一种！
        // 执行变0的操作，x操作，代价 -> x
        // 执行变相反数的操作，y操作，代价 -> y
        // 还剩下sum这么多累加和，需要去搞定！
        // 返回搞定了sum，最低代价是多少？
        public static int process1(int[] arr, int x, int y, int i, int sum) {
            if (sum <= 0) {
                return 0;
            }
            // sum > 0 没搞定
            if (i == arr.length) {
                return Integer.MAX_VALUE;
            }
            // 第一选择，什么也不干！
            int p1 = process1(arr, x, y, i + 1, sum);
            // 第二选择，执行x的操作，变0 x + 后续
            int p2 = Integer.MAX_VALUE;
            int next2 = process1(arr, x, y, i + 1, sum - arr[i]);
            if (next2 != Integer.MAX_VALUE) {
                p2 = x + next2;
            }
            // 第三选择，执行y的操作，变相反数 x + 后续 7 -7 -14
            int p3 = Integer.MAX_VALUE;
            int next3 = process1(arr, x, y, i + 1, sum - (arr[i] << 1));
            if (next3 != Integer.MAX_VALUE) {
                p3 = y + next3;
            }
            return Math.min(p1, Math.min(p2, p3));
        }

        // 贪心（最优解）
        public static int minOpStep2(int[] arr, int x, int y) {
            Arrays.sort(arr); // 小 -> 大
            int n = arr.length;
            for (int l = 0, r = n - 1; l <= r; l++, r--) {
                int tmp = arr[l];
                arr[l] = arr[r];
                arr[r] = tmp;
            }
            // arr 大 -> 小
            if (x >= y) { // 没有任何必要执行x操作
                int sum = 0;
                for (int num : arr) {
                    sum += num;
                }
                int cost = 0;
                for (int i = 0; i < n && sum > 0; i++) {
                    sum -= arr[i] << 1;
                    cost += y;
                }
                return cost;
            } else {
                for (int i = n - 2; i >= 0; i--) {
                    arr[i] += arr[i + 1];
                }
                int benefit = 0;
                // 注意，可以不二分，用不回退的方式！
                // 执行Y操作的数，有0个的时候
                int left = mostLeft(arr, 0, benefit);
                int cost = left * x;
                for (int i = 0; i < n - 1; i++) {
                    // 0..i 这些数，都执行Y
                    benefit += arr[i] - arr[i + 1];
                    left = mostLeft(arr, i + 1, benefit);
                    cost = Math.min(cost, (i + 1) * y + (left - i - 1) * x);
                }
                return cost;
            }
        }

        // arr是后缀和数组， arr[l...]中找到值<=v的最左位置
        public static int mostLeft(int[] arr, int l, int v) {
            int r = arr.length - 1;
            int m = 0;
            int ans = arr.length;
            while (l <= r) {
                m = (l + r) / 2;
                if (arr[m] <= v) {
                    ans = m;
                    r = m - 1;
                } else {
                    l = m + 1;
                }
            }
            return ans;
        }

        // 为了测试
        public static int[] randomArray(int len, int v) {
            int[] arr = new int[len];
            for (int i = 0; i < len; i++) {
                arr[i] = (int) (Math.random() * v) + 1;
            }
            return arr;
        }

        // 为了测试
        public static int[] copyArray(int[] arr) {
            int[] ans = new int[arr.length];
            for (int i = 0; i < arr.length; i++) {
                ans[i] = arr[i];
            }
            return ans;
        }

        // 为了测试
        public static void main(String[] args) {
            int n = 12;
            int v = 20;
            int c = 10;
            int testTime = 10000;
            System.out.println("测试开始");
            for (int i = 0; i < testTime; i++) {
                int len = (int) (Math.random() * n);
                int[] arr = randomArray(len, v);
                int[] arr1 = copyArray(arr);
                int[] arr2 = copyArray(arr);
                int[] arr3 = copyArray(arr);
                int x = (int) (Math.random() * c);
                int y = (int) (Math.random() * c);
                int ans1 = minOpStep1(arr1, x, y);
                int ans2 = minOpStep2(arr2, x, y);
                if (ans1 != ans2) {
                    System.out.println("出错了!");
                }
            }
            System.out.println("测试结束");

        }
    }

    // 测试链接：https://leetcode.cn/problems/subarrays-with-k-different-integers/
    public static class SubArraysWithKDifferentIntegers {

        // nums 数组，题目规定，nums中的数字，不会超过nums的长度
        // [ ]长度为5，0~5
        public static int subArraysWithKDistinct1(int[] nums, int k) {
            int n = nums.length;
            // k-1种数的窗口词频统计
            int[] lessCounts = new int[n + 1];
            // k种数的窗口词频统计
            int[] equalCounts = new int[n + 1];
            int lessLeft = 0;
            int equalLeft = 0;
            int lessKinds = 0;
            int equalKinds = 0;
            int ans = 0;
            for (int r = 0; r < n; r++) {
                // 当前刚来到r位置！
                if (lessCounts[nums[r]] == 0) {
                    lessKinds++;
                }
                if (equalCounts[nums[r]] == 0) {
                    equalKinds++;
                }
                lessCounts[nums[r]]++;
                equalCounts[nums[r]]++;
                while (lessKinds == k) {
                    if (lessCounts[nums[lessLeft]] == 1) {
                        lessKinds--;
                    }
                    lessCounts[nums[lessLeft++]]--;
                }
                while (equalKinds > k) {
                    if (equalCounts[nums[equalLeft]] == 1) {
                        equalKinds--;
                    }
                    equalCounts[nums[equalLeft++]]--;
                }
                ans += lessLeft - equalLeft;
            }
            return ans;
        }

        // 可以收集子数组中出现的种类个数小于等于k 记为a个
        // 再收集子数组中出现的种类个数小于等于k - 1 记为b个
        // 那么子数组中出现种类个数等于k的个数就是 a - b
        public static int subArraysWithKDistinct2(int[] arr, int k) {
            return numsMostK(arr, k) - numsMostK(arr, k - 1);
        }

        public static int numsMostK(int[] arr, int k) {
            int i = 0, res = 0;
            HashMap<Integer, Integer> count = new HashMap<>();
            for (int j = 0; j < arr.length; ++j) {
                if (count.getOrDefault(arr[j], 0) == 0) {
                    k--;
                }
                count.put(arr[j], count.getOrDefault(arr[j], 0) + 1);
                while (k < 0) {
                    count.put(arr[i], count.get(arr[i]) - 1);
                    if (count.get(arr[i]) == 0) {
                        k++;
                    }
                    i++;
                }
                res += j - i + 1;
            }
            return res;
        }
    }

    // 来自京东笔试
    // 小明手中有n块积木，并且小明知道每块积木的重量。现在小明希望将这些积木堆起来
    // 要求是任意一块积木如果想堆在另一块积木上面，那么要求：
    // 1) 上面的积木重量不能小于下面的积木重量
    // 2) 上面积木的重量减去下面积木的重量不能超过x
    // 3) 每堆中最下面的积木没有重量要求
    // 现在小明有一个机会，除了这n块积木，还可以获得k块任意重量的积木。
    // 小明希望将积木堆在一起，同时希望积木堆的数量越少越好，你能帮他找到最好的方案么？
    // 输入描述:
    // 第一行三个整数n,k,x，1<=n<=200000，0<=x,k<=1000000000
    // 第二行n个整数，表示积木的重量，任意整数范围都在[1,1000000000]
    // 样例输出：
    // 13 1 38
    // 20 20 80 70 70 70 420 5 1 5 1 60 90
    // 1 1 5 5 20 20 60 70 70 70 80 90 420 -> 只有1块魔法积木，x = 38
    // 输出：2
    // 解释：
    // 两堆分别是
    // 1 1 5 5 20 20 (50) 60 70 70 70 80 90
    // 420
    // 其中x是一个任意重量的积木，夹在20和60之间可以让积木继续往上搭
    public static class SplitBuildingBlock {

        // 这是启发解
        // arr是从小到大排序的，x是限制，固定参数
        // 当前来到i位置，积木重量arr[i]
        // 潜台词 : 当前i位置的积木在一个堆里，堆的开头在哪？之前已经决定了
        // i i+1 该在一起 or 该用魔法积木弥合 or 该分家
        // 返回值：arr[i....]最少能分几个堆？
        public static int zuo(int[] arr, int x, int i, int r) {
            if (i == arr.length - 1) {
                return 1;
            }
            // i没到最后一个数
            if (arr[i + 1] - arr[i] <= x) { // 一定贴在一起
                return zuo(arr, x, i + 1, r);
            } else { // 弥合！分家
                // 分家
                int p1 = 1 + zuo(arr, x, i + 1, r);
                // 弥合
                int p2 = Integer.MAX_VALUE;
                int need = (arr[i + 1] - arr[i] - 1) / x;
                if (r >= need) {
                    p2 = zuo(arr, x, i + 1, r - need);
                }
                return Math.min(p1, p2);
            }
        }

        // 这是最优解
        // arr里装着所有积木的重量
        // k是魔法积木的数量，每一块魔法积木都能变成任何重量
        // x差值，后 - 前 <= x
        public static int minSplit(int[] arr, int k, int x) {
            Arrays.sort(arr);
            int n = arr.length;
            int[] needs = new int[n];
            int size = 0;
            int splits = 1;
            for (int i = 1; i < n; i++) {
                if (arr[i] - arr[i - 1] > x) {
                    needs[size++] = arr[i] - arr[i - 1];
                    splits++;
                }
            }
            if (splits == 1 || x == 0 || k == 0) {
                return splits;
            }
            // 试图去利用魔法积木，弥合堆！
            Arrays.sort(needs, 0, size);
            for (int i = 0; i < size; i++) {
                int need = (needs[i] - 1) / x;
                if (k >= need) {
                    splits--;
                    k -= need;
                } else {
                    break;
                }
            }
            return splits;
        }
    }

    // 来自学员问题
    // 比如{ 5, 3, 1, 4 }
    // 全部数字对是：(5,3)、(5,1)、(5,4)、(3,1)、(3,4)、(1,4)
    // 数字对的差值绝对值： 2、4、1、2、1、3
    // 差值绝对值排序后：1、1、2、2、3、4
    // 给定一个数组arr，和一个正数k
    // 返回arr中所有数字对差值的绝对值，第k小的是多少
    // arr = { 5, 3, 1, 4 }, k = 4
    // 返回2
    public static class MinKthPairMinusABS {

        // 暴力解，生成所有数字对差值绝对值，排序，拿出第k个，k从1开始
        public static int kthABS1(int[] arr, int k) {
            int n = arr.length;
            int m = ((n - 1) * n) >> 1;
            if (m == 0 || k < 1 || k > m) {
                return -1;
            }
            int[] abs = new int[m];
            int size = 0;
            for (int i = 0; i < n; i++) {
                for (int j = i + 1; j < n; j++) {
                    abs[size++] = Math.abs(arr[i] - arr[j]);
                }
            }
            Arrays.sort(abs);
            return abs[k - 1];
        }

        // 二分 + 不回退
        public static int kthABS2(int[] arr, int k) {
            int n = arr.length;
            if (n < 2 || k < 1 || k > ((n * (n - 1)) >> 1)) {
                return -1;
            }
            Arrays.sort(arr);
            // 0 ~ 大-小 二分
            // l  ~  r
            int left = 0;
            int right = arr[n - 1] - arr[0];
            int mid = 0;
            int rightest = -1;
            while (left <= right) {
                mid = (left + right) / 2;
                // 数字对差值的绝对值<=mid的数字对个数，是不是 < k个的！
                if (valid(arr, mid, k)) {
                    rightest = mid;
                    left = mid + 1;
                } else {
                    right = mid - 1;
                }
            }
            return rightest + 1;
        }

        // 假设arr中的所有数字对，差值绝对值<=limit的个数为x
        // 如果 x < k，达标，返回true
        // 如果 x >= k，不达标，返回false
        public static boolean valid(int[] arr, int limit, int k) {
            int x = 0;
            for (int l = 0, r = 1; l < arr.length; r = Math.max(r, ++l)) {
                while (r < arr.length && arr[r] - arr[l] <= limit) {
                    r++;
                }
                x += r - l - 1;
            }
            return x < k;
        }
    }

    // 测试链接：https://leetcode.cn/problems/arithmetic-slices-ii-subsequence
    public static class ArithmeticSlicesIISubsequence {

        // 时间复杂度是O(N^2)，最优解的时间复杂度
        public static int numberOfArithmeticSlices(int[] arr) {
            int N = arr.length;
            int ans = 0;
            ArrayList<HashMap<Integer, Integer>> maps = new ArrayList<>();
            for (int i = 0; i < N; i++) {
                maps.add(new HashMap<>());
                //  ....j...i（结尾）
                for (int j = i - 1; j >= 0; j--) {
                    long diff = (long) arr[i] - (long) arr[j];
                    if (diff <= Integer.MIN_VALUE || diff > Integer.MAX_VALUE) {
                        continue;
                    }
                    int dif = (int) diff;
                    int count = maps.get(j).getOrDefault(dif, 0);
                    ans += count;
                    maps.get(i).put(dif, maps.get(i).getOrDefault(dif, 0) + count + 1);
                }
            }
            return ans;
        }
    }

    //
    public static class RobotRoomCleaner {
        // 不要提交这个接口的内容
        interface Robot {
            // 若下一个方格为空，则返回true，并移动至该方格
            // 若下一个方格为障碍物，则返回false，并停留在原地
            public boolean move();

            // 在调用turnLeft/turnRight后机器人会停留在原位置
            // 每次转弯90度
            public void turnLeft();

            public void turnRight();

            // 清理所在方格
            public void clean();
        }

        // 提交下面的内容
        public static void cleanRoom(Robot robot) {
            clean(robot, 0, 0, 0, new HashSet<>());
        }

        private static final int[][] ds = {{-1, 0}, {0, 1}, {1, 0}, {0, -1}};

        // 机器人robot，
        // 当前来到的位置(x,y)，且之前没来过
        // 机器人脸冲什么方向d，0 1 2 3
        // visited里记录了机器人走过哪些位置
        // 函数的功能：不要重复走visited里面的位置，把剩下的位置，都打扫干净！
        //           而且要回去！
        public static void clean(Robot robot, int x, int y, int d, HashSet<String> visited) {
            robot.clean();
            visited.add(x + "_" + y);
            for (int i = 0; i < 4; i++) {
                // d = 0 :  0 1 2 3
                // d = 1 :  1 2 3 0
                // d = 2 :  2 3 0 1
                // d = 3 :  3 0 1 2
                // 下一步的方向！
                int nd = (i + d) % 4;
                // 当下一步的方向定了！下一步的位置在哪？(nx, ny)
                int nx = ds[nd][0] + x;
                int ny = ds[nd][1] + y;
                if (!visited.contains(nx + "_" + ny) && robot.move()) {
                    clean(robot, nx, ny, nd, visited);
                }
                robot.turnRight();
            }
            // 负责回去：之前的位置，怎么到你的！你要回去，而且方向和到你之前，要一致！
            robot.turnRight();
            robot.turnRight();
            robot.move();
            robot.turnRight();
            robot.turnRight();
        }
    }

    // 测试链接：https://leetcode.cn/problems/koko-eating-bananas
    public static class KokoEatingBananas {

        public static int minEatingSpeed(int[] piles, int h) {
            int L = 1;
            int R = 0;
            for (int pile : piles) {
                R = Math.max(R, pile);
            }
            int ans = 0;
            int M = 0;
            while (L <= R) {
                M = L + ((R - L) >> 1);
                if (hours(piles, M) <= h) {
                    ans = M;
                    R = M - 1;
                } else {
                    L = M + 1;
                }
            }
            return ans;
        }

        // 技巧：求一个数除以一个数向上取整
        // 例子 x / y up！！！ -> （x + y - 1） / y
        public static long hours(int[] piles, int speed) {
            long ans = 0;
            int offset = speed - 1;
            for (int pile : piles) {
                ans += (pile + offset) / speed;
            }
            return ans;
        }
    }

    // 测试链接：https://leetcode.cn/problems/uncrossed-lines
    public static class UncrossedLines {

        // 针对这个题的题意，做的动态规划
        public static int maxUncrossedLines1(int[] A, int[] B) {
            if (A == null || A.length == 0 || B == null || B.length == 0) {
                return 0;
            }
            int N = A.length;
            int M = B.length;
            // dp[i][j]代表: A[0...i]对应B[0...j]最多能划几条线
            int[][] dp = new int[N][M];
            if (A[0] == B[0]) {
                dp[0][0] = 1;
            }
            for (int j = 1; j < M; j++) {
                dp[0][j] = A[0] == B[j] ? 1 : dp[0][j - 1];
            }
            for (int i = 1; i < N; i++) {
                dp[i][0] = A[i] == B[0] ? 1 : dp[i - 1][0];
            }
            // 某个值(key)，上次在A中出现的位置(value)
            HashMap<Integer, Integer> AValueLastIndex = new HashMap<>();
            AValueLastIndex.put(A[0], 0);
            // 某个值(key)，上次在B中出现的位置(value)
            HashMap<Integer, Integer> BValueLastIndex = new HashMap<>();
            for (int i = 1; i < N; i++) {
                AValueLastIndex.put(A[i], i);
                BValueLastIndex.put(B[0], 0);
                for (int j = 1; j < M; j++) {
                    BValueLastIndex.put(B[j], j);
                    // 可能性1，就是不让A[i]去划线
                    int p1 = dp[i - 1][j];
                    // 可能性2，就是不让B[j]去划线
                    int p2 = dp[i][j - 1];
                    // 可能性3，就是要让A[i]去划线，那么如果A[i]==5，它跟谁划线？
                    // 贪心的点：一定是在B[0...j]中，尽量靠右侧的5
                    int p3 = 0;
                    if (BValueLastIndex.containsKey(A[i])) {
                        int last = BValueLastIndex.get(A[i]);
                        p3 = (last > 0 ? dp[i - 1][last - 1] : 0) + 1;
                    }
                    // 可能性4，就是要让B[j]去划线，那么如果B[j]==7，它跟谁划线？
                    // 贪心的点：一定是在A[0...i]中，尽量靠右侧的7
                    int p4 = 0;
                    if (AValueLastIndex.containsKey(B[j])) {
                        int last = AValueLastIndex.get(B[j]);
                        p4 = (last > 0 ? dp[last - 1][j - 1] : 0) + 1;
                    }
                    dp[i][j] = Math.max(Math.max(p1, p2), Math.max(p3, p4));
                }
                BValueLastIndex.clear();
            }
            return dp[N - 1][M - 1];
        }

        // 但是其实这个题，不就是求两个数组的最长公共子序列吗？
        public static int maxUncrossedLines2(int[] A, int[] B) {
            if (A == null || A.length == 0 || B == null || B.length == 0) {
                return 0;
            }
            int N = A.length;
            int M = B.length;
            int[][] dp = new int[N][M];
            dp[0][0] = A[0] == B[0] ? 1 : 0;
            for (int j = 1; j < M; j++) {
                dp[0][j] = A[0] == B[j] ? 1 : dp[0][j - 1];
            }
            for (int i = 1; i < N; i++) {
                dp[i][0] = A[i] == B[0] ? 1 : dp[i - 1][0];
            }
            for (int i = 1; i < N; i++) {
                for (int j = 1; j < M; j++) {
                    int p1 = dp[i - 1][j];
                    int p2 = dp[i][j - 1];
                    int p3 = A[i] == B[j] ? (1 + dp[i - 1][j - 1]) : 0;
                    dp[i][j] = Math.max(p1, Math.max(p2, p3));
                }
            }
            return dp[N - 1][M - 1];
        }
    }

    // 测试链接：https://leetcode.cn/problems/k-empty-slots
    // N个灯泡排成一行，编号从1到N。最初，所有灯泡都关闭。每天只打开一个灯泡，直到N天后所有灯泡都打开。
    // 给你一个长度为N的灯泡数组bulbs ，其中bulbs[i] =x意味着在第(i+1)天，我们会把在位置x的灯泡打开，其中i从0开始，x从1开始。
    // 给你一个整数K ，请你输出在第几天恰好有两个打开的灯泡，使得它们中间正好有K个灯泡且这些灯泡全部是关闭的 。
    // 如果不存在这种情况，返回 -1 。如果有多天都出现这种情况，请返回最小的天数 。
    public static class KEmptySlots {

        public static int kEmptySlots1(int[] bulbs, int k) {
            int n = bulbs.length;
            int[] days = new int[n];
            for (int i = 0; i < n; i++) {
                days[bulbs[i] - 1] = i + 1;
            }
            int ans = Integer.MAX_VALUE;
            if (k == 0) {
                for (int i = 1; i < n; i++) {
                    ans = Math.min(ans, Math.max(days[i - 1], days[i]));
                }
            } else {
                int[] minQueue = new int[n];
                int l = 0;
                int r = -1;
                for (int i = 1; i < n && i < k; i++) {
                    while (l <= r && days[minQueue[r]] >= days[i]) {
                        r--;
                    }
                    minQueue[++r] = i;
                }
                for (int i = 1, j = k; j < n - 1; i++, j++) {
                    while (l <= r && days[minQueue[r]] >= days[j]) {
                        r--;
                    }
                    minQueue[++r] = j;
                    int cur = Math.max(days[i - 1], days[j + 1]);
                    if (days[minQueue[l]] > cur) {
                        ans = Math.min(ans, cur);
                    }
                    if (i == minQueue[l]) {
                        l++;
                    }
                }
            }
            return (ans == Integer.MAX_VALUE) ? -1 : ans;
        }

        public static int kEmptySlots2(int[] bulbs, int k) {
            int n = bulbs.length;
            int[] days = new int[n];
            for (int i = 0; i < n; i++) {
                days[bulbs[i] - 1] = i + 1;
            }
            int ans = Integer.MAX_VALUE;
            for (int left = 0, mid = 1, right = k + 1; right < n; mid++) {
                // 验证指针mid
                // mid 永远不和left撞上的！
                // 1) mid在left和right中间验证的时候，没通过！
                // 2) mid是撞上right的时候
                if (days[mid] <= Math.max(days[left], days[right])) {
                    if (mid == right) { // 收答案！
                        ans = Math.min(ans, Math.max(days[left], days[right]));
                    }
                    left = mid;
                    right = mid + k + 1;
                }
            }
            return (ans == Integer.MAX_VALUE) ? -1 : ans;
        }
    }

    // https://leetcode.cn/problems/avoid-flood-in-the-city
    public static class AvoidFloodInTheCity {

        // rains[i] = j 第i天轮到j号湖泊下雨
        // 规定，下雨日，干啥 : -1
        // 不下雨日，如果没有湖泊可抽 : 1
        public static int[] avoidFlood(int[] rains) {
            int n = rains.length;
            int[] ans = new int[n];
            int[] invalid = new int[0];
            // key : 某个湖
            // value : 这个湖在哪些位置降雨
            // 4 : {3,7,19,21}
            // 1 : { 13 }
            // 2 : {4, 56}
            HashMap<Integer, LinkedList<Integer>> map = new HashMap<>();
            for (int i = 0; i < n; i++) {
                if (rains[i] != 0) { // 第i天要下雨，rains[i]
                    // 3天 9号
                    // 9号 { 3 }
                    // 9号 {1, 3}
                    if (!map.containsKey(rains[i])) {
                        map.put(rains[i], new LinkedList<>());
                    }
                    map.get(rains[i]).addLast(i);
                }
            }
            // 没抽干的湖泊表
            // 某个湖如果满了，加入到set里
            // 某个湖被抽干了，从set中移除
            HashSet<Integer> set = new HashSet<>();
            // 这个堆的堆顶表示最先处理的湖是哪个
            PriorityQueue<Work> heap = new PriorityQueue<>();
            for (int i = 0; i < n; i++) { // 0 1 2 3 ...
                if (rains[i] != 0) {
                    if (set.contains(rains[i])) {
                        return invalid;
                    }
                    // 放入到没抽干的表里
                    set.add(rains[i]);
                    map.get(rains[i]).pollFirst();
                    if (!map.get(rains[i]).isEmpty()) {
                        heap.add(new Work(rains[i], map.get(rains[i]).peekFirst()));
                    }
                    // 题目规定
                    ans[i] = -1;
                } else { // 今天干活！
                    if (heap.isEmpty()) {
                        ans[i] = 1;
                    } else {
                        Work cur = heap.poll();
                        set.remove(cur.lake);
                        ans[i] = cur.lake;
                    }
                }
            }
            return ans;
        }

        public static class Work implements Comparable<Work> {
            public int lake;
            public int nextRain;

            public Work(int l, int p) {
                lake = l;
                nextRain = p;
            }

            @Override
            public int compareTo(Work o) {
                return nextRain - o.nextRain;
            }
        }
    }

    // 由空地和墙组成的迷宫中有一个球。球可以向上（u）下（d）左（l）右（r）四个方向滚动，但在遇到墙壁前不会停止滚动。
    // 当球停下时，可以选择下一个方向。迷宫中还有一个洞，当球运动经过洞时，就会掉进洞里。
    // 给定球的起始位置，目的地和迷宫，找出让球以最短距离掉进洞里的路径。
    // 距离的定义是球从起始位置（不包括）到目的地（包括）经过的空地个数。
    // 通过'u', 'd', 'l' 和 'r'输出球的移动方向。
    // 由于可能有多条最短路径， 请输出字典序最小的路径。如果球无法进入洞，输出"impossible"。
    // 迷宫由一个0和1的二维数组表示。 1表示墙壁，0表示空地。你可以假定迷宫的边缘都是墙壁。
    // 起始位置和目的地的坐标通过行号和列号给出。
    public static class TheMazeIII {

        // 节点：来到了哪？(r,c)这个位置
        // 从哪个方向来的！d -> 0 1 2 3 4
        // 之前做了什么决定让你来到这个位置。
        public static class Node {
            // 行
            public int r;
            // 列
            public int c;
            // 方向
            public int d;
            // 路径 怎么走到终点的路径
            public String p;

            public Node(int row, int col, int dir, String path) {
                r = row;
                c = col;
                d = dir;
                p = path;
            }

        }

        public static String findShortestWay(int[][] maze, int[] ball, int[] hole) {
            int n = maze.length;
            int m = maze[0].length;
            Node[] q1 = new Node[n * m], q2 = new Node[n * m];
            int s1 = 0, s2 = 0;
            boolean[][][] visited = new boolean[maze.length][maze[0].length][4];
            s1 = spread(maze, n, m, new Node(ball[0], ball[1], 4, ""), visited, q1, s1);
            while (s1 != 0) {
                for (int i = 0; i < s1; i++) {
                    Node cur = q1[i];
                    if (hole[0] == cur.r && hole[1] == cur.c) {
                        return cur.p;
                    }
                    s2 = spread(maze, n, m, cur, visited, q2, s2);
                }
                Node[] tmp = q1;
                q1 = q2;
                q2 = tmp;
                s1 = s2;
                s2 = 0;
            }
            return "impossible";
        }

        public static int[][] to = { { 1, 0 }, { 0, -1 }, { 0, 1 }, { -1, 0 }, { 0, 0 } };

        public static String[] re = { "d", "l", "r", "u" };

        // maze迷宫，走的格子
        // n 行数
        // m 列数
        // 当前来到的节点，cur -> (r,c) 方向 路径（决定）
        // v [行][列][方向] 一个格子，其实在宽度有限遍历时，是4个点！
        // q 下一层的队列
        // s 下一层队列填到了哪，size
        // 当前点cur，该分裂分裂，该继续走继续走，所产生的一下层的点，进入q，s++
        // 返回值：q增长到了哪？返回size -> s
        public static int spread(int[][] maze, int n, int m,
                                 Node cur, boolean[][][] v, Node[] q, int s) {
            int d = cur.d;
            int r = cur.r + to[d][0];
            int c = cur.c + to[d][1];
            // 分裂去！
            if (d == 4 || r < 0 || r == n || c < 0 || c == m || maze[r][c] != 0) {
                for (int i = 0; i < 4; i++) {
                    if (i != d) {
                        r = cur.r + to[i][0];
                        c = cur.c + to[i][1];
                        if (r >= 0 && r < n && c >= 0 && c < m && maze[r][c] == 0 && !v[r][c][i]) {
                            v[r][c][i] = true;
                            Node next = new Node(r, c, i, cur.p + re[i]);
                            q[s++] = next;
                        }
                    }
                }
            } else { // 不分裂！继续走！
                if (!v[r][c][d]) {
                    v[r][c][d] = true;
                    q[s++] = new Node(r, c, d, cur.p);
                }
            }
            return s;
        }
    }

    // 给定一个数组，这里面只有1和-1，求和大于0的最长连续子数组的长度
    public static int longestSubArraySumsLagerZero(int[] arr){
        int n = arr.length;
        HashMap<Integer,Integer> map = new HashMap<>();
        int ans = 0;
        int sum = 0;
        for (int i = 0; i < n; i++) {
            sum += arr[i];
            if(sum > 0){
                ans = Math.max(ans,i + 1);
            }else {
                if(map.containsKey(sum - 1)) {
                    ans = Math.max(ans, i - map.get(sum - 1));
                }
            }
            if(!map.containsKey(sum)){
                map.put(sum,i);
            }
        }
        return ans;
    }
}
