package algorithmProblems;

import java.util.*;

public class algorithmIntermediateEnhanceQuestions {

    // 设计LRU缓存结构，该结构在构造时确定大小，假设大小为K，并有如下两个功能
    // 1.set(key, value)：将记录(key, value)插入该结构
    // 10get(key)：返回key对应的value值
    // [要求]：
    // 1.set和get方法的时间复杂度为O(1)
    // 2.某个key的set或get操作一旦发生，认为这个key的记录成了最常使用的。
    // 3.当缓存的大小超过K时，移除最不经常使用的记录，即set或get最久远的。
    // 本题测试链接 : https://leetcode.com/problems/lru-cache/
    // 提交时把类名和构造方法名改成 : LRUCache
    public static class LRUCache {

        public LRUCache(int capacity) {
            cache = new MyCache<>(capacity);
        }

        private MyCache<Integer, Integer> cache;

        public int get(int key) {
            Integer ans = cache.get(key);
            return ans == null ? -1 : ans;
        }

        public void put(int key, int value) {
            cache.set(key, value);
        }

        public static class Node<K, V> {
            public K key;
            public V value;
            public Node<K, V> last;
            public Node<K, V> next;

            public Node(K key, V value) {
                this.key = key;
                this.value = value;
            }
        }

        public static class NodeDoubleLinkedList<K, V> {
            private Node<K, V> head;
            private Node<K, V> tail;

            public NodeDoubleLinkedList() {
                head = null;
                tail = null;
            }

            // 现在来了一个新的node，请挂到尾巴上去
            public void addNode(Node<K, V> newNode) {
                if (newNode == null) {
                    return;
                }
                if (head == null) {
                    head = newNode;
                    tail = newNode;
                } else {
                    tail.next = newNode;
                    newNode.last = tail;
                    tail = newNode;
                }
            }

            // node 入参，一定保证！node在双向链表里！
            // node原始的位置，左右重新连好，然后把node分离出来
            // 挂到整个链表的尾巴上
            public void moveNodeToTail(Node<K, V> node) {
                if (tail == node) {
                    return;
                }
                if (head == node) {
                    head = node.next;
                    head.last = null;
                } else {
                    node.last.next = node.next;
                    node.next.last = node.last;
                }
                node.last = tail;
                node.next = null;
                tail.next = node;
                tail = node;
            }

            public Node<K, V> removeHead() {
                if (head == null) {
                    return null;
                }
                Node<K, V> res = head;
                if (head == tail) {
                    head = null;
                    tail = null;
                } else {
                    head = res.next;
                    res.next = null;
                    head.last = null;
                }
                return res;
            }

        }

        public static class MyCache<K, V> {
            private HashMap<K, Node<K, V>> keyNodeMap;
            private NodeDoubleLinkedList<K, V> nodeList;
            private final int capacity;

            public MyCache(int cap) {
                if (cap < 0) {
                    throw new RuntimeException("capacity should be more than 0");
                }
                keyNodeMap = new HashMap<K, Node<K, V>>();
                nodeList = new NodeDoubleLinkedList<K, V>();
                capacity = cap;
            }

            public V get(K key) {
                if (keyNodeMap.containsKey(key)) {
                    Node<K, V> res = keyNodeMap.get(key);
                    nodeList.moveNodeToTail(res);
                    return res.value;
                }
                return null;
            }

            // set(Key, Value)
            // 新增  更新value的操作
            public void set(K key, V value) {
                if (keyNodeMap.containsKey(key)) {
                    Node<K, V> node = keyNodeMap.get(key);
                    node.value = value;
                    nodeList.moveNodeToTail(node);
                } else { // 新增！
                    Node<K, V> newNode = new Node<K, V>(key, value);
                    keyNodeMap.put(key, newNode);
                    nodeList.addNode(newNode);
                    if (keyNodeMap.size() == capacity + 1) {
                        removeMostUnusedCache();
                    }
                }
            }

            private void removeMostUnusedCache() {
                Node<K, V> removeNode = nodeList.removeHead();
                keyNodeMap.remove(removeNode.key);
            }

        }
    }

    // 给出一组正整数，你从第一个数向最后一个数方向跳跃，每次至少跳跃1格，每个数的值表示你从这个位置可以跳跃的最大长度。
    // 计算如何以最少的跳跃次数跳到最后一个数。
    // 例子：
    // 7
    // 2 3 2 1 2 1 5
    // 输出：3
    // 本题测试链接 : https://leetcode.com/problems/jump-game-ii/
    public static class Code01_JumpGame {
        public static int jump(int[] arr) {
            if (arr == null || arr.length == 0) {
                return 0;
            }
            int step = 0;
            int cur = 0;
            int next = 0;
            for (int i = 0; i < arr.length; i++) {
                if (cur < i) {
                    step++;
                    cur = next;
                }
                next = Math.max(next, i + arr[i]);
            }
            return step;
        }
    }

    // 给定两个有序数组arr1和arr2，再给定一个整数k，返回来自arr1和arr2的两个数相加和最大的前k个，两个数必须分别来自两个数组。按照降序输出。
    // [要求]时间复杂度为O(k * log^k)。
    // 牛客的测试链接：
    // https://www.nowcoder.com/practice/7201cacf73e7495aa5f88b223bbbf6d1
    // 放入大根堆中的结构
    public static class Node {
        public int index1;// arr1中的位置
        public int index2;// arr2中的位置
        public int sum;// arr1[index1] + arr2[index2]的值

        public Node(int i1, int i2, int s) {
            index1 = i1;
            index2 = i2;
            sum = s;
        }
    }

    // 生成大根堆的比较器
    public static class MaxHeapComp implements Comparator<Node> {
        @Override
        public int compare(Node o1, Node o2) {
            return o2.sum - o1.sum;
        }
    }

    public static int[] topKSum(int[] arr1, int[] arr2, int topK) {
        if (arr1 == null || arr2 == null || topK < 1) {
            return null;
        }
        int N = arr1.length;
        int M = arr2.length;
        topK = Math.min(topK, N * M);
        int[] res = new int[topK];
        int resIndex = 0;
        PriorityQueue<Node> maxHeap = new PriorityQueue<>(new MaxHeapComp());
        HashSet<String> set = new HashSet<>();
        int i1 = N - 1;
        int i2 = M - 1;
        maxHeap.add(new Node(i1, i2, arr1[i1] + arr2[i2]));
        set.add(i1 + "_" + i2);
        while (resIndex != topK) {
            Node curNode = maxHeap.poll();
            res[resIndex++] = curNode.sum;
            i1 = curNode.index1;
            i2 = curNode.index2;
            set.remove(i1 + "_" + i2);
            if (i1 - 1 >= 0 && !set.contains((i1 - 1) + "_" + i2)) {
                set.add((i1 - 1) + "_" + i2);
                maxHeap.add(new Node(i1 - 1, i2, arr1[i1 - 1] + arr2[i2]));
            }
            if (i2 - 1 >= 0 && !set.contains(i1 + "_" + (i2 - 1))) {
                set.add(i1 + "_" + (i2 - 1));
                maxHeap.add(new Node(i1, i2 - 1, arr1[i1] + arr2[i2 - 1]));
            }
        }
        return res;
    }

    // 给定两个数组arrX和arrY，长度都为N。代表二维平面上有N个点，第i个点的x坐标和y坐标分别为arrX[i]和arrY[i]，返回求一条直线最多能穿过多少个点?
    // 本题测试链接: https://leetcode.com/problems/max-points-on-a-line/
    public static class MaxPointsOnALine {

        // [
        //    [1,3]
        //    [4,9]
        //    [5,7]
        //   ]

        public static int maxPoints(int[][] points) {
            if (points == null) {
                return 0;
            }
            if (points.length <= 2) {
                return points.length;
            }
            // key = 3
            // value = {7 , 10}  -> 斜率为3/7的点 有10个
            //         {5,  15}  -> 斜率为3/5的点 有15个
            Map<Integer, Map<Integer, Integer>> map = new HashMap<Integer, Map<Integer, Integer>>();
            int result = 0;
            for (int i = 0; i < points.length; i++) {
                map.clear();
                int samePosition = 1;
                int sameX = 0;
                int sameY = 0;
                int line = 0;
                for (int j = i + 1; j < points.length; j++) { // i号点，和j号点，的斜率关系
                    int x = points[j][0] - points[i][0];
                    int y = points[j][1] - points[i][1];
                    if (x == 0 && y == 0) {
                        samePosition++;
                    } else if (x == 0) {
                        sameX++;
                    } else if (y == 0) {
                        sameY++;
                    } else { // 普通斜率
                        int gcd = gcd(x, y);
                        x /= gcd;
                        y /= gcd;
                        // x / y
                        if (!map.containsKey(x)) {
                            map.put(x, new HashMap<Integer, Integer>());
                        }
                        if (!map.get(x).containsKey(y)) {
                            map.get(x).put(y, 0);
                        }
                        map.get(x).put(y, map.get(x).get(y) + 1);
                        line = Math.max(line, map.get(x).get(y));
                    }
                }
                result = Math.max(result, Math.max(Math.max(sameX, sameY), line) + samePosition);
            }
            return result;
        }

        // 保证初始调用的时候，a和b不等于0
        // O(1)
        public static int gcd(int a, int b) {
            return b == 0 ? a : gcd(b, a % b);
        }
    }

    // 给定三个字符串str1、str2和aim，如果aim包含且仅包含来自str1和st2的所有字符，而目在aim中属于str1的字符之间保持原来
    // 在str1中的顺序，属于str2的字符之间保持原来在str2中的顺序，那么称aim是str1和str2的交错组成。
    // 实现一个函数，判断aim是否是str1和str2交错组成的字符串
    // 本题测试链接 : https://leetcode.com/problems/interleaving-string/
    public static class InterleavingString {

        public static boolean isInterleave(String s1, String s2, String s3) {
            if (s1 == null || s2 == null || s3 == null) {
                return false;
            }
            char[] str1 = s1.toCharArray();
            char[] str2 = s2.toCharArray();
            char[] str3 = s3.toCharArray();
            if (str3.length != str1.length + str2.length) {
                return false;
            }
            boolean[][] dp = new boolean[str1.length + 1][str2.length + 1];
            dp[0][0] = true;
            for (int i = 1; i <= str1.length; i++) {
                if (str1[i - 1] != str3[i - 1]) {
                    break;
                }
                dp[i][0] = true;
            }
            for (int j = 1; j <= str2.length; j++) {
                if (str2[j - 1] != str3[j - 1]) {
                    break;
                }
                dp[0][j] = true;
            }
            for (int i = 1; i <= str1.length; i++) {
                for (int j = 1; j <= str2.length; j++) {
                    if (    // 以str1[i-1]位置作为交错字符串的结尾
                            (str1[i - 1] == str3[i + j - 1] && dp[i - 1][j]) ||
                                    // 以str2[j-1]位置作为交错字符串的结尾
                                    (str2[j - 1] == str3[i + j - 1] && dp[i][j - 1])
                    ) {
                        dp[i][j] = true;
                    }
                }
            }
            return dp[str1.length][str2.length];
        }
    }

    // 给定一个长度大于3的正整数数组arr, 返回该数组能不能切成四部分,切掉的数不算,并且每部分的累加和相等
    public static class Code01_Split4Parts {

        public static boolean canSplits1(int[] arr) {
            if (arr == null || arr.length < 7) {
                return false;
            }
            HashSet<String> set = new HashSet<String>();
            int sum = 0;
            for (int i = 0; i < arr.length; i++) {
                sum += arr[i];
            }
            int leftSum = arr[0];
            for (int i = 1; i < arr.length - 1; i++) {
                set.add(leftSum + "_" + (sum - leftSum - arr[i]));
                leftSum += arr[i];
            }
            int l = 1;
            int lsum = arr[0];
            int r = arr.length - 2;
            int rsum = arr[arr.length - 1];
            while (l < r - 3) {
                if (lsum == rsum) {
                    String lkey = String.valueOf(lsum * 2 + arr[l]);
                    String rkey = String.valueOf(rsum * 2 + arr[r]);
                    if (set.contains(lkey + "_" + rkey)) {
                        return true;
                    }
                    lsum += arr[l++];
                } else if (lsum < rsum) {
                    lsum += arr[l++];
                } else {
                    rsum += arr[r--];
                }
            }
            return false;
        }

        public static boolean canSplits2(int[] arr) {
            if (arr == null || arr.length < 7) {
                return false;
            }
            // key 某一个累加和， value出现的位置
            HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
            int sum = arr[0];
            for (int i = 1; i < arr.length; i++) {
                map.put(sum, i);
                sum += arr[i];
            }
            int lsum = arr[0]; // 第一刀左侧的累加和
            for (int s1 = 1; s1 < arr.length - 5; s1++) { // s1是第一刀的位置
                int checkSum = lsum * 2 + arr[s1]; // 100 x 100   100*2 + x
                if (map.containsKey(checkSum)) {
                    int s2 = map.get(checkSum); // j -> y
                    checkSum += lsum + arr[s2];
                    if (map.containsKey(checkSum)) { // 100 * 3 + x + y
                        int s3 = map.get(checkSum); // k -> z
                        if (checkSum + arr[s3] + lsum == sum) {
                            return true;
                        }
                    }
                }
                lsum += arr[s1];
            }
            return false;
        }

        public static int[] generateRandomArray() {
            int[] res = new int[(int) (Math.random() * 10) + 7];
            for (int i = 0; i < res.length; i++) {
                res[i] = (int) (Math.random() * 10) + 1;
            }
            return res;
        }

        public static void test() {
            int testTime = 3000000;
            for (int i = 0; i < testTime; i++) {
                int[] arr = generateRandomArray();
                if (canSplits1(arr) ^ canSplits2(arr)) {
                    System.out.println("Error");
                }
            }
        }
    }

    // 把只包含质因子2、3和5的数称作丑数（Ugly Number）。
    // 例如6、8都是丑数，但14不是，因为它包含质因子7。 习惯上我们把1当做是第一个丑数。求按从小到大的顺序的第N个丑数。
    public static class UglyNumber {
        public static int getNthUglyNumber(int n) {
            int[] arr = new int[n];
            int index = 0;
            arr[0] = 1;
            int i = 2;
            while (index != n) {
                if (isUglyNumber(i)) {
                    arr[index++] = i;
                }
                i++;
            }
            return arr[n - 1];
        }

        public static boolean isUglyNumber(int n) {
            while (n % 2 == 0) {
                n = n / 2;
            }
            while (n % 3 == 0) {
                n = n / 3;
            }
            while (n % 5 == 0) {
                n = n / 5;
            }
            return n == 1;
        }
    }

    public static int getNthUglyNumber2(int n) {
        int[] help = new int[n];
        help[0] = 1;
        int i2 = 0;
        int i3 = 0;
        int i5 = 0;
        int index = 1;
        while (index != n) {
            help[index] = Math.min(2 * help[i2], Math.min(3 * help[i3], 5 * help[i5]));
            if (help[index] == 2 * help[i2]) {
                i2++;
            }
            if (help[index] == 3 * help[i3]) {
                i3++;
            }
            if (help[index] == 5 * help[i5]) {
                i5++;
            }
            index++;
        }
        return help[n - 1];
    }

    // 给定一个数组arr，只能对arr中的一个子数组排序，但是想让arr整体都有序（升序）返回满足这一设定的子数组中，最短是多长
    // 本题测试链接 : https://leetcode.com/problems/shortest-unsorted-continuous-subarray/
    public static class MinLengthForSort {
        public static int findUnsortedSubarray(int[] nums) {
            if (nums == null || nums.length < 2) {
                return 0;
            }
            int N = nums.length;
            int right = -1;
            int max = Integer.MIN_VALUE;
            for (int i = 0; i < N; i++) {
                if (max > nums[i]) {
                    right = i;
                }
                max = Math.max(max, nums[i]);
            }
            int min = Integer.MAX_VALUE;
            int left = N;
            for (int i = N - 1; i >= 0; i--) {
                if (min < nums[i]) {
                    left = i;
                }
                min = Math.min(min, nums[i]);
            }
            return Math.max(0, right - left + 1);
        }
    }

    // 给定一个正数数组arr，其中所有的值都是整数，以下是最小不可组成和的概念：
    // 把arr每个子集内的所有元素加起来会出现很多值，其中最小的记为min，最大的记为max。
    // 在区间[min, max]上，如果有数不可以被arr某一个子集相加得到，那么其中最小的那个数就是arr的最小不可组成和。
    // 在区间[min, max]上，如果所有的数都可以被arr的某一个子集相加得到，那么max + 1是arr的最小不可组成和。
    public static class SmallestUnFormedSum {

        public static int unformedSum1(int[] arr) {
            if (arr == null || arr.length == 0) {
                return 1;
            }
            HashSet<Integer> set = new HashSet<Integer>();
            process(arr, 0, 0, set);
            int min = Integer.MAX_VALUE;
            int sum = 0;
            for (int i = 0; i != arr.length; i++) {
                min = Math.min(min, arr[i]);
                sum += arr[i];
            }
            for (int i = min; i != sum; i++) {
                if (!set.contains(i)) {
                    return i;
                }
            }
            return sum + 1;
        }

        public static void process(int[] arr, int i, int sum, HashSet<Integer> set) {
            if (i == arr.length) {
                set.add(sum);
                return;
            }
            // 选当前数字
            process(arr, i + 1, sum, set);
            // 不选当前数字
            process(arr, i + 1, sum + arr[i], set);
        }

        public static int unformedSum2(int[] arr) {
            if (arr == null || arr.length == 0) {
                return 1;
            }
            int sum = 0;
            int min = Integer.MAX_VALUE;
            for (int i = 0; i != arr.length; i++) {
                sum += arr[i];
                min = Math.min(min, arr[i]);
            }
            // boolean[][] dp ...
            int N = arr.length;
            boolean[][] dp = new boolean[N][sum + 1];
            for (int i = 0; i < N; i++) {// arr[0..i] 0
                dp[i][0] = true;
            }
            dp[0][arr[0]] = true;
            for (int i = 1; i < N; i++) {
                for (int j = 1; j <= sum; j++) {
                    dp[i][j] = dp[i - 1][j] || ((j - arr[i] >= 0) ? dp[i - 1][j - arr[i]] : false);
                }
            }
            for (int j = min; j <= sum; j++) {
                if (!dp[N - 1][j]) {
                    return j;
                }
            }
            return sum + 1;
        }

        public static int[] generateArray(int len, int maxValue) {
            int[] res = new int[len];
            for (int i = 0; i != res.length; i++) {
                res[i] = (int) (Math.random() * maxValue) + 1;
            }
            return res;
        }
    }

    // 进阶问题：已知正数数组arr中肯定有1这个数，是否能更快得到最小不可组和
    public static int unformedSum3(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        Arrays.sort(arr); // O (N * logN)
        int range = 1;
        // arr[0] == 1
        for (int i = 1; i != arr.length; i++) {
            if (arr[i] > range + 1) {
                return range + 1;
            } else {
                range += arr[i];
            }
        }
        return range + 1;
    }

    // 给定一个有序的正数数组arr和一个正数range，如果可以自由选择arr中的数字，想累加得 到 1~range 范围上所有的数，返回arr最少还缺几个数。
    //【举例】
    // arr = {1,2,3,7}，range = 15，想累加得到 1~15 范围上所有的数，arr 还缺14这个数，所以返回1。
    // arr = {1,5,7}，range = 15 ，想累加得到 1~15 范围上所有的数，arr 还缺 2 和 4，所以返回2。
    public static class MinPatches {
        // arr请保证有序，且正数  1~aim
        public static int minPatches(int[] arr, int aim) {
            int patches = 0; // 缺多少个数字
            long range = 0; // 已经完成了1 ~ range的目标
            Arrays.sort(arr);
            for (int i = 0; i != arr.length; i++) {
                // arr[i]
                // 要求：1 ~ arr[i]-1 范围被搞定！
                while (arr[i] - 1 > range) { // arr[i] 1 ~ arr[i]-1
                    range += range + 1; // range + 1 是缺的数字
                    patches++;
                    if (range >= aim) {
                        return patches;
                    }
                }
                // 要求被满足了,可以最大使用arr[i]位置的值
                // 因为1~(arr[i1]-1)的值已经全部搞定，加上arr[i]的值，就直接搞定了1~(arr[i]-1+arr[i])的所有值
                range += arr[i];
                if (range >= aim) {
                    return patches;
                }
            }
            while (aim >= range + 1) {
                range += range + 1;
                patches++;
            }
            return patches;
        }

        // 嘚瑟
        public static int minPatches2(int[] arr, int K) {
            int patches = 0; // 缺多少个数字
            int range = 0; // 已经完成了1 ~ range的目标
            for (int i = 0; i != arr.length; i++) {
                // 1~range
                // 1 ~ arr[i]-1
                while (arr[i] > range + 1) { // arr[i] 1 ~ arr[i]-1

                    if (range > Integer.MAX_VALUE - range - 1) {
                        return patches + 1;
                    }

                    range += range + 1; // range + 1 是缺的数字
                    patches++;
                    if (range >= K) {
                        return patches;
                    }
                }
                if (range > Integer.MAX_VALUE - arr[i]) {
                    return patches;
                }
                range += arr[i];
                if (range >= K) {
                    return patches;
                }
            }
            while (K >= range + 1) {
                if (K == range && K == Integer.MAX_VALUE) {
                    return patches;
                }
                if (range > Integer.MAX_VALUE - range - 1) {
                    return patches + 1;
                }
                range += range + 1;
                patches++;
            }
            return patches;
        }

        public static void test() {
            int[] test = {1, 2, 31, 33};
            int n = 2147483647;
            System.out.println(minPatches(test, n));

        }
    }

    // 给定一个不含有1的正数数组arr，假设其中任意两个数为a和b，如果a和b的最大公约数比1大，那么认为a和b之间有路，反之认为没有路
    // 求arr有多少个连通区域
    // 求arr中最大的连通区域中有多少个数
    // 本题为leetcode原题
    // 测试链接：https://leetcode.com/problems/largest-component-size-by-common-factor/
    // 方法1会超时，但是方法2直接通过
    public static class LargestComponentSizebyCommonFactor {

        public static int largestComponentSize1(int[] arr) {
            int N = arr.length;
            UnionFind set = new UnionFind(N);
            for (int i = 0; i < N; i++) {
                for (int j = i + 1; j < N; j++) {
                    if (gcd(arr[i], arr[j]) != 1) {
                        set.union(i, j);
                    }
                }
            }
            return set.maxSize();
        }

        public static int largestComponentSize2(int[] arr) {
            int N = arr.length;
            // arr中，N个位置，在并查集初始时，每个位置自己是一个集合
            UnionFind unionFind = new UnionFind(N);
            //      key 某个因子   value 哪个位置拥有这个因子
            HashMap<Integer, Integer> factorsMap = new HashMap<>();
            for (int i = 0; i < N; i++) {
                int num = arr[i];
                // 求出根号N， -> limit
                // 求出num这个数的全部因子
                int limit = (int) Math.sqrt(num);
                for (int j = 1; j <= limit; j++) {
                    if (num % j == 0) {
                        if (j != 1) {
                            if (!factorsMap.containsKey(j)) {
                                factorsMap.put(j, i);
                            } else {
                                unionFind.union(factorsMap.get(j), i);
                            }
                        }
                        int other = num / j;
                        if (other != 1) {
                            if (!factorsMap.containsKey(other)) {
                                factorsMap.put(other, i);
                            } else {
                                unionFind.union(factorsMap.get(other), i);
                            }
                        }
                    }
                }
            }
            return unionFind.maxSize();
        }

        // O(1)
        // m,n 要是正数，不能有任何一个等于0
        public static int gcd(int a, int b) {
            return b == 0 ? a : gcd(b, a % b);
        }

        public static class UnionFind {
            private int[] parents;
            private int[] sizes;
            private int[] help;

            public UnionFind(int N) {
                parents = new int[N];
                sizes = new int[N];
                help = new int[N];
                for (int i = 0; i < N; i++) {
                    parents[i] = i;
                    sizes[i] = 1;
                }
            }

            public int maxSize() {
                int ans = 0;
                for (int size : sizes) {
                    ans = Math.max(ans, size);
                }
                return ans;
            }

            private int find(int i) {
                int hi = 0;
                while (i != parents[i]) {
                    help[hi++] = i;
                    i = parents[i];
                }
                for (hi--; hi >= 0; hi--) {
                    parents[help[hi]] = i;
                }
                return i;
            }

            public void union(int i, int j) {
                int f1 = find(i);
                int f2 = find(j);
                if (f1 != f2) {
                    int big = sizes[f1] >= sizes[f2] ? f1 : f2;
                    int small = big == f1 ? f2 : f1;
                    parents[small] = big;
                    sizes[big] = sizes[f1] + sizes[f2];
                }
            }
        }
    }

    // 给怪兽付金币，求最小金币数，雇佣的怪兽会一直守护，遇见的怪兽战力必须小于等于已经雇佣的怪兽战力总和，如果大于，怪兽就会攻击你，要求安全护送出怪兽谷。0
    // 要想成功穿越怪兽谷而不被攻击，他最少要准备多少金币。
    // 输入描述：
    // 第一行输入一个整数N，代表怪兽的只数。
    // 第二行输入N个整数，代表武力值。
    // 第三行输入N个整数，代表收买N只怪兽所需的金币数。
    public static class MoneyProblem {

        // int[] d d[i]：i号怪兽的武力
        // int[] p p[i]：i号怪兽要求的钱
        // ability 当前你所具有的能力
        // index 来到了第index个怪兽的面前

        // 尝试1
        public static long func1(int[] d, int[] p) {
            return process1(d, p, 0, 0);
        }

        // 目前，你的能力是ability，你来到了index号怪兽的面前，如果要通过后续所有的怪兽，
        // 请返回需要花的最少钱数
        public static long process1(int[] d, int[] p, int ability, int index) {
            if (index == d.length) {
                return 0;
            }
            if (ability < d[index]) {
                return p[index] + process1(d, p, ability + d[index], index + 1);
            } else { // ability >= d[index] 可以贿赂，也可以不贿赂
                return Math.min(
                        p[index] + process1(d, p, ability + d[index], index + 1),
                        process1(d, p, ability, index + 1));
            }
        }

        public static long func2(int[] d, int[] p) {
            int sum = 0;
            for (int num : d) {
                sum += num;
            }
            long[][] dp = new long[d.length + 1][sum + 1];
            for (int cur = d.length - 1; cur >= 0; cur--) {
                for (int hp = 0; hp <= sum; hp++) {
                    // 如果这种情况发生，那么这个hp必然是递归过程中不会出现的状态
                    // 既然动态规划是尝试过程的优化，尝试过程碰不到的状态，不必计算
                    if (hp + d[cur] > sum) {
                        continue;
                    }
                    if (hp < d[cur]) {
                        dp[cur][hp] = p[cur] + dp[cur + 1][hp + d[cur]];
                    } else {
                        dp[cur][hp] = Math.min(p[cur] + dp[cur + 1][hp + d[cur]], dp[cur + 1][hp]);
                    }
                }
            }
            return dp[0][0];
        }

        // 尝试2
        // 从0....index号怪兽，花的钱，必须严格==money
        // 并且获得最大的武力值
        // 如果通过不了，返回-1
        // 如果可以通过，返回能通过情况下的最大能力值
        public static long process2(int[] d, int[] p, int index, int money) {
            if (index == -1) { // 一个怪兽也没遇到呢
                return money == 0 ? 0 : -1;
            }
            // index >= 0
            // 1) 不贿赂当前index号怪兽
            long preMaxAbility = process2(d, p, index - 1, money);
            long p1 = -1;
            if (preMaxAbility != -1 && preMaxAbility >= d[index]) {
                p1 = preMaxAbility;
            }
            // 2) 贿赂当前的怪兽 当前的钱 p[index]
            long preMaxAbility2 = process2(d, p, index - 1, money - p[index]);
            long p2 = -1;
            if (preMaxAbility2 != -1) {
                p2 = d[index] + preMaxAbility2;
            }
            return Math.max(p1, p2);
        }

        public static int minMoney2(int[] d, int[] p) {
            int allMoney = 0;
            for (int i = 0; i < p.length; i++) {
                allMoney += p[i];
            }
            int N = d.length;
            for (int money = 0; money < allMoney; money++) {
                if (process2(d, p, N - 1, money) != -1) {
                    return money;
                }
            }
            return allMoney;
        }

        public static long func3(int[] d, int[] p) {
            int sum = 0;
            for (int num : p) {
                sum += num;
            }
            // dp[i][j]含义：
            // 能经过0～i的怪兽，且花钱为j（花钱的严格等于j）时的武力值最大是多少？
            // 如果dp[i][j]==-1，表示经过0～i的怪兽，花钱为j是无法通过的，或者之前的钱怎么组合也得不到正好为j的钱数
            int[][] dp = new int[d.length][sum + 1];
            for (int i = 0; i < dp.length; i++) {
                for (int j = 0; j <= sum; j++) {
                    dp[i][j] = -1;
                }
            }
            // 经过0～i的怪兽，花钱数一定为p[0]，达到武力值d[0]的地步。其他第0行的状态一律是无效的
            dp[0][p[0]] = d[0];
            for (int i = 1; i < d.length; i++) {
                for (int j = 0; j <= sum; j++) {
                    // 可能性一，为当前怪兽花钱
                    // 存在条件：
                    // j - p[i]要不越界，并且在钱数为j - p[i]时，要能通过0～i-1的怪兽，并且钱数组合是有效的。
                    if (j >= p[i] && dp[i - 1][j - p[i]] != -1) {
                        dp[i][j] = dp[i - 1][j - p[i]] + d[i];
                    }
                    // 可能性二，不为当前怪兽花钱
                    // 存在条件：
                    // 0~i-1怪兽在花钱为j的情况下，能保证通过当前i位置的怪兽
                    if (dp[i - 1][j] >= d[i]) {
                        // 两种可能性中，选武力值最大的
                        dp[i][j] = Math.max(dp[i][j], dp[i - 1][j]);
                    }
                }
            }
            int ans = 0;
            // dp表最后一行上，dp[N-1][j]代表：
            // 能经过0～N-1的怪兽，且花钱为j（花钱的严格等于j）时的武力值最大是多少？
            // 那么最后一行上，最左侧的不为-1的列数(j)，就是答案
            // 因此此时钱数是最少的且能保证我的能力刚好可以击败路上遇到的怪兽
            for (int j = 0; j <= sum; j++) {
                if (dp[d.length - 1][j] != -1) {
                    ans = j;
                    break;
                }
            }
            return ans;
        }

        public static int[][] generateTwoRandomArray(int len, int value) {
            int size = (int) (Math.random() * len) + 1;
            int[][] arrs = new int[2][size];
            for (int i = 0; i < size; i++) {
                arrs[0][i] = (int) (Math.random() * value) + 1;
                arrs[1][i] = (int) (Math.random() * value) + 1;
            }
            return arrs;
        }

        public static void test() {
            int len = 10;
            int value = 20;
            int testTimes = 10000;
            for (int i = 0; i < testTimes; i++) {
                int[][] arrs = generateTwoRandomArray(len, value);
                int[] d = arrs[0];
                int[] p = arrs[1];
                long ans1 = func1(d, p);
                long ans2 = func2(d, p);
                long ans3 = func3(d, p);
                long ans4 = minMoney2(d, p);
                if (ans1 != ans2 || ans2 != ans3 || ans1 != ans4) {
                    System.out.println("oops!");
                }
            }
        }
    }

    // 先给出可整合数组的定义:如果一个数组在排序之后，每相邻两个数差的绝对值都为 1， 则该数组为可整合数组。
    // 例如，[5,3,4,6,2]排序之后为[2,3,4,5,6]， 符合每相邻两个数差的绝对值都为 1，所以这个数组为可整合数组。
    // 给定一个整型数组 arr，请返回其中最大可整合子数组的长度。例如， [5,5,3,2,6,4,3]的最大 可整合子数组为[5,3,2,6,4]，所以返回 5
    public static class LongestIntegratedLength {
        public static int getLIL1(int[] arr) {
            if (arr == null || arr.length == 0) {
                return 0;
            }
            int len = 0;
            // O(N^3 * log N)
            for (int start = 0; start < arr.length; start++) { // 子数组所有可能的开头
                for (int end = start; end < arr.length; end++) { // 开头为start的情况下，所有可能的结尾
                    // arr[s ... e]
                    // O(N * logN)
                    if (isIntegrated(arr, start, end)) {
                        len = Math.max(len, end - start + 1);
                    }
                }
            }
            return len;
        }

        public static boolean isIntegrated(int[] arr, int left, int right) {
            int[] newArr = Arrays.copyOfRange(arr, left, right + 1); // O(N)
            Arrays.sort(newArr); // O(N*logN)
            for (int i = 1; i < newArr.length; i++) {
                if (newArr[i - 1] != newArr[i] - 1) {
                    return false;
                }
            }
            return true;
        }

        public static int getLIL2(int[] arr) {
            if (arr == null || arr.length == 0) {
                return 0;
            }
            int len = 0;
            int max = 0;
            int min = 0;
            HashSet<Integer> set = new HashSet<Integer>();
            for (int L = 0; L < arr.length; L++) { // L 左边界
                // L .......
                set.clear();
                max = Integer.MIN_VALUE;
                min = Integer.MAX_VALUE;
                for (int R = L; R < arr.length; R++) { // R 右边界
                    // arr[L..R]这个子数组在验证 l...R L...r+1 l...r+2
                    if (set.contains(arr[R])) {
                        // arr[L..R]上开始 出现重复值了，arr[L..R往后]不需要验证了，
                        // 一定不是可整合的
                        break;
                    }
                    // arr[L..R]上无重复值
                    set.add(arr[R]);
                    max = Math.max(max, arr[R]);
                    min = Math.min(min, arr[R]);
                    if (max - min == R - L) { // L..R 是可整合的
                        len = Math.max(len, R - L + 1);
                    }
                }
            }
            return len;
        }

        public static void test() {
            int[] arr = {5, 5, 3, 2, 6, 4, 3};
            System.out.println(getLIL1(arr));
            System.out.println(getLIL2(arr));

        }

    }

    // 给定一个字符串str，只能在str的后面添加字符，生成一个更长的字符串，更长的字符串需要包含两个str，且两个str开始的位置不能一样，求最少添加几个字符
    public static class ShortestHaveTwice {

        public static String answer(String str) {
            if (str == null || str.length() == 0) {
                return "";
            }
            char[] chas = str.toCharArray();
            if (chas.length == 1) {
                return str + str;
            }
            if (chas.length == 2) {
                return chas[0] == chas[1] ? (str + String.valueOf(chas[0])) : (str + str);
            }
            int endNext = endNextLength(chas);
            return str + str.substring(endNext);
        }

        public static int endNextLength(char[] chas) {
            int[] next = new int[chas.length + 1];
            next[0] = -1;
            next[1] = 0;
            int pos = 2;
            int cn = 0;
            while (pos < next.length) {
                if (chas[pos - 1] == chas[cn]) {
                    next[pos++] = ++cn;
                } else if (cn > 0) {
                    cn = next[cn];
                } else {
                    next[pos++] = 0;
                }
            }
            return next[next.length - 1];
        }
    }

    // 给定两棵二叉树T1和T2，怎么知道T1的某棵子树和T2相等，并返回结果
    // 先对T1进行序列化，然后再对T2进行一样的序列化，然后判断T2序列化的字符串是否是T1的子串
    public static class T1SubtreeEqualsT2 {

        public static class Node {
            public int value;
            public Node left;
            public Node right;

            public Node(int data) {
                this.value = data;
            }
        }

        public static boolean isSubtree(Node t1, Node t2) {
            String t1Str = serialByPre(t1);
            String t2Str = serialByPre(t2);
            return getIndexOf(t1Str, t2Str) != -1;
        }

        public static String serialByPre(Node head) {
            if (head == null) {
                return "#!";
            }
            String res = head.value + "!";
            res += serialByPre(head.left);
            res += serialByPre(head.right);
            return res;
        }

        // KMP
        public static int getIndexOf(String s, String m) {
            if (s == null || m == null || m.length() < 1 || s.length() < m.length()) {
                return -1;
            }
            char[] ss = s.toCharArray();
            char[] ms = m.toCharArray();
            int[] nextArr = getNextArray(ms);
            int index = 0;
            int mi = 0;
            while (index < ss.length && mi < ms.length) {
                if (ss[index] == ms[mi]) {
                    index++;
                    mi++;
                } else if (nextArr[mi] == -1) {
                    index++;
                } else {
                    mi = nextArr[mi];
                }
            }
            return mi == ms.length ? index - mi : -1;
        }

        public static int[] getNextArray(char[] ms) {
            if (ms.length == 1) {
                return new int[]{-1};
            }
            int[] nextArr = new int[ms.length];
            nextArr[0] = -1;
            nextArr[1] = 0;
            int pos = 2;
            int cn = 0;
            while (pos < nextArr.length) {
                if (ms[pos - 1] == ms[cn]) {
                    nextArr[pos++] = ++cn;
                } else if (cn > 0) {
                    cn = nextArr[cn];
                } else {
                    nextArr[pos++] = 0;
                }
            }
            return nextArr;
        }

        public static void test() {
            Node t1 = new Node(1);
            t1.left = new Node(2);
            t1.right = new Node(3);
            t1.left.left = new Node(4);
            t1.left.right = new Node(5);
            t1.right.left = new Node(6);
            t1.right.right = new Node(7);
            t1.left.left.right = new Node(8);
            t1.left.right.left = new Node(9);

            Node t2 = new Node(2);
            t2.left = new Node(4);
            t2.left.right = new Node(8);
            t2.right = new Node(5);
            t2.right.left = new Node(9);

            System.out.println(isSubtree(t1, t2));

        }
    }

    // 给定一个字符串str，只能在str的后面添加字符，最少添加几个能让字符串整体都是回文串
    public static class AddShortestEnd {
        public static String shortestEnd(String s) {
            if (s == null || s.length() == 0) {
                return null;
            }
            char[] str = manacherString(s);
            int[] pArr = new int[str.length];
            int C = -1;
            int R = -1;
            int maxContainsEnd = -1;
            for (int i = 0; i != str.length; i++) {
                pArr[i] = R > i ? Math.min(pArr[2 * C - i], R - i) : 1;
                while (i + pArr[i] < str.length && i - pArr[i] > -1) {
                    if (str[i + pArr[i]] == str[i - pArr[i]])
                        pArr[i]++;
                    else {
                        break;
                    }
                }
                if (i + pArr[i] > R) {
                    R = i + pArr[i];
                    C = i;
                }
                if (R == str.length) {
                    maxContainsEnd = pArr[i];
                    break;
                }
            }
            char[] res = new char[s.length() - maxContainsEnd + 1];
            for (int i = 0; i < res.length; i++) {
                res[res.length - 1 - i] = str[i * 2 + 1];
            }
            return String.valueOf(res);
        }

        public static char[] manacherString(String str) {
            char[] charArr = str.toCharArray();
            char[] res = new char[str.length() * 2 + 1];
            int index = 0;
            for (int i = 0; i != res.length; i++) {
                res[i] = (i & 1) == 0 ? '#' : charArr[index++];
            }
            return res;
        }

        public static void test() {
            String str1 = "abcd123321";
            System.out.println(shortestEnd(str1));
        }
    }

    // 已知某字符串只含有小写字母，压缩之后的字符串包括数字、大括号、小写字符
    // 请根据str还原字符串并返回
    // 例如：
    // 3{2{abc}} abcabcabcabcabcabc
    // 3{a}2{bc}aaabcbc
    // 3{a2{c}}accaccacc
    public static class DecompressString {
        public static String decompress(String str) {
            char[] chs = str.toCharArray();
            return process(chs, 0).str;
        }

        public static class ReturnData {
            public String str;
            public int end;

            public ReturnData(String str, int end) {
                this.str = str;
                this.end = end;
            }
        }

        public static ReturnData process(char[] str, int end) {
            StringBuilder sb = new StringBuilder();
            int times = 0;
            while (end < str.length && str[end] != '}') {
                if (str[end] == '{') {
                    ReturnData returnData = process(str, end + 1);
                    sb.append(getTimesString(returnData.str, times));
                    times = 0;
                    end = returnData.end + 1;
                } else {
                    if (str[end] >= '0' && str[end] <= '9') {
                        times = times * 10 + str[end] - '0';
                    } else {
                        sb.append(String.valueOf(str[end]));
                    }
                    end++;
                }
            }
            return new ReturnData(sb.toString(), end);
        }

        private static String getTimesString(String base, int times) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < times; i++) {
                sb.append(base);
            }
            return sb.toString();
        }
    }

    // 一个矩形可以由左上角点的坐标和右下角点的坐标代表。给定一些矩形，请返回这些矩形全拼接在一起，能否拼出完美的矩形
    // 完美的矩形是指内部没有重叠、没有突出、没有凹陷、所有矩形能正好拼接在一起
    public static class Problem_0391_PerfectRectangle {

        public static boolean isRectangleCover(int[][] matrix) {
            if (matrix.length == 0 || matrix[0].length == 0) {
                return false;
            }
            int l = Integer.MAX_VALUE;
            int r = Integer.MIN_VALUE;
            int d = Integer.MAX_VALUE;
            int u = Integer.MIN_VALUE;
            HashMap<Integer, HashMap<Integer, Integer>> map = new HashMap<>();
            int area = 0;
            for (int[] rect : matrix) {
                add(map, rect[0], rect[1]);
                add(map, rect[0], rect[3]);
                add(map, rect[2], rect[1]);
                add(map, rect[2], rect[3]);
                area += (rect[2] - rect[0]) * (rect[3] - rect[1]);
                l = Math.min(rect[0], l);
                d = Math.min(rect[1], d);
                r = Math.max(rect[2], r);
                u = Math.max(rect[3], u);
            }
            return checkPoints(map, l, d, r, u) && area == (r - l) * (u - d);
        }

        public static void add(HashMap<Integer, HashMap<Integer, Integer>> map, int row, int col) {
            if (!map.containsKey(row)) {
                map.put(row, new HashMap<>());
            }
            map.get(row).put(col, map.get(row).getOrDefault(col, 0) + 1);
        }

        public static boolean checkPoints(HashMap<Integer, HashMap<Integer, Integer>> map, int l, int d, int r, int u) {
            if (map.get(l).getOrDefault(d, 0) != 1
                    || map.get(l).getOrDefault(u, 0) != 1
                    || map.get(r).getOrDefault(d, 0) != 1
                    || map.get(r).getOrDefault(u, 0) != 1) {
                return false;
            }
            map.get(l).remove(d);
            map.get(l).remove(u);
            map.get(r).remove(d);
            map.get(r).remove(u);
            for (int key : map.keySet()) {
                for (int value : map.get(key).values()) {
                    if ((value & 1) != 0) {
                        return false;
                    }
                }
            }
            return true;
        }
    }

    // 给定k个有序链表的头节点，怎么把他们merger成一个有序的链表
    // https://leetcode.cn/problems/merge-k-sorted-lists/
    public static class MergeKSortedList {
        public static class ListNode {
            int val;
            ListNode next;

            ListNode() {
            }

            ListNode(int val) {
                this.val = val;
            }

            ListNode(int val, ListNode next) {
                this.val = val;
                this.next = next;
            }
        }

        public static class NodeComparator implements Comparator<ListNode> {

            @Override
            public int compare(ListNode o1, ListNode o2) {
                return o1.val - o2.val;
            }
        }

        public static ListNode mergeKLists(ListNode[] lists) {
            PriorityQueue<ListNode> queue = new PriorityQueue<>();
            for (ListNode node : lists) {
                if (node != null) {
                    queue.offer(node);
                }
            }
            ListNode head = new ListNode(0);
            ListNode tail = head;
            while (!queue.isEmpty()) {
                ListNode cur = queue.poll();
                tail.next = cur;
                tail = tail.next;
                if (cur.next != null) {
                    queue.offer(cur.next);
                }
            }
            return head.next;
        }
    }

    // 给定一个有序数组arr，和一个整数aim，请不重复打印arr中所有累加和为aim的二元组
    // 给定一个有序数组arr，和一个整数aim，请不重复打印arr中所有累加和为aim的三元组
    public static class PrintUniquePairAndTried {
        public static HashMap<Integer, Integer> printUniquePair(int[] arr, int aim) {
            if (arr == null || arr.length == 0) {
                return null;
            }
            int l = 0;
            int r = arr.length - 1;
            HashMap<Integer, Integer> map = new HashMap<>();
            while (l < r) {
                if (arr[l] + arr[r] < aim) {
                    l++;
                } else if (arr[l] + arr[r] > aim) {
                    r--;
                } else {
                    if (!map.containsKey(l)) {
                        map.put(l, r);
                    }
                    l++;
                }
            }
            return map;
        }

        public static HashMap<Integer, Integer> printUniquePair(int[] arr, int aim, int start) {
            if (arr == null || arr.length == 0) {
                return null;
            }
            int l = start;
            int r = arr.length - 1;
            HashMap<Integer, Integer> map = new HashMap<>();
            while (l < r) {
                if (arr[l] + arr[r] < aim) {
                    l++;
                } else if (arr[l] + arr[r] > aim) {
                    r--;
                } else {
                    if (!map.containsKey(l)) {
                        map.put(l, r);
                    }
                    l++;
                }
            }
            return map;
        }

        public static HashMap<Integer, HashMap<Integer, Integer>> printUniqueTried(int[] arr, int aim) {
            if (arr == null || arr.length == 0) {
                return null;
            }
            HashMap<Integer, HashMap<Integer, Integer>> ans = new HashMap<>();
            for (int i = 0; i < arr.length; i++) {
                HashMap<Integer, Integer> map = printUniquePair(arr, aim - arr[i], i + 1);
                if (!map.isEmpty()) {
                    ans.put(i, map);
                }
            }
            return ans;
        }

        public static void test() {
            int[] arr = {1, 2, 3, 4, 5, 7, 10};
            int aim = 9;
            HashMap<Integer, HashMap<Integer, Integer>> map = printUniqueTried(arr, aim);
            System.out.println(map);
        }
    }

    // 一种消息接收并打印的结构设计：已知一个消息流会不断地吐出整数 1 ~ N，但不一定按照顺序吐出。
    // 如果上次打印的数为i，那么当 i + 1 出现时，请打印 i + 1 及其之后接收过的并且连续的所有数，
    // 直到 1 ~ N 全部接收并打印完，请设计这种接收并打印的结构。
    // 初始默认i等于0
    public static class ReceiveAndPrintOrderLine {

        public static class Node {
            public String info;
            public Node next;

            public Node(String str) {
                info = str;
            }
        }

        public static class MessageBox {
            private HashMap<Integer, Node> headMap;
            private HashMap<Integer, Node> tailMap;
            private int waitPoint;

            public MessageBox() {
                headMap = new HashMap<Integer, Node>();
                tailMap = new HashMap<Integer, Node>();
                waitPoint = 1;
            }

            // 消息的编号，info消息的内容, 消息一定从1开始
            public void receive(int num, String info) {
                if (num < 1) {
                    return;
                }
                Node cur = new Node(info);
                // num~num
                headMap.put(num, cur);
                tailMap.put(num, cur);
                // 建立了num~num这个连续区间的头和尾
                // 查询有没有某个连续区间以num-1结尾
                if (tailMap.containsKey(num - 1)) {
                    tailMap.get(num - 1).next = cur;
                    tailMap.remove(num - 1);
                    headMap.remove(num);
                }
                // 查询有没有某个连续区间以num+1开头的
                if (headMap.containsKey(num + 1)) {
                    cur.next = headMap.get(num + 1);
                    tailMap.remove(num);
                    headMap.remove(num + 1);
                }
                if (num == waitPoint) {
                    print();
                }
            }

            private void print() {
                Node node = headMap.get(waitPoint);
                headMap.remove(waitPoint);
                while (node != null) {
                    System.out.print(node.info + " ");
                    node = node.next;
                    waitPoint++;
                }
                tailMap.remove(waitPoint - 1);
                System.out.println();
            }

        }

        public static void test() {
            // MessageBox only receive 1~N
            MessageBox box = new MessageBox();
            // 1....
            System.out.println("这是2来到的时候");
            box.receive(2, "B"); // - 2"
            System.out.println("这是1来到的时候");
            box.receive(1, "A"); // 1 2 -> print, trigger is 1
            box.receive(4, "D"); // - 4
            box.receive(5, "E"); // - 4 5
            box.receive(7, "G"); // - 4 5 - 7
            box.receive(8, "H"); // - 4 5 - 7 8
            box.receive(6, "F"); // - 4 5 6 7 8
            box.receive(3, "C"); // 3 4 5 6 7 8 -> print, trigger is 3
            box.receive(9, "I"); // 9 -> print, trigger is 9
            box.receive(10, "J"); // 10 -> print, trigger is 10
            box.receive(12, "L"); // - 12
            box.receive(13, "M"); // - 12 13
            box.receive(11, "K"); // 11 12 13 -> print, trigger is 11
        }
    }

    // 本题为求最小包含区间
    // 你有 k 个 非递减排列 的整数列表。找到一个 最小 区间，使得 k 个列表中的每个列表至少有一个数包含在其中。
    // 我们定义如果 b-a < d-c 或者在 b-a == d-c 时 a < c，则区间 [a,b] 比 [c,d] 小。
    // 测试链接 :
    // https://leetcode.com/problems/smallest-range-covering-elements-from-k-lists/
    public static class MinRange {
        public static class Node {
            public int val;
            public int arrIndex;
            public int elementIndex;

            public Node(int value, int arrIndex, int index) {
                val = value;
                this.arrIndex = arrIndex;
                elementIndex = index;
            }
        }

        public static class NodeComparator implements Comparator<Node> {

            @Override
            public int compare(Node a, Node b) {
                return a.val != b.val ? (a.val - b.val) : (a.arrIndex - b.arrIndex);
            }

        }

        public static int[] smallestRange(List<List<Integer>> nums) {
            int N = nums.size();
            TreeSet<Node> set = new TreeSet<>(new NodeComparator());
            for (int i = 0; i < N; i++) {
                set.add(new Node(nums.get(i).get(0), i, 0));
            }
            int r = Integer.MAX_VALUE;
            int a = 0;
            int b = 0;
            while (set.size() == N) {
                Node max = set.last();
                Node min = set.pollFirst();
                if (max.val - min.val < r) {
                    r = max.val - min.val;
                    a = min.val;
                    b = max.val;
                }
                if (min.elementIndex < nums.get(min.arrIndex).size() - 1) {
                    set.add(new Node(nums.get(min.arrIndex).get(min.elementIndex + 1), min.arrIndex, min.elementIndex + 1));
                }
            }
            return new int[]{a, b};
        }

        // 以下为课堂题目，在main函数里有对数器
        public static int minRange1(int[][] m) {
            int min = Integer.MAX_VALUE;
            for (int i = 0; i < m[0].length; i++) {
                for (int j = 0; j < m[1].length; j++) {
                    for (int k = 0; k < m[2].length; k++) {
                        min = Math.min(min,
                                Math.abs(m[0][i] - m[1][j]) + Math.abs(m[1][j] - m[2][k]) + Math.abs(m[2][k] - m[0][i]));
                    }
                }
            }
            return min;
        }

        public static int minRange2(int[][] matrix) {
            int N = matrix.length;
            TreeSet<Node> set = new TreeSet<>(new NodeComparator());
            for (int i = 0; i < N; i++) {
                set.add(new Node(matrix[i][0], i, 0));
            }
            int min = Integer.MAX_VALUE;
            while (set.size() == N) {
                min = Math.min(min, set.last().val - set.first().val);
                Node cur = set.pollFirst();
                if (cur.elementIndex < matrix[cur.arrIndex].length - 1) {
                    set.add(new Node(matrix[cur.arrIndex][cur.elementIndex + 1], cur.arrIndex, cur.elementIndex + 1));
                }
            }
            return min << 1;
        }

        public static int[][] generateRandomMatrix(int n, int v) {
            int[][] m = new int[3][];
            int s = 0;
            for (int i = 0; i < 3; i++) {
                s = (int) (Math.random() * n) + 1;
                m[i] = new int[s];
                for (int j = 0; j < s; j++) {
                    m[i][j] = (int) (Math.random() * v);
                }
                Arrays.sort(m[i]);
            }
            return m;
        }

        public static void test() {
            int n = 20;
            int v = 200;
            int t = 1000000;
            System.out.println("测试开始");
            for (int i = 0; i < t; i++) {
                int[][] m = generateRandomMatrix(n, v);
                int ans1 = minRange1(m);
                int ans2 = minRange2(m);
                if (ans1 != ans2) {
                    System.out.println("出错了!");
                }
            }
            System.out.println("测试结束");
        }
    }

    // 给定一个矩形的四个点，因为这个矩形可能步平行于x轴，再给定一个点，判断这个点是否在矩形的内部
    public static class PointInRectangle {

        /**
         * 判断某个点是否在矩形内
         *
         * @param x1 矩形的左边界的横坐标
         * @param y1 矩形的左边界的纵坐标
         * @param x4 矩形的右边界的横坐标
         * @param y4 矩形的右边界的纵坐标
         * @param x  待测点的横坐标
         * @param y  待测点的纵坐标
         * @return 判断结果：是或者不是
         */
        public boolean isInside(double x1, double y1, double x4, double y4,
                                double x, double y) {
            if (x <= x1) {
                return false;
            }
            if (x >= x4) {
                return false;
            }
            if (y >= y1) {
                return false;
            }
            if (y <= y4) {
                return false;
            }
            return true;
        }

        // 判断是否在不规则的矩形内
        // 通过坐标变换把矩形转成平行的情况，在旋转时所有的点跟着转动
        public boolean isInside(double x1, double y1, double x2, double y2,
                                double x3, double y3, double x4, double y4, double x, double y) {
            if (y1 == y2) {
                return isInside(x1, y1, x4, y4, x, y);
            }
            double l = Math.abs(y4 - y3);
            double k = Math.abs(x4 - x3);
            double s = Math.abs(k * k + l * l);

            double sin = l / s;
            double cos = k / s;

            double x1R = cos * x1 + sin * y1;
            double y1R = -x1 * sin + y1 * cos;

            double x4R = cos * x4 + sin * y4;
            double y4R = -x4 * sin + y4 * cos;

            double xR = cos * x + sin * y;
            double yR = -x * sin + y * cos;
            return isInside(x1R, y1R, x4R, y4R, xR, yR);
        }
    }

    // 给定一个三角形的三个点，再给定一个点，判断这个点是否在三角形的内部
    public static class PointInTriangle {
        // 判断点是否在三角形内
        public static boolean isInside(double x1, double y1, double x2, double y2, double x3, double y3, double x, double y) {
            // 判断点是否是顺时针顺序，如果不是则调整一下
            if (getCross(x1, y1, x3, y3, x2, y2) < 0) {
                // 说明x2，y2在右边,交换3和2
                double temx = x2;
                double temy = y2;
                x2 = x3;
                y2 = y3;
                x3 = temx;
                y3 = temy;
            }
            // 判断叉乘是否都是小于0
            if (getCross(x1, y1, x2, y2, x, y) <= 0
                    && getCross(x2, y2, x3, y3, x, y) <= 0
                    && getCross(x3, y3, x1, y1, x, y) <= 0) {
                return true;
            }
            return false;
        }

        // 计算x，y点在(x1, y2) -> (x2, y2) 向量的左边还是右边
        // 大于0，左边 小于0，右边
        public static double getCross(double x1, double y1, double x2, double y2, double x, double y) {
            // 以x1，y1为基准
            double xs = x2 - x1;
            double ys = y2 - y1;
            double xs1 = x - x1;
            double ys1 = y - y1;
            // 计算叉乘
            return xs * ys1 - ys * xs1;
        }

        public static void test() {
            System.out.println(isInside(0, 0, 2, 0, 1, 2, 1, 1));
        }
    }

    // 给定一个无序数组arr，找到数组中未出现的最小正整数，时间复杂度O(N),空间复杂度O(1)
    // 测试链接：https://leetcode.com/problems/first-missing-positive/
    public static class MissingNumber {

        public static int firstMissingPositive(int[] arr) {
            // l是盯着的位置
            // 0 ~ L-1有效区
            int L = 0;
            int R = arr.length;
            while (L != R) {
                if (arr[L] == L + 1) {
                    L++;
                } else if (arr[L] <= L || arr[L] > R || arr[arr[L] - 1] == arr[L]) { // 垃圾的情况
                    swap(arr, L, --R);
                } else {
                    swap(arr, L, arr[L] - 1);
                }
            }
            return L + 1;
        }

        public static void swap(int[] arr, int i, int j) {
            int tmp = arr[i];
            arr[i] = arr[j];
            arr[j] = tmp;
        }
    }

    // 给定一个整形矩阵nums，每个位置都可以走向左，右，上，下四个方向，请找到其中最长的递增路径
    // 示例 1:
    // 输入: nums =
    // [
    //  [9,9,4],
    //  [6,6,8],
    //  [2,1,1]
    // ]
    // 输出: 4
    // 解释: 最长递增路径为 [1, 2, 6, 9]。
    public static class LongestIncreasingPath {

        public static int longestIncreasingPath1(int[][] matrix) {
            int ans = 0;
            int N = matrix.length;
            int M = matrix[0].length;
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < M; j++) {
                    ans = Math.max(ans, process1(matrix, i, j));
                }
            }
            return ans;
        }

        // 从m[i][j]开始走，走出来的最长递增链，返回！
        public static int process1(int[][] matrix, int i, int j) {
            int up = i > 0 && matrix[i][j] < matrix[i - 1][j] ? process1(matrix, i - 1, j) : 0;
            int down = i < (matrix.length - 1) && matrix[i][j] < matrix[i + 1][j] ? process1(matrix, i + 1, j) : 0;
            int left = j > 0 && matrix[i][j] < matrix[i][j - 1] ? process1(matrix, i, j - 1) : 0;
            int right = j < (matrix[0].length - 1) && matrix[i][j] < matrix[i][j + 1] ? process1(matrix, i, j + 1) : 0;
            return Math.max(Math.max(up, down), Math.max(left, right)) + 1;
        }

        public static int longestIncreasingPath2(int[][] matrix) {
            int ans = 0;
            int N = matrix.length;
            int M = matrix[0].length;
            int[][] dp = new int[N][M];
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < M; j++) {
                    ans = Math.max(ans, process2(matrix, i, j, dp));
                }
            }
            return ans;
        }

        // 从m[i][j]开始走，走出来的最长递增链，返回！
        public static int process2(int[][] matrix, int i, int j, int[][] dp) {
            if (dp[i][j] != 0) {
                return dp[i][j];
            }
            // (i,j)不越界
            int up = i > 0 && matrix[i][j] < matrix[i - 1][j] ? process2(matrix, i - 1, j, dp) : 0;
            int down = i < (matrix.length - 1) && matrix[i][j] < matrix[i + 1][j] ? process2(matrix, i + 1, j, dp) : 0;
            int left = j > 0 && matrix[i][j] < matrix[i][j - 1] ? process2(matrix, i, j - 1, dp) : 0;
            int right = j < (matrix[0].length - 1) && matrix[i][j] < matrix[i][j + 1] ? process2(matrix, i, j + 1, dp) : 0;
            int ans = Math.max(Math.max(up, down), Math.max(left, right)) + 1;
            dp[i][j] = ans;
            return ans;
        }
    }

    // 给定一个整形矩阵nums和一个整数k，找到不大于k的最大子矩阵累加和
    public static class MaxSumOfRectangleNoLargerThanK {
        // 子数组中找到不大于k的最大子数组累加和
        public static int nearK(int[] arr, int k) {
            if (arr == null || arr.length == 0) {
                return Integer.MIN_VALUE;
            }
            TreeSet<Integer> set = new TreeSet<>();
            set.add(0);
            int ans = Integer.MIN_VALUE;
            int sum = 0;
            for (int i = 0; i < arr.length; i++) {
                // 讨论子数组必须以i位置结尾，最接近k的累加和是多少？
                sum += arr[i];
                // 找之前哪个前缀和 >= sum - k  且最接近
                // 有序表中，ceiling(x) 返回>=x且最接近的！
                // 有序表中，floor(x) 返回<=x且最接近的！
                Integer find = set.ceiling(sum - k);
                if (find != null) {
                    int curAns = sum - find;
                    ans = Math.max(ans, curAns);
                }
                set.add(sum);
            }
            return ans;
        }

        public static int maxSumSubMatrix(int[][] matrix, int k) {
            if (matrix == null || matrix[0] == null) {
                return 0;
            }
            if (matrix.length > matrix[0].length) {
                matrix = rotate(matrix);
            }
            int row = matrix.length;
            int col = matrix[0].length;
            int res = Integer.MIN_VALUE;
            TreeSet<Integer> sumSet = new TreeSet<>();
            for (int s = 0; s < row; s++) {
                int[] colSum = new int[col];
                for (int e = s; e < row; e++) {
                    // s ~ e 这些行  选的子矩阵必须包含、且只包含s行~e行的数据
                    // 0 ~ 0  0 ~ 1  0 ~ 2 。。。
                    // 1 ~ 2  1 ~ 2  1 ~ 3 。。。
                    sumSet.add(0);
                    int rowSum = 0;
                    for (int c = 0; c < col; c++) {
                        colSum[c] += matrix[e][c];
                        rowSum += colSum[c];
                        Integer it = sumSet.ceiling(rowSum - k);
                        if (it != null) {
                            res = Math.max(res, rowSum - it);
                        }
                        sumSet.add(rowSum);
                    }
                    sumSet.clear();
                }
            }
            return res;
        }

        public static int[][] rotate(int[][] matrix) {
            int N = matrix.length;
            int M = matrix[0].length;
            int[][] r = new int[M][N];
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < M; j++) {
                    r[j][i] = matrix[i][j];
                }
            }
            return r;
        }
    }

    // 克隆一个无向图. 图中每个节点包括一个 label 和一个放置其邻居的 list.
    // 节点结构体定义如下：
    public static class UndirectedGraphNode {
        int label;
        ArrayList<UndirectedGraphNode> neighbors;

        UndirectedGraphNode(int x) {
            label = x;
            neighbors = new ArrayList<UndirectedGraphNode>();
        }
    }

    public static class CloneGraph {
        // BFS
        public static UndirectedGraphNode BFS(UndirectedGraphNode node) {
            if (node == null) {
                return null;
            }
            UndirectedGraphNode head = new UndirectedGraphNode(node.label);
            LinkedList<UndirectedGraphNode> queue = new LinkedList<>();
            // key原节点 value复制节点
            HashMap<UndirectedGraphNode, UndirectedGraphNode> map = new HashMap<>();
            queue.add(node);
            map.put(node, head);
            while (!queue.isEmpty()) {
                UndirectedGraphNode cur = queue.poll();
                for (UndirectedGraphNode next : cur.neighbors) {
                    if (!map.containsKey(next)) {
                        UndirectedGraphNode copy = new UndirectedGraphNode(next.label);
                        map.put(next, copy);
                        queue.add(next);
                    }
                }
            }
            for (Map.Entry<UndirectedGraphNode, UndirectedGraphNode> entry : map.entrySet()) {
                UndirectedGraphNode cur = entry.getKey();
                UndirectedGraphNode copy = entry.getValue();
                for (UndirectedGraphNode next : cur.neighbors) {
                    copy.neighbors.add(map.get(next));
                }
            }
            return head;
        }

        // DFS
        public static UndirectedGraphNode DFS(UndirectedGraphNode node) {
            // key原节点 value复制节点
            HashMap<UndirectedGraphNode, UndirectedGraphNode> map = new HashMap<>();
            return cloneGraph(node, map);
        }

        private static UndirectedGraphNode cloneGraph(UndirectedGraphNode node, HashMap<UndirectedGraphNode, UndirectedGraphNode> map) {
            if (node == null) {
                return null;
            }
            if (map.containsKey(node)) {
                return map.get(node);
            }
            UndirectedGraphNode copy = new UndirectedGraphNode(node.label);
            map.put(node, copy);
            for (UndirectedGraphNode next : node.neighbors) {
                copy.neighbors.add(cloneGraph(next, map));
            }
            return copy;
        }
    }

    // 给定一个由字符组成的矩阵board，还有一个字符数组words，里面由很多字符串word，在board中找word是指，从board中某个位置出发，每个位置都可以
    // 走向左、右、上、下四个方法，但是不能重复经过每一个位置。返回在board中能找到哪些word
    // 本题测试链接 : https://leetcode.com/problems/word-search-ii/
    public static class WordSearchII {
        public static class TrieNode {
            public TrieNode[] nexts;
            public int pass;
            public boolean end;

            public TrieNode() {
                nexts = new TrieNode[26];
                pass = 0;
                end = false;
            }
        }

        public static void fillWord(TrieNode head, String word) {
            head.pass++;
            char[] chs = word.toCharArray();
            int index = 0;
            TrieNode node = head;
            for (int i = 0; i < chs.length; i++) {
                index = chs[i] - 'a';
                if (node.nexts[index] == null) {
                    node.nexts[index] = new TrieNode();
                }
                node = node.nexts[index];
                node.pass++;
            }
            node.end = true;
        }

        public static String generatePath(LinkedList<Character> path) {
            char[] str = new char[path.size()];
            int index = 0;
            for (Character cha : path) {
                str[index++] = cha;
            }
            return String.valueOf(str);
        }

        public static List<String> findWords(char[][] board, String[] words) {
            TrieNode head = new TrieNode(); // 前缀树最顶端的头
            HashSet<String> set = new HashSet<>();
            for (String word : words) {
                if (!set.contains(word)) {
                    fillWord(head, word);
                    set.add(word);
                }
            }
            // 答案
            List<String> ans = new ArrayList<>();
            // 沿途走过的字符，收集起来，存在path里
            LinkedList<Character> path = new LinkedList<>();
            for (int row = 0; row < board.length; row++) {
                for (int col = 0; col < board[0].length; col++) {
                    // 枚举在board中的所有位置
                    // 每一个位置出发的情况下，答案都收集
                    process(board, row, col, path, head, ans);
                }
            }
            return ans;
        }

        // 从board[row][col]位置的字符出发，
        // 之前的路径上，走过的字符，记录在path里
        // cur还没有登上，有待检查能不能登上去的前缀树的节点
        // 如果找到words中的某个str，就记录在 res里
        // 返回值，从row,col 出发，一共找到了多少个str
        public static int process(
                char[][] board, int row, int col,
                LinkedList<Character> path, TrieNode cur,
                List<String> res) {
            char cha = board[row][col];
            if (cha == 0) { // 这个row col位置是之前走过的位置
                return 0;
            }
            // (row,col) 不是回头路 cha 有效

            int index = cha - 'a';
            // 如果没路，或者这条路上最终的字符串之前加入过结果里
            if (cur.nexts[index] == null || cur.nexts[index].pass == 0) {
                return 0;
            }
            // 没有走回头路且能登上去
            cur = cur.nexts[index];
            path.addLast(cha);// 当前位置的字符加到路径里去
            int fix = 0; // 从row和col位置出发，后续一共搞定了多少答案
            // 当我来到row col位置，如果决定不往后走了。是不是已经搞定了某个字符串了
            if (cur.end) {
                res.add(generatePath(path));
                cur.end = false;
                fix++;
            }
            // 往上、下、左、右，四个方向尝试
            board[row][col] = 0;
            if (row > 0) {
                fix += process(board, row - 1, col, path, cur, res);
            }
            if (row < board.length - 1) {
                fix += process(board, row + 1, col, path, cur, res);
            }
            if (col > 0) {
                fix += process(board, row, col - 1, path, cur, res);
            }
            if (col < board[0].length - 1) {
                fix += process(board, row, col + 1, path, cur, res);
            }
            board[row][col] = cha;
            path.pollLast();
            cur.pass -= fix;
            return fix;
        }
    }

    // 有序数组arr可能经过一次旋转处理，也可能没有，且arr可能存在重复的数。例如，有序数组[1,2,3,4,5,6,7]，可以旋转为[4,5,6,7,1,2,3]。
    // 给定一个可能旋转过的有序数组arr，返回arr中最小值。
    public static class SortedRotateArrayFindMin {
        public static int getMin(int[] arr) {
            int l = 0;
            int r = arr.length - 1;
            int mid = 0;
            while (l < r) {
                if (l == r - 1) {
                    break;
                }
                // arr[l] < arr[r] l...r范围上没有旋转
                if (arr[l] < arr[r]) {
                    return arr[l];
                }
                // arr[l] >= arr[r]
                mid = (l + r) / 2;
                // arr[l] > arr[mid]
                if (arr[l] > arr[mid]) {
                    r = mid;
                    continue;
                }
                // arr[l] >= arr[r] && arr[mid] > arr[l]
                if (arr[mid] > arr[r]) {
                    l = mid;
                    continue;
                }
                // arr[l] >= arr[r] && arr[mid] > arr[l] && arr[r] > arr[mid]
                while (l < mid) {
                    if (arr[l] == arr[mid]) {
                        l++;
                    } else if (arr[l] < arr[mid]) {
                        return arr[l];
                    } else {
                        r = mid;
                        break;
                    }
                }

            }
            return Math.min(arr[l], arr[r]);
        }
    }

    // 本题原本的设定是给定的有序数组中没有重复的数字
    // 但是在课上讲的时候，我们按照可以有重复数字来讲的
    // 而这正是Leetcode 81题的设定
    // 也就是说，这个版本是可以通过Leetcode 33、Leetcode 81两个题目的
    // 而且都是最优解
    // Leetcode 33 : https://leetcode.cn/problems/search-in-rotated-sorted-array
    // Leetcode 81 : https://leetcode.cn/problems/search-in-rotated-sorted-array-ii
    // 注意在提交leetcode 81题时，请把code中返回下标的地方，改成返回boolean类型的返回值
    public static class SearchInRotatedSortedArray {
        // arr，原本是有序数组，旋转过，而且左部分长度不知道
        // 找num
        // num所在的位置返回
        public static int search(int[] arr, int num) {
            int L = 0;
            int R = arr.length - 1;
            int M = 0;
            while (L <= R) {
                // M = L + ((R - L) >> 1)
                M = (L + R) / 2;
                if (arr[M] == num) {
                    return M;
                }
                // arr[M] != num
                // [L] == [M] == [R] != num 无法二分
                if (arr[L] == arr[M] && arr[M] == arr[R]) {
                    while (L != M && arr[L] == arr[M]) {
                        L++;
                    }
                    // 1) L == M L...M 一路都相等
                    // 2) 从L到M终于找到了一个不等的位置
                    if (L == M) { // L...M 一路都相等
                        L = M + 1;
                        continue;
                    }
                }
                // ...
                // arr[M] != num
                // [L] [M] [R] 不都一样的情况, 如何二分的逻辑
                if (arr[L] != arr[M]) {
                    if (arr[M] > arr[L]) { // L...M 一定有序
                        if (num >= arr[L] && num < arr[M]) { //  3  [L] == 1    [M]   = 5   L...M - 1
                            R = M - 1;
                        } else { // 9    [L] == 2    [M]   =  7   M... R
                            L = M + 1;
                        }
                    } else { // [L] > [M]    L....M  存在断点
                        if (num > arr[M] && num <= arr[R]) {
                            L = M + 1;
                        } else {
                            R = M - 1;
                        }
                    }
                } else { /// [L] [M] [R] 不都一样，  [L] === [M] -> [M]!=[R]
                    if (arr[M] < arr[R]) {
                        if (num > arr[M] && num <= arr[R]) {
                            L = M + 1;
                        } else {
                            R = M - 1;
                        }
                    } else {
                        if (num >= arr[L] && num < arr[M]) {
                            R = M - 1;
                        } else {
                            L = M + 1;
                        }
                    }
                }
            }
            return -1;
        }
    }

    // 股票一系列问题
    // 给定一个数组arr，从左到右表示昨天从早到晚股票的价格，你想知道只进行一次交易，且每次交易只买卖一股，返回能挣到的最大钱数
    // leetcode 121
    public static class BestTimeToBuyAndSellStock {
        public static int maxProfit(int[] prices) {
            if (prices == null || prices.length == 0) {
                return 0;
            }
            // 必须在0时刻卖掉，[0] - [0]
            int ans = 0;
            // arr[0...0]
            int min = prices[0];
            for (int i = 1; i < prices.length; i++) {
                min = Math.min(min, prices[i]);
                ans = Math.max(ans, prices[i] - min);
            }
            return ans;
        }
    }

    // 给定一个数组arr，从左到右表示昨天从早到晚股票的价格，你想知道不限制交易次数，且每次交易只买卖一股，返回能挣到的最大钱数
    // leetcode 122
    public static class BestTimeToBuyAndSellStockII {
        public static int maxProfit(int[] prices) {
            if (prices == null || prices.length == 0) {
                return 0;
            }
            int ans = 0;
            for (int i = 1; i < prices.length; i++) {
                ans += Math.max(prices[i] - prices[i - 1], 0);
            }
            return ans;
        }
    }

    // 给定一个数组arr，从左到右表示昨天从早到晚股票的价格，你想知道如果最多交易2次，且每次交易只买卖一股，返回能挣到的最大钱数
    // leetcode 123
    public static class BestTimeToBuyAndSellStockIII {
        public static int maxProfit(int[] prices) {
            if (prices == null || prices.length < 2) {
                return 0;
            }
            int ans = 0;
            int doneOnceMinusBuyMax = -prices[0];
            int doneOnceMax = 0;
            int min = prices[0];
            for (int i = 1; i < prices.length; i++) {
                min = Math.min(min, prices[i]);
                ans = Math.max(ans, doneOnceMinusBuyMax + prices[i]);
                doneOnceMax = Math.max(doneOnceMax, prices[i] - min);
                doneOnceMinusBuyMax = Math.max(doneOnceMinusBuyMax, doneOnceMax - prices[i]);
            }
            return ans;
        }
    }

    // 给定一个数组arr，从左到右表示昨天从早到晚股票的价格，你想知道如果最多交易k次，且每次交易只买卖一股，返回能挣到的最大钱数
    // leetcode 188
    public static class BestTimeToBuyAndSellStockIV {
        public static int maxProfit(int K, int[] prices) {
            if (prices == null || prices.length == 0) {
                return 0;
            }
            int N = prices.length;
            if (K >= N / 2) {
                return allTrans(prices);
            }
            int[][] dp = new int[K + 1][N];
            int ans = 0;
            for (int tran = 1; tran <= K; tran++) {
                int pre = dp[tran][0];
                int best = pre - prices[0];
                for (int index = 1; index < N; index++) {
                    pre = dp[tran - 1][index];
                    dp[tran][index] = Math.max(dp[tran][index - 1], prices[index] + best);
                    best = Math.max(best, pre - prices[index]);
                    ans = Math.max(dp[tran][index], ans);
                }
            }
            return ans;
        }

        public static int allTrans(int[] prices) {
            int ans = 0;
            for (int i = 1; i < prices.length; i++) {
                ans += Math.max(prices[i] - prices[i - 1], 0);
            }
            return ans;
        }

        // 课上写的版本，对了
        public static int maxProfit2(int K, int[] arr) {
            if (arr == null || arr.length == 0 || K < 1) {
                return 0;
            }
            int N = arr.length;
            if (K >= N / 2) {
                return allTrans(arr);
            }
            int[][] dp = new int[N][K + 1];
            // dp[...][0] = 0
            // dp[0][...] = arr[0.0] 0
            for (int j = 1; j <= K; j++) {
                // dp[1][j]
                // 不买卖
                int p1 = dp[0][j];
                // 进行买卖
                int best = Math.max(dp[1][j - 1] - arr[1], dp[0][j - 1] - arr[0]);
                dp[1][j] = Math.max(p1, best + arr[1]);
                // dp[1][j] 准备好一些枚举，接下来准备好的枚举
                for (int i = 2; i < N; i++) {
                    p1 = dp[i - 1][j];
                    int newP = dp[i][j - 1] - arr[i];
                    // 利用之前求解的best值优化  画图理解
                    best = Math.max(newP, best);
                    dp[i][j] = Math.max(p1, best + arr[i]);
                }
            }
            return dp[N - 1][K];
        }
    }

    // 给定两个字符串S和T，返回S子序列等于T的不同子序列个数有多少个？
    // 如果得到子序列A删除的位置与得到子序列B删除的位置不同，那么就认为A和B就是不相同的
    // 测试链接 : https://leetcode-cn.com/problems/21dk04/
    public static class DistinctSubSeq {

        public static int numDistinct1(String S, String T) {
            char[] s = S.toCharArray();
            char[] t = T.toCharArray();
            return process(s, t, s.length, t.length);
        }

        public static int process(char[] s, char[] t, int i, int j) {
            if (j == 0) {
                return 1;
            }
            if (i == 0) {
                return 0;
            }
            int res = process(s, t, i - 1, j);
            if (s[i - 1] == t[j - 1]) {
                res += process(s, t, i - 1, j - 1);
            }
            return res;
        }

        // S[...i]的所有子序列中，包含多少个字面值等于T[...j]这个字符串的子序列
        // 记为dp[i][j]
        // 可能性1）S[...i]的所有子序列中，都不以s[i]结尾，则dp[i][j]肯定包含dp[i-1][j]
        // 可能性2）S[...i]的所有子序列中，都必须以s[i]结尾，
        // 这要求S[i] == T[j]，则dp[i][j]包含dp[i-1][j-1]
        public static int numDistinct2(String S, String T) {
            char[] s = S.toCharArray();
            char[] t = T.toCharArray();
            // dp[i][j] : s[0..i] T[0...j]

            // dp[i][j] : s只拿前i个字符做子序列，有多少个子序列，字面值等于T的前j个字符的前缀串
            int[][] dp = new int[s.length + 1][t.length + 1];
            // dp[0][0]
            // dp[0][j] = s只拿前0个字符做子序列, T前j个字符
            for (int j = 0; j <= t.length; j++) {
                dp[0][j] = 0;
            }
            for (int i = 0; i <= s.length; i++) {
                dp[i][0] = 1;
            }
            for (int i = 1; i <= s.length; i++) {
                for (int j = 1; j <= t.length; j++) {
                    dp[i][j] = dp[i - 1][j] + (s[i - 1] == t[j - 1] ? dp[i - 1][j - 1] : 0);
                }
            }
            return dp[s.length][t.length];
        }

        public static int numDistinct3(String S, String T) {
            char[] s = S.toCharArray();
            char[] t = T.toCharArray();
            int[] dp = new int[t.length + 1];
            dp[0] = 1;
            for (int j = 1; j <= t.length; j++) {
                dp[j] = 0;
            }
            for (int i = 1; i <= s.length; i++) {
                for (int j = t.length; j >= 1; j--) {
                    dp[j] += s[i - 1] == t[j - 1] ? dp[j - 1] : 0;
                }
            }
            return dp[t.length];
        }

        public static int dp(String S, String T) {
            char[] s = S.toCharArray();
            char[] t = T.toCharArray();
            int N = s.length;
            int M = t.length;
            int[][] dp = new int[N][M];
            // s[0..0] T[0..0] dp[0][0]
            dp[0][0] = s[0] == t[0] ? 1 : 0;
            for (int i = 1; i < N; i++) {
                dp[i][0] = s[i] == t[0] ? (dp[i - 1][0] + 1) : dp[i - 1][0];
            }
            for (int i = 1; i < N; i++) {
                for (int j = 1; j <= Math.min(i, M - 1); j++) {
                    dp[i][j] = dp[i - 1][j];
                    if (s[i] == t[j]) {
                        dp[i][j] += dp[i - 1][j - 1];
                    }
                }
            }
            return dp[N - 1][M - 1];
        }

        public static void test() {
            String S = "1212311112121132";
            String T = "13";
            System.out.println(numDistinct3(S, T));
            System.out.println(dp(S, T));

        }
    }

    // 给定一个仅包含数字 0-9 的字符串 num 和一个目标值整数 target ，
    // 在 num 的数字之间添加 二元 运算符（不是一元）+、- 或 * ，返回所有能够得到目标值的表达式。
    // 输入: num = "123", target = 6
    // 输出: ["1+2+3", "1*2*3"]
    // 本题测试链接 : https://leetcode.com/problems/expression-add-operators/
    public static class ExpressionAddOperators {
        public static List<String> addOperators(String num, int target) {
            List<String> ret = new LinkedList<>();
            if (num.length() == 0) {
                return ret;
            }
            // 沿途的数字拷贝和+ - * 的决定，放在path里
            char[] path = new char[num.length() * 2 - 1];
            // num -> char[]
            char[] digits = num.toCharArray();
            long n = 0;
            for (int i = 0; i < digits.length; i++) { // 尝试0~i前缀作为第一部分
                n = n * 10 + digits[i] - '0';
                path[i] = digits[i];
                dfs(ret, path, i + 1, 0, n, digits, i + 1, target); // 后续过程
                if (n == 0) {
                    break;
                }
            }
            return ret;
        }

        // char[] digits 固定参数，字符类型数组，等同于num
        // int target 目标
        // char[] path 之前做的决定，已经从左往右依次填写的字符在其中，可能含有'0'~'9' 与 * - +
        // int len path[0..len-1]已经填写好，len是终止
        // int index 字符类型数组num, 使用到了哪
        // left -> 前面固定的部分 cur -> 前一块
        // 默认 left + cur ...
        public static void dfs(List<String> res, char[] path, int len,
                               long left, long cur,
                               char[] num, int index, int target) {
            if (index == num.length) {
                if (left + cur == target) {
                    res.add(new String(path, 0, len));
                }
                return;
            }
            long n = 0;
            int j = len + 1;
            for (int i = index; i < num.length; i++) { // pos ~ i
                // 试每一个可能的前缀，作为第一个数字！
                // num[index...i] 作为第一个数字！
                n = n * 10 + num[i] - '0';
                path[j++] = num[i];
                path[len] = '+';
                dfs(res, path, j, left + cur, n, num, i + 1, target);
                path[len] = '-';
                dfs(res, path, j, left + cur, -n, num, i + 1, target);
                path[len] = '*';
                dfs(res, path, j, left, cur * n, num, i + 1, target);
                if (num[index] == '0') {
                    break;
                }
            }
        }
    }

    // 删除链表倒数第k个节点
    public static class RemoveNthNodeFromEndOfList {
        public static class ListNode {
            public int val;
            public ListNode next;
        }

        public static ListNode removeNthFromEnd(ListNode head, int n) {
            ListNode cur = head;
            ListNode pre = null;
            while (cur != null) {
                n--;
                if (n == -1) {
                    pre = head;
                }
                if (n < -1) {
                    pre = pre.next;
                }
                cur = cur.next;
            }
            if (n > 0) {
                return head;
            }
            if (pre == null) {
                return head.next;
            }
            pre.next = pre.next.next;
            return head;
        }
    }

    // 反转链表中的一部分链表
    public static class ReversePartList {
        public static class ListNode {
            public int val;
            public ListNode next;

            ListNode() {

            }
            ListNode(int val) {
                this.val = val;
            }
            ListNode(int val, ListNode next) {
                this.val = val;
                this.next = next;
            }
        }
        public ListNode reverseBetween(ListNode head, int left, int right) {
            // 因为头节点有可能发生变化，使用虚拟头节点可以避免复杂的分类讨论
            ListNode dummyNode = new ListNode(-1);
            dummyNode.next = head;

            ListNode pre = dummyNode;
            // 第 1 步：从虚拟头节点走 left - 1 步，来到 left 节点的前一个节点
            // 建议写在 for 循环里，语义清晰
            for (int i = 0; i < left - 1; i++) {
                pre = pre.next;
            }

            // 第 2 步：从 pre 再走 right - left + 1 步，来到 right 节点
            ListNode rightNode = pre;
            for (int i = 0; i < right - left + 1; i++) {
                rightNode = rightNode.next;
            }

            // 第 3 步：切断出一个子链表（截取链表）
            ListNode leftNode = pre.next;
            ListNode curr = rightNode.next;

            // 注意：切断链接
            pre.next = null;
            rightNode.next = null;

            // 第 4 步：反转链表的子区间
            reverseLinkedList(leftNode);

            // 第 5 步：接回到原来的链表中
            pre.next = rightNode;
            leftNode.next = curr;
            return dummyNode.next;
        }

        // 指定头节点的反转链表的常规解法
        private void reverseLinkedList(ListNode head) {
            // 也可以使用递归反转一个链表
            ListNode pre = null;
            ListNode cur = head;

            while (cur != null) {
                ListNode next = cur.next;
                cur.next = pre;
                pre = cur;
                cur = next;
            }
        }
    }

    // 用ZigZag的方式打印二叉树
    public static class BinaryTreeZigzagLevelOrderTraversal {
        public static class TreeNode {
            int val;
            TreeNode left;
            TreeNode right;
        }

        public static List<List<Integer>> zigzagLevelOrder(TreeNode root) {
            List<List<Integer>> ans = new ArrayList<>();
            if (root == null) {
                return ans;
            }
            LinkedList<TreeNode> deque = new LinkedList<>();
            deque.add(root);
            int size = 0;
            boolean isHead = true;
            while (!deque.isEmpty()) {
                size = deque.size();
                List<Integer> curLevel = new ArrayList<>();
                for (int i = 0; i < size; i++) {
                    TreeNode cur = isHead ? deque.pollFirst() : deque.pollLast();
                    curLevel.add(cur.val);
                    if (isHead) {
                        if (cur.left != null) {
                            deque.addLast(cur.left);
                        }
                        if (cur.right != null) {
                            deque.addLast(cur.right);
                        }
                    } else {
                        if (cur.right != null) {
                            deque.addFirst(cur.right);
                        }
                        if (cur.left != null) {
                            deque.addFirst(cur.left);
                        }
                    }
                }
                ans.add(curLevel);
                isHead = !isHead;
            }
            return ans;
        }

        // 两个栈版本

    }

    // 在链表中，对每k个节点逆序，不满k个就不逆序
    public static class ConvertEveryKNodesInList {
        public static class ListNode {
            public int val;
            public ListNode next;

            ListNode() {

            }
            ListNode(int val) {
                this.val = val;
            }
            ListNode(int val, ListNode next) {
                this.val = val;
                this.next = next;
            }
        }
        public ListNode reverseKGroup(ListNode head, int k) {
            ListNode dummyNode = new ListNode(0);
            dummyNode.next = head;
            ListNode pre = dummyNode;

            while (head != null) {
                ListNode tail = pre;
                // 查看剩余部分长度是否大于等于 k
                for (int i = 0; i < k; ++i) {
                    tail = tail.next;
                    if (tail == null) {
                        return dummyNode.next;
                    }
                }
                ListNode nex = tail.next;
                ListNode[] reverse = myReverse(head, tail);
                head = reverse[0];
                tail = reverse[1];
                // 把子链表重新接回原链表
                pre.next = head;
                tail.next = nex;
                pre = tail;
                head = tail.next;
            }

            return dummyNode.next;
        }

        public ListNode[] myReverse(ListNode head, ListNode tail) {
            ListNode prev = tail.next;
            ListNode p = head;
            while (prev != tail) {
                ListNode nex = p.next;
                p.next = prev;
                prev = p;
                p = nex;
            }
            return new ListNode[]{tail, head};
        }
    }

    // 给定一个二维数组map，含义是一张地图，例如如下，矩阵：
    // -2 　-3　 3
    // -5　-10　1
    // 0 　30　-5
    // 游戏规则如下：
    // 骑士从左上角出发，每次只能向右或者向下走，最后到达右下角见到公主。
    // 地图中每个位置的只代表骑士要遭遇的事。如果是负数，表示此处有怪兽，要让骑士损失血量。如果是非负数，表示此处有血瓶，能让骑士回血。
    // 骑士从左上角到右下角的过程，走到任何一个位置，血量都不能少于１。
    // 为了保证骑士能顺利见到公主，初始血量至少是多少？根据map，返回初始血量。
    public static class DragonGame {
        public static int minHp(int[][] map) {
            if (map == null || map.length == 0 || map[0] == null || map[0].length == 0) {
                return 1;
            }
            int row = map.length;
            int col = map[0].length;
            int[][] dp = new int[row--][col--];
            dp[row][col] = map[row][col] > 0 ? 1 : -map[row][col];
            for (int i = col - 1; i >= 0; i--) {
                dp[row][i] = Math.max(dp[row][i + 1] - map[row][i], 1);
            }
            int right = 0;
            int down = 0;
            for (int i = row - 1; i >= 0; i--) {
                dp[i][col] = Math.max(dp[i + 1][col] - map[i][col], 1);
                for (int j = col - 1; j >= 0; j--) {
                    right = Math.max(dp[i][j + 1] - map[i][j], 1);
                    down = Math.max(dp[i + 1][j] - map[i][j], 1);
                    dp[i][j] = Math.min(right, down);
                }
            }
            return dp[0][0];
        }
    }

    // 合并区间是一个常见的编程问题，通常涉及到一组区间，你需要将重叠的区间合并成更大的区间。
    // 力扣链接：https://leetcode.cn/problems/merge-intervals/description/?envType=study-plan-v2&envId=top-100-liked
    // 输入：intervals = [[1,3],[2,6],[8,10],[15,18]]
    // 输出：[[1,6],[8,10],[15,18]]
    // 解释：区间 [1,3] 和 [2,6] 重叠, 将它们合并为 [1,6].
    public static class MergeIntervals {
        public static int[][] merge(int[][] intervals) {
            if (intervals.length == 0) {
                return new int[0][0];
            }
            Arrays.sort(intervals, (a, b) -> a[0] - b[0]);
            int s = intervals[0][0];
            int e = intervals[0][1];
            int size = 0;
            for (int i = 1; i < intervals.length; i++) {
                if (intervals[i][0] > e) {
                    intervals[size][0] = s;
                    intervals[size++][1] = e;
                    s = intervals[i][0];
                    e = intervals[i][1];
                } else {
                    e = Math.max(e, intervals[i][1]);
                }
            }
            intervals[size][0] = s;
            intervals[size++][1] = e;
            return Arrays.copyOf(intervals, size);
        }
    }

    //
    public static class SelfCrossing {
        public static boolean isSelfCrossing(int[] x) {
            if (x == null || x.length < 4) {
                return false;
            }
            if ((x.length > 3 && x[2] <= x[0] && x[3] >= x[1])
                    || (x.length > 4
                    && ((x[3] <= x[1] && x[4] >= x[2]) || (x[3] == x[1] && x[0] + x[4] >= x[2])))) {
                return true;
            }
            for (int i = 5; i < x.length; i++) {
                if (x[i - 1] <= x[i - 3] && ((x[i] >= x[i - 2])
                        || (x[i - 2] >= x[i - 4] && x[i - 5] + x[i - 1] >= x[i - 3] && x[i - 4] + x[i] >= x[i - 2]))) {
                    return true;
                }
            }
            return false;
        }

        public static void test() {
            int[] arr = {2, 2, 3, 2, 2};
            System.out.println(isSelfCrossing(arr));
        }
    }

    // 路径被定义为一条从树中任意节点出发，达到任意节点的序列。该路径至少包含一个节点，且不一定经过根节点
    public static class BinaryTreeMaximumPathSum {
        public static class TreeNode {
            int val;
            TreeNode left;
            TreeNode right;

            public TreeNode(int v) {
                val = v;
            }
        }

        public static int maxPathSum(TreeNode root) {
            if (root == null) {
                return 0;
            }
            return process(root).maxPathSum;
        }

        // 任何一棵树，必须汇报上来的信息
        public static class Info {
            public int maxPathSum;//整棵树的最大路径和
            public int maxPathSumFromHead;// 必须从该头节点出发的情况下最大路径和

            public Info(int path, int head) {
                maxPathSum = path;
                maxPathSumFromHead = head;
            }
        }

        public static Info process(TreeNode x) {
            if (x == null) {
                return null;
            }
            Info leftInfo = process(x.left);
            Info rightInfo = process(x.right);
            // 与x节点有关
            // x 1)只有x 2）x往左扎 3）x往右扎
            int maxPathSumFromHead = x.val;
            if (leftInfo != null) {
                maxPathSumFromHead = Math.max(maxPathSumFromHead, x.val + leftInfo.maxPathSumFromHead);
            }
            if (rightInfo != null) {
                maxPathSumFromHead = Math.max(maxPathSumFromHead, x.val + rightInfo.maxPathSumFromHead);
            }
            // 与x节点无关
            // x整棵树最大路径和 1)左树整体的最大路径和 2) 右树整体的最大路径和
            int maxPathSum = Integer.MIN_VALUE;
            if (leftInfo != null) {
                maxPathSum = Math.max(maxPathSum, leftInfo.maxPathSum);
            }
            if (rightInfo != null) {
                maxPathSum = Math.max(maxPathSum, rightInfo.maxPathSum);
            }
            // 求是否与x节点有关的最大路径和的最大值，作为整体最大路径和
            maxPathSum = Math.max(maxPathSumFromHead, maxPathSum);

            return new Info(maxPathSum, maxPathSumFromHead);
        }

        // 如果要返回路径的做法
        public static List<TreeNode> getMaxSumPath(TreeNode head) {
            List<TreeNode> ans = new ArrayList<>();
            if (head != null) {
                Data data = f(head);
                HashMap<TreeNode, TreeNode> fmap = new HashMap<>();
                fmap.put(head, head);
                fatherMap(head, fmap);
                fillPath(fmap, data.from, data.to, ans);
            }
            return ans;
        }

        public static class Data {
            public int maxAllSum;
            public TreeNode from;
            public TreeNode to;
            public int maxHeadSum;
            public TreeNode end;

            public Data(int a, TreeNode b, TreeNode c, int d, TreeNode e) {
                maxAllSum = a;
                from = b;
                to = c;
                maxHeadSum = d;
                end = e;
            }
        }

        public static Data f(TreeNode x) {
            if (x == null) {
                return null;
            }
            Data l = f(x.left);
            Data r = f(x.right);
            int maxHeadSum = x.val;
            TreeNode end = x;
            if (l != null && l.maxHeadSum > 0 && (r == null || l.maxHeadSum > r.maxHeadSum)) {
                maxHeadSum += l.maxHeadSum;
                end = l.end;
            }
            if (r != null && r.maxHeadSum > 0 && (l == null || r.maxHeadSum > l.maxHeadSum)) {
                maxHeadSum += r.maxHeadSum;
                end = r.end;
            }
            int maxAllSum = Integer.MIN_VALUE;
            TreeNode from = null;
            TreeNode to = null;
            if (l != null) {
                maxAllSum = l.maxAllSum;
                from = l.from;
                to = l.to;
            }
            if (r != null && r.maxAllSum > maxAllSum) {
                maxAllSum = r.maxAllSum;
                from = r.from;
                to = r.to;
            }
            int p3 = x.val + (l != null && l.maxHeadSum > 0 ? l.maxHeadSum : 0)
                    + (r != null && r.maxHeadSum > 0 ? r.maxHeadSum : 0);
            if (p3 > maxAllSum) {
                maxAllSum = p3;
                from = (l != null && l.maxHeadSum > 0) ? l.end : x;
                to = (r != null && r.maxHeadSum > 0) ? r.end : x;
            }
            return new Data(maxAllSum, from, to, maxHeadSum, end);
        }

        public static void fatherMap(TreeNode h, HashMap<TreeNode, TreeNode> map) {
            if (h.left == null && h.right == null) {
                return;
            }
            if (h.left != null) {
                map.put(h.left, h);
                fatherMap(h.left, map);
            }
            if (h.right != null) {
                map.put(h.right, h);
                fatherMap(h.right, map);
            }
        }

        public static void fillPath(HashMap<TreeNode, TreeNode> fmap, TreeNode a, TreeNode b, List<TreeNode> ans) {
            if (a == b) {
                ans.add(a);
            } else {
                HashSet<TreeNode> ap = new HashSet<>();
                TreeNode cur = a;
                while (cur != fmap.get(cur)) {
                    ap.add(cur);
                    cur = fmap.get(cur);
                }
                ap.add(cur);
                cur = b;
                TreeNode lca = null;
                while (lca == null) {
                    if (ap.contains(cur)) {
                        lca = cur;
                    } else {
                        cur = fmap.get(cur);
                    }
                }
                while (a != lca) {
                    ans.add(a);
                    a = fmap.get(a);
                }
                ans.add(lca);
                ArrayList<TreeNode> right = new ArrayList<>();
                while (b != lca) {
                    right.add(b);
                    b = fmap.get(b);
                }
                for (int i = right.size() - 1; i >= 0; i--) {
                    ans.add(right.get(i));
                }
            }
        }

        public static void test() {
            TreeNode head = new TreeNode(4);
            head.left = new TreeNode(-7);
            head.right = new TreeNode(-5);
            head.left.left = new TreeNode(9);
            head.left.right = new TreeNode(9);
            head.right.left = new TreeNode(4);
            head.right.right = new TreeNode(3);

            List<TreeNode> maxPath = getMaxSumPath(head);

            for (TreeNode n : maxPath) {
                System.out.println(n.val);
            }
        }
    }

    // 假设所有字符都是小写字母，长字符串是str。arr是去重的单词表, 每个单词都不是空字符串且可以使用任意次。使用单词表能否拼成长字符串
    // lintcode也有测试，数据量比leetcode大很多 : https://www.lintcode.com/problem/107/
    public static class WordBreak {
        public static class Node {
            public boolean end;
            public Node[] nexts;

            public Node() {
                end = false;
                nexts = new Node[26];
            }
        }

        public static boolean wordBreak(String s, List<String> wordDict) {
            Node root = new Node();
            for (String str : wordDict) {
                char[] chs = str.toCharArray();
                Node node = root;
                int index = 0;
                for (int i = 0; i < chs.length; i++) {
                    index = chs[i] - 'a';
                    if (node.nexts[index] == null) {
                        node.nexts[index] = new Node();
                    }
                    node = node.nexts[index];
                }
                node.end = true;
            }
            char[] str = s.toCharArray();
            int N = str.length;
            boolean[] dp = new boolean[N + 1];
            dp[N] = true; // dp[i]  word[i.....] 能不能被分解
            // dp[N] word[N...]  -> ""  能不能够被分解
            // dp[i] ... dp[i+1....]
            for (int i = N - 1; i >= 0; i--) {
                // i
                // word[i....] 能不能够被分解
                // i..i    i+1....
                // i..i+1  i+2...
                Node cur = root;
                for (int end = i; end < N; end++) {
                    cur = cur.nexts[str[end] - 'a'];
                    if (cur == null) {
                        break;
                    }
                    // 有路！
                    if (cur.end) {
                        // i...end 真的是一个有效的前缀串  end+1....  能不能被分解
                        dp[i] |= dp[end + 1];
                    }
                    if (dp[i]) {
                        break;
                    }
                }
            }
            return dp[0];
        }
    }

    // 假设所有字符都是小写字母，长字符串是str。arr是去重的单词表, 每个单词都不是空字符串且可以使用任意次。使用arr中的单词有多少种拼接str的方式，返回方法数。
    public static class UserSplitStringJointLongString {
        public static int ways(String str, String[] arr) {
            if (str == null || str.length() == 0 || arr == null || arr.length == 0) {
                return 0;
            }
            HashSet<String> set = new HashSet<>();
            for (String s : arr) {
                set.add(s);
            }
            return process(str, set, 0);
        }

        public static int process(String str, HashSet<String> set, int index) {
            if (index == str.length()) {
                return 1;
            }
            int ways = 0;
            for (int end = index; end < str.length(); end++) {
                if (set.contains(str.substring(index, end + 1))) {
                    ways += process(str, set, end + 1);
                }
            }
            return ways;
        }

        public static int ways2(String str, String[] arr) {
            if (str == null || str.length() == 0 || arr == null || arr.length == 0) {
                return 0;
            }
            HashSet<String> set = new HashSet<>();
            for (String s : arr) {
                set.add(s);
            }
            int n = str.length();
            int[] dp = new int[n + 1];
            dp[n] = 1;
            for (int i = n - 1; i >= 0; i--) {
                for (int j = i; j < n; j++) {
                    if (set.contains(str.substring(i, j + 1))) {
                        dp[i] += dp[j + 1];
                    }
                }
            }
            return dp[0];
        }

        // 使用前缀树进行优化
        public static class TireNode {
            public boolean end;
            public TireNode[] nexts;

            public TireNode() {
                end = false;
                nexts = new TireNode[26];
            }
        }

        public static int ways3(String str, String[] arr) {
            if (str == null || str.length() == 0 || arr == null || arr.length == 0) {
                return 0;
            }
            TireNode head = new TireNode();
            for (String s : arr) {
                char[] chs = s.toCharArray();
                TireNode node = head;
                int index = 0;
                for (char ch : chs) {
                    index = ch - 'a';
                    if (node.nexts[index] == null) {
                        node.nexts[index] = new TireNode();
                    }
                    node = node.nexts[index];
                }
                node.end = true;
            }
            return process(str.toCharArray(), head, 0);
        }

        public static int process(char[] str, TireNode head, int index) {
            if (index == str.length) {
                return 1;
            }
            int ways = 0;
            TireNode cur = head;
            for (int end = index; end < str.length; end++) {
                int path = str[end] - 'a';
                if (cur.nexts[path] == null) {
                    break;
                }
                cur = cur.nexts[path];
                if (cur.end) {
                    ways += process(str, head, end + 1);
                }
            }
            return ways;
        }

        // 前缀树实现的动态规划
        public static int ways4(String str, String[] arr) {
            if (str == null || str.length() == 0 || arr == null || arr.length == 0) {
                return 0;
            }
            TireNode head = new TireNode();
            for (String s : arr) {
                char[] chs = s.toCharArray();
                TireNode node = head;
                int index = 0;
                for (char ch : chs) {
                    index = ch - 'a';
                    if (node.nexts[index] == null) {
                        node.nexts[index] = new TireNode();
                    }
                    node = node.nexts[index];
                }
                node.end = true;
            }
            char[] chs = str.toCharArray();
            int n = chs.length;
            int[] dp = new int[n + 1];
            dp[n] = 1;
            for (int i = n - 1; i >= 0; i--) {
                TireNode cur = head;
                for (int end = i; end < chs.length; end++) {
                    int path = chs[end] - 'a';
                    if (cur.nexts[path] == null) {
                        break;
                    }
                    cur = cur.nexts[path];
                    if (cur.end) {
                        dp[i] += dp[end + 1];
                    }
                }
            }
            return dp[0];
        }
    }

    // 给定一个数组arr，返回一个结果数组ans，arr和ans等长，ans[i]表示：arr中表示除了arr[i]这个数字之外，剩下的所有数字的累乘结果
    public static class MultiplyExceptSelf {
        public static int[] multiplyExceptSelf(int[] arr) {
            if (arr == null || arr.length == 0) {
                return null;
            }
            int[] ans = new int[arr.length];
            int noZeroMul = 1;
            int zeroCount = 0;
            for (int num : arr) {
                if (num == 0) {
                    zeroCount++;
                } else {
                    noZeroMul *= num;
                }
            }
            for (int i = 0; i < arr.length; i++) {
                if (zeroCount == 0) {
                    ans[i] = noZeroMul / arr[i];
                } else if (zeroCount == 1) {
                    if (arr[i] == 0) {
                        ans[i] = noZeroMul;
                    }
                }
            }
            return ans;
        }
    }

    // 给定一个数组arr，已知除了一种数出现了1此之外，剩下的所有数字都出现了k次，如何使用空间复杂度为O(1)，找到这个数
    // k进制异或
    public static class KTimesOneTime {
        public static int oneNum(int[] arr, int k) {
            int[] e0 = new int[32];
            for (int i = 0; i != e0.length; i++) {
                setExclusiveOr(e0, arr[i], k);
            }
            int res = getNumFromKNum(e0, k);
            return res;
        }

        public static void setExclusiveOr(int[] e0, int value, int k) {
            int[] curKSysNum = getKSysNumFromNum(value, k);
            for (int i = 0; i != e0.length; i++) {
                e0[i] = (e0[i] + curKSysNum[i]) % k;
            }
        }

        public static int[] getKSysNumFromNum(int value, int k) {
            int[] res = new int[32];
            int index = 0;
            while (value != 0) {
                res[index++] = value % k;
                value = value / k;
            }
            return res;
        }

        public static int getNumFromKNum(int[] e0, int k) {
            int res = 0;
            for (int i = e0.length - 1; i != -1; i--) {
                res = (res * k + e0[i]);
            }
            return res;
        }
    }

    // 给定一颗搜索二叉树后序遍历结果，且其中没有重复值，生成整棵树并且返回
    public static class PostOrderArrayToBST {
        public static class Node {
            public Node left;
            public Node right;
            public int value;

            public Node(int value) {
                this.value = value;
            }
        }

        public static class Info {
            public boolean valid;
            public Node head;

            public Info(boolean v, Node h) {
                valid = v;
                head = h;
            }
        }

        public static Info process(int[] arr, int l, int r) {
            if (l == r) {
                return new Info(true, new Node(arr[l]));
            }
            int lastLessIndex = -1;
            int firstMoreIndex = -1;
            Node head = new Node(arr[r]);
            for (int i = l; i < r; i++) {
                if (arr[l] < arr[r]) {
                    lastLessIndex = i;
                } else {
                    firstMoreIndex = firstMoreIndex == -1 ? i : firstMoreIndex;
                }
            }
            if (lastLessIndex != -1 && firstMoreIndex != -1 && lastLessIndex + 1 != firstMoreIndex) {
                return new Info(false, null);
            }
            Info leftInfo = null;
            if (lastLessIndex != -1) {
                leftInfo = process(arr, l, lastLessIndex);
            }
            Info rightInfo = null;
            if (firstMoreIndex != -1) {
                rightInfo = process(arr, firstMoreIndex, r - 1);
            }
            if (leftInfo != null && !leftInfo.valid || rightInfo != null && !rightInfo.valid) {
                return new Info(false, null);
            }
            head.left = leftInfo != null ? leftInfo.head : null;
            head.right = rightInfo != null ? rightInfo.head : null;
            return new Info(true, head);
        }
    }

    // 设计一个区间操作器，可以对区间进行以下操作：
    // 1. addRange(int left,int right)
    // 加入[left,right）这个区间
    // 2. queryRange(int left,int right)
    // 查询[left,right）这个区间是否在容器中
    // 3.removeRange(int left,int right)
    // 删除[left,right)这个区间
    public static class RangeModule {
        public static class Range {
            // key表示区间开始位置，value表示区间结束位置
            public static TreeMap<Integer, Integer> map;

            public Range() {
                map = new TreeMap<>();
            }
        }

        public static void addRange(int left, int right) {
            if (right >= left) {
                return;
            }
            Integer start1 = Range.map.floorKey(left);
            Integer start2 = Range.map.floorKey(right);

            if (start1 == null && start2 == null) {// 当前这个区间不会影响其他的区间
                Range.map.put(left, right);
            } else if (start1 != null && Range.map.get(start1) >= left) {
                Range.map.put(start1, Math.max(Range.map.get(start2), right));
            } else {
                // 0 start1 == null && start2 == null 已经处理了
                // 1 start1 == null && start2 != null
                // 2 start1 != null && start2 == null 不可能出现这种情况，能找到比left小的，就一定能找到比right小的那个
                // 3 start1 != null && start2 != null && Range.map.get(start1) < left
                Range.map.put(left, Math.max(Range.map.get(start2), right));
            }
            // 删除所有start在(left，right]
            Map<Integer, Integer> subMap = Range.map.subMap(left, false, right, true);
            Set<Integer> set = new HashSet<>(subMap.keySet());
            Range.map.keySet().removeAll(set);
        }

        public static boolean queryRange(int left, int right) {
            Integer start = Range.map.floorKey(left);
            if (start == null) {
                return false;
            }
            return Range.map.get(start) >= right;
        }

        public static void removeRange(int left, int right) {
            if (right <= left) {
                return;
            }
            Integer start1 = Range.map.floorKey(left);
            Integer start2 = Range.map.floorKey(right);
            if (start2 != null && Range.map.get(start2) > right) {
                Range.map.put(right, Range.map.get(start2));
            }
            if (start1 != null && Range.map.get(start1) < left) {
                Range.map.put(start1, left);
            }
            // 删除在[left,right)这个区间的
            Map<Integer, Integer> subMap = Range.map.subMap(left, true, right, false);
            Set<Integer> set = new HashSet<>(subMap.keySet());
            Range.map.keySet().removeAll(set);
        }
    }
}
