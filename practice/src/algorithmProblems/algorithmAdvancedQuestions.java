package algorithmProblems;

import java.util.*;

public class algorithmAdvancedQuestions {

    // 给定一个数组，求如果排序之后，相邻两数的最大差值，要求时间复杂带为O(N)，且不能使用非基于比较的排序
    public static int maxGap(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        int len = arr.length;
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        for (int num : arr) {
            min = Math.min(num, min);
            max = Math.max(num, max);
        }
        boolean[] hasNum = new boolean[len + 1];//hasNum[i]代表是否有数字
        int[] maxs = new int[len + 1];
        int[] mins = new int[len + 1];
        int index = 0;
        for (int num : arr) {
            index = bucket(num, len, min, max);
            mins[index] = hasNum[index] ? Math.min(mins[index], num) : num;
            maxs[index] = hasNum[index] ? Math.max(maxs[index], num) : num;
            hasNum[index] = true;
        }
        int res = 0;
        int lastMax = maxs[0];
        for (int i = 1; i < len; i++) {
            if (hasNum[i]) {
                res = Math.max(res, mins[i] - lastMax);
                lastMax = maxs[i];
            }
        }
        return res;
    }

    public static int bucket(int num, int len, int min, int max) {
        return (num - min) * len / (max - min);
    }

    // 给出数字a1,a2……an，问最多有多少不重叠的非空区间，使得每个区间内数字的XOR为0(即异或为0)
    public static int mostEOR(int[] arr) {
        int xor = 0;
        // dp[i]->arr[o..i]最优化分的情况下，异或和为0最多的部分是多少个
        int[] dp = new int[arr.length];
        // key 从出发某个前缀异或和
        // value 这个前缀异或和出现最晚的位置
        HashMap<Integer, Integer> map = new HashMap<>();
        map.put(0, -1);
        for (int i = 0; i < arr.length; i++) {
            xor ^= arr[i];//xor->0..i所有数的异或和
            // 可能性二
            if (map.containsKey(xor)) {// 上次这个数字出现的异或和
                // 0...pre -> pre + 1...i是最优划分的最后一个部分
                // (pre + 1,i)，最后一个部分
                int pre = map.get(xor);
                dp[i] = pre == -1 ? 1 : dp[pre] + 1;
            }
            // 比较可能性1和可能性2的最大值
            // 可能性1是包括i这个部分的eor值不为0
            // dp[i]  = max(dp[i - 1],dp[k -1] + 1)
            if (i > 0) {
                dp[i] = Math.max(dp[i - 1], dp[i]);
            }
            map.put(xor, i);
        }
        return dp[arr.length - 1];
    }

    // 现有n1+n2种面值的硬币，其中前n1种为普通币，可以取任意枚，后n2种为纪念币，
    // 每种最多只能取一枚，每种硬币有一个面值，问能用多少种方法拼出m的面值?
    static int MOD = (int) 1e9 + 7;

    public static long moneyWays(int[] many, int[] one, int money) {
        if (money < 0) {
            return 0;
        }
        if ((many == null || many.length == 0) && (one == null || one.length == 0)) {
            return money == 0 ? 1 : 0;
        }
        long[][] dpMany = many(many, money);
        long[][] dpOne = one(one, money);
        if (dpMany == null) {
            return dpOne[dpOne.length - 1][money];
        }
        if (dpOne == null) {
            return dpMany[dpMany.length - 1][money];
        }
        long res = 0;
        for (int i = 0; i <= money; i++) {
            res += dpMany[dpMany.length - 1][i] * dpOne[dpOne.length - 1][money - i];
            res %= MOD;
        }
        return res;
    }

    public static long[][] many(int[] arr, int money) {
        if (arr == null || arr.length == 0) {
            return null;
        }
        long[][] dp = new long[arr.length][money + 1];
        for (int i = 0; i < arr.length; i++) {
            dp[i][0] = 1;
        }
        for (int j = 1; arr[0] * j <= money; j++) {
            dp[0][arr[0] * j] = 1;
        }
        for (int i = 1; i < arr.length; i++) {
            for (int j = 1; j <= money; j++) {
                dp[i][j] = dp[i - 1][j];
                dp[i][j] += j - arr[i] >= 0 ? dp[i][j - arr[i]] : 0;
                dp[i][j] %= MOD;
            }
        }
        return dp;
    }

    public static long[][] one(int[] arr, int money) {
        if (arr == null || arr.length == 0) {
            return null;
        }
        long[][] dp = new long[arr.length][money + 1];
        for (int i = 0; i < arr.length; i++) {
            dp[i][0] = 1;
        }
        if (arr[0] <= money) {
            dp[0][arr[0]] = 1;
        }
        for (int i = 1; i < arr.length; i++) {
            for (int j = 1; j <= money; j++) {
                dp[i][j] = dp[i - 1][j];
                dp[i][j] += j - arr[i] >= 0 ? dp[i - 1][j - arr[i]] : 0;
                dp[i][j] %= MOD;
            }
        }
        return dp;
    }

    // 给定两个一维int数组A和B.
    // 其中：A是长度为m、元素从小到大排好序的有序数组。B是长度为n、元素从小到大排好序的有序数组。
    // 希望找出两个数组的中位数
    // 算法原型：两个有序数组求解上中位数
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        int size = nums1.length + nums2.length;
        boolean even = (size & 1) == 0;
        if (nums1.length != 0 && nums2.length != 0) {
            if (even) {
                return (double) (findKthNum(nums1, nums2, size / 2) + findKthNum(nums1, nums2, size / 2 + 1)) / 2D;
            } else {
                return findKthNum(nums1, nums2, size / 2 + 1);
            }
        } else if (nums1.length != 0) {
            if (even) {
                return (double) (nums1[(size - 1) / 2] + nums1[size / 2]) / 2;
            } else {
                return nums1[size / 2];
            }
        } else if (nums2.length != 0) {
            if (even) {
                return (double) (nums2[(size - 1) / 2] + nums2[size / 2]) / 2;
            } else {
                return nums2[size / 2];
            }
        } else {
            return 0;
        }
    }

    // 进阶问题 : 在两个都有序的数组中，找整体第K小的数
    // 可以做到O(log(Min(M,N)))
    public static int findKthNum(int[] arr1, int[] arr2, int kth) {
        int[] longs = arr1.length >= arr2.length ? arr1 : arr2;
        int[] shorts = arr1.length < arr2.length ? arr1 : arr2;
        int l = longs.length;
        int s = shorts.length;
        if (kth <= s) { // 1)
            return getUpMedian(shorts, 0, kth - 1, longs, 0, kth - 1);
        }
        if (kth > l) { // 3)
            if (shorts[kth - l - 1] >= longs[l - 1]) {
                return shorts[kth - l - 1];
            }
            if (longs[kth - s - 1] >= shorts[s - 1]) {
                return longs[kth - s - 1];
            }
            return getUpMedian(shorts, kth - l, s - 1, longs, kth - s, l - 1);
        }
        // 2)  s < k <= l
        if (longs[kth - s - 1] >= shorts[s - 1]) {
            return longs[kth - s - 1];
        }
        return getUpMedian(shorts, 0, s - 1, longs, kth - s, kth - 1);
    }


    // A[s1...e1]
    // B[s2...e2]
    // 一定等长！
    // 返回整体的，上中位数
    public static int getUpMedian(int[] A, int s1, int e1, int[] B, int s2, int e2) {
        int mid1 = 0;
        int mid2 = 0;
        while (s1 < e1) {
            // mid1 = s1 + (e1 - s1) >> 1
            mid1 = (s1 + e1) / 2;
            mid2 = (s2 + e2) / 2;
            if (A[mid1] == B[mid2]) {
                return A[mid1];
            }
            // 两个中点一定不等！
            if (((e1 - s1 + 1) & 1) == 1) { // 奇数长度
                if (A[mid1] > B[mid2]) {
                    if (B[mid2] >= A[mid1 - 1]) {
                        return B[mid2];
                    }
                    e1 = mid1 - 1;
                    s2 = mid2 + 1;
                } else { // A[mid1] < B[mid2]
                    if (A[mid1] >= B[mid2 - 1]) {
                        return A[mid1];
                    }
                    e2 = mid2 - 1;
                    s1 = mid1 + 1;
                }
            } else { // 偶数长度
                if (A[mid1] > B[mid2]) {
                    e1 = mid1;
                    s2 = mid2 + 1;
                } else {
                    e2 = mid2;
                    s1 = mid1 + 1;
                }
            }
        }
        return Math.min(A[s1], B[s2]);
    }

    // 算法原型：环形单链表用约瑟夫环问题解决
    public static class Node {
        public Node next;
        public int value;
        public Node left;
        public Node right;

        public Node() {

        }
    }

    public static Node josephusKill2(Node head, int m) {
        if (head == null) {
            return head;
        }
        if (head.next == null) {
            return head;
        }
        if (m < 1) {
            return head;
        }
        Node cur = head.next;
        int size = 1;
        while (cur != head) {
            size++;
            cur = cur.next;
        }
        size = getLive(size, m);
        while (--size != 0) {
            cur = cur.next;
        }
        cur.next = cur;
        return cur;
    }

    // 现在一共有i个节点，数到m就杀死节点，最终会活下来的节点，请返回它在有i个节点时的编号
    // get(N,m)
    public static int getLive(int i, int m) {
        if (i == 1) {
            return 1;
        }
        // 长度为i - 1时，活下来的节点的新编号
        int newNode = getLive(i - 1, m);
        return (newNode + m - 1) % i + 1;
    }

    // 某公司招聘，有n个人入围，HR在黑板上依次写下m个正整数A1、A2、……、Am，然后让这n个人围成一个 圈，并按照顺时针顺序为他们编号0、1、2、……、n-1。录取规则是：
    // 第一轮从0号的人开始，取用黑板上的第1个数字，也就是A1黑板上的数字按次序循环取用，即如果某轮用了第m个，则下一轮需要用第1个；如果某轮用到第k个，则下轮需要用第k+1个（k<m）
    // 每一轮按照黑板上的次序取用到一个数字Ax，淘汰掉从当前轮到的人开始按照顺时针顺序数到的第Ax个人，
    // 下一轮开始时轮到的人即为被淘汰掉的人的顺时针顺序下一个人被淘汰的人直接回家，所以不会被后续轮次计数时数到经过n-1轮后，剩下的最后1人被录取所以最后被录取的人的编号与（n，m，A1，A2，……，Am）相关。
    // 输入描述：
    // 第一行是一个正整数N，表示有N组参数从第二行开始，每行有若干个正整数，依次存放n、m、A1、……、Am，一共有N行，也就是上面的N组参数。
    // 输出描述：
    // 输出有N行，每行对应相应的那组参数确定的录取之人的编号示例
    // 输入
    // 1
    // n m arr[m]
    // 4 2 3 1
    // 输出
    // 1
    public static int recruit(int n, int[] arr) {
        return live(n, arr, 0);
    }

    // 还剩i个人，当前取用的数字为arr[index]，并且在下面的过程中，循环取用arr中的数字，
    // 返回哪个人活(在这i个人中)
    public static int live(int n, int[] arr, int index) {
        if (n == 1) {
            return 1;
        }
        int newLive = live(n - 1, arr, nextIndex(arr.length, index));
        return (newLive + arr[index] - 1) % n + 1;
    }

    public static int nextIndex(int length, int index) {
        return index == length - 1 ? 0 : index + 1;
    }

    // 水平面上有 N 座大楼，每座大楼都是矩阵的形状，可以用一个三元组表示 (start, end, height)，分别代表其在x轴上的起点，终点和高度。
    // 大楼之间从远处看可能会重叠，求出 N 座大楼的外轮廓线。
    // 输入:
    // [
    //    [1, 3, 3],
    //    [2, 4, 4],
    //    [5, 6, 1]
    // ]
    // 输出:
    // [
    //    [1, 2, 3],
    //    [2, 4, 4],
    //    [5, 6, 1]
    // ]
    // 统计高度的变化,从而得到轮廓线
    public static class Border {
        public int index;
        public int height;
        public boolean isChange;

        public Border(int index, int height, boolean isChange) {
            this.index = index;
            this.height = height;
            this.isChange = isChange;
        }
    }

    public static class BorderComparator implements Comparator<Border> {

        @Override
        public int compare(Border o1, Border o2) {
            // 先根据index比较大小
            if (o1.index != o2.index) {
                return o1.index - o2.index;
            }
            // 在根据高度是否发生变化，如果index相同，高度增加放在高度降低前面
            if (o1.isChange != o2.isChange) {
                return o1.isChange ? -1 : 1;
            }
            return 0;
        }
    }

    public List<List<Integer>> buildingOutline(int[][] buildings) {
        Border[] borders = new Border[buildings.length * 2];
        // 每一个轮廓数组，产生两个描述高度变化的对象
        for (int i = 0; i < buildings.length; i++) {
            borders[i * 2] = new Border(buildings[i][0], buildings[i][2], true);
            borders[i * 2 + 1] = new Border(buildings[i][1], buildings[i][2], false);
        }
        // 把描述高度变化的数组，按规则排序
        Arrays.sort(borders, new BorderComparator());

        // 高度以及频率
        TreeMap<Integer, Integer> mapHeightTimes = new TreeMap<>();
        // 坐标以及最大高度
        TreeMap<Integer, Integer> mapXHeight = new TreeMap<>();

        for (Border border : borders) {
            if (border.isChange) {// 高度变高了
                if (!mapHeightTimes.containsKey(border.height)) {
                    mapHeightTimes.put(border.height, 1);
                } else {
                    mapHeightTimes.put(border.height, mapHeightTimes.get(border.height) + 1);
                }
            } else {// 高度变矮了
                if (mapHeightTimes.get(border.height) == 1) {
                    mapHeightTimes.remove(border.height);
                } else {
                    mapHeightTimes.put(border.height, mapHeightTimes.get(border.height) - 1);
                }
            }
            // 根据mapHeightTimes的最大高度，设置mapXHeight表
            if (mapHeightTimes.isEmpty()) {// 如果mapHeightTimes为空说明最大高度为0
                mapXHeight.put(border.index, 0);
            } else {// 如果mapHeightTimes不为空，取最大高度
                mapXHeight.put(border.index, mapHeightTimes.lastKey());
            }
        }
        // res为结果数组，每一个List<Integer>代表一个轮廓线，开始位置，结束位置，高度
        List<List<Integer>> res = new ArrayList<>();
        // 开始位置
        int start = 0;
        // 之前的最大高度
        int preHeight = 0;
        for (Map.Entry<Integer, Integer> entry : mapXHeight.entrySet()) {
            // 当前位置
            int curX = entry.getKey();
            // 当前最大高度
            int curHeight = entry.getValue();
            if (preHeight != curHeight) {
                if (preHeight != 0) {
                    res.add(new ArrayList<>(Arrays.asList(start, curX, preHeight)));
                }
                start = curX;
                preHeight = curHeight;
            }
        }
        return res;
    }

    // 给定一个数组arr,该数组无序，但每个数均为正数，再给定一个正数k。
    // 求arr的所有子数组中所有元素相加为k的最长子数组长度。
    // 例如，arr=[1,2,1,1,1],k=3。累加和为3的子数组为[1,2] ,[2,3], [1,1,1]。其中最长的子数组为[1,1,1]，因此返回3
    // 要求：时间复杂度为O(N)，空间复杂度为O(1)
    // 滑动窗口解决
    public static int getMaxLengthInPositiveArr(int[] arr, int k) {
        if (arr == null || arr.length == 0 || k < 0) {
            return 0;
        }
        int left = 0;
        int right = 0;
        int len = 0;
        int sum = 0;
        // left = right +1 表示窗口没有元素
        // left = right 表示有一个元素
        for (int i = 0; i < arr.length; i++) {
            if (sum == k) {
                len = Math.max(len, right - left + 1);
                sum -= arr[left];
                left++;
            } else if (sum < k) {
                right++;
                if (right == arr.length)
                    break;
                sum += arr[right];
            } else {
                sum -= arr[left];
                left++;
            }
        }
        return len;
    }

    // 给定一个无序数组arr，其中元素可正、可负、可0，给定一个整数k。求arr所有的子数组中累加和为k的最长子数组长度。
    public int maxLength(int[] arr, int k) {
        if (arr == null || arr.length == 0 || k < 0)
            return 0;
        Map<Integer, Integer> map = new HashMap<>();
        map.put(0, -1);
        int sum = 0, len = 0;
        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
            if (!map.containsKey(sum))
                map.put(sum, i);
            if (map.containsKey(sum - k))
                len = Math.max(len, i - map.get(sum - k));
        }
        return len;
    }


    // 给定一个无序数组arr，其中元素可正、可负、可0。给定一个整数k，求arr所有的子数组中累加和小于或等于k的最长子数组长度
    // 例如：arr = [3, -2, -4, 0, 6], k = -2. 相加和小于等于-2的最长子数组为{3, -2, -4, 0}，所以结果返回4
    public static int maxLengthInNoOrder(int[] arr, int k) {
        if (arr == null || arr.length == 0 || k < 0)
            return 0;
        int[] minSum = new int[arr.length];
        int[] minSumEnd = new int[arr.length];
        minSum[arr.length - 1] = arr[arr.length - 1];
        minSumEnd[arr.length - 1] = arr.length - 1;
        for (int i = arr.length - 2; i >= 0; i++) {
            if (minSum[i + 1] < 0) {
                minSum[i] = minSum[i + 1] + arr[i];
                minSumEnd[i] = minSumEnd[i + 1];
            } else {
                minSum[i] = arr[i];
                minSumEnd[i] = i;
            }
        }

        int end = 0;
        int sum = 0;
        int res = 0;
        for (int i = 0; i < arr.length - 1; i++) {
            while (end < arr.length - 1 && sum + minSum[end] <= k) {
                sum += minSum[end];
                end = minSumEnd[end] + 1;
            }
            res = Math.max(res, end - i);
            if (end > i) {// 窗口还有数
                sum -= arr[i];
            } else {// 窗口没有数
                end = i + 1;
            }
        }
        return res;
    }

    // Nim博弈问题
    // 给定一个非负数组，每一个值代表该位置上有几个铜板。a和b玩游戏，a先手，b后手，
    // 轮到某个人的时候，只能在一个位置上拿任意数量的铜板，但是不能不拿。
    // 谁最先把铜 板拿完谁赢。假设a和b都极度聪明，请返回获胜者的名字
    public static String getWinner(int[] array) {
        int eorResult = 0;
        for (int num : array) {
            eorResult ^= num;
        }
        return eorResult == 0 ? "后手" : "先手";
    }

    // 一个 char 类型的数组 chs，其中所有的字符都不同。 例如，chs=['A', 'B', 'C', ... 'Z']，
    // 则字符串与整数的对应关系如下:
    // A, B... Z, AA,AB...AZ,BA,BB...ZZ,AAA... ZZZ, AAAA...
    // 1, 2...26,27, 28... 52,53,54...702,703...18278, 18279...
    // 给定一个数组 chs，实现根据对应关系完成字符串与整数相互转换的两个函数。
    // k伪进制表达，代表每个位置至少要有一个1
    public static String getString(char[] chs, int n) {
        if (chs == null || chs.length == 0 || n < 1) {
            return "";
        }
        int cur = 1;
        int base = chs.length;
        int len = 0;
        while (n >= cur) {
            len++;
            n--;
            cur *= base;
        }
        char[] res = new char[len];
        int index = 0;
        int nCur = 0;
        do {
            cur /= base;
            nCur = n / cur;
            res[index++] = getKthCharAtChs(chs, nCur + 1);
            n %= cur;
        } while (index != res.length);
        return String.valueOf(res);
    }

    public static char getKthCharAtChs(char[] chs, int k) {
        if (k < 1 || k > chs.length) {
            return 0;
        }
        return chs[k - 1];
    }

    public static int getNum(char[] chs, String str) {
        if (chs == null || chs.length == 0) {
            return 0;
        }
        char[] strs = str.toCharArray();
        int base = chs.length;
        int cur = 1;
        int res = 0;
        for (int i = strs.length - 1; i != -1; i--) {
            res += getNthFromChar(chs, strs[i]) * cur;
            cur *= base;
        }
        return res;
    }

    public static int getNthFromChar(char[] chs, char str) {
        int res = -1;
        for (int i = 0; i != chs.length; i++) {
            if (chs[i] == str) {
                res = i + 1;
                break;
            }
        }
        return res;
    }

    // 给定一个二叉树的头节点head，路径可以从任何节点出发，但必须往下走到达任何节点，返回最大路径和
    public static int maxSum(Node head) {
        if (head == null) {
            return 0;
        }
        return process(head).allTreeMaxSum;
    }

    public static class Info {
        public int allTreeMaxSum;
        public int fromHeadMaxSum;

        public Info(int all, int from) {
            allTreeMaxSum = all;
            fromHeadMaxSum = from;
        }
    }

    // 1）X无关的时候， 1， 左树上的整体最大路径和 2， 右树上的整体最大路径和
    // 2) X有关的时候 3， x自己 4， x往左走 5，x往右走
    public static Info process(Node x) {
        if (x == null) {
            return null;
        }
        Info leftInfo = process(x.left);
        Info rightInfo = process(x.right);
        int p1 = Integer.MIN_VALUE;
        if (leftInfo != null) {
            // 可能性一
            p1 = leftInfo.allTreeMaxSum;
        }
        int p2 = Integer.MIN_VALUE;
        if (rightInfo != null) {
            // 可能性二
            p2 = rightInfo.allTreeMaxSum;
        }
        // 可能性三
        int p3 = x.value;
        // 可能性四
        int p4 = Integer.MIN_VALUE;
        if (leftInfo != null) {
            p4 = x.value + leftInfo.fromHeadMaxSum;
        }
        // 可能性五
        int p5 = Integer.MIN_VALUE;
        if (rightInfo != null) {
            p5 = x.value + rightInfo.fromHeadMaxSum;
        }
        int allTreeMaxSum = Math.max(Math.max(Math.max(p1, p2), p3), Math.max(p4, p5));
        int fromHeadMaxSum = Math.max(Math.max(p3, p4), p5);
        return new Info(allTreeMaxSum, fromHeadMaxSum);
    }

    // 给定一个二维数组matrix，每个单元都是一个整数，有正有负。
    // 最开始的时候小Q操纵 一条长度为0的蛇，蛇从矩阵最左侧任选一个单元格进入地图，
    // 蛇每次只能够到达当前位置的右上相邻，右侧相邻和右下相邻的单元格。
    // 蛇蛇到达一个单元格后，自身的长度会瞬间加上该单元格的数值，任何情况下长度为负则游戏结束。
    // 小Q是个天才，他拥有一个超能力，可以在游戏开始的时候把地图中的某一个节点的值变为其相反数(注:最多只能改变一个节点)。
    // 问在小Q游戏过程中，他的蛇蛇最长长度可以到多少?
    // 比如:
    //  1   -4   10
    //  3   -2   -1
    //   2   -1   0
    //  0   5    -2
    // 最优路径为从最左侧的3开始，3 -> -4(利用能力变成4) -> 10。所以返回17。
    public static int walk1(int[][] matrix) {
        if (matrix == null || matrix.length == 0 || matrix[0].length == 0) {
            return 0;
        }
        int res = Integer.MIN_VALUE;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                int[] ans = process(matrix, i, j);
                res = Math.max(res, Math.max(ans[0], ans[1]));
            }
        }
        return res;
    }

    // 从假想的最优左侧到达(i,j)的旅途中
    // 0)在没有使用过能力的情况下，返回路径最大和，没有可能到达的话返回负数
    // 1)在使用过能力的情况下，返回路径最大和，没有可能达到的话，返回负数
    public static int[] process(int[][] matrix, int i, int j) {
        if (j == 0) {// (i,j)在最左侧位置
            return new int[]{matrix[i][j], -matrix[i][j]};
        }

        // (i,j)不在最左侧
        // P1从自己的左边位置过来
        int[] preAns = process(matrix, i, j - 1);
        // 在之前所有的路中，完全不使用能力的情况下，能够到达的最好长度是多少
        int preUnUse = preAns[0];
        // 在之前所有的路中，使用过一次能力的情况下，能够到达的最好长度是多少
        int preUse = preAns[1];

        // P2从自己的左上位置过来
        if (i > 0) {
            preAns = process(matrix, i - 1, j - 1);
            preUnUse = Math.max(preUnUse, preAns[0]);
            preUse = Math.max(preUse, preAns[1]);
        }

        // P3从自己的左下位置过来
        if (i < matrix.length - 1) {
            preAns = process(matrix, i + 1, j - 1);
            preUnUse = Math.max(preUnUse, preAns[0]);
            preUse = Math.max(preUse, preAns[1]);
        }

        // preUnUse之前的旅途没有使用过能力
        // preUse之前的旅途使用过能力
        int no = -1;// 之前没有使用过能力，当前位置也不使用能力
        int yes = -1;// 不管能力是之前使用的还是当前使用的，保证只使用一次
        if (preUnUse >= 0) {
            // 之前没有使用能力，现在也不使用
            no = matrix[i][j] + preUnUse;
            // 之前没有使用能力，现在使用能力
            yes = -matrix[i][j] + preUnUse;
        }
        if (preUse >= 0) {
            // 之前使用了能力，现在不使用能力
            yes = Math.max(yes, matrix[i][j] + preUse);
        }
        return new int[]{no, yes};
    }

    public static class WalkInfo {
        public int noPower;// 之前路径不使用能力
        public int usePower; // 之前路径使用能力

        public WalkInfo(int noPower, int usePower) {
            this.noPower = noPower;
            this.usePower = usePower;
        }
    }

    // 加上缓存的方式，记忆化搜索
    public static int walk2(int[][] matrix) {
        if (matrix == null || matrix.length == 0 || matrix[0].length == 0) {
            return 0;
        }
        int res = Integer.MIN_VALUE;
        WalkInfo[][] dp = new WalkInfo[matrix.length][matrix[0].length];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                WalkInfo ans = process2(matrix, i, j, dp);
                res = Math.max(res, Math.max(ans.noPower, ans.usePower));
            }
        }
        return res;
    }

    // 从假想的最优左侧到达(i,j)的旅途中
    // 0)在没有使用过能力的情况下，返回路径最大和，没有可能到达的话返回负数
    // 1)在使用过能力的情况下，返回路径最大和，没有可能达到的话，返回负数
    public static WalkInfo process2(int[][] matrix, int i, int j, WalkInfo[][] dp) {
        if (dp[i][j] != null) {
            return dp[i][j];
        }

        if (j == 0) {// (i,j)在最左侧位置
            dp[i][j] = new WalkInfo(matrix[i][j], -matrix[i][j]);
            return dp[i][j];
        }

        // (i,j)不在最左侧
        // P1从自己的左边位置过来
        WalkInfo preAns = process2(matrix, i, j - 1, dp);
        // 在之前所有的路中，完全不使用能力的情况下，能够到达的最好长度是多少
        int preUnUse = preAns.noPower;
        // 在之前所有的路中，使用过一次能力的情况下，能够到达的最好长度是多少
        int preUse = preAns.usePower;

        // P2从自己的左上位置过来
        if (i > 0) {
            preAns = process2(matrix, i - 1, j - 1, dp);
            preUnUse = Math.max(preUnUse, preAns.noPower);
            preUse = Math.max(preUse, preAns.usePower);
        }

        // P3从自己的左下位置过来
        if (i < matrix.length - 1) {
            preAns = process2(matrix, i + 1, j - 1, dp);
            preUnUse = Math.max(preUnUse, preAns.noPower);
            preUse = Math.max(preUse, preAns.usePower);
        }

        // preUnUse之前的旅途没有使用过能力
        // preUse之前的旅途使用过能力
        int no = -1;// 之前没有使用过能力，当前位置也不使用能力
        int yes = -1;// 不管能力是之前使用的还是当前使用的，保证只使用一次
        if (preUnUse >= 0) {
            // 之前没有使用能力，现在也不使用
            no = matrix[i][j] + preUnUse;
            // 之前没有使用能力，现在使用能力
            yes = -matrix[i][j] + preUnUse;
        }
        if (preUse >= 0) {
            // 之前使用了能力，现在不使用能力
            yes = Math.max(yes, matrix[i][j] + preUse);
        }
        dp[i][j] = new WalkInfo(no, yes);
        return dp[i][j];
    }

    // 给定一个字符串str，str 表示一个公式，公式里可能有整数，加减乘除和左右括号，返回公式的计算结果。
    // 【举例】
    // str = “48*((70-65)-43)+8*1” , 返回 -1816
    // str = “3+1*4” , 返回 7
    // str = “3+(1*4)” , 返回 7
    //【说明】
    // 1.可以认为给定的字符串一定是正确的公式，即不需要对 str 做公式有效性检查。
    // 2.如果是负数，就需要用括号括起来，比如”4*(-3)“ 。但如果负数作为公式的开头或者括号部分的开头，则可以没有括号，比如：”-3*4“ 和 ”(-3*4)“ 都是合法的。
    // 3.不用考虑计算过程中发生的溢出的情况
    public static int getValue(String str) {
        return value(str.toCharArray(), 0)[0];
    }

    // 从chs[i...]往下开始计算，遇到字符串终止位置或右括号返回两个值
    // 第一个值负责计算遇到右括号之前这一段结果是多少
    // 第二个值是代表计算到了哪个位置
    public static int[] value(char[] chs, int index) {
        LinkedList<String> queue = new LinkedList<>();
        int num = 0;
        int[] res;
        while (index < chs.length && chs[index] != ')') {
            if (chs[index] >= '0' && chs[index] <= '9') {// 数字
                num = num * 10 + chs[index] - '0';
                index++;
            } else if (chs[index] != '(') {// 运算符
                addNum(queue, num);
                queue.addLast(String.valueOf(chs[index]));
                index++;
                num = 0;
            } else {// 左括号
                res = value(chs, index + 1);
                num = res[0];
                index = res[1] + 1;
            }
        }
        addNum(queue, num);
        return new int[]{getNum(queue), index};
    }

    // 处理乘法和除法
    public static void addNum(LinkedList<String> queue, int num) {
        if (!queue.isEmpty()) {
            int cur = 0;
            String top = queue.pollLast();
            if (top.equals("+") || top.equals("-")) {
                queue.addLast(top);
            } else {
                cur = Integer.parseInt(queue.pollLast());
                num = top.equals("*") ? cur * num : cur / num;
            }
        }
        queue.addLast(String.valueOf(num));
    }

    public static int getNum(LinkedList<String> queue) {
        int res = 0;
        boolean isAdd = true;
        String cur;
        int num = 0;
        while (!queue.isEmpty()) {
            cur = queue.pollFirst();
            if (cur.equals("+")) {
                isAdd = true;
            } else if (cur.equals("-")) {
                isAdd = false;
            } else {
                num = Integer.parseInt(cur);
                res += isAdd ? num : -num;
            }

        }
        return res;
    }

    // 空间压缩技巧
    // 求两个字符串的最长公共子串
    public static String lcs1(String s1, String s2) {
        if (s1 == null || s2 == null || s1.equals("") || s2.equals("")) {
            return "";
        }
        char[] chs1 = s1.toCharArray();
        char[] chs2 = s2.toCharArray();
        int[][] dp = getDp(chs1, chs2);
        int end = 0;// 记录最长子串结束位置
        int max = 0;
        for (int i = 0; i < chs1.length; i++) {
            for (int j = 0; j < chs2.length; j++) {
                if (dp[i][j] > max) {
                    end = i;
                    max = dp[i][j];
                }
            }
        }
        return s1.substring(end - max + 1, end + 1);
    }

    private static int[][] getDp(char[] chs1, char[] chs2) {
        int[][] dp = new int[chs1.length][chs2.length];
        for (int i = 0; i < chs1.length; i++) {
            if (chs1[i] == chs2[0]) {
                dp[i][0] = 1;
            }
        }
        for (int i = 0; i < chs2.length; i++) {
            if (chs2[i] == chs1[0]) {
                dp[0][i] = 1;
            }
        }
        for (int i = 1; i < chs1.length; i++) {
            for (int j = 1; j < chs2.length; j++) {
                if (chs1[i] == chs2[j]) {
                    dp[i][j] = dp[i - 1][j - 1] + 1;
                } else {
                    dp[i][j] = dp[i - 1][j - 1];
                }
            }
        }
        return dp;
    }

    public static String lcs2(String s1, String s2) {
        if (s1 == null || s2 == null || s1.equals("") || s2.equals("")) {
            return "";
        }
        char[] chs1 = s1.toCharArray();
        char[] chs2 = s2.toCharArray();
        int row = 0;// 斜线开始位置的行
        int col = chs2.length - 1;// 斜线开始位置的列
        int max = 0;// 子串最大长度
        int end = 0;// 记录最长子串结束位置
        while (row < chs1.length) {
            int i = row;
            int j = col;
            int len = 0;
            while (i < chs1.length && j < chs2.length) {
                if (chs1[i] != chs2[j]) {
                    len = 0;
                } else {
                    len++;
                }
                if (len > max) {
                    max = len;
                    end = i;
                }
                i++;
                j++;
            }
            if (col > 0) {
                col--;
            } else {
                row++;
            }
        }
        return s1.substring(end - max + 1, end + 1);
    }

    // 求两个字符串的最长公共子序列
    // 从结尾讨论，例如dp[i][j]的值与是否与i和j作为结尾字符有关
    // 四种可能性
    // 1.i和j都为结尾，前提二者相等
    // 2.i作为结尾，j不是
    // 3.i不是，j作为结尾
    // 4.i和j都不是
    public static int[][] getDp2(char[] str1, char[] str2) {
        int[][] dp = new int[str1.length][str2.length];

        dp[0][0] = (str1[0] == str2[0] ? 1 : 0);
        for (int i = 1; i < str1.length; i++) {
            dp[i][0] = Math.max(dp[i - 1][0], str1[i] == str2[0] ? 1 : 0);
        }
        for (int j = 1; j < str2.length; j++) {
            dp[0][j] = Math.max(dp[0][j - 1], str1[0] == str2[j] ? 1 : 0);
        }

        for (int i = 1; i < str1.length; i++) {
            for (int j = 1; j < str2.length; j++) {
                //                  i不是，j作为结尾 i作为结尾，j不是
                dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]);
                if (str1[i] == str2[j]) {
                    //                       i和j都为结尾，前提二者相等
                    dp[i][j] = Math.max(dp[i][j], dp[i - 1][j - 1] + 1);
                }
            }
        }
        return dp;
    }

    public static String lcse(String str1, String str2) {
        if (str1 == null || str2 == null || "".equals(str1) || "".equals(str2)) {
            return "";
        }

        char[] chs1 = str1.toCharArray();
        char[] chs2 = str2.toCharArray();

        int[][] dp = getDp2(chs1, chs2);

        int m = chs1.length - 1;
        int n = chs2.length - 1;

        // 存储最长公共子序列
        char[] res = new char[dp[m][n]];
        int index = res.length - 1;
        while (index >= 0) {
            if (n > 0 && dp[m][n] == dp[m][n - 1]) {
                n--;
            } else if (m > 0 && dp[m][n] == dp[m - 1][n]) {
                m--;
            } else {
                res[index--] = chs1[m];
                m--;
                n--;
            }
        }

        return String.valueOf(res);

    }

    // 给定一个数组arr，长度为N且每个值都是正数，代表N个人的体重。再给定一个正数limit，代表一艘船的载重。
    // 以下是坐船规则，1）每艘船最多只能做两人；2）乘客的体重和不能超过limit。返回如果同时让这N个人过河最少需要几条船。
    public static int minBoat(int[] arr, int limit) {
        // arr排序
        if (arr == null || arr.length == 0) {
            return 0;
        }

        if (arr[arr.length - 1] <= (limit / 2)) {
            return (arr.length + 1) / 2;
        }

        if (arr[0] > (limit / 2)) {
            return arr.length;
        }

        int lessR = -1;
        for (int i = arr.length - 1; i >= 0; i--) {
            if (arr[i] <= (limit / 2)) {
                lessR = i;
                break;
            }
        }
        int l = lessR;
        int r = lessR + 1;
        int lessUnused = 0;
        while (l >= 0) {
            int solved = 0;
            while (r < arr.length && arr[l] + arr[r] <= limit) {
                r++;
                // 左边和右边能组队上船的个数
                solved++;
            }
            if (solved == 0) {
                // 左边无法上船的个数
                lessUnused++;
                l--;
            } else {
                l = Math.max(-1, l - solved);
            }
        }
        int lessAll = lessR + 1;
        // 右边和左边能组合上船的个数
        int lessUsed = lessAll - lessUnused;
        // 右边只能单独上船的个数
        int moreUnsolved = arr.length - lessR - 1 - lessUsed;
        return lessUsed + (lessUnused + 1) >> 1 + moreUnsolved;
    }

    // 给定一个字符串，求最长的回文子序列
    // 范围尝试，根据范围的开头和结尾讨论可能性有哪些，然后填表，确定dp状态转移方程式
    public static int maxLongestSubSequence(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        char[] str = s.toCharArray();
        int[][] dp = new int[str.length][str.length];
        // 初始化
        for (int i = 0; i < str.length; i++) {
            dp[i][i] = 1;
        }
        // 初始化
        for (int i = 0; i < str.length - 1; i++) {
            dp[i][i + 1] = str[i] == str[i + 1] ? 2 : 1;
        }
        for (int i = str.length - 2; i >= 0; i--) {
            for (int j = i + 2; j < str.length; j++) {
                dp[i][j] = Math.max(dp[i][j - 1], dp[i + 1][j]);
                if (str[i] == str[j]) {
                    dp[i][j] = Math.max(dp[i][j], dp[i + 1][j - 1] + 2);
                }
            }
        }
        return dp[0][str.length - 1];
    }

    // 给定一个字符串str，如果可以在str的任意位置添加字符，请返回添加字符最少的情况下，让str整体都是回文字符串
    public static int[][] getDP(char[] str) {
        int[][] dp = new int[str.length][str.length];
        for (int j = 1; j < str.length; j++) {
            dp[j - 1][j] = str[j - 1] == str[j] ? 0 : 1;
            for (int i = j - 2; i > -1; i--) {
                if (str[i] == str[j]) {
                    dp[i][j] = dp[i + 1][j - 1];
                } else {
                    dp[i][j] = Math.min(dp[i + 1][j], dp[i][j - 1]) + 1;
                }
            }
        }
        return dp;
    }

    public static String addMinCharsToPalindrome(String str) {
        if (str == null || str.length() < 2) {
            return str;
        }
        char[] chs = str.toCharArray();
        int[][] dp = getDP(chs);
        char[] res = new char[dp[0][chs.length - 1] + chs.length];
        int i = 0;
        int j = chs.length - 1;
        int resi = 0;
        int resj = res.length - 1;
        // 随机还原路径
        while (i <= j) {
            if (chs[i] == chs[j]) {
                res[resi++] = chs[i++];
                res[resj--] = chs[j--];
            } else if (dp[i][j - 1] < dp[i + 1][j]) {
                res[resi++] = chs[j];
                res[resj--] = chs[j--];
            } else {
                res[resj--] = chs[i];
                res[resi++] = chs[i++];
            }
        }
        return String.valueOf(res);
    }

    // 给定一个字符串str，返回把str全部切割成回文子串的最小分割数
    // 从左往右尝试模型，得到所有的可能性，填表，获得状态转移方程式
    // 优化的时候需要使用范围尝试记录i到j位置的字符串是否为回文字符串
    public static int minPartitionsGetPalindrome(String str) {
        if (str == null || str.equals("")) {
            return 0;
        }
        char[] chs = str.toCharArray();
        int len = chs.length;
        int[] dp = new int[len + 1];
        dp[len] = 0;
        dp[len - 1] = 1;
        boolean[][] isValid = store(chs);
        for (int i = len - 2; i >= 0; i--) {
            dp[i] = Integer.MIN_VALUE;
            for (int j = i; j < len; j++) {// i...j范围
                if (isValid[i][j]) {// i到j是回文字符串 判断j+1后面的位置
                    dp[i] = Math.min(dp[i], dp[j + 1] + 1);
                }
            }
        }
        return dp[0];
    }

    public static boolean[][] store(char[] chs) {
        boolean[][] res = new boolean[chs.length][chs.length];
        res[chs.length - 1][chs.length - 1] = true;
        for (int i = 0; i < chs.length - 1; i++) {
            res[i][i] = true;
            res[i][i + 1] = chs[i] == chs[i + 1];
        }
        for (int i = chs.length - 3; i >= 0; i--) {
            for (int j = i + 2; j < chs.length; j++) {
                res[i][j] = chs[i] == chs[j] && res[i + 1][j - 1];
            }
        }
        return res;
    }

    // 对于一个字符串, 从前开始读和从后开始读是一样的, 我们就称这个字符串是回文串。例如"ABCBA","AA", "A" 是回文串, 而"ABCD", "AAB"不是回文串。
    // 牛牛特别喜欢回文串, 他手中有一个字符串s, 牛牛在思考能否从字符串中移除部分(0个或多个)字符使其变为回文串，并且牛牛认为空串不是回文串。
    // 牛牛发现移除的方案可能有很多种, 希望你来帮他计算一下一共有多少种移除方案可以使s变为回文串。
    // 对于两种移除方案, 如果移除的字符依次构成的序列不一样就是不同的方案。
    // 例如，XXY 4种 ABA 5种
    //【说明】 这是今年的原题，提供的说明和例子都很让人费解。现在根据当时题目的所有测试用例，重新解释当时的题目含义:
    // 1)"1AB23CD21"，你可以选择删除A、B、C、D，然后剩下子序列{1,2,3,2,1}，
    // 只要剩下的子序列是同一个，那么就只算1种方法，和A、B、C、D选择什么样的删除顺序没有关系。
    // 2)"121A1"，其中有两个{1,2,1}的子序列，第一个{1,2,1}是由{位置0，位置1，位置2}构成，第二个{1,2,1}是由{位置0，位置1，位置4}构成。
    // 这两个子序列被认为是不同的子序列。也就是说在本题中，认为字面值一样 但是位置不同的字符就是不同的。
    // 3)其实这道题是想求，str中有多少个不同的子序列，每一种子序列只对应一种删除方法，那就是把多余的东西去掉，而和去掉的顺序无关。
    // 4)也许你觉得我的解释很荒谬，但真的是这样，不然解释不了为什么，XXY 4种 ABA 5种，而且其他的测试用例都印证了这一点。
    public static int getPalindromeByRemoveChars(String str) {
        char[] s = str.toCharArray();
        int n = s.length;
        int[][] dp = new int[n][n];
        for (int i = 0; i < n; i++) {
            dp[i][i] = 1;
        }
        for (int i = 0; i < n - 1; i++) {
            dp[i][i + 1] = s[i] == s[i + 1] ? 3 : 2;
        }
        for (int L = n - 3; L >= 0; L--) {
            for (int R = L + 2; R < n; R++) {
                dp[L][R] = dp[L + 1][R] // 不以l开头，可以以r结尾或者不以r结尾
                        + dp[L][R - 1]  // 可以以l开头或者不以l开头，但不以r结尾
                        - dp[L + 1][R - 1];// 因为多加了不以l开头和不以r结尾的可能性，要减去
                if (s[L] == s[R]) {
                    dp[L][R] += dp[L + 1][R - 1] + 1;
                }
            }
        }
        return dp[0][n - 1];
    }

    // 一个无序数组中，求最小的第k个数
    // bfprt算法
    // 在arr[begin...end]范围中，求如果排序的话，i位置的数是谁
    public static int bfprt(int[] arr, int begin, int end, int i) {
        if (begin == end) {
            return arr[begin];
        }
        // 分组 + 组内排序 + 组成newArr + 选出newArr的上中位数pivot
        int pivot = medianOfMedians(arr, begin, end);
        // 根据pivot划分取值分成三部分 <pivot =pivot >pivot，返回等于区域的左边界和右边界
        // pivotRange[0] 等于区域的左边界
        // pivotRange[0] 等于区域的右边界
        int[] pivotRange = partition(arr, begin, end, pivot);
        if (i >= pivotRange[0] && i < pivotRange[1]) {
            return arr[i];
        } else if (i < pivotRange[0]) {
            return bfprt(arr, begin, pivotRange[0] - 1, i);
        } else {
            return bfprt(arr, pivotRange[1] + 1, end, i);
        }
    }

    // 获取中位数中的上中位数
    private static int medianOfMedians(int[] arr, int begin, int end) {
        int num = end - begin + 1;
        int offset = num % 5 == 0 ? 0 : 1;
        int[] mArr = new int[num / 5 + offset];
        for (int i = 0; i < mArr.length; i++) {
            int beginI = begin + i * 5;
            int endI = beginI + 4;
            mArr[i] = getMedian(arr, beginI, Math.min(end, endI));
        }
        return bfprt(mArr, 0, mArr.length - 1, mArr.length / 2);
    }

    private static int getMedian(int[] arr, int begin, int end) {
        insertionSort(arr, begin, end);
        int sum = end + begin;
        int mid = sum / 2 + sum % 2;
        return arr[mid];
    }

    private static void insertionSort(int[] arr, int begin, int end) {
        if (arr == null || arr.length < 2) {
            return;
        }
        for (int i = begin; i < end; i++) {
            int newNumIndex = i;
            while (newNumIndex - 1 >= 0 && arr[newNumIndex - 1] > arr[newNumIndex]) {
                swap(arr, newNumIndex - 1, newNumIndex);
                newNumIndex--;
            }
        }
    }

    // 根据pivot划分取值分成三部分 <pivot =pivot >pivot，返回等于区域的左边界和右边界
    private static int[] partition(int[] arr, int begin, int end, int pivot) {
        int small = begin - 1;
        int cur = begin;
        int big = end + 1;
        while (cur != big) {
            if (arr[cur] < pivot) {
                swap(arr, ++small, cur++);
            } else if (arr[cur] > pivot) {
                swap(arr, cur, --big);
            } else {
                cur++;
            }
        }
        int[] range = new int[2];
        range[0] = small + 1;
        range[1] = big - 1;
        return range;
    }

    private static void swap(int[] arr, int i1, int i2) {
        int temp = arr[i1];
        arr[i1] = arr[i2];
        arr[i2] = temp;
    }

    // 给定一个正数 1，裂开的方法有一种：(1)
    // 给定一个正数 2，裂开的方法有一种：(1，1)，(2)
    // 给定一个正数 3，裂开的方法有一种：(1，1，1)，(1，2)，(3)
    // 给定一个正数 4，裂开的方法有一种：(1，1，1，1)，(1，1，2)，(1，3)，(2，2)，(4)
    // 给定一个正数 n，求裂开的方法数。
    public static int getPartitionWays(int n) {
        if (n < 1) {
            return 0;
        }
        return process(1, n);
    }

    // pre 之前裂开的部分
    // rest 还剩余多少，但是剩余的一定要比之前裂开pre的大
    public static int process(int pre, int rest) {
        if (rest == 0) {
            return 1;
        }
        if (pre > rest) {// 例如求3 只能出现1 2这样的裂法，不能出现2 1这样的
            return 0;
        }
        int ways = 0;
        for (int i = pre; i <= rest; i++) {
            ways += process(i, rest - i);
        }
        return ways;
    }

    // 动态规划版本
    public static int getPartitionWays2(int n) {
        if (n < 1) {
            return 0;
        }
        int[][] dp = new int[n + 1][n + 1];
        for (int pre = 1; pre < dp.length; pre++) {
            dp[pre][0] = 1;
        }
        for (int pre = n; pre >= 1; pre--) {
            for (int rest = pre; rest <= n; rest++) {
                for (int i = pre; i <= rest; i++) {
                    dp[pre][rest] += dp[i][rest - i];
                }
            }
        }
        return dp[1][n];
    }

    // 动态规划之斜率优化
    public static int getPartitionWays3(int n) {
        if (n < 1) {
            return 0;
        }
        int[][] dp = new int[n + 1][n + 1];
        for (int pre = 1; pre < dp.length; pre++) {
            dp[pre][0] = 1;
        }
        for (int pre = n - 1; pre >= 1; pre--) {
            for (int rest = pre + 1; rest <= n; rest++) {
                dp[pre][rest] = dp[pre][rest - pre] + dp[pre + 1][rest];
            }
        }
        return dp[1][n];
    }

    // 给定一个棵二叉树的头节点，已知所有节点的值都不一样，返回其中最大的且符合搜索二叉树条件的最大拓扑结构的大小
    // 拓扑结构：不是子树，是要能连起来就是

    public static class MyTreeNode {
        public int value;
        public MyTreeNode left;
        public MyTreeNode right;

        public MyTreeNode(int data) {
            this.value = data;
        }
    }

    public static int bstTopoSize(Node head) {
        if (head == null) {
            return 0;
        }
        int max = maxTop(head, head);
        max = Math.max(bstTopoSize(head.left), max);
        max = Math.max(bstTopoSize(head.right), max);
        return max;
    }

    private static int maxTop(Node head, Node node) {
        if (node != null && isBSTNode(head, node, node.value)) {
            return maxTop(head, node.left) + maxTop(head, node.right) + 1;
        }
        return 0;
    }

    // 判断是否是BST节点
    private static boolean isBSTNode(Node head, Node node, int value) {
        if (head == null) {
            return false;
        }
        if (node == null) {
            return true;
        }
        return isBSTNode(head.value > value ? head.left : head.right, node, value);
    }

    // 套路解法
    public static class Record {
        public int l;
        public int r;

        public Record(int left, int right) {
            this.l = left;
            this.r = right;
        }
    }

    public static int bstTopoSize2(MyTreeNode head) {
        if (head == null) {
            return 0;
        }
        HashMap<MyTreeNode, Record> map = new HashMap<>();
        return posOrderBTS(head, map);
    }

    private static int posOrderBTS(MyTreeNode head, HashMap<MyTreeNode, Record> map) {
        if (head == null) {
            return 0;
        }
        // 左边最大搜索值
        int leftMax = posOrderBTS(head.left, map);
        // 右边最大搜索值
        int rightMax = posOrderBTS(head.right, map);
        // map更新,依据当前值
        modifyMap(head.left, head.value, map, true);
        modifyMap(head.right, head.value, map, false);
        // 更新完毕
        Record leftR = map.get(head.left);
        Record rightR = map.get(head.right);
        int leftNum = leftR == null ? 0 : leftR.l + leftR.r + 1;
        int rightNum = rightR == null ? 0 : rightR.l + rightR.r + 1;
        map.put(head, new Record(leftNum, rightNum));
        return Math.max(leftNum + rightNum + 1, Math.max(leftMax, rightMax));
    }

    // 更新树贡献值
    private static int modifyMap(MyTreeNode child, Integer value, HashMap<MyTreeNode, Record> map, boolean isLeft) {
        if (child == null || !map.containsKey(child)) {
            return 0;
        }
        Record record = map.get(child);
        if ((isLeft && child.value > value)
                || (!isLeft && child.value < value)) {
            // 发现当前节点不满足搜索二叉树条件时，返回当前节点的全部贡献作为要减掉的值,并在map中删掉当前节点
            map.remove(child);
            return 1 + record.l + record.r;
        } else {
            // 左孩子一直往右边遍历，右孩子一直往左边遍历，只要一个不符合整个树就咔嚓掉，并把自己咔嚓掉的数量返回到上层也减掉
            int sub = modifyMap(isLeft ? child.right : child.left, value, map, isLeft);
            if (isLeft) {
                record.r = (record.r - sub);
            } else {
                record.l = (record.l - sub);
            }
            return sub;
        }
    }

    // 给定一个长度为偶数的数组arr，长度记为2*N。前N个为左部分，后N个为右部分。
    // arr就可以表示为{L1,L2,…,Ln,R1,R2,…,Rn}，请将数组调整成{R1,L1,R2,L2,…,Rn,Ln}的样子。要求空间复杂度O（1），时间复杂度尽量低
    // 数组长度为len，调整之前在i位置，返回调整之后的位置
    // 下标从1开始
    public static int modifyIndex(int i, int len) {
        if (i <= len / 2) {
            return 2 * i;
        } else {
            return 2 * (i - len / 2) - 1;
        }
    }

    public static void shuffle(int[] arr) {
        if (arr != null && arr.length != 0 && (arr.length & 1) == 0) {
            shuffle(arr, 0, arr.length - 1);
        }
    }

    public static void shuffle(int[] arr, int l, int r) {
        while (r - l + 1 > 0) {
            int len = r - l + 1;
            int base = 3;
            int k = 1;
            // 计算小于等于len且离len最近满足3^(k) - 1的数
            // 因为3^(k) - 1的数有固定出发点3^(k-1) 1 3 9.....
            while (base <= (len + 1) / 3) {
                base = base * 3;
                k++;
            }
            // 当前要解决长度为base-1的块，一半就是再除以2
            int half = (base - 1) / 2;
            // 找到[l..r]中间位置
            int mid = (l + r) / 2;
            // 要左旋的部分为l + half......mid，要进行右旋的部分为mid+1.....mid + half
            rotate(arr, l + half, mid, mid + half);
            // 旋转完成之后，从l开始算起，长度为base - 1的部分进行下标连续推
            cycle(arr, l, base - 1, k);
            l = l + base - 1;
        }
    }

    // 从start位置开始，往后数len的长度这一段，做下标连续推
    // 出发位置为1，3，9
    private static void cycle(int[] arr, int start, int len, int k) {
        for (int i = 0, trigger = 1; i < k; i++, trigger *= 3) {
            int preValue = arr[trigger + start - 1];
            int cur = modifyIndex(trigger, len);
            while (cur != trigger) {
                int tmp = arr[cur + start - 1];
                arr[cur + start - 1] = preValue;
                preValue = tmp;
                cur = modifyIndex(cur, len);
            }
            arr[cur + start - 1] = preValue;
        }
    }

    // [l...mid]左侧部分，[mid + 1，r]为右侧部分
    private static void rotate(int[] arr, int l, int mid, int r) {
        // 先逆序左侧部分
        reverse(arr, l, mid);
        // 再逆序右侧部分
        reverse(arr, mid + 1, r);
        // 整体逆序
        reverse(arr, l, r);
    }

    private static void reverse(int[] arr, int l, int r) {
        while (l < r) {
            int temp = arr[l];
            arr[l] = arr[r];
            arr[r] = temp;
            l++;
            r--;
        }
    }

    // 给定一个无序数组arr，请将数组调整为依次相邻的数字，总是先<=、再>=的关系，并交替下去。
    // 比如数组中有五个数字，调整成[a,b,c,d,e],使之满足a<=b>=c<=d>=e这种关系
    // 要求空间复杂度为O(1)
    // 1.使用堆排序
    // 2.如果数组长度是偶数，使用完美洗牌，然后两个数两个数进行交换位置
    // 3.如果数组长度是奇数，不管第一个位置的元素，后对面的偶数个元素进行完美洗牌，然后再对后面的元素两个数两个数进行交换


    // 判定一个由[a - z]字符构成的字符串和一个包含'.'和'*'通配符的字符串是否匹配，'.'匹配任意单一字符，'*'匹配任意多个字符包括0个字符
    public static boolean isMatch(String str, String exp) {
        if (str == null || exp == null) {
            return false;
        }
        char[] s = str.toCharArray();
        char[] e = exp.toCharArray();
        return isValid(s, e) && process(s, e, 0, 0);
    }

    private static boolean isValid(char[] s, char[] e) {
        for (int i = 0; i < s.length; i++) {
            if (s[i] == '*' || s[i] == '.') {
                return false;
            }
        }
        for (int i = 1; i < e.length; i++) {
            if (e[i] == '*' && ((i - 1) == 0 || e[i - 1] == '*')) {
                return false;
            }
        }
        return true;
    }

    // s[si....] 看e[ei....]能否构成
    // 保证ei不能走到*也就是说每一次必须要跳过*的位置，因为*必须要配合前面的字符使用
    public static boolean process(char[] s, char[] e, int si, int ei) {
        if (ei == e.length) {
            return si == s.length;
        }
        // 可能性1 ei +1位置不是*
        // 只有si位置字符和ei位置相等才能继续下一步，因此ei位置可以是与si位置相等的字符，也可也是.字符
        if (ei + 1 != e.length || e[ei + 1] != '*') {
            return si != s.length && (e[ei] == s[si] || e[ei] == '.')
                    && process(s, e, si + 1, ei + 1);
        }
        // 可能性2 ei +1位置是*
        // 枚举所有前缀与*的可能组合
        while (si != s.length && (e[ei] == s[si]) || e[ei] == '.') {
            if (process(s, e, si, ei + 2)) {
                return true;
            }
            si++;
        }
        // 最后一个前缀和*的组合尝试，或者一开始si位置和ei位置就不等，只能选择前缀变成0个选择，看后面是否能匹配
        return process(s, e, si, ei + 2);
    }

    public static boolean isMatchDp(String str, String exp) {
        if (str == null || exp == null) {
            return false;
        }
        char[] s = str.toCharArray();
        char[] e = exp.toCharArray();
        if (!isValid(s, e)) {
            return false;
        }
        boolean[][] dp = initDp(s, e);
        for (int i = s.length - 1; i > -1; i--) {
            for (int j = e.length - 2; j > -1; j--) {
                if (e[j + 1] != '*') {// 不是*号
                    dp[i][j] = (s[i] == e[j] || e[j] == '.') && dp[i + 1][j + 1];
                } else {// 是*号
                    int si = i;
                    while (si != s.length && (s[si] == e[j] || e[j] == '.')) {
                        if (dp[si][j + 2]) {
                            dp[i][j] = true;
                            break;
                        }
                        si++;
                    }
                    if (!dp[i][j]) {
                        dp[i][j] = dp[si][j + 2];
                    }
                }
            }
        }
        return dp[0][0];
    }

    private static boolean[][] initDp(char[] s, char[] e) {
        int sLen = s.length;
        int eLen = e.length;
        boolean[][] dp = new boolean[sLen + 1][eLen + 1];
        dp[sLen][eLen] = true;
        // 填最后一行的初始化数据
        // s中已经没有字符可以选，只有出现了a*b*c*这样的才能出现空串与子匹配
        for (int j = eLen - 2; j > -1; j = j - 2) {
            if (e[j] != '*' && e[j + 1] == '*') {
                dp[sLen][j] = true;
            } else {
                break;
            }
        }
        // s字符串中还剩最后一个字符可选 e字符串中还剩最后一个字符可选
        // 看这种情况二者是否匹配
        if (sLen > 0 && eLen > 0) {
            if ((e[eLen - 1] == '.' || s[sLen - 1] == e[eLen - 1])) {
                dp[sLen - 1][eLen - 1] = true;
            }
        }
        return dp;
    }

    // 数组异或和：数组中所有的数异或起来得到的值
    // 给一个整型数组arr，有正有负有0，求子数组的最大异或和
    // 暴力遍历1
    public static int maxEOR(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        //尝试必须以arr[i]结尾的子数组，最大异或和是多少，尝试所有的arr[i]
        int ans = 0;
        for (int i = 0; i < arr.length; i++) {
            //必须以arr[i]结尾
            //0..i每一个开头
            for (int start = 0; start <= i; start++) {
                //arr[start...i]这个子数组
                int sum = 0;
                for (int index = start; index <= i; index++) {
                    sum ^= arr[index];
                }
                ans = Math.max(ans, sum);
            }
        }
        return ans;
    }

    // 暴力遍历2
    public static int maxEOR2(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        //preSum[i] = arr[0..i]的异或和
        int[] preSum = new int[arr.length];
        preSum[0] = arr[0];
        for (int i = 1; i < arr.length; i++) {
            preSum[i] = arr[i] ^ preSum[i - 1];
        }

        //尝试必须以arr[i]结尾的子数组，最大异或和是多少，尝试所有的arr[i]
        int ans = 0;
        for (int i = 0; i < arr.length; i++) {
            //必须以arr[i]结尾
            //0..i每一个开头
            for (int start = 0; start <= i; start++) {
                //arr[start...i]这个子数组
                int sum = preSum[i] ^ (start - 1 == -1 ? 0 : preSum[start - 1]);
                //arr[start..i]异或和 = arr[0..i] ^ arr[0..start - 1]
                ans = Math.max(ans, sum);
            }
        }
        return ans;
    }

    // 前缀树解决
    // arr[11, 1, 15, 10, 13, 4]
    //     0   1  2   3   4   5
    //异或和：无 = 0 = 0000
    // arr[0..0] = 11 = 1011
    // arr[0..1] = 11 ^ 1 = 1010
    // arr[0..2] = 0101
    // arr[0..3] = 1111
    // arr[0..4] = 0010
    // arr[0..5] = 0110 = sum
    // 贪心策略：首先满足高位为1
    //把所有前缀异或和，加入到NumTrie,并按照前缀树组织
    public static class NumTrie {
        public static class Node {
            // 走向0的路和走向1的路
            public Node[] nexts = new Node[2];
        }

        public Node head = new Node();

        public void add(int num) {
            Node cur = head;
            for (int move = 31; move >= 0; move--) {//move:向右移多少位
                int path = ((num >> move) & 1);
                cur.nexts[path] = cur.nexts[path] == null ? new Node() : cur.nexts[path];
                cur = cur.nexts[path];
            }
        }

        //sum最希望遇到的路径，最大的异或结果返回 O(32)
        public int maxXor(int sum) {
            Node cur = head;
            int res = 0;//最后的结果（num ^ 最优选择）所得到的值
            for (int move = 31; move >= 0; move--) {
                //当前位如果是0，path就是整数0
                //当前位如果是1，path就是整数1
                int path = (sum >> move) & 1; //num 第move位置上的状态提取出来
                //sum该位的状态，最期待的路
                int best = move == 31 ? path : (path ^ 1);
                //best : 最期待的路 -> 实际走的路
                best = cur.nexts[best] != null ? best : (best ^ 1);
                //path num第move位的状态，best是根据path实际走的路
                res |= (path ^ best) << move;
                cur = cur.nexts[best];
            }
            return res;

        }
    }

    public static int maxXorSubarray(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        int max = Integer.MIN_VALUE;
        int sum = 0; // 一个数也没有的时候，异或和为0
        NumTrie numTrie = new NumTrie();
        numTrie.add(0);
        for (int i = 0; i < arr.length; i++) {
            sum ^= arr[i]; //sum -> 0 ~ i 异或和
            //numTrie 装着所有：一个数也没有、0~0、 0~1、0~2、 0~i-1
            max = Math.max(max, numTrie.maxXor(sum));
            numTrie.add(sum);
        }
        return max;
    }

    // 给定一个数组 arr，代表一排有分数的气球。每打爆一个气球都能获得分数，假设打爆气球的分数为 X，获得分数的规则如下：
    // 如果被打爆气球的左边有没打爆的气球，找到离被打爆气球最近的气球，假设分数为 L；如果被打爆气球的右边有没有打爆的气球，找到离被打爆气球最近的气球，假设分数为 R。获得分数为 L * X * R
    // 如果被打爆气球的左边有没打爆的气球，找到离被打爆气球最近的气球，假设分数为 L；如果被打爆气球的右边所有气球都已经被打爆。获得分数为：L*X
    // 如果被打爆气球的左边所有气球都已经被打爆；如果被打爆气球的右边有没有打爆的气球，找到离被打爆气球最近的气球，假设分数为 R。获得分数为：R*X
    // 如果被打爆气球的左边和右边所有气球都已经被打爆。获得分数为：X
    // 目标是打爆所有气球，返回能获得的最大分数。
    //【举例】
    // arr =【3,2,5】
    // 如果先打爆 3，获得 3*2；再打爆 2，获得 2* 5 ;最后打爆 5 ，获得 5；总分为：6+10+5=21.
    // 如果先打爆 3，获得 3*2；再打爆 5，获得 2* 5 ;最后打爆 2 ，获得 2；总分为：6+10+2=18.
    // 如果先打爆 2，获得 3*2*5；再打爆 3，获得 3* 5 ;最后打爆 5 ，获得 5；总分为：30+15+5=50.
    // 如果先打爆 2，获得 3*2*5；再打爆 5，获得 3* 5 ;最后打爆 3 ，获得 3；总分为：30+15+3=48.
    // 如果先打爆 5，获得 2*5；再打爆 3，获得 3* 2 ;最后打爆 2 ，获得 2；总分为：10+6+2=18.
    // 如果先打爆 5，获得 2*5；再打爆 2，获得 3* 2 ;最后打爆 3 ，获得 3；总分为：10+6+3=19.
    // 返回最大分数为 50
    public static int maxCoins1(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        if (arr.length == 1) {
            return arr[0];
        }
        int N = arr.length;
        int[] help = new int[N + 2];
        help[0] = 1;
        help[N + 1] = 1;
        for (int i = 0; i < N; i++) {
            help[i + 1] = arr[i];
        }
        return process(help, 1, N);
    }

    // 打爆arr[L..R]范围上的所有气球，返回最大的分数
    // 假设arr[L-1]和arr[R+1]一定没有被打爆
    // 尝试的方式：每一个位置的气球都最后被打爆
    public static int process(int[] arr, int L, int R) {
        if (L == R) {// 如果arr[L..R]范围上只有一个气球，直接打爆即可
            return arr[L - 1] * arr[L] * arr[R + 1];
        }
        // 最后打爆arr[L]的方案，和最后打爆arr[R]的方案，先比较一下
        int max = Math.max(
                arr[L - 1] * arr[L] * arr[R + 1] + process(arr, L + 1, R),
                arr[L - 1] * arr[R] * arr[R + 1] + process(arr, L, R - 1));
        // 尝试中间位置的气球最后被打爆的每一种方案
        for (int i = L + 1; i < R; i++) {
            max = Math.max(max,
                    arr[L - 1] * arr[i] * arr[R + 1] + process(arr, L, i - 1)
                            + process(arr, i + 1, R));
        }
        return max;
    }

    // 汉诺塔游戏的要求把所有的圆盘从左边都移到右边的柱子上，给定一个整型数组arr，其中只含有1、2、3，代表所有圆盘目前的状态，
    // 1 代表左柱，2代表中柱，3 代表右柱，arr[i] 的值代表第 i + 1 个圆盘的位置。
    // 比如：arr = [3,3,2,1] ，代表第 1 个圆盘在右柱上，第 2 个圆盘在右柱上，第 3 个圆盘在中柱上，第 4 个圆盘在左柱上。
    // 如果 arr 代表的状态是最优移动轨迹过程中出现的状态，返回 arr 这种状态是最优移动轨迹中第几状态；
    // 如果 arr 代表的状态不是最优移动轨迹过程中出现的状态，则返回 -1。
    public static int step1(int[] arr) {
        if (arr == null || arr.length == 0) {
            return -1;
        }
        return process(arr, arr.length - 1, 1, 2, 3);
    }

    // 目标是：把arr[0~i]的圆盘，从from全部挪到to上
    // 返回，根据arr中的状态arr[0..i]，它是最优解的第几步？
    // O(N)
    public static int process(int[] arr, int i, int from, int other, int to) {
        if (i == -1) {
            return 0;
        }
        if (arr[i] != from && arr[i] != to) {
            return -1;
        }
        if (arr[i] == from) { // 第一大步没走完
            return process(arr, i - 1, from, to, other);
        } else { // arr[i] == to
            int rest = process(arr, i - 1, other, from, to);// 第三大步完成的程度
            if (rest == -1) {
                return -1;
            }
            return (1 << i) + rest;
        }
    }

    // 一个字符串可以分解成多种二叉树结构。如果 str 长度为 1 ，认为不可分解。
    // 如果 str 长度为 N（N > 1）,左部分长度可 以为 1 ~ N - 1，剩下的为右部分的长度。
    // 左部分和右部分都可以按照同样的逻辑，继续分解。形成的所有结构都是 str 的二叉树结构
    public static boolean isScramble(String s1, String s2) {
        if ((s1 == null && s2 != null) || (s1 != null && s2 == null)) {
            return false;
        }
        if (s1 == null && s2 == null) {
            return true;
        }
        if (s1.equals(s2)) {
            return true;
        }
        char[] str1 = s1.toCharArray();
        char[] str2 = s2.toCharArray();
        if (!sameTypeSameNumber(str1, str2)) {
            return false;
        }
        int N = s1.length();
        return process(str1, str2, 0, 0, N);
    }

    private static boolean sameTypeSameNumber(char[] str1, char[] str2) {
        if (str1.length != str2.length) {
            return false;
        }
        int[] map = new int[256];
        for (char str : str1) {
            map[str]++;
        }
        for (char str : str2) {
            if (--map[str] < 0) {
                return false;
            }
        }
        return true;
    }

    //返回str1[从L1开始往右长度为size的子串]和str2[从L2开始往右长度为size的子串]是否互为旋变字符串
    //在str1中的这一段和str2中的这一段一定是等长的，所以只用一个参数size
    public static boolean process(char[] str1, char[] str2, int L1, int L2, int size) {
        if (size == 1) {
            return str1[L1] == str2[L2];
        }
        //枚举每一种情况，有一个计算出互为旋变就返回true。都算不出来就返回false
        for (int leftPart = 1; leftPart < size; leftPart++) {
            if (
                //如果1左对2左，并且1右对2右
                    (process(str1, str2, L1, L2, leftPart)
                            &&
                            process(str1, str2, L1 + leftPart, L2 + leftPart, size - leftPart))
                            ||
                            //如果1左对2右，并且1右对2左
                            (process(str1, str2, L1, L2 + size - leftPart, leftPart)
                                    &&
                                    process(str1, str2, L1 + leftPart, L2, size - leftPart))) {
                return true;
            }
        }
        return false;
    }

    public static boolean dpCheck(String s1, String s2) {
        if ((s1 == null && s2 != null) || (s1 != null && s2 == null)) {
            return false;
        }
        if (s1 == null && s2 == null) {
            return true;
        }
        if (s1.equals(s2)) {
            return true;
        }
        char[] str1 = s1.toCharArray();
        char[] str2 = s2.toCharArray();
        if (!sameTypeSameNumber(str1, str2)) {
            return false;
        }
        int N = str1.length;

        //L1的可能性 L2的可能性 size可能性
        boolean[][][] dp = new boolean[N][N][N + 1];
        for (int L1 = 0; L1 < N; L1++) {
            for (int L2 = 0; L2 < N; L2++) {
                dp[L1][L2][1] = str1[L1] == str2[L2];
            }
        }
        // 层数
        for (int size = 2; size <= N; size++) {
            //本层的东西，不互相依赖
            //任何一个dp[i][j][size]都依赖dp[...][...][K] (k < size)
            for (int L1 = 0; L1 <= N - size; L1++) {
                for (int L2 = 0; L2 <= N - size; L2++) {
                    dp[L1][L2][size] = false;
                    for (int leftPart = 1; leftPart < size; leftPart++) {
                        if (//如果1左对2左，并且1右对2右
                                ((dp[L1][L2][leftPart]) && (dp[L1 + leftPart][L2 + leftPart][size - leftPart]))
                                        ||
                                        //如果1左对2右，并且1右对2左
                                        (dp[L1][L2 + size - leftPart][leftPart]) && dp[L1 + leftPart][L2][size - leftPart]) {
                            // 已经尝试出了一种情况，不用在尝试了
                            dp[L1][L2][size] = true;
                            break;
                        }
                    }
                }
            }
        }
        return dp[0][0][N];
    }

    // 给定字符串str1和str2，求str1的子串中包含有str2所有字符的最小字串长度(与顺序无关)
    public static int minLength(String str1, String str2) {
        if (str1 == null || str2 == null || str1.length() < str2.length()) {
            return 0;
        }
        char[] s1 = str1.toCharArray();
        char[] s2 = str2.toCharArray();
        int[] map = new int[256];
        for (int i = 0; i != s2.length; i++) {
            map[s2[i]]++;
        }
        int left = 0;
        int right = 0;
        int matchLength = str2.length();
        int minLen = Integer.MAX_VALUE;
        while (right < s1.length) {
            map[s1[right]]--;
            if (map[s1[right]] >= 0) {
                matchLength--;
            }
            if (matchLength == 0) {
                while (map[s1[left]] < 0) {
                    map[s1[left++]]++;
                }
                minLen = Math.min(minLen, right - left + 1);
                matchLength++;
                map[s1[left++]]++;
            }
            right++;
        }
        return minLen == Integer.MAX_VALUE ? 0 : minLen;
    }

    // LFU缓存
    // 一个缓存结构需要实现如下功能：
    // void set(int key,int value)：加入或者修改 key 对应的 value
    // int get(int key)：查询 key 对应的 value 值
    // 但是缓存最多放 K 条记录，如果新的 K + 1 条记录需要加入，就需要根据策略删掉一条记录，然后才能把新记录加入。
    // 这个策略为：在缓存结构的 K 条记录中，哪一个 key 从进入缓存结构的时刻开始，被调用 set 或者 get 次数最少，就删掉这个key 的记录；
    // 如果调用次数最少的 key 有多个，上次调用发送最早的 key 被删除。
    // 这个就是 LFU 缓存替换算法。实现这个结构，K 作为参数。
    // 本题测试链接 : https://leetcode.com/problems/lfu-cache/
    // 提交时把类名和构造方法名改为 : LFUCache
    public static class LFUCache {

        public LFUCache(int K) {
            capacity = K;
            size = 0;
            records = new HashMap<>();
            heads = new HashMap<>();
            headList = null;
        }

        private int capacity; // 缓存的大小限制，即K
        private int size; // 缓存目前有多少个节点
        private HashMap<Integer, Node> records;// 表示key(Integer)由哪个节点(Node)代表
        private HashMap<Node, NodeList> heads; // 表示节点(Node)在哪个桶(NodeList)里
        private NodeList headList; // 整个结构中位于最左的桶

        // 节点的数据结构
        public static class Node {
            public Integer key;
            public Integer value;
            public Integer times; // 这个节点发生get或者set的次数总和
            public Node up; // 节点之间是双向链表所以有上一个节点
            public Node down;// 节点之间是双向链表所以有下一个节点

            public Node(int k, int v, int t) {
                key = k;
                value = v;
                times = t;
            }
        }

        // 桶结构
        public static class NodeList {
            public Node head; // 桶的头节点
            public Node tail; // 桶的尾节点
            public NodeList last; // 桶之间是双向链表所以有前一个桶
            public NodeList next; // 桶之间是双向链表所以有后一个桶

            public NodeList(Node node) {
                head = node;
                tail = node;
            }

            // 把一个新的节点加入这个桶，新的节点都放在顶端变成新的头部
            public void addNodeFromHead(Node newHead) {
                newHead.down = head;
                head.up = newHead;
                head = newHead;
            }

            // 判断这个桶是不是空的
            public boolean isEmpty() {
                return head == null;
            }

            // 删除node节点并保证node的上下环境重新连接
            public void deleteNode(Node node) {
                if (head == tail) {
                    head = null;
                    tail = null;
                } else {
                    if (node == head) {
                        head = node.down;
                        head.up = null;
                    } else if (node == tail) {
                        tail = node.up;
                        tail.down = null;
                    } else {
                        node.up.down = node.down;
                        node.down.up = node.up;
                    }
                }
                node.up = null;
                node.down = null;
            }
        }

        // removeNodeList：刚刚减少了一个节点的桶
        // 这个函数的功能是，判断刚刚减少了一个节点的桶是不是已经空了。
        // 1）如果不空，什么也不做
        //
        // 2)如果空了，removeNodeList还是整个缓存结构最左的桶(headList)。
        // 删掉这个桶的同时也要让最左的桶变成removeNodeList的下一个。
        //
        // 3)如果空了，removeNodeList不是整个缓存结构最左的桶(headList)。
        // 把这个桶删除，并保证上一个的桶和下一个桶之间还是双向链表的连接方式
        //
        // 函数的返回值表示刚刚减少了一个节点的桶是不是已经空了，空了返回true；不空返回false
        private boolean modifyHeadList(NodeList removeNodeList) {
            if (removeNodeList.isEmpty()) {
                if (headList == removeNodeList) {
                    headList = removeNodeList.next;
                    if (headList != null) {
                        headList.last = null;
                    }
                } else {
                    removeNodeList.last.next = removeNodeList.next;
                    if (removeNodeList.next != null) {
                        removeNodeList.next.last = removeNodeList.last;
                    }
                }
                return true;
            }
            return false;
        }

        // 函数的功能
        // node这个节点的次数+1了，这个节点原来在oldNodeList里。
        // 把node从oldNodeList删掉，然后放到次数+1的桶中
        // 整个过程既要保证桶之间仍然是双向链表，也要保证节点之间仍然是双向链表
        private void move(Node node, NodeList oldNodeList) {
            oldNodeList.deleteNode(node);
            // preList表示次数+1的桶的前一个桶是谁
            // 如果oldNodeList删掉node之后还有节点，oldNodeList就是次数+1的桶的前一个桶
            // 如果oldNodeList删掉node之后空了，oldNodeList是需要删除的，所以次数+1的桶的前一个桶，是oldNodeList的前一个
            NodeList preList = modifyHeadList(oldNodeList) ? oldNodeList.last : oldNodeList;
            // nextList表示次数+1的桶的后一个桶是谁
            NodeList nextList = oldNodeList.next;
            if (nextList == null) {
                NodeList newList = new NodeList(node);
                if (preList != null) {
                    preList.next = newList;
                }
                newList.last = preList;
                if (headList == null) {
                    headList = newList;
                }
                heads.put(node, newList);
            } else {
                if (nextList.head.times.equals(node.times)) {
                    nextList.addNodeFromHead(node);
                    heads.put(node, nextList);
                } else {
                    NodeList newList = new NodeList(node);
                    if (preList != null) {
                        preList.next = newList;
                    }
                    newList.last = preList;
                    newList.next = nextList;
                    nextList.last = newList;
                    if (headList == nextList) {
                        headList = newList;
                    }
                    heads.put(node, newList);
                }
            }
        }

        public void put(int key, int value) {
            if (capacity == 0) {
                return;
            }
            if (records.containsKey(key)) {
                Node node = records.get(key);
                node.value = value;
                node.times++;
                NodeList curNodeList = heads.get(node);
                move(node, curNodeList);
            } else {
                if (size == capacity) {
                    Node node = headList.tail;
                    headList.deleteNode(node);
                    modifyHeadList(headList);
                    records.remove(node.key);
                    heads.remove(node);
                    size--;
                }
                Node node = new Node(key, value, 1);
                if (headList == null) {
                    headList = new NodeList(node);
                } else {
                    if (headList.head.times.equals(node.times)) {
                        headList.addNodeFromHead(node);
                    } else {
                        NodeList newList = new NodeList(node);
                        newList.next = headList;
                        headList.last = newList;
                        headList = newList;
                    }
                }
                records.put(key, node);
                heads.put(node, headList);
                size++;
            }
        }

        public int get(int key) {
            if (!records.containsKey(key)) {
                return -1;
            }
            Node node = records.get(key);
            node.times++;
            NodeList curNodeList = heads.get(node);
            move(node, curNodeList);
            return node.value;
        }
    }

    // N个加油站组成一个环形，给定两个长度都是N的非负数组oil和dis(N>1)，oil[i]代表第i个加油站存的油可以跑多少千米，
    // dis[i]代表第i个加油站到环中下一个加油站相隔多少千米。
    // 假设你有一辆油箱足够大的车，初始时车里没有油。如果车从第i个加油站出发，最终可以回到这个加油站，那么第i个加油站就算良好出发点，否则就不算。
    // 请返回长度为N的boolean型数组res，res[i]代表第i个加油站是不是良好出发点
    // 规定只能按照顺时针走，也就是i只能走到i+1，N只能走到1
    public static void canCompleteCirculate(int[] oil, int[] dis) {
        boolean[] res = getRes(oil, dis);
        StringBuilder sb = new StringBuilder();
        for (boolean re : res) {
            int tmp = re ? 1 : 0;
            sb.append(tmp).append(" ");
        }
        System.out.println(sb.toString().trim());
    }

    public static boolean[] getRes(int[] oil, int[] dis) {
        if (dis == null || oil == null || dis.length < 2 || dis.length != oil.length) return null;
        int init = init(oil, dis);
        return init == -1 ? new boolean[dis.length] : enlargeArea(dis, init);
    }

    public static boolean[] enlargeArea(int[] dis, int init) {
        boolean[] res = new boolean[dis.length];
        int start = init;
        int end = nextIndex2(init, dis.length);
        long need = 0;
        long rest = 0;
        do {
            //往后扩充已经没法扩充了
            if (start != init && start == lastIndex(end, dis.length)) {
                break;
            }
            if (dis[start] < need) {
                need -= dis[start];
            } else {
                //试着往前进
                rest += dis[start] - need;
                need = 0;
                while (rest >= 0 && end != start) {
                    rest += dis[end];
                    end = nextIndex2(end, dis.length);
                }
                if (rest >= 0) {
                    res[start] = true;
                    connectGood(dis, lastIndex(start, dis.length), init, res);
                    break;
                }
            }
            start = lastIndex(start, dis.length);
        } while (start != init);
        return res;
    }

    public static void connectGood(int[] dis, int start, int init, boolean[] res) {
        long need = 0;
        while (start != init) {
            if (dis[start] < need) {
                need -= dis[start];
            } else {
                res[start] = true;
                need = 0;
            }
            start = lastIndex(start, dis.length);
        }
    }

    public static int lastIndex(int init, int size) {
        return init == 0 ? size - 1 : (init - 1);
    }

    public static int nextIndex2(int init, int size) {
        return init == size - 1 ? 0 : (init + 1);
    }

    public static int init(int[] oil, int[] dis) {
        int init = -1;
        for (int i = 0; i < oil.length; i++) {
            dis[i] = oil[i] - dis[i];
            if (dis[i] >= 0) {
                init = i;
            }
        }
        return init;
    }

    // 一棵二叉树原本是搜索二叉树，但是其中有两个节点调换了位置，使得这棵二叉树不再是搜索二叉树，请找到这两个错误节点并返回。
    // 已知二叉树中所有节点的值都不一样，给定二叉树的头节点 head，返回一个长度为 2 的二叉树节点类型数组 errs，
    // errs[0] 表示一个错误节点，errs[1] 表示另一个错误节点。
    // 本题测试链接 : https://leetcode.com/problems/recover-binary-search-tree/
    // 不要提交这个类
    public static class TreeNode {
        public int val;
        public TreeNode left;
        public TreeNode right;

        public TreeNode(int v) {
            val = v;
        }
    }

    // 如果能过leetcode，只需要提交这个方法即可
    // 但其实recoverTree2才是正路，只不过leetcode没有那么考
    public static void recoverTree(TreeNode root) {
        TreeNode[] errors = twoErrors(root);
        if (errors[0] != null && errors[1] != null) {
            int tmp = errors[0].val;
            errors[0].val = errors[1].val;
            errors[1].val = tmp;
        }
    }

    public static TreeNode[] twoErrors(TreeNode head) {
        TreeNode[] ans = new TreeNode[2];
        if (head == null) {
            return ans;
        }
        TreeNode cur = head;
        TreeNode mostRight = null;
        TreeNode pre = null;
        TreeNode e1 = null;
        TreeNode e2 = null;
        while (cur != null) {
            mostRight = cur.left;
            if (mostRight != null) {
                while (mostRight.right != null && mostRight.right != cur) {
                    mostRight = mostRight.right;
                }
                if (mostRight.right == null) {
                    mostRight.right = cur;
                    cur = cur.left;
                    continue;
                } else {
                    mostRight.right = null;
                }
            }
            if (pre != null && pre.val >= cur.val) {
                e1 = e1 == null ? pre : e1;
                e2 = cur;
            }
            pre = cur;
            cur = cur.right;
        }
        ans[0] = e1;
        ans[1] = e2;
        return ans;
    }

    // 以下的方法，提交leetcode是通过不了的，但那是因为leetcode的验证方式有问题
    // 但其实！以下的方法，才是正路！在结构上彻底交换两个节点，而不是值交换
    public static TreeNode recoverTree2(TreeNode head) {
        TreeNode[] errs = getTwoErrNodes(head);
        TreeNode[] parents = getTwoErrParents(head, errs[0], errs[1]);
        TreeNode e1 = errs[0];
        TreeNode e1P = parents[0];
        TreeNode e1L = e1.left;
        TreeNode e1R = e1.right;
        TreeNode e2 = errs[1];
        TreeNode e2P = parents[1];
        TreeNode e2L = e2.left;
        TreeNode e2R = e2.right;
        if (e1 == head) {
            if (e1 == e2P) {
                e1.left = e2L;
                e1.right = e2R;
                e2.right = e1;
                e2.left = e1L;
            } else if (e2P.left == e2) {
                e2P.left = e1;
                e2.left = e1L;
                e2.right = e1R;
                e1.left = e2L;
                e1.right = e2R;
            } else {
                e2P.right = e1;
                e2.left = e1L;
                e2.right = e1R;
                e1.left = e2L;
                e1.right = e2R;
            }
            head = e2;
        } else if (e2 == head) {
            if (e2 == e1P) {
                e2.left = e1L;
                e2.right = e1R;
                e1.left = e2;
                e1.right = e2R;
            } else if (e1P.left == e1) {
                e1P.left = e2;
                e1.left = e2L;
                e1.right = e2R;
                e2.left = e1L;
                e2.right = e1R;
            } else {
                e1P.right = e2;
                e1.left = e2L;
                e1.right = e2R;
                e2.left = e1L;
                e2.right = e1R;
            }
            head = e1;
        } else {
            if (e1 == e2P) {
                if (e1P.left == e1) {
                    e1P.left = e2;
                    e1.left = e2L;
                    e1.right = e2R;
                    e2.left = e1L;
                    e2.right = e1;
                } else {
                    e1P.right = e2;
                    e1.left = e2L;
                    e1.right = e2R;
                    e2.left = e1L;
                    e2.right = e1;
                }
            } else if (e2 == e1P) {
                if (e2P.left == e2) {
                    e2P.left = e1;
                    e2.left = e1L;
                    e2.right = e1R;
                    e1.left = e2;
                    e1.right = e2R;
                } else {
                    e2P.right = e1;
                    e2.left = e1L;
                    e2.right = e1R;
                    e1.left = e2;
                    e1.right = e2R;
                }
            } else {
                if (e1P.left == e1) {
                    if (e2P.left == e2) {
                        e1.left = e2L;
                        e1.right = e2R;
                        e2.left = e1L;
                        e2.right = e1R;
                        e1P.left = e2;
                        e2P.left = e1;
                    } else {
                        e1.left = e2L;
                        e1.right = e2R;
                        e2.left = e1L;
                        e2.right = e1R;
                        e1P.left = e2;
                        e2P.right = e1;
                    }
                } else {
                    if (e2P.left == e2) {
                        e1.left = e2L;
                        e1.right = e2R;
                        e2.left = e1L;
                        e2.right = e1R;
                        e1P.right = e2;
                        e2P.left = e1;
                    } else {
                        e1.left = e2L;
                        e1.right = e2R;
                        e2.left = e1L;
                        e2.right = e1R;
                        e1P.right = e2;
                        e2P.right = e1;
                    }
                }
            }
        }
        return head;
    }

    public static TreeNode[] getTwoErrNodes(TreeNode head) {
        TreeNode[] errs = new TreeNode[2];
        if (head == null) {
            return errs;
        }
        Stack<TreeNode> stack = new Stack<TreeNode>();
        TreeNode pre = null;
        while (!stack.isEmpty() || head != null) {
            if (head != null) {
                stack.push(head);
                head = head.left;
            } else {
                head = stack.pop();
                if (pre != null && pre.val > head.val) {
                    errs[0] = errs[0] == null ? pre : errs[0];
                    errs[1] = head;
                }
                pre = head;
                head = head.right;
            }
        }
        return errs;
    }

    public static TreeNode[] getTwoErrParents(TreeNode head, TreeNode e1, TreeNode e2) {
        TreeNode[] parents = new TreeNode[2];
        if (head == null) {
            return parents;
        }
        Stack<TreeNode> stack = new Stack<TreeNode>();
        while (!stack.isEmpty() || head != null) {
            if (head != null) {
                stack.push(head);
                head = head.left;
            } else {
                head = stack.pop();
                if (head.left == e1 || head.right == e1) {
                    parents[0] = head;
                }
                if (head.left == e2 || head.right == e2) {
                    parents[1] = head;
                }
                head = head.right;
            }
        }
        return parents;
    }

    // for test -- print tree
    public static void printTree(TreeNode head) {
        System.out.println("Binary Tree:");
        printInOrder(head, 0, "H", 17);
        System.out.println();
    }

    public static void printInOrder(TreeNode head, int height, String to, int len) {
        if (head == null) {
            return;
        }
        printInOrder(head.right, height + 1, "v", len);
        String val = to + head.val + to;
        int lenM = val.length();
        int lenL = (len - lenM) / 2;
        int lenR = len - lenM - lenL;
        val = getSpace(lenL) + val + getSpace(lenR);
        System.out.println(getSpace(height * len) + val);
        printInOrder(head.left, height + 1, "^", len);
    }

    public static String getSpace(int num) {
        String space = " ";
        StringBuffer buf = new StringBuffer("");
        for (int i = 0; i < num; i++) {
            buf.append(space);
        }
        return buf.toString();
    }

    // 为了测试
    public static boolean isBST(TreeNode head) {
        if (head == null) {
            return false;
        }
        Stack<TreeNode> stack = new Stack<TreeNode>();
        TreeNode pre = null;
        while (!stack.isEmpty() || head != null) {
            if (head != null) {
                stack.push(head);
                head = head.left;
            } else {
                head = stack.pop();
                if (pre != null && pre.val > head.val) {
                    return false;
                }
                pre = head;
                head = head.right;
            }
        }
        return true;
    }

    // 给定很多线段，每条线段都有两个数组 [start, end]，表示线段的开始位置和结束位置，左右都是闭区间。规定：
    // 线段开始和结束位置一定都是整数值；
    // 线段重合区域的长度必须 >=1 （比如(1,3) 和 (3,5) 不算重合）
    // 返回线段最多重合区域中，包含了几条线段。
    public static int maxCover1(int[][] lines) {
        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < lines.length; i++) {
            min = Math.min(min, lines[i][0]);
            max = Math.max(max, lines[i][1]);
        }
        int cover = 0;
        for (double p = min + 0.5; p < max; p += 1) {
            int cur = 0;
            for (int i = 0; i < lines.length; i++) {
                if (lines[i][0] < p && lines[i][1] > p) {
                    cur++;
                }
            }
            cover = Math.max(cover, cur);
        }
        return cover;
    }

    public static class StartComparator implements Comparator<Line> {

        @Override
        public int compare(Line o1, Line o2) {
            return o1.start - o2.start;
        }

    }

    public static int maxCover2(int[][] m) {
        Line[] lines = new Line[m.length];
        for (int i = 0; i < m.length; i++) {
            lines[i] = new Line(m[i][0], m[i][1]);
        }
        Arrays.sort(lines, new StartComparator());
        // 小根堆，每一条线段的结尾数值，使用默认的
        PriorityQueue<Integer> heap = new PriorityQueue<>();
        int max = 0;
        for (int i = 0; i < lines.length; i++) {
            // lines[i] -> cur 在黑盒中，把<=cur.start 东西都弹出
            while (!heap.isEmpty() && heap.peek() <= lines[i].start) {
                heap.poll();
            }
            heap.add(lines[i].end);
            max = Math.max(max, heap.size());
        }
        return max;
    }

    public static class Line {
        public int start;
        public int end;

        public Line(int s, int e) {
            start = s;
            end = e;
        }
    }

    public static class EndComparator implements Comparator<Line> {

        @Override
        public int compare(Line o1, Line o2) {
            return o1.end - o2.end;
        }

    }

    // 和maxCover2过程是一样的
    // 只是代码更短
    // 不使用类定义的写法
    public static int maxCover3(int[][] m) {
        // m是二维数组，可以认为m内部是一个一个的一维数组
        // 每一个一维数组就是一个对象，也就是线段
        // 如下的code，就是根据每一个线段的开始位置排序
        // 比如, m = { {5,7}, {1,4}, {2,6} } 跑完如下的code之后变成：{ {1,4}, {2,6}, {5,7} }
        Arrays.sort(m, (a, b) -> (a[0] - b[0]));
        // 准备好小根堆，和课堂的说法一样
        PriorityQueue<Integer> heap = new PriorityQueue<>();
        int max = 0;
        for (int[] line : m) {
            while (!heap.isEmpty() && heap.peek() <= line[0]) {
                heap.poll();
            }
            heap.add(line[1]);
            max = Math.max(max, heap.size());
        }
        return max;
    }

    // 平面内有 n 个矩形，第 i 个矩形的左下角坐标为 (x1[i], y1[i])，右上角坐标为 (x2[i], y2[i])
    // 如果两个或者多个矩形有公共区域则认为它们是相互重叠的（不考虑边界和角落）。
    // 请你计算出平面内重叠矩形数量最多的地方，有多少个矩形相互重叠。

}