package algorithmProblems;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

public class interviewQuestions {
    // 面试题
    // 来自神策
    //给定一个数组arr，表示连续n天的股价，数组下标表示第几天
    //指标X：任意两天的股价之和 - 此两天间隔的天数
    //比如
    //第3天，价格是10
    //第9天，价格是30
    //那么第3天和第9天的指标X = 10 + 30 - (9 - 3) = 34。
    //返回arr中最大的指标X。
    //时间复杂度O(N)。
    public static int maxXFromStock(int[] nums) {
        if (nums == null || nums.length < 2) {
            return -1;
        }
        int preBest = nums[0];
        int ans = 0;
        for (int i = 1; i < nums.length; i++) {
            ans = Math.max(ans, nums[i] - i + preBest);
            preBest = Math.max(preBest, nums[i] + i);
        }
        return ans;
    }

    // 来自美团
    // 小团生日收到妈妈送的两个一模一样的数列作为礼物！
    // 他很开心的把玩，不过不小心没拿稳将数列摔坏了！
    // 现在他手上的两个数列分别为A和B，长度分别为n和m。
    // 小团很想再次让这两个数列变得一样。他现在能做两种操作：
    // 操作一是将一个选定数列中的某一个数a改成数b，这会花费|b-a|的时间，
    // 操作二是选择一个数列中某个数a，将它从数列中丢掉，花费|a|的时间。
    // 小团想知道，他最少能以多少时间将这两个数列变得再次相同！
    // 输入描述：
    // 第一行两个空格隔开的正整数n和m，分别表示数列A和B的长度。
    // 接下来一行n个整数，分别为A1 A2…An
    // 接下来一行m个整数，分别为B1 B2…Bm
    // 对于所有数据，1 ≤ n,m ≤ 2000， |Ai|,|Bi| ≤ 10000
    // 暴力递归
    public static int changeToSame(int[] A, int[] B, int ai, int bi) {
        // 两个数组都走完了，不需要操作
        if (ai == A.length && bi == B.length) {
            return 0;
        }
        // 数组A走完了，数字B没有走完
        if (ai == A.length) {
            // 只能删除B后续的数组
            return B[bi] + changeToSame(A, B, ai, bi + 1);
        }
        // 数组B走完了，数组A没有走完
        if (bi == B.length) {
            return A[ai] + changeToSame(A, B, ai + 1, bi);
        }

        // 数组A和数组B都有剩余
        // 列举所有可能出现的情况
        // 删除数组A中的元素
        int p1 = A[ai] + changeToSame(A, B, ai + 1, bi);
        // 删除数组B中的元素
        int p2 = B[ai] + changeToSame(A, B, ai, bi + 1);
        // 同时删除元素
        //int p3 = A[ai] + B[bi] + changeToSame(A,B,ai + 1,bi + 1);
        // 把数组A的元素变成数组B的元素，或者相反，再或者两个元素相等不用改变
        int p4 = Math.abs(A[ai] - B[bi]) + changeToSame(A, B, ai + 1, bi + 1);
        // 因为同时删除两个元素付出的代价肯定比把其中一个变成另外一个，因此p4完全覆盖了p3，因此p3不要了

        return Math.min(p1, Math.min(p2, p4));
    }

    // 上面的暴力递归方法改动态规划
    public static int minChange(int[] A, int[] B, int indexA, int indexB, int[][] dp) {
        if (indexA == A.length && indexB == B.length) {
            return 0;
        }
        if (dp[indexA][indexB] != -1) {
            return dp[indexA][indexB];
        }
        int ans = 0;
        if (indexA == A.length && indexB != B.length) {
            ans = B[indexB] + minChange(A, B, indexA, indexB + 1, dp);
        } else if (indexA != A.length && indexB == B.length) {
            ans = A[indexA] + minChange(A, B, indexA + 1, indexB, dp);
        } else {
            int p1 = A[indexA] + minChange(A, B, indexA + 1, indexB, dp);
            int p2 = B[indexB] + minChange(A, B, indexA, indexB + 1, dp);
            int p45 = Math.abs(A[indexA] - B[indexB]) + minChange(A, B, indexA + 1, indexB + 1, dp);
            ans = Math.min(Math.min(p1, p2), p45);
        }
        dp[indexA][indexB] = ans;
        return ans;
    }

    // 来自美团
    // 小美将要期中考试，有n道题，对于第i道题，
    // 小美有pi的几率做对，获得ai的分值，还有(1-pi)的概率做错，得0分。
    // 小美总分是每道题获得的分数。
    // 小美不甘于此，决定突击复习，因为时间有限，她最多复习m道题，复习后的试题正确率为100%。
    // 如果以最佳方式复习，能获得期望最大总分是多少？
    // 输入n、m
    // 接下来输入n个整数，代表pi%，为了简单期间，将概率扩大了100倍。
    // 接下来输入n个整数，代表ai，某道题的分值
    // 输出最大期望分值，精确到小数点后2位
    // 数据 1m<=n<=50000
    public static double review(int[] possibilities, int[] scores, int n, int m) {
        double[] missScores = new double[n];
        for (int i = 0; i < n; i++) {
            missScores[i] = (scores[i] - scores[i] * (possibilities[i] * 1.0 / 100));
        }
        double noReviewScores = 0.0;
        for (int i = 0; i < n; i++) {
            noReviewScores += scores[i] * (possibilities[i] * 1.0 / 100);
        }
        double reviewMScore = 0.0;
        Arrays.sort(missScores);
        for (int i = n - 1; i >= n - m; i--) {
            reviewMScore += missScores[i];
        }
        return reviewMScore + noReviewScores;
    }

    // 小团在地图上放了3个定位装置，想依赖他们进行定位！
    // 地图是一个n*n的棋盘，
    // 有3个定位装置(x1,y1),(x2,y2),(x3,y3)，每个值均在[1,n]内。
    // 小团在(a,b)位置放了一个信标，
    // 每个定位装置会告诉小团它到信标的曼哈顿距离，也就是对于每个点，小团知道|xi-a|+|yi-b|
    // 求信标位置，信标不唯一，输出字典序最小的。
    // 输入n，然后是3个定位装置坐标，
    // 最后是3个定位装置到信标的曼哈顿记录。
    // 输出最小字典序的信标位置。
    // 1 <= 所有数据值 <= 50000。
    public static int[] find(int n, int[] a, int[] b, int[] c, int ad, int bd, int cd) {
        int[] x1 = null;
        int r1 = Integer.MAX_VALUE;
        int[] x2 = null;
        int r2 = 0;
        int[] x3 = null;
        int r3 = 0;
        if (ad < r1) {
            x1 = a;
            r1 = ad;
            x2 = b;
            r2 = bd;
            x3 = c;
            r3 = cd;
        }
        if (bd < r1) {
            x1 = b;
            r1 = bd;
            x2 = a;
            r2 = ad;
            x3 = c;
            r3 = cd;
        }
        if (cd < r1) {
            x1 = c;
            r1 = cd;
            x2 = a;
            r2 = ad;
            x3 = b;
            r3 = bd;
        }
        // x1 r1     x2 r2 x3 r3
        int[] cur = {x1[0] - r1, x1[1]};
        Queue<int[]> queue = new LinkedList<>();
        HashSet<String> visited = new HashSet<>();
        ArrayList<int[]> ans = new ArrayList<>();
        queue.add(cur);
        visited.add(cur[0] + "_" + cur[1]);

        while (!queue.isEmpty()) {
            // cur x1为圆心，r1为半径的圆周上
            cur = queue.poll();
            if (
                    cur[0] >= 1 && cur[0] <= n
                            &&
                            cur[1] >= 1 && cur[1] <= n
                            &&
                            distance(cur[0], cur[1], x2) == r2
                            &&
                            distance(cur[0], cur[1], x3) == r3) {
                ans.add(cur);
            }
            if (ans.size() == 2) {
                break;
            }
            add(cur[0] - 1, cur[1] - 1, x1, r1, queue, visited);
            add(cur[0] - 1, cur[1], x1, r1, queue, visited);
            add(cur[0] - 1, cur[1] + 1, x1, r1, queue, visited);
            add(cur[0], cur[1] - 1, x1, r1, queue, visited);
            add(cur[0], cur[1] + 1, x1, r1, queue, visited);
            add(cur[0] + 1, cur[1] - 1, x1, r1, queue, visited);
            add(cur[0] + 1, cur[1], x1, r1, queue, visited);
            add(cur[0] + 1, cur[1] + 1, x1, r1, queue, visited);
        }
        if (ans.size() == 1 || ans.get(0)[0] < ans.get(1)[0] || (ans.get(0)[0] == ans.get(1)[0] && ans.get(0)[1] < ans.get(1)[1])) {
            return ans.get(0);
        } else {
            return ans.get(1);
        }
    }

    public static void add(int x, int y, int[] c, int r, Queue<int[]> queue, HashSet<String> visited) {
        String key = x + "_" + y;
        if (distance(x, y, c) == r && !visited.contains(key)) {
            queue.add(new int[]{x, y});
            visited.add(key);
        }
    }

    public static int distance(int x, int y, int[] c) {
        return Math.abs(x - c[0]) + Math.abs(y - c[1]);
    }

    // 来自微软
    // 给定一个字符串s，只含有0~9这些字符
    // 你可以使用来自s中的数字，目的是拼出一个最大的回文数
    // 使用数字的个数，不能超过s里含有的个数
    // 比如 :
    // 39878，能拼出的最大回文数是 : 898
    // 00900，能拼出的最大回文数是 : 9
    // 54321，能拼出的最大回文数是 : 5
    // 最终的结果以字符串形式返回
    // str的长度为N，1 <= N <= 100000
    // 测试链接 : https://leetcode.cn/problems/largest-palindromic-number/
    public static String largestPalindromic(String s) {
        if (s == null || s.equals("")) {
            return "";
        }
        HashMap<Integer, Integer> map = new HashMap<>();
        int n = s.length();
        for (int i = 0; i < n; i++) {
            int number = s.charAt(i) - '0';
            map.put(number, map.getOrDefault(number, 0) + 1);
        }
        PriorityQueue<Record> heap = new PriorityQueue<>(new RecordComparator());
        for (int key : map.keySet()) {
            heap.add(new Record(key, map.get(key)));
        }
        Record top = heap.poll();
        if (top.times == 1) {
            return String.valueOf(top.number);
        } else if (top.number == 0) {
            return String.valueOf(heap.isEmpty() ? 0 : heap.peek().number);
        } else {
            StringBuilder left = new StringBuilder();
            left.append(top.number);
            top.times -= 2;
            if (top.times > 0) {
                heap.add(top);
            }
            while (!heap.isEmpty() && heap.peek().times > 1) {
                top = heap.poll();
                left.append(top.number);
                top.times -= 2;
                if (top.times > 0) {
                    heap.add(top);
                }
            }
            StringBuilder ans = new StringBuilder();
            for (int i = 0; i < left.length(); i++) {
                ans.append(left.charAt(i));
            }
            if (!heap.isEmpty()) {
                ans.append(heap.peek().number);
            }
            for (int i = left.length() - 1; i >= 0; i--) {
                ans.append(left.charAt(i));
            }
            return ans.toString();
        }
    }

    public static class Record {
        public int number;
        public int times;

        public Record(int n, int t) {
            number = n;
            times = t;
        }
    }

    public static class RecordComparator implements Comparator<Record> {
        @Override
        public int compare(Record o1, Record o2) {
            if (o1.times == 1 && o2.times > 1) {
                return 1;
            }
            if (o1.times > 1 && o2.times == 1) {
                return -1;
            }
            return o2.number - o1.number;
        }
    }

    // 来自微软
    // 给定两个数组A和B，比如
    // A = { 0, 1, 1 }
    // B = { 1, 2, 3 }
    // A[0] = 0, B[0] = 1，表示0到1有双向道路
    // A[1] = 1, B[1] = 2，表示1到2有双向道路
    // A[2] = 1, B[2] = 3，表示1到3有双向道路
    // 给定数字N，编号从0~N，所以一共N+1个节点
    // 题目输入一定保证所有节点都联通，并且一定没有环
    // 默认办公室是0节点，其他1~N节点上，每个节点上都有一个居民
    // 每天所有居民都去往0节点上班
    // 所有的居民都有一辆5座的车，也都乐意和别人一起坐车
    // 车不管负重是多少，只要走过一条路，就耗费1的汽油
    // 比如A、B、C的居民，开着自己的车来到D居民的位置，一共耗费3的汽油
    // D居民和E居民之间，假设有一条路
    // 那么D居民可以接上A、B、C，4个人可以用一辆车，去往E的话，就再耗费1的汽油
    // 求所有居民去办公室的路上，最少耗费多少汽油
    // 测试链接 : https://leetcode.cn/problems/minimum-fuel-cost-to-report-to-the-capital/
    public static int cnt = 0;

    public static int minFuel(int[] a, int[] b, int n) {
        // 先建图
        ArrayList<ArrayList<Integer>> graph = new ArrayList<>();
        for (int i = 0; i <= n; i++) {
            graph.add(new ArrayList<>());
        }
        for (int i = 0; i < a.length; i++) {
            graph.get(a[i]).add(b[i]);
            graph.get(b[i]).add(a[i]);
        }
        // 建图完毕
        // 根据题目描述，办公室一定是0号点
        // 所有员工一定是往0号点汇聚

        // a 号，dfn[a] == 0 没遍历过！
        // dfn[a] != 0 遍历过！
        int[] dfn = new int[n + 1];
        // a为头的树，一共有10个节点
        // size[a] = 0
        // size[a] = 10
        int[] size = new int[n + 1];
        // 所有居民要汇总吗？
        // a为头的树，所有的居民是要向a来汇聚
        // cost[a] : 所有的居民要向a来汇聚，总油量的耗费
        int[] cost = new int[n + 1];
        cnt = 0;
        dfs(graph, 0, dfn, size, cost);
        return cost[0];
    }

    // 图 ： graph
    // 当前的头，原来的编号，不是dfn序号！ : cur
    // 从cur开始，请遍历
    // 遍历完成后，请把dfn[cur]填好！size[cur]填好！cost[cur]填好
    public static void dfs(ArrayList<ArrayList<Integer>> graph, int cur, int[] dfn, int[] size, int[] cost) {
        dfn[cur] = ++cnt;
        size[cur] = 1;
        for (int next : graph.get(cur)) {
            if (dfn[next] == 0) {
                dfs(graph, next, dfn, size, cost);
                size[cur] += size[next];
                cost[cur] += cost[next];
                cost[cur] += (size[next] + 4) / 5;
            }
        }
    }

    // 找到了这个题的在线测试链接 :
    // https://leetcode.cn/problems/minimum-fuel-cost-to-report-to-the-capital/
    // 如下方法提交了可以直接通过
    public static long minimumFuelCost(int[][] roads, int seats) {
        int n = roads.length;
        ArrayList<ArrayList<Integer>> graph = new ArrayList<>();
        for (int i = 0; i <= n; i++) {
            graph.add(new ArrayList<>());
        }
        for (int[] r : roads) {
            graph.get(r[0]).add(r[1]);
            graph.get(r[1]).add(r[0]);
        }
        int[] size = new int[n + 1];
        long[] cost = new long[n + 1];
        dfs(0, -1, seats, graph, size, cost);
        return cost[0];
    }

    public static void dfs(int cur, int father, int seats, ArrayList<ArrayList<Integer>> graph, int[] size,
                           long[] cost) {
        size[cur] = 1;
        for (int next : graph.get(cur)) {
            if (next != father) {
                dfs(next, cur, seats, graph, size, cost);
                size[cur] += size[next];
                cost[cur] += cost[next];
                cost[cur] += (size[next] + seats - 1) / seats;
            }
        }
    }

    // 来自网易
    // 小红拿到了一个仅由r、e、d组成的字符串
    // 她定义一个字符e为"好e" : 当且仅当这个e字符和r、d相邻
    // 例如"reeder"只有一个"好e"，前两个e都不是"好e"，只有第三个e是"好e"
    // 小红每次可以将任意字符修改为任意字符，即三种字符可以相互修改
    // 她希望"好e"的数量尽可能多
    // 小红想知道，自己最少要修改多少次
    // 输入一个只有r、e、d三种字符的字符串
    // 长度 <= 2 * 10^5
    // 输出最小修改次数
    public static int minCost(String str) {
        int n = str.length();
        if (n < 3) {
            return -1;
        }
        int[] arr = new int[n];
        // d认为是0，e认为是1，r认为是2
        for (int i = 0; i < n; i++) {
            char cur = str.charAt(i);
            if (cur == 'd') {
                arr[i] = 0;
            } else if (cur == 'e') {
                arr[i] = 1;
            } else {
                arr[i] = 2;
            }
        }
        // 通过上面的转化，问题变成了：
        // 1的左右，一定要被0和2包围，这个1才是"好1"
        // 请让"好1"的尽量多，返回最少的修改代价
        int maxGood = 0;
        int minCost = Integer.MAX_VALUE;
        for (int prepre = 0; prepre < 3; prepre++) {
            for (int pre = 0; pre < 3; pre++) {
                int cost = arr[0] == prepre ? 0 : 1;
                cost += arr[1] == pre ? 0 : 1;
                Info cur = process(arr, 2, prepre, pre);
                if (cur.maxGood > maxGood) {
                    maxGood = cur.maxGood;
                    minCost = cur.minCost + cost;
                } else if (cur.maxGood == maxGood) {
                    minCost = Math.min(minCost, cur.minCost + cost);
                }
            }
        }
        return minCost;
    }

    public static class Info {
        public int maxGood;
        public int minCost;

        public Info(int a, int b) {
            maxGood = a;
            minCost = b;
        }
    }

    // 暴力递归
    // 可以自己改成动态规划
    // arr[index-2]位置的数值是prePre
    // arr[index-1]位置的数值是pre
    // 在这种情况下，请让arr[index...]上的好1尽量多
    // 返回:
    // 尽量多的"好1"，是多少？
    // 得到尽量多的"好1"，最小代价是多少?
    public static Info process(int[] arr, int index, int prePre, int pre) {
        if (index == arr.length) {
            return new Info(0, 0);
        }
        // 可能性1，arr[index]，变成0
        int p1Value = prePre == 2 && pre == 1 ? 1 : 0;
        int p1Cost = arr[index] == 0 ? 0 : 1;
        Info info = process(arr, index + 1, pre, 0);
        p1Value += info.maxGood;
        p1Cost += info.minCost;
        // 可能性2，arr[index]，变成1
        int p2Value = 0;
        int p2Cost = arr[index] == 1 ? 0 : 1;
        info = process(arr, index + 1, pre, 1);
        p2Value += info.maxGood;
        p2Cost += info.minCost;
        // 可能性3，arr[index]，变成2
        int p3Value = prePre == 0 && pre == 1 ? 1 : 0;
        int p3Cost = arr[index] == 2 ? 0 : 1;
        info = process(arr, index + 1, pre, 2);
        p3Value += info.maxGood;
        p3Cost += info.minCost;
        // 开始决策，选出三种可能性中的最优解
        int maxGood = 0;
        int minCost = Integer.MAX_VALUE;
        if (p1Value > maxGood) {
            maxGood = p1Value;
            minCost = p1Cost;
        } else if (p1Value == maxGood) {
            minCost = Math.min(minCost, p1Cost);
        }
        if (p2Value > maxGood) {
            maxGood = p2Value;
            minCost = p2Cost;
        } else if (p2Value == maxGood) {
            minCost = Math.min(minCost, p2Cost);
        }
        if (p3Value > maxGood) {
            maxGood = p3Value;
            minCost = p3Cost;
        } else if (p3Value == maxGood) {
            minCost = Math.min(minCost, p3Cost);
        }
        return new Info(maxGood, minCost);
    }

    // 来自弗吉尼亚理工大学(VT)，算法考试卷
    // 精选了还可以的几道题
    // 这些都是简单难度的动态规划，是面试中最常见的难度
    // 这几个题都有一些非常小的常见技巧可说
    // 题目1
    // arr[i][0] : 有趣值
    // arr[i][1] : 进攻值
    // arr[index...]所有的方案自由选择
    // 必须让restFunny、restOffense值 <= 0
    // index之前的值不能选择
    // 返回最小的方案数量(index...)
    public static int minStickers1(int[][] stickers, int funnyGoal, int offenseGoal) {
        return process1(stickers, 0, funnyGoal, offenseGoal);
    }

    public static int process1(int[][] stickers, int index, int restFunny, int restOffense) {
        if (restFunny <= 0 && restOffense <= 0) {
            return 0;
        }
        if (index == stickers.length) {
            return Integer.MAX_VALUE;
        }
        // 不选当前的贴纸
        int p1 = process1(stickers, index + 1, restFunny, restOffense);
        // 选当前贴纸
        int p2 = Integer.MAX_VALUE;
        int next2 = process1(stickers, index + 1, Math.max(0, restFunny - stickers[index][0]),
                Math.max(0, restOffense - stickers[index][1]));
        if (next2 != Integer.MAX_VALUE) {
            p2 = next2 + 1;
        }
        return Math.min(p1, p2);
    }

    // 改动态规划
    public static int minStickers2(int[][] stickers, int funnyGoal, int offenseGoal) {
        int[][][] dp = new int[stickers.length][funnyGoal + 1][offenseGoal + 1];
        for (int i = 0; i < stickers.length; i++) {
            for (int j = 0; j <= funnyGoal; j++) {
                for (int k = 0; k <= offenseGoal; k++) {
                    dp[i][j][k] = -1;
                }
            }
        }
        return process2(stickers, 0, funnyGoal, offenseGoal, dp);
    }

    public static int process2(int[][] stickers, int index, int restFunny, int restOffense, int[][][] dp) {
        if (restFunny <= 0 && restOffense <= 0) {
            return 0;
        }
        if (index == stickers.length) {
            return Integer.MAX_VALUE;
        }
        if (dp[index][restFunny][restOffense] != -1) {
            return dp[index][restFunny][restOffense];
        }
        // 不选当前的贴纸
        int p1 = process2(stickers, index + 1, restFunny, restOffense, dp);
        // 选当前贴纸
        int p2 = Integer.MAX_VALUE;
        int next2 = process2(stickers, index + 1, Math.max(0, restFunny - stickers[index][0]),
                Math.max(0, restOffense - stickers[index][1]), dp);
        if (next2 != Integer.MAX_VALUE) {
            p2 = next2 + 1;
        }
        int ans = Math.min(p1, p2);
        dp[index][restFunny][restOffense] = ans;
        return ans;
    }

    // 严格位置依赖的动态规划
    public static int minStickers3(int[][] stickers, int funnyGoal, int offenseGoal) {
        int n = stickers.length;
        int[][][] dp = new int[n + 1][funnyGoal + 1][offenseGoal + 1];
        for (int f = 0; f <= funnyGoal; f++) {
            for (int o = 0; o <= offenseGoal; o++) {
                if (f != 0 || o != 0) {
                    dp[n][f][o] = Integer.MAX_VALUE;
                }
            }
        }
        for (int i = n - 1; i >= 0; i--) {
            for (int f = 0; f <= funnyGoal; f++) {
                for (int o = 0; o <= offenseGoal; o++) {
                    if (f != 0 || o != 0) {
                        int p1 = dp[i + 1][f][o];
                        int p2 = Integer.MAX_VALUE;
                        int next2 = dp[i + 1][Math.max(0, f - stickers[i][0])][Math.max(0, o - stickers[i][1])];
                        if (next2 != Integer.MAX_VALUE) {
                            p2 = next2 + 1;
                        }
                        dp[i][f][o] = Math.min(p1, p2);
                    }
                }
            }
        }
        return dp[0][funnyGoal][offenseGoal];
    }

    // 题目2
    // 绳子总长度为M
    // 100 -> M
    // (6, 100) (7,23) (10,34) -> arr
    // 每一个长度的绳子对应一个价格，比如(6, 10)表示剪成长度为6的绳子，对应价格10
    // 可以重复切出某个长度的绳子，求切割绳子后的价值最大
    // 定义递归如下：
    // 所有可以切出来的长度 对应 价值都在数组ropes里
    // ropes[i] = {6, 10} 代表i方案为：切出长度为6的绳子，可以卖10元
    // index....所有的方案，随便选择。index之前的方案，不能选择
    // 返回最大的价值
    // 自己去改动态规划
    // arr[i][0] -> i号方案能切多少长度
    // arr[i][1] -> 切出来这个长度，就能获得的价值
    // arr[index....]自由选择，绳子还剩restLen长度
    // 返回，最大价值
    public static int maxValue(int[][] arr, int index, int restLen) {
        if (restLen <= 0 || index == arr.length) {
            return 0;
        }
        // 绳子还有剩余、且还有方案
        // index号方案
        // 不选
        int p1 = maxValue(arr, index + 1, restLen);
        // 选
        int p2 = 0;
        if (arr[index][0] <= restLen) { // 剩余绳子够长，才能选当前方案
            p2 = arr[index][1] + maxValue(arr, index, restLen - arr[index][0]);
        }
        return Math.max(p1, p2);
    }

    // 题目3
    // 每一个序列都是[a,b]的形式，a < b
    // 序列连接的方式为，前一个序列的b，要等于后一个序列的a
    // 比如 : [3, 7]、[7, 13]、[13, 26]这三个序列就可以依次连接
    // 给定若干个序列，求最大连接的数量
    // 定义尝试过程如下
    // arr[i] = {4, 9}表示，第i个序列4开始，9结束
    // pre : 代表选择的上一个序列的index是多少
    // 比如选择的上一个序列如果是(4,9)，是第5个序列，那么pre=5
    // 特别注意：如果从来没有选过序列，那么pre = -1
    // 这个函数含义 :
    // index....所有的序列，随便选择。index之前的序列，不能选择
    // 上一个选择的序列，是pre号，如果pre=-1,说明之前没有选择过序列
    // 返回题目要求的那种连接方式下，最大的序列数量
    // 请保证数组是以开始位置有序的
    public static int maxLen(int[][] arr, int index, int preIndex) {
        // 已经没有序列可选
        if (index == arr.length) {
            return 0;
        }
        // 有序列可以选
        // 不选
        int p1 = maxLen(arr, index + 1, preIndex);
        // 选
        int p2 = 0;
        // [3,17] index(9,24)
        if (arr[preIndex][1] == arr[index][0]) { // 才能选
            p2 = 1 + maxLen(arr, index + 1, index);
        }
        return Math.max(p1, p2);
    }

    // 给定区间的范围[xi,yi]，xi<=yi，且都是正整数
    // 找出一个坐标集合set，set中有若干个数字
    // set要和每个给定的区间，有交集
    // 求set的最少需要几个数
    // 比如给定区间 : [5, 8] [1, 7] [2, 4] [1, 9]
    // set最小可以是: {2, 6}或者{2, 5}或者{4, 5}
    public static int minSet(int[][] ranges) {
        int n = ranges.length;
        // events[i] = {a, b, c}
        // a == 0, 表示这是一个区间的开始事件，这个区间结束位置是b
        // a == 1, 表示这是一个区间的结束事件，b的值没有意义
        // c表示这个事件的时间点，不管是开始事件还是结束事件，都会有c这个值
        int[][] events = new int[n << 1][3];
        for (int i = 0; i < n; i++) {
            // [3, 7]
            // (0,7,3)
            // (1,X,7)
            events[i][0] = 0;
            events[i][1] = ranges[i][1];
            events[i][2] = ranges[i][0];
            events[i + n][0] = 1;
            events[i + n][2] = ranges[i][1];
        }
        Arrays.sort(events, (a, b) -> a[2] - b[2]);
        // 容器
        HashSet<Integer> tmp = new HashSet<>();
        int ans = 0;
        for (int[] event : events) {
            if (event[0] == 0) {
                tmp.add(event[1]);
            } else {
                if (tmp.contains(event[2])) {
                    ans++;
                    tmp.clear();
                }
            }
        }
        return ans;
    }

    // 来自字节
    // 5.6笔试
    // 给定一个数组arr，长度为n，最多可以删除一个连续子数组，
    // 求剩下的数组，严格连续递增的子数组最大长度
    // n <= 10^6
    // 正式方法
    // 时间复杂度O(N*logN)
    public static int maxLen2(int[] arr) {
        if (arr.length == 0) {
            return 0;
        }
        int n = arr.length;
        int[] sorted = new int[n];
        System.arraycopy(arr, 0, sorted, 0, n);
        Arrays.sort(sorted);
        SegmentTree st = new SegmentTree(n);
        st.update(rank(sorted, arr[0]), 1);
        int[] dp = new int[n];
        dp[0] = 1;
        int ans = 1;
        // 一个数字也不删！长度！
        int cur = 1;
        for (int i = 1; i < n; i++) {
            int rank = rank(sorted, arr[i]);
            // (dp[i - 1] + 1)
            int p1 = arr[i - 1] < arr[i] ? (dp[i - 1] + 1) : 1;
//			// rank : 就是当前的数字
//			// 1~rank-1 : 第二个信息的max
            int p2 = rank > 1 ? (st.max(rank - 1) + 1) : 1;
            dp[i] = Math.max(p1, p2);
            ans = Math.max(ans, dp[i]);
            if (arr[i] > arr[i - 1]) {
                cur++;
            } else {
                cur = 1;
            }
            // 我的当前值是rank
            // 之前有没有还是rank的记录！
            // 有的话  我要之前和现在相比最大的那一个 没有以前的大我就不跟新，反之跟新
            if (st.get(rank) < cur) {
                st.update(rank, cur);
            }
        }
        return ans;
    }

    public static int rank(int[] sorted, int num) {
        int l = 0;
        int r = sorted.length - 1;
        int m = 0;
        int ans = -1;
        while (l <= r) {
            m = (l + r) / 2;
            if (sorted[m] >= num) {
                ans = m;
                r = m - 1;
            } else {
                l = m + 1;
            }
        }
        return ans + 1;
    }

    public static class SegmentTree {
        private int n;
        private int[] max;
        private int[] update;

        public SegmentTree(int maxSize) {
            n = maxSize + 1;
            max = new int[n << 2];
            update = new int[n << 2];
            Arrays.fill(update, -1);
        }

        public int get(int index) {
            return max(index, index, 1, n, 1);
        }

        public void update(int index, int c) {
            update(index, index, c, 1, n, 1);
        }

        public int max(int right) {
            return max(1, right, 1, n, 1);
        }

        private void pushUp(int rt) {
            max[rt] = Math.max(max[rt << 1], max[rt << 1 | 1]);
        }

        private void pushDown(int rt, int ln, int rn) {
            if (update[rt] != -1) {
                update[rt << 1] = update[rt];
                max[rt << 1] = update[rt];
                update[rt << 1 | 1] = update[rt];
                max[rt << 1 | 1] = update[rt];
                update[rt] = -1;
            }
        }

        private void update(int L, int R, int C, int l, int r, int rt) {
            if (L <= l && r <= R) {
                max[rt] = C;
                update[rt] = C;
                return;
            }
            int mid = (l + r) >> 1;
            pushDown(rt, mid - l + 1, r - mid);
            if (L <= mid) {
                update(L, R, C, l, mid, rt << 1);
            }
            if (R > mid) {
                update(L, R, C, mid + 1, r, rt << 1 | 1);
            }
            pushUp(rt);
        }

        private int max(int L, int R, int l, int r, int rt) {
            if (L <= l && r <= R) {
                return max[rt];
            }
            int mid = (l + r) >> 1;
            pushDown(rt, mid - l + 1, r - mid);
            int ans = 0;
            if (L <= mid) {
                ans = Math.max(ans, max(L, R, l, mid, rt << 1));
            }
            if (R > mid) {
                ans = Math.max(ans, max(L, R, mid + 1, r, rt << 1 | 1));
            }
            return ans;
        }

    }

    // 来自京东
    // 4.2笔试
    // 给定一个长度为3N的数组，其中最多含有0、1、2三种值
    // 你可以把任何一个连续区间上的数组，全变成0、1、2中的一种
    // 目的是让0、1、2三种数字的个数都是N
    // 返回最小的变化次数
    // 正式方法
    // 时间复杂度O(N)
    public static int minTimes(int[] arr) {
        int[] cnt = new int[3];
        for (int num : arr) {
            cnt[num]++;
        }
        if (cnt[0] == cnt[1] && cnt[0] == cnt[2]) {
            return 0;
        }
        int n = arr.length;
        int m = n / 3;
        if ((cnt[0] < m && cnt[1] < m) || (cnt[0] < m && cnt[2] < m) || (cnt[1] < m && cnt[2] < m)) {
            return 2;
        } else { // 只有一种数的个数是小于m的
            return once(arr, cnt, m) ? 1 : 2;
        }
    }

    // 只有一种数是少于N/3
    public static boolean once(int[] arr, int[] cnt, int m) {
        int lessV = cnt[0] < m ? 0 : (cnt[1] < m ? 1 : 2);
        int lessT = lessV == 0 ? cnt[0] : (lessV == 1 ? cnt[1] : cnt[2]);
        if (cnt[0] > m && modify(arr, 0, cnt[0], lessV, lessT)) {
            return true;
        }
        if (cnt[1] > m && modify(arr, 1, cnt[1], lessV, lessT)) {
            return true;
        }
        if (cnt[2] > m && modify(arr, 2, cnt[2], lessV, lessT)) {
            return true;
        }
        return false;
    }

    // 0 -> 10个
    // 1 -> 10个
    // 2 -> 10个
    // ==========
    // 0 -> 7个
    // 2 -> 12个   1 -> 11个
    // 多的数 2
    // 少的数 0
    public static boolean modify(int[] arr,
                                 int more, int moreT,
                                 int less, int lessT) {
        int[] cnt = new int[3];
        cnt[less] = lessT;
        cnt[more] = moreT;
        // 目标
        int aim = arr.length / 3;
        int L = 0;
        int R = 0;
        while (R < arr.length || cnt[more] <= aim) {
            // cnt[more] 窗口之外，多的数有几个？
            if (cnt[more] > aim) {
                // R++ 窗口右边界，右移
                cnt[arr[R++]]--;
            } else if (cnt[more] < aim) {
                cnt[arr[L++]]++;
            } else { // 在窗口之外，多的数，够了！
                // 少的数，和，另一种数other，能不能平均！都是10个！
                if (cnt[less] + R - L < aim) {
                    cnt[arr[R++]]--;
                } else if (cnt[less] + R - L > aim) {
                    cnt[arr[L++]]++;
                } else {
                    return true;
                }
            }
        }
        return false;
    }

    // 一个数组如果满足 :
    // 升降升降升降... 或者 降升降升...都是满足的
    // 给定一个数组，
    // 1，看有几种方法能够剔除一个元素，达成上述的要求
    // 2，数组天然符合要求返回0
    // 3，剔除1个元素达成不了要求，返回-1，
    // 比如：
    // 给定[3, 4, 5, 3, 7]，返回3
    // 移除0元素，4 5 3 7 符合
    // 移除1元素，3 5 3 7 符合
    // 移除2元素，3 4 3 7 符合
    // 再比如：给定[1, 2, 3, 4] 返回-1
    // 因为达成不了要求
    // 时间复杂度O(N)
    public static int ways(int[] arr) {
        if (arr == null || arr.length < 2) {
            return 0;
        }
        int n = arr.length;
        boolean[] rightUp = new boolean[n];
        boolean[] rightDown = new boolean[n];
        rightUp[n - 1] = true;
        rightDown[n - 1] = true;
        for (int i = n - 2; i >= 0; i--) {
            rightUp[i] = arr[i] < arr[i + 1] && rightDown[i + 1];
            rightDown[i] = arr[i] > arr[i + 1] && rightUp[i + 1];
        }
        // 数组是不是天然符合！
        if (rightUp[0] || rightDown[0]) {
            return 0;
        }
        // 删掉0位置的数，数组达标还是不达标！
        // 1 升
        // 1 降
        int ans = (rightUp[1] || rightDown[1]) ? 1 : 0;
        // ...[0]
        // 这是0位置的左边的信息
        boolean leftUp = true;
        boolean leftDown = true;
        boolean tmp;
        for (int i = 1, l = 0, r = 2; i < n - 1; i++, l++, r++) {
            ans += (arr[l] > arr[r] && rightUp[r] && leftDown) || (arr[l] < arr[r] && rightDown[r] && leftUp) ? 1 : 0;
            tmp = leftUp;
            // 7 4
            // 跟新每一个i位置左边的信息
            leftUp = arr[l] > arr[i] && leftDown;
            leftDown = arr[l] < arr[i] && tmp;
        }
        // 单独算一下 删掉n-1位置数的时候
        ans += leftUp || leftDown ? 1 : 0;
        return ans == 0 ? -1 : ans;
    }

    // 你将得到一个整数数组 matchsticks ，其中matchsticks[i]是第i个火柴棒的长度。
    // 你要用所有的火柴棍拼成一个正方形。
    // 你不能折断任何一根火柴棒，但你可以把它们连在一起，而且每根火柴棒必须使用一次 。
    // 如果你能拼出正方形，则返回true，否则返回false。
    // 测试链接 : https://leetcode.cn/problems/matchsticks-to-square/
    // 利用数组里的火柴，必须都使用
    // 能不能拼出正方形
    public static boolean yesOrNo(int[] arr) {
        long sum = 0;
        for (int num : arr) {
            sum += num;
        }
        if (sum % 4 != 0) {
            return false;
        }
        long len = sum / 4;
        // 最多15根
        // 00000000000.....000000000000000
        return process(arr, 0, 0, len, 4);
    }

    // 所有火柴都在arr里
    // 哪些火柴用了、哪些火柴没用，的状态都在status里
    // sum: 当前耕耘的这条边，长度已经达成了sum
    // len: 固定参数，每条边必须都达成这个长度
    // edges: 还剩几条边，没填完
    // 返回：最终能不能达成正方形的目标
    // 看似，三个可变参数status, sum, edges
    // 其实，只有一个，status
    // 15位状态，2^15 -> 32...
    public static boolean process(int[] arr, int status, long sum, long len, int edges) {
        if (edges == 0) {
            return status == (1 << arr.length) - 1;
        }
        // 剩下边 edges > 0
        boolean ans = false;
        for (int i = 0; i < arr.length; i++) { // 当前可以选择的边，全试一遍！
            // 当前的i就是火柴编号！
            if ((status & (1 << i)) == 0) {
                if (sum + arr[i] <= len) {
                    // 当前的边已经耕耘长度sum + 当前边的长度 不能超过len！
                    if (sum + arr[i] < len) {
                        ans = process(arr, status | (1 << i), sum + arr[i], len, edges);
                    } else { // sum + arr[i] == len
                        ans = process(arr, status | (1 << i), 0, len, edges - 1);
                    }
                }
            }
            if (ans) {
                break;
            }
        }
        return ans;
    }

    // 利用数组里的火柴，必须都使用
    // 能不能拼出正方形
    public static boolean yesOrNo2(int[] arr) {
        long sum = 0;
        for (int num : arr) {
            sum += num;
        }
        if (sum % 4 != 0) {
            return false;
        }
        long len = sum / 4;
        // dp[status] =
        int[] dp = new int[1 << arr.length];
        // dp[status] == 0 没算过！
        // dp[status] == 1 算过，结果是true
        // dp[status] == -1 算过，结果是false

        return process2(arr, 0, 0, len, 4, dp);
    }

    // 所有火柴都在arr里
    // 哪些火柴用了、哪些火柴没用，的状态都在status里
    // sum: 当前耕耘的这条边，长度已经达成了sum
    // len: 固定参数，每条边必须都达成这个长度
    // edges: 还剩几条边，没填完
    // 返回：最终能不能达成正方形的目标
    // 看似，三个可变参数status, sum, edges
    // 其实，只有一个，status
    // 15位状态，2^15 -> 32...
    public static boolean process2(int[] arr, int status, long sum, long len, int edges, int[] dp) {
        if (edges == 0) {
            return status == (1 << arr.length) - 1;
        }
        // 缓存命中
        if (dp[status] != 0) {
            return dp[status] == 1;
        }
        // 剩下边 edges > 0
        boolean ans = false;
        for (int i = 0; i < arr.length; i++) { // 当前可以选择的边，全试一遍！
            // 当前的i就是火柴编号！
            if ((status & (1 << i)) == 0) {
                if (sum + arr[i] <= len) {
                    // 当前的边已经耕耘长度sum + 当前边的长度 不能超过len！
                    if (sum + arr[i] < len) {
                        ans = process2(arr, status | (1 << i), sum + arr[i], len, edges, dp);
                    } else { // sum + arr[i] == len
                        ans = process2(arr, status | (1 << i), 0, len, edges - 1, dp);
                    }
                }
            }
            if (ans) {
                break;
            }
        }
        // ans == true dp[status] = 1
        // ans == false dp[status] = -1
        dp[status] = ans ? 1 : -1;
        return ans;
    }

    // 给你一个整数数组nums。如果nums的一个子集中，
    // 所有元素的乘积可以表示为一个或多个互不相同的质数的乘积，那么我们称它为好子集。
    // 比方说，如果nums = [1, 2, 3, 4]：
    // [2, 3]，[1, 2, 3]和[1, 3]是 好子集，乘积分别为6 = 2*3，6 = 2*3和3 = 3。
    // [1, 4] 和[4]不是 好子集，因为乘积分别为4 = 2*2 和4 = 2*2。
    // 请你返回 nums中不同的好子集的数目对109 + 7取余的结果。
    // nums中的 子集是通过删除 nums中一些（可能一个都不删除，也可能全部都删除）
    // 元素后剩余元素组成的数组。
    // 如果两个子集删除的下标不同，那么它们被视为不同的子集。
    // 测试链接 : https://leetcode.cn/problems/the-number-of-good-subsets/
    // 2, 3, 5, 6, 7, 10, 11, 13, 14,
    // 15, 17, 19, 21, 22, 23, 26, 29, 30
    public static int[] primes = {
            //        11 7 5 3 2
            // 2       0 0 0 0 1
            // 2 5     0 0 1 0 1
            0, // 0 00000000
            0, // 1 00000000
            1, // 2 00000001
            2, // 3 00000010
            0, // 4 00000000
            4, // 5 00000100
            3, // 6 00000011
            8, // 7 00001000
            0, // 8 00000000
            0, // 9 00000000
            5, // 10 00000101
            16, 0, 32, 9, 6, 0, 64, 0, 128, 0, 10, 17, 256, 0, 0, 33, 0, 0,
            512,// 29  10000000
            7   // 30  2 * 3 * 5   111
    };

    public static int[] counts = new int[31];
    public static int[] status = new int[1 << 10];
    public static int mod = 1000000007;

    public static int numberOfGoodSubsets(int[] nums) {
        Arrays.fill(counts, 0);
        Arrays.fill(status, 0);
        for (int num : nums) {
            counts[num]++;
        }
        status[0] = 1;
        for (int i = 0; i < counts[1]; i++) {
            status[0] = (status[0] << 1) % mod;
        }
        for (int i = 2; i <= 30; i++) {
            // 2 几次 3 几次 4几次 5几次 30 几次
            int curPrimesStatus = primes[i];
            if (curPrimesStatus != 0 && counts[i] != 0) {
                // curPrimesStatus K次
                for (int from = 0; from < (1 << 10); from++) {
                    // from 11111111
                    // 枚举所有的状态 from
                    // from & curPrimesStatus == 0
                    if ((from & curPrimesStatus) == 0) {
                        // to
                        int to = from | curPrimesStatus;
                        status[to] = (int) (((long) status[to] + ((long) status[from] * counts[i])) % mod);
//						// status[to] += status[from] * counts[i];
                    }
                }
            }
        }
        int ans = 0;
        for (int s = 1; s < (1 << 10); s++) {
            ans = (ans + status[s]) % mod;
        }
        return ans;
    }

    // 来自猿辅导
    // 2022.8.7笔试第三道
    // 给定一个数组arr，和一个正数k
    // 如果arr[i] == 0，表示i这里既可以是左括号也可以是右括号，
    // 而且可以涂上1~k每一种颜色
    // 如果arr[i] != 0，表示i这里已经确定是左括号，颜色就是arr[i]的值
    // 那么arr整体就可以变成某个括号字符串，并且每个括号字符都带有颜色
    // 返回在括号字符串合法的前提下，有多少种不同的染色方案
    // 不管是排列、还是颜色，括号字符串任何一点不一样，就算不同的染色方案
    // 最后的结果%10001，为了方便，我们不处理mod，就管核心思路
    // 2 <= arr长度 <= 5000
    // 1 <= k <= 1000
    // 0 <= arr[i] <= k
    // 正式方法
    // 时间复杂度O(N^2), N是数组长度
    // 首先求合法的括号组合数量（忽略染色这件事），
    // 就是combines方法，看注释
    // 当括号数量求出来，再看染色能有几种
    // 比如忽略颜色，某个合法的括号结合 长度为n，
    // 如果已经有b个涂上了颜色，而且是左括号
    // 那么，因为该结合是合法的，
    // 所以这b个涂上了颜色的左括号，和哪些右括号结合，
    // 其实是确定的，这些右括号颜色也是确定的
    // 那么还剩n-(b*2)个字符
    // 这n-(b*2)个字符，就是(n-(b*2))/2对括号
    // 每对括号都可以自由发挥，所以，任何一个合法的组合，涂色方案为k^((n-(b*2))/2)
    // 最终答案 : 合法括号组合数量 * k^((n-(b*2))/2)
    public static int ways2(int[] arr, int k) {
        int n = arr.length;
        if ((n & 1) != 0) {
            return 0;
        }
        int a = combines(arr);
        int b = 0;
        for (int num : arr) {
            if (num != 0) {
                b++;
            }
        }
        return a * ((int) Math.pow(k, (n - (b << 1)) >> 1));
    }

    // 忽略染色这件事，求合法的括号结合数量
    public static int combines(int[] arr) {
        int n = arr.length;
        int[][] dp = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                dp[i][j] = -1;
            }
        }
        return f(arr, 0, 0, dp);
    }

    // arr[i....]范围上，去做决定
    // j : arr[0..i-1]已经做完决定的部分，左括号比右括号，多几个
    // 返回：
    // arr[i....]范围上，去做决定，
    // 已经做完决定的部分，左括号比右括号多j个
    // 这样的情况下，最终合法的括号结合，多少个！
    // process(arr, 0, 0)
    public static int process(int[] arr, int i, int j) {
        if (i == arr.length) {
            return j == 0 ? 1 : 0;
        }
        if (j < 0) {
            return 0;
        }
        // 这个不写也行
        // 锦上添花的剪枝条件
        if (arr.length - i < j) {
            return 0;
        }
        // arr[i] != 0
        if (arr[i] != 0) {
            // (
            return process(arr, i + 1, j + 1);
        } else {
            // arr[i] 0 ? ( )
            int p1 = process(arr, i + 1, j + 1);
            int p2 = process(arr, i + 1, j - 1);
            return p1 + p2;
        }
    }

    // 在arr[i...]范围上做决定
    // 之前在arr[0...i-1]上的决定，使得左括号比右括号多了j个
    // 最终合法的括号结合是多少
    public static int f(int[] arr, int i, int j, int[][] dp) {
        int n = arr.length;
        if (i == n) {
            return j == 0 ? 1 : 0;
        }
        if (j < 0) {
            return 0;
        }
        if (n - i < j) {
            return 0;
        }
        // 如果缓存命中，直接返回答案
        if (dp[i][j] != -1) {
            return dp[i][j];
        }
        int ans = 0;
        if (arr[i] > 0) {
            ans = f(arr, i + 1, j + 1, dp);
        } else {
            ans = f(arr, i + 1, j + 1, dp) + f(arr, i + 1, j - 1, dp);
        }
        dp[i][j] = ans;
        return ans;
    }

    // 来自米哈游
    // 给定一个正数n，表示有多少个节点
    // 给定一个二维数组edges，表示所有无向边
    // edges[i] = {a, b} 表示a到b有一条无向边
    // edges一定表示的是一个无环无向图，也就是树结构
    // 每个节点可以染1、2、3三种颜色
    // 要求 : 非叶节点的相邻点一定要至少有两种和自己不同颜色的点
    // 返回一种达标的染色方案，也就是一个数组，表示每个节点的染色状况
    // 1 <= 节点数量 <= 10的5次方
    // 1 2 3 1 2 3 1 2 3
    public static int[] rule1 = {1, 2, 3};

    // 1 3 2 1 3 2 1 3 2
    public static int[] rule2 = {1, 3, 2};

    public static int[] dye(int n, int[][] edges) {
        ArrayList<ArrayList<Integer>> graph = new ArrayList<>();
        // 0 : { 2, 1 }
        // 1 : { 0 }
        // 2 : { 0 }
        for (int i = 0; i < n; i++) {
            graph.add(new ArrayList<>());
        }
        for (int[] edge : edges) {
            // 0 -> 2
            // 1 -> 0
            graph.get(edge[0]).add(edge[1]);
            graph.get(edge[1]).add(edge[0]);
        }
        // 选一个头节点！
        int head = -1;
        for (int i = 0; i < n; i++) {
            if (graph.get(i).size() >= 2) {
                head = i;
                break;
            }
        }
        // graph
        // head
        int[] colors = new int[n];
        if (head == -1) { // 两个点，互相连一下
            // 把colors，所有位置，都设置成1
            Arrays.fill(colors, 1);
        } else {
            // dfs 染色了！
            colors[head] = 1;
            dfs(graph, graph.get(head).get(0), 1, rule1, colors);
            for (int i = 1; i < graph.get(head).size(); i++) {
                dfs(graph, graph.get(head).get(i), 1, rule2, colors);
            }
        }
        return colors;
    }

    // 整个图结构，都在graph
    // 当前来到的节点，是head号节点
    // head号节点，在level层
    // 染色的规则，rule {1,2,3...} {1,3,2...}
    // 做的事情：以head为头的整颗树，每个节点，都染上颜色
    // 填入到colors数组里去
    public static void dfs(
            ArrayList<ArrayList<Integer>> graph,
            int head,
            int level,
            int[] rule, int[] colors) {

        colors[head] = rule[level % 3];
        for (int next : graph.get(head)) {
            if (colors[next] == 0) {
                dfs(graph, next, level + 1, rule, colors);
            }
        }
    }

    // 为了测试
    // 生成无环无向图
    public static int[][] randomEdges(int n) {
        int[] order = new int[n];
        for (int i = 0; i < n; i++) {
            order[i] = i;
        }
        for (int i = n - 1; i >= 0; i--) {
            swap(order, i, (int) (Math.random() * (i + 1)));
        }
        int[][] edges = new int[n - 1][2];
        for (int i = 1; i < n; i++) {
            edges[i - 1][0] = order[i];
            edges[i - 1][1] = order[(int) (Math.random() * i)];
        }
        return edges;
    }

    // 为了测试
    public static void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

    // 为了测试
    public static boolean rightAnswer(int n, int[][] edges, int[] colors) {
        ArrayList<ArrayList<Integer>> graph = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            graph.add(new ArrayList<>());
        }
        for (int[] edge : edges) {
            graph.get(edge[0]).add(edge[1]);
            graph.get(edge[1]).add(edge[0]);
        }
        boolean[] hasColors = new boolean[4];
        for (int i = 0, colorCnt = 1; i < n; i++, colorCnt = 1) {
            if (colors[i] == 0) {
                return false;
            }
            if (graph.get(i).size() <= 1) { // i号点是叶节点
                continue;
            }
            hasColors[colors[i]] = true;
            for (int near : graph.get(i)) {
                if (!hasColors[colors[near]]) {
                    hasColors[colors[near]] = true;
                    colorCnt++;
                }
            }
            if (colorCnt != 3) {
                return false;
            }
            Arrays.fill(hasColors, false);
        }
        return true;
    }

    // 给定一个逆波兰式
    // 转化成正确的中序表达式
    // 要求只有必要加括号的地方才加括号
    // 请保证给定的逆波兰式是正确的！
    public static int getAns(String rpn) {
        if (rpn == null || rpn.equals("")) {
            return 0;
        }
        String[] parts = rpn.split(" ");
        Stack<Integer> stack = new Stack<>();
        for (String part : parts) {
            if (part.equals("+") || part.equals("-") || part.equals("*") || part.equals("/")) {
                int right = stack.pop();
                int left = stack.pop();
                int ans = 0;
                if (part.equals("+")) {
                    ans = left + right;
                } else if (part.equals("-")) {
                    ans = left - right;
                } else if (part.equals("*")) {
                    ans = left * right;
                } else {
                    ans = left / right;
                }
                stack.push(ans);
            } else {
                stack.push(Integer.valueOf(part));
            }
        }
        // stack 只有一个数，最终的结果
        return stack.pop();
    }

    enum Operation {
        SingleNumber, AddOrMinus, MultiplyOrDivide;
    }

    // 请保证输入的逆波兰式是正确的
    // 否则该函数不保证正确性
    // 逆波兰式仅支持+、-、*、/
    // 想支持更多算术运算符自己改
    public static String convert(String rpn) {
        if (rpn == null || rpn.equals("")) {
            return rpn;
        }
        String[] parts = rpn.split(" ");
        Stack<String> stack1 = new Stack<>();
        Stack<Operation> stack2 = new Stack<>();
        for (String cur : parts) {
            // cur 当前遇到的字符串
            // +- */ 单数
            if (cur.equals("+") || cur.equals("-")) {
                String b = stack1.pop();
                String a = stack1.pop();
                stack2.pop();
                stack2.pop();
                stack1.push(a + cur + b);
                stack2.push(Operation.AddOrMinus);
            } else if (cur.equals("*") || cur.equals("/")) {
                String b = stack1.pop();
                String a = stack1.pop();
                Operation bOp = stack2.pop();
                Operation aOp = stack2.pop();
                // 只有当左操作数或者右操作数是加减运算出来的的结果，要给这个结果加上括号，因为不加上括号，乘除会影响结果的值
                String left = aOp == Operation.AddOrMinus ? ("(" + a + ")") : (a);
                String right = bOp == Operation.AddOrMinus ? ("(" + b + ")") : (b);
                stack1.push(left + cur + right);
                stack2.push(Operation.MultiplyOrDivide);
            } else {
                stack1.push(cur);
                stack2.push(Operation.SingleNumber);
            }
        }
        return stack1.pop();
    }

    // 给你一个 n个节点的有向图，节点编号为0到n - 1，
    // 其中每个节点至多有一条出边。
    // 图用一个大小为n下标从0开始的数组edges表示，
    // 节点i到节点edges[i]之间有一条有向边。
    // 如果节点i没有出边，那么edges[i] == -1。
    // 请你返回图中的最长环，如果没有任何环，请返回-1。
    // 一个环指的是起点和终点是同一个节点的路径。
    // 测试链接 : https://leetcode.cn/problems/longest-cycle-in-a-graph/
    // 除了下面的解法，还可以看强连通分量求解的方法
    public int longestCycle(int[] edges) {
        // edges[i] = j  i -> j i到j有条边
        // edges[i] = -1  i X   i没有出边
        int n = edges.length;
        // 每个点默认编号为0，不为0代表已经访问过
        int[] ids = new int[n];
        // 发现的最大环，有几个点！
        int ans = -1;
        // cnt是编号
        for (int from = 0, cnt = 1; from < n; from++) {
            if (ids[from] == 0) {
                for (int cur = from, fromId = cnt; cur != -1; cur = edges[cur]) {
                    // from -> -> cur ->
                    if (ids[cur] > 0) {
                        // 访问过，此时的环，之前遍历过的点
                        if (ids[cur] >= fromId) { // 新的环
                            ans = Math.max(ans, cnt - ids[cur]);
                        }
                        // 如果上面的if不成立，老的点，直接跳出
                        break;
                    }
                    ids[cur] = cnt++;
                }
            }
        }
        return ans;
    }

    // 来自学员问题
    // 给定怪兽的血量为hp
    // 第i回合如果用刀砍，怪兽在这回合会直接掉血，没有后续效果
    // 第i回合如果用毒，怪兽在这回合不会掉血，
    // 但是之后每回合都会掉血，并且所有中毒的后续效果会叠加
    // 给定的两个数组cuts、poisons，两个数组等长，长度都是n
    // 表示你在n回合内的行动，
    // 每一回合的刀砍的效果由cuts[i]表示
    // 每一回合的中毒的效果由poisons[i]表示
    // 如果你在n个回合内没有直接杀死怪兽，意味着你已经无法有新的行动了
    // 但是怪兽如果有中毒效果的话，那么怪兽依然会在hp耗尽的那回合死掉
    // 返回你最快能在多少回合内将怪兽杀死
    // 数据范围 :
    // 1 <= n <= 10的5次方
    // 1 <= hp <= 10的9次方
    // 1 <= cuts[i]、poisons[i] <= 10的9次方
    public static int fast(int[] cuts, int[] poisons, int hp) {
        // 怪兽可能的最快死亡回合
        int l = 1;
        // 怪兽可能的最晚死亡回合
        int r = hp + 1;
        int m = 0;
        int ans = Integer.MAX_VALUE;
        while (l <= r) {
            m = l + ((r - l) >> 1);
            if (ok(cuts, poisons, hp, m)) {
                ans = m;
                r = m - 1;
            } else {
                l = m + 1;
            }
        }
        return ans;
    }

    public static boolean ok(int[] cuts, int[] poisons, long hp, int limit) {
        int n = Math.min(cuts.length, limit);
        for (int i = 0, j = 1; i < n; i++, j++) {
            hp -= Math.max((long) cuts[i], (long) (limit - j) * (long) poisons[i]);
            if (hp <= 0) {
                return true;
            }
        }
        return false;
    }

    // 设计一个最大栈数据结构，既支持栈操作，又支持查找栈中最大元素。
    // 实现MaxStack类：
    // MaxStack()初始化栈对象
    // void push(int x)将元素 x 压入栈中。
    // int pop()移除栈顶元素并返回这个元素。
    // int top()返回栈顶元素，无需移除。
    // int peekMax()检索并返回栈中最大元素，无需移除。
    // int popMax()检索并返回栈中最大元素，并将其移除。
    // 如果有多个最大元素，只要移除最靠近栈顶的那个。
    // 测试链接 : https://leetcode.cn/problems/max-stack/
    static class MaxStack {

        public int cnt;

        public HeapGreater<Node> heap;

        public Node top;

        public MaxStack() {
            cnt = 0;
            heap = new HeapGreater<>(new NodeComparator());
            top = null;
        }

        public void push(int x) {
            Node cur = new Node(x, ++cnt);
            heap.push(cur);
            if (top == null) {
                top = cur;
            } else {
                top.last = cur;
                cur.next = top;
                top = cur;
            }
        }

        public int pop() {
            Node ans = top;
            if (top.next == null) {
                top = null;
            } else {
                top = top.next;
                top.last = null;
            }
            heap.remove(ans);
            return ans.val;
        }

        public int top() {
            return top.val;
        }

        public int peekMax() {
            return heap.peek().val;
        }

        public int popMax() {
            Node ans = heap.pop();
            if (ans == top) {
                if (top.next == null) {
                    top = null;
                } else {
                    top = top.next;
                    top.last = null;
                }
            } else {
                if (ans.next != null) {
                    ans.next.last = ans.last;
                }
                if (ans.last != null) {
                    ans.last.next = ans.next;
                }
            }
            return ans.val;
        }

        static class Node {
            public int val;
            public int cnt;
            public Node next;
            public Node last;

            public Node(int v, int c) {
                val = v;
                cnt = c;
            }
        }

        static class NodeComparator implements Comparator<Node> {

            @Override
            public int compare(Node o1, Node o2) {
                return o1.val != o2.val ? (o2.val - o1.val) : (o2.cnt - o1.cnt);
            }

        }

        static class HeapGreater<T> {

            private ArrayList<T> heap;
            private HashMap<T, Integer> indexMap;
            private int heapSize;
            private Comparator<? super T> comp;

            public HeapGreater(Comparator<? super T> c) {
                heap = new ArrayList<>();
                indexMap = new HashMap<>();
                heapSize = 0;
                comp = c;
            }

            public T peek() {
                return heap.get(0);
            }

            public void push(T obj) {
                heap.add(obj);
                indexMap.put(obj, heapSize);
                heapInsert(heapSize++);
            }

            public T pop() {
                T ans = heap.get(0);
                swap(0, heapSize - 1);
                indexMap.remove(ans);
                heap.remove(--heapSize);
                heapify(0);
                return ans;
            }

            public void remove(T obj) {
                T replace = heap.get(heapSize - 1);
                int index = indexMap.get(obj);
                indexMap.remove(obj);
                heap.remove(--heapSize);
                if (obj != replace) {
                    heap.set(index, replace);
                    indexMap.put(replace, index);
                    resign(replace);
                }
            }

            private void resign(T obj) {
                heapInsert(indexMap.get(obj));
                heapify(indexMap.get(obj));
            }

            private void heapInsert(int index) {
                while (comp.compare(heap.get(index), heap.get((index - 1) / 2)) < 0) {
                    swap(index, (index - 1) / 2);
                    index = (index - 1) / 2;
                }
            }

            private void heapify(int index) {
                int left = index * 2 + 1;
                while (left < heapSize) {
                    int best = left + 1 < heapSize && comp.compare(heap.get(left + 1), heap.get(left)) < 0 ? (left + 1)
                            : left;
                    best = comp.compare(heap.get(best), heap.get(index)) < 0 ? best : index;
                    if (best == index) {
                        break;
                    }
                    swap(best, index);
                    index = best;
                    left = index * 2 + 1;
                }
            }

            private void swap(int i, int j) {
                T o1 = heap.get(i);
                T o2 = heap.get(j);
                heap.set(i, o2);
                heap.set(j, o1);
                indexMap.put(o2, i);
                indexMap.put(o1, j);
            }

        }
    }

    // 这里有n个航班，它们分别从 1 到 n 进行编号。
    // 有一份航班预订表bookings ，
    // 表中第i条预订记录bookings[i] = [firsti, lasti, seatsi]
    // 意味着在从firsti到lasti
    //（包含 firsti和lasti）的 每个航班上预订了seatsi个座位。
    // 请你返回一个长度为n的数组answer，里面的元素是每个航班预定的座位总数。
    // 测试链接 : https://leetcode.cn/problems/corporate-flight-bookings/
    public static int[] corpFlightBookings(int[][] bookings, int n) {
        // 1 2 3 4 n
        // 0 1 2 3 .. n n+1
        int[] cnt = new int[n + 2];
        for (int[] book : bookings) {
            // start book[0]
            // end   book[1]
            // 票    book[2]
            cnt[book[0]] += book[2];
            cnt[book[1] + 1] -= book[2];
        }
        for (int i = 1; i < cnt.length; i++) {
            cnt[i] += cnt[i - 1];
        }
        int[] ans = new int[n];
        for (int i = 0; i < n; i++) {
            ans[i] = cnt[i + 1];
        }
        return ans;
    }

    // 给你一个数组nums，我们可以将它按一个非负整数 k 进行轮调，
    // 例如，数组为nums = [2,4,1,3,0]，
    // 我们按k = 2进行轮调后，它将变成[1,3,0,2,4]。
    // 这将记为 3 分，
    // 因为 1 > 0 [不计分]、3 > 1 [不计分]、0 <= 2 [计 1 分]、
    // 2 <= 3 [计 1 分]，4 <= 4 [计 1 分]。
    // 在所有可能的轮调中，返回我们所能得到的最高分数对应的轮调下标 k 。
    // 如果有多个答案，返回满足条件的最小的下标 k 。
    // 测试链接 : 
    // https://leetcode.cn/problems/smallest-rotation-with-highest-score/
    public static int bestRotation(int[] nums) {
        int n = nums.length;

        // cnt : 差分数组
        // cnt最后进行前缀和的加工！
        // 加工完了的cnt[0] :  整体向右移动0的距离, 一共能得多少分
        // 加工完了的cnt[i] :  整体向右移动i的距离, 一共能得多少分
        int[] cnt = new int[n + 1];
        for (int i = 0; i < n; i++) {
            // 遍历每个数！
            // 看看每个数，对差分数组哪些范围，会产生影响!
            if (nums[i] < n) {
                if (i <= nums[i]) {
                    add(cnt, nums[i] - i, n - i - 1);
                } else {
                    add(cnt, 0, n - i - 1);
                    add(cnt, n - i + nums[i], n - 1);
                }
            }
        }
        for (int i = 1; i <= n; i++) {
            cnt[i] += cnt[i - 1];
        }
        // 最大得分是啥！已经求出来了
        int max = cnt[0];
        int ans = 0;
        for (int i = n - 1; i >= 1; i--) {
            // 整体移动的i 0 n-1 n-2 n-3 1
            //         k 0  1   2   3   n-1
            if (cnt[i] > max) {
                max = cnt[i];
                ans = i;
            }
        }
        return ans == 0 ? 0 : (n - ans);
    }

    public static void add(int[] cnt, int l, int r) {
        cnt[l]++;
        cnt[r + 1]--;
    }

    // 小红书
    // 3.13 笔试
    // 数组里有0和1，一定要翻转一个区间，翻转：0变1，1变0
    // 请问翻转后可以使得1的个数最多是多少？
    public static int maxOneNumbers1(int[] arr) {
        int ans = 0;
        for (int l = 0; l < arr.length; l++) {
            for (int r = l; r < arr.length; r++) {
                reverse(arr, l, r);
                ans = Math.max(ans, oneNumbers(arr));
                reverse(arr, l, r);
            }
        }
        return ans;
    }

    public static void reverse(int[] arr, int l, int r) {
        for (int i = l; i <= r; i++) {
            arr[i] ^= 1;
        }
    }

    public static int oneNumbers(int[] arr) {
        int ans = 0;
        for (int num : arr) {
            ans += num;
        }
        return ans;
    }

    public static int maxOneNumbers2(int[] arr) {
        int ans = 0;
        for (int num : arr) {
            ans += num;
        }
        for (int i = 0; i < arr.length; i++) {
            arr[i] = arr[i] == 0 ? 1 : -1;
        }
        int max = Integer.MIN_VALUE;
        int cur = 0;
        for (int i = 0; i < arr.length; i++) {
            cur += arr[i];
            max = Math.max(max, cur);
            cur = Math.max(cur, 0);
        }
        return ans + max;
    }

    // 小红书
    // 3.13 笔试
    // 给定一个数组，想随时查询任何范围上的最大值
    // 如果只是根据初始数组建立、并且以后没有修改，
    // 那么RMQ方法比线段树方法好实现，时间复杂度O(N*logN)，额外空间复杂度O(N*logN)
    // RMQ结构是解决范围上查询最大或最小的结构
    public static class RMQ {
        public int[][] max;

        // 下标一定要从1开始，没有道理！就是约定俗成！
        public RMQ(int[] arr) {
            // 长度！
            int n = arr.length;
            // 2的几次方，可以拿下n
            int k = power2(n);
            // n*logn
            max = new int[n + 1][k + 1];
            for (int i = 1; i <= n; i++) {
                // i 0：从下标i开始，往下连续的2的0次方个数，中，最大值
                // 1...1个
                // 2...1个
                // 3...1个
                max[i][0] = arr[i - 1];
            }
            for (int j = 1; (1 << j) <= n; j++) {
                // i...连续的、2的1次方个数，这个范围，最大值
                // i...连续的、2的2次方个数，这个范围，最大值
                // i...连续的、2的3次方个数，这个范围，最大值
                for (int i = 1; i + (1 << j) - 1 <= n; i++) {
                    // max[10][3]
                    // 下标10开始，连续的8个数，最大值是多少
                    // 1) max[10][2]
                    // 2) max[14][2]
                    max[i][j] = Math.max(
                            max[i][j - 1],
                            max[i + (1 << (j - 1))][j - 1]);
                }
            }
        }

        public int max(int l, int r) {
            // l...r -> r - l + 1 -> 2的哪个次方最接近它！
            int k = power2(r - l + 1);
            return Math.max(max[l][k], max[r - (1 << k) + 1][k]);
        }

        private int power2(int m) {
            int ans = 0;
            while ((1 << ans) <= (m >> 1)) {
                ans++;
            }
            return ans;
        }
    }

    // 来自腾讯音乐
    // 原本数组中都是大于0、小于等于k的数字，是一个单调不减的数组
    // 其中可能有相等的数字，总体趋势是递增的
    // 但是其中有些位置的数被替换成了0，我们需要求出所有的把0替换的方案数量：
    // 1）填充的每一个数可以大于等于前一个数，小于等于后一个数
    // 2）填充的每一个数不能大于k
    // 动态规划
    public static long validSortedArrayWays(int[] nums, int k) {
        int n = nums.length;
        // dp[i][j] : 一共i个格子，随意填，但是不能降序，j种数可以选
        long[][] dp = new long[n + 1][k + 1];
        for (int i = 1; i <= n; i++) {
            dp[i][1] = 1;
        }
        for (int i = 1; i <= k; i++) {
            dp[1][i] = i;
        }
        for (int i = 2; i <= n; i++) {
            for (int j = 2; j <= k; j++) {
                dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
            }
        }
        long res = 1;
        for (int i = 0, j = 0; i < nums.length; i++) {
            if (nums[i] == 0) {
                j = i + 1;
                while (j < nums.length && nums[j] == 0) {
                    j++;
                }
                int leftValue = i - 1 >= 0 ? nums[i - 1] : 1;
                int rightValue = j < nums.length ? nums[j] : k;
                res *= dp[j - i][rightValue - leftValue + 1];
                i = j;
            }
        }
        return res;
    }

    // 数学方法
    // 结论：
    // a ~ b范围的数字随便选，可以选重复的数，一共选m个
    // 选出有序序列的方案数：C ( m, b - a + m )
    public static long validSortedArrayWays2(int[] nums, int k) {
        long res = 1;
        for (int i = 0, j = 0; i < nums.length; i++) {
            if (nums[i] == 0) {
                j = i + 1;
                while (j < nums.length && nums[j] == 0) {
                    j++;
                }
                int leftValue = i - 1 >= 0 ? nums[i - 1] : 1;
                int rightValue = j < nums.length ? nums[j] : k;
                int numbers = j - i;
                res *= c(rightValue - leftValue + numbers, numbers);
                i = j;
            }
        }
        return res;
    }

    // 从一共a个数里，选b个数，方法数是多少
    public static long c(int a, int b) {
        if (a == b) {
            return 1;
        }
        long x = 1;
        long y = 1;
        for (int i = b + 1, j = 1; i <= a; i++, j++) {
            x *= i;
            y *= j;
            long gcd = gcd(x, y);
            x /= gcd;
            y /= gcd;
        }
        return x / y;
    }

    public static long gcd(long m, long n) {
        return n == 0 ? m : gcd(n, m % n);
    }

    // 总长度为n的数组中，所有长度为k的子序列里，有多少子序列的和为偶数
    public static int sumEvenSubNumber1(int[] arr, int k) {
        if (arr == null || arr.length == 0 || k < 1 || k > arr.length) {
            return 0;
        }
        return process1(arr, 0, k, 0);
    }

    public static int process1(int[] arr, int index, int rest, int sum) {
        if (index == arr.length) {
            return rest == 0 && (sum & 1) == 0 ? 1 : 0;
        } else {
            return process1(arr, index + 1, rest, sum) + process1(arr, index + 1, rest - 1, sum + arr[index]);
        }
    }

    public static int sumEvenSubNumber2(int[] arr, int k) {
        if (arr == null || arr.length == 0 || k < 1 || k > arr.length) {
            return 0;
        }
        int n = arr.length;
        // even[i][j] : 在前i个数的范围上(0...i-1)，一定选j个数，加起来是偶数的子序列个数
        // odd[i][j] : 在前i个数的范围上(0...i-1)，一定选j个数，加起来是奇数的子序列个数
        int[][] even = new int[n + 1][k + 1];
        int[][] odd = new int[n + 1][k + 1];
        for (int i = 0; i <= n; i++) {
            // even[0][0] = 1;
            // even[1][0] = 1;
            // even[2][0] = 1;
            // even[n][0] = 1;
            even[i][0] = 1;
        }
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= Math.min(i, k); j++) {
                even[i][j] = even[i - 1][j];
                odd[i][j] = odd[i - 1][j];
                even[i][j] += (arr[i - 1] & 1) == 0 ? even[i - 1][j - 1] : odd[i - 1][j - 1];
                odd[i][j] += (arr[i - 1] & 1) == 0 ? odd[i - 1][j - 1] : even[i - 1][j - 1];
            }
        }
        return even[n][k];
    }

    // 补充一个更数学的方法
    // 统计arr中的偶数个数、奇数个数
    // k个数加起来是偶数的方案 :
    // 1) 奇数选0个，偶数选k个
    // 2) 奇数选2个，偶数选k-2个
    // 3) 奇数选4个，偶数选k-4个
    // ...
    public static int sumEvenSubNumber3(int[] arr, int k) {
        if (arr == null || arr.length == 0 || k < 1 || k > arr.length) {
            return 0;
        }
        int even = 0;
        int odd = 0;
        for (int num : arr) {
            if ((num & 1) == 0) {
                even++;
            } else {
                odd++;
            }
        }
        int ans = 0;
        for (int pick = 0, rest = k; pick <= k; pick += 2, rest -= 2) {
            ans += c(pick, odd) * c(rest, even);
        }
        return ans;
    }

    public static long c(long m, long n) {
        if (m > n) {
            return 0;
        }
        if (m == 0 && m == n) {
            return 1;
        }
        long up = 1;
        long down = 1;
        for (long i = m + 1, j = 1; i <= n; i++, j++) {
            up *= i;
            down *= j;
            long gcd = gcd(up, down);
            up /= gcd;
            down /= gcd;
        }
        return up;
    }

    // 来自微众
    // 4.11笔试
    // 给定n位长的数字字符串和正数k，求该子符串能被k整除的子串个数
    // (n<=1000，k<=100)
    // 正式方法
    // 时间复杂度O(N * k)
    public static int modKSubstringNumber(String s, int k) {
        int[] cur = new int[k];
        // 帮忙迁移
        int[] next = new int[k];
        // 0...i 整体余几？
        int mod = 0;
        // 答案：统计有多少子串的值%k == 0
        int ans = 0;
        for (char cha : s.toCharArray()) {
            for (int i = 0; i < k; i++) {
                // i -> 10个
                // (i * 10) % k
                next[(i * 10) % k] += cur[i];
                cur[i] = 0;
            }
            int[] tmp = cur;
            cur = next;
            next = tmp;
            mod = (mod * 10 + (cha - '0')) % k;
            ans += (mod == 0 ? 1 : 0) + cur[mod];
            cur[mod]++;
        }
        return ans;
    }

    // 来自蔚来汽车
    // 给你一个整数数组arr，你一开始在数组的第一个元素处（下标为 0）。
    // 每一步，你可以从下标i跳到下标i + 1 、i - 1 或者 j ：
    // i + 1 需满足：i + 1 < arr.length
    // i - 1需满足：i - 1 >= 0
    // j需满足：arr[i] == arr[j]且i != j
    // 请你返回到达数组最后一个元素的下标处所需的最少操作次数。
    // 注意：任何时候你都不能跳到数组外面。
    // leetcode测试链接 : https://leetcode-cn.com/problems/jump-game-iv/
    public static int jumMinSameValue(int[] arr) {
        int n = arr.length;
        // key : 某个值9，
        // value : 列表：0，7，19
        HashMap<Integer, ArrayList<Integer>> valueIndex = new HashMap<>();
        for (int i = 0; i < n; i++) {
            if (!valueIndex.containsKey(arr[i])) {
                valueIndex.put(arr[i], new ArrayList<>());
            }
            valueIndex.get(arr[i]).add(i);
        }
        // i会有哪些展开：左，右，i通过自己的值，能蹦到哪些位置上去
        // 宽度优先遍历，遍历过的位置，不希望重复处理
        // visited[i] == false：i位置，之前没来过，可以处理
        // visited[i] == true : i位置，之前来过，可以跳过
        boolean[] visited = new boolean[n];
        int[] queue = new int[n];
        int l = 0;
        int r = 0;
        // 0位置加到队列里去
        queue[r++] = 0;
        visited[0] = true;
        int jump = 0;
        // 宽度优先遍历
        // 一次，遍历一整层！
        // 该技巧，多次出现！
        while (l != r) { // 队列里还有东西的意思！
            // 此时的r记录！
            // 0 1 2 | 3 4 5 6 7 8
            // 当前层的终止位置
            int tmp = r;
            for (; l < tmp; l++) { // 遍历当前层！
                int cur = queue[l];
                if (cur == n - 1) {
                    return jump;
                }
                if (cur + 1 < n && !visited[cur + 1]) {
                    visited[cur + 1] = true;
                    queue[r++] = cur + 1;
                }
                // cur > 0  cur - 1 >=0
                if (cur > 0 && !visited[cur - 1]) {
                    visited[cur - 1] = true;
                    queue[r++] = cur - 1;
                }
                // i -> 9
                // 值同样为9的那些位置，也能去
                for (int next : valueIndex.get(arr[cur])) {
                    if (!visited[next]) {
                        visited[next] = true;
                        queue[r++] = next;
                    }
                }
                // 重要优化！
                valueIndex.get(arr[cur]).clear();
            }
            jump++;
        }
        return -1;
    }

    // 来自微众
    // 人工智能岗
    // 一开始有21个球，甲和乙轮流拿球，甲先、乙后
    // 每个人在自己的回合，一定要拿不超过3个球，不能不拿
    // 最终谁的总球数为偶数，谁赢
    // 请问谁有必胜策略
    // balls = 21
    // ball是奇数
    public static String whoWin21Balls(int balls) {
        return process(0, balls, 0, 0);
    }

    // 憋递归！
    // turn 谁的回合！
    // turn == 0 甲回合
    // turn == 1 乙回合
    // rest剩余球的数量
    // 之前，jiaBalls、yiBalls告诉你！
    // 当前，根据turn，知道是谁的回合！
    // 当前，还剩多少球，rest
    // 返回：谁会赢！
    public static String process(int turn, int rest, int jia, int yi) {
        if (rest == 0) {
            return (jia & 1) == 0 ? "甲" : "乙";
        }
        // rest > 0, 还剩下球！
        if (turn == 0) { // 甲的回合！
            // 甲，自己赢！甲赢！
            for (int pick = 1; pick <= Math.min(rest, 3); pick++) {
                // pick 甲当前做的选择
                if (process(1, rest - pick, jia + pick, yi).equals("甲")) {
                    return "甲";
                }
            }
            return "乙";
        } else {
            for (int pick = 1; pick <= Math.min(rest, 3); pick++) {
                // pick 甲当前做的选择
                if (process(0, rest - pick, jia, yi + pick).equals("乙")) {
                    return "乙";
                }
            }
            return "甲";
        }
    }

    // 来自学员问题，真实大厂面试题
    // 1、2、3...n-1、n、n、n+1、n+2...
    // 在这个序列中，只有一个数字有重复(n)
    // 这个序列是无序的，找到重复数字n
    // 这个序列是有序的，找到重复数字n
    // 符合题目要求的、无序数组，找重复数
    // 时间复杂度O(N)，额外空间复杂度O(1)
    public static int findDuplicate(int[] arr) {
        if (arr == null || arr.length < 2) {
            return -1;
        }
        int slow = arr[0];
        int fast = arr[arr[0]];
        while (slow != fast) {
            slow = arr[slow];
            fast = arr[arr[fast]];
        }
        // slow == fast
        fast = 0;
        while (slow != fast) {
            fast = arr[fast];
            slow = arr[slow];
        }
        // 再相遇！一个结论
        return slow;
    }

    // 符合题目要求的、有序数组，找重复数
    // 时间复杂度O(logN)，额外空间复杂度O(1)
    public static int findDuplicateSorted(int[] arr) {
        if (arr == null || arr.length < 2) {
            return -1;
        }
        int l = 0;
        int r = arr.length - 1;
        int m = 0;
        int ans = -1;
        while (l <= r) {
            m = (l + r) / 2;
            if ((m - 1 >= 0 && arr[m - 1] == arr[m]) || (m + 1 < arr.length && arr[m + 1] == arr[m])) {
                ans = arr[m];
                break;
            }
            if (m - l == arr[m] - arr[l]) {
                l = m + 1;
            } else {
                r = m - 1;
            }
        }
        return ans;
    }

    // 来自学员问题，蓝桥杯练习题
    // f(i) : i的所有因子，每个因子都平方之后，累加起来
    // 比如f(10) = 1平方 + 2平方 + 5平方 + 10平方 = 1 + 4 + 25 + 100 = 130
    // 给定一个数n，求f(1) + f(2) + .. + f(n)
    // n <= 10的9次方
    // O(n)的方法都会超时！低于它的！
    // O(根号N)的方法，就过了，一个思路
    // O(log N)的方法，
    // 暴力方法
    public static long sumOfQuadraticSum1(long n) {
        int[] cnt = new int[(int) n + 1];
        for (int num = 1; num <= n; num++) {
            for (int j = 1; j <= num; j++) {
                if (num % j == 0) {
                    cnt[j]++;
                }
            }
        }
        long ans = 0;
        for (long i = 1; i <= n; i++) {
            ans += i * i * (long) (cnt[(int) i]);
        }
        return ans;
    }

    // 正式方法
    // 时间复杂度O(开平方根N + 开平方根N * logN)
    public static long sumOfQuadraticSum2(long n) {
        // 前半段硬算
        // 100 -> 10
        // 200 -> 14
        long sqrt = (long) Math.pow((double) n, 0.5);
        long ans = 0;
        for (long i = 1; i <= sqrt; i++) {
            ans += i * i * (n / i);
        }
        // 后半段
        // 给你一个个数，二分出几个因子，处在k这个数上！
        // 由最大个数(根号N), 开始二分
        for (long k = n / (sqrt + 1); k >= 1; k--) {
            ans += sumOfLimitNumber(n, k);
        }
        return ans;
    }

    // 平方和公式n(n+1)(2n+1)/6
    public static long sumOfLimitNumber(long v, long n) {
        long r = cover(v, n);
        long l = cover(v, n + 1);
        return ((r * (r + 1) * ((r << 1) + 1)
                - l * (l + 1) * ((l << 1) + 1)) * n)
                / 6;
    }

    public static long cover(long v, long n) {
        long l = 1;
        long r = v;
        long m = 0;
        long ans = 0;
        while (l <= r) {
            m = (l + r) / 2;
            if (m * n <= v) {
                ans = m;
                l = m + 1;
            } else {
                r = m - 1;
            }
        }
        return ans;
    }

    // 来自optiver
    // 给定一个字符串str，和一个正数k
    // 你可以随意的划分str成多个子串，
    // 目的是找到在某一种划分方案中，有尽可能多的回文子串，长度>=k，并且没有重合
    // 返回有几个回文子串
    // 测试链接 : https://leetcode.cn/problems/maximum-number-of-non-overlapping-palindrome-substrings/
    public static int palindromeStringNoLessKLenNoOverlappingMaxParts(String s, int k) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        char[] str = manacherString(s);
        int[] p = new int[str.length];
        int ans = 0;
        int next = 0;
        // k == 5 回文串长度要 >= 5
        // next == 0
        // 0.... 8 第一块！
        // next -> 9
        // 9.....17 第二块！
        // next -> 18
        // 18....23 第三块
        // next一直到最后!
        while ((next = manacherFind(str, p, next, k)) != -1) {
            next = str[next] == '#' ? next : (next + 1);
            ans++;
        }
        return ans;
    }

    public static char[] manacherString(String s) {
        char[] str = s.toCharArray();
        char[] ans = new char[s.length() * 2 + 1];
        int index = 0;
        for (int i = 0; i != ans.length; i++) {
            ans[i] = (i & 1) == 0 ? '#' : str[index++];
        }
        return ans;
    }

    // s[l...]字符串只在这个范围上，且s[l]一定是'#'
    // 从下标l开始，之前都不算，一旦有某个中心回文半径>k，马上返回右边界
    public static int manacherFind(char[] s, int[] p, int l, int k) {
        int c = l - 1;
        int r = l - 1;
        int n = s.length;
        for (int i = l; i < s.length; i++) {
            p[i] = r > i ? Math.min(p[2 * c - i], r - i) : 1;
            while (i + p[i] < n && i - p[i] > l - 1 && s[i + p[i]] == s[i - p[i]]) {
                if (++p[i] > k) {
                    return i + k;
                }
            }
            if (i + p[i] > r) {
                r = i + p[i];
                c = i;
            }
        }
        return -1;
    }

    // 来自字节
    // 5.6笔试
    // 给定N件物品，每个物品有重量(w[i])、有价值(v[i])
    // 只能最多选两件商品，重量不超过bag，返回价值最大能是多少？
    // N <= 10^5, w[i] <= 10^5, v[i] <= 10^5, bag <= 10^5
    // 本题的关键点：什么数据范围都很大，唯独只需要最多选两件商品，这个可以利用一下
    public static int twoObjectMaxValue(int[] w, int[] v, int bag) {
        int n = w.length;
        int[][] arr = new int[n][2];
        for (int i = 0; i < n; i++) {
            arr[i][0] = w[i];
            arr[i][1] = v[i];
        }
        // O(N * logN)
        Arrays.sort(arr, (a, b) -> (a[0] - b[0]));
        // 重量从轻到重，依次标号1、2、3、4....
        // 价值依次被构建成了RMQ结构
        // O(N * logN)
        RMQ2 rmq = new RMQ2(arr);
        int ans = 0;
        // N * logN
        for (int i = 0, j = 1; i < n && arr[i][0] <= bag; i++, j++) {
            // 当前来到0号货物，RMQ结构1号
            // 当前来到i号货物，RMQ结构i+1号
            // 查询重量的边界，重量 边界 <= bag - 当前货物的重量
            // 货物数组中，找到 <= 边界，最右的位置i
            // RMQ，位置 i + 1
            int right = right(arr, bag - arr[i][0]) + 1;
            int rest = 0;
            // j == i + 1，当前的货物，在RMQ里的下标
            if (right == j) {
                rest = rmq.max(1, right - 1);
            } else if (right < j) {
                rest = rmq.max(1, right);
            } else { // right > j
                rest = Math.max(rmq.max(1, j - 1), rmq.max(j + 1, right));
            }
            ans = Math.max(ans, arr[i][1] + rest);
        }
        return ans;
    }

    public static int right(int[][] arr, int limit) {
        int l = 0;
        int r = arr.length - 1;
        int m = 0;
        int ans = -1;
        while (l <= r) {
            m = (l + r) / 2;
            if (arr[m][0] <= limit) {
                ans = m;
                l = m + 1;
            } else {
                r = m - 1;
            }
        }
        return ans;
    }

    public static class RMQ2 {
        public int[][] max;

        public RMQ2(int[][] arr) {
            int n = arr.length;
            int k = power2(n);
            max = new int[n + 1][k + 1];
            for (int i = 1; i <= n; i++) {
                max[i][0] = arr[i - 1][1];
            }
            for (int j = 1; (1 << j) <= n; j++) {
                for (int i = 1; i + (1 << j) - 1 <= n; i++) {
                    max[i][j] = Math.max(max[i][j - 1], max[i + (1 << (j - 1))][j - 1]);
                }
            }
        }

        public int max(int l, int r) {
            if (r < l) {
                return 0;
            }
            int k = power2(r - l + 1);
            return Math.max(max[l][k], max[r - (1 << k) + 1][k]);
        }

        private int power2(int m) {
            int ans = 0;
            while ((1 << ans) <= (m >> 1)) {
                ans++;
            }
            return ans;
        }
    }

    // 来自网易
    // 小红拿到了一个长度为N的数组arr，她准备只进行一次修改
    // 可以将数组中任意一个数arr[i]，修改为不大于P的正数（修改后的数必须和原数不同)
    // 并使得所有数之和为X的倍数
    // 小红想知道，一共有多少种不同的修改方案
    // 1 <= N, X <= 10^5
    // 1 <= arr[i], P <= 10^9
    public static int modifyOneNumberModXWays1(int[] arr, int p, int x) {
        long sum = 0;
        for (int num : arr) {
            sum += num;
        }
        int ans = 0;
        for (int num : arr) {
            sum -= num;
            for (int v = 1; v <= p; v++) {
                if (v != num) {
                    if ((sum + v) % x == 0) {
                        ans++;
                    }
                }
            }
            sum += num;
        }
        return ans;
    }

    public static int modifyOneNumberModXWays2(int[] arr, int p, int x) {
        long sum = 0;
        for (int num : arr) {
            sum += num;
        }
        int ans = 0;
        for (int num : arr) {
            ans += cnt(p, x, num, (x - (int) ((sum - num) % x)) % x);
        }
        return ans;
    }

    // 当前数字num
    // 1~p以内，不能是num的情况下，% x == mod的数字有几个
    // O(1)
    public static int cnt(int p, int x, int num, int mod) {
        // p/x 至少有几个
        // (p % x) >= mod ? 1 : 0
        // 在不考虑变出来的数，是不是num的情况下，算一下有几个数，符合要求
        int ans = (p / x) + ((p % x) >= mod ? 1 : 0) - (mod == 0 ? 1 : 0);
        // 不能等于num！
        return ans - ((num <= p && num % x == mod) ? 1 : 0);
    }

    // 来自字节
    // 一共有n个人，从左到右排列，依次编号0~n-1
    // h[i]是第i个人的身高
    // v[i]是第i个人的分数
    // 要求从左到右选出一个子序列，在这个子序列中的人，从左到右身高是不下降的
    // 返回所有符合要求的子序列中，分数最大累加和是多大
    // n <= 10的5次方, 1 <= h[i] <= 10的9次方, 1 <= v[i] <= 10的9次方
    public static int sortedSubsequenceMaxSum(int[] h, int[] v) {
        int n = h.length;
        int[] rank = new int[n];
        for (int i = 0; i < n; i++) {
            rank[i] = h[i];
        }
        Arrays.sort(rank);
        SegmentTree st = new SegmentTree(n);
        for (int i = 0; i < n; i++) {
            int height = rank2(rank, h[i]);
            // 1~height max
            st.update(height, st.max(height) + v[i]);
        }
        return st.max(n);
    }

    // [150, 152, 160, 175]  160
    //   1    2    3    4
    // 3
    public static int rank2(int[] rank, int num) {
        int l = 0;
        int r = rank.length - 1;
        int m = 0;
        int ans = 0;
        while (l <= r) {
            m = (l + r) / 2;
            if (rank[m] >= num) {
                ans = m;
                r = m - 1;
            } else {
                l = m + 1;
            }
        }
        return ans + 1;
    }

    // 来自网易
    // 给出一个有n个点，m条有向边的图
    // 你可以施展魔法，把有向边，变成无向边
    // 比如A到B的有向边，权重为7。施展魔法之后，A和B通过该边到达彼此的代价都是7。
    // 求，允许施展一次魔法的情况下，1到n的最短路，如果不能到达，输出-1。
    // n为点数, 每条边用(a,b,v)表示，含义是a到b的这条边，权值为v
    // 点的数量 <= 10^5，边的数量 <= 2 * 10^5，1 <= 边的权值 <= 10^6
    // 最优解
    // 时间复杂度O(N * logN)
    // N <= 2 * 10^5
    public static int oneEdgeMagicMinPathSum(int n, int[][] roads) {
        ArrayList<ArrayList<int[]>> graph = new ArrayList<>();
        for (int i = 0; i <= n; i++) {
            graph.add(new ArrayList<>());
        }
        for (int[] r : roads) {
            graph.get(r[0]).add(new int[]{0, r[1], r[2]});
            graph.get(r[1]).add(new int[]{1, r[0], r[2]});
        }
        PriorityQueue<int[]> heap = new PriorityQueue<>((a, b) -> a[2] - b[2]);
        boolean[][] visited = new boolean[2][n + 1];
        // a -> 0,a   1,a
        // boolean[] visited = new boolean[n+1]
        // visited[i] == true 去过了！从队列里弹出来过了！以后别碰了！
        // visited[i] == false 没去过！第一次从队列里弹出来！当前要处理！
        // 0,1,0 -> 之前没有走过魔法路，当前来到1号出发点，代价是0
        heap.add(new int[]{0, 1, 0});
        int ans = Integer.MAX_VALUE;
        while (!heap.isEmpty()) {
            int[] cur = heap.poll();
            if (visited[cur[0]][cur[1]]) {
                continue;
            }
            visited[cur[0]][cur[1]] = true;
            if (cur[1] == n) {
                ans = Math.min(ans, cur[2]);
                if (visited[0][n] && visited[1][n]) {
                    break;
                }
            }
            for (int[] edge : graph.get(cur[1])) {
                // 当前来到cur
                // 之前有没有走过魔法路径：cur[0] == 0 ，没走过！cur[0] = 1, 走过了
                // 当前来到的点是啥，cur[1]，点编号！
                // 之前的总代价是啥？cur[2]
                // cur，往下，能走的，所有的路在哪？
                // 当前的路，叫edge
                // 当前的路，是不是魔法路！edge[0] = 0 , 不是魔法路
                // edge[0] == 1，是魔法路
                // cur[0] + edge[0] == 0
                // 路 ：0 5 20
                // 当前路，不是魔法路，去往的点是5号点，该路权重是20
                // 路 ：1 7 13
                // 当前路，是魔法路，去往的点是7号点，该路权重是13
                if (cur[0] + edge[0] == 0) {
                    if (!visited[0][edge[1]]) {
                        heap.add(new int[]{0, edge[1], cur[2] + edge[2]});
                    }
                }
                // cur[0] + edge[0] == 1
                // 0         1
                // 1         0
                if (cur[0] + edge[0] == 1) {
                    if (!visited[1][edge[1]]) {
                        heap.add(new int[]{1, edge[1], cur[2] + edge[2]});
                    }
                }
                // 1 1 == 2
            }
        }
        return ans == Integer.MAX_VALUE ? -1 : ans;
    }

    // 来自网易
    // 小红拿到了一个大立方体，该大立方体由1*1*1的小方块拼成，初始每个小方块都是白色。
    // 小红可以每次选择一个小方块染成红色
    // 每次小红可能选择同一个小方块重复染色
    // 每次染色以后，你需要帮小红回答出当前的白色连通块数
    // 如果两个小方块共用同一个面，且颜色相同，则它们是连通的
    // 给定n、m、h，表示大立方体的长、宽、高
    // 给定k次操作，每一次操作用(a, b, c)表示在大立方体的该位置进行染色
    // 返回长度为k的数组，表示每一次操作后，白色方块的连通块数
    // n * m * h <= 10 ^ 5，k <= 10 ^ 5
    // 最优解
    // O(k + n * m * h)
    public static int[] redAndWhiteSquares(int n, int m, int h, int[][] ops) {
        int k = ops.length;
        int[][][] red = new int[n][m][h];
        for (int[] op : ops) {
            red[op[0]][op[1]][op[2]]++;
        }
        UnionFind uf = new UnionFind(n, m, h, red);
        int[] ans = new int[k];
        for (int i = k - 1; i >= 0; i--) {
            ans[i] = uf.sets;
            int x = ops[i][0];
            int y = ops[i][1];
            int z = ops[i][2];
            if (--red[x][y][z] == 0) {
                // x, y ,z 这个格子，变白，建立自己的小集合
                // 然后6个方向，集合该合并合并
                uf.finger(x, y, z);
            }
        }
        return ans;
    }

    public static class UnionFind {
        public int n;
        public int m;
        public int h;
        public int[] father;
        public int[] size;
        public int[] help;
        public int sets;

        public UnionFind(int a, int b, int c, int[][][] red) {
            n = a;
            m = b;
            h = c;
            int len = n * m * h;
            father = new int[len];
            size = new int[len];
            help = new int[len];
            for (int x = 0; x < n; x++) {
                for (int y = 0; y < m; y++) {
                    for (int z = 0; z < h; z++) {
                        if (red[x][y][z] == 0) {
                            finger(x, y, z);
                        }
                    }
                }
            }
        }

        public void finger(int x, int y, int z) {
            // x，y，z
            // 一维数值
            int i = index(x, y, z);
            father[i] = i;
            size[i] = 1;
            sets++;
            union(i, x - 1, y, z);
            union(i, x + 1, y, z);
            union(i, x, y - 1, z);
            union(i, x, y + 1, z);
            union(i, x, y, z - 1);
            union(i, x, y, z + 1);
        }

        private int index(int x, int y, int z) {
            return z * n * m + y * n + x;
        }

        private void union(int i, int x, int y, int z) {
            if (x < 0 || x == n || y < 0 || y == m || z < 0 || z == h) {
                return;
            }
            int j = index(x, y, z);
            if (size[j] == 0) {
                return;
            }
            i = find(i);
            j = find(j);
            if (i != j) {
                if (size[i] >= size[j]) {
                    father[j] = i;
                    size[i] += size[j];
                } else {
                    father[i] = j;
                    size[j] += size[i];
                }
                sets--;
            }
        }

        private int find(int i) {
            int s = 0;
            while (i != father[i]) {
                help[s++] = i;
                i = father[i];
            }
            while (s > 0) {
                father[help[--s]] = i;
            }
            return i;
        }
    }

    // 来自字节
    // 输入:
    // 去重数组arr，里面的数只包含0~9,给定一个数字limit
    // 返回要求比limit小的情况下，能够用arr拼出来的最大数字(数组中的值可重复使用)
    public static int maxNumberUnderLimit(int[] arr, int limit) {
        // arr里面是不重复的数字，且只包含0~9
        Arrays.sort(arr);
        limit--;
        // <= limit 且最大的数字
        // 68886
        // 10000
        // 为了取数而设计的！
        // 457
        // 100
        int offset = 1;
        while (offset <= limit / 10) {
            offset *= 10;
        }
        int ans = process2(arr, limit, offset);
        if (ans != -1) {
            return ans;
        } else {
            offset /= 10;
            int rest = 0;
            while (offset > 0) {
                rest += arr[arr.length - 1] * offset;
                offset /= 10;
            }
            return rest;
        }
    }

    // 可以选哪些数字，都在arr里，arr是有序的，[3,6,8,9]
    // limit : <= limit 且尽量的大！  68886
    // offset :                     10000
    //                               1000
    //                                100
    // offset 下标用！
    public static int process2(int[] arr, int limit, int offset) {
        // 之前的数字和limit完全一样，且limit所有数字都一样
        if (offset == 0) {
            return limit;
        }
        // 当前的数字
        // 68886
        // 10000
        // 6
        int cur = (limit / offset) % 10;
        // 6 arr中 <=6 最近的！
        int near = near(arr, cur);
        if (near == -1) {
            return -1;
        } else if (arr[near] == cur) { // 找出来的数字，真的和当前数字cur一样!
            int ans = process2(arr, limit, offset / 10);
            if (ans != -1) {
                return ans;
            } else if (near > 0) { // 虽然后续没成功，但是我自己还能下降！还能选更小的数字
                near--;
                return (limit / (offset * 10)) * offset * 10 + (arr[near] * offset) + rest(arr, offset / 10);
            } else { // 后续没成功，我自己也不能再下降了！宣告失败，往上返回！
                return -1;
            }
        } else { // arr[near] < cur
            return (limit / (offset * 10)) * offset * 10 + (arr[near] * offset) + rest(arr, offset / 10);
        }
    }

    // 比如offset = 100
    // 一共3位数
    // 那么就把arr中最大的数字x，拼成xxx，返回
    // 比如offset = 10000
    // 一共5位数
    // 那么就把arr中最大的数字x，拼成xxxxx，返回
    public static int rest(int[] arr, int offset) {
        int rest = 0;
        while (offset > 0) {
            rest += arr[arr.length - 1] * offset;
            offset /= 10;
        }
        return rest;
    }

    // 在有序数组arr中，找到<=num，且最大的数字，在arr中的位置返回
    // 如果所有数字都大于num，返回-1
    // [3,6,9] num = 4  3
    // [5,7,9] num = 4  -1
    public static int near(int[] arr, int num) {
        int l = 0;
        int r = arr.length - 1;
        int m = 0;
        int near = -1;
        while (l <= r) {
            m = (l + r) / 2;
            if (arr[m] <= num) {
                near = m;
                l = m + 1;
            } else {
                r = m - 1;
            }
        }
        return near;
    }

    // 来自京东
    // 4.2笔试
    // 给定一个数组arr，长度为N，arr中所有的值都在1~K范围上
    // 你可以删除数字，目的是让arr的最长递增子序列长度小于K
    // 返回至少删除几个数字能达到目的
    // N <= 10^4，K <= 10^2
    // 正式方法
    // 时间复杂度O(N*K)
    public static int removeNumbersNotIncreasingAll(int[] arr, int k) {
        int n = arr.length;
        int[][] dp = new int[n][k];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < k; j++) {
                dp[i][j] = -1;
            }
        }
        return process2(arr, k, 0, 0, dp);
    }

    public static int process2(int[] arr, int k, int index, int range, int[][] dp) {
        if (range == k) {
            return Integer.MAX_VALUE;
        }
        if (index == arr.length) {
            return 0;
        }
        if (dp[index][range] != -1) {
            return dp[index][range];
        }
        int ans = 0;
        if (arr[index] == range + 1) {
            int p1 = process2(arr, k, index + 1, range, dp);
            p1 += p1 != Integer.MAX_VALUE ? 1 : 0;
            int p2 = process2(arr, k, index + 1, range + 1, dp);
            ans = Math.min(p1, p2);
        } else {
            ans = process2(arr, k, index + 1, range, dp);
        }
        dp[index][range] = ans;
        return ans;
    }

    // 来自学员问题
    // 给定一个数组arr，表示从早到晚，依次会出现的导弹的高度
    // 大炮打导弹的时候，如果一旦大炮定了某个高度去打，那么这个大炮每次打的高度都必须下降一点
    // 1) 如果只有一个大炮，返回最多能拦截多少导弹
    // 2) 如果所有的导弹都必须拦截，返回最少的大炮数量
    public static int numOfCannon(int[] arr) {
        // key : 某个大炮打的结尾数值
        // value : 有多少个大炮有同样的结尾数值
        // 比如：
        // 一共有A、B、C三个大炮
        // 如果A大炮此时打的高度是17，B大炮此时打的高度是7，C大炮此时打的高度是13
        // 那么在表中：
        // 7, 1
        // 13, 1
        // 17, 1
        // 如果A大炮此时打的高度是13，B大炮此时打的高度是7，C大炮此时打的高度是13
        // 那么在表中：
        // 7, 1
        // 13, 2
        TreeMap<Integer, Integer> ends = new TreeMap<>();
        for (int num : arr) {
            if (ends.ceilingKey(num + 1) == null) {
                ends.put(Integer.MAX_VALUE, 1);
            }
            int ceilKey = ends.ceilingKey(num + 1);
            if (ends.get(ceilKey) > 1) {
                ends.put(ceilKey, ends.get(ceilKey) - 1);
            } else {
                ends.remove(ceilKey);
            }
            ends.put(num, ends.getOrDefault(num, 0) + 1);
        }
        int ans = 0;
        for (int value : ends.values()) {
            ans += value;
        }
        return ans;
    }

    // 为了给刷题的同学一些奖励，力扣团队引入了一个弹簧游戏机
    // 游戏机由 N 个特殊弹簧排成一排，编号为 0 到 N-1
    // 初始有一个小球在编号 0 的弹簧处。若小球在编号为 i 的弹簧处
    // 通过按动弹簧，可以选择把小球向右弹射jump[i] 的距离，或者向左弹射到任意左侧弹簧的位置
    // 也就是说，在编号为 i 弹簧处按动弹簧，
    // 小球可以弹向 0 到 i-1 中任意弹簧或者 i+jump[i] 的弹簧（若 i+jump[i]>=N ，则表示小球弹出了机器）
    // 小球位于编号 0 处的弹簧时不能再向左弹。
    // 为了获得奖励，你需要将小球弹出机器。
    // 请求出最少需要按动多少次弹簧，可以将小球从编号 0 弹簧弹出整个机器，即向右越过编号 N-1 的弹簧。
    // 测试链接 : https://leetcode-cn.com/problems/zui-xiao-tiao-yue-ci-shu/
    // 宽度优先遍历
    // N*logN
    public int minJumpUsePre1(int[] jump) {
        int n = jump.length;
        int[] queue = new int[n];
        int l = 0;
        int r = 0;
        queue[r++] = 0;
        IndexTree it = new IndexTree(n);
        // 1...n初始化的时候 每个位置填上1
        for (int i = 1; i < n; i++) {
            it.add(i, 1);
        }
        int step = 0;
        while (l != r) { // 队列里面还有东西
            // tmp记录了当前层的终止位置！
            int tmp = r;
            // 当前层的所有节点，都去遍历!
            for (; l < tmp; l++) {
                int cur = queue[l];
                int forward = cur + jump[cur];
                if (forward >= n) {
                    return step + 1;
                }
                if (it.value(forward) != 0) {
                    queue[r++] = forward;
                    it.add(forward, -1);
                }
                // cur
                // 1....cur-1 cur
                while (it.sum(cur - 1) != 0) {
                    int find = find(it, cur - 1);
                    it.add(find, -1);
                    queue[r++] = find;
                }
            }
            step++;
        }
        return -1;
    }

    public static int find(IndexTree it, int right) {
        int left = 0;
        int mid = 0;
        int find = 0;
        while (left <= right) {
            mid = (left + right) / 2;
            if (it.sum(mid) > 0) {
                find = mid;
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }
        return find;
    }

    // 下标从1开始！
    public static class IndexTree {

        private int[] tree;
        private int N;

        // 0位置弃而不用！
        public IndexTree(int size) {
            N = size;
            tree = new int[N + 1];
        }

        public int value(int index) {
            if (index == 0) {
                return sum(0);
            } else {
                return sum(index) - sum(index - 1);
            }
        }

        // index & -index : 提取出index最右侧的1出来
        // index :           0011001000
        // index & -index :  0000001000
        public int sum(int i) {
            int index = i + 1;
            int ret = 0;
            while (index > 0) {
                ret += tree[index];
                index -= index & -index;
            }
            return ret;
        }

        public void add(int i, int d) {
            int index = i + 1;
            while (index <= N) {
                tree[index] += d;
                index += index & -index;
            }
        }
    }

    public static class IndexTree2D {
        private int[][] tree;
        private int[][] nums;
        private int N;
        private int M;

        public IndexTree2D(int[][] matrix) {
            if (matrix.length == 0 || matrix[0].length == 0) {
                return;
            }
            N = matrix.length;
            M = matrix[0].length;
            tree = new int[N + 1][M + 1];
            nums = new int[N][M];
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < M; j++) {
                    update(i, j, matrix[i][j]);
                }
            }
        }

        private int sum(int row, int col) {
            int sum = 0;
            for (int i = row + 1; i > 0; i -= i & (-i)) {
                for (int j = col + 1; j > 0; j -= j & (-j)) {
                    sum += tree[i][j];
                }
            }
            return sum;
        }

        public void update(int row, int col, int val) {
            if (N == 0 || M == 0) {
                return;
            }
            int add = val - nums[row][col];
            nums[row][col] = val;
            for (int i = row + 1; i <= N; i += i & (-i)) {
                for (int j = col + 1; j <= M; j += j & (-j)) {
                    tree[i][j] += add;
                }
            }
        }

        public int sumRegion(int row1, int col1, int row2, int col2) {
            if (N == 0 || M == 0) {
                return 0;
            }
            return sum(row2, col2) + sum(row1 - 1, col1 - 1) - sum(row1 - 1, col2) - sum(row2, col1 - 1);
        }
    }

    // 来自携程
    // 给出n个数字，你可以任选其中一些数字相乘，相乘之后得到的新数字x
    // x的价值是x的不同质因子的数量
    // 返回所有选择数字的方案中，得到的x的价值之和
    // 工具！
    // 返回num质数因子列表(去重)
    // 时间复杂度，根号(num)
    public static ArrayList<Long> primes(long num) {
        ArrayList<Long> ans = new ArrayList<>();
        for (long i = 2; i * i <= num && num > 1; i++) {
            if (num % i == 0) {
                ans.add(i);
                while (num % i == 0) {
                    num /= i;
                }
            }
        }
        if (num != 1) {
            ans.add(num);
        }
        return ans;
    }

    public static long sumOfValuesAboutPrimes(int[] arr) {
        // key : 某个质数因子
        // value : 有多少个数含有这个因子
        HashMap<Long, Long> cntMap = new HashMap<>();
        for (int num : arr) {
            for (long factor : primes(num)) {
                cntMap.put(factor, cntMap.getOrDefault(factor, 0L) + 1L);
            }
        }
        int n = arr.length;
        long ans = 0;
        // count ：含有这个因子的数，有多少个
        // others : 不含有这个因子的数，有多少个
        for (long count : cntMap.values()) {
            long others = n - count;
            ans += (power(2, count) - 1) * power(2, others);
        }
        return ans;
    }

    public static long power(long num, long n) {
        if (n == 0L) {
            return 1L;
        }
        long ans = 1L;
        while (n > 0) {
            if ((n & 1) != 0) {
                ans *= num;
            }
            num *= num;
            n >>= 1;
        }
        return ans;
    }

    // 来自网易
    // 3.27笔试
    // 一个二维矩阵，上面只有 0 和 1，只能上下左右移动
    // 如果移动前后的元素值相同，则耗费 1 ，否则耗费 2。
    // 问从左上到右下的最小耗费
    // 正确的解法
    // Dijkstra
    public static int minDistanceFromLeftUpToRightDown(int[][] map) {
        int n = map.length;
        int m = map[0].length;
        // 小根堆：[代价，行，列]
        // 根据代价，谁代价小，谁放在堆的上面
        PriorityQueue<int[]> heap = new PriorityQueue<>((a, b) -> a[0] - b[0]);
        // popped[i][j] == true 已经弹出过了！不要再处理，直接忽略！
        // popped[i][j] == false 之间(i,j)没弹出过！要处理
        boolean[][] popped = new boolean[n][m];
        heap.add(new int[]{0, 0, 0});
        int ans = 0;
        while (!heap.isEmpty()) {
            // 当前弹出了，[代价，行，列]，当前位置
            int[] cur = heap.poll();
            int dis = cur[0];
            int row = cur[1];
            int col = cur[2];
            if (popped[row][col]) {
                continue;
            }
            // 第一次弹出！
            popped[row][col] = true;
            if (row == n - 1 && col == m - 1) {
                ans = dis;
                break;
            }
            add(dis, row - 1, col, map[row][col], n, m, map, popped, heap);
            add(dis, row + 1, col, map[row][col], n, m, map, popped, heap);
            add(dis, row, col - 1, map[row][col], n, m, map, popped, heap);
            add(dis, row, col + 1, map[row][col], n, m, map, popped, heap);
        }
        return ans;
    }

    // preDistance ： 之前的距离
    // int row, int col ： 当前要加入的是什么位置
    // preValue : 前一个格子是什么值，
    // int n, int m ：边界，固定参数
    // map: 每一个格子的值，都在map里
    // boolean[][] popped : 当前位置如果是弹出过的位置，要忽略！
    // PriorityQueue<int[]> heap : 小根堆
    public static void add(int preDistance,
                           int row, int col, int preValue, int n, int m,
                           int[][] map, boolean[][] popped,
                           PriorityQueue<int[]> heap) {
        if (row >= 0 && row < n && col >= 0 && col < m
                && !popped[row][col]) {
            heap.add(new int[]{
                    preDistance + (map[row][col] == preValue ? 1 : 2),
                    row, col});
        }
    }

    // 来自美团
    // 3.26笔试
    // 给定一个非负数组，任意选择数字，使累加和最大且为7的倍数，返回最大累加和
    // n比较大，10的5次方
    public static int maxSumDivided7EqualsZero1(int[] arr) {
        return process1(arr, 0, 0);
    }

    public static int process1(int[] arr, int index, int pre) {
        if (index == arr.length) {
            return pre % 7 == 0 ? pre : 0;
        }
        int p1 = process1(arr, index + 1, pre);
        int p2 = process1(arr, index + 1, pre + arr[index]);
        return Math.max(p1, p2);
    }

    public static int maxSumDivided7EqualsZero2(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        int n = arr.length;
        int[][] dp = new int[n][7];
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < 7; j++) {
                dp[i][j] = -1;
            }
        }
        dp[0][arr[0] % 7] = arr[0];
        for (int i = 1; i < n; i++) {
            // 当前arr[i] % 7 的余数
            int curMod = arr[i] % 7;
            for (int j = 0; j < 7; j++) {
                dp[i][j] = dp[i - 1][j];
                int findMod = (7 - curMod + j) % 7;
                if (dp[i - 1][findMod] != -1) {
                    dp[i][j] = Math.max(dp[i][j], dp[i - 1][findMod] + arr[i]);
                }
            }
        }
        return dp[n - 1][0] == -1 ? 0 : dp[n - 1][0];
    }

    // 来自美团
    // 3.26笔试
    // 给定一个正数n, 表示有0~n-1号任务
    // 给定一个长度为n的数组time，time[i]表示i号任务做完的时间
    // 给定一个二维数组matrix
    // matrix[j] = {a, b} 代表：a任务想要开始，依赖b任务的完成
    // 只要能并行的任务都可以并行，但是任何任务只有依赖的任务完成，才能开始
    // 返回一个长度为n的数组ans，表示每个任务完成的时间
    // 输入可以保证没有循环依赖
    public static int[] allJobFinishTime(int n, int[] time, int[][] matrix) {
        ArrayList<ArrayList<Integer>> nexts = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            nexts.add(new ArrayList<>());
        }
        int[] in = new int[n];
        for (int[] line : matrix) {
            nexts.get(line[1]).add(line[0]);
            in[line[0]]++;
        }
        Queue<Integer> zeroInQueue = new LinkedList<>();
        int[] ans = new int[n];
        for (int i = 0; i < n; i++) {
            if (in[i] == 0) {
                zeroInQueue.add(i);
            }
        }
        while (!zeroInQueue.isEmpty()) {
            int cur = zeroInQueue.poll();
            ans[cur] += time[cur];
            for (int next : nexts.get(cur)) {
                ans[next] = Math.max(ans[next], ans[cur]);
                if (--in[next] == 0) {
                    zeroInQueue.add(next);
                }
            }
        }
        return ans;
    }

    // 来自百度
    // 给出一个长度为n的01串，现在请你找到两个区间，
    // 使得这两个区间中，1的个数相等，0的个数也相等
    // 这两个区间可以相交，但是不可以完全重叠，即两个区间的左右端点不可以完全一样
    // 现在请你找到两个最长的区间，满足以上要求。
    public static int towLongestSubarraySame01Number1(int[] arr) {
        HashMap<Integer, HashMap<Integer, Integer>> map = new HashMap<>();
        for (int i = 0; i < arr.length; i++) {
            int zero = 0;
            int one = 0;
            for (int j = i; j < arr.length; j++) {
                zero += arr[j] == 0 ? 1 : 0;
                one += arr[j] == 1 ? 1 : 0;
                map.putIfAbsent(zero, new HashMap<>());
                map.get(zero).put(one, map.get(zero).getOrDefault(one, 0) + 1);
            }
        }
        int ans = 0;
        for (int zeros : map.keySet()) {
            for (int ones : map.get(zeros).keySet()) {
                int num = map.get(zeros).get(ones);
                if (num > 1) {
                    ans = Math.max(ans, zeros + ones);
                }
            }
        }
        return ans;
    }

    public static int towLongestSubarraySame01Number2(int[] arr) {
        int leftZero = -1;
        int rightZero = -1;
        int leftOne = -1;
        int rightOne = -1;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 0) {
                leftZero = i;
                break;
            }
        }
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 1) {
                leftOne = i;
                break;
            }
        }
        for (int i = arr.length - 1; i >= 0; i--) {
            if (arr[i] == 0) {
                rightZero = i;
                break;
            }
        }
        for (int i = arr.length - 1; i >= 0; i--) {
            if (arr[i] == 1) {
                rightOne = i;
                break;
            }
        }
        int p1 = rightZero - leftZero;
        int p2 = rightOne - leftOne;
        return Math.max(p1, p2);
    }

    // 来自阿里
    // x = { a, b, c, d }
    // y = { e, f, g, h }
    // x、y两个小数组长度都是4
    // 如果有: a + e = b + f = c + g = d + h
    // 那么说x和y是一个完美对
    // 题目给定N个小数组，每个小数组长度都是K
    // 返回这N个小数组中，有多少完美对
    // 本题测试链接 : https://www.nowcoder.com/practice/f5a3b5ab02ed4202a8b54dfb76ad035e
    public static long perfectPairs(int[][] matrix) {
        long ans = 0;
        // key : 字符串 特征，差值特征 : "_5_-2_6_9"
        HashMap<String, Integer> counts = new HashMap<>();
        for (int[] arr : matrix) {
            StringBuilder self = new StringBuilder();
            StringBuilder minus = new StringBuilder();
            for (int i = 1; i < arr.length; i++) {
                self.append("_" + (arr[i] - arr[i - 1]));
                minus.append("_" + (arr[i - 1] - arr[i]));
            }
            ans += counts.getOrDefault(minus.toString(), 0);
            counts.put(self.toString(), counts.getOrDefault(self.toString(), 0) + 1);
        }
        return ans;
    }

    // 来自快手
    // 某公司年会上，大家要玩一食发奖金游戏，一共有n个员工，
    // 每个员工都有建设积分和捣乱积分
    // 他们需要排成一队，在队伍最前面的一定是老板，老板也有建设积分和捣乱积分
    // 排好队后，所有员工都会获得各自的奖金，
    // 该员工奖金 = 排在他前面所有人的建设积分乘积 / 该员工自己的捣乱积分，向下取整
    // 为了公平(放屁)，老板希望 : 让获得奖金最高的员工，所获得的奖金尽可能少
    // 所以想请你帮他重新排一下队伍，返回奖金最高的员工获得的、尽可能少的奖金数额
    // 快手考试的时候，给定的数据量，全排列的代码也能过的！
    // 1 <= n <= 1000, 1<= 积分 <= 10000;
    // 正式方法
    // 所有员工数量为N
    // 假设所有员工建设积分乘起来为M
    // 时间复杂度O(N * logN * logM)
    public static long maxMoneyMostMin(int a, int b, int[] value, int[] trouble) {
        int n = value.length;
        long l = 0;
        long r = 0;
        long valueAll = a;
        long[][] staff = new long[n][2];
        for (int i = 0; i < n; i++) {
            r = Math.max(r, valueAll / trouble[i]);
            valueAll *= value[i];
            staff[i][0] = (long) value[i] * (long) trouble[i];
            staff[i][1] = value[i];
        }
        Arrays.sort(staff, (x, y) -> (y[0] >= x[0] ? 1 : -1));
        long m = 0;
        long ans = 0;
        while (l <= r) {
            m = l + ((r - l) >> 1);
            if (yeah(valueAll, staff, m)) {
                ans = m;
                r = m - 1;
            } else {
                l = m + 1;
            }
        }
        return ans;
    }

    // staff长度为N，时间复杂度O(N * logN)
    public static boolean yeah(long all, long[][] staff, long limit) {
        int n = staff.length;
        SegmentTree2 st = new SegmentTree2(n);
        HashMap<Integer, LinkedList<Integer>> map = new HashMap<>();
        for (int i = 0, index = 1; i < n; i++, index++) {
            int value = (int) staff[i][1];
            st.update(index, value);
            if (!map.containsKey(value)) {
                map.put(value, new LinkedList<>());
            }
            map.get(value).addLast(index);
        }
        for (int k = 0; k < n; k++) {
            int right = boundary(staff, all, limit);
            if (right == 0) {
                return false;
            }
            int max = st.max(right);
            if (max == 0) {
                return false;
            }
            int index = map.get(max).pollFirst();
            st.update(index, 0);
            all /= max;
        }
        return true;
    }

    public static int boundary(long[][] staff, long all, long limit) {
        int l = 0;
        int r = staff.length - 1;
        int m = 0;
        int ans = -1;
        while (l <= r) {
            m = l + ((r - l) >> 1);
            if (all / staff[m][0] <= limit) {
                ans = m;
                l = m + 1;
            } else {
                r = m - 1;
            }
        }
        return ans + 1;
    }

    public static class SegmentTree2 {
        private int n;
        private int[] max;
        private int[] update;

        public SegmentTree2(int maxSize) {
            n = maxSize + 1;
            max = new int[n << 2];
            update = new int[n << 2];
            Arrays.fill(update, -1);
        }

        public void update(int index, int c) {
            update(index, index, c, 1, n, 1);
        }

        public int max(int right) {
            return max(1, right, 1, n, 1);
        }

        private void pushUp(int rt) {
            max[rt] = Math.max(max[rt << 1], max[rt << 1 | 1]);
        }

        private void pushDown(int rt, int ln, int rn) {
            if (update[rt] != -1) {
                update[rt << 1] = update[rt];
                max[rt << 1] = update[rt];
                update[rt << 1 | 1] = update[rt];
                max[rt << 1 | 1] = update[rt];
                update[rt] = -1;
            }
        }

        private void update(int L, int R, int C, int l, int r, int rt) {
            if (L <= l && r <= R) {
                max[rt] = C;
                update[rt] = C;
                return;
            }
            int mid = (l + r) >> 1;
            pushDown(rt, mid - l + 1, r - mid);
            if (L <= mid) {
                update(L, R, C, l, mid, rt << 1);
            }
            if (R > mid) {
                update(L, R, C, mid + 1, r, rt << 1 | 1);
            }
            pushUp(rt);
        }

        private int max(int L, int R, int l, int r, int rt) {
            if (L <= l && r <= R) {
                return max[rt];
            }
            int mid = (l + r) >> 1;
            pushDown(rt, mid - l + 1, r - mid);
            int ans = 0;
            if (L <= mid) {
                ans = Math.max(ans, max(L, R, l, mid, rt << 1));
            }
            if (R > mid) {
                ans = Math.max(ans, max(L, R, mid + 1, r, rt << 1 | 1));
            }
            return ans;
        }
    }

    // 测试链接 : https://leetcode.com/problems/cheapest-flights-within-k-stops/
    // 类似宽度优先遍历
    // n，城市数量，0...n-1
    // flights = { {0,7,100},  {15,3,300} .... }
    // src -> 源点
    // dst -> 目标点
    // k -> 最多能中转几次
    public static int findCheapestPrice1(int n, int[][] flights, int src, int dst, int k) {

        // 图
        // 0 :  {1, 500}   {7, 20}    {13, 70}
        ArrayList<ArrayList<int[]>> graph = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            graph.add(new ArrayList<int[]>());
        }
        for (int[] line : flights) {
            // [0, 7 , 200]
            // 0 : {    {7, 200}   }
            graph.get(line[0]).add(new int[]{line[1], line[2]});
        }
        // cost[i] :  从源点src出发的情况下，到达i号点，最少费用是多少
        int[] cost = new int[n];
        Arrays.fill(cost, Integer.MAX_VALUE);
        cost[src] = 0;
        HashMap<Integer, Integer> curMap = new HashMap<>();
        curMap.put(src, 0);
        for (int i = 0; i <= k; i++) {
            HashMap<Integer, Integer> nextMap = new HashMap<>();
            for (Map.Entry<Integer, Integer> entry : curMap.entrySet()) {
                // A -> from  100
                // from -> ????
                int from = entry.getKey();
                int preCost = entry.getValue();
                for (int[] line : graph.get(from)) {
                    int to = line[0];
                    int curCost = line[1];
                    cost[to] = Math.min(cost[to], preCost + curCost);
                    nextMap.put(to,
                            Math.min(nextMap.getOrDefault(to, Integer.MAX_VALUE),
                                    preCost + curCost));
                }
            }
            curMap = nextMap;
        }
        return cost[dst] == Integer.MAX_VALUE ? -1 : cost[dst];
    }

    // Bellman Ford
    // 适用范围：可以解决解决负权图且可以判断有无负权环
    // 快！
    public static int findCheapestPrice2(int n, int[][] flights, int src, int dst, int k) {
        int[] cost = new int[n];
        Arrays.fill(cost, Integer.MAX_VALUE);
        cost[src] = 0;
        for (int i = 0; i <= k; i++) {
            int[] next = Arrays.copyOf(cost, n);
            for (int[] flight : flights) {
                if (cost[flight[0]] != Integer.MAX_VALUE) {
                    next[flight[1]] = Math.min(next[flight[1]], cost[flight[0]] + flight[2]);
                }
            }
            cost = next;
        }
        return cost[dst] == Integer.MAX_VALUE ? -1 : cost[dst];
    }

    // 测试链接 : https://leetcode.com/problems/minimum-number-of-days-to-eat-n-oranges/
    // 所有的答案都填在这个表里
    // 这个表对所有的过程共用
    public static HashMap<Integer, Integer> dp = new HashMap<>();

    public static int minDays(int n) {
        if (n <= 1) {
            return n;
        }
        if (dp.containsKey(n)) {
            return dp.get(n);
        }
        // 1) 吃掉一个橘子
        // 2) 如果n能被2整除，吃掉一半的橘子，剩下一半
        // 3) 如果n能被3正数，吃掉三分之二的橘子，剩下三分之一
        // 因为方法2）和3），是按比例吃橘子，所以必然会非常快
        // 所以，决策如下：
        // 可能性1：为了使用2）方法，先把橘子吃成2的整数倍，然后直接干掉一半，剩下的n/2调用递归
        // 即，n % 2 + 1 + minDays(n/2)
        // 可能性2：为了使用3）方法，先把橘子吃成3的整数倍，然后直接干掉三分之二，剩下的n/3调用递归
        // 即，n % 3 + 1 + minDays(n/3)
        // 至于方法1)，完全是为了这两种可能性服务的，因为能按比例吃，肯定比一个一个吃快(显而易见的贪心)
        int ans = Math.min(n % 2 + 1 + minDays(n / 2), n % 3 + 1 + minDays(n / 3));
        dp.put(n, ans);
        return ans;
    }

    // 测试链接 : https://leetcode.com/problems/robot-bounded-in-circle/
    public static boolean robotBoundedInCircle(String ins) {
        int row = 0;
        int col = 0;
        int direction = 0; // 0 1 2 3
        char[] str = ins.toCharArray();
        for (char cur : str) {
            if (cur == 'R') {
                direction = right(direction);
            } else if (cur == 'L') {
                direction = left(direction);
            } else {
                row = row(direction, row);
                col = col(direction, col);
            }
        }
        return row == 0 && col == 0 || direction != 0;
    }

    public static int left(int direction) {
        return direction == 0 ? 3 : (direction - 1);
    }

    public static int right(int direction) {
        return direction == 3 ? 0 : (direction + 1);
    }

    public static int row(int direction, int r) {
        return (direction == 1 || direction == 3) ? r : (r + (direction == 0 ? 1 : -1));
    }

    public static int col(int direction, int c) {
        return (direction == 0 || direction == 2) ? c : (c + (direction == 1 ? 1 : -1));
    }

    // 来自微软
    // 给定一个数组arr，一个正数num，一个正数k
    // 可以把arr中的某些数字拿出来组成一组，要求该组中的最大值减去最小值<=num
    // 且该组数字的个数一定要正好等于k
    // 每个数字只能选择进某一组，不能进多个组
    // 返回arr中最多有多少组
    public static int maxTeamNumber(int[] arr, int num, int k) {
        int n = arr.length;
        if (k > n) {
            return 0;
        }
        Arrays.sort(arr);
        int[] dp = new int[n];
        dp[k - 1] = arr[k - 1] - arr[0] <= num ? 1 : 0;
        for (int i = k; i < n; i++) {
            // 不要i位置的数
            int p1 = dp[i - 1];
            // 包含i位置的数，说明i位置的数一定是最后一组的最后一个位置的数
            int p2 = (arr[i] - arr[i - k + 1] <= num ? 1 : 0) + dp[i - k];
            dp[i] = Math.max(p1, p2);
        }
        return dp[n - 1];
    }

    // 测试链接 : https://leetcode.com/problems/stone-game-ix/
    // Alice和Bob再次设计了一款新的石子游戏。
    // 现有一行n个石子，每个石子都有一个关联的数字表示它的价值。给你一个整数数组stones ，其中stones[i]是第i个石子的价值。
    // Alice和 Bob轮流进行自己的回合。Alice先手。每一回合，玩家需要从stones中移除任一石子。
    // 如果玩家移除石子后，导致所有已移除石子的价值总和可以被3整除，那么该玩家就输掉游戏 。
    // 如果不满足上一条，且移除后没有任何剩余的石子，那么Bob将会直接获胜（即便是在Alice的回合）。
    // 假设两位玩家均采用最佳决策。如果Alice获胜，返回true；如果Bob获胜，返回false。
    public static boolean stoneGameIX(int[] stones) {
        int[] counts = new int[3];
        for (int num : stones) {
            counts[num % 3]++;
        }
        return counts[0] % 2 == 0
                ? counts[1] != 0 && counts[2] != 0
                : Math.abs(counts[1] - counts[2]) > 2;
    }

    // 给定一个由 0 和 1 组成的数组 arr ，将数组分成  3 个非空的部分
    // 使得所有这些部分表示相同的二进制值。
    // 如果可以做到，请返回任何 [i, j]，其中 i+1 < j，这样一来
    // arr[0], arr[1], ..., arr[i] 为第一部分
    // arr[i + 1], arr[i + 2], ..., arr[j - 1] 为第二部分
    // arr[j], arr[j + 1], ..., arr[arr.length - 1] 为第三部分
    // 这三个部分所表示的二进制值相等
    // 如果无法做到，就返回 [-1, -1]
    // 注意，在考虑每个部分所表示的二进制时，应当将其看作一个整体
    // 例如，[1,1,0] 表示十进制中的 6，而不会是 3。此外，前导零也是被允许的
    // 所以 [0,1,1] 和 [1,1] 表示相同的值。
    // 测试链接 : https://leetcode.cn/problems/three-equal-parts/
    public static int[] threeEqualParts(int[] arr) {
        // 计算arr中1的数量
        int ones = 0;
        for (int num : arr) {
            ones += num == 1 ? 1 : 0;
        }
        // 如果1的数量不能被3整除，肯定不存在方案
        if (ones % 3 != 0) {
            return new int[]{-1, -1};
        }
        int n = arr.length;
        // 如果1的数量是0，怎么划分都可以了，因为全是0
        if (ones == 0) {
            return new int[]{0, n - 1};
        }
        // 接下来的过程
        // 因为1的数量能被3整除，比如一共有12个1
        // 那么第一段肯定含有第1个1~第4个1
        // 那么第二段肯定含有第5个1~第8个1
        // 那么第三段肯定含有第9个1~第12个1
        // 所以把第1个1，当做第一段的开头，start1
        // 所以把第5个1，当做第二段的开头，start2
        // 所以把第9个1，当做第三段的开头，start3
        int part = ones / 3;
        int start1 = -1;
        int start2 = -1;
        int start3 = -1;
        int cnt = 0;
        // 1个数21个
        // part = 7
        // 1    8
        for (int i = 0; i < n; i++) {
            if (arr[i] == 1) {
                cnt++;
                if (start1 == -1 && cnt == 1) {
                    start1 = i;
                }
                if (start2 == -1 && cnt == part + 1) {
                    start2 = i;
                }
                if (start3 == -1 && cnt == 2 * part + 1) {
                    start3 = i;
                }
            }
        }
        // 第一段的开头往下的部分
        // 第二段的开头往下的部分
        // 第三段的开头往下的部分
        // 要都一样，这三段的状态才是一样的
        // 所以接下来就验证这一点，是不是每一步都一样
        while (start3 < n) {
            if (arr[start1] != arr[start2] || arr[start1] != arr[start3]) {
                // 一旦不一样，肯定没方案了
                return new int[]{-1, -1};
            }
            start1++;
            start2++;
            start3++;
        }
        // 如果验证通过，返回断点即可
        return new int[]{start1 - 1, start2};
    }

    // 有n个动物重量分别是a1、a2、a3.....an，
    // 这群动物一起玩叠罗汉游戏，
    // 规定从左往右选择动物，每只动物左边动物的总重量不能超过自己的重量
    // 返回最多能选多少个动物，求一个高效的算法。
    // 比如有7个动物，从左往右重量依次为：1，3，5，7，9，11，21
    // 则最多能选5个动物：1，3，5，9，21
    // 注意本题给的例子是有序的，但是实际给定的动物数组，可能是无序的，
    // 要求从左往右选动物，且不能打乱原始数组
    public static int maxAnimalNumber(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        // ends数组
        int[] ends = new int[arr.length + 1];
        ends[0] = 0;
        int endsSize = 1;
        int max = 1;
        for (int i = 0; i < arr.length; i++) {
            int l = 0;
            int r = endsSize - 1;
            int m = 0;
            int find = 0;
            while (l <= r) {
                m = (l + r) / 2;
                if (ends[m] <= arr[i]) {
                    find = m;
                    l = m + 1;
                } else {
                    r = m - 1;
                }
            }
            if (find == endsSize - 1) {
                ends[endsSize] = ends[endsSize - 1] + arr[i];
                endsSize++;
            } else {
                if (ends[find + 1] > ends[find] + arr[i]) {
                    ends[find + 1] = ends[find] + arr[i];
                }
            }
            max = Math.max(max, find + 1);
        }
        return max;
    }

    // 本题测试链接 : https://leetcode.com/problems/russian-doll-envelopes/
    public static int maxEnvelopes(int[][] matrix) {
        Envelope[] arr = sort(matrix);
        int[] ends = new int[matrix.length];
        ends[0] = arr[0].h;
        int right = 0;
        int l = 0;
        int r = 0;
        int m = 0;
        for (int i = 1; i < arr.length; i++) {
            l = 0;
            r = right;
            while (l <= r) {
                m = (l + r) / 2;
                if (arr[i].h > ends[m]) {
                    l = m + 1;
                } else {
                    r = m - 1;
                }
            }
            right = Math.max(right, l);
            ends[l] = arr[i].h;
        }
        return right + 1;
    }

    public static class Envelope {
        public int w;
        public int h;

        public Envelope(int weight, int height) {
            w = weight;
            h = height;
        }
    }

    public static class EnvelopeComparator implements Comparator<Envelope> {
        @Override
        public int compare(Envelope o1, Envelope o2) {
            return o1.w != o2.w ? o1.w - o2.w : o2.h - o1.h;
        }
    }

    public static Envelope[] sort(int[][] matrix) {
        Envelope[] res = new Envelope[matrix.length];
        for (int i = 0; i < matrix.length; i++) {
            res[i] = new Envelope(matrix[i][0], matrix[i][1]);
        }
        Arrays.sort(res, new EnvelopeComparator());
        return res;
    }

    // 一个序列的 宽度 定义为该序列中最大元素和最小元素的差值。
    // 给你一个整数数组 nums ，返回 nums 的所有非空 子序列 的 宽度之和
    // 由于答案可能非常大，请返回对 109 + 7 取余 后的结果。
    // 子序列 定义为从一个数组里删除一些（或者不删除）元素，
    // 但不改变剩下元素的顺序得到的数组
    // 例如，[3,6,2,7] 就是数组 [0,3,1,6,2,2,7] 的一个子序列。
    // 测试链接 : https://leetcode.cn/problems/sum-of-subsequence-widths/
    public static int sumSubsequenceWidths(int[] nums) {
        Arrays.sort(nums);
        int mod = 1000000007;
        long ans = 0;
        long A = 0;
        long B = 0;
        long C = 1;
        long D = C;
        for (int i = 1; i < nums.length; i++) {
            A = (D * nums[i]) % mod;
            B = (B * 2 + nums[i - 1]) % mod;
            ans = (ans + A - B + mod) % mod;
            C = (C * 2) % mod;
            D = (D + C) % mod;
        }
        return (int) (ans);
    }

    // 来自学员问题
    // 商场中有一展柜A，其大小固定，现已被不同的商品摆满
    // 商家提供了一些新商品B，需要对A中的部分商品进行更新替换
    // B中的商品可以自由使用，也就是可以用B中的任何商品替换A中的任何商品
    // A中的商品一旦被替换，就认为消失了！而不是回到了B中！
    // 要求更新过后的展柜中，商品严格按照价格由低到高进行排列
    // 不能有相邻商品价格相等的情况
    // A[i]为展柜中第i个位置商品的价格，B[i]为各个新商品的价格
    // 求能够满足A中商品价格严格递增的最小操作次数，若无法满足则返回-1
    // 可以用B里的数字，替换A里的数字，想让A严格递增
    // 返回至少换几个数字
    public static int makeASortedMinSwaps(int[] A, int[] B) {
        // 根据题意，B里的数字随意拿
        // 所以B里的数字排序，不会影响拿
        // 同时，A如果从左往右考虑，依次被B替换上去的数字，肯定是从小到大的
        // 这是一定的！比如B = {5,3,2,9}
        // 可能先用5替换A的某个左边的数，再用2替换A的某个右边的数吗？不可能
        // 所以将B排序
        Arrays.sort(B);
        int ans = process(A, B, 0, 0, 0);
        return ans == Integer.MAX_VALUE ? -1 : ans;
    }

    // 参数解释：
    // A[0...ai-1]范围上已经做到升序了
    // 接下来请让A[ai....]范围上的数字做到升序
    // 之前的过程中，B里可能已经拿过一些数字了
    // 拿过的数字都在B[0...bi-1]范围上，不一定都拿了
    // 但是最后拿的数字一定是B[bi-1]
    // 如果想用B里的数字替换当前的A[ai]，请在B[bi....]范围上考虑拿数字
    // pre : 表示之前的A[ai-1]被替换了没有，
    // 如果pre==0，表示A[ai-1]没被替换
    // 如果pre==1，表示A[ai-1]被替换了，被谁替换的？被B[bi-1]替换的！
    // 返回值：让A[ai....]范围上的数字做到升序，最少还要在B[bi...]上拿几个数字
    // 如果返回值是Integer.MAX_VALUE，表示怎么都做不到
    // 这就是一个三维动态规划，自己改！
    // ai 是N范围
    // bi 是M范围
    // pre 只有0、1两种值
    // 所以时间复杂度O(N*M*logM)
    // 这个logM怎么来的，二分来的，看代码！
    public static int process(int[] A, int[] B, int ai, int bi, int pre) {
        if (ai == A.length) {
            return 0;
        }
        // 之前的数是什么
        int preNum = 0;
        if (ai == 0) {
            preNum = Integer.MIN_VALUE;
        } else {
            if (pre == 0) {
                preNum = A[ai - 1];
            } else {
                preNum = B[bi - 1];
            }
        }
        // 可能性1，搞定当前的A[ai]，不依靠交换
        int p1 = preNum < A[ai] ? process(A, B, ai + 1, bi, 0) : Integer.MAX_VALUE;
        // 可能性2，搞定当前的A[ai]，依靠交换
        int p2 = Integer.MAX_VALUE;
        // 在B[bi....]这个范围上，找到>preNum，最左的位置
        // 这一步是可以二分的！因为B整体有序
        int nearMoreIndex = bs(B, bi, preNum);
        if (nearMoreIndex != -1) {
            int next2 = process(A, B, ai + 1, nearMoreIndex + 1, 1);
            if (next2 != Integer.MAX_VALUE) {
                p2 = 1 + next2;
            }
        }
        return Math.min(p1, p2);
    }

    // 在B[start....]这个范围上，找到>num，最左的位置
    public static int bs(int[] B, int start, int num) {
        int ans = -1;
        int l = start;
        int r = B.length - 1;
        int m = 0;
        while (l <= r) {
            m = (l + r) / 2;
            if (B[m] > num) {
                ans = m;
                r = m - 1;
            } else {
                l = m + 1;
            }
        }
        return ans;
    }

    // 给你一个由正整数组成的数组 nums 。
    // 数字序列的 最大公约数 定义为序列中所有整数的共有约数中的最大整数。
    // 例如，序列 [4,6,16] 的最大公约数是 2 。
    // 数组的一个 子序列 本质是一个序列，可以通过删除数组中的某些元素（或者不删除）得到。
    // 例如，[2,5,10] 是 [1,2,1,2,4,1,5,10] 的一个子序列。
    // 计算并返回 nums 的所有 非空 子序列中 不同 最大公约数的 数目 。
    // 测试链接 : https://leetcode.com/problems/number-of-different-subsequences-gcds/
    // n/1 + n/2 + n/3 + n/4 + ... + n/n -> O(N * logN)
    public static int countDifferentSubsequenceGCDs(int[] nums) {
        // 找到数组中的最大数！max
        int max = Integer.MIN_VALUE;
        for (int num : nums) {
            max = Math.max(max, num);
        }
        // 1~max，哪个数有哪个数没有
        boolean[] set = new boolean[max + 1];
        for (int num : nums) {
            set[num] = true;
        }
        int ans = 0;
        // a是当前想确定，是不是某个子序列的最大公约数，有a！
        for (int a = 1; a <= max; a++) {
            // 1)找到，离a最近的，a的倍数！1 2 3 ... g就是
            int g = a;
            for (; g <= max; g += a) {
                if (set[g]) {
                    break;
                }
            }
            // 2) 找到了a最近的倍数，g
            // g + 0 , g ?= a
            // g + a , g ?= a
            // g + 2a , g ?= a
            // g + 3a , g ?= a
            for (int b = g; b <= max; b += a) {
                if (set[b]) {
                    g = gcd(g, b);
                    if (g == a) {
                        ans++;
                        break;
                    }
                }
            }
        }
        return ans;
    }

    public static int gcd(int m, int n) {
        return n == 0 ? m : gcd(n, m % n);
    }

    // 来自通维数码
    // 每个会议给定开始和结束时间
    // 后面的会议如果跟前面的会议有任何冲突，完全取消冲突的、之前的会议，安排当前的
    // 给定一个会议数组，返回安排的会议列表
    // 会议有N个，时间复杂度O(N*logN)
    public static ArrayList<int[]> arrange2(int[][] meetings) {
        int n = meetings.length;
        // n << 1 -> n*2
        int[] rank = new int[n << 1];
        for (int i = 0; i < meetings.length; i++) {
            rank[i] = meetings[i][0]; // 会议开头点
            rank[i + n] = meetings[i][1] - 1; // 会议的结束点
        }
        Arrays.sort(rank);
        // n*2
        SegmentTree3 st = new SegmentTree3(n << 1);
        // 哪些会议安排了，放入到ans里去！
        ArrayList<int[]> ans = new ArrayList<>();
        // 从右往左遍历，意味着，后出现的会议，先看看能不能安排
        for (int i = meetings.length - 1; i >= 0; i--) {
            // cur 当前会议
            int[] cur = meetings[i];
            // cur[0] = 17万 -> 6
            int from = rank3(rank, cur[0]);
            // cur[1] = 90 -> 89 -> 2
            int to = rank3(rank, cur[1] - 1);
            if (st.sum(from, to) == 0) {
                ans.add(cur);
            }
            st.add(from, to, 1);
        }
        return ans;
    }

    public static int rank3(int[] rank, int num) {
        int l = 0;
        int r = rank.length - 1;
        int m = 0;
        int ans = 0;
        while (l <= r) {
            m = (l + r) / 2;
            if (rank[m] >= num) {
                ans = m;
                r = m - 1;
            } else {
                l = m + 1;
            }
        }
        return ans + 1;
    }

    public static class SegmentTree3 {
        private int n;
        private int[] sum;
        private int[] lazy;

        public SegmentTree3(int size) {
            n = size + 1;
            sum = new int[n << 2];
            lazy = new int[n << 2];
            n--;
        }

        private void pushUp(int rt) {
            sum[rt] = sum[rt << 1] + sum[rt << 1 | 1];
        }

        private void pushDown(int rt, int ln, int rn) {
            if (lazy[rt] != 0) {
                lazy[rt << 1] += lazy[rt];
                sum[rt << 1] += lazy[rt] * ln;
                lazy[rt << 1 | 1] += lazy[rt];
                sum[rt << 1 | 1] += lazy[rt] * rn;
                lazy[rt] = 0;
            }
        }

        public void add(int L, int R, int C) {
            add(L, R, C, 1, n, 1);
        }

        private void add(int L, int R, int C, int l, int r, int rt) {
            if (L <= l && r <= R) {
                sum[rt] += C * (r - l + 1);
                lazy[rt] += C;
                return;
            }
            int mid = (l + r) >> 1;
            pushDown(rt, mid - l + 1, r - mid);
            if (L <= mid) {
                add(L, R, C, l, mid, rt << 1);
            }
            if (R > mid) {
                add(L, R, C, mid + 1, r, rt << 1 | 1);
            }
            pushUp(rt);
        }

        public int sum(int L, int R) {
            return query(L, R, 1, n, 1);
        }

        private int query(int L, int R, int l, int r, int rt) {
            if (L <= l && r <= R) {
                return sum[rt];
            }
            int mid = (l + r) >> 1;
            pushDown(rt, mid - l + 1, r - mid);
            int ans = 0;
            if (L <= mid) {
                ans += query(L, R, l, mid, rt << 1);
            }
            if (R > mid) {
                ans += query(L, R, mid + 1, r, rt << 1 | 1);
            }
            return ans;
        }
    }

    // 把字符串 s 看作 "abcdefghijklmnopqrstuvwxyz"的无限环绕字符串，
    // 所以s 看起来是这样的：
    // ...zabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcd....
    // 现在给定另一个字符串 p 。返回s 中 不同 的 p 的 非空子串的数量
    // 测试链接 : https://leetcode.com/problems/unique-substrings-in-wraparound-string/
    public int UniqueSubstringsInWrapAroundString(String s) {
        char[] str = s.toCharArray();
        int n = str.length;
        // 答案
        int ans = 0;
        int len = 1;
        // 256 0~255
        int[] max = new int[256];
        max[str[0]]++;
        for (int i = 1; i < n; i++) {
            char cur = str[i];
            char pre = str[i - 1];
            if ((pre == 'z' && cur == 'a') || pre + 1 == cur) {
                len++;
            } else {
                len = 1;
            }
            max[cur] = Math.max(max[cur], len);
        }
        for (int i = 0; i < 256; i++) {
            ans += max[i];
        }
        return ans;
    }

    // 来自小红书
    // [0,4,7] ： 0表示这里石头没有颜色，如果变红代价是4，如果变蓝代价是7
    // [1,X,X] ： 1表示这里石头已经是红，而且不能改颜色，所以后两个数X无意义
    // [2,X,X] ： 2表示这里石头已经是蓝，而且不能改颜色，所以后两个数X无意义
    // 颜色只可能是0、1、2，代价一定>=0
    // 给你一批这样的小数组，要求最后必须所有石头都有颜色，且红色和蓝色一样多，返回最小代价
    // 如果怎么都无法做到所有石头都有颜色、且红色和蓝色一样多，返回-1
    public static int magicStone(int[][] stones) {
        int n = stones.length;
        if ((n & 1) != 0) {
            return -1;
        }
        Arrays.sort(stones, (a, b) -> a[0] == 0 && b[0] == 0 ? (b[1] - b[2] - (a[1] - a[2])) : (a[0] - b[0]));
        int zero = 0;
        int red = 0;
        int blue = 0;
        int cost = 0;
        for (int i = 0; i < n; i++) {
            if (stones[i][0] == 0) {
                zero++;
                cost += stones[i][1];
            } else if (stones[i][0] == 1) {
                red++;
            } else {
                blue++;
            }
        }
        if (red > (n >> 1) || blue > (n >> 1)) {
            return -1;
        }
        blue = zero - ((n >> 1) - red);
        for (int i = 0; i < blue; i++) {
            cost += stones[i][2] - stones[i][1];
        }
        return cost;
    }

    // 给定一个整数数组A，坡是元组(i, j)，其中i < j且A[i] <= A[j]
    // 这样的坡的宽度为j - i
    // 找出A中的坡的最大宽度，如果不存在，返回 0
    // 示例 1：
    // 输入：[6,0,8,2,1,5]
    // 输出：4
    // 解释：
    // 最大宽度的坡为 (i, j) = (1, 5): A[1] = 0 且 A[5] = 5
    // 示例 2：
    // 输入：[9,8,1,0,1,9,4,0,4,1]
    // 输出：7
    // 解释：
    // 最大宽度的坡为 (i, j) = (2, 9): A[2] = 1 且 A[9] = 1
    // 测试链接 : https://leetcode.cn/problems/maximum-width-ramp/
    // 单调栈求解
    public static int maxWidthRamp(int[] arr) {
        int n = arr.length;
        // 栈中只放下标
        int[] stack = new int[n];
        // 栈的大小
        int r = 0;
        for (int i = 0; i < n; i++) {
            if (r == 0 || arr[stack[r - 1]] > arr[i]) {
                stack[r++] = i;
            }
        }
        int ans = 0;
        // 从右往左遍历
        // j = n - 1
        for (int j = n - 1; j >= 0; j--) {
            while (r != 0 && arr[stack[r - 1]] <= arr[j]) {
                int i = stack[--r];
                ans = Math.max(ans, j - i);
            }
        }
        return ans;
    }

    // 来自学员问题
    // 一共有n个项目，每个项目都有两个信息
    // projects[i] = {a, b}
    // 表示i号项目做完要a天，但是当你投入b个资源，它就会缩短1天的时间
    // 你一共有k个资源，你的目标是完成所有的项目，但是希望总天数尽可能缩短
    // 在所有项目同时开工的情况下，返回尽可能少的天数
    // 1 <= n <= 10^5
    // 1 <= k <= 10^7
    public static int minDays(int[][] projects, int k) {
        // l......r
        // 0   所有项目中，天数的最大值
        int l = 0;
        int r = 0;
        // project[0] : 既定天数
        // project[1] : 投入多少资源能减少1天
        for (int[] project : projects) {
            r = Math.max(r, project[0]);
        }
        // l......r
        int m, ans = r;
        while (l <= r) {
            m = (l + r) / 2;
            if (yeah(projects, m) <= k) {
                ans = m;
                r = m - 1;
            } else {
                l = m + 1;
            }
        }
        return ans;
    }

    // 给你所有的项目！projects
    // 一定要在days天内完成！
    // 返回，需要的资源是多少！
    public static int yeah(int[][] projects, int days) {
        int ans = 0;
        for (int[] p : projects) {
            if (p[0] > days) {
                ans += (p[0] - days) * p[1];
            }
        }
        return ans;
    }

    // 给你一个 非递减 的正整数数组 nums 和整数 K
    // 判断该数组是否可以被分成一个或几个 长度至少 为 K 的 不相交的递增子序列
    // 测试链接 : https://leetcode.cn/problems/divide-array-into-increasing-sequences/
    public static boolean canDivideIntoSubsequences(int[] nums, int k) {
        int cnt = 1;
        int maxCnt = 1;
        for (int i = 1; i < nums.length; i++) {
            if (nums[i - 1] != nums[i]) {
                maxCnt = Math.max(maxCnt, cnt);
                cnt = 1;
            } else {
                cnt++;
            }
        }
        maxCnt = Math.max(maxCnt, cnt);
        return nums.length / maxCnt >= k;
    }

    // 来自百度
    // 用r、e、d三种字符，拼出一个回文子串数量等于x的字符串
    // 1 <= x <= 10^5
    public static String palindromeX(int x) {
        StringBuilder builder = new StringBuilder();
        char cur = 'r';
        while (x > 0) {
            // 根据x，找到当前需要几个相同字符来凑回文子串数量
            // x = 12,  1 3 6 10 15
            //          1 2 3 4  5
            int number = near(x);
            for (int i = 0; i < number; i++) {
                builder.append(cur);
            }
            // 4 -> 10
            x -= number * (number + 1) / 2;
            cur = cur == 'r' ? 'e' : (cur == 'e' ? 'd' : 'r');
        }
        return builder.toString();
    }

    public static int near(int x) {
        int l = 1;
        int r = x;
        int m, ans = 0;
        while (l <= r) {
            m = (l + r) / 2;
            if (ok(m, x)) {
                ans = m;
                l = m + 1;
            } else {
                r = m - 1;
            }
        }
        return ans;
    }

    public static boolean ok(int n, int x) {
        return ((long) n * (n + 1) / 2) <= x;
    }

    // n块石头放置在二维平面中的一些整数坐标点上
    // 每个坐标点上最多只能有一块石头
    // 如果一块石头的 同行或者同列 上有其他石头存在，那么就可以移除这块石头。
    // 给你一个长度为 n 的数组 stones ，
    // 其中 stones[i] = [xi, yi] 表示第 i 块石头的位置，
    // 返回 可以移除的石子 的最大数量。
    // 测试链接 : https://leetcode.com/problems/most-stones-removed-with-same-row-or-column/
    public static int removeStones(int[][] stones) {
        int n = stones.length;
        HashMap<Integer, Integer> rowPre = new HashMap<Integer, Integer>();
        HashMap<Integer, Integer> colPre = new HashMap<Integer, Integer>();
        UnionFind2 uf = new UnionFind2(n);
        for (int i = 0; i < n; i++) {
            int x = stones[i][0];
            int y = stones[i][1];
            if (!rowPre.containsKey(x)) {
                rowPre.put(x, i);
            } else {
                uf.union(i, rowPre.get(x));
            }
            if (!colPre.containsKey(y)) {
                colPre.put(y, i);
            } else {
                uf.union(i, colPre.get(y));
            }
        }
        return n - uf.sets();
    }

    public static class UnionFind2 {

        public int[] father;
        public int[] size;
        public int[] help;
        public int sets;// 并查集中集合的个数

        public UnionFind2(int n) {
            father = new int[n];
            size = new int[n];
            help = new int[n];
            for (int i = 0; i < n; i++) {
                father[i] = i;
                size[i] = 1;
            }
            sets = n;
        }

        private int find(int i) {
            int hi = 0;
            while (i != father[i]) {
                help[hi++] = i;
                i = father[i];
            }
            while (hi != 0) {
                father[help[--hi]] = i;
            }
            return i;
        }

        public void union(int i, int j) {
            int fi = find(i);
            int fj = find(j);
            if (fi != fj) {
                if (size[fi] >= size[fj]) {
                    father[fj] = fi;
                    size[fi] += size[fj];
                } else {
                    father[fi] = fj;
                    size[fj] += size[fi];
                }
                sets--;
            }
        }

        public int sets() {
            return sets;
        }
    }

    // 给定一个由 '[' ，']'，'('，‘)’ 组成的字符串
    // 请问最少插入多少个括号就能使这个字符串的所有括号左右配对
    // 例如当前串是 "([[])"，那么插入一个']'即可满足
    // 输出最少插入多少个括号
    // 测试链接 : https://www.nowcoder.com/practice/e391767d80d942d29e6095a935a5b96b
    // 提交以下所有代码，把主类名改成Main，可以直接通过
    public static int minAdd(char[] s) {
        int n = s.length;
        int[][] dp = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                dp[i][j] = -1;
            }
        }
        return process(s, 0, s.length - 1, dp);
    }

    // 让s[l...r]都完美匹配
    // 至少需要加几个字符
    public static int process(char[] s, int l, int r, int[][] dp) {
        // 只有一个字符，不管是什么，要想配对，都需要添加一个字符
        if (l == r) {
            return 1;
        }
        // 只有两个字符，
        // 如果是()、[]，那什么也不需要添加
        // 否则，都需要添加2个字符
        if (l == r - 1) {
            if ((s[l] == '(' && s[r] == ')') || (s[l] == '[' && s[r] == ']')) {
                return 0;
            }
            return 2;
        }
        if (dp[l][r] != -1) {
            return dp[l][r];
        }
        // 重点是如下的过程
        // 可能性1，先搞定l+1...r，然后搞定l
        // 比如s[l...r] = ([][]
        // 先搞定[][]，需要添加0个，然后搞定(，需要添加1个
        // 整体变成([][])搞定
        // l....r -> l l+1....r ?
        int p1 = 1 + process(s, l + 1, r, dp);
        // 可能性2，先搞定l...r-1，然后搞定r
        // 和可能性1同理
        // l...r -> ? l...r-1 r
        int p2 = 1 + process(s, l, r - 1, dp);
        // l( ...r) l+1..r-1
        // l[    r] l+1..r-1
        // 可能性3，s[l]和s[r]天然匹配，需要搞定的就是l+1..r-1
        // 比如([[)，搞定中间的[[，就是最优解了
        int p3 = Integer.MAX_VALUE;
        if ((s[l] == '(' && s[r] == ')') || (s[l] == '[' && s[r] == ']')) {
            p3 = process(s, l + 1, r - 1, dp);
        }

        // l......r
        // l..l   l+1..r
        // l..l+1 l+2..r
        // l...l+2 l+3..r
        // 可能性后续：可能，最优解并不是l....r整体变成最大的嵌套
        // 而是，并列关系！
        // l....split 先变成合法
        // split+1...r 再变成合法
        // 是并列的关系！
        // 比如(())[[]]
        // l...split : (())
        // split+1...r : [[]]
        // 这种并列关系下，有可能出最优解
        // 所以，枚举每一个可能的并列划分点(split)
        int ans = Math.min(p1, Math.min(p2, p3));
        for (int split = l; split < r; split++) {
            ans = Math.min(ans, process(s, l, split, dp) + process(s, split + 1, r, dp));
        }
        dp[l][r] = ans;
        return ans;
    }

    // 来自bilibili
    // 现在有N条鱼，每条鱼的体积为Ai，从左到右排列，数组arr给出
    // 每一轮，左边的大鱼一定会吃掉右边比自己小的第一条鱼，
    // 并且每条鱼吃比自己小的鱼的事件是同时发生的。
    // 返回多少轮之后，鱼的数量会稳定
    // 注意：6 6 3 3
    // 第一轮过后 :
    // 对于两个6来说，右边比自己小的第一条鱼都是第1个3，所以只有这个3被吃掉，
    // 数组变成 : 6 6 3（第2个3）
    // 第二轮过后 : 6 6
    // 返回2
    public static int minTurns(int[] arr) {
        int n = arr.length;
        int[][] stack = new int[n][2];
        int stackSize = 0;
        int ans = 0;
        for (int i = n - 1; i >= 0; i--) {
            int curAns = 0;
            while (stackSize > 0 && stack[stackSize - 1][0] < arr[i]) {
                curAns = Math.max(curAns + 1, stack[--stackSize][1]);
            }
            stack[stackSize][0] = arr[i];
            stack[stackSize++][1] = curAns;
            ans = Math.max(ans, curAns);
        }
        return ans;
    }

    // 来自学员面试
    // nim博弈 + 巴什博弈
    // 有a块草莓蛋糕，有b块芝士蛋糕，两人轮流拿蛋糕
    // 每次不管是谁只能选择在草莓蛋糕和芝士蛋糕中拿一种
    // 拿的数量在1~m之间随意
    // 谁先拿完最后的蛋糕谁赢
    // 返回先手赢还是后手赢
    public static String whoWin2(int a, int b, int m) {
        if (m >= Math.max(a, b)) { // nim博弈
            return a != b ? "先手" : "后手";
        }
        // m < max(a,b)
        if (a == b) {
            // 蛋糕一样多
            // 先手必输，因为先手不管拿什么，拿多少
            // 后手都在另一堆上，拿同样多的蛋糕
            // 继续让两堆蛋糕一样多
            // 最终先手必输，后手必赢
            return "后手";
        }
        // 如果 a != b
        // 关注a和b的差值，
        // 谁最先遇到差值为0，谁输
        // 那么这就是巴什博奕
        // 差值蛋糕数量共rest个。
        // 每次从最少取1个，最多取m个，最后取光的人取胜。
        // 如果rest=(m+1)*k + s (s!=0) 那么先手一定必胜
        // 因为第一次取走s个，
        // 接下来无论对手怎么取，
        // 先手都能保证取到所有(m+1)倍数的点，
        // 那么循环下去一定能取到差值最后一个。
        int rest = Math.max(a, b) - Math.min(a, b);
        return rest % (m + 1) != 0 ? "先手" : "后手";
    }

    // 来自学员问题
    // 给定N、M两个参数
    // 一共有N个格子，每个格子可以涂上一种颜色，颜色在M种里选
    // 当涂满N个格子，并且M种颜色都使用了，叫一种有效方法
    // 求一共有多少种有效方法
    // 1 <= N, M <= 5000
    // 返回结果比较大，请把结果 % 1000000007 之后返回
    public static int ways2(int n, int m) {
        int MAXN = 5001;
        int[][] dp = new int[MAXN][MAXN];
        int mod = 1000000007;
        for (int i = 1; i <= n; i++) {
            dp[i][1] = m;
        }
        for (int i = 2; i <= n; i++) {
            for (int j = 2; j <= m; j++) {
                dp[i][j] = (int) (((long) dp[i - 1][j] * j) % mod);
                dp[i][j] = (int) ((((long) dp[i - 1][j - 1] * (m - j + 1)) + dp[i][j]) % mod);
            }
        }
        return dp[n][m];
    }

    // 将一个给定字符串 s 根据给定的行数 numRows
    // 以从上往下、从左到右进行 Z 字形排列
    // 比如输入字符串为 "PAYPALISHIRING" 行数为 3 时，排列如下
    // P   A   H   N
    // A P L S I I G
    // Y   I   R
    // 之后，你的输出需要从左往右逐行读取，产生出一个新的字符串
    // "PAHNAPLSIIGYIR"
    // 请你实现这个将字符串进行指定行数变换的函数
    // string convert(string s, int numRows)
    // 测试链接 : https://leetcode.cn/problems/zigzag-conversion/
    public static String convert(String s, int row) {
        int n = s.length();
        if (row == 1 || row >= n) {
            return s;
        }
        int t = 2 * (row - 1);
        char[] ans = new char[n];
        int fill = 0;
        for (int i = 0; i < row; i++) {
            for (int j = i, nextColTop = t; j < n; j += t, nextColTop += t) {
                ans[fill++] = s.charAt(j);
                if (i >= 1 && i <= row - 2 && nextColTop - i < n) {
                    ans[fill++] = s.charAt(nextColTop - i);
                }
            }
        }
        return String.valueOf(ans);
    }

    // 来自阿里笔试
    // 牛牛今年上幼儿园了，老师叫他学习减法
    // 老师给了他5个数字，他每次操作可以选择其中的4个数字减1
    // 减一之后的数字不能小于0，因为幼儿园的牛牛还没有接触过负数
    // 现在牛牛想知道，自己最多可以进行多少次这样的操作
    // 扩展问题来自leetcode 2141，掌握了这个题原始问题就非常简单了
    // leetcode测试链接 : https://leetcode.com/problems/maximum-running-time-of-n-computers/
    public static long fourNumbersMinusOn(int n, int[] arr) {
        Arrays.sort(arr);
        int size = arr.length;
        long[] sums = new long[size];
        sums[0] = arr[0];
        for (int i = 1; i < size; i++) {
            sums[i] = sums[i - 1] + arr[i];
        }
        long l = 0;
        long m = 0;
        long r = sums[size - 1] / n;
        long ans = -1;
        while (l <= r) {
            m = (l + r) / 2;
            if (ok(arr, sums, m, n)) {
                ans = m;
                l = m + 1;
            } else {
                r = m - 1;
            }
        }
        return ans;
    }

    public static boolean ok(int[] arr, long[] sum, long time, int num) {
        int l = 0;
        int m = 0;
        int r = arr.length - 1;
        int left = arr.length;
        // >= time 最左
        while (l <= r) {
            m = (l + r) / 2;
            if (arr[m] >= time) {
                left = m;
                r = m - 1;
            } else {
                l = m + 1;
            }
        }
        num -= arr.length - left;
        long rest = left == 0 ? 0 : sum[left - 1];
        return time * (long) num <= rest;
    }

    // 给你一个长度为 n的整数数组rolls和一个整数k。
    // 你扔一个k面的骰子 n次，骰子的每个面分别是1到k，
    // 其中第i次扔得到的数字是rolls[i]。
    // 请你返回 无法从 rolls中得到的 最短骰子子序列的长度。
    // 扔一个 k面的骰子 len次得到的是一个长度为 len的 骰子子序列。
    // 注意，子序列只需要保持在原数组中的顺序，不需要连续。
    // 测试链接 : https://leetcode.cn/problems/shortest-impossible-sequence-of-rolls/
    // 所有数字1~k
    public static int shortestImpossibleSequenceOfRolls(int[] rolls, int k) {
        // 1~k上，某个数字是否收集到了！
        // set[i] == true
        // set[i] == false
        boolean[] set = new boolean[k + 1];
        // 当前这一套，收集了几个数字了？
        int size = 0;
        int ans = 0;
        for (int num : rolls) {
            if (!set[num]) {
                set[num] = true;
                size++;
            }
            if (size == k) {
                ans++;
                Arrays.fill(set, false);
                size = 0;
            }
        }
        return ans + 1;
    }

    // 给定一个字符串s，求s中有多少个字面值不同的子序列
    // 本题测试链接 : https://leetcode.com/problems/distinct-subsequences-ii/
    public static int distinctSubSeqII(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        long m = 1000000007;
        char[] str = s.toCharArray();
        long[] count = new long[26];
        long all = 1; // 算空集
        for (char x : str) {
            long add = (all - count[x - 'a'] + m) % m;
            all = (all + add) % m;
            count[x - 'a'] = (count[x - 'a'] + add) % m;
        }
        all = (all - 1 + m) % m;
        return (int) all;
    }

    // 找到数组中的水王数
    // 水王数：如果某一种数的出现次数大于数组长度的一半，就说明这个数是水王数
    // 时间复杂度O(N) 空间复杂度O(1)
    public static int waterKing(int[] arr) {
        // 利用水王数出现的次数是大于数组长度的一半的特性
        int candidate = 0;
        int hp = 0;
        for (int i = 0; i < arr.length; i++) {
            if (hp == 0) {
                candidate = arr[i];
                hp = 1;
            } else if (arr[i] == candidate) {
                hp++;
            } else {
                hp--;
            }
        }
        if (hp == 0) {
            return -1;
        }
        hp = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == candidate) {
                hp++;
            }
        }
        return hp > arr.length / 2 ? candidate : -1;
    }

    // 在线测试链接 : https://leetcode.com/problems/house-robber/
    public static int rob1(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        if (arr.length == 1) {
            return arr[0];
        }
        if (arr.length == 2) {
            return Math.max(arr[0], arr[1]);
        }
        int[] dp = new int[arr.length];
        // dp[i] : arr[0..i]挑选，满足不相邻设定的情况下，随意挑选，最大的累加和
        dp[0] = arr[0];
        dp[1] = Math.max(arr[0], arr[1]);
        for (int i = 2; i < arr.length; i++) {
            int p1 = dp[i - 1];
            int p2 = arr[i] + Math.max(dp[i - 2], 0);
            dp[i] = Math.max(p1, p2);
        }
        return dp[arr.length - 1];
    }

    // 给定一个数组arr，在不能取相邻数的情况下，返回所有组合中的最大累加和
    // 思路：
    // 定义dp[i] : 表示arr[0...i]范围上，在不能取相邻数的情况下，返回所有组合中的最大累加和
    // 在arr[0...i]范围上，在不能取相邻数的情况下，得到的最大累加和，可能性分类：
    // 可能性 1) 选出的组合，不包含arr[i]。那么dp[i] = dp[i-1]
    // 比如，arr[0...i] = {3,4,-4}，最大累加和是不包含i位置数的时候
    //
    // 可能性 2) 选出的组合，只包含arr[i]。那么dp[i] = arr[i]
    // 比如，arr[0...i] = {-3,-4,4}，最大累加和是只包含i位置数的时候
    //
    // 可能性 3) 选出的组合，包含arr[i], 且包含arr[0...i-2]范围上的累加和。那么dp[i] = arr[i] + dp[i-2]
    // 比如，arr[0...i] = {3,1,4}，最大累加和是3和4组成的7，因为相邻不能选，所以i-1位置的数要跳过
    //
    // 综上所述：dp[i] = Max { dp[i-1], arr[i] , arr[i] + dp[i-2] }
    public static int rob2(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        int N = arr.length;
        if (N == 1) {
            return arr[0];
        }
        if (N == 2) {
            return Math.max(arr[0], arr[1]);
        }
        int[] dp = new int[N];
        dp[0] = arr[0];
        dp[1] = Math.max(arr[0], arr[1]);
        for (int i = 2; i < N; i++) {
            dp[i] = Math.max(Math.max(dp[i - 1], arr[i]), arr[i] + dp[i - 2]);
        }
        return dp[N - 1];
    }

    // 给你一个 m x n 的二进制矩阵 grid
    // 每个格子要么为 0 （空）要么为 1 （被占据）
    // 给你邮票的尺寸为 stampHeight x stampWidth
    // 我们想将邮票贴进二进制矩阵中，且满足以下 限制 和 要求 ：
    // 覆盖所有空格子，不覆盖任何被占据的格子
    // 可以放入任意数目的邮票，邮票可以相互有重叠部分
    // 邮票不允许旋转，邮票必须完全在矩阵内
    // 如果在满足上述要求的前提下，可以放入邮票，请返回 true ，否则返回 false
    // 测试链接 : https://leetcode.cn/problems/stamping-the-grid/
    public static boolean possibleToStamp(int[][] grid, int h, int w) {
        int n = grid.length;
        int m = grid[0].length;
        // 左上累加和矩阵
        int[][] sum = new int[n + 1][m + 1];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                sum[i + 1][j + 1] = grid[i][j];
            }
        }
        build(sum);
        int[][] diff = new int[n + 2][m + 2];
        for (int a = 1, c = a + h - 1; c <= n; a++, c++) {
            for (int b = 1, d = b + w - 1; d <= m; b++, d++) {
                if (empty(sum, a, b, c, d)) {
                    set(diff, a, b, c, d);
                }
            }
        }
        build(diff);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if (grid[i][j] == 0 && diff[i + 1][j + 1] == 0) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void build(int[][] m) {
        for (int i = 1; i < m.length; i++) {
            for (int j = 1; j < m[0].length; j++) {
                m[i][j] += m[i - 1][j] + m[i][j - 1] - m[i - 1][j - 1];
            }
        }
    }

    public static boolean empty(int[][] sum, int a, int b, int c, int d) {
        return sum[c][d] - sum[c][b - 1] - sum[a - 1][d] + sum[a - 1][b - 1] == 0;
    }

    // (3,4) (8,9)
    // 3,4 +1
    public static void set(int[][] diff, int a, int b, int c, int d) {
        diff[a][b] += 1;
        diff[c + 1][d + 1] += 1;
        diff[c + 1][b] -= 1;
        diff[a][d + 1] -= 1;
    }

    // 测试链接 : https://leetcode.com/problems/reaching-points/
    // 对大体思路的优化
    // s ( 5, 10)
    // t (100, 65)
    public static boolean reachingPoints2(int sx, int sy, int tx, int ty) {
        while (sx < tx && sy < ty) {
            if (tx < ty) {
                ty %= tx;
            } else {
                tx %= ty;
            }
        }
        // 1) startx >= tx
        // 2) starty >= ty
        return (sx == tx && sy <= ty && (ty - sy) % sx == 0)
                || (sy == ty && sx <= tx && (tx - sx) % sy == 0);
    }

    // 来自学员问题
    // 给你一根长度为 n 的绳子
    // 请把绳子剪成整数长度的 m 段
    // m、n都是整数，n > 1并且m > 1
    // 每段绳子的长度记为 k[0],k[1]...k[m - 1]
    // 请问 k[0]*k[1]*...*k[m - 1] 可能的最大乘积是多少
    // 例如，当绳子的长度是8时，我们把它剪成长度分别为2、3、3的三段，此时得到的最大乘积是18
    // 答案需要取模1000000007
    // 测试链接 : https://leetcode.cn/problems/jian-sheng-zi-ii-lcof/
    // x的n次方，% mod之后，是多少？
    // 快速幂的方式，体系学习班，斐波那契数列章节
    public static long power(long x, int n) {
        long ans = 1;
        while (n > 0) {
            if ((n & 1) == 1) {
                ans = (ans * x) % mod;
            }
            x = (x * x) % mod;
            n >>= 1;
        }
        return ans;
    }

    // 纯观察，没有为什么
    public static int cuttingRope(int n) {
        if (n == 2) {
            return 1;
        }
        if (n == 3) {
            return 2;
        }
        // n >= 4
        // n = 13
        // n % 3 == 1
        // 4 -> 2 * 2 (n-4) / 3 -> 3的(n-4)/3 次方
        // n % 3 == 2
        // 2 -> 2 (n-2)/3 -> 3的(n-2)/3次方
        // n:
        //
        // last :
        // n = 9
        // 3 * 3 * 3 * 1
        // n = 10
        // 3 * 3 * (2 * 2)
        // n = 11
        // 3 * 3 * 3 * (2)
        // n = 9
        // rest = 9 -> 3 ?
        // n = 10
        // rest = 10 - 4 -> 6 ?
        // n == 11
        // rest = 11 - 2 = 9 ?
        int rest = n % 3 == 0 ? n : (n % 3 == 1 ? (n - 4) : (n - 2));
        int last = n % 3 == 0 ? 1 : (n % 3 == 1 ? 4 : 2);
        // (3的(rest/3)次方 * last) % mod
        return (int) ((power(3, rest / 3) * last) % mod);
    }

    // 如果一个正整数自身是回文数，而且它也是一个回文数的平方，那么我们称这个数为超级回文数。
    // 现在，给定两个正整数 L 和 R （以字符串形式表示），
    // 返回包含在范围 [L, R] 中的超级回文数的数目。
    // 测试链接 : https://leetcode.cn/problems/super-palindromes/
    // L ... R    "123213213" ~ "31283712710381299823"
    public static int superPalindromesInRange(String left, String right) {
        long l = Long.parseLong(left);
        long r = Long.parseLong(right);
        // 限制是根据开方的范围
        long limit = (long) Math.sqrt((double) r);
        int cnt = 0;
        long seed = 1;
        long enlarge = 0;
        do {
            // seed = 123
            // 123321
            enlarge = enlarge2(seed);
            if (isValid(enlarge * enlarge, l, r)) {
                cnt++;
            }
            // seed = 123
            // 12321
            enlarge = enlarge1(seed);
            if (isValid(enlarge * enlarge, l, r)) {
                cnt++;
            }
            seed++;
        } while (enlarge < limit);
        return cnt;
    }

    // 123 -> 12321
    public static long enlarge1(long seed) {
        long ans = seed;
        seed /= 10;
        while (seed != 0) {
            ans = ans * 10 + seed % 10;
            seed /= 10;
        }
        return ans;
    }

    // 123 -> 123321
    public static long enlarge2(long seed) {
        long ans = seed;
        while (seed != 0) {
            ans = ans * 10 + seed % 10;
            seed /= 10;
        }
        return ans;
    }

    public static boolean isValid(long ans, long l, long r) {
        return isPalindrome(ans) && ans >= l && ans <= r;
    }

    public static boolean isPalindrome(long n) {
        // n =    3721837
        // help = 1000000
        long help = 1;
        while (n / help >= 10) {
            help *= 10;
        }
        // n =    3  72183   7
        // help = 1000000
        // 左 : n / help = 3
        // 右 : n % 10 = 7
        while (n != 0) {
            // 最左位置和最后位置不相同
            // 直接结束
            if (n / help != n % 10) {
                return false;
            }
            n = (n % help) / 10;
            help /= 100;
        }
        return true;
    }

    // 测试链接 : https://leetcode.cn/problems/recover-a-tree-from-preorder-traversal/
    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode() {
        }

        TreeNode(int val) {
            this.val = val;
        }

        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }

    public static int max = 2001;
    public static int[] queue = new int[max];
    public static int l, r;

    public TreeNode recoverFromPreorder(String traversal) {
        l = 0;
        r = 0;
        int number = 0;
        int level = 0;
        boolean pickLevel = true;
        for (int i = 0; i < traversal.length(); i++) {
            if (traversal.charAt(i) != '-') {
                if (pickLevel) {
                    queue[r++] = level;
                    level = 0;
                    pickLevel = false;
                }
                number = number * 10 + traversal.charAt(i) - '0';
            } else {
                if (!pickLevel) {
                    queue[r++] = number;
                    number = 0;
                    pickLevel = true;
                }
                level++;
            }
        }
        queue[r++] = number;
        return f();
    }

    public static TreeNode f() {
        int level = queue[l++];
        TreeNode head = new TreeNode(queue[l++]);
        if (l < r && queue[l] > level) {
            head.left = f();
        }
        if (l < r && queue[l] > level) {
            head.right = f();
        }
        return head;
    }

    // 来自谷歌
    // 给定一个数组arr，长度为n
    // 表示n个服务员，每个人服务一个人的时间
    // 给定一个正数m，表示有m个人等位
    // 如果你是刚来的人，请问你需要等多久？
    // 假设：m远远大于n，比如n<=1000, m <= 10的9次方，该怎么做？
    public static int minWaitingTime1(int[] arr, int m) {
        if (arr == null || arr.length == 0) {
            return -1;
        }
        PriorityQueue<int[]> heap = new PriorityQueue<>((a, b) -> (a[0] - b[0]));
        int n = arr.length;
        for (int i = 0; i < n; i++) {
            heap.add(new int[]{0, arr[i]});
        }
        for (int i = 0; i < m; i++) {
            int[] cur = heap.poll();
            cur[0] += cur[1];
            heap.add(cur);
        }
        return heap.peek()[0];
    }

    public static int minWaitingTime2(int[] arr, int m) {
        if (arr == null || arr.length == 0) {
            return -1;
        }
        int best = Integer.MAX_VALUE;
        for (int num : arr) {
            best = Math.min(best, num);
        }
        int left = 0;
        int right = best * m;
        int mid = 0;
        int near = 0;
        while (left <= right) {
            mid = (left + right) / 2;
            int cover = 0;
            for (int num : arr) {
                cover += (mid / num) + 1;
            }
            if (cover >= m + 1) {
                near = mid;
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }
        return near;
    }

    // 给定两个数组A和B，长度都是N
    // A[i]不可以在A中和其他数交换，只可以选择和B[i]交换(0<=i<n)
    // 你的目的是让A有序，返回你能不能做到
    public static boolean letASorted1(int[] A, int[] B) {
        return process1(A, B, 0, Integer.MIN_VALUE);
    }

    // 当前推进到了i位置，对于A和B都是i位置
    // A[index]前一个数字，lastA
    // 能否通过题意中的操作，A[index] B[index] 让A整体有序
    public static boolean process1(int[] A, int[] B, int index, int lastA) {
        if (index == A.length) {
            return true;
        }
        // 第一种选择 : A[index]不和B[index]交换
        if (A[index] >= lastA && process1(A, B, index + 1, A[index])) {
            return true;
        }
        // 第二种选择 : A[index]和B[index]交换
        if (B[index] >= lastA && process1(A, B, index + 1, B[index])) {
            return true;
        }
        return false;
    }

    public static boolean letASorted2(int[] A, int[] B) {
        return process2(A, B, 1, true) || process2(A, B, 1, false);
    }

    // 当前推进到了i位置，对于A和B都是i位置
    // A[index]前一个数字是否来自A ：
    // 如果来自A，fromA = true；如果来自B，fromA = false；
    // 能否通过题意中的操作，A[index] B[index] 让A整体有序
    // 好处：可变参数成了int + boolean，时间复杂度可以做到O(N)
    public static boolean process2(int[] A, int[] B, int index, boolean fromA) {
        if (index == A.length) {
            return true;
        }
        if (index == 0 ||
                (A[index] >= (fromA ? A[index - 1] : B[index - 1])) && process2(A, B, index + 1, true)) {
            return true;
        }
        if (index == 0 ||
                (B[index] >= (fromA ? A[index - 1] : B[index - 1])) && process2(A, B, index + 1, false)) {
            return true;
        }
        return false;
    }

    // 比如地点是7, 9, 14
    // 供暖点的位置: 1 3 4 5 13 15 17
    // 先看地点7
    // 由1供暖，半径是6
    // 由3供暖，半径是4
    // 由4供暖，半径是3
    // 由5供暖，半径是2
    // 由13供暖，半径是6
    // 由此可知，地点7应该由供暖点5来供暖，半径是2
    // 再看地点9
    // 供暖点不回退
    // 由5供暖，半径是4
    // 由13供暖，半径是4
    // 由15供暖，半径是6
    // 由此可知，地点9应该由供暖点13来供暖，半径是4
    // 为什么是13而不是5？因为接下来的地点都会更靠右，所以半径一样的时候，就应该选更右的供暖点
    // 再看地点14
    // 供暖点不回退
    // 由13供暖，半径是1
    // 由15供暖，半径是1
    // 由17供暖，半径是3
    // 由此可知，地点14应该由供暖点15来供暖，半径是1
    // 以此类推
    public static int findRadius(int[] houses, int[] heaters) {
        Arrays.sort(houses);
        Arrays.sort(heaters);
        int ans = 0;
        // 时间复杂度O(N)
        // i是地点，j是供暖点
        for (int i = 0, j = 0; i < houses.length; i++) {
            while (!best(houses, heaters, i, j)) {
                j++;
            }
            ans = Math.max(ans, Math.abs(heaters[j] - houses[i]));
        }
        return ans;
    }

    // 这个函数含义：
    // 当前的地点houses[i]由heaters[j]来供暖是最优的吗？
    // 当前的地点houses[i]由heaters[j]来供暖，产生的半径是a
    // 当前的地点houses[i]由heaters[j + 1]来供暖，产生的半径是b
    // 如果a < b, 说明是最优，供暖不应该跳下一个位置
    // 如果a >= b, 说明不是最优，应该跳下一个位置
    public static boolean best(int[] houses, int[] heaters, int i, int j) {
        return j == heaters.length - 1
                || Math.abs(heaters[j] - houses[i]) < Math.abs(heaters[j + 1] - houses[i]);
    }

    // 在第 1天，有一个人发现了一个秘密。
    // 给你一个整数delay，表示每个人会在发现秘密后的 delay天之后，
    // 每天给一个新的人分享秘密。
    // 同时给你一个整数forget，表示每个人在发现秘密forget天之后会忘记这个秘密。
    // 一个人不能在忘记秘密那一天及之后的日子里分享秘密。
    // 给你一个整数n，请你返回在第 n天结束时，知道秘密的人数。
    // 由于答案可能会很大，请你将结果对109 + 7取余后返回。
    // 测试链接 : https://leetcode.cn/problems/number-of-people-aware-of-a-secret/
    public static int peopleAwareOfSecret(int n, int delay, int forget) {
        long mod = 1000000007;
        // dpKnow[i], 第i天知道秘密的人
        long[] dpKnow = new long[n + 1];
        // dpForget[i], 第i天将要忘记秘密的人
        long[] dpForget = new long[n + 1];
        // dpShare[i], 第i天可以分享秘密的人
        long[] dpShare = new long[n + 1];
        // 第1天的时候，知道秘密的人1个，A
        // 第1天的时候，将要忘记秘密的人0个
        // 第1天的时候，可以分享秘密的人0个
        dpKnow[1] = 1;
        if (1 + forget <= n) {
            dpForget[1 + forget] = 1;
        }
        if (1 + delay <= n) {
            dpShare[1 + delay] = 1;
        }
        // 从第2天开始！i
        for (int i = 2; i <= n; i++) {
            // 第i天
            // dpKnow[i - 1] - dpForget[i] + dpShare[i]
            dpKnow[i] = (mod + dpKnow[i - 1] - dpForget[i] + dpShare[i]) % mod;
            if (i + forget <= n) {
                // dpShare[i] 是第i天，刚知道秘密的人！
                // 这批人，会在i + forget天，都忘了!
                dpForget[i + forget] = dpShare[i];
            }
            if (i + delay <= n) {
                // dpShare[i + delay - 1] + dpShare[i] - dpForget[i + delay]
                // i + delay 天 , 100天后，会分享秘密的人
                // 第i天，有一些新人，i + delay天分享，一部分, dpShare[i]
                // 第二部分呢？i + delay - 1天，知道秘密并且会散播的人，- dpForget[i + delay]
                dpShare[i + delay] = (mod + dpShare[i + delay - 1] - dpForget[i + delay] + dpShare[i]) % mod;
            }
        }
        return (int) dpKnow[n];
    }

    // 测试链接 : https://leetcode.com/problems/guess-number-higher-or-lower-ii/
    // 正确的数字，在1~n之间
    // 每次猜错，花费就是你猜的数字
    // 返回：永远最倒霉的情况下，也能赢的胜利，所需要的最少钱数
    public static int minGold(int n) {
        return zuo(1, n);
    }

    // 目前锁定了，正确的数字，在L~R范围上，除了这个范围都不可能了！
    // 返回，永远最倒霉的情况下，猜中这个数字，所需要的最少钱数
    public static int zuo(int L, int R) {
        if (L == R) {
            return 0;
        }
        if (L == R - 1) {
            return L;
        }
        int min = Math.min(L + zuo(L + 1, R), R + zuo(L, R - 1));
        for (int i = L + 1; i < R; i++) {
            min = Math.min(min, i + Math.max(zuo(L, i - 1), zuo(i + 1, R)));
        }
        return min;
    }

    public static int minGold2(int n) {
        // L -> 1~n
        // R -> 1~n
        int[][] dp = new int[n + 1][n + 1];
        // 因为初始化都是0，所以dp的对角线，不用填了
        for (int i = 1; i < n; i++) {
            dp[i][i + 1] = i;
        }
        for (int L = n - 2; L >= 1; L--) {
            for (int R = L + 2; R <= n; R++) {
                int min = Math.min(L + dp[L + 1][R], R + dp[L][R - 1]);
                for (int i = L + 1; i < R; i++) {
                    min = Math.min(min, i + Math.max(dp[L][i - 1], dp[i + 1][R]));
                }
                dp[L][R] = min;
            }
        }
        return dp[1][n];
    }

    // 限制：0 <= start <= end，0 <= target <= 64
    // [start,end]范围上的数字，有多少数字二进制中1的个数等于target
    public static long nums2(long start, long end, int target) {
        if (start < 0 || end < 0 || start > end || target < 0) {
            return -1;
        }
        if (start == 0 && end == 0) {
            return target == 0 ? 1 : 0;
        }
        // 寻找end这数，最高位的1在哪？
        int eHigh = 62;
        while ((end & (1L << eHigh)) == 0) {
            eHigh--;
        }
        if (start == 0) {
            return process2(eHigh, 0, target, end);
        } else { // 170 ~ 3657 0 ~ 169 0 ~ 3657
            start--;
            int sHigh = 62;
            while (sHigh >= 0 && (start & (1L << sHigh)) == 0) {
                sHigh--;
            }
            return process2(eHigh, 0, target, end) - process2(sHigh, 0, target, start);
        }
    }

    public static long process2(int index, int less, int rest, long num) {
        if (rest > index + 1) {
            return 0;
        }
        // rest <= index + 1
        if (rest == 0) {
            return 1L;
        }
        // 0 < rest <= index + 1
        // 还有1需要去消耗
        // 位数也够用
        if (less == 1) { // less == 1 之前做的决定 已经小于 num所对应的前缀状态了
            if (rest == index + 1) {
                return 1;
            } else {
                // 后面剩余的位数 > 需要消耗掉1的数量的！
                // 某些位置填1，某些位置填0
                // index 0 1
                // index 0
                // process2(index - 1, 1, rest, num );
                // index 1
                // process2(index - 1, 1, rest - 1, num);
                return process2(index - 1, 1, rest - 1, num) + process2(index - 1, 1, rest, num);
            }
        } else { // less == 0, 之前做的决定 等于 num所对应的前缀状态的
            if (rest == index + 1) { // 后面剩余的位数，必须都填1，才能消耗完
                // index
                // 1 1 1 1 1 1 1 1 1
                // num
                // 1
                // index 1
                //
                // num 111111111 1
                return (num & (1L << index)) == 0 ? 0 : process2(index - 1, 0, rest - 1, num);
            } else {
                // less == 0, 之前做的决定 等于 num所对应的前缀状态的
                // 后面剩余的位数 > 需要消耗掉1的数量的！
                // 某些位置填1，某些位置填0
                if ((num & (1L << index)) == 0) {
                    return process2(index - 1, 0, rest, num);
                } else { // num 当前位置 1
                    // index 1
                    // index 0
                    return process2(index - 1, 0, rest - 1, num) + process2(index - 1, 1, rest, num);
                }
            }
        }
    }


    //
    // Dijkstra的解
    // n是城市数量
    // 城市编号：1 ~ n    0弃而不用
    public static int minDistance2(int n, int[][] roads, int x, int y) {
        // 第一步生成邻接矩阵
        int[][] map = new int[n + 1][n + 1];
        for (int i = 0; i <= n; i++) {
            for (int j = 0; j <= n; j++) {
                map[i][j] = Integer.MAX_VALUE;
            }
        }
        // 建立路
        for (int[] road : roads) {
            map[road[0]][road[1]] = Math.min(map[road[0]][road[1]], road[2]);
            map[road[1]][road[0]] = Math.min(map[road[1]][road[0]], road[2]);
        }
        // computed[i] = true，表示从源出发点到i这个城市，已经计算出最短距离了
        // computed[i] = false，表示从源出发点到i这个城市，还没有计算出最短距离
        boolean[] computed = new boolean[n + 1];
        // 距离小根堆
        PriorityQueue<Node> heap = new PriorityQueue<>((a, b) -> (a.pathSum - b.pathSum));
        heap.add(new Node(x, 0));
        while (!heap.isEmpty()) {
            // x -> ... -> 当前的城市, 有距离
            Node cur = heap.poll();
            if (computed[cur.city]) {
                continue;
            }
            // 没算过
            // 开始算！
            if (cur.city == y) {
                return cur.pathSum;
            }
            computed[cur.city] = true;
            for (int next = 1; next <= n; next++) {
                if (next != cur.city && map[cur.city][next] != Integer.MAX_VALUE && !computed[next]) {
                    heap.add(new Node(next, cur.pathSum + map[cur.city][next]));
                }
            }
        }
        return Integer.MAX_VALUE;
    }

    // 当前来到的Node，注意这不是城市的意思，这是就是一个普通的封装类
    // Node封装了，当前来到的城市是什么，以及，从源出发点到这个城市的路径和是多少？
    public static class Node {
        // 当前来到的城市编号
        public int city;
        // 从源出发点到这个城市的路径和
        public int pathSum;

        public Node(int c, int p) {
            city = c;
            pathSum = p;
        }
    }

    // 开始屏幕上什么也没有，粘贴板里什么也没有，
    // 你只能在键盘上做如下4种操作中的1种:
    // 输入∶在屏幕上已经显示内容的后面加一个A
    // 全选∶把屏幕上已经显示的全部内容选中
    // 复制︰被选中的内容复制进粘贴板
    // 粘贴︰在屏幕上已经显示内容的后面添加粘贴板里的内容
    // 给定一个正数n，表示你能操作的步数.
    // 返回n步内你能让最多多少个A显示在屏幕上

    // 可以证明：
    // 来到i的时候，包括i在内最多有连续4次粘贴行为
    // 不可能更多，如果有连续5次粘贴，一定就不再是最优解
    // 假设开始时，A的数量为S，看如下的变化过程，我们称这是行为一：
    // 开始  全选  复制(粘贴板S个A)  粘贴  粘贴  粘贴  粘贴  粘贴
    // S      S         S         2*S  3*S   4*S  5*S  6*S
    // 但是，注意看如下的行为二：
    // 开始  全选  复制(粘贴板S个A)  粘贴  全选  复制(粘贴板2S个A) 粘贴   粘贴
    // S      S         S         2*S  2*S        2*S        4*S   6*S
    // 行为一，经历8步，最后是6*S个A
    // 行为二，经历8步，最后是6*S个A
    // 但是行为二在粘贴板上有2S个A，而行为一在粘贴板上有S个A
    // 所以行为一没有行为二优
    // 以此说明：来到i的时候，包括i在内最多有连续4次粘贴行为
    // 那么就尝试：连续1次、连续2次、连续3次、连续4次粘贴行为即可
    public static int maxA(int n) {
        // dp[0] 1步以内的最优解
        // dp[1] 2步以内的最优解
        // dp[2] 3步以内的最优解
        // dp[i] i+1步以内的最优解
        int[] dp = new int[n];
        for (int i = 0; i < 6 && i < n; i++) {
            dp[i] = i + 1;
        }
        for (int i = 6; i < n; i++) {
            dp[i] = Math.max(
                    Math.max(dp[i - 3] * 2, dp[i - 4] * 3),
                    Math.max(dp[i - 5] * 4, dp[i - 6] * 5));
        }
        return dp[n - 1];
    }

    // 来自美团
    // 所有黑洞的中心点记录在holes数组里
    // 比如[[3,5] [6,9]]表示，第一个黑洞在(3,5)，第二个黑洞在(6,9)
    // 并且所有黑洞的中心点都在左下角(0,0)，右上角(x,y)的区域里
    // 飞船一旦开始进入黑洞，就会被吸进黑洞里
    // 返回：
    // 如果统一所有黑洞的半径，最大半径是多少，依然能保证飞船从(0,0)能到达(x,y)
    // 1000  1000*1000  10^6 * 二分
    public static class AwayFromBlackHole {

        public static int maxRadius(int[][] holes, int x, int y) {
            int L = 1;
            int R = Math.max(x, y);
            int ans = 0;
            while (L <= R) {
                int M = (L + R) / 2;
                if (ok(holes, M, x, y)) {
                    ans = M;
                    L = M + 1;
                } else {
                    R = M - 1;
                }
            }
            return ans;
        }

        public static boolean ok(int[][] holes, int r, int x, int y) {
            int n = holes.length;
            UnionFind uf = new UnionFind(holes, n, r);
            for (int i = 0; i < n; i++) {
                for (int j = i; j < n; j++) {
                    if (touch(holes[i][0], holes[i][1], holes[j][0], holes[j][1], r)) {
                        uf.union(i, j);
                    }
                    if (uf.block(i, x, y)) {
                        return false;
                    }
                }
            }
            return true;
        }

        public static boolean touch(int x1, int y1, int x2, int y2, int r) {
            return (r << 1) >= Math.sqrt((Math.pow(Math.abs(x1 - x2), 2) + Math.pow(Math.abs(y1 - y2), 2)));
        }

        public static class UnionFind {
            public int[] father;
            public int[] size;
            public int[] xMin;
            public int[] xMax;
            public int[] yMin;
            public int[] yMax;
            public int[] help;

            public UnionFind(int[][] holes, int n, int r) {
                father = new int[n];
                size = new int[n];
                xMin = new int[n];
                xMax = new int[n];
                yMin = new int[n];
                yMax = new int[n];
                help = new int[n];
                for (int i = 0; i < n; i++) {
                    father[i] = i;
                    size[i] = 1;
                    xMin[i] = holes[i][0] - r;
                    xMax[i] = holes[i][0] + r;
                    yMin[i] = holes[i][1] - r;
                    yMax[i] = holes[i][1] + r;
                }
            }

            private int find(int i) {
                int hi = 0;
                while (i != father[i]) {
                    help[hi++] = i;
                    i = father[i];
                }
                for (hi--; hi >= 0; hi--) {
                    father[help[hi]] = i;
                }
                return i;
            }

            public void union(int i, int j) {
                int fatheri = find(i);
                int fatherj = find(j);
                if (fatheri != fatherj) {
                    int sizei = size[fatheri];
                    int sizej = size[fatherj];
                    int big = sizei >= sizej ? fatheri : fatherj;
                    int small = big == fatheri ? fatherj : fatheri;
                    father[small] = big;
                    size[big] = sizei + sizej;
                    xMin[big] = Math.min(xMin[big], xMin[small]);
                    xMax[big] = Math.max(xMax[big], xMax[small]);
                    yMin[big] = Math.min(yMin[big], yMin[small]);
                    yMax[big] = Math.max(yMax[big], yMax[small]);
                }
            }

            public boolean block(int i, int x, int y) {
                i = find(i);
                return (xMin[i] <= 0 && xMax[i] >= x)
                        || (yMin[i] <= 0 && yMax[i] >= y)
                        || (xMin[i] <= 0 && yMin[i] <= 0)
                        || (xMax[i] >= x && yMax[i] >= y);
            }
        }
    }

    // arr数组长度为n, magic数组长度为m
    // 含义：
    // arr = { 3, 1, 4, 5, 7 }
    // 如果完全不改变arr中的值，那么收益就是累加和 = 3 + 1 + 4 + 5 + 7 = 20
    // magics = {
    //      {0,2,5} 表示arr[0~2]中的任何一个值，都能改成5
    //      {3,4,3} 表示arr[3~4]中的任何一个值，都能改成3
    //      {1,3,7} 表示arr[1~3]中的任何一个值，都能改成7
    // }
    // 就是说，magics中的任何一组数据{a,b,c}，都表示一种操作，
    // 你可以选择arr[a~b]中任何一个数字，变成c。
    // 并且每一种操作，都可以执行任意次
    // 其中 0 <= a <= b < n
    // 那么经过若干次的魔法操作，你当然可能得到arr的更大的累加和
    // 返回arr尽可能大的累加和
    // n <= 10^7
    // m <= 10^6
    // arr中的值和c的范围 <= 10^12
    public static class MagicSum {
        // O(N) + O(M * logM) + O(M * logN) + O(N * logN)
        public static int maxSum(int[] arr, int[][] magics) {
            int n = arr.length;
            // 线段树里的下标，从1开始，不从0开始！
            SegmentTree st = new SegmentTree(n);
            Arrays.sort(magics, (a, b) -> (a[2] - b[2]));
            for (int[] magic : magics) {
                st.update(magic[0] + 1, magic[1] + 1, magic[2], 1, n, 1);
            }
            int ans = 0;
            for (int i = 0; i < n; i++) {
                ans += Math.max(st.query(i + 1, i + 1, 1, n, 1), arr[i]);
            }
            return ans;
        }

        // 这是一棵普通的线段树
        // 区间上维持最大值的线段树
        // 支持区间值更新
        // 支持区间最大值查询
        public static class SegmentTree {
            private int[] max;
            private int[] change;
            private boolean[] update;

            public SegmentTree(int size) {
                int N = size + 1;
                max = new int[N << 2];
                change = new int[N << 2];
                update = new boolean[N << 2];
            }

            private void pushUp(int rt) {
                max[rt] = Math.max(max[rt << 1], max[rt << 1 | 1]);
            }

            private void pushDown(int rt, int ln, int rn) {
                if (update[rt]) {
                    update[rt << 1] = true;
                    update[rt << 1 | 1] = true;
                    change[rt << 1] = change[rt];
                    change[rt << 1 | 1] = change[rt];
                    max[rt << 1] = change[rt];
                    max[rt << 1 | 1] = change[rt];
                    update[rt] = false;
                }
            }

            public void update(int L, int R, int C, int l, int r, int rt) {
                if (L <= l && r <= R) {
                    update[rt] = true;
                    change[rt] = C;
                    max[rt] = C;
                    return;
                }
                int mid = (l + r) >> 1;
                pushDown(rt, mid - l + 1, r - mid);
                if (L <= mid) {
                    update(L, R, C, l, mid, rt << 1);
                }
                if (R > mid) {
                    update(L, R, C, mid + 1, r, rt << 1 | 1);
                }
                pushUp(rt);
            }

            public int query(int L, int R, int l, int r, int rt) {
                if (L <= l && r <= R) {
                    return max[rt];
                }
                int mid = (l + r) >> 1;
                pushDown(rt, mid - l + 1, r - mid);
                int left = 0;
                int right = 0;
                if (L <= mid) {
                    left = query(L, R, l, mid, rt << 1);
                }
                if (R > mid) {
                    right = query(L, R, mid + 1, r, rt << 1 | 1);
                }
                return Math.max(left, right);
            }

        }
    }

    // 测试链接 : https://leetcode.com/problems/lowest-common-ancestor-of-a-binary-tree-iv/
    public static class LowestCommonAncestorOfABinaryTreeIV {

        public static class TreeNode {
            public int val;
            public TreeNode left;
            public TreeNode right;
        }

        public TreeNode lowestCommonAncestor(TreeNode root, TreeNode[] nodes) {
            HashSet<Integer> set = new HashSet<>();
            for (TreeNode node : nodes) {
                set.add(node.val);
            }
            return process(root, set, set.size()).find;
        }

        public static class Info {
            // 找没找到最低公共祖先
            // 没找到，find = null
            // 找到了最低公共祖先，find是最低公共祖先
            public TreeNode find;
            // 我这颗子树上，删掉了几个节点！
            public int removes;

            public Info(TreeNode f, int r) {
                find = f;
                removes = r;
            }
        }

        public static Info process(TreeNode x, HashSet<Integer> set, int all) {
            if (x == null) {
                return new Info(null, 0);
            }
            Info left = process(x.left, set, all);
            if (left.find != null) {
                return left;
            }
            Info right = process(x.right, set, all);
            if (right.find != null) {
                return right;
            }
            int cur = set.contains(x.val) ? 1 : 0;
            set.remove(x.val);
            if (left.removes + right.removes + cur == all) {
                return new Info(x, all);
            } else {
                return new Info(null, left.removes + right.removes + cur);
            }
        }
    }

    // 来自CMU入学申请考试
    // 给定一个长度为 N 的字符串 S，由字符'a'和'b'组成，空隙由 '?' 表示
    // 你的任务是用a字符或b字符替换每个间隙，
    // 替换完成后想让连续出现同一种字符的最长子串尽可能短
    // 例如，S = "aa??bbb"，
    // 如果将"??"替换为"aa" ，即"aaaabbb"，则由相等字符组成的最长子串长度为4
    // 如果将"??"替换为"ba" ，即"aababbb"，则由相等字符组成的最长子串长度为3
    // 那么方案二是更好的结果，返回3
    // S的长度 <= 10^6
    public static int minContinuous(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }
        char[] str = s.toCharArray();
        int N = str.length;
        int L = 0;
        int R = -1;
        for (int i = 0; i < N; i++) {
            if (str[i] != '?') {
                set(str, L, R);
                L = i + 1;
                R = i;
            } else {
                R++;
            }
        }
        set(str, L, R);
        // 下面的for循环，是单独处理，条件5）
        for (int i = 1; i < N; i++) {
            if (str[i] == '?') {
                // baaaa?bbbbbbbba
                for (L = i - 1; L >= 0 && str[L] == str[i - 1]; L--)
                    ;
                for (R = i + 1; R < N && str[R] == str[i + 1]; R++)
                    ;
                L = i - L - 1;
                R = R - i - 1;
                if (L <= R) {
                    str[i] = str[i - 1];
                } else {
                    str[i] = str[i + 1];
                }
            }
        }
        return maxLen(str);
    }

    // L...R 都是？
    // 如果这一坨问号，满足1）2）3）4）中的一种，就填好
    // 如果满足5），就不填！a?b
    public static void set(char[] str, int L, int R) {
        int N = str.length;
        if (L > R) {
            return;
        }
        if (L == 0 && R == N - 1) {
            for (int i = 0; i < N; i++) {
                str[i] = (i & 1) == 0 ? 'a' : 'b';
            }
        } else if (L == 0) {
            for (int i = R; i >= 0; i--) {
                str[i] = str[i + 1] == 'a' ? 'b' : 'a';
            }
        } else if (R == N - 1) {
            for (int i = L; i < str.length; i++) {
                str[i] = str[i - 1] == 'a' ? 'b' : 'a';
            }
        } else {
            if (str[L - 1] == str[R + 1] || L != R) {
                for (; L <= R; L++, R--) {
                    str[L] = str[L - 1] == 'a' ? 'b' : 'a';
                    str[R] = str[R + 1] == 'a' ? 'b' : 'a';
                }
            }
        }
    }

    public static int maxLen(char[] str) {
        int ans = 1;
        int cur = 1;
        for (int i = 1; i < str.length; i++) {
            if (str[i] != str[i - 1]) {
                ans = Math.max(ans, cur);
                cur = 1;
            } else {
                cur++;
            }
        }
        ans = Math.max(ans, cur);
        return ans;
    }

    // 测试链接 : https://leetcode.com/problems/cut-off-trees-for-golf-event/
    public static class CutOffTreesForGolfEvent {
        public static int MAXN = 51;
        public static int MAXM = 51;
        public static int LIMIT = MAXN * MAXM;
        public static int[][] f = new int[MAXN][MAXM];
        public static int[][] arr = new int[LIMIT][3];
        public static int[] move = new int[]{1, 0, -1, 0, 1};
        public static int n, m;

        // 为了快，手撸双端队列
        public static int[][] deque = new int[LIMIT][3];
        // 双端队列的头、尾、大小
        public static int l, r, size;

        // 初始化双端队列
        public static void buildDeque() {
            l = -1;
            r = -1;
            size = 0;
        }

        // 双端队列从头部弹出
        public static int[] pollFirst() {
            int[] ans = deque[l];
            if (l < LIMIT - 1) {
                l++;
            } else {
                l = 0;
            }
            size--;
            if (size == 0) {
                l = r = -1;
            }
            return ans;
        }

        // 双端队列从头部加入
        public static void offerFirst(int x, int y, int d) {
            if (l == -1) {
                deque[0][0] = x;
                deque[0][1] = y;
                deque[0][2] = d;
                l = r = 0;
            } else {
                int fill = l == 0 ? (LIMIT - 1) : (l - 1);
                deque[fill][0] = x;
                deque[fill][1] = y;
                deque[fill][2] = d;
                l = fill;
            }
            size++;
        }

        // 双端队列从尾部加入
        public static void offerLast(int x, int y, int d) {
            if (l == -1) {
                deque[0][0] = x;
                deque[0][1] = y;
                deque[0][2] = d;
                l = r = 0;
            } else {
                int fill = (r == LIMIT - 1) ? 0 : (r + 1);
                deque[fill][0] = x;
                deque[fill][1] = y;
                deque[fill][2] = d;
                r = fill;
            }
            size++;
        }

        public static int cutOffTree(List<List<Integer>> forest) {
            n = forest.size();
            m = forest.get(0).size();
            int cnt = 0;
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < m; j++) {
                    int value = forest.get(i).get(j);
                    f[i][j] = value > 0 ? 1 : 0;
                    if (value > 1) {
                        arr[cnt][0] = value;
                        arr[cnt][1] = i;
                        arr[cnt++][2] = j;
                    }
                }
            }
            Arrays.sort(arr, 0, cnt, (a, b) -> a[0] - b[0]);
            int ans = 0;
            for (int i = 0, x = 0, y = 0, block = 2; i < cnt; i++, block++) {
                int toX = arr[i][1];
                int toY = arr[i][2];
                int step = walk(x, y, toX, toY, block);
                if (step == -1) {
                    return -1;
                }
                ans += step;
                x = toX;
                y = toY;
            }
            return ans;
        }

        public static int walk(int a, int b, int c, int d, int block) {
            buildDeque();
            offerFirst(a, b, 0);
            while (size > 0) {
                int[] cur = pollFirst();
                int x = cur[0];
                int y = cur[1];
                int distance = cur[2];
                if (f[x][y] != block) {
                    f[x][y] = block;
                    if (x == c && y == d) {
                        return distance;
                    }
                    for (int i = 1; i < 5; i++) {
                        int nextX = x + move[i];
                        int nextY = y + move[i - 1];
                        if (nextX >= 0 && nextX < n && nextY >= 0 && nextY < m && f[nextX][nextY] != 0
                                && f[nextX][nextY] != block) {
                            if ((i == 1 && y < d) || (i == 2 && x > c) || (i == 3 && y > d) || (i == 4 && x < c)) {
                                // 离的更近
                                offerFirst(nextX, nextY, distance + 1);
                            } else {
                                // 离的更远
                                offerLast(nextX, nextY, distance + 1);
                            }
                        }
                    }
                }
            }
            return -1;
        }
    }

    // 来自hulu
    // 有一个以原点为圆心，半径为1的圆
    // 在这个圆的圆周上，有一些点
    // 因为所有的点都在圆周上，所以每个点可以有很简练的表达
    // 比如：用0来表示一个圆周上的点，这个点就在(1,0)位置
    // 比如：用6000来表示一个点，这个点是(1,0)点沿着圆周逆时针转60.00度之后所在的位置
    // 比如：用18034来表示一个点，这个点是(1,0)点沿着圆周逆时针转180.34度之后所在的位置
    // 这样一来，所有的点都可以用[0, 36000)范围上的数字来表示
    // 那么任意三个点都可以组成一个三角形，返回能组成钝角三角形的数量
    public static long obtuseAngles(int[] arr) {
        // n长度的排序，O(N * logN)
        // O(N)
        int n = arr.length;
        int m = n << 1;
        int[] enlarge = new int[m];
        Arrays.sort(arr);
        for (int i = 0; i < n; i++) {
            enlarge[i] = arr[i];
            enlarge[i + n] = arr[i] + 36000;
        }
        long ans = 0;
        // 这里不用二分查找(太慢)，能做一个不回退的优化
        for (int L = 0, R = 0; L < n; L++) {
            while (R < m && enlarge[R] - enlarge[L] < 18000) {
                R++;
            }
            ans += num(R - L - 1);
        }
        return ans;
    }

    public static long num(long nodes) {
        return nodes < 2 ? 0 : ((nodes - 1) * nodes) >> 1;
    }

    // 整个二维平面算是一张地图，给定[x,y]，表示你站在x行y列
    // 你可以选择面朝的任何方向
    // 给定一个正数值angle，表示你视野的角度为
    // 这个角度内你可以看无穷远，这个角度外你看不到任何东西
    // 给定一批点的二维坐标，返回你在朝向最好的情况下，最多能看到几个点
    // 测试链接 : https://leetcode.com/problems/maximum-number-of-visible-points/
    public static int visiblePoints(List<List<Integer>> points, int angle, List<Integer> location) {
        int n = points.size();
        int a = location.get(0);
        int b = location.get(1);
        int zero = 0;
        double[] arr = new double[n << 1];
        int m = 0;
        for (int i = 0; i < n; i++) {
            int x = points.get(i).get(0) - a;
            int y = points.get(i).get(1) - b;
            if (x == 0 && y == 0) {
                zero++;
            } else {
                // 算出任意二维坐标点与x坐标轴之间的夹角
                arr[m] = Math.toDegrees(Math.atan2(y, x));
                arr[m + 1] = arr[m] + 360;
                m += 2;
            }
        }
        Arrays.sort(arr, 0, m);
        int max = 0;
        for (int L = 0, R = 0; L < n; L++) {
            while (R < m && arr[R] - arr[L] <= angle) {
                R++;
            }
            max = Math.max(max, R - L);
        }
        return max + zero;
    }

    // 有m个同样的苹果，认为苹果之间无差别
    // 有n个同样的盘子，认为盘子之间也无差别
    // 还有，比如5个苹果如果放进3个盘子，
    // 那么1、3、1和1、1、3和3、1、1的放置方法，也认为是一种方法
    // 如上的设定下，返回有多少种放置方法
    // 测试链接 : https://www.nowcoder.com/practice/bfd8234bb5e84be0b493656e390bdebf
    // 请同学们务必参考如下代码中关于输入、输出的处理
    // 这是输入输出处理效率很高的写法
    // 思路来自于分裂数问题
    // 体系学习班代码第22节，题目3，split number问题
    public static class SplitApples {
        public static int ways1(int apples, int plates) {
            return process1(1, apples, plates);
        }

        // pre : 上一个盘子分到的苹果数量，当前的盘子分到的数量不能小于pre
        // apples : 剩余的苹果数量
        // plates : 剩余的盘子数量
        // 在盘子够用的情况下，把苹果分完，有几种方法
        public static int process1(int pre, int apples, int plates) {
            if (apples == 0) {
                return 1;
            }
            // apples != 0
            if (plates == 0) {
                return 0;
            }
            // apples != 0 && plates != 0
            if (pre > apples) {
                return 0;
            }
            // apples != 0 && plates != 0 && pre <= apples
            int way = 0;
            // 之前的盘子分了3个苹果，现在还剩下8个苹果
            // 当前的盘子，可以装几个苹果：3、4、5、6、7、8
            for (int cur = pre; cur <= apples; cur++) {
                way += process1(cur, apples - cur, plates - 1);
            }
            return way;
        }

        // 新的尝试，最优解
        // 苹果有apples个，盘子有plates个
        // 返回有几种摆法
        // 如果苹果数为0，有1种摆法：什么也不摆
        // 如果苹果数不为0，但是盘子数为0，有0种摆法（做不到）
        // 如果苹果数不为0，盘子数也不为0，进行如下的情况讨论：
        // 假设苹果数为apples，盘子数为plates
        // 可能性 1) apples < plates
        // 这种情况下，一定有多余的盘子，这些盘子完全没用，所以砍掉
        // 后续是f(apples, apples)
        // 可能性 2) apples >= plates
        // 在可能性2)下，讨论摆法，有如下两种选择
        // 选择a) 不是所有的盘子都使用
        // 选择b) 就是所有的盘子都使用
        // 对于选择a)，既然不是所有盘子都使用，那么后续就是f(apples, plates - 1)
        // 意思是：既然不是所有盘子都使用，那盘子减少一个，然后继续讨论吧！
        // 对于选择b)，既然就是所有的盘子都使用，那么先把所有盘子都摆上1个苹果。
        // 剩余苹果数 = apples - plates
        // 然后继续讨论，剩下的这些苹果，怎么摆进plates个盘子里，
        // 所以后续是f(apples - plates, plates)
        public static int ways2(int apples, int plates) {
            if (apples == 0) {
                return 1;
            }
            if (plates == 0) {
                return 0;
            }
            if (plates > apples) {
                return ways2(apples, apples);
            } else { // apples >= plates;
                return ways2(apples, plates - 1) + ways2(apples - plates, plates);
            }
        }
    }

    // 来自hulu
    // 你只有1*1、1*2、1*3、1*4，四种规格的砖块
    // 你想铺满n行m列的区域，规则如下：
    // 1）不管那种规格的砖，都只能横着摆
    // 比如1*3这种规格的砖，3长度是水平方向，1长度是竖直方向
    // 2）会有很多方法铺满整个区域，整块区域哪怕有一点点不一样，就算不同的方法
    // 3）区域内部(不算区域整体的4条边界)，不能有任何砖块的边界线，是从上一直贯穿到下的直线
    // 返回符合三条规则下，铺满n行m列的区域，有多少种不同的摆放方法
    public static class WaysToBuildWall {

        public static long[] r = {0, 1, 2, 4, 8};

        public static long ways(int n, int m) {
            if (n <= 0 || m <= 1) {
                return 1;
            }
            // len[i] = 一共有1行的情况下，列的长度为i的时候有几种摆法(所有，不分合法和非法)
            long[] len = new long[m + 1];
            for (int i = 1; i <= Math.min(m, 4); i++) {
                len[i] = r[i];
            }
            for (int i = 5; i <= m; i++) {
                len[i] = len[i - 1] + len[i - 2] + len[i - 3] + len[i - 4];
            }
            // any[i] = 一共有n行的情况下，列的长度为i的时候有几种摆法(所有，不分合法和非法)
            long[] any = new long[m + 1];
            for (int i = 1; i <= m; i++) {
                // n * i的区域：总共的摆法！不区分合法、不合法
                any[i] = power(len[i], n);
            }
            // solid[i] = 一共有n行的情况下，列的长度为i的时候有几种合法的摆法
            long[] solid = new long[m + 1];
            solid[1] = 1;
            for (int i = 2; i <= m; i++) {
                long invalid = 0;
                // N * i
                // 1) （N * 1 合法） * （N * (i-1) 总共）
                // 2) （N * 2 合法） * （N * (i-2) 总共）
                // 3) （N * 3 合法） * （N * (i-3) 总共）
                //
                // j) （N * j 合法） * （N * (i-j) 总共）
                for (int j = 1; j < i; j++) {
                    invalid += solid[j] * any[i - j];
                }
                solid[i] = any[i] - invalid;
            }
            return solid[m];
        }

        public static long power(long base, int power) {
            long ans = 1;
            while (power != 0) {
                if ((power & 1) != 0) {
                    ans *= base;
                }
                base *= base;
                power >>= 1;
            }
            return ans;
        }
    }

    // 来自阿里
    // 给定一个只由'a'和'b'组成的字符串str，
    // str中"ab"和"ba"子串都可以消除，消除之后剩下字符会重新靠在一起，继续出现可以消除的子串...
    // 你的任务是决定一种消除的顺序，最后让str消除到尽可能的短
    // 返回尽可能的短的剩余字符串
    // 暴力尝试，尝试所有的可能性
    // 所有情况都枚举，所有先后消除的顺序都暴力尝试
    public static String disappear1(String str) {
        String ans = str;
        for (int i = 1; i < str.length(); i++) {
            // i == 1 0 1 是不是a和b都全，2...
            // i == 2 1 2 是不是a和b都全，03....
            // i == 3 2 3 是不是a和b都全，014....
            // (i-1 i)扣掉！
            boolean hasA = str.charAt(i - 1) == 'a' || str.charAt(i) == 'a';
            boolean hasB = str.charAt(i - 1) == 'b' || str.charAt(i) == 'b';
            if (hasA && hasB) {
                String curAns = disappear1(str.substring(0, i - 1) + str.substring(i + 1));
                if (curAns.length() < ans.length()) {
                    ans = curAns;
                }
            }
        }
        return ans;
    }

    // 好一点的方法尝试
    // 遇到第一个能消除的就消除，
    // 剩下的字符串继续：遇到第一个能消除的就消除
    // 剩下的字符串继续：遇到第一个能消除的就消除
    // ...
    public static String disappear2(String str) {
        String ans = str;
        for (int i = 1; i < str.length(); i++) {
            boolean hasA = str.charAt(i - 1) == 'a' || str.charAt(i) == 'a';
            boolean hasB = str.charAt(i - 1) == 'b' || str.charAt(i) == 'b';
            if (hasA && hasB) {
                return disappear2(str.substring(0, i - 1) + str.substring(i + 1));
            }
        }
        return ans;
    }

    // 时间复杂度O(N)
    // 利用栈
    public static String disappear3(String s) {
        char[] str = s.toCharArray();
        int n = str.length;
        // 用数组结构，自己实现栈
        int[] stack = new int[n];
        int size = 0;
        for (int i = 0; i < n; i++) {
            boolean hasA = size != 0 && str[stack[size - 1]] == 'a';
            boolean hasB = size != 0 && str[stack[size - 1]] == 'b';
            hasA |= str[i] == 'a';
            hasB |= str[i] == 'b';
            if (hasA && hasB) {
                size--;
            } else {
                stack[size++] = i;
            }
        }
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < size; i++) {
            builder.append(str[stack[i]]);
        }
        return builder.toString();
    }

    // 贪心，时间复杂度O(N)
    // A的个数如果比B的个数多，那么一定只剩下A
    // B的个数如果比A的个数多，那么一定只剩下B
    public static String disappear4(String s) {
        int count = 0;
        for (char cha : s.toCharArray()) {
            count += cha == 'a' ? 1 : -1;
        }
        StringBuilder builder = new StringBuilder();
        char rest = count > 0 ? 'a' : 'b';
        count = Math.abs(count);
        for (int i = 0; i < count; i++) {
            builder.append(rest);
        }
        return builder.toString();
    }

    // 测试链接 : https://leetcode.com/problems/maximum-score-from-performing-multiplication-operations/
    public static class MaximumScoreFromPerformingMultiplicationOperations {
        public static int maximumScore1(int[] A, int[] B) {
            if (A == null || A.length == 0 || B == null || B.length == 0 || A.length < B.length) {
                return 0;
            }
            return process1(A, B, 0, A.length - 1);
        }

        // B数组消耗完之前，A数组不会耗尽，题目输入保证的！
        // A[left...right]
        // B[0..i-1]已经消耗完了！B[i...m-1]
        // 直到把B数组消耗完，能获得的最大分数返回
        public static int process1(int[] A, int[] B, int L, int R) {
            int indexB = L + A.length - R - 1;
            if (indexB == B.length) {
                return 0;
            } else {
                int p1 = A[L] * B[indexB] + process1(A, B, L + 1, R);
                int p2 = A[R] * B[indexB] + process1(A, B, L, R - 1);
                return Math.max(p1, p2);
            }
        }

        public static int maximumScore2(int[] A, int[] B) {
            if (A == null || A.length == 0 || B == null || B.length == 0 || A.length < B.length) {
                return 0;
            }
            int N = A.length;
            int M = B.length;
            int[][] dp = new int[M + 1][M + 1];
            for (int L = M - 1; L >= 0; L--) {
                for (int j = L + 1; j <= M; j++) {
                    int R = N - M + j - 1;
                    int indexB = L + N - R - 1;
                    dp[L][j] = Math.max(A[L] * B[indexB] + dp[L + 1][j], A[R] * B[indexB] + dp[L][j - 1]);
                }
            }
            return dp[0][M];
        }
    }

    // 来自学员问题
    // 给定一个二维数组，其中全是非负数
    // 每一步都可以往上、下、左、右四个方向运动
    // 走过的路径，会沿途累加数字
    // 返回从左下角走到右下角的累加和最小的多少
    public static int bestWalk1(int[][] map) {
        int n = map.length;
        int m = map[0].length;
        int step = n * m - 1;
        return process(map, n, m, step, 0, 0, 0, 0);
    }

    public static int process(int[][] map, int n, int m, int limit, int step, int row, int col, int cost) {
        if (row < 0 || row == n || col < 0 || col == m || step > limit) {
            return Integer.MAX_VALUE;
        }
        if (row == n - 1 && col == m - 1) {
            return cost + map[row][col];
        }
        cost += map[row][col];
        int p1 = process(map, n, m, limit, step + 1, row - 1, col, cost);
        int p2 = process(map, n, m, limit, step + 1, row + 1, col, cost);
        int p3 = process(map, n, m, limit, step + 1, row, col - 1, cost);
        int p4 = process(map, n, m, limit, step + 1, row, col + 1, cost);
        return Math.min(Math.min(p1, p2), Math.min(p3, p4));
    }

    public static int bestWalk2(int[][] map) {
        int n = map.length;
        int m = map[0].length;
        // 堆
        // 每一个对象，都是一个小数组
        // {dis, row, col}
        //  0    1    2
        PriorityQueue<int[]> heap = new PriorityQueue<>((a, b) -> a[0] - b[0]);
        // X,0,1 已经弹出了！ 以后在遇到(0,1)的事情，不管！
        // poped记录哪些位置弹出，哪些没有，换句话说就是判断当前坐标是否已经被处理过！
        boolean[][] poped = new boolean[n][m];
        heap.add(new int[]{map[0][0], 0, 0});
        int ans = 0;
        while (!heap.isEmpty()) {
            int[] cur = heap.poll();
            int dis = cur[0];
            int row = cur[1];
            int col = cur[2];
            if (poped[row][col]) {
                continue;
            }
            // 接下来就是要处理这个位置了！
            poped[row][col] = true;
            if (row == n - 1 && col == m - 1) {
                ans = dis;
                break;
            }
            add(dis, row - 1, col, n, m, map, poped, heap);
            add(dis, row + 1, col, n, m, map, poped, heap);
            add(dis, row, col - 1, n, m, map, poped, heap);
            add(dis, row, col + 1, n, m, map, poped, heap);
        }
        return ans;
    }

    public static void add(int pre, int row, int col, int n, int m, int[][] map, boolean[][] used,
                           PriorityQueue<int[]> heap) {
        if (row >= 0 && row < n && col >= 0 && col < m && !used[row][col]) {
            heap.add(new int[]{pre + map[row][col], row, col});
        }
    }

    // 给定一个有序数组arr，从左到右依次表示X轴上从左往右点的位置
    // 给定一个正整数K，返回如果有一根长度为K的绳子，最多能盖住几个点
    // 绳子的边缘点碰到X轴上的点，也算盖住
    public static int maxPoint2(int[] arr, int L) {
        int left = 0;
        int right = 0;
        int N = arr.length;
        int max = 0;
        while (left < N) {
            while (right < N && arr[right] - arr[left] <= L) {
                right++;
            }
            max = Math.max(max, right - (left++));
        }
        return max;
    }

    // 一个数组中只有两种字符'G'和'B'，
    // 可以让所有的G都放在左侧，所有的B都放在右侧
    // 或者可以让所有的G都放在右侧，所有的B都放在左侧
    // 但是只能在相邻字符之间进行交换操作，请问请问至少需要交换几次，
    public static int minSteps2(String s) {
        if (s == null || s.equals("")) {
            return 0;
        }
        char[] str = s.toCharArray();
        int step1 = 0;
        int step2 = 0;
        int gi = 0;
        int bi = 0;
        for (int i = 0; i < str.length; i++) {
            if (str[i] == 'G') { // 当前的G，去左边   方案1
                step1 += i - (gi++);
            } else {// 当前的B，去左边   方案2
                step2 += i - (bi++);
            }
        }
        return Math.min(step1, step2);
    }

    // https://leetcode.cn/problems/target-sum/
    public static class TargetSum {

        public static int findTargetSumWays1(int[] arr, int target) {
            return process1(arr, 0, target);
        }

        // 可以自由使用arr[index....]所有的数字！
        // 搞出rest这个数，方法数是多少？返回
        public static int process1(int[] arr, int index, int target) {
            if (index == arr.length) { // 没数了！
                return target == 0 ? 1 : 0;
            }
            // 还有数！arr[index] arr[index+1 ... ]
            return process1(arr, index + 1, target - arr[index])
                    + process1(arr, index + 1, target + arr[index]);
        }

        public static int findTargetSumWays2(int[] arr, int target) {
            return process2(arr, 0, target, new HashMap<>());
        }

        // 傻缓存的结构举例
        // index == 7 rest = 13
        // map "7_13" 256
        public static int process2(int[] arr, int index, int target, HashMap<Integer, HashMap<Integer, Integer>> dp) {
            if (dp.containsKey(index) && dp.get(index).containsKey(target)) {
                return dp.get(index).get(target);
            }
            // 否则，没命中！
            int ans = 0;
            if (index == arr.length) {
                ans = target == 0 ? 1 : 0;
            } else {
                ans = process2(arr, index + 1, target - arr[index], dp)
                        + process2(arr, index + 1, target + arr[index], dp);
            }
            if (!dp.containsKey(index)) {
                dp.put(index, new HashMap<>());
            }
            dp.get(index).put(target, ans);
            return ans;
        }

        // 优化点一 :
        // 你可以认为arr中都是非负数
        // 因为即便是arr中有负数，比如[3,-4,2]
        // 因为你能在每个数前面用+或者-号
        // 所以[3,-4,2]其实和[3,4,2]达成一样的效果
        // 那么我们就全把arr变成非负数，不会影响结果的
        // 优化点二 :
        // 如果arr都是非负数，并且所有数的累加和是sum
        // 那么如果target>sum，很明显没有任何方法可以达到target，可以直接返回0
        // 优化点三 :
        // arr内部的数组，不管怎么+和-，最终的结果都一定不会改变奇偶性
        // 所以，如果所有数的累加和是sum，
        // 并且与target的奇偶性不一样，没有任何方法可以达到target，可以直接返回0
        // 优化点四 :
        // 比如说给定一个数组, arr = [1, 2, 3, 4, 5] 并且 target = 3
        // 其中一个方案是 : +1 -2 +3 -4 +5 = 3
        // 该方案中取了正的集合为P = {1，3，5}
        // 该方案中取了负的集合为N = {2，4}
        // 所以任何一种方案，都一定有 sum(P) - sum(N) = target
        // 现在我们来处理一下这个等式，把左右两边都加上sum(P) + sum(N)，那么就会变成如下：
        // sum(P) - sum(N) + sum(P) + sum(N) = target + sum(P) + sum(N)
        // 2 * sum(P) = target + 数组所有数的累加和
        // sum(P) = (target + 数组所有数的累加和) / 2
        // 也就是说，任何一个集合，只要累加和是(target + 数组所有数的累加和) / 2
        // 那么就一定对应一种target的方式
        // 也就是说，比如非负数组arr，target = 7, 而所有数累加和是11
        // 求有多少方法组成7，其实就是求有多少种达到累加和(7+11)/2=9的方法
        // 优化点五 :
        // 二维动态规划的空间压缩技巧
        public static int findTargetSumWays(int[] arr, int target) {
            int sum = 0;
            for (int n : arr) {
                sum += n;
            }
            return sum < target || ((target & 1) ^ (sum & 1)) != 0 ? 0 : subset2(arr, (target + sum) >> 1);
        }

        // 求非负数组nums有多少个子集，累加和是s
        // 二维动态规划
        // 不用空间压缩
        public static int subset1(int[] nums, int target) {
            if (target < 0) {
                return 0;
            }
            int n = nums.length;
            // dp[i][j] : nums前缀长度为i的所有子集，有多少累加和是j？
            int[][] dp = new int[n + 1][target + 1];
            // nums前缀长度为0的所有子集，有多少累加和是0？一个：空集
            dp[0][0] = 1;
            for (int i = 1; i <= n; i++) {
                for (int j = 0; j <= target; j++) {
                    dp[i][j] = dp[i - 1][j];
                    if (j - nums[i - 1] >= 0) {
                        dp[i][j] += dp[i - 1][j - nums[i - 1]];
                    }
                }
            }
            return dp[n][target];
        }

        // 求非负数组nums有多少个子集，累加和是s
        // 二维动态规划
        // 用空间压缩:
        // 核心就是for循环里面的：for (int i = s; i >= n; i--) {
        // 为啥不枚举所有可能的累加和？只枚举 n...s 这些累加和？
        // 因为如果 i - n < 0，dp[i]怎么更新？和上一步的dp[i]一样！所以不用更新
        // 如果 i - n >= 0，dp[i]怎么更新？上一步的dp[i] + 上一步dp[i - n]的值，这才需要更新
        public static int subset2(int[] nums, int target) {
            if (target < 0) {
                return 0;
            }
            int[] dp = new int[target + 1];
            dp[0] = 1;
            for (int n : nums) {
                for (int i = target; i >= n; i--) {
                    dp[i] += dp[i - n];
                }
            }
            return dp[target];
        }
    }

    // 司机调度时间限制:3000MS内存限制:589824KB 题目描述:
    // 正值下班高峰时期，现有可载客司机数2N人，调度中心将调度相关司机服务A、B两个出行高峰区域。
    // 第i个司机前往服务A区域可得收入为income[i][0，前往服务B区域可得收入为income[i][1]。
    // 返回将每位司机调度完成服务后，所有司机总可得的最高收入金额，要求每个区域都有N位司机服务。
    // 输入描述10 20 20 40#如上:第一个司机服务A区域，收入为10元第一个司机服务B区域，收入为20元
    // 第二个司机服务A区域，收入为20元第二个司机服务B区域，收入为40元 输入参数以‘#’结束输入
    // 输出描述最高总收入为10+ 40= 50，每个区域都有一半司机服务
    // 参数及相关数据异常请输出: error样例输入∶10 30 100 200 150 50 60 20#样例输出440
    public static class Drive {
        public static int maxMoney1(int[][] income) {
            if (income == null || income.length < 2 || (income.length & 1) != 0) {
                return 0;
            }
            int N = income.length; // 司机数量一定是偶数，所以才能平分，A N /2 B N/2
            int M = N >> 1; // M = N / 2 要去A区域的人
            return process1(income, 0, M);
        }

        // index.....所有的司机，往A和B区域分配！
        // A区域还有rest个名额!
        // 返回把index...司机，分配完，并且最终A和B区域同样多的情况下，index...这些司机，整体收入最大是多少！
        public static int process1(int[][] income, int index, int rest) {
            if (index == income.length) {
                return 0;
            }
            // 还剩下司机！
            if (income.length - index == rest) {
                return income[index][0] + process1(income, index + 1, rest - 1);
            }
            if (rest == 0) {
                return income[index][1] + process1(income, index + 1, rest);
            }
            // 当前司机，可以去A，或者去B
            int p1 = income[index][0] + process1(income, index + 1, rest - 1);
            int p2 = income[index][1] + process1(income, index + 1, rest);
            return Math.max(p1, p2);
        }

        // 严格位置依赖的动态规划版本
        public static int maxMoney2(int[][] income) {
            int N = income.length;
            int M = N >> 1;
            int[][] dp = new int[N + 1][M + 1];
            for (int i = N - 1; i >= 0; i--) {
                for (int j = 0; j <= M; j++) {
                    if (N - i == j) {
                        dp[i][j] = income[i][0] + dp[i + 1][j - 1];
                    } else if (j == 0) {
                        dp[i][j] = income[i][1] + dp[i + 1][j];
                    } else {
                        int p1 = income[i][0] + dp[i + 1][j - 1];
                        int p2 = income[i][1] + dp[i + 1][j];
                        dp[i][j] = Math.max(p1, p2);
                    }
                }
            }
            return dp[0][M];
        }
    }

    // 具有setAll功能的哈希表
    public static class SetAllHashMap {

        public static class MyValue<V> {
            public V value;
            public long time;

            public MyValue(V v, long t) {
                value = v;
                time = t;
            }
        }

        public static class MyHashMap<K, V> {
            private HashMap<K, MyValue<V>> map;
            private long time;
            private MyValue<V> setAll;

            public MyHashMap() {
                map = new HashMap<>();
                time = 0;
                setAll = new MyValue<V>(null, -1);
            }

            public void put(K key, V value) {
                map.put(key, new MyValue<V>(value, time++));
            }

            public void setAll(V value) {
                setAll = new MyValue<V>(value, time++);
            }

            public V get(K key) {
                if (!map.containsKey(key)) {
                    return null;
                }
                if (map.get(key).time > setAll.time) {
                    return map.get(key).value;
                } else {
                    return setAll.value;
                }
            }
        }
    }

    // 在一个字符串中找到没有重复字符子串中最长的长度。
    // 例如:
    // abcabcbb没有重复字符的最长子串是abc，长度为3
    // bbbbb，答案是b，长度为1
    // pwwkew，答案是wke，长度是3
    // 要求:答案必须是子串，"pwke" 是一个子字符序列但不是一个子字符串。
    // 本题测试链接 : https://leetcode.com/problems/longest-substring-without-repeating-characters/
    // 动态规划
    public static int longestNoRepeatSubString(String str) {
        if (str == null || str.length() == 0) {
            return 0;
        }
        char[] chars = str.toCharArray();
        int[] map = new int[256];
        Arrays.fill(map, -1);
        int len = 0;
        int pre = -1;
        int cur = 0;
        for (int i = 0; i != chars.length; i++) {
            // 选出离当前位置最近的隔离点
            pre = Math.max(pre, map[chars[i]]);
            // 记录当前字符到最近隔离点的长度
            cur = i - pre;
            // 比较长度大小
            len = Math.max(len, cur);
            // 记录当前字符出现的位置
            map[chars[i]] = i;
        }
        return len;
    }

    // 给定一个数组arr，代表每个人的能力值。再给定一个非负数k。
    // 如果两个人能力差值正好为k，那么可以凑在一起比赛，一局比赛只有两个人
    // 返回最多可以同时有多少场比赛
    public static class MaxPairNumber {

        // 暴力解
        public static int maxPairNum1(int[] arr, int k) {
            if (k < 0) {
                return -1;
            }
            return process1(arr, 0, k);
        }

        public static int process1(int[] arr, int index, int k) {
            int ans = 0;
            if (index == arr.length) {
                for (int i = 1; i < arr.length; i += 2) {
                    if (arr[i] - arr[i - 1] == k) {
                        ans++;
                    }
                }
            } else {
                for (int r = index; r < arr.length; r++) {
                    swap(arr, index, r);
                    ans = Math.max(ans, process1(arr, index + 1, k));
                    swap(arr, index, r);
                }
            }
            return ans;
        }

        public static void swap(int[] arr, int i, int j) {
            int tmp = arr[i];
            arr[i] = arr[j];
            arr[j] = tmp;
        }

        // 时间复杂度O(N*logN)
        public static int maxPairNum2(int[] arr, int k) {
            if (k < 0 || arr == null || arr.length < 2) {
                return 0;
            }
            Arrays.sort(arr);
            int ans = 0;
            int N = arr.length;
            int L = 0;
            int R = 0;
            boolean[] usedR = new boolean[N];
            while (L < N && R < N) {
                if (usedR[L]) {
                    L++;
                } else if (L >= R) {
                    R++;
                } else { // 不止一个数，而且都没用过！
                    int distance = arr[R] - arr[L];
                    if (distance == k) {
                        ans++;
                        usedR[R++] = true;
                        L++;
                    } else if (distance < k) {
                        R++;
                    } else {
                        L++;
                    }
                }
            }
            return ans;
        }
    }

    // 给定一个数组arr，长度为N且每个值都是正数，代表N个人的体重。
    // 再给定一个正数limit，代表一艘船的载重。
    // 以下是坐船规则，1）每艘船最多只能做两人；
    // 2）乘客的体重和不能超过limit。
    // 返回如果同时让这N个人过河最少需要几条船。
    public static int minBoat(int[] arr, int limit) {
        // arr排序
        if (arr == null || arr.length == 0) {
            return 0;
        }

        if (arr[arr.length - 1] <= (limit / 2)) {
            return (arr.length + 1) / 2;
        }

        if (arr[0] > (limit / 2)) {
            return arr.length;
        }

        int lessR = -1;
        for (int i = arr.length - 1; i >= 0; i--) {
            if (arr[i] <= (limit / 2)) {
                lessR = i;
                break;
            }
        }
        int l = lessR;
        int r = lessR + 1;
        int lessUnused = 0;
        while (l >= 0) {
            int solved = 0;
            while (r < arr.length && arr[l] + arr[r] <= limit) {
                r++;
                // 左边和右边能组队上船的个数
                solved++;
            }
            if (solved == 0) {
                // 左边无法上船的个数
                lessUnused++;
                l--;
            } else {
                l = Math.max(-1, l - solved);
            }
        }
        int lessAll = lessR + 1;
        // 右边和左边能组合上船的个数
        int lessUsed = lessAll - lessUnused;
        // 右边只能单独上船的个数
        int moreUnsolved = arr.length - lessR - 1 - lessUsed;
        return lessUsed + (lessUnused + 1) >> 1 + moreUnsolved;
    }

    // 数组为{3, 2, 2, 3, 1}，查询为(0, 3, 2)。意思是在数组里下标0~3这个范围上，有几个2？返回2。
    // 假设给你一个数组arr，对这个数组的查询非常频繁，请返回所有查询的结果
    public static class QueryBox2 {
        private HashMap<Integer, ArrayList<Integer>> map;

        public QueryBox2(int[] arr) {
            map = new HashMap<>();
            for (int i = 0; i < arr.length; i++) {
                if (!map.containsKey(arr[i])) {
                    map.put(arr[i], new ArrayList<>());
                }
                map.get(arr[i]).add(i);
            }
        }

        public int query(int L, int R, int value) {
            if (!map.containsKey(value)) {
                return 0;
            }
            ArrayList<Integer> indexArr = map.get(value);
            // 查询 < L 的下标有几个
            int a = countLess(indexArr, L);
            // 查询 < R+1 的下标有几个
            int b = countLess(indexArr, R + 1);
            return b - a;
        }

        // 在有序数组arr中，用二分的方法数出<limit的数有几个
        // 也就是用二分法，找到<limit的数中最右的位置
        private int countLess(ArrayList<Integer> arr, int limit) {
            int L = 0;
            int R = arr.size() - 1;
            int mostRight = -1;
            while (L <= R) {
                int mid = L + ((R - L) >> 1);
                if (arr.get(mid) < limit) {
                    mostRight = mid;
                    L = mid + 1;
                } else {
                    R = mid - 1;
                }
            }
            return mostRight + 1;
        }
    }

    // 一群孩子做游戏，现在请你根据游戏得分来发糖果，要求如下：
    // 1. 每个孩子不管得分多少，起码分到一个糖果。
    // 2. 任意两个相邻的孩子之间，得分较多的孩子必须拿多一些糖果。(若相同则无此限制)
    // 给定一个数组arr代表得分数组，请返回最少需要多少糖果。
    // [要求]
    // 时间复杂度为O(n), 空间复杂度为O(1)
    // 测试链接 : https://leetcode.com/problems/candy/
    public static class CandyProblem {
        // 这是原问题的优良解
        // 时间复杂度O(N)，额外空间复杂度O(N)
        public static int candy1(int[] arr) {
            if (arr == null || arr.length == 0) {
                return 0;
            }
            int N = arr.length;
            int[] left = new int[N];
            for (int i = 1; i < N; i++) {
                if (arr[i - 1] < arr[i]) {
                    left[i] = left[i - 1] + 1;
                }
            }
            int[] right = new int[N];
            for (int i = N - 2; i >= 0; i--) {
                if (arr[i] > arr[i + 1]) {
                    right[i] = right[i + 1] + 1;
                }
            }
            int ans = 0;
            for (int i = 0; i < N; i++) {
                ans += Math.max(left[i], right[i]);
            }
            return ans + N;
        }

        // 这是原问题空间优化后的解
        // 时间复杂度O(N)，额外空间复杂度O(1)
        public static int candy2(int[] arr) {
            if (arr == null || arr.length == 0) {
                return 0;
            }
            int index = nextMinIndex2(arr, 0);
            int res = rightCandies(arr, 0, index++);
            int lbase = 1;
            int next = 0;
            int rcands = 0;
            int rbase = 0;
            while (index != arr.length) {
                if (arr[index] > arr[index - 1]) {
                    res += ++lbase;
                    index++;
                } else if (arr[index] < arr[index - 1]) {
                    next = nextMinIndex2(arr, index - 1);
                    rcands = rightCandies(arr, index - 1, next++);
                    rbase = next - index + 1;
                    res += rcands + (rbase > lbase ? -lbase : -rbase);
                    lbase = 1;
                    index = next;
                } else {
                    res += 1;
                    lbase = 1;
                    index++;
                }
            }
            return res;
        }

        public static int nextMinIndex2(int[] arr, int start) {
            for (int i = start; i != arr.length - 1; i++) {
                if (arr[i] <= arr[i + 1]) {
                    return i;
                }
            }
            return arr.length - 1;
        }

        public static int rightCandies(int[] arr, int left, int right) {
            int n = right - left + 1;
            return n + n * (n - 1) / 2;
        }

        // 这是进阶问题的最优解，不要提交这个
        // 时间复杂度O(N), 额外空间复杂度O(1)
        public static int candy3(int[] arr) {
            if (arr == null || arr.length == 0) {
                return 0;
            }
            int index = nextMinIndex3(arr, 0);
            int[] data = rightCandiesAndBase(arr, 0, index++);
            int res = data[0];
            int lbase = 1;
            int same = 1;
            int next = 0;
            while (index != arr.length) {
                if (arr[index] > arr[index - 1]) {
                    res += ++lbase;
                    same = 1;
                    index++;
                } else if (arr[index] < arr[index - 1]) {
                    next = nextMinIndex3(arr, index - 1);
                    data = rightCandiesAndBase(arr, index - 1, next++);
                    if (data[1] <= lbase) {
                        res += data[0] - data[1];
                    } else {
                        res += -lbase * same + data[0] - data[1] + data[1] * same;
                    }
                    index = next;
                    lbase = 1;
                    same = 1;
                } else {
                    res += lbase;
                    same++;
                    index++;
                }
            }
            return res;
        }

        public static int nextMinIndex3(int[] arr, int start) {
            for (int i = start; i != arr.length - 1; i++) {
                if (arr[i] < arr[i + 1]) {
                    return i;
                }
            }
            return arr.length - 1;
        }

        public static int[] rightCandiesAndBase(int[] arr, int left, int right) {
            int base = 1;
            int cands = 1;
            for (int i = right - 1; i >= left; i--) {
                if (arr[i] == arr[i + 1]) {
                    cands += base;
                } else {
                    cands += ++base;
                }
            }
            return new int[]{cands, base};
        }

        public static void test() {
            int[] test1 = {3, 0, 5, 5, 4, 4, 0};
            System.out.println(candy2(test1));

            int[] test2 = {3, 0, 5, 5, 4, 4, 0};
            System.out.println(candy3(test2));
        }
    }

    // 生成长度为size的达标数组
    // 达标：对于任意的 i<k<j，满足 [i] + [j] != [k] * 2
    public static int[] makeNo(int size) {
        if (size == 1) {
            return new int[]{1};
        }
        // size
        // 一半长达标来
        // 7 : 4
        // 8 : 4
        // [4个奇数] [3个偶]
        int halfSize = (size + 1) / 2;
        int[] base = makeNo(halfSize);
        // base -> 等长奇数达标来
        // base -> 等长偶数达标来
        int[] ans = new int[size];
        int index = 0;
        for (; index < halfSize; index++) {
            ans[index] = base[index] * 2 - 1;
        }
        for (int i = 0; index < size; index++, i++) {
            ans[index] = base[i] * 2;
        }
        return ans;
    }

    // 检验函数
    public static boolean isValid(int[] arr) {
        int N = arr.length;
        for (int i = 0; i < N; i++) {
            for (int k = i + 1; k < N; k++) {
                for (int j = k + 1; j < N; j++) {
                    if (arr[i] + arr[j] == 2 * arr[k]) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    // 给定三个字符串str1、str2和aim，如果aim包含且仅包含来自str1和st2的所有字符，而目在aim中属于str1的字符之间保持原来
    // 在str1中的顺序，属于str2的字符之间保持原来在str2中的顺序，那么称aim是str1和str2的交错组成。
    // 实现一个函数，判断aim是否是str1和str2交错组成的字符串
    // 本题测试链接 : https://leetcode.com/problems/interleaving-string/
    public static class InterleavingString {

        public static boolean isInterleave(String s1, String s2, String s3) {
            if (s1 == null || s2 == null || s3 == null) {
                return false;
            }
            char[] str1 = s1.toCharArray();
            char[] str2 = s2.toCharArray();
            char[] str3 = s3.toCharArray();
            if (str3.length != str1.length + str2.length) {
                return false;
            }
            boolean[][] dp = new boolean[str1.length + 1][str2.length + 1];
            dp[0][0] = true;
            for (int i = 1; i <= str1.length; i++) {
                if (str1[i - 1] != str3[i - 1]) {
                    break;
                }
                dp[i][0] = true;
            }
            for (int j = 1; j <= str2.length; j++) {
                if (str2[j - 1] != str3[j - 1]) {
                    break;
                }
                dp[0][j] = true;
            }
            for (int i = 1; i <= str1.length; i++) {
                for (int j = 1; j <= str2.length; j++) {
                    if (    // 以str1[i-1]位置作为交错字符串的结尾
                            (str1[i - 1] == str3[i + j - 1] && dp[i - 1][j]) ||
                                    // 以str2[j-1]位置作为交错字符串的结尾
                                    (str2[j - 1] == str3[i + j - 1] && dp[i][j - 1])
                    ) {
                        dp[i][j] = true;
                    }
                }
            }
            return dp[str1.length][str2.length];
        }
    }

    // 如果一个节点X，它左树结构和右树结构完全一样，那么我们说以X为头的子树是相等子树
    // 给定一棵二叉树的头节点head，返回head整棵树上有多少棵相等子树
    public static class LeftRightSameTreeNumber {

        public static class Node {
            public int value;
            public Node left;
            public Node right;

            public Node(int v) {
                value = v;
            }
        }

        // 时间复杂度O(N * logN)
        public static int sameNumber1(Node head) {
            if (head == null) {
                return 0;
            }
            return sameNumber1(head.left) + sameNumber1(head.right) + (same(head.left, head.right) ? 1 : 0);
        }

        public static boolean same(Node h1, Node h2) {
            if (h1 == null ^ h2 == null) {
                return false;
            }
            if (h1 == null && h2 == null) {
                return true;
            }
            // 两个都不为空
            return h1.value == h2.value && same(h1.left, h2.left) && same(h1.right, h2.right);
        }

        public static class Hash {

            private MessageDigest hash;

            public Hash(String algorithm) {
                try {
                    hash = MessageDigest.getInstance(algorithm);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
            }

            public String hashCode(String input) {
                return DatatypeConverter.printHexBinary(hash.digest(input.getBytes())).toUpperCase();
            }
        }

        // 时间复杂度O(N)
        public static int sameNumber2(Node head) {
            String algorithm = "SHA-256";
            Hash hash = new Hash(algorithm);
            return process(head, hash).ans;
        }

        public static class Info {
            public int ans;
            public String str;

            public Info(int a, String s) {
                ans = a;
                str = s;
            }
        }

        public static Info process(Node head, Hash hash) {
            if (head == null) {
                return new Info(0, hash.hashCode("#,"));
            }
            Info l = process(head.left, hash);
            Info r = process(head.right, hash);
            int ans = (l.str.equals(r.str) ? 1 : 0) + l.ans + r.ans;
            String str = hash.hashCode(String.valueOf(head.value) + "," + l.str + r.str);
            return new Info(ans, str);
        }

        public static Node randomBinaryTree(int restLevel, int maxValue) {
            if (restLevel == 0) {
                return null;
            }
            Node head = Math.random() < 0.2 ? null : new Node((int) (Math.random() * maxValue));
            if (head != null) {
                head.left = randomBinaryTree(restLevel - 1, maxValue);
                head.right = randomBinaryTree(restLevel - 1, maxValue);
            }
            return head;
        }

        public static void test(String[] args) {
            int maxLevel = 8;
            int maxValue = 4;
            int testTime = 100000;
            System.out.println("测试开始");
            for (int i = 0; i < testTime; i++) {
                Node head = randomBinaryTree(maxLevel, maxValue);
                int ans1 = sameNumber1(head);
                int ans2 = sameNumber2(head);
                if (ans1 != ans2) {
                    System.out.println("出错了！");
                    System.out.println(ans1);
                    System.out.println(ans2);
                }
            }
            System.out.println("测试结束");
        }
    }

    // 编辑距离问题
    public static int minCost1(String s1, String s2, int ic, int dc, int rc) {
        if (s1 == null || s2 == null) {
            return 0;
        }
        char[] str1 = s1.toCharArray();
        char[] str2 = s2.toCharArray();
        int N = str1.length + 1;
        int M = str2.length + 1;
        int[][] dp = new int[N][M];
        // dp[0][0] = 0
        for (int i = 1; i < N; i++) {
            dp[i][0] = dc * i;
        }
        for (int j = 1; j < M; j++) {
            dp[0][j] = ic * j;
        }
        for (int i = 1; i < N; i++) {
            for (int j = 1; j < M; j++) {
                dp[i][j] = dp[i - 1][j - 1] + (str1[i - 1] == str2[j - 1] ? 0 : rc);
                dp[i][j] = Math.min(dp[i][j], dp[i][j - 1] + ic);
                dp[i][j] = Math.min(dp[i][j], dp[i - 1][j] + dc);
            }
        }
        return dp[N - 1][M - 1];
    }

    // 空间压缩
    public static int minCost2(String str1, String str2, int ic, int dc, int rc) {
        if (str1 == null || str2 == null) {
            return 0;
        }
        char[] chs1 = str1.toCharArray();
        char[] chs2 = str2.toCharArray();
        char[] longs = chs1.length >= chs2.length ? chs1 : chs2;
        char[] shorts = chs1.length < chs2.length ? chs1 : chs2;
        if (chs1.length < chs2.length) {
            int tmp = ic;
            ic = dc;
            dc = tmp;
        }
        int[] dp = new int[shorts.length + 1];
        for (int i = 1; i <= shorts.length; i++) {
            dp[i] = ic * i;
        }
        for (int i = 1; i <= longs.length; i++) {
            int pre = dp[0];
            dp[0] = dc * i;
            for (int j = 1; j <= shorts.length; j++) {
                int tmp = dp[j];
                if (longs[i - 1] == shorts[j - 1]) {
                    dp[j] = pre;
                } else {
                    dp[j] = pre + rc;
                }
                dp[j] = Math.min(dp[j], dp[j - 1] + ic);
                dp[j] = Math.min(dp[j], tmp + dc);
                pre = tmp;
            }
        }
        return dp[shorts.length];
    }

    // 有2 * n的一个长方形方格，用一个1 * 2的骨牌铺满方格
    // 编写一个程序，试对给出的任意一个n(n>0), 输出铺法总数。
    public static int way(int N) {
        if (N <= 0) {
            return -1;
        }
        if (N < 3) {
            return N;
        }
        int first = 1;
        int second = 2;
        int ans = 0;
        while (N - 1 >= 2) {
            ans = first + second;
            first = second;
            second = ans;
            N--;
        }
        return ans;
    }

    // 算法原型：环形单链表用约瑟夫环问题解决
    public static class Node1 {
        public Node1 next;
        public int value;
        public Node1 left;
        public Node1 right;

        public Node1() {

        }
    }

    public static Node1 josephusKill2(Node1 head, int m) {
        if (head == null) {
            return head;
        }
        if (head.next == null) {
            return head;
        }
        if (m < 1) {
            return head;
        }
        Node1 cur = head.next;
        int size = 1;
        while (cur != head) {
            size++;
            cur = cur.next;
        }
        size = getLive(size, m);
        while (--size != 0) {
            cur = cur.next;
        }
        cur.next = cur;
        return cur;
    }

    // 现在一共有i个节点，数到m就杀死节点，最终会活下来的节点，请返回它在有i个节点时的编号
    // get(N,m)
    public static int getLive(int i, int m) {
        if (i == 1) {
            return 1;
        }
        // 长度为i - 1时，活下来的节点的新编号
        int newNode = getLive(i - 1, m);
        return (newNode + m - 1) % i + 1;
    }

    // 某公司招聘，有n个人入围，HR在黑板上依次写下m个正整数A1、A2、……、Am，然后让这n个人围成一个 圈，并按照顺时针顺序为他们编号0、1、2、……、n-1。录取规则是：
    // 第一轮从0号的人开始，取用黑板上的第1个数字，也就是A1黑板上的数字按次序循环取用，即如果某轮用了第m个，则下一轮需要用第1个；如果某轮用到第k个，则下轮需要用第k+1个（k<m）
    // 每一轮按照黑板上的次序取用到一个数字Ax，淘汰掉从当前轮到的人开始按照顺时针顺序数到的第Ax个人，
    // 下一轮开始时轮到的人即为被淘汰掉的人的顺时针顺序下一个人被淘汰的人直接回家，所以不会被后续轮次计数时数到经过n-1轮后，剩下的最后1人被录取所以最后被录取的人的编号与（n，m，A1，A2，……，Am）相关。
    // 输入描述：
    // 第一行是一个正整数N，表示有N组参数从第二行开始，每行有若干个正整数，依次存放n、m、A1、……、Am，一共有N行，也就是上面的N组参数。
    // 输出描述：
    // 输出有N行，每行对应相应的那组参数确定的录取之人的编号示例
    // 输入
    // 1
    // n m arr[m]
    // 4 2 3 1
    // 输出
    // 1
    public static int recruit(int n, int[] arr) {
        return live(n, arr, 0);
    }

    // 还剩i个人，当前取用的数字为arr[index]，并且在下面的过程中，循环取用arr中的数字，
    // 返回哪个人活(在这i个人中)
    public static int live(int n, int[] arr, int index) {
        if (n == 1) {
            return 1;
        }
        int newLive = live(n - 1, arr, nextIndex(arr.length, index));
        return (newLive + arr[index] - 1) % n + 1;
    }

    public static int nextIndex(int length, int index) {
        return index == length - 1 ? 0 : index + 1;
    }

    // Nim博弈问题
    // 给定一个非负数组，每一个值代表该位置上有几个铜板。a和b玩游戏，a先手，b后手，
    // 轮到某个人的时候，只能在一个位置上拿任意数量的铜板，但是不能不拿。
    // 谁最先把铜 板拿完谁赢。假设a和b都极度聪明，请返回获胜者的名字
    public static String getWinner(int[] array) {
        int eorResult = 0;
        for (int num : array) {
            eorResult ^= num;
        }
        return eorResult == 0 ? "后手" : "先手";
    }

    // 给定一个二叉树的头节点head，路径可以从任何节点出发，但必须往下走到达任何节点，返回最大路径和
    public static int maxSum(Node2 head) {
        if (head == null) {
            return 0;
        }
        return process(head).allTreeMaxSum;
    }

    public static class Info1 {
        public int allTreeMaxSum;
        public int fromHeadMaxSum;

        public Info1(int all, int from) {
            allTreeMaxSum = all;
            fromHeadMaxSum = from;
        }
    }

    public static class Node2 {
        public Node2 left;
        public Node2 right;
        public Integer value;

        public Node2() {

        }
    }

    // 1）X无关的时候， 1， 左树上的整体最大路径和 2， 右树上的整体最大路径和
    // 2) X有关的时候 3， x自己 4， x往左走 5，x往右走
    public static Info1 process(Node2 x) {
        if (x == null) {
            return null;
        }
        Info1 leftInfo = process(x.left);
        Info1 rightInfo = process(x.right);
        int p1 = Integer.MIN_VALUE;
        if (leftInfo != null) {
            // 可能性一
            p1 = leftInfo.allTreeMaxSum;
        }
        int p2 = Integer.MIN_VALUE;
        if (rightInfo != null) {
            // 可能性二
            p2 = rightInfo.allTreeMaxSum;
        }
        // 可能性三
        int p3 = x.value;
        // 可能性四
        int p4 = Integer.MIN_VALUE;
        if (leftInfo != null) {
            p4 = x.value + leftInfo.fromHeadMaxSum;
        }
        // 可能性五
        int p5 = Integer.MIN_VALUE;
        if (rightInfo != null) {
            p5 = x.value + rightInfo.fromHeadMaxSum;
        }
        int allTreeMaxSum = Math.max(Math.max(Math.max(p1, p2), p3), Math.max(p4, p5));
        int fromHeadMaxSum = Math.max(Math.max(p3, p4), p5);
        return new Info1(allTreeMaxSum, fromHeadMaxSum);
    }

    // 本题测试链接 : https://leetcode.com/problems/basic-calculator-iii/
    public static class ExpressionCompute {

        public static int calculate(String str) {
            return f(str.toCharArray(), 0)[0];
        }

        // 请从str[i...]往下算，遇到字符串终止位置或者右括号，就停止
        // 返回两个值，长度为2的数组
        // 0) 负责的这一段的结果是多少
        // 1) 负责的这一段计算到了哪个位置
        public static int[] f(char[] str, int i) {
            LinkedList<String> queue = new LinkedList<String>();
            int cur = 0;
            int[] bra = null;
            // 从i出发，开始撸串
            while (i < str.length && str[i] != ')') {
                if (str[i] >= '0' && str[i] <= '9') {
                    cur = cur * 10 + str[i++] - '0';
                } else if (str[i] != '(') { // 遇到的是运算符号
                    addNum(queue, cur, str[i++]);
                    cur = 0;
                } else { // 遇到左括号了
                    bra = f(str, i + 1);
                    cur = bra[0];
                    i = bra[1] + 1;
                }
            }
            addNum(queue, cur, '+');
            return new int[]{getAns(queue), i};
        }

        public static void addNum(LinkedList<String> queue, int num, char op) {
            if (!queue.isEmpty() && (queue.peekLast().equals("*") || queue.peekLast().equals("/"))) {
                // 如果队列尾部是*或者/需要进行优先级计算
                // 弹出尾部的符号
                String top = queue.pollLast();
                // 弹出左操作数
                int pre = Integer.valueOf(queue.pollLast());
                num = top.equals("*") ? (pre * num) : (pre / num);
            }
            queue.addLast(String.valueOf(num));
            queue.addLast(String.valueOf(op));
        }

        public static int getAns(LinkedList<String> queue) {
            int ans = Integer.valueOf(queue.pollFirst());
            while (queue.size() > 1) {
                String op = queue.pollFirst();
                int num = Integer.valueOf(queue.pollFirst());
                ans += op.equals("+") ? num : -num;
            }
            return ans;
        }
    }

    // 本题测试链接 : https://leetcode.com/problems/container-with-most-water/
    public static class ContainerWithMostWater {

        public static int maxArea1(int[] h) {
            int max = 0;
            int N = h.length;
            for (int i = 0; i < N; i++) { // h[i]
                for (int j = i + 1; j < N; j++) { // h[j]
                    max = Math.max(max, Math.min(h[i], h[j]) * (j - i));
                }
            }
            return max;
        }

        public static int maxArea2(int[] h) {
            int max = 0;
            int l = 0;
            int r = h.length - 1;
            while (l < r) {
                max = Math.max(max, Math.min(h[l], h[r]) * (r - l));
                if (h[l] > h[r]) {
                    r--;
                } else {
                    l++;
                }
            }
            return max;
        }
    }

    // 测试链接 : https://leetcode.com/problems/remove-invalid-parentheses/
    public static class RemoveInvalidParentheses {

        // 来自leetcode投票第一的答案，实现非常好，我们来赏析一下
        public static List<String> removeInvalidParentheses(String s) {
            List<String> ans = new ArrayList<>();
            remove(s, ans, 0, 0, new char[]{'(', ')'});
            return ans;
        }

        // modifyIndex <= checkIndex
        // 只查s[checkIndex....]的部分，因为之前的一定已经调整对了
        // 但是之前的部分是怎么调整对的，调整到了哪？就是modifyIndex
        // 比如：
        // ( ( ) ( ) ) ) ...
        // 0 1 2 3 4 5 6
        // 一开始当然checkIndex = 0，modifyIndex = 0
        // 当查到6的时候，发现不对了，
        // 然后可以去掉2位置、4位置的 )，都可以
        // 如果去掉2位置的 ), 那么下一步就是
        // ( ( ( ) ) ) ...
        // 0 1 2 3 4 5 6
        // checkIndex = 6 ，modifyIndex = 2
        // 如果去掉4位置的 ), 那么下一步就是
        // ( ( ) ( ) ) ...
        // 0 1 2 3 4 5 6
        // checkIndex = 6 ，modifyIndex = 4
        // 也就是说，
        // checkIndex和modifyIndex，分别表示查的开始 和 调的开始，之前的都不用管了  par  (  )
        public static void remove(String s, List<String> ans, int checkIndex, int modifyIndex, char[] par) {
            for (int count = 0, i = checkIndex; i < s.length(); i++) {
                // 遇到左括号
                if (s.charAt(i) == par[0]) {
                    count++;
                }
                // 遇到右括号
                if (s.charAt(i) == par[1]) {
                    count--;
                }
                // i check计数<0的第一个位置
                if (count < 0) {
                    for (int j = modifyIndex; j <= i; ++j) {
                        // 比如
                        if (s.charAt(j) == par[1] && (j == modifyIndex || s.charAt(j - 1) != par[1])) {
                            remove(s.substring(0, j) + s.substring(j + 1, s.length()),
                                    ans, i, j, par);
                        }
                    }
                    return;
                }
            }
            String reversed = new StringBuilder(s).reverse().toString();
            if (par[0] == '(') {
                remove(reversed, ans, 0, 0, new char[]{')', '('});
            } else {
                ans.add(reversed);
            }
        }
    }

    //最长递增子序列
    // 本题测试链接 : https://leetcode.com/problems/longest-increasing-subsequence
    public static class LIS {

        public static int lengthOfLIS(int[] arr) {
            if (arr == null || arr.length == 0) {
                return 0;
            }
            // ends数组
            // ends[i]表示 : 目前所有长度为i+1的递增子序列的最小结尾
            int[] ends = new int[arr.length];
            // 根据含义, 一开始ends[0] = arr[0]
            ends[0] = arr[0];
            // ends有效区范围是0...right，right往右为无效区
            // 所以一开始right = 0, 表示有效区只有0...0范围
            int right = 0;
            // 最长递增子序列的长度
            // 全局变量，抓取每一步的答案，取最大的结果
            int max = 1;
            for (int i = 1; i < arr.length; i++) {
                int l = 0;
                int r = right;
                // 在ends[l...r]范围上二分
                // 如果 当前数(arr[i]) > ends[m]，砍掉左侧
                // 如果 当前数(arr[i]) <= ends[m]，砍掉右侧
                // 整个二分就是在ends里寻找 >= 当前数(arr[i])的最左位置
                // 就是从while里面出来时，l所在的位置。
                // 如果ends中不存在 >= 当前数(arr[i])的情况，将返回有效区的越界位置
                // 也就是从while里面出来时，l所在的位置，是有效区的越界位置
                // 比如 : ends = { 3, 5, 9, 12, 再往右无效}
                // 如果当前数为8, 从while里面出来时，l将来到2位置
                // 比如 : ends = { 3, 5, 9, 12, 再往右无效}
                // 如果当前数为13, 从while里面出来时，l将来到有效区的越界位置，4位置
                while (l <= r) {
                    int m = (l + r) / 2;
                    if (arr[i] > ends[m]) {
                        l = m + 1;
                    } else {
                        r = m - 1;
                    }
                }
                // 从while里面出来，看l的位置
                // 如果l比right大，说明扩充了有效区，那么right变量要随之变大
                // 如果l不比right大，说明l没有来到有效区的越界位置，right不变
                right = Math.max(right, l);
                // l的位置，就是当前数应该填到ends数组里的位置
                ends[l] = arr[i];
                // 更新全局变量
                max = Math.max(max, l + 1);
            }
            return max;
        }
    }

    // 定义何为stepSum：比如680，680+68+6 = 754，680的stepSum叫754
    // 给定一个正数判断是它是否是某个数的stepSum
    public static class IsStepSum {

        public static boolean isStepSum(int stepSum) {
            int L = 0;
            int R = stepSum;
            int M = 0;
            int cur = 0;
            while (L <= R) {
                M = L + ((R - L) >> 1);
                cur = stepSum(M);
                if (cur == stepSum) {
                    return true;
                } else if (cur < stepSum) {
                    L = M + 1;
                } else {
                    R = M - 1;
                }
            }
            return false;
        }

        public static int stepSum(int num) {
            int sum = 0;
            while (num != 0) {
                sum += num;
                num /= 10;
            }
            return sum;
        }
    }

    // 本题测试链接 : https://leetcode.com/problems/jump-game-ii/
    public static class JumpGame {
        public static int jump(int[] arr) {
            if (arr == null || arr.length == 0) {
                return 0;
            }
            // 步数
            int step = 0;
            // 能跳多远
            int cur = 0;
            // 走一步后能跳到多远
            int next = 0;
            for (int i = 0; i < arr.length; i++) {
                if (cur < i) {
                    step++;
                    cur = next;
                }
                next = Math.max(next, i + arr[i]);
            }
            return step;
        }
    }

    // 本题测试链接：https://www.lintcode.com/problem/top-k-frequent-words-ii/
    public static class TopK {

        private Node[] heap;
        private int heapSize;
        // 词频表   key  abc   value  (abc,7)
        private HashMap<String, Node> strNodeMap;
        private HashMap<Node, Integer> nodeIndexMap;
        private NodeHeapComp comp;
        private TreeSet<Node> treeSet;

        public TopK(int K) {
            heap = new Node[K];
            heapSize = 0;
            strNodeMap = new HashMap<String, Node>();
            nodeIndexMap = new HashMap<Node, Integer>();
            comp = new NodeHeapComp();
            treeSet = new TreeSet<>(new NodeTreeSetComp());
        }

        public static class Node {
            public String str;
            public int times;

            public Node(String s, int t) {
                str = s;
                times = t;
            }
        }

        public static class NodeHeapComp implements Comparator<Node> {

            @Override
            public int compare(Node o1, Node o2) {
                return o1.times != o2.times ? (o1.times - o2.times) : (o2.str.compareTo(o1.str));
            }

        }

        public static class NodeTreeSetComp implements Comparator<Node> {

            @Override
            public int compare(Node o1, Node o2) {
                return o1.times != o2.times ? (o2.times - o1.times) : (o1.str.compareTo(o2.str));
            }

        }

        public void add(String str) {
            if (heap.length == 0) {
                return;
            }
            // str   找到对应节点  curNode
            Node curNode = null;
            // 对应节点  curNode  在堆上的位置
            int preIndex = -1;
            if (!strNodeMap.containsKey(str)) {
                curNode = new Node(str, 1);
                strNodeMap.put(str, curNode);
                nodeIndexMap.put(curNode, -1);
            } else {
                curNode = strNodeMap.get(str);
                // 要在time++之前，先在treeSet中删掉
                // 原因是因为一但times++，curNode在treeSet中的排序就失效了
                // 这种失效会导致整棵treeSet出现问题
                if (treeSet.contains(curNode)) {
                    treeSet.remove(curNode);
                }
                curNode.times++;
                preIndex = nodeIndexMap.get(curNode);
            }
            if (preIndex == -1) {
                if (heapSize == heap.length) {
                    if (comp.compare(heap[0], curNode) < 0) {
                        treeSet.remove(heap[0]);
                        treeSet.add(curNode);
                        nodeIndexMap.put(heap[0], -1);
                        nodeIndexMap.put(curNode, 0);
                        heap[0] = curNode;
                        heapify(0, heapSize);
                    }
                } else {
                    treeSet.add(curNode);
                    nodeIndexMap.put(curNode, heapSize);
                    heap[heapSize] = curNode;
                    heapInsert(heapSize++);
                }
            } else {
                treeSet.add(curNode);
                heapify(preIndex, heapSize);
            }
        }

        public List<String> topk() {
            ArrayList<String> ans = new ArrayList<>();
            for (Node node : treeSet) {
                ans.add(node.str);
            }
            return ans;
        }

        private void heapInsert(int index) {
            while (index != 0) {
                int parent = (index - 1) / 2;
                if (comp.compare(heap[index], heap[parent]) < 0) {
                    swap(parent, index);
                    index = parent;
                } else {
                    break;
                }
            }
        }

        private void heapify(int index, int heapSize) {
            int l = index * 2 + 1;
            int r = index * 2 + 2;
            int smallest = index;
            while (l < heapSize) {
                if (comp.compare(heap[l], heap[index]) < 0) {
                    smallest = l;
                }
                if (r < heapSize && comp.compare(heap[r], heap[smallest]) < 0) {
                    smallest = r;
                }
                if (smallest != index) {
                    swap(smallest, index);
                } else {
                    break;
                }
                index = smallest;
                l = index * 2 + 1;
                r = index * 2 + 2;
            }
        }

        private void swap(int index1, int index2) {
            nodeIndexMap.put(heap[index1], index2);
            nodeIndexMap.put(heap[index2], index1);
            Node tmp = heap[index1];
            heap[index1] = heap[index2];
            heap[index2] = tmp;
        }
    }

    // 本题测试链接 : https://leetcode-cn.com/problems/boolean-evaluation-lcci/
    public static class BooleanEvaluation {

        public static int countEval0(String express, int desired) {
            if (express == null || express.equals("")) {
                return 0;
            }
            char[] exp = express.toCharArray();
            int N = exp.length;
            Info[][] dp = new Info[N][N];
            Info allInfo = func(exp, 0, exp.length - 1, dp);
            return desired == 1 ? allInfo.t : allInfo.f;
        }

        public static class Info {
            public int t;
            public int f;

            public Info(int tr, int fa) {
                t = tr;
                f = fa;
            }
        }

        // 限制:
        // L...R上，一定有奇数个字符
        // L位置的字符和R位置的字符，非0即1，不能是逻辑符号！
        // 返回str[L...R]这一段，为true的方法数，和false的方法数
        public static Info func(char[] str, int L, int R, Info[][] dp) {
            if (dp[L][R] != null) {
                return dp[L][R];
            }
            int t = 0;
            int f = 0;
            if (L == R) {
                t = str[L] == '1' ? 1 : 0;
                f = str[L] == '0' ? 1 : 0;
            } else { // L..R >=3
                // 每一个种逻辑符号，split枚举的东西
                // 都去试试最后结合
                for (int split = L + 1; split < R; split += 2) {
                    Info leftInfo = func(str, L, split - 1, dp);
                    Info rightInfo = func(str, split + 1, R, dp);
                    int a = leftInfo.t;
                    int b = leftInfo.f;
                    int c = rightInfo.t;
                    int d = rightInfo.f;
                    switch (str[split]) {
                        case '&':
                            t += a * c;
                            f += b * c + b * d + a * d;
                            break;
                        case '|':
                            t += a * c + a * d + b * c;
                            f += b * d;
                            break;
                        case '^':
                            t += a * d + b * c;
                            f += a * c + b * d;
                            break;
                    }
                }

            }
            dp[L][R] = new Info(t, f);
            return dp[L][R];
        }

        public static int countEval1(String express, int desired) {
            if (express == null || express.equals("")) {
                return 0;
            }
            char[] exp = express.toCharArray();
            return f(exp, desired, 0, exp.length - 1);
        }

        public static int f(char[] str, int desired, int L, int R) {
            if (L == R) {
                if (str[L] == '1') {
                    return desired;
                } else {
                    return desired ^ 1;
                }
            }
            int res = 0;
            if (desired == 1) {
                for (int i = L + 1; i < R; i += 2) {
                    switch (str[i]) {
                        case '&':
                            res += f(str, 1, L, i - 1) * f(str, 1, i + 1, R);
                            break;
                        case '|':
                            res += f(str, 1, L, i - 1) * f(str, 0, i + 1, R);
                            res += f(str, 0, L, i - 1) * f(str, 1, i + 1, R);
                            res += f(str, 1, L, i - 1) * f(str, 1, i + 1, R);
                            break;
                        case '^':
                            res += f(str, 1, L, i - 1) * f(str, 0, i + 1, R);
                            res += f(str, 0, L, i - 1) * f(str, 1, i + 1, R);
                            break;
                    }
                }
            } else {
                for (int i = L + 1; i < R; i += 2) {
                    switch (str[i]) {
                        case '&':
                            res += f(str, 0, L, i - 1) * f(str, 1, i + 1, R);
                            res += f(str, 1, L, i - 1) * f(str, 0, i + 1, R);
                            res += f(str, 0, L, i - 1) * f(str, 0, i + 1, R);
                            break;
                        case '|':
                            res += f(str, 0, L, i - 1) * f(str, 0, i + 1, R);
                            break;
                        case '^':
                            res += f(str, 1, L, i - 1) * f(str, 1, i + 1, R);
                            res += f(str, 0, L, i - 1) * f(str, 0, i + 1, R);
                            break;
                    }
                }
            }
            return res;
        }

        public static int countEval2(String express, int desired) {
            if (express == null || express.equals("")) {
                return 0;
            }
            char[] exp = express.toCharArray();
            int N = exp.length;
            int[][][] dp = new int[2][N][N];
            dp[0][0][0] = exp[0] == '0' ? 1 : 0;
            dp[1][0][0] = dp[0][0][0] ^ 1;
            for (int i = 2; i < exp.length; i += 2) {
                dp[0][i][i] = exp[i] == '1' ? 0 : 1;
                dp[1][i][i] = exp[i] == '0' ? 0 : 1;
                for (int j = i - 2; j >= 0; j -= 2) {
                    for (int k = j; k < i; k += 2) {
                        if (exp[k + 1] == '&') {
                            dp[1][j][i] += dp[1][j][k] * dp[1][k + 2][i];
                            dp[0][j][i] += (dp[0][j][k] + dp[1][j][k]) * dp[0][k + 2][i] + dp[0][j][k] * dp[1][k + 2][i];
                        } else if (exp[k + 1] == '|') {
                            dp[1][j][i] += (dp[0][j][k] + dp[1][j][k]) * dp[1][k + 2][i] + dp[1][j][k] * dp[0][k + 2][i];
                            dp[0][j][i] += dp[0][j][k] * dp[0][k + 2][i];
                        } else {
                            dp[1][j][i] += dp[0][j][k] * dp[1][k + 2][i] + dp[1][j][k] * dp[0][k + 2][i];
                            dp[0][j][i] += dp[0][j][k] * dp[0][k + 2][i] + dp[1][j][k] * dp[1][k + 2][i];
                        }
                    }
                }
            }
            return dp[desired][0][N - 1];
        }
    }

    // 谷歌面试题
    // 面值为1~10的牌组成一组，
    // 每次你从组里等概率的抽出1~10中的一张
    // 下次抽会换一个新的组，有无限组
    // 当累加和<17时，你将一直抽牌
    // 当累加和>=17且<21时，你将获胜
    // 当累加和>=21时，你将失败
    // 返回获胜的概率
    public static class NCardsABWin {
        public static double f1() {
            return p1(0);
        }

        // 游戏的规则，如上
        // 当你来到cur这个累加和的时候，获胜概率是多少返回！
        public static double p1(int cur) {
            if (cur >= 17 && cur < 21) {
                return 1.0;
            }
            if (cur >= 21) {
                return 0.0;
            }
            double w = 0.0;
            for (int i = 1; i <= 10; i++) {
                w += p1(cur + i);
            }
            return w / 10;
        }

        // 谷歌面试题扩展版
        // 面值为1~N的牌组成一组，
        // 每次你从组里等概率的抽出1~N中的一张
        // 下次抽会换一个新的组，有无限组
        // 当累加和<a时，你将一直抽牌
        // 当累加和>=a且<b时，你将获胜
        // 当累加和>=b时，你将失败
        // 返回获胜的概率，给定的参数为N，a，b
        public static double f2(int N, int a, int b) {
            if (N < 1 || a >= b || a < 0 || b < 0) {
                return 0.0;
            }
            if (b - a >= N) {
                return 1.0;
            }
            // 所有参数都合法，并且b-a < N
            return p2(0, N, a, b);
        }

        // 游戏规则，如上，int N, int a, int b，固定参数！
        // cur，目前到达了cur的累加和
        // 返回赢的概率
        public static double p2(int cur, int N, int a, int b) {
            if (cur >= a && cur < b) {
                return 1.0;
            }
            if (cur >= b) {
                return 0.0;
            }
            double w = 0.0;
            for (int i = 1; i <= N; i++) {
                w += p2(cur + i, N, a, b);
            }
            return w / N;
        }

        // f2的改进版本，用到了观察位置优化枚举的技巧
        // 可以课上讲一下
        public static double f3(int N, int a, int b) {
            if (N < 1 || a >= b || a < 0 || b < 0) {
                return 0.0;
            }
            if (b - a >= N) {
                return 1.0;
            }
            return p3(0, N, a, b);
        }

        public static double p3(int cur, int N, int a, int b) {
            if (cur >= a && cur < b) {
                return 1.0;
            }
            if (cur >= b) {
                return 0.0;
            }
            if (cur == a - 1) {
                return 1.0 * (b - a) / N;
            }
            double w = p3(cur + 1, N, a, b) + p3(cur + 1, N, a, b) * N;
            if (cur + 1 + N < b) {
                w -= p3(cur + 1 + N, N, a, b);
            }
            return w / N;
        }

        // f3的改进版本的动态规划
        // 可以课上讲一下
        public static double f4(int N, int a, int b) {
            if (N < 1 || a >= b || a < 0 || b < 0) {
                return 0.0;
            }
            if (b - a >= N) {
                return 1.0;
            }
            double[] dp = new double[b];
            for (int i = a; i < b; i++) {
                dp[i] = 1.0;
            }
            if (a - 1 >= 0) {
                dp[a - 1] = 1.0 * (b - a) / N;
            }
            for (int cur = a - 2; cur >= 0; cur--) {
                double w = dp[cur + 1] + dp[cur + 1] * N;
                if (cur + 1 + N < b) {
                    w -= dp[cur + 1 + N];
                }
                dp[cur] = w / N;
            }
            return dp[0];
        }
    }

    // 本题测试链接 : https://leetcode.com/problems/super-washing-machines/
    public static class SuperWashingMachines {
        public static int findMinMoves(int[] arr) {
            if (arr == null || arr.length == 0) {
                return 0;
            }
            int size = arr.length;
            int sum = 0;
            for (int i = 0; i < size; i++) {
                sum += arr[i];
            }
            if (sum % size != 0) {
                return -1;
            }
            int avg = sum / size;
            int leftSum = 0;
            int ans = 0;
            for (int i = 0; i < arr.length; i++) {
                int leftRest = leftSum - i * avg;
                int rightRest = (sum - leftSum - arr[i]) - (size - i - 1) * avg;
                if (leftRest < 0 && rightRest < 0) {
                    ans = Math.max(ans, Math.abs(leftRest) + Math.abs(rightRest));
                } else {
                    ans = Math.max(ans, Math.max(Math.abs(leftRest), Math.abs(rightRest)));
                }
                leftSum += arr[i];
            }
            return ans;
        }
    }

    // 本题测试链接 : https://leetcode.com/problems/kth-smallest-element-in-a-sorted-matrix/
    public static class KthSmallestElementInSortedMatrix {

        // 堆的方法
        public static int kthSmallest1(int[][] matrix, int k) {
            int N = matrix.length;
            int M = matrix[0].length;
            PriorityQueue<Node> heap = new PriorityQueue<>(new NodeComparator());
            boolean[][] set = new boolean[N][M];
            heap.add(new Node(matrix[0][0], 0, 0));
            set[0][0] = true;
            int count = 0;
            Node ans = null;
            while (!heap.isEmpty()) {
                ans = heap.poll();
                if (++count == k) {
                    break;
                }
                int row = ans.row;
                int col = ans.col;
                if (row + 1 < N && !set[row + 1][col]) {
                    heap.add(new Node(matrix[row + 1][col], row + 1, col));
                    set[row + 1][col] = true;
                }
                if (col + 1 < M && !set[row][col + 1]) {
                    heap.add(new Node(matrix[row][col + 1], row, col + 1));
                    set[row][col + 1] = true;
                }
            }
            return ans.value;
        }

        public static class Node {
            public int value;
            public int row;
            public int col;

            public Node(int v, int r, int c) {
                value = v;
                row = r;
                col = c;
            }

        }

        public static class NodeComparator implements Comparator<Node> {

            @Override
            public int compare(Node o1, Node o2) {
                return o1.value - o2.value;
            }

        }

        // 二分的方法
        public static int kthSmallest2(int[][] matrix, int k) {
            int N = matrix.length;
            int M = matrix[0].length;
            int left = matrix[0][0];
            int right = matrix[N - 1][M - 1];
            int ans = 0;
            while (left <= right) {
                int mid = left + ((right - left) >> 1);
                // <=mid 有几个 <= mid 在矩阵中真实出现的数，谁最接近mid
                Info info = noMoreNum(matrix, mid);
                if (info.num < k) {
                    left = mid + 1;
                } else {
                    ans = info.near;
                    right = mid - 1;
                }
            }
            return ans;
        }

        public static class Info {
            public int near;
            public int num;

            public Info(int n1, int n2) {
                near = n1;
                num = n2;
            }
        }

        public static Info noMoreNum(int[][] matrix, int value) {
            int near = Integer.MIN_VALUE;
            int num = 0;
            int N = matrix.length;
            int M = matrix[0].length;
            int row = 0;
            int col = M - 1;
            while (row < N && col >= 0) {
                if (matrix[row][col] <= value) {
                    near = Math.max(near, matrix[row][col]);
                    num += col + 1;
                    row++;
                } else {
                    col--;
                }
            }
            return new Info(near, num);
        }
    }

    // 本题测试链接 : https://leetcode.com/problems/smallest-range-covering-elements-from-k-lists/
    public static class SmallestRangeCoveringElementsfromKLists {

        public static class Node {
            public int value;
            public int arrId;
            public int index;

            public Node(int v, int ai, int i) {
                value = v;
                arrId = ai;
                index = i;
            }
        }

        public static class NodeComparator implements Comparator<Node> {
            @Override
            public int compare(Node o1, Node o2) {
                return o1.value != o2.value ? o1.value - o2.value : o1.arrId - o2.arrId;
            }

        }

        public static int[] smallestRange(List<List<Integer>> nums) {
            int N = nums.size();
            TreeSet<Node> orderSet = new TreeSet<>(new NodeComparator());
            for (int i = 0; i < N; i++) {
                orderSet.add(new Node(nums.get(i).get(0), i, 0));
            }
            boolean set = false;
            int a = 0;
            int b = 0;
            while (orderSet.size() == N) {
                Node min = orderSet.first();
                Node max = orderSet.last();
                if (!set || (max.value - min.value < b - a)) {
                    set = true;
                    a = min.value;
                    b = max.value;
                }
                min = orderSet.pollFirst();
                int arrId = min.arrId;
                int index = min.index + 1;
                if (index != nums.get(arrId).size()) {
                    orderSet.add(new Node(nums.get(arrId).get(index), arrId, index));
                }
            }
            return new int[] { a, b };
        }
    }

    // 本题测试链接 : https://leetcode.com/problems/trapping-rain-water/
    public static class TrappingRainWater {

        public static int trap(int[] arr) {
            if (arr == null || arr.length < 2) {
                return 0;
            }
            int N = arr.length;
            int L = 1;
            int leftMax = arr[0];
            int R = N - 2;
            int rightMax = arr[N - 1];
            int water = 0;
            while (L <= R) {
                if (leftMax <= rightMax) {
                    water += Math.max(0, leftMax - arr[L]);
                    leftMax = Math.max(leftMax, arr[L++]);
                } else {
                    water += Math.max(0, rightMax - arr[R]);
                    rightMax = Math.max(rightMax, arr[R--]);
                }
            }
            return water;
        }
    }

    // 本题测试链接 : https://leetcode.com/problems/trapping-rain-water-ii/
    public static class TrappingRainWaterII {
        public static class Node {
            public int value;
            public int row;
            public int col;

            public Node(int v, int r, int c) {
                value = v;
                row = r;
                col = c;
            }

        }

        public static int trapRainWater(int[][] heightMap) {
            if (heightMap == null || heightMap.length == 0 || heightMap[0] == null || heightMap[0].length == 0) {
                return 0;
            }
            int N = heightMap.length;
            int M = heightMap[0].length;
            boolean[][] isEnter = new boolean[N][M];
            PriorityQueue<Node> heap = new PriorityQueue<>((a, b) -> a.value - b.value);
            for (int col = 0; col < M - 1; col++) {
                isEnter[0][col] = true;
                heap.add(new Node(heightMap[0][col], 0, col));
            }
            for (int row = 0; row < N - 1; row++) {
                isEnter[row][M - 1] = true;
                heap.add(new Node(heightMap[row][M - 1], row, M - 1));
            }
            for (int col = M - 1; col > 0; col--) {
                isEnter[N - 1][col] = true;
                heap.add(new Node(heightMap[N - 1][col], N - 1, col));
            }
            for (int row = N - 1; row > 0; row--) {
                isEnter[row][0] = true;
                heap.add(new Node(heightMap[row][0], row, 0));
            }
            int water = 0;
            int max = 0;
            while (!heap.isEmpty()) {
                Node cur = heap.poll();
                max = Math.max(max, cur.value);
                int r = cur.row;
                int c = cur.col;
                if (r > 0 && !isEnter[r - 1][c]) {
                    water += Math.max(0, max - heightMap[r - 1][c]);
                    isEnter[r - 1][c] = true;
                    heap.add(new Node(heightMap[r - 1][c], r - 1, c));
                }
                if (r < N - 1 && !isEnter[r + 1][c]) {
                    water += Math.max(0, max - heightMap[r + 1][c]);
                    isEnter[r + 1][c] = true;
                    heap.add(new Node(heightMap[r + 1][c], r + 1, c));
                }
                if (c > 0 && !isEnter[r][c - 1]) {
                    water += Math.max(0, max - heightMap[r][c - 1]);
                    isEnter[r][c - 1] = true;
                    heap.add(new Node(heightMap[r][c - 1], r, c - 1));
                }
                if (c < M - 1 && !isEnter[r][c + 1]) {
                    water += Math.max(0, max - heightMap[r][c + 1]);
                    isEnter[r][c + 1] = true;
                    heap.add(new Node(heightMap[r][c + 1], r, c + 1));
                }
            }
            return water;
        }
    }

    // 本题测试链接 : https://leetcode.com/problems/minimum-cost-to-merge-stones/
    public static class MinimumCostToMergeStones {
        public static int mergeStones1(int[] stones, int K) {
            int n = stones.length;
            if ((n - 1) % (K - 1) > 0) {
                return -1;
            }
            int[] presum = new int[n + 1];
            for (int i = 0; i < n; i++) {
                presum[i + 1] = presum[i] + stones[i];
            }
            return process1(0, n - 1, 1, stones, K, presum);
        }

        // part >= 1
        // arr[L..R] 一定要弄出part份，返回最低代价
        // arr、K、presum（前缀累加和数组，求i..j的累加和，就是O(1)了）
        public static int process1(int L, int R, int P, int[] arr, int K, int[] presum) {
            if (L == R) { // arr[L..R]
                return P == 1 ? 0 : -1;
            }
            // L ... R 不只一个数
            if (P == 1) {
                int next = process1(L, R, K, arr, K, presum);
                if (next == -1) {
                    return -1;
                } else {
                    return next + presum[R + 1] - presum[L];
                }
            } else { // P > 1
                int ans = Integer.MAX_VALUE;
                // L...mid是第1块，剩下的是part-1块
                for (int mid = L; mid < R; mid += K - 1) {
                    // L..mid(一份) mid+1...R(part - 1)
                    int next1 = process1(L, mid, 1, arr, K, presum);
                    int next2 = process1(mid + 1, R, P - 1, arr, K, presum);
                    if (next1 != -1 && next2 != -1) {
                        ans = Math.min(ans, next1 + next2);
                    }
                }
                return ans;
            }
        }

        public static int mergeStones2(int[] stones, int K) {
            int n = stones.length;
            if ((n - 1) % (K - 1) > 0) {
                return -1;
            }
            int[] presum = new int[n + 1];
            for (int i = 0; i < n; i++) {
                presum[i + 1] = presum[i] + stones[i];
            }
            int[][][] dp = new int[n][n][K + 1];
            return process2(0, n - 1, 1, stones, K, presum, dp);
        }

        // 因为上游调用的时候，一定确保都是有效的调用
        // 核心是这一句 : for (int mid = L; mid < R; mid += K - 1) { ... }
        // 这一句保证了一定都是有效调用
        // 所以不需要写很多边界条件的判断，因为任何调用，都一定会返回正常答案
        // mid每次跳K-1个位置，这保证了左侧递归、右侧递归都是有效的
        // 根本不会返回无效解的
        public static int process2(int L, int R, int P, int[] arr, int K, int[] presum, int[][][] dp) {
            if (dp[L][R][P] != 0) {
                return dp[L][R][P];
            }
            if (L == R) {
                return 0;
            }
            int ans = Integer.MAX_VALUE;
            if (P == 1) {
                ans = process2(L, R, K, arr, K, presum, dp) + presum[R + 1] - presum[L];
            } else {
                for (int mid = L; mid < R; mid += K - 1) {
                    int next1 = process2(L, mid, 1, arr, K, presum, dp);
                    int next2 = process2(mid + 1, R, P - 1, arr, K, presum, dp);
                    ans = Math.min(ans, next1 + next2);
                }
            }
            dp[L][R][P] = ans;
            return ans;
        }
    }

    // 给定两个字符串s1和s2，求在s1中包含字符串s2的最短子串的长度
    // 最短子串的顺序可以和字符串s2不保持一致，只需要包含字符串s2即可
    public static class MinWindowLength {
        public static int minLength(String s1, String s2) {
            if (s1 == null || s2 == null || s1.length() < s2.length()) {
                return Integer.MAX_VALUE;
            }
            char[] str1 = s1.toCharArray();
            char[] str2 = s2.toCharArray();
            int[] map = new int[256]; // map[37] = 4 37 4次
            for (int i = 0; i != str2.length; i++) {
                map[str2[i]]++;
            }
            int all = str2.length;

            // [L,R-1] R
            // [L,R) -> [0,0)
            int L = 0;
            int R = 0;
            int minLen = Integer.MAX_VALUE;
            while (R != str1.length) {
                map[str1[R]]--;
                if (map[str1[R]] >= 0) {
                    all--;
                }
                if (all == 0) { // 还完了
                    while (map[str1[L]] < 0) {
                        map[str1[L++]]++;
                    }
                    // [L..R]
                    minLen = Math.min(minLen, R - L + 1);
                    all++;
                    map[str1[L++]]++;
                }
                R++;
            }
            return minLen == Integer.MAX_VALUE ? 0 : minLen;
        }

        // 测试链接 : https://leetcode.com/problems/minimum-window-substring/
        public static String minWindowSubString(String s, String t) {
            if (s.length() < t.length()) {
                return "";
            }
            char[] str = s.toCharArray();
            char[] target = t.toCharArray();
            int[] map = new int[256];
            for (char cha : target) {
                map[cha]++;
            }
            int all = target.length;
            int L = 0;
            int R = 0;
            int minLen = Integer.MAX_VALUE;
            int ansl = -1;
            int ansr = -1;
            while (R != str.length) {
                map[str[R]]--;
                if (map[str[R]] >= 0) {
                    all--;
                }
                if (all == 0) {
                    while (map[str[L]] < 0) {
                        map[str[L++]]++;
                    }
                    if (minLen > R - L + 1) {
                        minLen = R - L + 1;
                        ansl = L;
                        ansr = R;
                    }
                    all++;
                    map[str[L++]]++;
                }
                R++;
            }
            return minLen == Integer.MAX_VALUE ? "" : s.substring(ansl, ansr + 1);
        }
    }

    // 本题测试链接 : https://leetcode.com/problems/remove-duplicate-letters/
    public static class RemoveDuplicateLettersLessLexi {

        public static String removeDuplicateLetters(String s) {
            int[] cnts = new int[26];
            boolean[] enter = new boolean[26];
            for (int i = 0; i < s.length(); i++) {
                cnts[s.charAt(i) - 'a']++;
            }
            // 单调栈
            // 从左到右只保留依次变大的字符
            // 比如: a c e....
            char[] stack = new char[26];
            int size = 0;
            for (int i = 0; i < s.length(); i++) {
                // 假设当前字符是d
                char cur = s.charAt(i);
                // 如果d已经在单调栈里，不进入!
                // 因为单调栈里每种字符只保留一个
                if (!enter[cur - 'a']) {
                    // 如果d不在单调栈里，进入!
                    enter[cur - 'a'] = true;
                    // 如果单调栈里已经有 :
                    // a c e f
                    // 当前字符是d
                    // 那么f弹出、e弹出、然后d进入，
                    // 单调栈变成 : a c d
                    // 但是!
                    // 如果后面还有f，f才能弹出
                    // 如果后面还有e，e才能弹出
                    // 如果一个字符要弹出，但是后面已经没有这种字符了，不能弹出!
                    // 因为一旦弹出，再也没有机会收集到这种字符了！因为后面没有了
                    // 这就是核心逻辑
                    // 所以 : size > 0，表示单调栈里有字符
                    // stack[size - 1] > cur，表示单调栈当前最右的字符，比当前字符大，那么它弹出
                    // cnts[stack[size - 1] - 'a'] > 0，重要限制 : 后面还有单调栈当前最右的字符，才能弹出
                    // 缺一不可
                    while (size > 0 && stack[size - 1] > cur && cnts[stack[size - 1] - 'a'] > 0) {
                        // 这种字符要弹出了，所以标记这种字符出去了
                        enter[stack[size - 1] - 'a'] = false;
                        // 单调栈大小缩减了，也就是弹出了
                        size--;
                    }
                    // 当前字符进入了单调栈
                    stack[size++] = cur;
                }
                // 当前字符的词频调整，调整后表示后面还有多少个当前字符
                // 注意词频表更新
                cnts[cur - 'a']--;
            }
            // 单调栈里的字符，拼字符串返回
            return String.valueOf(stack, 0, size);
        }
    }

    // 约瑟夫环问题
    // 题解：https://www.bilibili.com/video/BV1DT411s7hp/?p=149&vd_source=f6b0f3f570df83e7c18c51119ac2b885
    // 本题测试链接 : https://leetcode-cn.com/problems/yuan-quan-zhong-zui-hou-sheng-xia-de-shu-zi-lcof/
    public static class JosephusProblem {

        // 提交直接通过
        // 给定的编号是0~n-1的情况下，数到m就杀
        // 返回谁会活？
        public int lastRemaining1(int n, int m) {
            return getLive(n, m) - 1;
        }

        // 课上题目的设定是，给定的编号是1~n的情况下，数到m就杀
        // 返回谁会活？
        public static int getLive(int n, int m) {
            if (n == 1) {
                return 1;
            }
            return (getLive(n - 1, m) + m - 1) % n + 1;
        }

        // 提交直接通过
        // 给定的编号是0~n-1的情况下，数到m就杀
        // 返回谁会活？
        // 这个版本是迭代版
        public int lastRemaining2(int n, int m) {
            int ans = 1;
            int r = 1;
            while (r <= n) {
                ans = (ans + m - 1) % (r++) + 1;
            }
            return ans - 1;
        }

        // 以下的code针对单链表，不要提交
        public static class Node {
            public int value;
            public Node next;

            public Node(int data) {
                this.value = data;
            }
        }

        public static Node josephusKill1(Node head, int m) {
            if (head == null || head.next == head || m < 1) {
                return head;
            }
            Node last = head;
            while (last.next != head) {
                last = last.next;
            }
            int count = 0;
            while (head != last) {
                if (++count == m) {
                    last.next = head.next;
                    count = 0;
                } else {
                    last = last.next;
                }
                head = last.next;
            }
            return head;
        }

        public static Node josephusKill2(Node head, int m) {
            if (head == null || head.next == head || m < 1) {
                return head;
            }
            Node cur = head.next;
            int size = 1; // tmp -> list size
            while (cur != head) {
                size++;
                cur = cur.next;
            }
            int live = getLive(size, m); // tmp -> service node position
            while (--live != 0) {
                head = head.next;
            }
            head.next = head;
            return head;
        }

        public static void printCircularList(Node head) {
            if (head == null) {
                return;
            }
            System.out.print("Circular List: " + head.value + " ");
            Node cur = head.next;
            while (cur != head) {
                System.out.print(cur.value + " ");
                cur = cur.next;
            }
            System.out.println("-> " + head.value);
        }
    }

    // 测试链接 : https://leetcode-cn.com/problems/21dk04/
    public static class DistinctSubSeq {

        public static int numDistinct1(String S, String T) {
            char[] s = S.toCharArray();
            char[] t = T.toCharArray();
            return process(s, t, s.length, t.length);
        }

        public static int process(char[] s, char[] t, int i, int j) {
            if (j == 0) {
                return 1;
            }
            if (i == 0) {
                return 0;
            }
            int res = process(s, t, i - 1, j);
            if (s[i - 1] == t[j - 1]) {
                res += process(s, t, i - 1, j - 1);
            }
            return res;
        }

        // S[...i]的所有子序列中，包含多少个字面值等于T[...j]这个字符串的子序列
        // 记为dp[i][j]
        // 可能性1）S[...i]的所有子序列中，都不以s[i]结尾，则dp[i][j]肯定包含dp[i-1][j]
        // 可能性2）S[...i]的所有子序列中，都必须以s[i]结尾，
        // 这要求S[i] == T[j]，则dp[i][j]包含dp[i-1][j-1]
        public static int numDistinct2(String S, String T) {
            char[] s = S.toCharArray();
            char[] t = T.toCharArray();
            // dp[i][j] : s[0..i] T[0...j]

            // dp[i][j] : s只拿前i个字符做子序列，有多少个子序列，字面值等于T的前j个字符的前缀串
            int[][] dp = new int[s.length + 1][t.length + 1];
            // dp[0][0]
            // dp[0][j] = s只拿前0个字符做子序列, T前j个字符
            for (int j = 0; j <= t.length; j++) {
                dp[0][j] = 0;
            }
            for (int i = 0; i <= s.length; i++) {
                dp[i][0] = 1;
            }
            for (int i = 1; i <= s.length; i++) {
                for (int j = 1; j <= t.length; j++) {
                    dp[i][j] = dp[i - 1][j] + (s[i - 1] == t[j - 1] ? dp[i - 1][j - 1] : 0);
                }
            }
            return dp[s.length][t.length];
        }

        public static int numDistinct3(String S, String T) {
            char[] s = S.toCharArray();
            char[] t = T.toCharArray();
            int[] dp = new int[t.length + 1];
            dp[0] = 1;
            for (int j = 1; j <= t.length; j++) {
                dp[j] = 0;
            }
            for (int i = 1; i <= s.length; i++) {
                for (int j = t.length; j >= 1; j--) {
                    dp[j] += s[i - 1] == t[j - 1] ? dp[j - 1] : 0;
                }
            }
            return dp[t.length];
        }

        public static int dp(String S, String T) {
            char[] s = S.toCharArray();
            char[] t = T.toCharArray();
            int N = s.length;
            int M = t.length;
            int[][] dp = new int[N][M];
            // s[0..0] T[0..0] dp[0][0]
            dp[0][0] = s[0] == t[0] ? 1 : 0;
            for (int i = 1; i < N; i++) {
                dp[i][0] = s[i] == t[0] ? (dp[i - 1][0] + 1) : dp[i - 1][0];
            }
            for (int i = 1; i < N; i++) {
                for (int j = 1; j <= Math.min(i, M - 1); j++) {
                    dp[i][j] = dp[i - 1][j];
                    if (s[i] == t[j]) {
                        dp[i][j] += dp[i - 1][j - 1];
                    }
                }
            }
            return dp[N - 1][M - 1];
        }
    }

    // 本题测试链接 : https://leetcode.com/problems/distinct-subsequences-ii/
    public static class DistinctSubSeqValue {

        public static int distinctSubSeqII(String s) {
            if (s == null || s.length() == 0) {
                return 0;
            }
            long m = 1000000007;
            char[] str = s.toCharArray();
            long[] count = new long[26];
            long all = 1; // 算空集
            for (char x : str) {
                long add = (all - count[x - 'a'] + m) % m;
                all = (all + add) % m;
                count[x - 'a'] = (count[x - 'a'] + add) % m;
            }
            all = (all - 1 + m) % m;
            return (int) all;
        }

        public static int zuo(String s) {
            if (s == null || s.length() == 0) {
                return 0;
            }
            int m = 1000000007;
            char[] str = s.toCharArray();
            HashMap<Character, Integer> map = new HashMap<>();
            int all = 1; // 一个字符也没遍历的时候，有空集
            for (char x : str) {
                int newAdd = all;
//			int curAll = all + newAdd - (map.containsKey(x) ? map.get(x) : 0);
                int curAll = (all + newAdd) % m;
                curAll = (curAll - (map.containsKey(x) ? map.get(x) : 0) + m) % m;
                all = curAll;
                map.put(x, newAdd);
            }
            return all;
        }
    }

    // 本题测试链接 : https://leetcode.com/problems/shortest-bridge/
    public static class ShortestBridge {

        public static int shortestBridge(int[][] m) {
            int N = m.length;
            int M = m[0].length;
            int all = N * M;
            int island = 0;
            int[] curs = new int[all];
            int[] nexts = new int[all];
            int[][] records = new int[2][all];
            for (int i = 0; i < N; i++) {
                for (int j = 0; j < M; j++) {
                    if (m[i][j] == 1) { // 当前位置发现了1！
                        // 把这一片的1，都变成2，同时，抓上来了，这一片1组成的初始队列
                        // curs, 把这一片的1到自己的距离，都设置成1了，records
                        int queueSize = infect(m, i, j, N, M, curs, 0, records[island]);
                        int V = 1;
                        while (queueSize != 0) {
                            V++;
                            // curs里面的点，上下左右，records[点]==0， nexts
                            queueSize = bfs(N, M, all, V, curs, queueSize, nexts, records[island]);
                            int[] tmp = curs;
                            curs = nexts;
                            nexts = tmp;
                        }
                        island++;
                    }
                }
            }
            int min = Integer.MAX_VALUE;
            for (int i = 0; i < all; i++) {
                min = Math.min(min, records[0][i] + records[1][i]);
            }
            return min - 3;
        }

        // 当前来到m[i][j] , 总行数是N，总列数是M
        // m[i][j]感染出去(找到这一片岛所有的1),把每一个1的坐标，放入到int[] curs队列！
        // 1 (a,b) -> curs[index++] = (a * M + b)
        // 1 (c,d) -> curs[index++] = (c * M + d)
        // 二维已经变成一维了， 1 (a,b) -> a * M + b
        // 设置距离record[a * M +b ] = 1
        public static int infect(int[][] m, int i, int j, int N, int M, int[] curs, int index, int[] record) {
            if (i < 0 || i == N || j < 0 || j == M || m[i][j] != 1) {
                return index;
            }
            // m[i][j] 不越界，且m[i][j] == 1
            m[i][j] = 2;
            int p = i * M + j;
            record[p] = 1;
            // 收集到不同的1
            curs[index++] = p;
            index = infect(m, i - 1, j, N, M, curs, index, record);
            index = infect(m, i + 1, j, N, M, curs, index, record);
            index = infect(m, i, j - 1, N, M, curs, index, record);
            index = infect(m, i, j + 1, N, M, curs, index, record);
            return index;
        }

        // 二维原始矩阵中，N总行数，M总列数
        // all 总 all = N * M
        // V 要生成的是第几层 curs V-1 nexts V
        // record里面拿距离
        public static int bfs(int N, int M, int all, int V,
                              int[] curs, int size, int[] nexts, int[] record) {
            int nexti = 0; // 我要生成的下一层队列成长到哪了？
            for (int i = 0; i < size; i++) {
                // curs[i] -> 对应到二维数组中的位置，从而确定上下左右的位置信息
                int up = curs[i] < M ? -1 : curs[i] - M;
                int down = curs[i] + M >= all ? -1 : curs[i] + M;
                int left = curs[i] % M == 0 ? -1 : curs[i] - 1;
                int right = curs[i] % M == M - 1 ? -1 : curs[i] + 1;
                if (up != -1 && record[up] == 0) {
                    record[up] = V;
                    nexts[nexti++] = up;
                }
                if (down != -1 && record[down] == 0) {
                    record[down] = V;
                    nexts[nexti++] = down;
                }
                if (left != -1 && record[left] == 0) {
                    record[left] = V;
                    nexts[nexti++] = left;
                }
                if (right != -1 && record[right] == 0) {
                    record[right] = V;
                    nexts[nexti++] = right;
                }
            }
            return nexti;
        }
    }

    // 本题测试链接 : https://leetcode.com/problems/lru-cache/
    // 提交时把类名和构造方法名改成 : LRUCache
    public static class LRUCache {

        public LRUCache(int capacity) {
            cache = new MyCache<>(capacity);
        }

        private MyCache<Integer, Integer> cache;

        public int get(int key) {
            Integer ans = cache.get(key);
            return ans == null ? -1 : ans;
        }

        public void put(int key, int value) {
            cache.set(key, value);
        }

        public static class Node<K, V> {
            public K key;
            public V value;
            public Node<K, V> last;
            public Node<K, V> next;

            public Node(K key, V value) {
                this.key = key;
                this.value = value;
            }
        }

        public static class NodeDoubleLinkedList<K, V> {
            private Node<K, V> head;
            private Node<K, V> tail;

            public NodeDoubleLinkedList() {
                head = null;
                tail = null;
            }

            // 现在来了一个新的node，请挂到尾巴上去
            public void addNode(Node<K, V> newNode) {
                if (newNode == null) {
                    return;
                }
                if (head == null) {
                    head = newNode;
                    tail = newNode;
                } else {
                    tail.next = newNode;
                    newNode.last = tail;
                    tail = newNode;
                }
            }

            // node 入参，一定保证！node在双向链表里！
            // node原始的位置，左右重新连好，然后把node分离出来
            // 挂到整个链表的尾巴上
            public void moveNodeToTail(Node<K, V> node) {
                if (tail == node) {
                    return;
                }
                if (head == node) {
                    head = node.next;
                    head.last = null;
                } else {
                    node.last.next = node.next;
                    node.next.last = node.last;
                }
                node.last = tail;
                node.next = null;
                tail.next = node;
                tail = node;
            }

            public Node<K, V> removeHead() {
                if (head == null) {
                    return null;
                }
                Node<K, V> res = head;
                if (head == tail) {
                    head = null;
                    tail = null;
                } else {
                    head = res.next;
                    res.next = null;
                    head.last = null;
                }
                return res;
            }

        }

        public static class MyCache<K, V> {
            private HashMap<K, Node<K, V>> keyNodeMap;
            private NodeDoubleLinkedList<K, V> nodeList;
            private final int capacity;

            public MyCache(int cap) {
                keyNodeMap = new HashMap<K, Node<K, V>>();
                nodeList = new NodeDoubleLinkedList<K, V>();
                capacity = cap;
            }

            public V get(K key) {
                if (keyNodeMap.containsKey(key)) {
                    Node<K, V> res = keyNodeMap.get(key);
                    nodeList.moveNodeToTail(res);
                    return res.value;
                }
                return null;
            }

            // set(Key, Value)
            // 新增  更新value的操作
            public void set(K key, V value) {
                if (keyNodeMap.containsKey(key)) {
                    Node<K, V> node = keyNodeMap.get(key);
                    node.value = value;
                    nodeList.moveNodeToTail(node);
                } else { // 新增！
                    Node<K, V> newNode = new Node<K, V>(key, value);
                    keyNodeMap.put(key, newNode);
                    nodeList.addNode(newNode);
                    if (keyNodeMap.size() == capacity + 1) {
                        removeMostUnusedCache();
                    }
                }
            }

            private void removeMostUnusedCache() {
                Node<K, V> removeNode = nodeList.removeHead();
                keyNodeMap.remove(removeNode.key);
            }
        }
    }

    // 本题测试链接 : https://leetcode.com/problems/lfu-cache/
    // 提交时把类名和构造方法名改为 : LFUCache
    public static class LFUCache {

        public LFUCache(int K) {
            capacity = K;
            size = 0;
            records = new HashMap<>();
            heads = new HashMap<>();
            headList = null;
        }

        private int capacity; // 缓存的大小限制，即K
        private int size; // 缓存目前有多少个节点
        private HashMap<Integer, Node> records;// 表示key(Integer)由哪个节点(Node)代表
        private HashMap<Node, NodeList> heads; // 表示节点(Node)在哪个桶(NodeList)里
        private NodeList headList; // 整个结构中位于最左的桶

        // 节点的数据结构
        public static class Node {
            public Integer key;
            public Integer value;
            public Integer times; // 这个节点发生get或者set的次数总和
            public Node up; // 节点之间是双向链表所以有上一个节点
            public Node down;// 节点之间是双向链表所以有下一个节点

            public Node(int k, int v, int t) {
                key = k;
                value = v;
                times = t;
            }
        }

        // 桶结构
        public static class NodeList {
            public Node head; // 桶的头节点
            public Node tail; // 桶的尾节点
            public NodeList last; // 桶之间是双向链表所以有前一个桶
            public NodeList next; // 桶之间是双向链表所以有后一个桶

            public NodeList(Node node) {
                head = node;
                tail = node;
            }

            // 把一个新的节点加入这个桶，新的节点都放在顶端变成新的头部
            public void addNodeFromHead(Node newHead) {
                newHead.down = head;
                head.up = newHead;
                head = newHead;
            }

            // 判断这个桶是不是空的
            public boolean isEmpty() {
                return head == null;
            }

            // 删除node节点并保证node的上下环境重新连接
            public void deleteNode(Node node) {
                if (head == tail) {
                    head = null;
                    tail = null;
                } else {
                    if (node == head) {
                        head = node.down;
                        head.up = null;
                    } else if (node == tail) {
                        tail = node.up;
                        tail.down = null;
                    } else {
                        node.up.down = node.down;
                        node.down.up = node.up;
                    }
                }
                node.up = null;
                node.down = null;
            }
        }

        // removeNodeList：刚刚减少了一个节点的桶
        // 这个函数的功能是，判断刚刚减少了一个节点的桶是不是已经空了。
        // 1）如果不空，什么也不做
        //
        // 2)如果空了，removeNodeList还是整个缓存结构最左的桶(headList)。
        // 删掉这个桶的同时也要让最左的桶变成removeNodeList的下一个。
        //
        // 3)如果空了，removeNodeList不是整个缓存结构最左的桶(headList)。
        // 把这个桶删除，并保证上一个的桶和下一个桶之间还是双向链表的连接方式
        //
        // 函数的返回值表示刚刚减少了一个节点的桶是不是已经空了，空了返回true；不空返回false
        private boolean modifyHeadList(NodeList removeNodeList) {
            if (removeNodeList.isEmpty()) {
                if (headList == removeNodeList) {
                    headList = removeNodeList.next;
                    if (headList != null) {
                        headList.last = null;
                    }
                } else {
                    removeNodeList.last.next = removeNodeList.next;
                    if (removeNodeList.next != null) {
                        removeNodeList.next.last = removeNodeList.last;
                    }
                }
                return true;
            }
            return false;
        }

        // 函数的功能
        // node这个节点的次数+1了，这个节点原来在oldNodeList里。
        // 把node从oldNodeList删掉，然后放到次数+1的桶中
        // 整个过程既要保证桶之间仍然是双向链表，也要保证节点之间仍然是双向链表
        private void move(Node node, NodeList oldNodeList) {
            oldNodeList.deleteNode(node);
            // preList表示次数+1的桶的前一个桶是谁
            // 如果oldNodeList删掉node之后还有节点，oldNodeList就是次数+1的桶的前一个桶
            // 如果oldNodeList删掉node之后空了，oldNodeList是需要删除的，所以次数+1的桶的前一个桶，是oldNodeList的前一个
            NodeList preList = modifyHeadList(oldNodeList) ? oldNodeList.last : oldNodeList;
            // nextList表示次数+1的桶的后一个桶是谁
            NodeList nextList = oldNodeList.next;
            if (nextList == null) {
                NodeList newList = new NodeList(node);
                if (preList != null) {
                    preList.next = newList;
                }
                newList.last = preList;
                if (headList == null) {
                    headList = newList;
                }
                heads.put(node, newList);
            } else {
                if (nextList.head.times.equals(node.times)) {
                    nextList.addNodeFromHead(node);
                    heads.put(node, nextList);
                } else {
                    NodeList newList = new NodeList(node);
                    if (preList != null) {
                        preList.next = newList;
                    }
                    newList.last = preList;
                    newList.next = nextList;
                    nextList.last = newList;
                    if (headList == nextList) {
                        headList = newList;
                    }
                    heads.put(node, newList);
                }
            }
        }

        public void put(int key, int value) {
            if (capacity == 0) {
                return;
            }
            if (records.containsKey(key)) {
                Node node = records.get(key);
                node.value = value;
                node.times++;
                NodeList curNodeList = heads.get(node);
                move(node, curNodeList);
            } else {
                if (size == capacity) {
                    Node node = headList.tail;
                    headList.deleteNode(node);
                    modifyHeadList(headList);
                    records.remove(node.key);
                    heads.remove(node);
                    size--;
                }
                Node node = new Node(key, value, 1);
                if (headList == null) {
                    headList = new NodeList(node);
                } else {
                    if (headList.head.times.equals(node.times)) {
                        headList.addNodeFromHead(node);
                    } else {
                        NodeList newList = new NodeList(node);
                        newList.next = headList;
                        headList.last = newList;
                        headList = newList;
                    }
                }
                records.put(key, node);
                heads.put(node, headList);
                size++;
            }
        }

        public int get(int key) {
            if (!records.containsKey(key)) {
                return -1;
            }
            Node node = records.get(key);
            node.times++;
            NodeList curNodeList = heads.get(node);
            move(node, curNodeList);
            return node.value;
        }
    }

    // 超级水龙王问题和摩尔投票以及其扩展问题
    public static class FindKMajority {

        public static void printHalfMajor(int[] arr) {
            int cand = 0;
            int HP = 0;
            for (int i = 0; i < arr.length; i++) {
                if (HP == 0) {
                    cand = arr[i];
                    HP = 1;
                } else if (arr[i] == cand) {
                    HP++;
                } else {
                    HP--;
                }
            }
            if(HP == 0) {
                System.out.println("no such number.");
                return;
            }
            HP = 0;
            for (int i = 0; i < arr.length; i++) {
                if (arr[i] == cand) {
                    HP++;
                }
            }
            if (HP > arr.length / 2) {
                System.out.println(cand);
            } else {
                System.out.println("no such number.");
            }
        }

        public static void printKMajor(int[] arr, int K) {
            if (K < 2) {
                System.out.println("the value of K is invalid.");
                return;
            }
            // 攒候选，candidates，候选表，最多K-1条记录！ > N / K次的数字，最多有K-1个
            HashMap<Integer, Integer> candidates = new HashMap<Integer, Integer>();
            for (int i = 0; i != arr.length; i++) {
                if (candidates.containsKey(arr[i])) {
                    candidates.put(arr[i], candidates.get(arr[i]) + 1);
                } else { // arr[i] 不是候选
                    if (candidates.size() == K - 1) { // 当前数肯定不要！，每一个候选付出1点血量，血量变成0的候选，要删掉！
                        allCandidatesMinusOne(candidates);
                    } else {
                        candidates.put(arr[i], 1);
                    }
                }
            }
            // 所有可能的候选，都在candidates表中！遍历一遍arr，每个候选收集真实次数
            HashMap<Integer, Integer> reals = getReals(arr, candidates);
            boolean hasPrint = false;
            for (Map.Entry<Integer, Integer> set : candidates.entrySet()) {
                Integer key = set.getKey();
                if (reals.get(key) > arr.length / K) {
                    hasPrint = true;
                    System.out.print(key + " ");
                }
            }
            System.out.println(hasPrint ? "" : "no such number.");
        }

        public static void allCandidatesMinusOne(HashMap<Integer, Integer> map) {
            List<Integer> removeList = new LinkedList<Integer>();
            for (Map.Entry<Integer, Integer> set : map.entrySet()) {
                Integer key = set.getKey();
                Integer value = set.getValue();
                if (value == 1) {
                    removeList.add(key);
                }
                map.put(key, value - 1);
            }
            for (Integer removeKey : removeList) {
                map.remove(removeKey);
            }
        }

        public static HashMap<Integer, Integer> getReals(int[] arr, HashMap<Integer, Integer> cands) {
            HashMap<Integer, Integer> reals = new HashMap<Integer, Integer>();
            for (int i = 0; i != arr.length; i++) {
                int curNum = arr[i];
                if (cands.containsKey(curNum)) {
                    if (reals.containsKey(curNum)) {
                        reals.put(curNum, reals.get(curNum) + 1);
                    } else {
                        reals.put(curNum, 1);
                    }
                }
            }
            return reals;
        }
    }

    // 本题为求最小包含区间
    // 测试链接 :
    // https://leetcode.com/problems/smallest-range-covering-elements-from-k-lists/
    public static class MinRange {
        public static class Node {
            public int val;
            public int arr;
            public int idx;

            public Node(int value, int arrIndex, int index) {
                val = value;
                arr = arrIndex;
                idx = index;
            }
        }

        public static class NodeComparator implements Comparator<Node> {
            @Override
            public int compare(Node a, Node b) {
                return a.val != b.val ? (a.val - b.val) : (a.arr - b.arr);
            }
        }

        public static int[] smallestRange(List<List<Integer>> nums) {
            int N = nums.size();
            TreeSet<Node> set = new TreeSet<>(new NodeComparator());
            for (int i = 0; i < N; i++) {
                set.add(new Node(nums.get(i).get(0), i, 0));
            }
            int r = Integer.MAX_VALUE;
            int a = 0;
            int b = 0;
            while (set.size() == N) {
                Node max = set.last();
                Node min = set.pollFirst();
                if (max.val - min.val < r) {
                    r = max.val - min.val;
                    a = min.val;
                    b = max.val;
                }
                if (min.idx < nums.get(min.arr).size() - 1) {
                    set.add(new Node(nums.get(min.arr).get(min.idx + 1), min.arr, min.idx + 1));
                }
            }
            return new int[] { a, b };
        }

        // 以下为课堂题目，在main函数里有对数器
        public static int minRange1(int[][] m) {
            int min = Integer.MAX_VALUE;
            for (int i = 0; i < m[0].length; i++) {
                for (int j = 0; j < m[1].length; j++) {
                    for (int k = 0; k < m[2].length; k++) {
                        min = Math.min(min,
                                Math.abs(m[0][i] - m[1][j]) + Math.abs(m[1][j] - m[2][k]) + Math.abs(m[2][k] - m[0][i]));
                    }
                }
            }
            return min;
        }

        public static int minRange2(int[][] matrix) {
            int N = matrix.length;
            TreeSet<Node> set = new TreeSet<>(new NodeComparator());
            for (int i = 0; i < N; i++) {
                set.add(new Node(matrix[i][0], i, 0));
            }
            int min = Integer.MAX_VALUE;
            while (set.size() == N) {
                min = Math.min(min, set.last().val - set.first().val);
                Node cur = set.pollFirst();
                if (cur.idx < matrix[cur.arr].length - 1) {
                    set.add(new Node(matrix[cur.arr][cur.idx + 1], cur.arr, cur.idx + 1));
                }
            }
            return min << 1;
        }

        public static int[][] generateRandomMatrix(int n, int v) {
            int[][] m = new int[3][];
            int s = 0;
            for (int i = 0; i < 3; i++) {
                s = (int) (Math.random() * n) + 1;
                m[i] = new int[s];
                for (int j = 0; j < s; j++) {
                    m[i][j] = (int) (Math.random() * v);
                }
                Arrays.sort(m[i]);
            }
            return m;
        }
    }

    //题目二：
    //	企鹅厂每年都会发文化衫，文化衫有很多种，厂庆的时候，企鹅们都需要穿文化衫来拍照
    //  一次采访中，记者随机遇到的企鹅，企鹅会告诉记者还有多少企鹅跟他穿一种文化衫
    //  我们将这些回答放在 answers 数组里，返回鹅厂中企鹅的最少数量。
    //	输入: answers = [1]    输出：2
    //	输入: answers = [1, 1, 2]    输出：5
    //  Leetcode题目：https://leetcode.com/problems/rabbits-in-forest/
    public static int numRabbits(int[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        Arrays.sort(arr);
        int x = arr[0];
        int c = 1;
        int ans = 0;
        for (int i = 1; i < arr.length; i++) {
            if (x != arr[i]) {
                ans += ((c + x) / (x + 1)) * (x + 1);
                x = arr[i];
                c = 1;
            } else {
                c++;
            }
        }
        return ans + ((c + x) / (x + 1)) * (x + 1);
    }

    // leetcode链接：https://leetcode.cn/problems/valid-parentheses/
    public static class ValidParentheses {

        public static boolean isValid(String s) {
            if (s == null || s.length() == 0) {
                return true;
            }
            char[] str = s.toCharArray();
            int N = str.length;
            char[] stack = new char[N];
            int size = 0;
            for (int i = 0; i < N; i++) {
                char cha = str[i];
                if (cha == '(' || cha == '[' || cha == '{') {
                    stack[size++] = cha == '(' ? ')' : (cha == '[' ? ']' : '}');
                } else {
                    if (size == 0) {
                        return false;
                    }
                    char last = stack[--size];
                    if (cha != last) {
                        return false;
                    }
                }
            }
            return size == 0;
        }
    }

    // leetcode测试链接：https://leetcode.cn/problems/valid-sudoku/
    public static class ValidSudoku {

        public static boolean isValidSudoku(char[][] board) {
            boolean[][] row = new boolean[9][10];
            boolean[][] col = new boolean[9][10];
            boolean[][] bucket = new boolean[9][10];
            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    int bid = 3 * (i / 3) + (j / 3);
                    if (board[i][j] != '.') {
                        int num = board[i][j] - '0';
                        if (row[i][num] || col[j][num] || bucket[bid][num]) {
                            return false;
                        }
                        row[i][num] = true;
                        col[j][num] = true;
                        bucket[bid][num] = true;
                    }
                }
            }
            return true;
        }
    }

    // leetcode测试链接：https://leetcode.cn/problems/sudoku-solver/
    public static class SudokuSolver {

        public static void solveSudoku(char[][] board) {
            boolean[][] row = new boolean[9][10];
            boolean[][] col = new boolean[9][10];
            boolean[][] bucket = new boolean[9][10];
            initMaps(board, row, col, bucket);
            process(board, 0, 0, row, col, bucket);
        }

        public static void initMaps(char[][] board, boolean[][] row, boolean[][] col, boolean[][] bucket) {
            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    int bid = 3 * (i / 3) + (j / 3);
                    if (board[i][j] != '.') {
                        int num = board[i][j] - '0';
                        row[i][num] = true;
                        col[j][num] = true;
                        bucket[bid][num] = true;
                    }
                }
            }
        }

        //  当前来到(i,j)这个位置，如果已经有数字，跳到下一个位置上
        //                      如果没有数字，尝试1~9，不能和row、col、bucket冲突
        public static boolean process(char[][] board, int i, int j, boolean[][] row, boolean[][] col, boolean[][] bucket) {
            if (i == 9) {
                return true;
            }
            // 当离开(i，j)，应该去哪？(nexti, nextj)
            int nexti = j != 8 ? i : i + 1;
            int nextj = j != 8 ? j + 1 : 0;
            if (board[i][j] != '.') {
                return process(board, nexti, nextj, row, col, bucket);
            } else {
                // 可以尝试1~9
                int bid = 3 * (i / 3) + (j / 3);
                for (int num = 1; num <= 9; num++) { // 尝试每一个数字1~9
                    if ((!row[i][num]) && (!col[j][num]) && (!bucket[bid][num])) {
                        // 可以尝试num
                        row[i][num] = true;
                        col[j][num] = true;
                        bucket[bid][num] = true;
                        board[i][j] = (char) (num + '0');
                        if (process(board, nexti, nextj, row, col, bucket)) {
                            return true;
                        }
                        row[i][num] = false;
                        col[j][num] = false;
                        bucket[bid][num] = false;
                        board[i][j] = '.';
                    }
                }
                return false;
            }
        }
    }
}
