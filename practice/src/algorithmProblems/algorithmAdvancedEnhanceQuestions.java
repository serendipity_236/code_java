package algorithmProblems;

import dataStructure.Tree;

import java.util.*;

public class algorithmAdvancedEnhanceQuestions {

    // 给定一个路径统计数组paths，表示一张图。paths[i]==j代表城市i连向城市j，如果paths[i]==i，则表示i城市是首都，一张图里只会有一个首都且图中除首都指向自己之外不会有环。
    // 例如，paths=[9,1,4,9,0,4,8,9,0,1]，代表的图如图9-14所示。
    // 由数组表示的图可以知道，城市1是首都，所以距离为0，离首都距离为1的城市只有城市9，
    // 离首都距离为2的城市有城市0、3和7，
    // 离首都距离为3的城市有城市4和8，
    // 离首都距离为4的城市有城市2、5和6。
    // 所以距离为0的城市有1座，距离为1的城市有1座，距离为2的城市有3座，距离为3的城市有2座，距离为4的城市有3座。
    // 那么统计数组为nums=[1,1,3,2,3,0,0,0,0,0]
    public static class PathsToNums {
        public static void pathsToNums(int[] paths) {
            if (paths == null || paths.length == 0) {
                return;
            }
            // citiesPath -> distanceArray
            pathsToDistance(paths);
            // distanceArray -> countsArray
            distanceToCounts(paths);
        }

        public static void pathsToDistance(int[] paths) {
            int cap = 0;
            for (int start = 0; start < paths.length; start++) {
                if (paths[start] == start) {
                    cap = start;
                } else if (paths[start] > -1) {
                    int cur = paths[start];
                    paths[start] = -1;
                    int pre = start;
                    while (paths[cur] != cur) {
                        if (paths[cur] > -1) {
                            int next = paths[cur];
                            paths[cur] = pre;
                            pre = cur;
                            cur = next;
                        } else {
                            break;
                        }
                    }
                    int value = paths[cur] == cur ? 0 : paths[cur];
                    while (paths[pre] != -1) {
                        int lastPre = paths[pre];
                        paths[pre] = --value;
                        pre = lastPre;
                    }
                    paths[pre] = --value;
                }
            }
            paths[cap] = 0;
        }

        public static void distanceToCounts(int[] disArr) {
            for (int i = 0; i < disArr.length; i++) {
                int index = disArr[i];// index为负数，首都的index为0
                if (index < 0) {
                    disArr[i] = 0;
                    while (true) {
                        index = -index;
                        if (disArr[index] > -1) {
                            disArr[index]++;
                            break;
                        } else {
                            int nextIndex = disArr[index];
                            disArr[index] = 1;
                            index = nextIndex;
                        }
                    }
                }
            }
            disArr[0] = 1;
        }
    }

    // 一群孩子做游戏，现在请你根据游戏得分来发糖果，要求如下：
    // 1. 每个孩子不管得分多少，起码分到一个糖果。
    // 2. 任意两个相邻的孩子之间，得分较多的孩子必须拿多一些糖果。(若相同则无此限制)
    // 给定一个数组arr代表得分数组，请返回最少需要多少糖果。
    // [要求]
    // 时间复杂度为O(n), 空间复杂度为O(1)
    // 测试链接 : https://leetcode.com/problems/candy/
    public static class CandyProblem {
        // 这是原问题的优良解
        // 时间复杂度O(N)，额外空间复杂度O(N)
        public static int candy1(int[] arr) {
            if (arr == null || arr.length == 0) {
                return 0;
            }
            int N = arr.length;
            int[] left = new int[N];
            for (int i = 1; i < N; i++) {
                if (arr[i - 1] < arr[i]) {
                    left[i] = left[i - 1] + 1;
                }
            }
            int[] right = new int[N];
            for (int i = N - 2; i >= 0; i--) {
                if (arr[i] > arr[i + 1]) {
                    right[i] = right[i + 1] + 1;
                }
            }
            int ans = 0;
            for (int i = 0; i < N; i++) {
                ans += Math.max(left[i], right[i]);
            }
            return ans + N;
        }

        // 这是原问题空间优化后的解
        // 时间复杂度O(N)，额外空间复杂度O(1)
        public static int candy2(int[] arr) {
            if (arr == null || arr.length == 0) {
                return 0;
            }
            int index = nextMinIndex2(arr, 0);
            int res = rightCandies(arr, 0, index++);
            int lbase = 1;
            int next = 0;
            int rcands = 0;
            int rbase = 0;
            while (index != arr.length) {
                if (arr[index] > arr[index - 1]) {
                    res += ++lbase;
                    index++;
                } else if (arr[index] < arr[index - 1]) {
                    next = nextMinIndex2(arr, index - 1);
                    rcands = rightCandies(arr, index - 1, next++);
                    rbase = next - index + 1;
                    res += rcands + (rbase > lbase ? -lbase : -rbase);
                    lbase = 1;
                    index = next;
                } else {
                    res += 1;
                    lbase = 1;
                    index++;
                }
            }
            return res;
        }

        public static int nextMinIndex2(int[] arr, int start) {
            for (int i = start; i != arr.length - 1; i++) {
                if (arr[i] <= arr[i + 1]) {
                    return i;
                }
            }
            return arr.length - 1;
        }

        public static int rightCandies(int[] arr, int left, int right) {
            int n = right - left + 1;
            return n + n * (n - 1) / 2;
        }

        // 这是进阶问题的最优解，不要提交这个
        // 时间复杂度O(N), 额外空间复杂度O(1)
        public static int candy3(int[] arr) {
            if (arr == null || arr.length == 0) {
                return 0;
            }
            int index = nextMinIndex3(arr, 0);
            int[] data = rightCandiesAndBase(arr, 0, index++);
            int res = data[0];
            int lbase = 1;
            int same = 1;
            int next = 0;
            while (index != arr.length) {
                if (arr[index] > arr[index - 1]) {
                    res += ++lbase;
                    same = 1;
                    index++;
                } else if (arr[index] < arr[index - 1]) {
                    next = nextMinIndex3(arr, index - 1);
                    data = rightCandiesAndBase(arr, index - 1, next++);
                    if (data[1] <= lbase) {
                        res += data[0] - data[1];
                    } else {
                        res += -lbase * same + data[0] - data[1] + data[1] * same;
                    }
                    index = next;
                    lbase = 1;
                    same = 1;
                } else {
                    res += lbase;
                    same++;
                    index++;
                }
            }
            return res;
        }

        public static int nextMinIndex3(int[] arr, int start) {
            for (int i = start; i != arr.length - 1; i++) {
                if (arr[i] < arr[i + 1]) {
                    return i;
                }
            }
            return arr.length - 1;
        }

        public static int[] rightCandiesAndBase(int[] arr, int left, int right) {
            int base = 1;
            int cands = 1;
            for (int i = right - 1; i >= left; i--) {
                if (arr[i] == arr[i + 1]) {
                    cands += base;
                } else {
                    cands += ++base;
                }
            }
            return new int[]{cands, base};
        }

        public static void test() {
            int[] test1 = {3, 0, 5, 5, 4, 4, 0};
            System.out.println(candy2(test1));

            int[] test2 = {3, 0, 5, 5, 4, 4, 0};
            System.out.println(candy3(test2));
        }
    }

    // 给定一棵二叉树的头节点head，如果在某一个节点x上放置相机，那么x的父节点、x的所有子节点以及x都可以被覆盖。
    // 返回如果要把所有数都覆盖，至少需要多少个相机。
    // 本题测试链接 : https://leetcode.com/problems/binary-tree-cameras/
    public static class MinCameraCover {

        public static class TreeNode {
            public int value;
            public TreeNode left;
            public TreeNode right;
        }

        public static int minCameraCover1(TreeNode root) {
            Info data = process1(root);
            return (int) Math.min(data.uncovered + 1, Math.min(data.coveredNoCamera, data.coveredHasCamera));
        }

        // 潜台词：x是头节点，x下方的点都被覆盖的情况下
        public static class Info {
            public long uncovered; // x没相机，也没有被覆盖，x为头的树至少需要几个相机
            public long coveredNoCamera; // x被相机覆盖，但是x没相机，x为头的树至少需要几个相机
            public long coveredHasCamera; // x被相机覆盖了，并且x上放了相机，x为头的树至少需要几个相机

            public Info(long un, long no, long has) {
                uncovered = un;
                coveredNoCamera = no;
                coveredHasCamera = has;
            }
        }

        // 所有可能性都穷尽了
        public static Info process1(TreeNode X) {
            if (X == null) { // base case
                return new Info(Integer.MAX_VALUE, 0, Integer.MAX_VALUE);
            }

            Info left = process1(X.left);
            Info right = process1(X.right);
            // x uncovered x自己不被覆盖，x下方所有节点，都被覆盖
            // 左孩子： 左孩子没被覆盖，左孩子以下的点都被覆盖
            // 左孩子被覆盖但没相机，左孩子以下的点都被覆盖
            // 左孩子被覆盖也有相机，左孩子以下的点都被覆盖
            long uncovered = left.coveredNoCamera + right.coveredNoCamera;

            // x下方的点都被covered，x也被cover，但x上没相机
            long coveredNoCamera = Math.min(
                    // 1)
                    left.coveredHasCamera + right.coveredHasCamera,
                    Math.min(
                            // 2)
                            left.coveredHasCamera + right.coveredNoCamera,
                            // 3)
                            left.coveredNoCamera + right.coveredHasCamera));
            // x下方的点都被covered，x也被cover，且x上有相机
            long coveredHasCamera =
                    Math.min(left.uncovered, Math.min(left.coveredNoCamera, left.coveredHasCamera))
                            + Math.min(right.uncovered, Math.min(right.coveredNoCamera, right.coveredHasCamera))
                            + 1;
            return new Info(uncovered, coveredNoCamera, coveredHasCamera);
        }

        public static int minCameraCover2(TreeNode root) {
            Data data = process2(root);
            return data.cameras + (data.status == Status.UNCOVERED ? 1 : 0);
        }

        // 以x为头，x下方的节点都是被covered，x自己的状况，分三种
        public static enum Status {
            UNCOVERED, COVERED_NO_CAMERA, COVERED_HAS_CAMERA
        }

        // 以x为头，x下方的节点都是被covered，得到的最优解中：
        // x是什么状态，在这种状态下，需要至少几个相机
        public static class Data {
            public Status status;
            public int cameras;

            public Data(Status status, int cameras) {
                this.status = status;
                this.cameras = cameras;
            }
        }

        public static Data process2(TreeNode X) {
            if (X == null) {
                return new Data(Status.COVERED_NO_CAMERA, 0);
            }
            Data left = process2(X.left);
            Data right = process2(X.right);
            int cameras = left.cameras + right.cameras;

            // 左、或右，哪怕有一个没覆盖
            if (left.status == Status.UNCOVERED || right.status == Status.UNCOVERED) {
                return new Data(Status.COVERED_HAS_CAMERA, cameras + 1);
            }

            // 左右孩子，不存在没被覆盖的情况
            if (left.status == Status.COVERED_HAS_CAMERA || right.status == Status.COVERED_HAS_CAMERA) {
                return new Data(Status.COVERED_NO_CAMERA, cameras);
            }
            // 左右孩子，不存在没被覆盖的情况，也都没有相机
            return new Data(Status.UNCOVERED, cameras);
        }
    }

    // 牛牛和 15 个朋友来玩打土豪分田地的游戏，牛牛决定让你来分田地，地主的田地可以看成是一个矩形，每个位置有一个价值。
    // 分割田地的方法是横竖各切三刀，分成 16 份，作为领导干部，牛牛总是会选择其中总价值最小的一份田地，
    // 作为牛牛最好的朋友，你希望牛牛取得的田地的价值和尽可能大，你知道这个值最大可以是多少吗？
    // 输入描述:
    // 每个输入包含 1 个测试用例。
    // 每个测试用例的第一行包含两个整数 n 和 m（1 <= n, m <= 75），表示田地的大小，接下来的 n 行，每行包含 m 个 0-9 之间的数字，表示每块位置的价值。
    // 输出描述:
    // 输出一行表示牛牛所能取得的最大的价值。
    // 示例输入
    // 4 4
    // 3332
    // 3233
    // 3332
    // 2323
    // 示例输出
    // 2
    public static class NiuNiuSplitField {
        public static int maxMinSumIn16Field(int[][] matrix) {
            if (matrix == null || matrix.length < 4 || matrix[0].length < 4) {
                return 0;
            }
            // record[i][j]含义 左上角为(0,0)，右下角为(i,j)的子矩阵累加和是多少
            int[][] record = generateSumRecord(matrix);

            int col = matrix[0].length;
            int res = Integer.MIN_VALUE;
            for (int c1 = 0; c1 < col - 3; c1++) {
                for (int c2 = c1 + 1; c2 < col - 2; c2++) {
                    for (int c3 = c2 + 1; c3 < col - 1; c3++) {
                        res = Math.max(res, getBestDecision(record, c1, c2, c3));
                    }
                }
            }
            return res;
        }

        // record[i][j]含义 左上角为(0,0)，右下角为(i,j)的子矩阵累加和是多少
        private static int[][] generateSumRecord(int[][] matrix) {
            int row = matrix.length;
            int col = matrix[0].length;
            int[][] record = new int[row][col];
            record[0][0] = matrix[0][0];
            for (int i = 1; i < row; i++) {
                record[i][0] = record[i - 1][0] + matrix[i][0];
            }
            for (int i = 1; i < col; i++) {
                record[0][i] = record[0][i - 1] + matrix[0][i];
            }
            for (int i = 1; i < row; i++) {
                for (int j = 1; j < col; j++) {
                    record[i][j] = record[i][j - 1] + record[i - 1][j] - record[i - 1][j - 1] + matrix[i][j];
                }
            }
            return record;
        }

        private static int getBestDecision(int[][] record, int c1, int c2, int c3) {
            int[] up = getUpSplitArray(record, c1, c2, c3);
            int[] down = getDownSplitArray(record, c1, c2, c3);
            int res = Integer.MIN_VALUE;
            for (int mid = 1; mid < record.length - 2; mid++) {
                res = Math.max(res, Math.min(up[mid], down[mid + 1]));
            }
            return res;
        }

        public static int value(int[][] record, int c1, int c2, int c3, int prow, int crow) {
            int value1 = area(record, prow, 0, crow, c1);
            int value2 = area(record, prow, c1 + 1, crow, c2);
            int value3 = area(record, prow, c2 + 1, crow, c3);
            int value4 = area(record, prow, c3 + 1, crow, record[0].length - 1);
            return Math.min(Math.min(value1, value2), Math.min(value3, value4));
        }

        private static int area(int[][] record, int i1, int j1, int i2, int j2) {
            int all = record[i2][j2];
            int left = j1 > 0 ? record[i2][j1 - 1] : 0;
            int up = i1 > 0 ? record[i1 - 1][j2] : 0;
            int makeUp = (i1 > 0 && j1 > 0) ? record[i1 - 1][j1 - 1] : 0;
            return all - left - up + makeUp;
        }

        private static int[] getUpSplitArray(int[][] record, int c1, int c2, int c3) {
            int size = record.length;
            int[] up = new int[size];
            int split = 0;
            up[1] = Math.min(value(record, c1, c2, c3, 0, 0), value(record, c1, c2, c3, 1, 1));
            for (int i = 2; i < size; i++) {
                int minMax = toSubMatrixMin(record, c1, c2, c3, 0, split, i);
                while (split < i) {
                    if (split == i - 1) {
                        break;
                    }
                    int moved = toSubMatrixMin(record, c1, c2, c3, 0, split + 1, i);
                    if (moved < minMax) {
                        break;
                    } else {
                        minMax = moved;
                        split++;
                    }
                }
                up[i] = minMax;
            }
            return up;
        }

        private static int[] getDownSplitArray(int[][] record, int c1, int c2, int c3) {
            int size = record.length;
            int[] down = new int[size];
            int split = size - 1;
            down[split - 2] = Math.min(value(record, c1, c2, c3, size - 2, size - 2), value(record, c1, c2, c3, size - 1, size - 1));
            for (int i = size - 3; i >= 0; i--) {
                int minMax = toSubMatrixMin(record, c1, c2, c3, i, split - 1, size - 1);
                while (split > i) {
                    if (split == i + 1) {
                        break;
                    }
                    int moved = toSubMatrixMin(record, c1, c2, c3, i, split - 2, size - 1);
                    if (moved < minMax) {
                        break;
                    } else {
                        minMax = moved;
                        split--;
                    }
                }
                down[i] = minMax;
            }
            return down;
        }

        private static int toSubMatrixMin(int[][] record, int c1, int c2, int c3, int i, int i1, int i2) {

            return 0;
        }
    }

    // 本题测试链接 : https://leetcode.com/problems/bricks-falling-when-hit/
    public static class BricksFallingWhenHit {
        public static int[] hitBricks(int[][] grid, int[][] hits) {
            for (int i = 0; i < hits.length; i++) {
                if (grid[hits[i][0]][hits[i][1]] == 1) {
                    grid[hits[i][0]][hits[i][1]] = 2;
                }
            }
            UnionFind unionFind = new UnionFind(grid);
            int[] ans = new int[hits.length];
            for (int i = hits.length - 1; i >= 0; i--) {
                if (grid[hits[i][0]][hits[i][1]] == 2) {
                    ans[i] = unionFind.finger(hits[i][0], hits[i][1]);
                }
            }
            return ans;
        }

        // 并查集
        public static class UnionFind {
            private int N;// 行数
            private int M;// 列数
            private int cellingAll;// 有多少块砖，连到了天花板上
            private int[][] grid;// 原始矩阵，因为炮弹的影响，1 -> 2
            private boolean[] cellingSet;// cellingSet[i] = true; i 是头节点，所在的集合是天花板集合
            private int[] fatherMap;
            private int[] sizeMap;
            private int[] stack;// 模拟栈结构，找节点的代表节点时使用

            public UnionFind(int[][] matrix) {
                initSpace(matrix);
                // 把炮弹打完之后，是1的集合都各自连接到集合
                initConnect();
            }

            private void initSpace(int[][] matrix) {
                grid = matrix;
                N = grid.length;
                M = grid[0].length;
                int all = N * M;
                cellingAll = 0;
                cellingSet = new boolean[all];
                fatherMap = new int[all];
                sizeMap = new int[all];
                stack = new int[all];
                for (int row = 0; row < N; row++) {
                    for (int col = 0; col < M; col++) {
                        if (grid[row][col] == 1) {
                            int index = row * M + col;
                            fatherMap[index] = index;
                            sizeMap[index] = 1;
                            if (row == 0) {
                                cellingSet[index] = true;
                                cellingAll++;
                            }
                        }
                    }
                }
            }

            private void initConnect() {
                for (int row = 0; row < N; row++) {
                    for (int col = 0; col < M; col++) {
                        union(row, col, row - 1, col);
                        union(row, col, row + 1, col);
                        union(row, col, row, col - 1);
                        union(row, col, row, col + 1);
                    }
                }
            }

            private int find(int row, int col) {
                int stackSize = 0;
                int index = row * M + col;
                while (index != fatherMap[index]) {
                    stack[stackSize++] = index;
                    index = fatherMap[index];
                }
                // 把每一个节点的代表节点全部设置为同一个父，简化下次搜索的常数时间
                while (stackSize != 0) {
                    fatherMap[stack[--stackSize]] = index;
                }
                return index;
            }

            private void union(int r1, int c1, int r2, int c2) {
                if (valid(r1, c1) && valid(r2, c2)) {
                    int father1 = find(r1, c1);
                    int father2 = find(r2, c2);
                    if (father1 != father2) {
                        int size1 = sizeMap[father1];
                        int size2 = sizeMap[father2];
                        boolean status1 = cellingSet[father1];
                        boolean status2 = cellingSet[father2];
                        if (size1 <= size2) {
                            fatherMap[father1] = father2;
                            sizeMap[father2] = size1 + size2;
                            if (status1 ^ status2) {
                                cellingSet[father2] = true;
                                cellingAll += status1 ? size2 : size1;
                            }
                        } else {
                            fatherMap[father2] = father1;
                            sizeMap[father1] = size1 + size2;
                            if (status1 ^ status2) {
                                cellingSet[father1] = true;
                                cellingAll += status1 ? size2 : size1;
                            }
                        }
                    }
                }
            }

            private boolean valid(int row, int col) {
                return row >= 0 && row < N && col >= 0 && col < M && grid[row][col] == 1;
            }

            public int cellingNum() {
                return cellingAll;
            }

            public int finger(int row, int col) {
                grid[row][col] = 1;
                int cur = row * M + col;
                if (row == 0) {
                    cellingSet[cur] = true;
                    cellingAll++;
                }
                fatherMap[cur] = cur;
                sizeMap[cur] = 1;
                int pre = cellingNum();
                union(row, col, row - 1, col);
                union(row, col, row + 1, col);
                union(row, col, row, col - 1);
                union(row, col, row, col + 1);
                int now = cellingNum();
                if (row == 0) {
                    return now - pre;
                } else {
                    return now == pre ? 0 : now - pre - 1;
                }
            }
        }
    }

    // 给定一个字符串s，求s中有多少个字面值不同的子序列
    // 本题测试链接 : https://leetcode.com/problems/distinct-subsequences-ii/
    public static class DistinctSubSeqValue {
        public static int distinctSubSeqII(String s) {
            if (s == null || s.length() == 0) {
                return 0;
            }
            long m = 1000000007;
            char[] str = s.toCharArray();
            long[] count = new long[26];
            long all = 1; // 算空集
            for (char x : str) {
                long add = (all - count[x - 'a'] + m) % m;
                all = (all + add) % m;
                count[x - 'a'] = (count[x - 'a'] + add) % m;
            }
            all = (all - 1 + m) % m;
            return (int) all;
        }

        public static int f(String s) {
            if (s == null || s.length() == 0) {
                return 0;
            }
            int m = 1000000007;
            char[] str = s.toCharArray();
            HashMap<Character, Integer> map = new HashMap<>();
            int all = 1; // 一个字符也没遍历的时候，有空集
            for (char x : str) {
                int newAdd = all;
//			int curAll = all + newAdd - (map.containsKey(x) ? map.get(x) : 0);
                int curAll = all;
                curAll = (curAll + newAdd) % m;
                curAll = (curAll - (map.getOrDefault(x, 0)) + m) % m;
                all = curAll;
                map.put(x, newAdd);
            }
            return all;
        }

        public static void test() {
            String s = "aaa";
            System.out.println(distinctSubSeqII(s) + 1);
            System.out.println(f(s));
        }
    }

    // 给定一个字符串类型的数组strs，其中每个字符串的长度都一样，你的目标是让每一个字符串都变成从左往右字符不降序的样子。
    // 你可以删除所有字符串中i位置上的字符，但是一旦决定执行这个删除将对所有字符串生效，请返回最少操作次数


    // 给定数组arr和整数num，共返回多少个子数组满足如下情况：
    // max(arr[i...j]) - min(arr[i...j]) <= num
    // max(arr[i…j])表示子数组arr[i…j]中的最大值，min[arr[i…j])表示子数组arr[i…j]中的最小值。
    // 要求数组长度为N，请实现时间复杂度为O(N)的解法
    public static class AllLessNumSubArray {
        // 暴力的对数器方法
        public static int right(int[] arr, int sum) {
            if (arr == null || arr.length == 0 || sum < 0) {
                return 0;
            }
            int N = arr.length;
            int count = 0;
            for (int L = 0; L < N; L++) {
                for (int R = L; R < N; R++) {
                    int max = arr[L];
                    int min = arr[L];
                    for (int i = L + 1; i <= R; i++) {
                        max = Math.max(max, arr[i]);
                        min = Math.min(min, arr[i]);
                    }
                    if (max - min <= sum) {
                        count++;
                    }
                }
            }
            return count;
        }

        public static int num(int[] arr, int sum) {
            if (arr == null || arr.length == 0 || sum < 0) {
                return 0;
            }
            int N = arr.length;
            int count = 0;
            LinkedList<Integer> maxWindow = new LinkedList<>();
            LinkedList<Integer> minWindow = new LinkedList<>();
            int R = 0;
            for (int L = 0; L < N; L++) {
                while (R < N) {
                    while (!maxWindow.isEmpty() && arr[maxWindow.peekLast()] <= arr[R]) {
                        maxWindow.pollLast();
                    }
                    maxWindow.addLast(R);
                    while (!minWindow.isEmpty() && arr[minWindow.peekLast()] >= arr[R]) {
                        minWindow.pollLast();
                    }
                    minWindow.addLast(R);
                    if (arr[maxWindow.peekFirst()] - arr[minWindow.peekFirst()] > sum) {
                        break;
                    } else {
                        R++;
                    }
                }
                count += R - L;
                // 每次移动窗口的左边界，要把不可能的值移除
                if (maxWindow.peekFirst() == L) {
                    maxWindow.pollFirst();
                }
                if (minWindow.peekFirst() == L) {
                    minWindow.pollFirst();
                }
            }
            return count;
        }

        // for test
        public static int[] generateRandomArray(int maxLen, int maxValue) {
            int len = (int) (Math.random() * (maxLen + 1));
            int[] arr = new int[len];
            for (int i = 0; i < len; i++) {
                arr[i] = (int) (Math.random() * (maxValue + 1)) - (int) (Math.random() * (maxValue + 1));
            }
            return arr;
        }

        // for test
        public static void printArray(int[] arr) {
            if (arr != null) {
                for (int i = 0; i < arr.length; i++) {
                    System.out.print(arr[i] + " ");
                }
                System.out.println();
            }
        }

        public static void test() {
            int maxLen = 100;
            int maxValue = 200;
            int testTime = 100000;
            System.out.println("测试开始");
            for (int i = 0; i < testTime; i++) {
                int[] arr = generateRandomArray(maxLen, maxValue);
                int sum = (int) (Math.random() * (maxValue + 1));
                int ans1 = right(arr, sum);
                int ans2 = num(arr, sum);
                if (ans1 != ans2) {
                    System.out.println("Oops!");
                    printArray(arr);
                    System.out.println(sum);
                    System.out.println(ans1);
                    System.out.println(ans2);
                    break;
                }
            }
            System.out.println("测试结束");

        }
    }

    // 给定一个整型矩形map，其中的值只有0和1两种，求其中全是1的所有矩形区域中，最大的矩形区域为1的数量
    // 例如:
    // 1 1 1 0
    // 其中，最大的矩形区域有3个1,所以返回3。
    // 再如:
    // 1 0 1 1
    // 1 1 1 1
    // 1 1 1 0
    // 其中，最大的矩形区域有6个1，所以返回6
    // 测试链接：https://leetcode.com/problems/maximal-rectangle/
    public static class MaximalRectangle {

        public static int maximalRectangle(char[][] map) {
            if (map == null || map.length == 0 || map[0].length == 0) {
                return 0;
            }
            int maxArea = 0;
            int[] height = new int[map[0].length];
            for (int i = 0; i < map.length; i++) {
                for (int j = 0; j < map[0].length; j++) {
                    height[j] = map[i][j] == '0' ? 0 : height[j] + 1;
                }
                maxArea = Math.max(maxRecFromBottom(height), maxArea);
            }
            return maxArea;
        }

        // height是正方图数组
        public static int maxRecFromBottom(int[] height) {
            if (height == null || height.length == 0) {
                return 0;
            }
            int maxArea = 0;
            Stack<Integer> stack = new Stack<Integer>();
            for (int i = 0; i < height.length; i++) {
                while (!stack.isEmpty() && height[i] <= height[stack.peek()]) {
                    int j = stack.pop();
                    int k = stack.isEmpty() ? -1 : stack.peek();
                    int curArea = (i - k - 1) * height[j];
                    maxArea = Math.max(maxArea, curArea);
                }
                stack.push(i);
            }
            while (!stack.isEmpty()) {
                int j = stack.pop();
                int k = stack.isEmpty() ? -1 : stack.peek();
                int curArea = (height.length - k - 1) * height[j];
                maxArea = Math.max(maxArea, curArea);
            }
            return maxArea;
        }
    }

    // 给定一个非负整数N，返回N!结果的末尾为0的个数
    // 例如，3! = 6，结果末尾没有0，则返回值为0；5!=120，结果的末尾有1个0，所以返回值为1； 1000000000!，结果的末尾有249999998个0，返回249999998
    // 要数多少个0，就要看有多少个10相乘，从而就要看有多少个5和2相乘，而2的数量一定比5多，因此只需要看5的数量有多少个
    public static class CountZero {
        public static int zeroNum(int num) {
            if (num < 0) {
                return 0;
            }
            int ans = 0;
            while (num != 0) {
                ans += num / 5;
                num = num / 5;
            }
            return ans;
        }
    }

    // 给定一个非负数N，如果二进制数表达N！的结果，返回最低位的1在哪个位置上，认为最右的位置为位置0
    // 例如：1！ = 1，最低位的1在0位置上，2！=2，最低位的1在1位置上，1000000000!，最低位的1在999999987位置上
    public static class RightOne {
        public static int getRightestOne(int num) {
            if (num < 0) {
                return 0;
            }
            int ones = 0;
            int rightOne = 0;
            while (num != 0) {
                rightOne = num & (~num + 1);
                num -= rightOne;
                ones++;
            }
            return num - ones;
        }
    }

    // 给定一个整数n，返回1到n的数字中出现1的个数
    // 例如：
    // n=5，1~n：1，2，3，4，5   1出现了1次
    // n=11，1~n：1，2，3，4，5，6，7，8，9，10，11   1出现了4次
    public static class OneNumber {
        public static int solution1(int num) {
            if (num < 1) {
                return 0;
            }
            int count = 0;
            for (int i = 1; i != num + 1; i++) {
                count += get1Nums(i);
            }
            return count;
        }

        public static int get1Nums(int num) {
            int res = 0;
            while (num != 0) {
                if (num % 10 == 1) {
                    res++;
                }
                num /= 10;
            }
            return res;
        }

        // 测试链接 :
        // https://leetcode.cn/problems/1nzheng-shu-zhong-1chu-xian-de-ci-shu-lcof/
        // 提交如下方法可以直接通过
        public static int countDigitOne(int num) {
            if (num < 1) {
                return 0;
            }
            // num -> 13625
            // len = 5位数
            int len = getLenOfNum(num);
            if (len == 1) {
                return 1;
            }
            // num 13625
            // mod 10000
            int mod = powerBaseOf10(len - 1);
            // num最高位 num / mod
            int first = num / mod;
            // 最高位位1的数量
            int firstOneNum = first == 1 ? num % mod + 1 : mod;
            // 除去最高位之外，剩下1的数量
            // 最高位first 10(k-2次方) * (k-1) * first
            // mod = 10^（k-1）
            int otherOneNum = first * (len - 1) * (mod / 10);
            return firstOneNum + otherOneNum + countDigitOne(num % mod);
        }

        // 求num的位数
        public static int getLenOfNum(int num) {
            int len = 0;
            while (num != 0) {
                len++;
                num /= 10;
            }
            return len;
        }

        public static int powerBaseOf10(int base) {
            return (int) Math.pow(10, base);
        }

        public static void test() {
            int num = 50000000;
            long start1 = System.currentTimeMillis();
            System.out.println(solution1(num));
            long end1 = System.currentTimeMillis();
            System.out.println("cost time: " + (end1 - start1) + " ms");

            long start2 = System.currentTimeMillis();
            System.out.println(countDigitOne(num));
            long end2 = System.currentTimeMillis();
            System.out.println("cost time: " + (end2 - start2) + " ms");
        }
    }

    // 给定无序数组arr，返回其中最长的连续序列的长度
    public static class LongestContinuousSubSequence {
        public static int longestContinuousSubSequence(int[] arr) {
            // key是区间开始，value是区间长度
            HashMap<Integer, Integer> mapHead = new HashMap<>();
            // key是区间结束，value是区间长度
            HashMap<Integer, Integer> mapTail = new HashMap<>();
            for (int i = 0; i < arr.length; i++) {
                mapHead.put(arr[i], 1);
                mapTail.put(arr[i], 1);
                if (mapHead.containsKey(arr[i] - 1)) {
                    mapHead.put(arr[i] - 1, mapHead.get(arr[i] - 1) + 1);
                    mapHead.remove(arr[i]);
                    mapTail.put(arr[i], mapTail.get(arr[i]) + 1);
                    mapTail.remove(arr[i] - 1);
                }
                if (mapTail.containsKey(arr[i] + 1)) {
                    mapTail.put(arr[i] + 1, mapTail.get(arr[i] + 1) + 1);
                    mapTail.remove(arr[i]);
                    mapHead.put(arr[i], mapHead.get(arr[i]) + 1);
                    mapHead.remove(arr[i] + 1);
                }
            }
            int maxLen = 0;
            for (Map.Entry<Integer, Integer> entry : mapHead.entrySet()) {
                if (maxLen < entry.getValue()) {
                    maxLen = entry.getValue();
                }
            }
            return maxLen;
        }

        public static int longestContinuousSubSequence2(int[] arr) {
            if (arr == null || arr.length == 0) {
                return 0;
            }
            HashSet<Integer> set = new HashSet<>();
            for (int num : arr) {
                set.add(num);
            }
            int maxLen = 0;
            for (int num : set) {
                int end = num + 1;
                if (set.contains(num + 1)) {
                    while (set.contains(end)) {
                        end++;
                    }
                }
                maxLen = Math.max(maxLen, end - num);
            }
            return maxLen;
        }
    }

    // 后缀数组
    // 使用DC3算法生成
    public static class DC3 {

        public int[] sa;// 后缀数组排名

        public int[] rank;// 后缀数组排名的下标(原数组中的)

        public int[] height;

        // 构造方法的约定:
        // 数组叫nums，如果你是字符串，请转成整型数组nums
        // 数组中，最小值>=1
        // 如果不满足，处理成满足的，也不会影响使用
        // max, nums里面最大值是多少
        public DC3(int[] nums, int max) {
            sa = sa(nums, max);
            rank = rank();
            height = height(nums);
        }

        private int[] sa(int[] nums, int max) {
            int n = nums.length;
            int[] arr = new int[n + 3];
            for (int i = 0; i < n; i++) {
                arr[i] = nums[i];
            }
            return skew(arr, n, max);
        }

        private int[] skew(int[] nums, int n, int K) {
            int n0 = (n + 2) / 3, n1 = (n + 1) / 3, n2 = n / 3, n02 = n0 + n2;
            int[] s12 = new int[n02 + 3], sa12 = new int[n02 + 3];
            for (int i = 0, j = 0; i < n + (n0 - n1); ++i) {
                if (0 != i % 3) {
                    s12[j++] = i;
                }
            }
            radixPass(nums, s12, sa12, 2, n02, K);
            radixPass(nums, sa12, s12, 1, n02, K);
            radixPass(nums, s12, sa12, 0, n02, K);
            int name = 0, c0 = -1, c1 = -1, c2 = -1;
            for (int i = 0; i < n02; ++i) {
                if (c0 != nums[sa12[i]] || c1 != nums[sa12[i] + 1] || c2 != nums[sa12[i] + 2]) {
                    name++;
                    c0 = nums[sa12[i]];
                    c1 = nums[sa12[i] + 1];
                    c2 = nums[sa12[i] + 2];
                }
                if (1 == sa12[i] % 3) {
                    s12[sa12[i] / 3] = name;
                } else {
                    s12[sa12[i] / 3 + n0] = name;
                }
            }
            if (name < n02) {
                sa12 = skew(s12, n02, name);
                for (int i = 0; i < n02; i++) {
                    s12[sa12[i]] = i + 1;
                }
            } else {
                for (int i = 0; i < n02; i++) {
                    sa12[s12[i] - 1] = i;
                }
            }
            int[] s0 = new int[n0], sa0 = new int[n0];
            for (int i = 0, j = 0; i < n02; i++) {
                if (sa12[i] < n0) {
                    s0[j++] = 3 * sa12[i];
                }
            }
            radixPass(nums, s0, sa0, 0, n0, K);
            int[] sa = new int[n];
            for (int p = 0, t = n0 - n1, k = 0; k < n; k++) {
                int i = sa12[t] < n0 ? sa12[t] * 3 + 1 : (sa12[t] - n0) * 3 + 2;
                int j = sa0[p];
                if (sa12[t] < n0 ? leq(nums[i], s12[sa12[t] + n0], nums[j], s12[j / 3])
                        : leq(nums[i], nums[i + 1], s12[sa12[t] - n0 + 1], nums[j], nums[j + 1], s12[j / 3 + n0])) {
                    sa[k] = i;
                    t++;
                    if (t == n02) {
                        for (k++; p < n0; p++, k++) {
                            sa[k] = sa0[p];
                        }
                    }
                } else {
                    sa[k] = j;
                    p++;
                    if (p == n0) {
                        for (k++; t < n02; t++, k++) {
                            sa[k] = sa12[t] < n0 ? sa12[t] * 3 + 1 : (sa12[t] - n0) * 3 + 2;
                        }
                    }
                }
            }
            return sa;
        }

        private void radixPass(int[] nums, int[] input, int[] output, int offset, int n, int k) {
            int[] cnt = new int[k + 1];
            for (int i = 0; i < n; ++i) {
                cnt[nums[input[i] + offset]]++;
            }
            for (int i = 0, sum = 0; i < cnt.length; ++i) {
                int t = cnt[i];
                cnt[i] = sum;
                sum += t;
            }
            for (int i = 0; i < n; ++i) {
                output[cnt[nums[input[i] + offset]]++] = input[i];
            }
        }

        private boolean leq(int a1, int a2, int b1, int b2) {
            return a1 < b1 || (a1 == b1 && a2 <= b2);
        }

        private boolean leq(int a1, int a2, int a3, int b1, int b2, int b3) {
            return a1 < b1 || (a1 == b1 && leq(a2, a3, b2, b3));
        }

        private int[] rank() {
            int n = sa.length;
            int[] ans = new int[n];
            for (int i = 0; i < n; i++) {
                ans[sa[i]] = i;
            }
            return ans;
        }

        private int[] height(int[] s) {
            int n = s.length;
            int[] ans = new int[n];
            for (int i = 0, k = 0; i < n; ++i) {
                if (rank[i] != 0) {
                    if (k > 0) {
                        --k;
                    }
                    int j = sa[rank[i] - 1];
                    while (i + k < n && j + k < n && s[i + k] == s[j + k]) {
                        ++k;
                    }
                    ans[rank[i]] = k;
                }
            }
            return ans;
        }

        // 为了测试
        public static int[] randomArray(int len, int maxValue) {
            int[] arr = new int[len];
            for (int i = 0; i < len; i++) {
                arr[i] = (int) (Math.random() * maxValue) + 1;
            }
            return arr;
        }

        // 为了测试
        public static void test() {
            int len = 100000;
            int maxValue = 100;
            long start = System.currentTimeMillis();
            DC3 dc3 = new DC3(randomArray(len, maxValue), maxValue);
            //DC3 dc3 = new DC3(new int[]{1,5,6,3,7,8}, 8);
            //int[] rank = dc3.rank;
            //int[] sa = dc3.sa;
            System.out.println();
            long end = System.currentTimeMillis();
            System.out.println("数据量 " + len + ", 运行时间 " + (end - start) + " ms");
        }
    }

    // 后缀数组实例
    // 给定两个长度分别位M和N的整型数组nums1和nums2，其中每个值都不大于9，在给定一个整数k。
    // 你可以在nums1和nums2中挑选数字，要求一共挑选k个，并且都要从左往右挑选。返回所有可能的结果中，代表最大k位数字的结果
    // 例如：
    // 输入： nums1[3,4,6,5]   nums2[9,1,2,5,8,3]
    // 输出： output[9,8,6,5,3]
    // 测试链接: https://leetcode.com/problems/create-maximum-number/
    public static class CreateMaximumNumber {
        public static int[] maxNumber(int[] nums1, int[] nums2, int k) {
            int len1 = nums1.length;
            int len2 = nums2.length;
            if (k < 0 || k > len1 + len2) {
                return null;
            }
            int[] res = new int[k];
            int[][] dp1 = getDp(nums1);
            int[][] dp2 = getDp(nums2);
            for (int get1 = Math.max(0, k - len2); get1 <= Math.min(k, len1); get1++) {
                int[] pick1 = maxPick(nums1, dp1, get1);
                int[] pick2 = maxPick(nums2, dp2, k - get1);
                int[] merge = mergeBySuffixArray(pick1, pick2);
                res = moreThan(res, merge) ? res : merge;
            }
            return res;
        }

        public static boolean moreThan(int[] pre, int[] last) {
            int i = 0;
            int j = 0;
            while (i < pre.length && j < last.length && pre[i] == last[j]) {
                i++;
                j++;
            }
            return j == last.length || (i < pre.length && pre[i] > last[j]);
        }

        public static int[] mergeBySuffixArray(int[] nums1, int[] nums2) {
            int size1 = nums1.length;
            int size2 = nums2.length;
            int[] nums = new int[size1 + 1 + size2];
            for (int i = 0; i < size1; i++) {
                nums[i] = nums1[i] + 2;
            }
            nums[size1] = 1;
            for (int j = 0; j < size2; j++) {
                nums[j + size1 + 1] = nums2[j] + 2;
            }
            DC3 dc3 = new DC3(nums, 11);
            int[] rank = dc3.rank;
            int[] ans = new int[size1 + size2];
            int i = 0;
            int j = 0;
            int r = 0;
            while (i < size1 && j < size2) {
                ans[r++] = rank[i] > rank[j + size1 + 1] ? nums1[i++] : nums2[j++];
            }
            while (i < size1) {
                ans[r++] = nums1[i++];
            }
            while (j < size2) {
                ans[r++] = nums2[j++];
            }
            return ans;
        }

        public static int[][] getDp(int[] arr) {
            int size = arr.length; // 0~N-1
            int pick = arr.length + 1; // 1 ~ N
            int[][] dp = new int[size][pick];
            // get 不从0开始，因为拿0个无意义
            for (int get = 1; get < pick; get++) { // 1 ~ N
                int maxIndex = size - get;
                // i~N-1
                for (int i = size - get; i >= 0; i--) {
                    if (arr[i] >= arr[maxIndex]) {
                        maxIndex = i;
                    }
                    dp[i][get] = maxIndex;
                }
            }
            return dp;
        }

        public static int[] maxPick(int[] arr, int[][] dp, int pick) {
            int[] res = new int[pick];
            for (int resIndex = 0, dpRow = 0; pick > 0; pick--, resIndex++) {
                res[resIndex] = arr[dp[dpRow][pick]];
                dpRow = dp[dpRow][pick] + 1;
            }
            return res;
        }
    }

    // 给定一个数组arr，还有两个数low和upper(low<=upper)，返回累加和在[low，upper]之间的子数组的数量
    public static class CountOfRangeSum {
        public static int countRangeSum1(int[] nums, int lower, int upper) {
            int n = nums.length;
            long[] sums = new long[n + 1];
            for (int i = 0; i < n; ++i)
                sums[i + 1] = sums[i] + nums[i];
            return countWhileMergeSort(sums, 0, n + 1, lower, upper);
        }

        private static int countWhileMergeSort(long[] sums, int start, int end, int lower, int upper) {
            if (end - start <= 1)
                return 0;
            int mid = (start + end) / 2;
            int count = countWhileMergeSort(sums, start, mid, lower, upper)
                    + countWhileMergeSort(sums, mid, end, lower, upper);
            int j = mid, k = mid, t = mid;
            long[] cache = new long[end - start];
            for (int i = start, r = 0; i < mid; ++i, ++r) {
                while (k < end && sums[k] - sums[i] < lower)
                    k++;
                while (j < end && sums[j] - sums[i] <= upper)
                    j++;
                while (t < end && sums[t] < sums[i])
                    cache[r++] = sums[t++];
                cache[r] = sums[i];
                count += j - k;
            }
            System.arraycopy(cache, 0, sums, start, t - start);
            return count;
        }

        public static class SBTNode {
            public long key;
            public SBTNode l;
            public SBTNode r;
            public long size; // 不同key的size
            public long all; // 总的size

            public SBTNode(long k) {
                key = k;
                size = 1;
                all = 1;
            }
        }

        public static class SizeBalancedTreeSet {
            private SBTNode root;
            private HashSet<Long> set = new HashSet<>();

            private SBTNode rightRotate(SBTNode cur) {
                long same = cur.all - (cur.l != null ? cur.l.all : 0) - (cur.r != null ? cur.r.all : 0);
                SBTNode leftNode = cur.l;
                cur.l = leftNode.r;
                leftNode.r = cur;
                leftNode.size = cur.size;
                cur.size = (cur.l != null ? cur.l.size : 0) + (cur.r != null ? cur.r.size : 0) + 1;
                // all modify
                leftNode.all = cur.all;
                cur.all = (cur.l != null ? cur.l.all : 0) + (cur.r != null ? cur.r.all : 0) + same;
                return leftNode;
            }

            private SBTNode leftRotate(SBTNode cur) {
                long same = cur.all - (cur.l != null ? cur.l.all : 0) - (cur.r != null ? cur.r.all : 0);
                SBTNode rightNode = cur.r;
                cur.r = rightNode.l;
                rightNode.l = cur;
                rightNode.size = cur.size;
                cur.size = (cur.l != null ? cur.l.size : 0) + (cur.r != null ? cur.r.size : 0) + 1;
                // all modify
                rightNode.all = cur.all;
                cur.all = (cur.l != null ? cur.l.all : 0) + (cur.r != null ? cur.r.all : 0) + same;
                return rightNode;
            }

            private SBTNode maintain(SBTNode cur) {
                if (cur == null) {
                    return null;
                }
                long leftSize = cur.l != null ? cur.l.size : 0;
                long leftLeftSize = cur.l != null && cur.l.l != null ? cur.l.l.size : 0;
                long leftRightSize = cur.l != null && cur.l.r != null ? cur.l.r.size : 0;
                long rightSize = cur.r != null ? cur.r.size : 0;
                long rightLeftSize = cur.r != null && cur.r.l != null ? cur.r.l.size : 0;
                long rightRightSize = cur.r != null && cur.r.r != null ? cur.r.r.size : 0;
                if (leftLeftSize > rightSize) {
                    cur = rightRotate(cur);
                    cur.r = maintain(cur.r);
                    cur = maintain(cur);
                } else if (leftRightSize > rightSize) {
                    cur.l = leftRotate(cur.l);
                    cur = rightRotate(cur);
                    cur.l = maintain(cur.l);
                    cur.r = maintain(cur.r);
                    cur = maintain(cur);
                } else if (rightRightSize > leftSize) {
                    cur = leftRotate(cur);
                    cur.l = maintain(cur.l);
                    cur = maintain(cur);
                } else if (rightLeftSize > leftSize) {
                    cur.r = rightRotate(cur.r);
                    cur = leftRotate(cur);
                    cur.l = maintain(cur.l);
                    cur.r = maintain(cur.r);
                    cur = maintain(cur);
                }
                return cur;
            }

            private SBTNode add(SBTNode cur, long key, boolean contains) {
                if (cur == null) {
                    return new SBTNode(key);
                } else {
                    cur.all++;
                    if (key == cur.key) {
                        return cur;
                    } else { // 还在左滑或者右滑
                        if (!contains) {
                            cur.size++;
                        }
                        if (key < cur.key) {
                            cur.l = add(cur.l, key, contains);
                        } else {
                            cur.r = add(cur.r, key, contains);
                        }
                        return maintain(cur);
                    }
                }
            }

            public void add(long sum) {
                boolean contains = set.contains(sum);
                root = add(root, sum, contains);
                set.add(sum);
            }

            public long lessKeySize(long key) {
                SBTNode cur = root;
                long ans = 0;
                while (cur != null) {
                    if (key == cur.key) {
                        return ans + (cur.l != null ? cur.l.all : 0);
                    } else if (key < cur.key) {
                        cur = cur.l;
                    } else {
                        ans += cur.all - (cur.r != null ? cur.r.all : 0);
                        cur = cur.r;
                    }
                }
                return ans;
            }

            // > 7 8...
            // <8 ...<=7
            public long moreKeySize(long key) {
                return root != null ? (root.all - lessKeySize(key + 1)) : 0;
            }

        }

        public static int countRangeSum2(int[] nums, int lower, int upper) {
            // 黑盒，加入数字（前缀和），不去重，可以接受重复数字
            // < num , 有几个数？
            SizeBalancedTreeSet treeSet = new SizeBalancedTreeSet();
            long sum = 0;
            int ans = 0;
            treeSet.add(0);// 一个数都没有的时候，就已经有一个前缀和累加和为0，
            for (int i = 0; i < nums.length; i++) {
                sum += nums[i];
                // [sum - upper, sum - lower]
                // [10, 20] ?
                // < 10 ?  < 21 ?
                long a = treeSet.lessKeySize(sum - lower + 1);
                long b = treeSet.lessKeySize(sum - upper);
                ans += a - b;
                treeSet.add(sum);
            }
            return ans;
        }

        // for test
        public static void printArray(int[] arr) {
            for (int i = 0; i < arr.length; i++) {
                System.out.print(arr[i] + " ");
            }
            System.out.println();
        }

        // for test
        public static int[] generateArray(int len, int varible) {
            int[] arr = new int[len];
            for (int i = 0; i < arr.length; i++) {
                arr[i] = (int) (Math.random() * varible);
            }
            return arr;
        }

        public static void test() {
            int len = 200;
            int variable = 50;
            for (int i = 0; i < 10000; i++) {
                int[] test = generateArray(len, variable);
                int lower = (int) (Math.random() * variable) - (int) (Math.random() * variable);
                int upper = lower + (int) (Math.random() * variable);
                int ans1 = countRangeSum1(test, lower, upper);
                int ans2 = countRangeSum2(test, lower, upper);
                if (ans1 != ans2) {
                    printArray(test);
                    System.out.println(lower);
                    System.out.println(upper);
                    System.out.println(ans1);
                    System.out.println(ans2);
                }
            }
        }
    }

    // 给定一个整数n，找到离n最近的(不包括n本身)的回文数的数
    // 最近是指和n做差的绝对值最小
    public static class FindTheClosestPalindrome {

        public static String nearestPalindromic(String n) {
            Long num = Long.valueOf(n);
            Long raw = getRawPalindrome(n);
            Long big = raw > num ? raw : getBigPalindrome(raw);
            Long small = raw < num ? raw : getSmallPalindrome(raw);
            return String.valueOf(big - num >= num - small ? small : big);
        }

        // 对称轴不变，只改变对称轴后面的数字
        public static Long getRawPalindrome(String n) {
            char[] chs = n.toCharArray();
            int len = chs.length;
            for (int i = 0; i < len / 2; i++) {
                chs[len - 1 - i] = chs[i];
            }
            return Long.valueOf(String.valueOf(chs));
        }

        // 对称轴位置++
        public static Long getBigPalindrome(Long raw) {
            char[] chs = String.valueOf(raw).toCharArray();
            char[] res = new char[chs.length + 1];
            res[0] = '0';
            for (int i = 0; i < chs.length; i++) {
                res[i + 1] = chs[i];
            }
            int size = chs.length;
            for (int j = (size - 1) / 2 + 1; j >= 0; j--) {
                if (++res[j] > '9') {
                    res[j] = '0';
                } else {
                    break;
                }
            }
            int offset = res[0] == '1' ? 1 : 0;
            size = res.length;
            for (int i = size - 1; i >= (size + offset) / 2; i--) {
                res[i] = res[size - i - offset];
            }
            return Long.valueOf(String.valueOf(res));
        }

        // 对称轴位置--
        public static Long getSmallPalindrome(Long raw) {
            char[] chs = String.valueOf(raw).toCharArray();
            char[] res = new char[chs.length];
            int size = res.length;
            for (int i = 0; i < size; i++) {
                res[i] = chs[i];
            }
            for (int j = (size - 1) / 2; j >= 0; j--) {
                if (--res[j] < '0') {
                    res[j] = '9';
                } else {
                    break;
                }
            }
            if (res[0] == '0') {
                res = new char[size - 1];
                for (int i = 0; i < res.length; i++) {
                    res[i] = '9';
                }
                return size == 1 ? 0 : Long.parseLong(String.valueOf(res));
            }
            for (int k = 0; k < size / 2; k++) {
                res[size - 1 - k] = res[k];
            }
            return Long.valueOf(String.valueOf(res));
        }
    }

    // 一条直线上有居民点，邮局只能建在居民点上。给定一个有序整形数组arr，每个值表示居民点的一维坐标，再给定一个正数num，表示邮局数量。
    // 选择num个居民点建立num个邮局，使所有的居民点到邮局的总距离最短，返回最短的总距离。
    // 例如：
    // 输入：
    // 6 2
    // 1 2 3 4 5 1000
    // 输出：
    // 6
    // 第一个邮局建立在3位置，第二个邮局建立在1000位置。那么1位置到邮局的距离为2,2位置到邮局距离为1,3位置到邮局的距离为0，4位置到邮局的距离为1,
    // 5位置到邮局的距离为2,1000位置到邮局的距离为0.这种方案下的总距离为6。
    // 四边形不等式优化条件
    // 1.属于区间划分问题的动态规划
    // 2.变化的指标，一个变一个不变对于答案有单调性的关系
    // 3.求一个dp[i][j]的值时，该位置不会同时依赖本行或者本列的问题
    // 如果该位置只依赖本行的值，枚举的时候按照列从左到右，每一行从下到上，该位置的左边和下边分别是枚举的下界和上界
    // 如果该位置只依赖本列的值，枚举的时候按照行从上到下，每一列从右到作左，该位置的上边和右边分别是枚举的下界和上界
    public static class PostOfficeProblem {

        public static int min1(int[] arr, int num) {
            if (arr == null || num < 1 || arr.length < num) {
                return 0;
            }
            int N = arr.length;
            int[][] w = new int[N + 1][N + 1];
            for (int L = 0; L < N; L++) {
                for (int R = L + 1; R < N; R++) {
                    w[L][R] = w[L][R - 1] + arr[R] - arr[(L + R) >> 1];
                }
            }
            int[][] dp = new int[N][num + 1];
            for (int i = 0; i < N; i++) {
                dp[i][1] = w[0][i];
            }
            for (int i = 1; i < N; i++) {
                for (int j = 2; j <= Math.min(i, num); j++) {
                    int ans = Integer.MAX_VALUE;
                    for (int k = 0; k <= i; k++) {
                        ans = Math.min(ans, dp[k][j - 1] + w[k + 1][i]);
                    }
                    dp[i][j] = ans;
                }
            }
            return dp[N - 1][num];
        }

        public static int min2(int[] arr, int num) {
            if (arr == null || num < 1 || arr.length < num) {
                return 0;
            }
            int N = arr.length;
            int[][] w = new int[N + 1][N + 1];
            for (int L = 0; L < N; L++) {
                for (int R = L + 1; R < N; R++) {
                    w[L][R] = w[L][R - 1] + arr[R] - arr[(L + R) >> 1];
                }
            }
            int[][] dp = new int[N][num + 1];
            int[][] best = new int[N][num + 1];
            for (int i = 0; i < N; i++) {
                dp[i][1] = w[0][i];
                best[i][1] = -1;
            }
            for (int j = 2; j <= num; j++) {
                for (int i = N - 1; i >= j; i--) {
                    int down = best[i][j - 1];
                    int up = i == N - 1 ? N - 1 : best[i + 1][j];
                    int ans = Integer.MAX_VALUE;
                    int bestChoose = -1;
                    for (int leftEnd = down; leftEnd <= up; leftEnd++) {
                        int leftCost = leftEnd == -1 ? 0 : dp[leftEnd][j - 1];
                        int rightCost = leftEnd == i ? 0 : w[leftEnd + 1][i];
                        int cur = leftCost + rightCost;
                        if (cur <= ans) {
                            ans = cur;
                            bestChoose = leftEnd;
                        }
                    }
                    dp[i][j] = ans;
                    best[i][j] = bestChoose;
                }
            }
            return dp[N - 1][num];
        }

        // for test
        public static int[] randomSortedArray(int len, int range) {
            int[] arr = new int[len];
            for (int i = 0; i != len; i++) {
                arr[i] = (int) (Math.random() * range);
            }
            Arrays.sort(arr);
            return arr;
        }

        // for test
        public static void printArray(int[] arr) {
            for (int i = 0; i != arr.length; i++) {
                System.out.print(arr[i] + " ");
            }
            System.out.println();
        }

        // for test
        public static void test() {
            int N = 30;
            int maxValue = 100;
            int testTime = 10000;
            System.out.println("测试开始");
            for (int i = 0; i < testTime; i++) {
                int len = (int) (Math.random() * N) + 1;
                int[] arr = randomSortedArray(len, maxValue);
                int num = (int) (Math.random() * N) + 1;
                int ans1 = min1(arr, num);
                int ans2 = min2(arr, num);
                if (ans1 != ans2) {
                    printArray(arr);
                    System.out.println(num);
                    System.out.println(ans1);
                    System.out.println(ans2);
                    System.out.println("Oops!");
                }
            }
            System.out.println("测试结束");
        }
    }

    // AC自动机
    public static class AC1 {
        public static class Node {
            public int end; // 有多少个字符串以该节点结尾
            public Node fail;
            public Node[] nexts;

            public Node() {
                end = 0;
                fail = null;
                nexts = new Node[26];
            }
        }

        public static class ACAutomation {
            private Node root;

            public ACAutomation() {
                root = new Node();
            }

            // 你有多少个匹配串，就调用多少次insert
            public void insert(String s) {
                char[] str = s.toCharArray();
                Node cur = root;
                int index = 0;
                for (int i = 0; i < str.length; i++) {
                    index = str[i] - 'a';
                    if (cur.nexts[index] == null) {
                        Node next = new Node();
                        cur.nexts[index] = next;
                    }
                    cur = cur.nexts[index];
                }
                cur.end++;
            }

            public void build() {
                Queue<Node> queue = new LinkedList<>();
                queue.add(root);
                Node cur = null;
                Node cfail = null;
                while (!queue.isEmpty()) {
                    cur = queue.poll(); // 父
                    for (int i = 0; i < 26; i++) { // 下级所有的路
                        if (cur.nexts[i] != null) { // 该路下有子节点
                            cur.nexts[i].fail = root; // 初始时先设置一个值
                            cfail = cur.fail;
                            while (cfail != null) { // cur不是头节点
                                if (cfail.nexts[i] != null) {
                                    cur.nexts[i].fail = cfail.nexts[i];
                                    break;
                                }
                                cfail = cfail.fail;
                            }
                            queue.add(cur.nexts[i]);
                        }
                    }
                }
            }

            public int containNum(String content) {
                char[] str = content.toCharArray();
                Node cur = root;
                Node follow = null;
                int index = 0;
                int ans = 0;
                for (int i = 0; i < str.length; i++) {
                    index = str[i] - 'a';
                    while (cur.nexts[index] == null && cur != root) {
                        cur = cur.fail;
                    }
                    cur = cur.nexts[index] != null ? cur.nexts[index] : root;
                    follow = cur;
                    while (follow != root) {
                        if (follow.end == -1) {
                            break;
                        }
                        { // 不同的需求，在这一段{ }之间修改
                            ans += follow.end;
                            follow.end = -1;
                        } // 不同的需求，在这一段{ }之间修改
                        follow = follow.fail;
                    }
                }
                return ans;
            }

        }

        public static void main(String[] args) {
            ACAutomation ac = new ACAutomation();
            ac.insert("dhe");
            ac.insert("he");
            ac.insert("c");
            ac.build();
            System.out.println(ac.containNum("cdhe"));
        }
    }

    public static class AC2 {
        // 前缀树的节点
        public static class Node {
            // 如果一个node，end为空，不是结尾
            // 如果end不为空，表示这个点是某个字符串的结尾，end的值就是这个字符串
            public String end;
            // 只有在上面的end变量不为空的时候，endUse才有意义
            // 表示，这个字符串之前有没有加入过答案
            public boolean endUse;
            public Node fail;
            public Node[] nexts;

            public Node() {
                endUse = false;
                end = null;
                fail = null;
                nexts = new Node[26];
            }
        }

        public static class ACAutomation {
            private Node root;

            public ACAutomation() {
                root = new Node();
            }

            public void insert(String s) {
                char[] str = s.toCharArray();
                Node cur = root;
                int index = 0;
                for (int i = 0; i < str.length; i++) {
                    index = str[i] - 'a';
                    if (cur.nexts[index] == null) {
                        cur.nexts[index] = new Node();
                    }
                    cur = cur.nexts[index];
                }
                cur.end = s;
            }

            public void build() {
                Queue<Node> queue = new LinkedList<>();
                queue.add(root);
                Node cur = null;
                Node cfail = null;
                while (!queue.isEmpty()) {
                    // 某个父亲，cur
                    cur = queue.poll();
                    for (int i = 0; i < 26; i++) { // 所有的路
                        // cur -> 父亲  i号儿子，必须把i号儿子的fail指针设置好！
                        if (cur.nexts[i] != null) { // 如果真的有i号儿子
                            cur.nexts[i].fail = root;
                            cfail = cur.fail;
                            while (cfail != null) {
                                if (cfail.nexts[i] != null) {
                                    cur.nexts[i].fail = cfail.nexts[i];
                                    break;
                                }
                                cfail = cfail.fail;
                            }
                            queue.add(cur.nexts[i]);
                        }
                    }
                }
            }

            // 大文章：content
            public List<String> containWords(String content) {
                char[] str = content.toCharArray();
                Node cur = root;
                Node follow = null;
                int index = 0;
                List<String> ans = new ArrayList<>();
                for (int i = 0; i < str.length; i++) {
                    index = str[i] - 'a'; // 路
                    // 如果当前字符在这条路上没配出来，就随着fail方向走向下条路径
                    while (cur.nexts[index] == null && cur != root) {
                        cur = cur.fail;
                    }
                    // 1) 现在来到的路径，是可以继续匹配的
                    // 2) 现在来到的节点，就是前缀树的根节点
                    cur = cur.nexts[index] != null ? cur.nexts[index] : root;
                    follow = cur;
                    while (follow != root) {
                        if (follow.endUse) {
                            break;
                        }
                        // 不同的需求，在这一段之间修改
                        if (follow.end != null) {
                            ans.add(follow.end);
                            follow.endUse = true;
                        }
                        // 不同的需求，在这一段之间修改
                        follow = follow.fail;
                    }
                }
                return ans;
            }

        }

        public static void main(String[] args) {
            ACAutomation ac = new ACAutomation();
            ac.insert("dhe");
            ac.insert("he");
            ac.insert("abcdheks");
            // 设置fail指针
            ac.build();
            List<String> contains = ac.containWords("abcdhekskdjfafhasldkflskdjhwqaeruv");
            for (String word : contains) {
                System.out.println(word);
            }
        }
    }

    // 线段树
    public static class SegmentTreeCode {

        public static class SegmentTree {
            // arr[]为原序列的信息从0开始，但在arr里是从1开始的
            // sum[]模拟线段树维护区间和
            // lazy[]为累加和懒惰标记
            // change[]为更新的值
            // update[]为更新慵懒标记
            private int MAXN;
            private int[] arr;
            private int[] sum;
            private int[] lazy;
            private int[] change;
            private boolean[] update;

            public SegmentTree(int[] origin) {
                MAXN = origin.length + 1;
                arr = new int[MAXN]; // arr[0] 不用 从1开始使用
                for (int i = 1; i < MAXN; i++) {
                    arr[i] = origin[i - 1];
                }
                sum = new int[MAXN << 2]; // 用来支持脑补概念中，某一个范围的累加和信息
                lazy = new int[MAXN << 2]; // 用来支持脑补概念中，某一个范围沒有往下傳遞的纍加任務
                change = new int[MAXN << 2]; // 用来支持脑补概念中，某一个范围有没有更新操作的任务
                update = new boolean[MAXN << 2]; // 用来支持脑补概念中，某一个范围更新任务，更新成了什么
            }

            private void pushUp(int rt) {
                sum[rt] = sum[rt << 1] + sum[rt << 1 | 1];
            }

            // 之前的，所有懒增加，和懒更新，从父范围，发给左右两个子范围
            // 分发策略是什么
            // ln表示左子树元素结点个数，rn表示右子树结点个数
            private void pushDown(int rt, int ln, int rn) {
                if (update[rt]) {
                    update[rt << 1] = true;
                    update[rt << 1 | 1] = true;
                    change[rt << 1] = change[rt];
                    change[rt << 1 | 1] = change[rt];
                    lazy[rt << 1] = 0;
                    lazy[rt << 1 | 1] = 0;
                    sum[rt << 1] = change[rt] * ln;
                    sum[rt << 1 | 1] = change[rt] * rn;
                    update[rt] = false;
                }
                if (lazy[rt] != 0) {
                    lazy[rt << 1] += lazy[rt];
                    sum[rt << 1] += lazy[rt] * ln;
                    lazy[rt << 1 | 1] += lazy[rt];
                    sum[rt << 1 | 1] += lazy[rt] * rn;
                    lazy[rt] = 0;
                }
            }

            // 在初始化阶段，先把sum数组，填好
            // 在arr[l~r]范围上，去build，1~N，
            // rt : 这个范围在sum中的下标
            public void build(int l, int r, int rt) {
                if (l == r) {
                    sum[rt] = arr[l];
                    return;
                }
                int mid = (l + r) >> 1;
                build(l, mid, rt << 1);
                build(mid + 1, r, rt << 1 | 1);
                pushUp(rt);
            }


            // L~R  所有的值变成C
            // l~r  rt
            public void update(int L, int R, int C, int l, int r, int rt) {
                if (L <= l && r <= R) {
                    update[rt] = true;
                    change[rt] = C;
                    sum[rt] = C * (r - l + 1);
                    lazy[rt] = 0;
                    return;
                }
                // 当前任务躲不掉，无法懒更新，要往下发
                int mid = (l + r) >> 1;
                pushDown(rt, mid - l + 1, r - mid);
                if (L <= mid) {
                    update(L, R, C, l, mid, rt << 1);
                }
                if (R > mid) {
                    update(L, R, C, mid + 1, r, rt << 1 | 1);
                }
                pushUp(rt);
            }

            // L~R, C 任务！
            // rt，l~r
            public void add(int L, int R, int C, int l, int r, int rt) {
                // 任务如果把此时的范围全包了！
                if (L <= l && r <= R) {
                    sum[rt] += C * (r - l + 1);
                    lazy[rt] += C;
                    return;
                }
                // 任务没有把你全包！
                // l  r  mid = (l+r)/2
                int mid = (l + r) >> 1;
                pushDown(rt, mid - l + 1, r - mid);
                // L~R
                if (L <= mid) {
                    add(L, R, C, l, mid, rt << 1);
                }
                if (R > mid) {
                    add(L, R, C, mid + 1, r, rt << 1 | 1);
                }
                pushUp(rt);
            }

            // 1~6 累加和是多少？ 1~8 rt
            public long query(int L, int R, int l, int r, int rt) {
                if (L <= l && r <= R) {
                    return sum[rt];
                }
                int mid = (l + r) >> 1;
                pushDown(rt, mid - l + 1, r - mid);
                long ans = 0;
                if (L <= mid) {
                    ans += query(L, R, l, mid, rt << 1);
                }
                if (R > mid) {
                    ans += query(L, R, mid + 1, r, rt << 1 | 1);
                }
                return ans;
            }

        }

        public static class Right {
            public int[] arr;

            public Right(int[] origin) {
                arr = new int[origin.length + 1];
                for (int i = 0; i < origin.length; i++) {
                    arr[i + 1] = origin[i];
                }
            }

            public void update(int L, int R, int C) {
                for (int i = L; i <= R; i++) {
                    arr[i] = C;
                }
            }

            public void add(int L, int R, int C) {
                for (int i = L; i <= R; i++) {
                    arr[i] += C;
                }
            }

            public long query(int L, int R) {
                long ans = 0;
                for (int i = L; i <= R; i++) {
                    ans += arr[i];
                }
                return ans;
            }

        }

        public static int[] generateRandomArray(int len, int max) {
            int size = (int) (Math.random() * len) + 1;
            int[] origin = new int[size];
            for (int i = 0; i < size; i++) {
                origin[i] = (int) (Math.random() * max) - (int) (Math.random() * max);
            }
            return origin;
        }

        public static boolean test() {
            int len = 100;
            int max = 1000;
            int testTimes = 5000;
            int addOrUpdateTimes = 1000;
            int queryTimes = 500;
            for (int i = 0; i < testTimes; i++) {
                int[] origin = generateRandomArray(len, max);
                SegmentTree seg = new SegmentTree(origin);
                int S = 1;
                int N = origin.length;
                int root = 1;
                seg.build(S, N, root);
                Right rig = new Right(origin);
                for (int j = 0; j < addOrUpdateTimes; j++) {
                    int num1 = (int) (Math.random() * N) + 1;
                    int num2 = (int) (Math.random() * N) + 1;
                    int L = Math.min(num1, num2);
                    int R = Math.max(num1, num2);
                    int C = (int) (Math.random() * max) - (int) (Math.random() * max);
                    if (Math.random() < 0.5) {
                        seg.add(L, R, C, S, N, root);
                        rig.add(L, R, C);
                    } else {
                        seg.update(L, R, C, S, N, root);
                        rig.update(L, R, C);
                    }
                }
                for (int k = 0; k < queryTimes; k++) {
                    int num1 = (int) (Math.random() * N) + 1;
                    int num2 = (int) (Math.random() * N) + 1;
                    int L = Math.min(num1, num2);
                    int R = Math.max(num1, num2);
                    long ans1 = seg.query(L, R, S, N, root);
                    long ans2 = rig.query(L, R);
                    if (ans1 != ans2) {
                        return false;
                    }
                }
            }
            return true;
        }

        public static void main(String[] args) {
            int[] origin = {2, 1, 1, 2, 3, 4, 5};
            SegmentTree seg = new SegmentTree(origin);
            int S = 1; // 整个区间的开始位置，规定从1开始，不从0开始 -> 固定
            int N = origin.length; // 整个区间的结束位置，规定能到N，不是N-1 -> 固定
            int root = 1; // 整棵树的头节点位置，规定是1，不是0 -> 固定
            int L = 2; // 操作区间的开始位置 -> 可变
            int R = 5; // 操作区间的结束位置 -> 可变
            int C = 4; // 要加的数字或者要更新的数字 -> 可变
            // 区间生成，必须在[S,N]整个范围上build
            seg.build(S, N, root);
            // 区间修改，可以改变L、R和C的值，其他值不可改变
            seg.add(L, R, C, S, N, root);
            // 区间更新，可以改变L、R和C的值，其他值不可改变
            seg.update(L, R, C, S, N, root);
            // 区间查询，可以改变L和R的值，其他值不可改变
            long sum = seg.query(L, R, S, N, root);
            System.out.println(sum);

            System.out.println("对数器测试开始...");
            System.out.println("测试结果 : " + (test() ? "通过" : "未通过"));
        }
    }

    // TSP问题
    public static class TSP {

        public static int t1(int[][] matrix) {
            int N = matrix.length; // 0...N-1
            // set
            // set.get(i) != null i这座城市在集合里
            // set.get(i) == null i这座城市不在集合里
            List<Integer> set = new ArrayList<>();
            for (int i = 0; i < N; i++) {
                set.add(1);
            }
            return func1(matrix, set, 0);
        }

        // 任何两座城市之间的距离，可以在matrix里面拿到
        // set中表示着哪些城市的集合，
        // start这座城一定在set里，
        // 从start出发，要把set中所有的城市过一遍，最终回到0这座城市，最小距离是多少
        public static int func1(int[][] matrix, List<Integer> set, int start) {
            int cityNum = 0;
            for (int i = 0; i < set.size(); i++) {
                if (set.get(i) != null) {
                    cityNum++;
                }
            }
            if (cityNum == 1) {
                return matrix[start][0];
            }
            // cityNum > 1  不只start这一座城
            set.set(start, null);
            int min = Integer.MAX_VALUE;
            for (int i = 0; i < set.size(); i++) {
                if (set.get(i) != null) {
                    // start -> i i... -> 0
                    int cur = matrix[start][i] + func1(matrix, set, i);
                    min = Math.min(min, cur);
                }
            }
            set.set(start, 1);
            return min;
        }

        public static int t2(int[][] matrix) {
            int N = matrix.length; // 0...N-1
            // 7座城 1111111
            int allCity = (1 << N) - 1;
            return f2(matrix, allCity, 0);
        }

        // 任何两座城市之间的距离，可以在matrix里面拿到
        // set中表示着哪些城市的集合，
        // start这座城一定在set里，
        // 从start出发，要把set中所有的城市过一遍，最终回到0这座城市，最小距离是多少
        public static int f2(int[][] matrix, int cityStatus, int start) {
            // cityStatus == cityStatux & (~cityStaus + 1)

            if (cityStatus == (cityStatus & (~cityStatus + 1))) {
                return matrix[start][0];
            }

            // 把start位的1去掉，
            cityStatus &= (~(1 << start));
            int min = Integer.MAX_VALUE;
            // 枚举所有的城市
            for (int move = 0; move < matrix.length; move++) {
                if ((cityStatus & (1 << move)) != 0) {
                    int cur = matrix[start][move] + f2(matrix, cityStatus, move);
                    min = Math.min(min, cur);
                }
            }
            cityStatus |= (1 << start);
            return min;
        }

        public static int t3(int[][] matrix) {
            int N = matrix.length; // 0...N-1
            // 7座城 1111111
            int allCity = (1 << N) - 1;
            int[][] dp = new int[1 << N][N];
            for (int i = 0; i < (1 << N); i++) {
                for (int j = 0; j < N; j++) {
                    dp[i][j] = -1;
                }
            }
            return f3(matrix, allCity, 0, dp);
        }

        // 任何两座城市之间的距离，可以在matrix里面拿到
        // set中表示着哪些城市的集合，
        // start这座城一定在set里，
        // 从start出发，要把set中所有的城市过一遍，最终回到0这座城市，最小距离是多少
        public static int f3(int[][] matrix, int cityStatus, int start, int[][] dp) {
            if (dp[cityStatus][start] != -1) {
                return dp[cityStatus][start];
            }
            if (cityStatus == (cityStatus & (~cityStatus + 1))) {
                dp[cityStatus][start] = matrix[start][0];
            } else {
                // 把start位的1去掉，
                cityStatus &= (~(1 << start));
                int min = Integer.MAX_VALUE;
                // 枚举所有的城市
                for (int move = 0; move < matrix.length; move++) {
                    if (move != start && (cityStatus & (1 << move)) != 0) {
                        int cur = matrix[start][move] + f3(matrix, cityStatus, move, dp);
                        min = Math.min(min, cur);
                    }
                }
                cityStatus |= (1 << start);
                dp[cityStatus][start] = min;
            }
            return dp[cityStatus][start];
        }

        public static int t4(int[][] matrix) {
            int N = matrix.length; // 0...N-1
            int statusNums = 1 << N;
            int[][] dp = new int[statusNums][N];

            for (int status = 0; status < statusNums; status++) {
                for (int start = 0; start < N; start++) {
                    if ((status & (1 << start)) != 0) {
                        if (status == (status & (~status + 1))) {
                            dp[status][start] = matrix[start][0];
                        } else {
                            int min = Integer.MAX_VALUE;
                            // start 城市在status里去掉之后，的状态
                            int preStatus = status & (~(1 << start));
                            // start -> i
                            for (int i = 0; i < N; i++) {
                                if ((preStatus & (1 << i)) != 0) {
                                    int cur = matrix[start][i] + dp[preStatus][i];
                                    min = Math.min(min, cur);
                                }
                            }
                            dp[status][start] = min;
                        }
                    }
                }
            }
            return dp[statusNums - 1][0];
        }

        // matrix[i][j] -> i城市到j城市的距离
        public static int tsp1(int[][] matrix, int origin) {
            if (matrix == null || matrix.length < 2 || origin < 0 || origin >= matrix.length) {
                return 0;
            }
            // 要考虑的集合
            ArrayList<Integer> cities = new ArrayList<>();
            // cities[0] != null 表示0城在集合里
            // cities[i] != null 表示i城在集合里
            for (int i = 0; i < matrix.length; i++) {
                cities.add(1);
            }
            // null,1,1,1,1,1,1
            // origin城不参与集合
            cities.set(origin, null);
            return process(matrix, origin, cities, origin);
        }

        // matrix 所有距离，存在其中
        // origin 固定参数，唯一的目标
        // cities 要考虑的集合，一定不含有origin
        // 当前来到的城市是谁，cur
        public static int process(int[][] matrix, int aim, ArrayList<Integer> cities, int cur) {
            boolean hasCity = false; // 集团中还是否有城市
            int ans = Integer.MAX_VALUE;
            for (int i = 0; i < cities.size(); i++) {
                if (cities.get(i) != null) {
                    hasCity = true;
                    cities.set(i, null);
                    // matrix[cur][i] + f(i, 集团(去掉i) )
                    ans = Math.min(ans, matrix[cur][i] + process(matrix, aim, cities, i));
                    cities.set(i, 1);
                }
            }
            return hasCity ? ans : matrix[cur][aim];
        }

        // cities 里，一定含有cur这座城
        // 解决的是，集合从cur出发，通过集合里所有的城市，最终来到aim，最短距离
        public static int process2(int[][] matrix, int aim, ArrayList<Integer> cities, int cur) {
            if (cities.size() == 1) {
                return matrix[cur][aim];
            }
            cities.set(cur, null);
            int ans = Integer.MAX_VALUE;
            for (int i = 0; i < cities.size(); i++) {
                if (cities.get(i) != null) {
                    int dis = matrix[cur][i] + process2(matrix, aim, cities, i);
                    ans = Math.min(ans, dis);
                }
            }
            cities.set(cur, 1);
            return ans;
        }

        public static int tsp2(int[][] matrix, int origin) {
            if (matrix == null || matrix.length < 2 || origin < 0 || origin >= matrix.length) {
                return 0;
            }
            int N = matrix.length - 1; // 除去origin之后是n-1个点
            int S = 1 << N; // 状态数量
            int[][] dp = new int[S][N];
            int icity = 0;
            int kcity = 0;
            for (int i = 0; i < N; i++) {
                icity = i < origin ? i : i + 1;
                // 00000000 i
                dp[0][i] = matrix[icity][origin];
            }
            for (int status = 1; status < S; status++) {
                // 尝试每一种状态 status = 0 0 1 0 0 0 0 0 0
                // 下标 8 7 6 5 4 3 2 1 0
                for (int i = 0; i < N; i++) {
                    // i 枚举的出发城市
                    dp[status][i] = Integer.MAX_VALUE;
                    if ((1 << i & status) != 0) {
                        // 如果i这座城是可以枚举的，i = 6 ， i对应的原始城的编号，icity
                        icity = i < origin ? i : i + 1;
                        for (int k = 0; k < N; k++) { // i 这一步连到的点，k
                            if ((1 << k & status) != 0) { // i 这一步可以连到k
                                kcity = k < origin ? k : k + 1; // k对应的原始城的编号，kcity
                                dp[status][i] = Math.min(dp[status][i], dp[status ^ (1 << i)][k] + matrix[icity][kcity]);
                            }
                        }
                    }
                }
            }
            int ans = Integer.MAX_VALUE;
            for (int i = 0; i < N; i++) {
                icity = i < origin ? i : i + 1;
                ans = Math.min(ans, dp[S - 1][i] + matrix[origin][icity]);
            }
            return ans;
        }

        public static int[][] generateGraph(int maxSize, int maxValue) {
            int len = (int) (Math.random() * maxSize) + 1;
            int[][] matrix = new int[len][len];
            for (int i = 0; i < len; i++) {
                for (int j = 0; j < len; j++) {
                    matrix[i][j] = (int) (Math.random() * maxValue) + 1;
                }
            }
            for (int i = 0; i < len; i++) {
                matrix[i][i] = 0;
            }
            return matrix;
        }

        public static void test() {
            int len = 10;
            int value = 100;
            System.out.println("功能测试开始");
            for (int i = 0; i < 20000; i++) {
                int[][] matrix = generateGraph(len, value);
                int origin = (int) (Math.random() * matrix.length);
                int ans1 = t3(matrix);
                int ans2 = t4(matrix);
                int ans3 = tsp2(matrix, origin);
                if (ans1 != ans2 || ans1 != ans3) {
                    System.out.println("fuck");
                }
            }
            System.out.println("功能测试结束");

            len = 22;
            System.out.println("性能测试开始，数据规模 : " + len);
            int[][] matrix = new int[len][len];
            for (int i = 0; i < len; i++) {
                for (int j = 0; j < len; j++) {
                    matrix[i][j] = (int) (Math.random() * value) + 1;
                }
            }
            for (int i = 0; i < len; i++) {
                matrix[i][i] = 0;
            }
            long start;
            long end;
            start = System.currentTimeMillis();
            t4(matrix);
            end = System.currentTimeMillis();
            System.out.println("运行时间 : " + (end - start) + " 毫秒");
            System.out.println("性能测试结束");

        }
    }

    public static class PavingTile {
        // 有2 * n的一个长方形方格，用一个1 * 2的骨牌铺满方格
        // 编写一个程序，试对给出的任意一个n(n>0), 输出铺法总数。
        public static int way(int N){
            if(N <= 0){
                return -1;
            }
            if(N < 3){
                return N;
            }
            int first = 1;
            int second = 2;
            int ans = 0;
            while(N - 1 >= 2){
                ans = first + second;
                first = second;
                second = ans;
                N--;
            }
            return ans;
        }

        // 有m * n的一个长方形方格，用一个1 * 2的骨牌铺满方格
        // 编写一个程序，试对给出的任意一个n(n>0), 输出铺法总数。
        public static int ways1(int N, int M) {
            if (N < 1 || M < 1 || ((N * M) & 1) != 0) {
                return 0;
            }
            if (N == 1 || M == 1) {
                return 1;
            }
            int[] pre = new int[M]; // pre代表-1行的状况
            Arrays.fill(pre, 1);
            return process(pre, 0, N);
        }

        // pre 表示level-1行的状态
        // level表示，正在level行做决定
        // N 表示一共有多少行 固定的
        // level-2行及其之上所有行，都摆满砖了
        // level做决定，让所有区域都满，方法数返回
        public static int process(int[] pre, int level, int N) {
            if (level == N) { // base case
                for (int i = 0; i < pre.length; i++) {
                    if (pre[i] == 0) {
                        return 0;
                    }
                }
                return 1;
            }

            // 没到终止行，可以选择在当前的level行摆瓷砖
            int[] op = getOp(pre);
            return dfs(op, 0, level, N);
        }

        // op[i] == 0 可以考虑摆砖
        // op[i] == 1 只能竖着向上
        public static int dfs(int[] op, int col, int level, int N) {
            // 在列上自由发挥，玩深度优先遍历，当col来到终止列，i行的决定做完了
            // 轮到i+1行，做决定
            if (col == op.length) {
                return process(op, level + 1, N);
            }
            int ans = 0;
            // col位置不横摆
            ans += dfs(op, col + 1, level, N); // col位置上不摆横转
            // col位置横摆, 向右
            if (col + 1 < op.length && op[col] == 0 && op[col + 1] == 0) {
                op[col] = 1;
                op[col + 1] = 1;
                ans += dfs(op, col + 2, level, N);
                op[col] = 0;
                op[col + 1] = 0;
            }
            return ans;
        }

        public static int[] getOp(int[] pre) {
            int[] cur = new int[pre.length];
            for (int i = 0; i < pre.length; i++) {
                cur[i] = pre[i] ^ 1;
            }
            return cur;
        }

        // Min (N,M) 不超过 32
        public static int ways2(int N, int M) {
            if (N < 1 || M < 1 || ((N * M) & 1) != 0) {
                return 0;
            }
            if (N == 1 || M == 1) {
                return 1;
            }
            int max = Math.max(N, M);
            int min = Math.min(N, M);
            int pre = (1 << min) - 1;
            return process2(pre, 0, max, min);
        }

        // 上一行的状态，是pre，limit是用来对齐的，固定参数不用管
        // 当前来到i行，一共N行，返回填满的方法数
        public static int process2(int pre, int i, int N, int M) {
            if (i == N) { // base case
                return pre == ((1 << M) - 1) ? 1 : 0;
            }
            int op = ((~pre) & ((1 << M) - 1));
            return dfs2(op, M - 1, i, N, M);
        }

        public static int dfs2(int op, int col, int level, int N, int M) {
            if (col == -1) {
                return process2(op, level + 1, N, M);
            }
            int ans = 0;
            ans += dfs2(op, col - 1, level, N, M);
            if ((op & (1 << col)) == 0 && col - 1 >= 0 && (op & (1 << (col - 1))) == 0) {
                ans += dfs2((op | (3 << (col - 1))), col - 2, level, N, M);
            }
            return ans;
        }

        // 记忆化搜索的解
        // Min(N,M) 不超过 32
        public static int ways3(int N, int M) {
            if (N < 1 || M < 1 || ((N * M) & 1) != 0) {
                return 0;
            }
            if (N == 1 || M == 1) {
                return 1;
            }
            int max = Math.max(N, M);
            int min = Math.min(N, M);
            int pre = (1 << min) - 1;
            int[][] dp = new int[1 << min][max + 1];
            for (int i = 0; i < dp.length; i++) {
                for (int j = 0; j < dp[0].length; j++) {
                    dp[i][j] = -1;
                }
            }
            return process3(pre, 0, max, min, dp);
        }

        public static int process3(int pre, int i, int N, int M, int[][] dp) {
            if (dp[pre][i] != -1) {
                return dp[pre][i];
            }
            int ans = 0;
            if (i == N) {
                ans = pre == ((1 << M) - 1) ? 1 : 0;
            } else {
                int op = ((~pre) & ((1 << M) - 1));
                ans = dfs3(op, M - 1, i, N, M, dp);
            }
            dp[pre][i] = ans;
            return ans;
        }

        public static int dfs3(int op, int col, int level, int N, int M, int[][] dp) {
            if (col == -1) {
                return process3(op, level + 1, N, M, dp);
            }
            int ans = 0;
            ans += dfs3(op, col - 1, level, N, M, dp);
            if (col > 0 && (op & (3 << (col - 1))) == 0) {
                ans += dfs3((op | (3 << (col - 1))), col - 2, level, N, M, dp);
            }
            return ans;
        }

        // 严格位置依赖的动态规划解
        public static int ways4(int N, int M) {
            if (N < 1 || M < 1 || ((N * M) & 1) != 0) {
                return 0;
            }
            if (N == 1 || M == 1) {
                return 1;
            }
            int big = N > M ? N : M;
            int small = big == N ? M : N;
            int sn = 1 << small;
            int limit = sn - 1;
            int[] dp = new int[sn];
            dp[limit] = 1;
            int[] cur = new int[sn];
            for (int level = 0; level < big; level++) {
                for (int status = 0; status < sn; status++) {
                    if (dp[status] != 0) {
                        int op = (~status) & limit;
                        dfs4(dp[status], op, 0, small - 1, cur);
                    }
                }
                for (int i = 0; i < sn; i++) {
                    dp[i] = 0;
                }
                int[] tmp = dp;
                dp = cur;
                cur = tmp;
            }
            return dp[limit];
        }

        public static void dfs4(int way, int op, int index, int end, int[] cur) {
            if (index == end) {
                cur[op] += way;
            } else {
                dfs4(way, op, index + 1, end, cur);
                if (((3 << index) & op) == 0) { // 11 << index 可以放砖
                    dfs4(way, op | (3 << index), index + 1, end, cur);
                }
            }
        }

        public static void main(String[] args) {
            int N = 8;
            int M = 6;
            System.out.println(ways1(N, M));
            System.out.println(ways2(N, M));
            System.out.println(ways3(N, M));
            System.out.println(ways4(N, M));
            N = 10;
            M = 10;
            System.out.println("=========");
            System.out.println(ways3(N, M));
            System.out.println(ways4(N, M));
        }
    }
}
