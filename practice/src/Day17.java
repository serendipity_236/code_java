import java.util.*;

public class Day17 {
  public static class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode() {}
      TreeNode(int val) { this.val = val; }
      TreeNode(int val, TreeNode left, TreeNode right) {
          this.val = val;
          this.left = left;
          this.right = right;
      }
  }
    // https://leetcode.cn/problems/binary-tree-inorder-traversal/
    // 94. 二叉树的中序遍历
    // 循环解决
    public List<Integer> inorderTraversal1(TreeNode root) {
      List<Integer> result = new ArrayList<>();
      inOrder(root,result);
      return result;
    }

    private void inOrder(TreeNode root, List<Integer> result) {
      if(root == null){
          return;
      }
      inOrder(root.left,result);
      result.add(root.val);
      inOrder(root.right, result);
    }

    // 栈解决
    public List<Integer> inorderTraversal(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();
        while (root != null || !stack.isEmpty()){
            while(root != null){
                stack.push(root);
                root = root.left;
            }
            root = stack.pop();
            result.add(root.val);
            root = root.right;
        }
        return result;
    }

    // https://leetcode.cn/problems/binary-tree-preorder-traversal/
    // 144. 二叉树的前序遍历
    public List<Integer> preorderTraversal1(TreeNode root) {
        List<Integer> result = new ArrayList<>();
        preOrder(root,result);
        return result;
    }

    private void preOrder(TreeNode root, List<Integer> result) {
        if(root == null){
            return;
        }
        result.add(root.val);
        preOrder(root.left,result);
        preOrder(root.right,result);
    }

    // 栈解决
    public List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> res = new ArrayList<Integer>();
        Deque<TreeNode> stack = new LinkedList<TreeNode>();
        while (!stack.isEmpty() || root != null) {
            while (root != null) {
                res.add(root.val);
                stack.push(root);
                root = root.left;
            }
            root = stack.pop();
            root = root.right;
        }
        return res;
    }

    // https://leetcode.cn/problems/binary-tree-postorder-traversal/
    // 145. 二叉树的后序遍历
    // 循环解决
    public List<Integer> postorderTraversal1(TreeNode root) {
        List<Integer> result = new ArrayList<Integer>();
        postorder(root, result);
        return result;
    }

    public void postorder(TreeNode root, List<Integer> result) {
        if (root == null) {
            return;
        }
        postorder(root.left, result);
        postorder(root.right, result);
        result.add(root.val);
    }

    // 栈解决
    public List<Integer> postorderTraversal(TreeNode root) {
        List<Integer> res = new ArrayList<Integer>();
        Deque<TreeNode> stack = new LinkedList<TreeNode>();
        TreeNode preAccess = null;
        while (!stack.isEmpty() || root != null) {
            while (root != null) {
                stack.push(root);
                root = root.left;
            }
            root = stack.pop();
            if(root.right == null || root.right == preAccess){
                res.add(root.val);
                preAccess = root;
                root = null;
            }else {
                stack.push(root);
                root = root.right;
            }
        }
        return res;
    }

    // https://leetcode.cn/problems/symmetric-tree/
    // 101. 对称二叉树
    // 循环解决
    public boolean isSymmetric1(TreeNode root) {
        if(root == null) {
            return true;
        }
        return isSymmetricChildren(root.left,root.right);
    }

    public boolean isSymmetricChildren(TreeNode leftTree,TreeNode rightTree) {
        if(leftTree == null && rightTree == null)
            return true;
        if(leftTree == null || rightTree == null)
            return false;
        if(leftTree.val != rightTree.val)
            return false;
        return isSymmetricChildren(leftTree.left,rightTree.right) && isSymmetricChildren(leftTree.right,rightTree.left);
    }

    //队列解决
    public boolean isSymmetric(TreeNode root) {
        if(root == null){
          return true;
        }
        TreeNode p = root.left;
        TreeNode q = root.right;
        Queue<TreeNode> queue = new LinkedList<>();
        if(p == null && q == null){
            return true;
        }
        queue.add(p);
        queue.add(q);
        while(!queue.isEmpty()){
            p = queue.poll();
            q = queue.poll();
            if(p == null && q == null){
                continue;
            }
            if((p == null || q == null) || p.val != q.val){
                return false;
            }
            queue.add(p.left);
            queue.add(q.right);

            queue.add(p.right);
            queue.add(q.left);
        }
        return true;
    }

    // https://leetcode.cn/problems/same-tree/description/
    // 100. 相同的树
    public boolean isSameTree(TreeNode p, TreeNode q) {
        if(p == null && q == null)
            return true;
        if(p == null || q==null)
            return false;
        if(p.val != q.val)
            return false;
        return isSameTree(p.left,q.left) && isSameTree(p.right,q.right);
    }

    // https://leetcode.cn/problems/maximum-depth-of-binary-tree/
    // 104. 二叉树的最大深度
    // 循环解决
    public int maxDepth1(TreeNode root) {
        if(root == null) {
            return 0;
        }
        return Math.max(maxDepth1(root.left),maxDepth1(root.right)) + 1;
    }

    // 队列解决
    public int maxDepth(TreeNode root) {
        if(root == null) {
            return 0;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        int depth = 0;
        while(!queue.isEmpty()){
            int size = queue.size();
            while(size > 0){
                TreeNode node = queue.poll();
                if(node.left != null){
                    queue.offer(node.left);
                }
                if(node.right != null){
                    queue.offer(node.right);
                }
                size--;
            }
            depth++;
        }
        return depth;
    }

    // https://leetcode.cn/problems/balanced-binary-tree/
    // 110. 平衡二叉树
    public int maxDepth2(TreeNode root) {
        if(root == null)
            return 0;
        //比较左右两边的子树是否平衡
        int leftTree = maxDepth2(root.left);
        int rightTree = maxDepth2(root.right);
        if(leftTree >= 0 && rightTree >= 0 && Math.abs(leftTree-rightTree) <= 1){
            return Math.max(leftTree,rightTree) + 1;
        }else{
            return -1;
        }
    }
    public boolean isBalanced(TreeNode root) {
        if(root==null){
            return true;
        }
        return maxDepth2(root) >= 0;
    }

    // https://leetcode.cn/problems/invert-binary-tree/
    // 226. 翻转二叉树
    public TreeNode invertTree(TreeNode root) {
        if(root==null)
            return null;
        TreeNode tmp = invertTree(root.left);
        root.left = invertTree(root.right);
        root.right = tmp;
        return root;
    }




















    // 贪心算法
    public static int circle_fly(int[] charge, int[] cost) {
        boolean flag = false;
        int tryies = 0;
        int n = charge.length;
        while(tryies < n){
            int count = 0;
            int power = 0;
            for(int i = tryies;count < n;i++){
                if(i < n){
                    power += charge[i];
                    power -= cost[i];
                }else {
                    power += charge[i % n];
                    power -= cost[i % n];
                }
                if(power < 0){
                    break;
                }
                count++;
            }
            if(count == n){
                flag = true;
                return tryies;
            }
            tryies++;
        }
        return -1;
    }

    public static void main(String[] args) {
        int[] charge = {1 ,2 ,3 ,4 ,5};
        int[] cost = {3 ,4 ,5 ,1 ,2};
        System.out.println(circle_fly(charge, cost));
    }

    
}
