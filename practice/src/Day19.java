import java.util.LinkedList;
import java.util.Queue;

public class Day19 {

    // https://leetcode.cn/problems/number-of-provinces/
    // 547. 省份数量
    // 深度优先
    public int findCircleNum1(int[][] isConnected) {
        int cities = isConnected.length;
        boolean[] visited = new boolean[cities];
        int provinces = 0;
        for (int i = 0; i <cities ; i++) {
            if(!visited[i]){
                dfs(visited,isConnected,i,cities);
                provinces++;
            }
        }
        return provinces;
    }

    private void dfs(boolean[] visited, int[][] isConnected, int i, int cities) {
        for (int j = 0; j < cities; j++) {
            if(isConnected[i][j] == 1 && !visited[j]){
                visited[j] = true;
                dfs(visited,isConnected,j,cities);
            }
        }
    }

    // 广度优先
    public int findCircleNum(int[][] isConnected) {
        int cities = isConnected.length;
        boolean[] visited = new boolean[cities];
        int provinces = 0;
        Queue<Integer> queue = new LinkedList<>();
        for (int i = 0; i < cities; i++) {
            if(!visited[i]){
                queue.offer(i);
                while(!queue.isEmpty()){
                    int currentCity = queue.poll();
                    visited[currentCity] = true;
                    for (int j = 0; j < cities; j++) {
                        if(isConnected[currentCity][j] == 1 && !visited[j]){
                            queue.offer(j);
                        }
                    }
                }
                provinces++;
            }
        }
        return provinces;
    }

    // 并查集
    private int mergeFind(int[][] isConnected){
        int cities = isConnected.length;
        int[] head = new int[cities];
        int[] level = new int[cities];
        // 初始化
        for (int i = 0; i < cities; i++) {
            head[i] = i;
            level[i] = 1;
        }
        for (int i = 0; i < cities; i++) {
            for (int j = 0; j < cities; j++) {
                if(isConnected[i][j] == 1){
                    merge(i,j,head,level);
                }
            }
        }
        int result = 0;
        for (int i = 0; i < cities; i++) {
            if(head[i] == i){
                result++;
            }
        }
        return result;
    }

    // 合并省份
    private void merge(int x, int y, int[] head, int[] level) {
        int i = find(x,head);
        int j = find(y,head);

        if(i == j)
            return;
        // 合并树
        if(level[i] < level[j]){
            head[i] = j;
            // 修改深度
            level[i]++;
        }else if(level[i] == level[j]){
            head[i] = j;
            // 修改深度
            level[i]++;
            level[j]++;
        }else {
            head[j] =i;
            level[j]++;
        }
    }

    private int find(int index, int[] head) {
        if(head[index] == index){
            return index;
        }
        // 同时修改当前节点的head节点
        head[index] = find(head[index],head);
        return head[index];
    }

    // https://leetcode.cn/problems/stone-game/
    // 石子游戏
    public boolean stoneGame(int[] piles) {
        int length = piles.length;
        int[][] dp = new int[length][length];
        for (int i = 0; i < length; i++) {
            dp[i][i] = piles[i];
        }
        for (int i = length - 2; i >= 0; i--) {
            for (int j = i + 1; j < length; j++) {
                dp[i][j] = Math.max(piles[i] - dp[i + 1][j], piles[j] - dp[i][j - 1]);
            }
        }
        return dp[0][length - 1] > 0;
    }

    // https://leetcode.cn/problems/predict-the-winner/
    // 486. 预测赢家
    // 递归
    public boolean predictTheWinner1(int[] nums) {
        int sum = 0;
        for (int num : nums) {
            sum += num;
        }
        int p1 = maxScore(nums,0,nums.length - 1);
        int p2 = sum - p1;
        //return p1 >= p2;

        return maxScore1(nums,0,nums.length - 1) >= 0;
    }

    // 获取玩家的最大的分数数量
    private int maxScore(int[] nums, int l, int r) {
        // 只剩下一个元素
        if(l == r){
            return nums[l];
        }
        int sLeft = 0;
        int sRight = 0;
        // 剩下两个元素，选择最大的
        if(r - l == 1){
            sLeft = nums[l];
            sRight = nums[r];
        }
        // 如果剩下超过两个元素要进行递归选取
        if(r - l > 1){
            // 选择剩下的最左边的值加上第二个玩家选后的最小值选择，因为玩家2肯定会把玩家1选后剩下的里面选出做大的，把小的给玩家1
            sLeft = nums[l] + Math.min(maxScore(nums,l + 2,r),maxScore(nums,l + 1,r - 1));
            // 选择剩下的最右边的值
            sRight = nums[r] + Math.min(maxScore(nums,l + 1,r - 1),maxScore(nums,l,r-2));
        }

        return Math.max(sLeft,sRight);
    }

    private int maxScore1(int[] nums, int l, int r){
        if(l == r)
            return nums[l];
        int sLeft = nums[l] - maxScore1(nums,l + 1,r);
        int sRight = nums[r] - maxScore1(nums,l,r - 1);

        return Math.max(sLeft,sRight);
    }

    // 动态规划
    public boolean predictTheWinner(int[] nums) {
        int length = nums.length;
        // dp[i][j] i到j区间之间的差值
        int[][] dp = new int[length][length];
        for (int i = 0; i < length; i++) {
            dp[i][i] = nums[i];
        }
        for (int i = length - 2; i >= 0; i--) {
            for (int j = i + 1; j < length; j++) {
                dp[i][j] = Math.max(nums[i] - dp[i + 1][j], nums[j] - dp[i][j - 1]);
            }
        }
        return dp[0][length - 1] >= 0;
    }

}
