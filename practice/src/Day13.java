import java.util.Arrays;

public class Day13 {

    // https://leetcode.cn/problems/er-cha-sou-suo-shu-de-di-kda-jie-dian-lcof/
    // 剑指 Offer 54. 二叉搜索树的第k大节点
    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode(int x) {
            val = x;
        }
    }
    int count = 0;
    int result = 0;
    public int kthLargest(TreeNode root, int k) {
        // 因为已知是二叉搜索树，进行中序遍历得到有序序列 从小到大的顺序
        // 但是要求第k大节点 因此可以按照右 根 左进行遍历
        this.count = k;
        right_root_left(root);
        return result;
    }

    private void right_root_left(TreeNode root) {
        if(root == null || count <= 0){
            return;
        }
        right_root_left(root.right);
        count--;
        // 保存第k大的节点
        if(count == 0){
            result = root.val;
        }
        right_root_left(root.left);
    }

    // https://leetcode.cn/problems/er-cha-shu-de-shen-du-lcof/
    // 剑指 Offer 55 - I. 二叉树的深度
    public int maxDepth(TreeNode root) {
        if(root == null){
            return 0;
        }
        int leftDepth = maxDepth(root.left);
        if(leftDepth == -1){
            return -1;
        }
        int rightDepth = maxDepth(root.right);
        if(rightDepth == -1){
            return -1;
        }

        // 计算的时候就可以判断是否为二叉搜索树
        if(Math.abs(leftDepth - rightDepth) > 1){
            return -1;
        }

        return Math.max(leftDepth,rightDepth) + 1;
    }

    // https://leetcode.cn/problems/ping-heng-er-cha-shu-lcof/
    // 剑指 Offer 55 - II. 平衡二叉树
    public boolean isBalanced(TreeNode root) {
        if(root == null){
            return true;
        }
//        int leftHeight = maxDepth(root.left);
//        int rightHeight = maxDepth(root.right);
//
//        return Math.abs(leftHeight - rightHeight)<=1 && isBalanced(root.left) && isBalanced(root.right);
        // 优化 从下往上遍历
        return maxDepth(root) != -1;
    }

    // https://leetcode.cn/problems/shu-zu-zhong-shu-zi-chu-xian-de-ci-shu-lcof/
    // 剑指 Offer 56 - I. 数组中数字出现的次数
    public int[] singleNumbers(int[] nums) {
        int z = 0;
        for (int num : nums) {
            z = z ^ num;
        }
        // 找到最低位为1的位置
        int m = 1;
        while((z & m) == 0){
            m = m << 1;
        }
        int x = 0;
        int y = 0;
        for (int num : nums){
            if((m & num) == 0){
                x = x ^ num;
            }else {
                y = y ^ num;
            }
        }
        return new int[]{x,y};
    }

    // https://leetcode.cn/problems/shu-zu-zhong-shu-zi-chu-xian-de-ci-shu-ii-lcof/
    // 剑指 Offer 56 - II. 数组中数字出现的次数 II
    public int singleNumber(int[] nums) {
        int[] array = new int[32];
        int m = 1;
        int result = 0;
        for (int i = 0; i < 32; i++) {
            for (int j = 0; j < nums.length; j++) {
                if((nums[j] & m) != 0){
                    array[i]++;
                }
            }
            array[i] = array[i] % 3;
            result = result + array[i] * m;
            m = m << 1;
        }
        return result;
    }
}
