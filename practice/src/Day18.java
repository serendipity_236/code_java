import java.util.*;

public class Day18 {
    
    
    // https://leetcode.cn/problems/reverse-linked-list/
    // 206. 反转链表
    // 递归版本
    public static class ListNode {
        int val;
        ListNode next;
        ListNode() {}
        ListNode(int val) {
            this.val = val;
        }
        ListNode(int val, ListNode next) {
          this.val = val;
          this.next = next;
        }
    }
    public ListNode reverseList(ListNode head) {
        // 此时的head结点为空或者已经遍历到最后一个结点直接返回
        if (head == null || head.next == null) {
            return head;
        }
        ListNode newHead = reverseList(head.next);
        head.next.next = head;
        head.next = null;
        return newHead;
    }

    // https://leetcode.cn/problems/count-primes/
    // 204. 计数质数
    // 枚举解法
    public int countPrimes1(int n) {
        int ans = 0;
        for (int i = 2; i < n; ++i) {
            ans += isPrime(i) ? 1 : 0;
        }
        return ans;
    }

    public boolean isPrime(int x) {
        for (int i = 2; i * i <= x; ++i) {
            if (x % i == 0) {
                return false;
            }
        }
        return true;
    }

    // 埃氏筛选解法
    public int countPrimes(int n) {
        int[] isPrime = new int[n];
        Arrays.fill(isPrime, 1);
        int ans = 0;
        for (int i = 2; i < n; ++i) {
            if (isPrime[i] == 1) {
                ans += 1;
                if ((long) i * i < n) {
                    for (int j = i * i; j < n; j += i) {
                        isPrime[j] = 0;
                    }
                }
            }
        }
        return ans;
    }

    // https://leetcode.cn/problems/remove-duplicates-from-sorted-array/
    // 26. 删除有序数组中的重复项
    // 双指针法
    public int removeDuplicates(int[] nums) {
        if(nums.length == 0){
            return 0;
        }
        int i = 0;
        for (int j = 1; j < nums.length; j++) {
            if(nums[j] != nums[i]){
                i++;
                nums[i] = nums[j];
            }
        }
        return i + 1;
    }

    // https://leetcode.cn/problems/find-pivot-index/
    // 724. 寻找数组的中心下标
    public int pivotIndex(int[] nums) {
        int sum = Arrays.stream(nums).sum();
        int total = 0;
        for (int i = 0; i < nums.length; i++) {
            total += nums[i];
            if(total == sum){
                return i;
            }
            sum -= nums[i];
        }
        return -1;
    }

    // https://leetcode.cn/problems/sqrtx/
    // 69. x 的平方根
    // 二分法
    public int mySqrt(int x) {
        int l = 0, r = x, ans = -1;
        while (l <= r) {
            int mid = l + (r - l) / 2;
            if ((long) mid * mid <= x) {
                ans = mid;
                l = mid + 1;
            } else {
                r = mid - 1;
            }
        }
        return ans;
    }

    // 牛顿迭代
    public int mySqrt1(int x) {
        if (x == 0) {
            return 0;
        }

        double C = x, x0 = x;
        while (true) {
            double xi = 0.5 * (x0 + C / x0);
            if (Math.abs(x0 - xi) < 1e-7) {
                break;
            }
            x0 = xi;
        }
        return (int) x0;
    }

    // https://leetcode.cn/problems/maximum-product-of-three-numbers/
    // 628. 三个数的最大乘积
    // 数组都为正数 直接找出三个最大的数相乘即可
    // 数组都为负数 直接找出三个最大的数相乘即可
    // 数组中有正数有负数，要找出两个最小负数相乘再乘以最大的正数和最大的三个正数相乘比较
    // 综上可得  我们要找出五个数 最小的两个负数和最大的三个正数，比较两个最小负数相乘再乘以最大的正数的结果A和最大的三个正数相乘的结果B谁大
    // 排序后再比较
    public int maximumProduct1(int[] nums) {
        Arrays.sort(nums);
        int n = nums.length;
        return Math.max(nums[0] * nums[1] * nums[n - 1], nums[n - 3] * nums[n - 2] * nums[n - 1]);
    }

    // 线性扫描
    public static int maximumProduct(int[] nums) {
        int min1 = Integer.MAX_VALUE;
        int min2 = Integer.MAX_VALUE;
        int max1 = Integer.MIN_VALUE;
        int max2 = Integer.MIN_VALUE;
        int max3 = Integer.MIN_VALUE;
        for (int num : nums){
            // 比最小的min1还要小，说明min1是第二小的置换值
            if(num < min1){
                min2 = min1;
                min1 = num;
            } else if (num < min2) {
                min2 = num;
            }

            // 比最大的max1还要大，说明max1是第二大的，max2是第三大的
            if(num > max1){
                max3 = max2;
                max2 = max1;
                max1 = num;
            } else if (num > max2) {
                max3 = max2;
                max2 = num;
            }else if(num > max3){
                max3 = num;
            }
        }
        return Math.max(min1 * min2 * max1,max1 * max2 * max3);
    }

    // https://leetcode.cn/problems/two-sum-ii-input-array-is-sorted/
    // 167. 两数之和 II - 输入有序数组
    // 二分查找
    public int[] twoSum1(int[] numbers, int target) {
        for (int i = 0; i < numbers.length; ++i) {
            int low = i + 1, high = numbers.length - 1;
            while (low <= high) {
                int mid = (high - low) / 2 + low;
                if (numbers[mid] == target - numbers[i]) {
                    return new int[]{i + 1, mid + 1};
                } else if (numbers[mid] > target - numbers[i]) {
                    high = mid - 1;
                } else {
                    low = mid + 1;
                }
            }
        }
        return new int[]{-1, -1};
    }

    // 双指针解法
    public int[] twoSum(int[] numbers, int target) {
        int low = 0, high = numbers.length - 1;
        while (low < high) {
            int sum = numbers[low] + numbers[high];
            if (sum < target) {
                ++low;
            } else if (sum > target) {
                high--;
            } else {
                // 下标从1开始
                return new int[]{low + 1, high + 1};
            }
        }
        return new int[]{-1, -1};
    }

    // https://leetcode.cn/problems/arranging-coins/
    // 441. 排列硬币
    // 暴力解法 穷举
    public int arrangeCoins1(int n) {
        for (int i = 1; i <= n; i++) {
            n = n - i;
            if(n <= i){
                return i;
            }
        }
        return 0;
    }

    // 二分查找解法
    // 假设有n层，从里面1-n层找到答案，计算i层的个数是否等于n
    public int arrangeCoins2(int n) {
        int left = 1, right = n;
        while (left <= right) {
            int mid = (right - left) / 2 + left;
            long count = (long) mid * (mid + 1) / 2;
            if ( count == n) {
                return mid;
            } else if(count > n){
                right = mid - 1;
            }else {
                left = mid + 1;
            }
        }
        return right;
    }

    // 求有几层，就是求(x^2 + x) / 2 = n这个方程式的解
    public int arrangeCoins(int n) {
        return (int) ((Math.sqrt((long) 8 * n + 1) - 1) / 2);
    }

    // https://leetcode.cn/problems/maximum-average-subarray-i/
    // 643. 子数组最大平均数 I
    // 滑动窗口解法(双指针特例)
    public double findMaxAverage(int[] nums, int k) {
        int sum = 0;
        int n = nums.length;
        for (int i = 0; i < k; i++) {
            sum += nums[i];
        }
        int maxSum = sum;
        for (int i = k; i < n; i++) {
            sum -= nums[i - k];
            sum += nums[i];
            if(maxSum < sum){
                maxSum = sum;
            }
        }
        return 1.0 * maxSum / k;
    }

    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode() {}
        TreeNode(int val) {
            this.val = val;
        }
        TreeNode(int val, TreeNode left, TreeNode right) {
          this.val = val;
          this.left = left;
          this.right = right;
        }
    }

    // https://leetcode.cn/problems/minimum-depth-of-binary-tree/
    // 111. 二叉树的最小深度
    // 最小深度是从根节点到最近叶子节点的最短路径上的节点数量
    // 深度优先 从下往上
    public int minDepth1(TreeNode root) {
        if(root == null){
            return 0;
        }
        // 已经到叶子节点了
        if(root.left == null && root.right == null){
            return 1;
        }
        int min = Integer.MAX_VALUE;
        if(root.left != null){
            min = Math.min(minDepth1(root.left),min);
        }
        if(root.right != null){
            min = Math.min(minDepth1(root.right),min);
        }
        return min +1;
    }

    // 广度优先 层次遍历比较结果,需要借助队列
    // 维护当前节点的深度
    public static class QueueNode {
        TreeNode node;
        int depth;

        public QueueNode(TreeNode node, int depth) {
            this.node = node;
            this.depth = depth;
        }
    }
    public int minDepth(TreeNode root) {
        if(root == null){
            return 0;
        }
        Queue<QueueNode> queue = new LinkedList<>();
        queue.offer(new QueueNode(root,1));
        while(!queue.isEmpty()){
            QueueNode current = queue.poll();
            int depth = current.depth;
            // 判断是否为叶子节点
            if(current.node.left == null && current.node.right == null){
                return depth;
            }
            if(current.node.left != null){
                queue.offer(new QueueNode(current.node.left,current.depth + 1));
            }
            if(current.node.right != null){
                queue.offer(new QueueNode(current.node.right,current.depth + 1));
            }
        }
        return 0;
    }

    // 贪心算法例题：
    // https://leetcode.cn/problems/longest-continuous-increasing-subsequence/
    // 674. 最长连续递增序列
    public int findLengthOfLCIS(int[] nums) {
        // 每一次递增序列的开始下标
        int start = 0;
        int max = 0;
        for (int i = 0; i < nums.length; i++) {
            if(i > 0 && nums[i] <= nums[i - 1]){
                start = i;
            }
            max = Math.max(max,i - start + 1);
        }
        return max;
    }

    // https://leetcode.cn/problems/lemonade-change/
    // 860. 柠檬水找零
    public boolean lemonadeChange(int[] bills) {
        int five = 0;
        int ten = 0;
        for (int bill : bills) {
            if(bill == 5){
                five++;
            }else if(bill == 10){
                if(five <= 0){
                    return false;
                }
                five--;
                ten++;
            }else {
                // 优先使用10，因为面值为10的只有在20找零钱时才能使用
                if(five > 0 && ten > 0){
                    five--;
                    ten--;
                }else if(five >= 3){
                    five -= 3;
                }else {
                    return false;
                }
            }
        }
        return true;
    }

    // https://leetcode.cn/problems/largest-perimeter-triangle/
    // 976. 三角形的最大周长
    public int largestPerimeter(int[] nums) {
        // 排序，然后从后开始比较
        Arrays.sort(nums);
        for (int i = nums.length - 1; i >= 2; i--) {
            if(nums[i - 2] + nums[i - 1] > nums[i]){
                return nums[i] + nums[i - 1] + nums[i - 2];
            }
        }
        return 0;
    }

    // 二叉树遍历相关例题：
    // https://leetcode.cn/problems/binary-tree-level-order-traversal/
    // 102. 二叉树的层序遍历
    // 迭代
    public List<List<Integer>> levelOrder(TreeNode root) {
        List<List<Integer>> ret = new ArrayList<List<Integer>>();
        if (root == null) {
            return ret;
        }

        Queue<TreeNode> queue = new LinkedList<TreeNode>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            List<Integer> temp = new ArrayList<Integer>();
            int size = queue.size();
            // 把一层的放进一个list中
            for (int i = 1; i <= size; ++i) {
                TreeNode node = queue.poll();
                temp.add(node.val);
                if (node.left != null) {
                    queue.offer(node.left);
                }
                if (node.right != null) {
                    queue.offer(node.right);
                }
            }
            ret.add(temp);
        }

        return ret;
    }

    // 递归  输出时候避开null即可
    public void levelOrder(TreeNode root,int i,ArrayList<Integer> list){
        if(root == null){
            return;
        }
        int length = list.size();
        if(length < i){
            for (int j = 0; j <= i - length ; j++) {
                list.add(length + j,null);
            }
        }
        list.set(i, root.val);
        levelOrder(root.left,2 * i,list);
        levelOrder(root.right,2 * i + 1,list);
    }

}
