import java.util.Arrays;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Scanner;

public class Day7 {
    // 两个有序数组相加和Topk问题
    // 放入大根堆中的结构
    static class Node {
        public int index1;  // arr1中的位置
        public int index2;  // arr2中的位置
        public int sum;     // arr1[index1]+arr2[index2]
        public Node(int i1, int i2, int s) {
            index1 = i1;
            index2 = i2;
            sum = s;
        }
    }

    public static int[] topKSum(Integer[] arr1, Integer[] arr2, int topK) {
        if (arr1 == null || arr2 == null || topK < 1) {
            return null;
        }
        topK = Math.min(topK, arr1.length * arr2.length);
        int[] res = new int[topK];
        int resIndex = 0;
        // 自定义比较器，实现大根堆
        PriorityQueue<Node> maxHeap = new PriorityQueue<>((N1, N2) -> N2.sum - N1.sum);
        // set[i][j] == false , arr1[i] arr2[j] 之前没进过堆
        // set[i][j] == true , arr1[i] arr2[j] 之前进过堆
        // boolean[][] set = new boolean[arr1.length][arr2.length];
        // 使用hashset解决超内存问题
        HashSet<String> positionSet = new HashSet<>();
        // 从右下角开始
        int i1 = arr1.length - 1;
        int i2 = arr2.length - 1;
        maxHeap.add(new Node(i1, i2, arr1[i1] + arr2[i2]));
        // set[i1][i2] = true;
        positionSet.add(i1 + "_" + i2);
        while (resIndex != topK) {
            Node curNode = maxHeap.poll();
            res[resIndex++] = curNode.sum;
            i1 = curNode.index1;
            i2 = curNode.index2;
//            if (i1 - 1 >= 0 && set[i1 - 1][i2] == false) {
//                set[i1 - 1][i2] = true;
//                maxHeap.add(new Node(i1 - 1, i2, arr1[i1 - 1] + arr2[i2]));
//            }
//            if (i2 - 1 >= 0 && set[i1][i2 - 1] == false) {
//                set[i1][i2 - 1] = true;
//                maxHeap.add(new Node(i1, i2 - 1, arr1[i1] + arr2[i2 - 1]));
//            }
            if (i1 - 1 >= 0 && !positionSet.contains(i1 - 1 + "_" + i2)) {
                positionSet.add(i1 - 1 + "_" + i2);
                maxHeap.add(new Node(i1 - 1, i2, arr1[i1 - 1] + arr2[i2]));
            }
            if (i2 - 1 >= 0 && !positionSet.contains(i1 + "_" + (i2 - 1))) {
                positionSet.add(i1 + "_" + (i2 - 1));
                maxHeap.add(new Node(i1, i2 - 1, arr1[i1] + arr2[i2 - 1]));
            }
        }
        return res;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        Integer[] arr1 = new Integer[n];
        Integer[] arr2 = new Integer[n];
        for (int i = 0; i < n; i++) {
            arr1[i] = in.nextInt();
        }
        for (int i = 0; i < n; i++) {
            arr2[i] = in.nextInt();
        }
        // 要将输入的两个数字排序
        Arrays.sort(arr1);
        Arrays.sort(arr2);
        int[] res = topKSum(arr1, arr2, k);
        for (int re : res) {
            System.out.print(re + " ");
        }
    }


    // 给你一个正数，输出n*n的蛇形矩阵
    public static void main1(String[] args) {
        int[][] array;
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        array = new int[n][n];
        int k = 1;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j <=i; j++) {
                if(i % 2 == 0){
                    array[i - j][j] = k++;
                }else {
                    array[j][i - j] = k++;
                }
            }
        }
        for (int i = n; i < 2 * n - 1; i++) {
            for (int j = 1; j < 2 * n - i; j++) {
                if(i % 2 == 0){
                    array[n - j][i - n + j] = k++;
                }else {
                    array[i - n + j][n - j] = k++;
                }
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
    }

    // 买卖股票的最好时机(三)
    public static void main2(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] prices = new int[n];
        for (int i = 0; i < n; i++) {
            prices[i] = scanner.nextInt();
        }
        int firstBuy = Integer.MIN_VALUE,firstSell = 0;
        int secondBuy = Integer.MIN_VALUE,secondSell = 0;
        for (int price:prices) {
            // 思路：首先分析状态，一共有5个状态，分别为不操作，第一次买入buy1，第一次卖出sale1，第二次买入buy2，第二次卖出sale2
            // 第一次买入时，赚取的最大利益为支出的费用，即-price[i]或者暂不执行第一次买入
            firstBuy = Math.max(firstBuy,-price);
            // 第一次买入时，赚取的最大利益为支出的费用，即-price[i]或者暂不执行第一次买入
            firstSell = Math.max(firstSell,firstBuy + price);
            // 第二次买入时，赚取的最大利益为第一次卖出时获取的最大利益与此时的支出的费用或暂不执行第二次的买入
            secondBuy = Math.max(secondBuy,firstSell - price);
            // 第二次买入时，赚取的最大利益为第一次卖出时获取的最大利益与此时的支出的费用或暂不执行第二次的买入
            secondSell = Math.max(secondSell,secondBuy + price);
        }
        System.out.println(secondSell);
    }

}
