package LeetCode;

import java.util.*;

public class practice {
    // https://leetcode.cn/problems/two-sum/
    static class Solution1 {
        public int[] twoSum(int[] nums, int target) {
            Map<Integer, Integer> map = new HashMap<>();
            for (int i = 0; i < nums.length; i++) {
                if (!map.containsKey(nums[i])) {
                    map.put(target - nums[i], i);
                } else {
                    return new int[]{map.get(nums[i]), i};
                }
            }
            return new int[0];
        }
    }

    // https://leetcode.cn/problems/add-two-numbers/
    static class Solution2 {
        static class ListNode {
            int val;
            ListNode next;

            ListNode() {
            }

            ListNode(int val) {
                this.val = val;
            }

            ListNode(int val, ListNode next) {
                this.val = val;
                this.next = next;
            }
        }

        public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
            ListNode head = null;
            ListNode tail = null;
            int carry = 0;
            while (l1 != null || l2 != null) {
                int val1 = l1 == null ? 0 : l1.val;
                int val2 = l2 == null ? 0 : l2.val;
                int sum = val1 + val2 + carry;
                if (head == null) {
                    head = tail = new ListNode(sum % 10);
                } else {
                    tail.next = new ListNode(sum % 10);
                    tail = tail.next;
                }
                carry = sum / 10;
                if (l1 != null) {
                    l1 = l1.next;
                }
                if (l2 != null) {
                    l2 = l2.next;
                }
            }
            if (carry > 0) {
                tail.next = new ListNode(carry);
            }
            return head;
        }
    }

    // https://leetcode.cn/problems/longest-substring-without-repeating-characters/
    static class Solution3 {
        public int lengthOfLongestSubstring(String s) {
            Set<Character> set = new HashSet<>();
            int right = -1;
            int n = s.length();
            int ans = 0;
            for (int left = 0; left < n; left++) {
                while (right + 1 < n && !set.contains(s.charAt(right + 1))) {
                    set.add(s.charAt(right + 1));
                    right++;
                }
                ans = Math.max(ans, right - left + 1);
                set.remove(s.charAt(left));
            }
            return ans;
        }
    }

    // https://leetcode.cn/problems/median-of-two-sorted-arrays/
    static class Solution4 {
        public double findMedianSortedArrays(int[] nums1, int[] nums2) {
            int m = nums1.length;
            int n = nums2.length;
            double median = 0.0;
            int totalLength = m + n;
            if (totalLength % 2 == 1) {
                int midIndex = totalLength / 2;
                median = getKthElement(nums1, nums2, midIndex + 1);
                return median;
            } else {
                int midIndex1 = totalLength / 2 - 1, midIndex2 = totalLength / 2;
                median = (getKthElement(nums1, nums2, midIndex1 + 1) + getKthElement(nums1, nums2, midIndex2 + 1)) / 2.0;
                return median;
            }
        }

        /**
         * @param k 代表找出第几小的
         */
        private int getKthElement(int[] nums1, int[] nums2, int k) {
            int length1 = nums1.length, length2 = nums2.length;
            int index1 = 0, index2 = 0;
            while (true) {
                // 边界情况
                // 其中任意一个数组已经被排除完了，只需要去另一个数组里面找到第k小的数字，下标就是index + k - 1;
                if (index1 == length1) {
                    return nums2[index2 + k - 1];
                }
                if (index2 == length2) {
                    return nums1[index1 + k - 1];
                }
                // 找第1小的，比较两个数组剩余内容的第一个元素即可
                if (k == 1) {
                    return Math.min(nums1[index1], nums2[index2]);
                }

                // 正常情况
                int half = k / 2;
                int newIndex1 = Math.min(index1 + half, length1) - 1;
                int newIndex2 = Math.min(index2 + half, length2) - 1;
                int pivot1 = nums1[newIndex1], pivot2 = nums2[newIndex2];
                if (pivot1 <= pivot2) {
                    k -= (newIndex1 - index1 + 1);
                    index1 = newIndex1 + 1;
                } else {
                    k -= (newIndex2 - index2 + 1);
                    index2 = newIndex2 + 1;
                }
            }
        }
    }

    // https://leetcode.cn/problems/longest-palindromic-substring
    static class Solution5 {
        // Manacher算法
        public static String longestPalindrome(String s) {
            if (s == null || s.length() == 0) {
                return s;
            }
            if (s.length() == 1) {
                return s;
            }
            // "12132" -> "#1#2#1#3#2#"
            char[] str = manacherString(s);
            // 回文半径的大小 对应真实回文长度 = pArr[i] - 1
            int[] pArr = new int[str.length];
            // 扩展回文中心
            int C = -1;
            // 讲述中：R代表最右的扩成功的位置
            // coding：最右的扩成功位置的，再下一个位置，即有效的区域是R-1
            int R = -1;
            int max = Integer.MIN_VALUE;
            // 扩展回文串结尾下表对应真实回文串终止位置 真实回文串终止位置=扩展回文串下标 / 2
            int end = -1;
            int begin = -1;
            for (int i = 0; i < str.length; i++) { // 0 1 2
                // R第一个违规的位置，i>= R
                // i位置扩出来的答案，i位置扩的区域，至少是多大。
                // 2 * C - i就是关于C对称的点i'
                pArr[i] = R > i ? Math.min(pArr[2 * C - i], R - i) : 1;

                while (i + pArr[i] < str.length && i - pArr[i] > -1) {
                    if (str[i + pArr[i]] == str[i - pArr[i]])
                        pArr[i]++;
                    else {
                        break;
                    }
                }
                // 跟新回文半径和中心点
                if (i + pArr[i] > R) {
                    R = i + pArr[i];
                    C = i;
                }
                if (max < pArr[i]) {
                    max = pArr[i] - 1;
                    end = R / 2;
                    begin = end - max;
                }
                //max = Math.max(max, pArr[i]);
            }
            return s.substring(begin, end);
        }

        public static char[] manacherString(String str) {
            char[] charArr = str.toCharArray();
            char[] res = new char[str.length() * 2 + 1];
            int index = 0;
            for (int i = 0; i != res.length; i++) {
                res[i] = (i & 1) == 0 ? '#' : charArr[index++];
            }
            return res;
        }
    }

    // https://leetcode.cn/problems/zigzag-conversion
    static class Solution6 {
        public static String convert(String s, int numRows) {
            if (numRows < 2) {
                return s;
            }
            StringBuilder[] rows = new StringBuilder[numRows];
            for (int i = 0; i < numRows; i++) {
                rows[i] = new StringBuilder();
            }
            int index = 0;
            int flag = -1;
            //boolean flag = false;
            for (char c : s.toCharArray()) {
                rows[index].append(c);
//                if (index == 0) {
//                    flag = true;
//                } else if (index == numRows - 1) {
//                    flag = false;
//                }
//                if (flag) {
//                    index++;
//                } else {
//                    index--;
//                }
                if (index == 0 || index == numRows - 1) {
                    flag = -flag;
                }
                index += flag;
            }
            StringBuilder sb = new StringBuilder();
            for (StringBuilder builder : rows) {
                sb.append(builder);
            }
            return sb.toString();
        }
    }

    // https://leetcode.cn/problems/reverse-integer
    static class Solution7 {
        public int reverse(int x) {
            int ans = 0;
            while (x != 0) {
                int digit = x % 10;
                // 判断溢出
                if (ans < Integer.MIN_VALUE / 10 || ans > Integer.MAX_VALUE / 10) {
                    return 0;
                }
                ans = ans * 10 + digit;
                x = x / 10;
            }
            return ans;
        }
    }

    // https://leetcode.cn/problems/string-to-integer-atoi
    static class Solution8 {
        public static int myAtoi(String s) {
            int sign = 1, ans = 0, index = 0;
            char[] array = s.toCharArray();
            // 去除前导空格
            while (index < array.length && array[index] == ' ') {
                index++;
            }
            // 判断是正数还是负数
            if (index < array.length && (array[index] == '-' || array[index] == '+')) {
                sign = array[index++] == '-' ? -1 : 1;
            }
            while (index < array.length && array[index] >= '0' && array[index] <= '9') {
                int digit = array[index++] - '0';
                if ((ans * 10L + digit) > Integer.MAX_VALUE) {
                    // ans > (Integer.MAX_VALUE - digit) / 10
                    return sign == 1 ? Integer.MAX_VALUE : Integer.MIN_VALUE;
                }
                ans = ans * 10 + digit;
            }
            // 乘上符号
            return ans * sign;
        }
    }

    // https://leetcode.cn/problems/palindrome-number
    static class Solution9 {
        // 解法1 把数字转换为字符串
        public boolean isPalindrome1(int x) {
            String reversedStr = (new StringBuilder(String.valueOf(x))).reverse().toString();
            return (String.valueOf(x)).equals(reversedStr);
        }

        // 解法2 数学解法 反转一半数字
        public boolean isPalindrome(int x) {
            // 所有的负数都不是回文数 因为前面带了一个符号-
            if (x < 0) {
                return false;
            }
            // 如果数字的最后一位是 0，为了使该数字为回文，则其第一位数字也应该是 0
            if (x % 10 == 0 && x != 0) {
                return false;
            }
            // 处理可能是回文数的数字
            int reverseNumber = 0;
            // 当反转后的数字已经大于x之后说明已经反转一半了
            // 例如 1221 当reverseNumber=12时候，x=12，已经不大于当reverseNumber了，可以停止反转了
            // 同理 12321 当当reverseNumber=123时候，x=12停止反转
            while (x > reverseNumber) {
                reverseNumber = reverseNumber * 10 + x % 10;
                x /= 10;
            }
            // 判断奇数                         判断偶数
            return x == reverseNumber / 10 || x == reverseNumber;
        }
    }

    // https://leetcode.cn/problems/regular-expression-matching
    static class Solution10 {
        // 状态转移方程
        // 1.如果 p.charAt(j) == s.charAt(i) : dp[i][j] = dp[i-1][j-1]；
        // 2.如果 p.charAt(j) == '.' : dp[i][j] = dp[i-1][j-1]；
        // 3.如果 p.charAt(j) == '*'：
        // 3.1如果 p.charAt(j-1) != s.charAt(i) : dp[i][j] = dp[i][j-2]
        // 3.2如果 p.charAt(i-1) == s.charAt(i) or p.charAt(i-1) == '.'：
        //  3.2.1dp[i][j] = dp[i-1][j] //in this case, a* counts as multiple a
        //  3.2.2or dp[i][j] = dp[i][j-1] // in this case, a* counts as single a
        //  3.2.3or dp[i][j] = dp[i][j-2] // in this case, a* counts as empty
        public boolean isMatch(String s, String p) {
            int m = s.length();
            int n = p.length();
            boolean[][] dp = new boolean[m + 1][n + 1];
            dp[0][0] = true;
            for (int j = 2; j <= n; j += 2)
                dp[0][j] = dp[0][j - 2] && p.charAt(j - 1) == '*';
            for (int i = 1; i <= m; i++) {
                for (int j = 1; j <= n; j++) {
                    // 情况3
                    if (p.charAt(j - 1) == '*') {
                        // 情况3.1
                        if (dp[i][j - 2]) dp[i][j] = true;
                            // 情况3.2 *号前一个字符和s对应字符相等
                        else if (dp[i - 1][j] && s.charAt(i - 1) == p.charAt(j - 2)) dp[i][j] = true;
                            // 情况3.2 *号前一个字符为.号
                        else if (dp[i - 1][j] && p.charAt(j - 2) == '.') dp[i][j] = true;
                    } else {
                        //情况1
                        if (dp[i - 1][j - 1] && s.charAt(i - 1) == p.charAt(j - 1)) dp[i][j] = true;
                            //情况2
                        else if (dp[i - 1][j - 1] && p.charAt(j - 1) == '.') dp[i][j] = true;
                    }
                }
            }
            return dp[m][n];
        }
    }

    // https://leetcode.cn/problems/container-with-most-water
    static class Solution11 {
        public int maxArea(int[] height) {
            int l = 0;
            int r = height.length - 1;
            int ans = 0;
            while (l < r) {
                int area = Math.min(height[l], height[r]) * (r - l);
                ans = Math.max(area, ans);
                if (height[l] <= height[r]) {
                    l++;
                } else {
                    r--;
                }
            }
            return ans;
        }
    }

    // https://leetcode.cn/problems/integer-to-roman
    static class Solution12 {
        int[] values = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
        String[] symbols = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};

        public String intToRoman(int num) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < values.length; i++) {
                int curValue = values[i];
                String symbol = symbols[i];
                while (num >= curValue) {
                    num -= curValue;
                    sb.append(symbol);
                }
                if (num == 0) {
                    break;
                }
            }
            return sb.toString();
        }

        // 官方题解 硬编码格式  把每一位的可能取值表示出来，先求数字的每一位置，在得到对应字符
        String[] thousands = {"", "M", "MM", "MMM"};
        String[] hundreds = {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"};
        String[] tens = {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"};
        String[] ones = {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"};

        public String intToRoman2(int num) {
            StringBuilder sb = new StringBuilder();
            // 得到千位
            sb.append(thousands[num / 1000]);
            // 得到百位
            sb.append(hundreds[num % 1000 / 100]);
            // 得到十位
            sb.append(tens[num % 100 / 10]);
            // 得到个位
            sb.append(ones[num % 10]);
            return sb.toString();
        }
    }

    // https://leetcode.cn/problems/roman-to-integer
    static class Solution13 {
        Map<Character, Integer> symbolValues = new HashMap<Character, Integer>() {{
            put('I', 1);
            put('V', 5);
            put('X', 10);
            put('L', 50);
            put('C', 100);
            put('D', 500);
            put('M', 1000);
        }};

        public int romanToInt(String s) {
            int result = 0;
            int n = s.length();
            for (int i = 0; i < n; i++) {
                int curValue = symbolValues.get(s.charAt(i));
                // 如果当前数字比下一个数字小 例如这样的情况IV IX等等 可以换算成 IV = -I + V = -1 + 5 = 4
                if (i < n - 1 && curValue < symbolValues.get(s.charAt(i + 1))) {
                    result -= curValue;
                } else {
                    result += curValue;
                }
            }
            return result;
        }
    }

    // https://leetcode.cn/problems/longest-common-prefix
    static class Solution14 {
        public String longestCommonPrefix(String[] strs) {
            // 思路先两两比较 因为整个字符串数组如果存在公共前缀，那么这个前缀是满足后面的一切的字符串的
            if (strs == null || strs.length == 0) {
                return "";
            }
            String prefix = strs[0];
            for (String str : strs) {
                prefix = findCommonPrefix(prefix, str);
                if (prefix.length() == 0) {
                    return "";
                }
            }
            return prefix;

        }

        private String findCommonPrefix(String str1, String str2) {
            int minLength = Math.min(str1.length(), str2.length());
            int index = 0;
            while (index < minLength && str1.charAt(index) == str2.charAt(index)) {
                index++;
            }
            return str1.substring(0, index);
        }

        // 思路升级
        // 两两比较想到可以把整个数组划分成两半 同时开始比较即可
        public String longestCommonPrefix2(String[] strs) {
            if (strs == null || strs.length == 0) {
                return "";
            } else {
                return longestCommonPrefix(strs, 0, strs.length - 1);
            }
        }

        public String longestCommonPrefix(String[] strs, int start, int end) {
            if (start == end) {
                return strs[start];
            } else {
                int mid = (end - start) / 2 + start;
                String left = longestCommonPrefix(strs, start, mid);
                String right = longestCommonPrefix(strs, mid + 1, end);
                return findCommonPrefix(left, right);
            }
        }
    }

    // https://leetcode.cn/problems/3sum
    static class Solution15 {
        public List<List<Integer>> threeSum(int[] nums) {
            Arrays.sort(nums);
            List<List<Integer>> res = new ArrayList<>();
            for (int first = 0; first < nums.length - 2; first++) {
                // 排序后如果数组第一个数字大于0 可以不用进行了
                if (nums[first] > 0) {
                    break;
                }
                // 防止出现这种情况 输入：nums[0,0,0,0] 输出：[[0,0,0],[0,0,0]] 预期结果：[[0,0,0]]
                // first对应位置的数字不能使用多次和second、third一样的 因此遇到相同的要排除掉
                // 遇到当前数字和前一个数字一样的直接排除 因为已经处理过
                if (first > 0 && nums[first] == nums[first - 1]) continue;
                int second = first + 1;
                int third = nums.length - 1;
                while (second < third) {
                    int sum = nums[first] + nums[second] + nums[third];
                    if (sum > 0) {
                        while (second < third && nums[third] == nums[--third]) ;
                    } else if (sum == 0) {
                        ArrayList<Integer> arrayList = new ArrayList<>();
                        arrayList.add(nums[first]);
                        arrayList.add(nums[second]);
                        arrayList.add(nums[third]);
                        res.add(arrayList);
                        while (second < third && nums[second] == nums[++second]) ;
                        while (second < third && nums[third] == nums[--third]) ;
                    } else {
                        while (second < third && nums[second] == nums[++second]) ;
                    }
                }
            }
            return res;
        }
    }

    // https://leetcode.cn/problems/3sum-closest
    static class Solution16 {
        public static int threeSumClosest(int[] nums, int target) {
            Arrays.sort(nums);
            int ans = nums[0] + nums[1] + nums[2];
            for (int first = 0; first < nums.length; first++) {
                int second = first + 1;
                int third = nums.length - 1;
                while (second < third) {
                    int sum = nums[first] + nums[second] + nums[third];
                    if (Math.abs(target - sum) < Math.abs(target - ans)) {
                        ans = sum;
                    }
                    if (ans > target) {
                        third--;
                    } else if (ans < target) {
                        second++;
                    } else {
                        return ans;
                    }
                }
            }
            return ans;
        }
    }

    // https://leetcode.cn/problems/letter-combinations-of-a-phone-number
    static class Solution17 {
        Map<Character, String> phoneMap = new HashMap<Character, String>() {{
            put('2', "abc");
            put('3', "def");
            put('4', "ghi");
            put('5', "jkl");
            put('6', "mno");
            put('7', "pqrs");
            put('8', "tuv");
            put('9', "wxyz");
        }};

        public List<String> letterCombinations(String digits) {
            List<String> combinations = new ArrayList<>();
            if (digits.length() == 0) {
                return combinations;
            }
            backTrace(combinations, digits, 0, new StringBuilder());
            return combinations;
        }

        private void backTrace(List<String> combinations, String digits, int index, StringBuilder sb) {
            if (index == digits.length()) {
                combinations.add(sb.toString());
            } else {
                String str = phoneMap.get(digits.charAt(index));
                for (int i = 0; i < str.length(); i++) {
                    sb.append(str.charAt(i));
                    backTrace(combinations, digits, index + 1, sb);
                    sb.deleteCharAt(index);
                }
            }
        }
    }

    // https://leetcode.cn/problems/4sum
    static class Solution18 {
        public List<List<Integer>> fourSum(int[] nums, int target) {
            // 思路 可以先确定一个数字  然后转换成三数之和的模式去逐一确认剩下的数字
            List<List<Integer>> ans = new ArrayList<List<Integer>>();
            if (nums == null || nums.length < 4) {
                return ans;
            }
            Arrays.sort(nums);
            int length = nums.length;
            // 第一个数
            for (int i = 0; i < length - 3; i++) {
                // 去掉重复元素
                if (i > 0 && nums[i] == nums[i - 1]) {
                    continue;
                }
                // 排序后如果数组前四个的值已经大于target就没必要找了，后面的只会更大
                if ((long) nums[i] + nums[i + 1] + nums[i + 2] + nums[i + 3] > target) {
                    break;
                }
                // 第二个数
                for (int j = i + 1; j < length - 2; j++) {
                    // 去掉重复元素
                    if (j > i + 1 && nums[j] == nums[j - 1]) {
                        continue;
                    }
                    // 第一个数和第二个数确定
                    if ((long) nums[i] + nums[j] + nums[j + 1] + nums[j + 2] > target) {
                        break;
                    }
                    // 第三个数和第四个数
                    int left = j + 1, right = length - 1;
                    while (left < right) {
                        long sum = (long) nums[i] + nums[j] + nums[left] + nums[right];
                        if (sum == target) {
                            ans.add(Arrays.asList(nums[i], nums[j], nums[left], nums[right]));
                            while (left < right && nums[left] == nums[left + 1]) {
                                left++;
                            }
                            left++;
                            while (left < right && nums[right] == nums[right - 1]) {
                                right--;
                            }
                            right--;
                        } else if (sum < target) {
                            left++;
                        } else {
                            right--;
                        }
                    }
                }
            }
            return ans;
        }
    }

    // https://leetcode.cn/problems/remove-nth-node-from-end-of-list
    static class Solution19 {
        static class ListNode {
            int val;
            ListNode next;

            ListNode() {
            }

            ListNode(int val) {
                this.val = val;
            }

            ListNode(int val, ListNode next) {
                this.val = val;
                this.next = next;
            }
        }

        public ListNode removeNthFromEnd(ListNode head, int n) {
            // 解法1 要删除倒数第几个节点，就是删除正着数的第length(链表长度) - n + 1
            ListNode dummy = new ListNode(0, head);
            int length = getLength(head);
            ListNode cur = dummy;
            for (int i = 1; i < length - n + 1; ++i) {
                cur = cur.next;
            }
            cur.next = cur.next.next;
            return dummy.next;
        }

        public int getLength(ListNode head) {
            int length = 0;
            while (head != null) {
                ++length;
                head = head.next;
            }
            return length;
        }

        public ListNode removeNthFromEnd2(ListNode head, int n) {
            // 解法2 使用栈
            ListNode dummy = new ListNode(0, head);
            Deque<ListNode> stack = new LinkedList<ListNode>();
            ListNode cur = dummy;
            while (cur != null) {
                stack.push(cur);
                cur = cur.next;
            }
            // 弹出要被剔除的节点
            for (int i = 0; i < n; ++i) {
                stack.pop();
            }
            ListNode prev = stack.peek();
            prev.next = prev.next.next;
            return dummy.next;
        }

        public ListNode removeNthFromEnd3(ListNode head, int n) {
            // 解法3 使用双指针
            ListNode dummy = new ListNode(0, head);
            ListNode fast = head;
            ListNode slow = dummy;
            for (int i = 0; i < n; ++i) {
                fast = fast.next;
            }
            while (fast != null) {
                fast = fast.next;
                slow = slow.next;
            }
            slow.next = slow.next.next;
            return dummy.next;
        }
    }

    // https://leetcode.cn/problems/valid-parentheses
    static class Solution20 {

        Map<Character, Character> map = new HashMap<Character, Character>() {{
            put(')', '(');
            put(']', '[');
            put('}', '{');
        }};

        public boolean isValid(String s) {

            // 括号的个数必定是偶数个
            int length = s.length();
            if (length % 2 != 0) {
                return false;
            }
            Deque<Character> stack = new LinkedList<Character>();
            for (int i = 0; i < length; i++) {
                char ch = s.charAt(i);
                // 遇到了左括号
                if (ch == '(' || ch == '[' || ch == '{') {
                    stack.push(ch);
                } else {
                    if (!stack.isEmpty() && map.get(ch) == stack.peek()) {
                        stack.pop();
                    } else {
                        return false;
                    }
                }
            }
            return stack.isEmpty();
        }
    }

    // https://leetcode.cn/problems/merge-two-sorted-lists
    static class Solution21 {

        static class ListNode {
            int val;
            ListNode next;

            ListNode() {
            }

            ListNode(int val) {
                this.val = val;
            }

            ListNode(int val, ListNode next) {
                this.val = val;
                this.next = next;
            }
        }

        public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
            if (list1 == null && list2 == null) {
                return null;
            }
            if (list1 == null) {
                return list2;
            }
            if (list2 == null) {
                return list1;
            }
            ListNode head = new ListNode(-1);
            ListNode pre = head;
            while (list1 != null && list2 != null) {
                if (list1.val > list2.val) {
                    pre.next = list2;
                    list2 = list2.next;
                } else {
                    pre.next = list1;
                    list1 = list1.next;
                }
                pre = pre.next;
            }
            pre.next = list1 == null ? list2 : list1;
            return head.next;
        }
    }

    // https://leetcode.cn/problems/generate-parentheses
    static class Solution22 {
        List<String> ans = new ArrayList<>();

        public List<String> generateParenthesis(int n) {
            if (n < 0) {
                return ans;
            }
            getAll("", n, n);
            return ans;
        }

        private void getAll(String s, int left, int right) {
            if (left == 0 && right == 0) {
                ans.add(s);
                return;
            }
            // 左括号和右括号数量相等 只能方放左括号
            if (left == right) {
                getAll(s + "(", left - 1, right);

            }// 左括号小于右括号 随便放左边还是右边都可以
            else if (left < right) {
                if (left > 0) {
                    getAll(s + "(", left - 1, right);
                }
                getAll(s + ")", left, right - 1);
            }
        }
    }

    // https://leetcode.cn/problems/merge-k-sorted-lists
    static class Solution23 {
        static class ListNode {
            int val;
            ListNode next;

            ListNode() {
            }

            ListNode(int val) {
                this.val = val;
            }

            ListNode(int val, ListNode next) {
                this.val = val;
                this.next = next;
            }
        }

        static class Data implements Comparable<Data> {
            int val;
            ListNode node;

            Data(int val, ListNode node) {
                this.val = val;
                this.node = node;
            }

            @Override
            public int compareTo(Data o) {
                return this.val - o.val;
            }
        }

        PriorityQueue<Data> priorityQueue = new PriorityQueue<>();

        public ListNode mergeKLists(ListNode[] lists) {
            for (ListNode listNode : lists) {
                if (listNode != null)
                    priorityQueue.add(new Data(listNode.val, listNode));
            }
            ListNode ans = new ListNode(-1);
            ListNode cur = ans;
            while (!priorityQueue.isEmpty()) {
                Data data = priorityQueue.poll();
                cur.next = data.node;
                cur = cur.next;
                if (data.node.next != null) {
                    priorityQueue.add(new Data(data.node.next.val, data.node.next));
                }
            }
            return ans.next;
        }
    }

    // https://leetcode.cn/problems/swap-nodes-in-pairs
    static class Solution24 {
        static class ListNode {
            int val;
            ListNode next;

            ListNode() {
            }

            ListNode(int val) {
                this.val = val;
            }

            ListNode(int val, ListNode next) {
                this.val = val;
                this.next = next;
            }
        }

        public ListNode swapPairs(ListNode head) {
            // 迭代方法
            if (head == null || head.next == null) {
                return head;
            }
            ListNode dummyNode = new ListNode(-1);
            dummyNode.next = head;
            ListNode cur = dummyNode;
            while (cur.next != null && cur.next.next != null) {
                ListNode node1 = cur.next;
                ListNode node2 = cur.next.next;
                cur.next = node2;
                node1.next = node2.next;
                node2.next = node1;
                cur = node1;
            }
            return dummyNode.next;
        }

        public ListNode swapPairs2(ListNode head) {
            // 递归方法
            if (head == null || head.next == null) {
                return head;
            }
            // 新的头节点
            ListNode newHead = head.next;
            // 原来头节点的下一个节点是翻转节点的头
            head.next = swapPairs(newHead.next);
            // 翻转
            newHead.next = head;
            return newHead;
        }
    }

    // https://leetcode.cn/problems/reverse-nodes-in-k-group
    static class Solution25 {
        static class ListNode {
            int val;
            ListNode next;

            ListNode() {
            }

            ListNode(int val) {
                this.val = val;
            }

            ListNode(int val, ListNode next) {
                this.val = val;
                this.next = next;
            }
        }

        public ListNode reverseKGroup(ListNode head, int k) {
            ListNode start = head;
            ListNode end = getKGroupEnd(start, k);
            // 链表长度不够k个
            if (end == null) {
                return head;
            }
            // 第一次翻转的位置的结束节点作为新头
            head = end;
            reverse(start, end);
            ListNode lastReverseNode = start;
            while (start.next != null) {
                start = lastReverseNode.next;
                end = getKGroupEnd(start, k);
                if (end == null) {
                    return head;
                }
                reverse(start, end);
                // 重新连接上一次结尾节点和翻转后的链表的头节点
                lastReverseNode.next = end;
                lastReverseNode = start;
            }
            return head;
        }

        // 返回k个节点的终止位置
        public static ListNode getKGroupEnd(ListNode start, int k) {
            while (--k != 0 && start != null) {
                start = start.next;
            }
            return start;
        }

        // 翻转指定位置链表
        public static void reverse(ListNode start, ListNode end) {
            end = end.next;
            ListNode pre = null;
            ListNode next = null;
            ListNode cur = start;
            while (cur != end) {
                next = cur.next;
                cur.next = pre;
                pre = cur;
                cur = next;
            }
            start.next = end;
        }
    }

    // https://leetcode.cn/problems/remove-duplicates-from-sorted-array
    static class Solution26 {
        public int removeDuplicates(int[] nums) {
            if (nums.length == 0) {
                return 0;
            }
            // 新数组第一个元素的位置
            int index = 0;
            for (int i = 1; i < nums.length; i++) {
                // 不相等说明是新数组的第index+1位置元素 放上去即可
                if (nums[i] != nums[index]) {
                    index++;
                    nums[index] = nums[i];
                }
            }
            return index + 1;
        }
    }

    // https://leetcode.cn/problems/remove-element
    static class Solution27 {
        public int removeElement(int[] nums, int val) {
            int n = nums.length;
            int left = 0;
            for (int right = 0; right < n; right++) {
                if (nums[right] != val) {
                    nums[left] = nums[right];
                    left++;
                }
            }
            return left;
        }
    }

    // https://leetcode.cn/problems/find-the-index-of-the-first-occurrence-in-a-string
    public int strStr(String haystack, String needle) {
        int[] next = new int[needle.length() + 1];
        getNext(needle.toCharArray(), next);
        return KMP(haystack.toCharArray(), needle.toCharArray(), next);
    }

    private int KMP(char[] str, char[] pattern, int[] next) {
        int i = 0;
        int j = 0;
        while (i < str.length && j < pattern.length) {
            if (j == -1 || str[i] == pattern[j]) {
                i++;
                j++;
            } else {
                j = next[j];
            }
        }
        if (j == pattern.length) {
            return i - j;
        } else {
            return -1;
        }
    }

    public void getNext(char[] pattern, int[] next) {
        next[0] = -1;
        int i = 0;
        int j = -1;
        while (i < pattern.length) {
            if (j == -1) {
                i++;
                j++;
            } else if (pattern[i] == pattern[j]) {
                i++;
                j++;
                next[i] = j;
            } else {
                j = next[j];
            }
        }
    }

    // 左神版KMP算法
    private int KMP2(char[] str, char[] pattern, int[] next) {
        // str和pattern比对的位置
        int x = 0;
        int y = 0;
        while (x < str.length && y < pattern.length) {
            if (str[x] == pattern[y]) {
                // 匹配接着比较下一个位置
                x++;
                y++;
            } else if (y == 0) {
                // y已经不能再往跳走
                x++;
            } else {
                // y可以往前走
                y = next[y];
            }
        }
        return y == pattern.length ? x - y : -1;
    }

    public static int[] getNextArray(char[] ms) {
        if (ms.length == 1) {
            return new int[]{-1};
        }
        int[] next = new int[ms.length];
        next[0] = -1;
        next[1] = 0;
        int pos = 2;
        // 即是当前前缀和的长度，也是当前需要与pos-1下标位置进行比较的位置
        int cn = 0;
        while (pos < next.length) {
            if (ms[pos - 1] == ms[cn]) {
                next[pos++] = ++cn;
            } else if (cn > 0) {
                cn = next[cn];
            } else {
                next[pos++] = 0;
            }
        }
        return next;
    }

    // https://leetcode.cn/problems/divide-two-integers
    static class Solution29 {
        public static int divide(int dividend, int divisor) {
            // 特殊情况
            // 考虑被除数为最小值的情况
            if (dividend == Integer.MIN_VALUE) {
                if (divisor == 1) {
                    return Integer.MIN_VALUE;
                }
                if (divisor == -1) {
                    return Integer.MAX_VALUE;
                }
            }
            // 考虑除数为最小值的情况
            if (divisor == Integer.MIN_VALUE) {
                return dividend == Integer.MIN_VALUE ? 1 : 0;
            }
            // 考虑被除数为 0 的情况
            if (dividend == 0) {
                return 0;
            }
            // 一般情况
            // 将所有的正数取相反数，这样就只需要考虑一种情况
            boolean rev = false;
            if (dividend > 0) {
                dividend = -dividend;
                rev = !rev;
            }
            if (divisor > 0) {
                divisor = -divisor;
                rev = !rev;
            }
            List<Integer> candidates = new ArrayList<Integer>();
            candidates.add(divisor);
            int index = 0;
            // 注意溢出
            while (candidates.get(index) >= dividend - candidates.get(index)) {
                candidates.add(candidates.get(index) + candidates.get(index));
                ++index;
            }
            int ans = 0;
            for (int i = candidates.size() - 1; i >= 0; --i) {
                if (candidates.get(i) >= dividend) {
                    ans += 1 << i;
                    dividend -= candidates.get(i);
                }
            }
            return rev ? -ans : ans;
        }
    }

    // https://leetcode.cn/problems/substring-with-concatenation-of-all-words
    static class Solution30 {
        public static List<Integer> findSubstring(String s, String[] words) {
            List<Integer> res = new ArrayList<>();
            int m = words.length, n = words[0].length(), ls = s.length();
            for (int i = 0; i < n; i++) {
                // 遍历的长度超过了整个字符串的长度，退出循环
                if (i + m * n > ls) {
                    break;
                }
                // differ表示窗口中的单词频次和words中的单词频次之差
                Map<String, Integer> differ = new HashMap<>();
                // 初始化窗口，窗口长度为m * n,依次计算窗口里每个切分的单词的频次
                for (int j = 0; j < m; j++) {
                    String word = s.substring(i + j * n, i + (j + 1) * n);
                    differ.put(word, differ.getOrDefault(word, 0) + 1);
                }
                // 遍历words中的word，对窗口里每个单词计算差值
                for (String word : words) {
                    differ.put(word, differ.getOrDefault(word, 0) - 1);
                    // 差值为0时，移除掉这个word
                    if (differ.get(word) == 0) {
                        differ.remove(word);
                    }
                }
                // 开始滑动窗口
                for (int start = i; start < ls - m * n + 1; start += n) {
                    if (start != i) {
                        // 右边的单词滑进来
                        String word = s.substring(start + (m - 1) * n, start + m * n);
                        differ.put(word, differ.getOrDefault(word, 0) + 1);
                        if (differ.get(word) == 0) {
                            differ.remove(word);
                        }
                        // 左边的单词滑出去
                        word = s.substring(start - n, start);
                        differ.put(word, differ.getOrDefault(word, 0) - 1);
                        if (differ.get(word) == 0) {
                            differ.remove(word);
                        }
                    }
                    // 窗口匹配的单词数等于words中对应的单词数
                    if (differ.isEmpty()) {
                        res.add(start);
                    }
                }
            }
            return res;
        }
    }

    // https://leetcode.cn/problems/next-permutation
    static class Solution31 {
        public void nextPermutation(int[] nums) {
            int i = nums.length - 2;
            while (i >= 0 && nums[i] >= nums[i + 1]) {
                i--;
            }
            if (i > 0) {
                int j = nums.length - 1;
                while (j >= 0 && nums[i] >= nums[j]) {
                    j--;
                }
                swap(nums, i, j);
            }
            reverse(nums, i + 1);
        }

        private void reverse(int[] nums, int begin) {
            int end = nums.length - 1;
            while (begin < end) {
                swap(nums, begin, end);
                begin++;
                end--;
            }
        }

        private void swap(int[] nums, int i, int j) {
            int tmp = nums[i];
            nums[i] = nums[j];
            nums[j] = tmp;
        }
    }

    // https://leetcode.cn/problems/longest-valid-parentheses
    static class Solution32 {
        public int longestValidParentheses(String s) {
            // 使用栈进行解决
            int ans = 0;
            Deque<Integer> stack = new LinkedList<>();
            stack.push(-1);
            for (int i = 0; i < s.length(); i++) {
                if (s.charAt(i) == '(') {
                    stack.push(i);
                } else {
                    stack.pop();
                    if (stack.isEmpty()) {
                        stack.push(i);
                    } else {
                        ans = Math.max(ans, i - stack.peek());
                    }
                }
            }
            return ans;
        }
    }

    // https://leetcode.cn/problems/search-in-rotated-sorted-array
    static class Solution33 {
        public static int search(int[] nums, int target) {
            int n = nums.length;
            if (n == 0) {
                return -1;
            }
            if (n == 1) {
                return nums[0] == target ? 0 : -1;
            }
            int left = 0;
            int right = nums.length - 1;
            while (left <= right) {
                int mid = (left + right) / 2;
                if (nums[mid] == target) {
                    return mid;
                }
                // 判断哪个部分是有序的区域
                if (nums[0] <= nums[mid]) {
                    // l - mid区域是有序的
                    if (nums[0] <= target && target < nums[mid]) {
                        right = mid - 1;
                    } else {
                        left = mid + 1;
                    }
                } else {
                    // mid + 1 到 r区域是有序的
                    if (nums[mid] < target && target <= nums[nums.length - 1]) {
                        left = mid + 1;
                    } else {
                        right = mid - 1;
                    }
                }
            }
            return -1;
        }
    }

    // https://leetcode.cn/problems/find-first-and-last-position-of-element-in-sorted-array
    static class Solution34 {
        public static int[] searchRange(int[] nums, int target) {
            // 进行两次二分查找就行了
            int left = 0;
            int right = nums.length - 1;
            int first = -1;
            int last = -1;
            while (left <= right) {
                int mid = (left + right) / 2;
                if (nums[mid] == target) {
                    first = mid;
                    right = mid - 1;
                } else if (nums[mid] < target) {
                    left = mid + 1;
                } else {
                    right = mid - 1;
                }
            }
            left = 0;
            right = nums.length - 1;
            while (left <= right) {
                int mid = (left + right) / 2;
                if (nums[mid] == target) {
                    last = mid;
                    left = mid + 1;
                } else if (nums[mid] < target) {
                    left = mid + 1;
                } else {
                    right = mid - 1;
                }
            }
            return new int[]{first, last};
        }
    }

    // https://leetcode.cn/problems/search-insert-position
    static class Solution35 {
        public int searchInsert(int[] nums, int target) {
            // 找到第一个大于等于mid的位置
            int left = 0;
            int right = nums.length - 1;
            int index = -1;
            while (left <= right) {
                // 防止mid溢出
                // int mid = (left + right) / 2;
                int mid = (right - left) / 2 + left;
                if (nums[mid] >= target) {
                    index = mid;
                    right = mid - 1;
                } else {
                    left = mid + 1;
                }
            }
            return index == -1 ? nums.length : index;
        }
    }

    // https://leetcode.cn/problems/valid-sudoku
    static class Solution36 {
        public boolean isValidSudoku(char[][] board) {
            boolean[][] row = new boolean[9][10];
            boolean[][] col = new boolean[9][10];
            boolean[][] bucket = new boolean[9][10];
            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    int bid = 3 * (i / 3) + (j / 3);
                    if (board[i][j] != '.') {
                        int num = board[i][j] - '0';
                        if (row[i][num] || col[j][num] || bucket[bid][num]) {
                            return false;
                        }
                        // 记录数字出现在了哪一行
                        row[i][num] = true;
                        // 记录数字出现在了哪一列
                        col[j][num] = true;
                        // 记录数字在哪个桶出现
                        bucket[bid][num] = true;
                    }
                }
            }
            return true;
        }
    }

    // https://leetcode.cn/problems/sudoku-solver
    static class Solution37 {
        public void solveSudoku(char[][] board) {
            boolean[][] row = new boolean[9][10];
            boolean[][] col = new boolean[9][10];
            boolean[][] bucket = new boolean[9][10];
            initMaps(board, row, col, bucket);
            process(board, 0, 0, row, col, bucket);
        }

        public static void initMaps(char[][] board, boolean[][] row, boolean[][] col, boolean[][] bucket) {
            for (int i = 0; i < 9; i++) {
                for (int j = 0; j < 9; j++) {
                    int bid = 3 * (i / 3) + (j / 3);
                    if (board[i][j] != '.') {
                        int num = board[i][j] - '0';
                        row[i][num] = true;
                        col[j][num] = true;
                        bucket[bid][num] = true;
                    }
                }
            }
        }

        //  当前来到(i,j)这个位置，如果已经有数字，跳到下一个位置上
        //  当前来到(i,j)这个位置，如果没有数字，尝试1~9，不能和row、col、bucket冲突
        public static boolean process(char[][] board, int i, int j, boolean[][] row, boolean[][] col, boolean[][] bucket) {
            if (i == 9) {
                return true;
            }
            // 当离开(i，j)，应该去哪？(nexti, nextj)
            // 下一个board中的位置
            int nexti = j != 8 ? i : i + 1;
            int nextj = j != 8 ? j + 1 : 0;
            if (board[i][j] != '.') {
                return process(board, nexti, nextj, row, col, bucket);
            } else {
                // 可以尝试1~9
                int bid = 3 * (i / 3) + (j / 3);
                for (int num = 1; num <= 9; num++) { // 尝试每一个数字1~9
                    if ((!row[i][num]) && (!col[j][num]) && (!bucket[bid][num])) {
                        // 可以尝试num
                        row[i][num] = true;
                        col[j][num] = true;
                        bucket[bid][num] = true;
                        board[i][j] = (char) (num + '0');
                        if (process(board, nexti, nextj, row, col, bucket)) {
                            return true;
                        }
                        // 恢复现场，尝试下一个数字
                        row[i][num] = false;
                        col[j][num] = false;
                        bucket[bid][num] = false;
                        board[i][j] = '.';
                    }
                }
                return false;
            }
        }
    }

    // https://leetcode.cn/problems/count-and-say
    static class Solution38 {
        public String countAndSay(int n) {
            String str = "1";
            for (int i = 2; i < n; i++) {
                StringBuilder sb = new StringBuilder();
                int start = 0;
                int pos = 0;
                while (pos < str.length()) {
                    while ((pos < str.length()) && str.charAt(pos) == str.charAt(start)) {
                        pos++;
                    }
                    sb.append(pos - start).append(str.charAt(start));
                    start = pos;
                }
                str = sb.toString();
            }
            return str;
        }

        // 对于数据量很小的题，可以选择写一个通用算法，得到结果，然后收集结果，写一个查这个结果的方法即可
        public String countAndSay2(int n) {
            String[] arr = {
                    "", "1", "11", "21", "1211", "111221", "312211", "13112221", "1113213211", "31131211131221",
                    "13211311123113112211", "11131221133112132113212221", "3113112221232112111312211312113211",
                    "1321132132111213122112311311222113111221131221",
                    "11131221131211131231121113112221121321132132211331222113112211",
                    "311311222113111231131112132112311321322112111312211312111322212311322113212221",
                    "132113213221133112132113311211131221121321131211132221123113112221131112311332111213211322211312113211",
                    "11131221131211132221232112111312212321123113112221121113122113111231133221121321132132211331121321231231121113122113322113111221131221",
                    "31131122211311123113321112131221123113112211121312211213211321322112311311222113311213212322211211131221131211132221232112111312111213111213211231131122212322211331222113112211",
                    "1321132132211331121321231231121113112221121321132122311211131122211211131221131211132221121321132132212321121113121112133221123113112221131112311332111213122112311311123112111331121113122112132113213211121332212311322113212221",
                    "11131221131211132221232112111312111213111213211231132132211211131221131211221321123113213221123113112221131112311332211211131221131211132211121312211231131112311211232221121321132132211331121321231231121113112221121321133112132112312321123113112221121113122113121113123112112322111213211322211312113211",
                    "311311222113111231133211121312211231131112311211133112111312211213211312111322211231131122211311122122111312211213211312111322211213211321322113311213212322211231131122211311123113223112111311222112132113311213211221121332211211131221131211132221232112111312111213111213211231132132211211131221232112111312211213111213122112132113213221123113112221131112311311121321122112132231121113122113322113111221131221",
                    "132113213221133112132123123112111311222112132113311213211231232112311311222112111312211311123113322112132113213221133122112231131122211211131221131112311332211211131221131211132221232112111312111213322112132113213221133112132113221321123113213221121113122123211211131221222112112322211231131122211311123113321112131221123113111231121113311211131221121321131211132221123113112211121312211231131122211211133112111311222112111312211312111322211213211321322113311213211331121113122122211211132213211231131122212322211331222113112211",
                    "111312211312111322212321121113121112131112132112311321322112111312212321121113122112131112131221121321132132211231131122211331121321232221121113122113121113222123112221221321132132211231131122211331121321232221123113112221131112311332111213122112311311123112112322211211131221131211132221232112111312211322111312211213211312111322211231131122111213122112311311221132211221121332211213211321322113311213212312311211131122211213211331121321123123211231131122211211131221131112311332211213211321223112111311222112132113213221123123211231132132211231131122211311123113322112111312211312111322212321121113122123211231131122113221123113221113122112132113213211121332212311322113212221",
                    "3113112221131112311332111213122112311311123112111331121113122112132113121113222112311311221112131221123113112221121113311211131122211211131221131211132221121321132132212321121113121112133221123113112221131112311332111213213211221113122113121113222112132113213221232112111312111213322112132113213221133112132123123112111311222112132113311213211221121332211231131122211311123113321112131221123113112221132231131122211211131221131112311332211213211321223112111311222112132113212221132221222112112322211211131221131211132221232112111312111213111213211231132132211211131221232112111312211213111213122112132113213221123113112221133112132123222112111312211312112213211231132132211211131221131211132221121311121312211213211312111322211213211321322113311213212322211231131122211311123113321112131221123113112211121312211213211321222113222112132113223113112221121113122113121113123112112322111213211322211312113211",
                    "132113213221133112132123123112111311222112132113311213211231232112311311222112111312211311123113322112132113212231121113112221121321132132211231232112311321322112311311222113111231133221121113122113121113221112131221123113111231121123222112132113213221133112132123123112111312111312212231131122211311123113322112111312211312111322111213122112311311123112112322211211131221131211132221232112111312111213111213211231132132211211131221232112111312212221121123222112132113213221133112132123123112111311222112132113213221132213211321322112311311222113311213212322211211131221131211221321123113213221121113122113121132211332113221122112133221123113112221131112311332111213122112311311123112111331121113122112132113121113222112311311221112131221123113112221121113311211131122211211131221131211132221121321132132212321121113121112133221123113112221131112212211131221121321131211132221123113112221131112311332211211133112111311222112111312211311123113322112111312211312111322212321121113121112133221121321132132211331121321231231121113112221121321132122311211131122211211131221131211322113322112111312211322132113213221123113112221131112311311121321122112132231121113122113322113111221131221",
                    "1113122113121113222123211211131211121311121321123113213221121113122123211211131221121311121312211213211321322112311311222113311213212322211211131221131211221321123113213221121113122113121113222112131112131221121321131211132221121321132132211331121321232221123113112221131112311322311211131122211213211331121321122112133221121113122113121113222123211211131211121311121321123113111231131122112213211321322113311213212322211231131122211311123113223112111311222112132113311213211221121332211231131122211311123113321112131221123113111231121113311211131221121321131211132221123113112211121312211231131122113221122112133221121113122113121113222123211211131211121311121321123113213221121113122113121113222113221113122113121113222112132113213221232112111312111213322112311311222113111221221113122112132113121113222112311311222113111221132221231221132221222112112322211213211321322113311213212312311211131122211213211331121321123123211231131122211211131221131112311332211213211321223112111311222112132113213221123123211231132132211231131122211311123113322112111312211312111322111213122112311311123112112322211213211321322113312211223113112221121113122113111231133221121321132132211331121321232221123123211231132132211231131122211331121321232221123113112221131112311332111213122112311311123112112322211211131221131211132221232112111312111213111213211231132132211211131221131211221321123113213221123113112221131112211322212322211231131122211322111312211312111322211213211321322113311213211331121113122122211211132213211231131122212322211331222113112211",
                    "31131122211311123113321112131221123113111231121113311211131221121321131211132221123113112211121312211231131122211211133112111311222112111312211312111322211213211321322123211211131211121332211231131122211311122122111312211213211312111322211231131122211311123113322112111331121113112221121113122113111231133221121113122113121113222123211211131211121332211213211321322113311213211322132112311321322112111312212321121113122122211211232221123113112221131112311332111213122112311311123112111331121113122112132113311213211321222122111312211312111322212321121113121112133221121321132132211331121321132213211231132132211211131221232112111312212221121123222112132113213221133112132123123112111311222112132113311213211231232112311311222112111312211311123113322112132113212231121113112221121321132122211322212221121123222112311311222113111231133211121312211231131112311211133112111312211213211312111322211231131122211311123113322113223113112221131112311332211211131221131211132211121312211231131112311211232221121321132132211331221122311311222112111312211311123113322112132113213221133122211332111213112221133211322112211213322112111312211312111322212321121113121112131112132112311321322112111312212321121113122112131112131221121321132132211231131122211331121321232221121113122113121122132112311321322112111312211312111322211213111213122112132113121113222112132113213221133112132123222112311311222113111231132231121113112221121321133112132112211213322112111312211312111322212311222122132113213221123113112221133112132123222112111312211312111322212321121113121112133221121311121312211213211312111322211213211321322123211211131211121332211213211321322113311213212312311211131122211213211331121321122112133221123113112221131112311332111213122112311311123112111331121113122112132113121113222112311311222113111221221113122112132113121113222112132113213221133122211332111213322112132113213221132231131122211311123113322112111312211312111322212321121113122123211231131122113221123113221113122112132113213211121332212311322113212221",
                    "13211321322113311213212312311211131122211213211331121321123123211231131122211211131221131112311332211213211321223112111311222112132113213221123123211231132132211231131122211311123113322112111312211312111322111213122112311311123112112322211213211321322113312211223113112221121113122113111231133221121321132132211331121321232221123123211231132132211231131122211331121321232221123113112221131112311332111213122112311311123112112322211211131221131211132221232112111312211322111312211213211312111322211231131122111213122112311311221132211221121332211213211321322113311213212312311211131122211213211331121321123123211231131122211211131221232112111312211312113211223113112221131112311332111213122112311311123112112322211211131221131211132221232112111312211322111312211213211312111322211231131122111213122112311311221132211221121332211211131221131211132221232112111312111213111213211231132132211211131221232112111312211213111213122112132113213221123113112221133112132123222112111312211312112213211231132132211211131221131211322113321132211221121332211213211321322113311213212312311211131122211213211331121321123123211231131122211211131221131112311332211213211321322113311213212322211322132113213221133112132123222112311311222113111231132231121113112221121321133112132112211213322112111312211312111322212311222122132113213221123113112221133112132123222112111312211312111322212311322123123112111321322123122113222122211211232221123113112221131112311332111213122112311311123112111331121113122112132113121113222112311311221112131221123113112221121113311211131122211211131221131211132221121321132132212321121113121112133221123113112221131112212211131221121321131211132221123113112221131112311332211211133112111311222112111312211311123113322112111312211312111322212321121113121112133221121321132132211331121321132213211231132132211211131221232112111312212221121123222112311311222113111231133211121321321122111312211312111322211213211321322123211211131211121332211231131122211311123113321112131221123113111231121123222112111331121113112221121113122113111231133221121113122113121113221112131221123113111231121123222112111312211312111322212321121113121112131112132112311321322112111312212321121113122122211211232221121321132132211331121321231231121113112221121321133112132112312321123113112221121113122113111231133221121321132132211331221122311311222112111312211311123113322112111312211312111322212311322123123112112322211211131221131211132221132213211321322113311213212322211231131122211311123113321112131221123113112211121312211213211321222113222112132113223113112221121113122113121113123112112322111213211322211312113211",
                    "11131221131211132221232112111312111213111213211231132132211211131221232112111312211213111213122112132113213221123113112221133112132123222112111312211312112213211231132132211211131221131211132221121311121312211213211312111322211213211321322113311213212322211231131122211311123113223112111311222112132113311213211221121332211211131221131211132221231122212213211321322112311311222113311213212322211211131221131211132221232112111312111213322112131112131221121321131211132221121321132132212321121113121112133221121321132132211331121321231231121113112221121321133112132112211213322112311311222113111231133211121312211231131122211322311311222112111312211311123113322112132113212231121113112221121321132122211322212221121123222112111312211312111322212321121113121112131112132112311321322112111312212321121113122112131112131221121321132132211231131122111213122112311311222113111221131221221321132132211331121321231231121113112221121321133112132112211213322112311311222113111231133211121312211231131122211322311311222112111312211311123113322112132113212231121113112221121321132122211322212221121123222112311311222113111231133211121312211231131112311211133112111312211213211312111322211231131122111213122112311311222112111331121113112221121113122113121113222112132113213221232112111312111213322112311311222113111221221113122112132113121113222112311311222113111221132221231221132221222112112322211211131221131211132221232112111312111213111213211231132132211211131221232112111312211213111213122112132113213221123113112221133112132123222112111312211312111322212321121113121112133221132211131221131211132221232112111312111213322112132113213221133112132113221321123113213221121113122123211211131221222112112322211231131122211311123113321112132132112211131221131211132221121321132132212321121113121112133221123113112221131112311332111213211322111213111213211231131211132211121311222113321132211221121332211213211321322113311213212312311211131122211213211331121321123123211231131122211211131221131112311332211213211321223112111311222112132113213221123123211231132132211231131122211311123113322112111312211312111322111213122112311311123112112322211213211321322113312211223113112221121113122113111231133221121321132132211331121321232221123123211231132132211231131122211331121321232221123113112221131112311332111213122112311311123112112322211211131221131211132221232112111312211322111312211213211312111322211231131122111213122112311311221132211221121332211213211321322113311213212312311211131211131221223113112221131112311332211211131221131211132211121312211231131112311211232221121321132132211331121321231231121113112221121321133112132112211213322112312321123113213221123113112221133112132123222112311311222113111231132231121113112221121321133112132112211213322112311311222113111231133211121312211231131112311211133112111312211213211312111322211231131122111213122112311311221132211221121332211211131221131211132221232112111312111213111213211231132132211211131221232112111312211213111213122112132113213221123113112221133112132123222112111312211312111322212311222122132113213221123113112221133112132123222112311311222113111231133211121321132211121311121321122112133221123113112221131112311332211322111312211312111322212321121113121112133221121321132132211331121321231231121113112221121321132122311211131122211211131221131211322113322112111312211322132113213221123113112221131112311311121321122112132231121113122113322113111221131221",
                    "3113112221131112311332111213122112311311123112111331121113122112132113121113222112311311221112131221123113112221121113311211131122211211131221131211132221121321132132212321121113121112133221123113112221131112212211131221121321131211132221123113112221131112311332211211133112111311222112111312211311123113322112111312211312111322212321121113121112133221121321132132211331121321132213211231132132211211131221232112111312212221121123222112311311222113111231133211121321321122111312211312111322211213211321322123211211131211121332211231131122211311123113321112131221123113111231121123222112111331121113112221121113122113111231133221121113122113121113221112131221123113111231121123222112111312211312111322212321121113121112131112132112311321322112111312212321121113122122211211232221121321132132211331121321231231121113112221121321132132211322132113213221123113112221133112132123222112111312211312112213211231132132211211131221131211322113321132211221121332211231131122211311123113321112131221123113111231121113311211131221121321131211132221123113112211121312211231131122211211133112111311222112111312211312111322211213211321223112111311222112132113213221133122211311221122111312211312111322212321121113121112131112132112311321322112111312212321121113122122211211232221121321132132211331121321231231121113112221121321132132211322132113213221123113112221133112132123222112111312211312112213211231132132211211131221131211322113321132211221121332211213211321322113311213212312311211131122211213211331121321123123211231131122211211131221131112311332211213211321223112111311222112132113213221123123211231132132211231131122211311123113322112111312211312111322111213122112311311123112112322211213211321322113312211223113112221121113122113111231133221121321132132211331222113321112131122211332113221122112133221123113112221131112311332111213122112311311123112111331121113122112132113121113222112311311221112131221123113112221121113311211131122211211131221131211132221121321132132212321121113121112133221123113112221131112311332111213122112311311123112112322211322311311222113111231133211121312211231131112311211232221121113122113121113222123211211131221132211131221121321131211132221123113112211121312211231131122113221122112133221121321132132211331121321231231121113121113122122311311222113111231133221121113122113121113221112131221123113111231121123222112132113213221133112132123123112111312211322311211133112111312211213211311123113223112111321322123122113222122211211232221121113122113121113222123211211131211121311121321123113213221121113122123211211131221121311121312211213211321322112311311222113311213212322211211131221131211221321123113213221121113122113121113222112131112131221121321131211132221121321132132211331121321232221123113112221131112311322311211131122211213211331121321122112133221121113122113121113222123112221221321132132211231131122211331121321232221121113122113121113222123211211131211121332211213111213122112132113121113222112132113213221232112111312111213322112132113213221133112132123123112111311222112132113311213211221121332211231131122211311123113321112131221123113112221132231131122211211131221131112311332211213211321223112111311222112132113212221132221222112112322211211131221131211132221232112111312111213111213211231131112311311221122132113213221133112132123222112311311222113111231132231121113112221121321133112132112211213322112111312211312111322212321121113121112131112132112311321322112111312212321121113122122211211232221121311121312211213211312111322211213211321322123211211131211121332211213211321322113311213211322132112311321322112111312212321121113122122211211232221121321132132211331121321231231121113112221121321133112132112312321123113112221121113122113111231133221121321132122311211131122211213211321222113222122211211232221123113112221131112311332111213122112311311123112111331121113122112132113121113222112311311221112131221123113112221121113311211131122211211131221131211132221121321132132212321121113121112133221123113112221131112311332111213213211221113122113121113222112132113213221232112111312111213322112132113213221133112132123123112111312211322311211133112111312212221121123222112132113213221133112132123222113223113112221131112311332111213122112311311123112112322211211131221131211132221232112111312111213111213211231132132211211131221131211221321123113213221123113112221131112211322212322211231131122211322111312211312111322211213211321322113311213211331121113122122211211132213211231131122212322211331222113112211"
            };
            return arr[n];
        }
    }

    public static void main(String[] args) {
        System.out.println(Solution39.combinationSum(new int[]{2, 3, 7}, 7));
    }

    // https://leetcode.cn/problems/combination-sum
    static class Solution39 {
        public static List<List<Integer>> combinationSum(int[] candidates, int target) {
            List<List<Integer>> ans = new ArrayList<>();
            List<Integer> combine = new ArrayList<>();
            dfs(ans, candidates, combine, target, 0);
            return ans;
        }

        private static void dfs(List<List<Integer>> ans, int[] candidates, List<Integer> combine, int target, int index) {
            if (index == candidates.length) {
                return;
            }
            if (target == 0) {
                ans.add(new ArrayList<>(combine));
                return;
            }
            dfs(ans, candidates, combine, target, index + 1);
            if (target - candidates[index] >= 0) {
                combine.add(candidates[index]);
                dfs(ans, candidates, combine, target - candidates[index], index);
                combine.remove(combine.size() - 1);
            }
        }

        void backtrack(List<Integer> state, int target, int[] choices, int start, List<List<Integer>> res) {
            // 子集和等于 target 时，记录解
            if (target == 0) {
                res.add(new ArrayList<>(state));
                return;
            }
            // 遍历所有选择
            // 剪枝二：从 start 开始遍历，避免生成重复子集
            // 遍历了左边的元素之后，看是否能够得到解，下一次遍历下一个元素之后，就不能再去遍历左边的元素的，因为可能会得到重复解答
            // 例子 3 4
            // i = 0第一次选择3 第二次选择4 这是一个解答
            // i = 1第一次选择4 第二次不能在去i之前的位置进行选择了，因为选择了就会出现重复子集：[3,4.....]和[4,3....]是重复的
            for (int i = start; i < choices.length; i++) {
                // 剪枝一：若子集和超过 target ，则直接结束循环
                // 这是因为数组已排序，后边元素更大，子集和一定超过 target
                if (target - choices[i] < 0) {
                    break;
                }
                // 尝试：做出选择，更新 target, start
                state.add(choices[i]);
                // 进行下一轮选择
                backtrack(state, target - choices[i], choices, i, res);
                // 回退：撤销选择，恢复到之前的状态
                state.remove(state.size() - 1);
            }
        }

        public List<List<Integer>> combinationSum2(int[] candidates, int target) {
            List<Integer> state = new ArrayList<>(); // 状态（子集）
            Arrays.sort(candidates); // 对 candidates 进行排序
            int start = 0; // 遍历起始点
            List<List<Integer>> res = new ArrayList<>(); // 结果列表（子集列表）
            backtrack(state, target, candidates, start, res);
            return res;
        }
    }

    // https://leetcode.cn/problems/combination-sum-ii
    static class Solution40 {
        public List<List<Integer>> combinationSum2(int[] candidates, int target) {
            List<Integer> state = new ArrayList<>(); // 状态（子集）
            Arrays.sort(candidates); // 对 candidates 进行排序
            int start = 0; // 遍历起始点
            List<List<Integer>> res = new ArrayList<>(); // 结果列表（子集列表）
            backtrack(state, target, candidates, start, res);
            return res;
        }

        private void backtrack(List<Integer> state, int target, int[] candidates, int start, List<List<Integer>> res) {
            // 子集和等于 target 时，记录解
            if (target == 0) {
                res.add(new ArrayList<>(state));
                return;
            }
            // 遍历所有选择
            // 剪枝二：从 start 开始遍历，避免生成重复子集 和前一题的思想是一样的  可以看前一题的解答
            // 剪枝三：从 start 开始遍历，避免重复选择同一元素
            for (int i = start; i < candidates.length; i++) {
                // 剪枝一：若子集和超过 target ，则直接结束循环
                // 这是因为数组已排序，后边元素更大，子集和一定超过 target
                if (target - candidates[i] < 0) {
                    break;
                }
                // 剪枝四：如果该元素与左边元素相等，说明该搜索分支重复，直接跳过 跳过重复元素(数组中可能出现同一元素)
                // 例如 2 2^
                // 第一次选择了2 下一次就不能选择2^了，否则会出现一样的分支了
                if (i > start && candidates[i] == candidates[i - 1]) {
                    continue;
                }
                // 尝试：做出选择，更新 target, start
                state.add(candidates[i]);
                // 进行下一轮选择
                backtrack(state, target - candidates[i], candidates, i + 1, res);
                // 回退：撤销选择，恢复到之前的状态
                state.remove(state.size() - 1);
            }
        }
    }

    // https://leetcode.cn/problems/first-missing-positive
    static class Solution41 {
        public int firstMissingPositive(int[] nums) {
            // 思路：要找到最小的没有出现的正数
            // 这个数字一定是在1~n + 1范围的
            // 因此只需要把数组中在这个范围的数字调整到正确的位置
            // 例如 0 1 2
            // 调整后为：1 2 0 没有出现的数字是3 因为1就应该出现在下标为0的位置
            // 通用公式：数组中的一个数：nums[i]应该出现在num[i] - 1的位置
            int n = nums.length;
            for (int i = 0; i < n; i++) {
                while (nums[i] > 0 && nums[i] <= n && nums[nums[i] - 1] != nums[i]) {
                    int temp = nums[nums[i] - 1];
                    nums[nums[i] - 1] = nums[i];
                    nums[i] = temp;
                }
            }
            for (int i = 0; i < n; i++) {
                if (nums[i] != i + 1) {
                    return i + 1;
                }
            }
            return n + 1;
        }
    }

    // https://leetcode.cn/problems/trapping-rain-water
    static class Solution {
        public int trap(int[] height) {
            int ans = 0;
            int left = 0;
            int right = height.length - 1;
            int leftMax = 0;
            int rightMax = 0;
            while (left < right) {
                leftMax = Math.max(leftMax, height[left]);
                rightMax = Math.max(rightMax, height[right]);
                if (leftMax < rightMax) {
                    ans += leftMax - height[left];
                    left++;
                } else {
                    ans += rightMax - height[right];
                    right--;
                }
            }
            return ans;
        }
    }

    // https://leetcode.cn/problems/add-strings
    static class Solution415 {
        public String addStrings(String num1, String num2) {
            StringBuilder sb = new StringBuilder();
            int index1 = num1.length() - 1;
            int index2 = num2.length() - 1;
            int sum = 0;
            int carry = 0;
            while (index1 >= 0 || index2 >= 0) {
                int n1 = index1 >= 0 ? num1.charAt(index1) - '0' : 0;
                int n2 = index2 >= 0 ? num2.charAt(index2) - '0' : 0;
                sum = n1 + n2 + carry;
                carry = sum / 10;
                sb.append(sum % 10);
                index1--;
                index2--;
            }
            if (carry == 1) {
                sb.append(1);
            }
            return sb.reverse().toString();
        }
    }

    // https://leetcode.cn/problems/multiply-strings
    static class Solution43 {
        public String multiply(String num1, String num2) {
            if (num1.equals("0") || num2.equals("0")) {
                return "0";
            }
            String ans = "0";
            // num2逐个位置与num1整体相乘
            for (int i = num2.length() - 1; i >= 0; i--) {
                int carry = 0;
                StringBuilder sb = new StringBuilder();
                // 补0
                for (int j = 0; j < num2.length() - 1 - i; j++) {
                    sb.append(0);
                }
                int n2 = num2.charAt(i) - '0';
                // num2 的第 i 位数字 n2 与 num1 相乘
                for (int j = num1.length() - 1; j >= 0 || carry != 0; j--) {
                    int n1 = j < 0 ? 0 : num1.charAt(j);
                    int product = (n1 * n2 + carry) % 10;
                    sb.append(product);
                    carry = (n1 * n2 + carry) / 10;
                }
                ans = addStrings(ans, sb.reverse().toString());
            }
            return ans;
        }

        public String addStrings(String num1, String num2) {
            StringBuilder sb = new StringBuilder();
            int index1 = num1.length() - 1;
            int index2 = num2.length() - 1;
            int sum = 0;
            int carry = 0;
            while (index1 >= 0 || index2 >= 0) {
                int n1 = index1 >= 0 ? num1.charAt(index1) - '0' : 0;
                int n2 = index2 >= 0 ? num2.charAt(index2) - '0' : 0;
                sum = n1 + n2 + carry;
                carry = sum / 10;
                sb.append(sum % 10);
                index1--;
                index2--;
            }
            if (carry == 1) {
                sb.append(1);
            }
            return sb.reverse().toString();
        }
    }

    // https://leetcode.cn/problems/wildcard-matching/
    static class Solution44 {
        public boolean isMatch(String s, String p) {
            int m = s.length();
            int n = p.length();
            // s的前i个字符和p的前i个字符的匹配情况
            boolean[][] dp = new boolean[m + 1][n + 1];
            dp[0][0] = true;
            for (int i = 1; i <= m; i++) {
                if (p.charAt(i - 1) == '*') {
                    dp[0][i] = true;
                } else {
                    break;
                }
            }
            for (int i = 1; i <= m; i++) {
                for (int j = 1; j <= n; j++) {
                    if (p.charAt(j - 1) == '?' || p.charAt(j - 1) == s.charAt(i - 1)) {
                        dp[i][j] = dp[i - 1][j - 1];
                    } else {
                        // *号      不使用           使用
                        dp[i][j] = dp[i][j - 1] || dp[i - 1][j];
                    }
                }
            }
            return dp[m][n];
        }
    }

    // https://leetcode.cn/problems/jump-game-ii
    static class Solution45 {
        public int jump(int[] nums) {
            if (nums == null || nums.length == 0) {
                return 0;
            }
            int step = 0;
            // 代表step步数以内，最远能到哪里
            int cur = 0;
            // 如果多跳一步能到哪里
            int next = 0;
            for (int i = 0; i < nums.length; i++) {
                if (cur < i) {
                    step++;
                    cur = next;
                }
                next = Math.max(next, i + nums[i]);
            }
            return step;
        }
    }

    // https://leetcode.cn/problems/permutations
    static class Solution46 {
        List<Integer> nums;
        List<List<Integer>> res;

        public List<List<Integer>> permute(int[] nums) {
            this.res = new ArrayList<>();
            this.nums = new ArrayList<>();
            for (int num : nums) {
                this.nums.add(num);
            }
            dfs(0);
            return res;
        }

        private void swap(int a, int b) {
            int tmp = nums.get(a);
            nums.set(a, nums.get(b));
            nums.set(b, tmp);
        }

        private void dfs(int index) {
            if (index == nums.size() - 1) {
                res.add(new ArrayList<>(nums));
                return;
            }
            for (int i = index; i < nums.size(); i++) {
                swap(i, index);
                dfs(index + 1);
                swap(i, index);
            }
        }
    }

    // https://leetcode.cn/problems/permutations-ii
    static class Solution47 {
        List<Integer> nums;
        List<List<Integer>> res;

        public List<List<Integer>> permuteUnique(int[] nums) {
            this.res = new ArrayList<>();
            this.nums = new ArrayList<>();
            for (int num : nums) {
                this.nums.add(num);
            }
            dfs(0);
            return res;
        }

        private void swap(int a, int b) {
            int tmp = nums.get(a);
            nums.set(a, nums.get(b));
            nums.set(b, tmp);
        }

        private void dfs(int index) {
            if (index == nums.size() - 1) {
                res.add(new ArrayList<>(nums));
                return;
            }
            HashSet<Integer> set = new HashSet<>();
            for (int i = index; i < nums.size(); i++) {
                if (set.contains(nums.get(i)))
                    continue;            // 重复，因此剪枝
                set.add(nums.get(i));
                swap(i, index);
                dfs(index + 1);
                swap(i, index);
            }
        }
    }

    // https://leetcode.cn/problems/rotate-image
    static class Solution48 {
        public void rotate(int[][] matrix) {
            int n = matrix.length;
            for (int i = 0; i < n / 2; i++) {
                for (int j = 0; j < (n + 1) / 2; j++) {
                    // 暂存左上角的值
                    int tmp = matrix[i][j];
                    // 左下角换到左上角
                    matrix[i][j] = matrix[n - 1 - j][i];
                    // 右下角换到左下角
                    matrix[n - 1 - j][i] = matrix[n - 1 - i][n - 1 - j];
                    // 右上角换到右下角
                    matrix[n - 1 - i][n - 1 - j] = matrix[j][n - 1 - i];
                    // 左上角到右上角
                    matrix[j][n - 1 - i] = tmp;
                }
            }
        }
    }

    // https://leetcode.cn/problems/group-anagrams
    static class Solution49 {
        public List<List<String>> groupAnagrams(String[] strs) {
            Map<String, List<String>> map = new HashMap<>();
            for (String str : strs) {
                int[] counts = new int[26];
                for (int i = 0; i < str.length(); i++) {
                    counts[str.charAt(i) - 'a']++;
                }
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < 26; i++) {
                    if (counts[i] != 0) {
                        sb.append((char) ('a' + i));
                        sb.append(counts[i]);
                    }
                }
                String key = sb.toString();
                List<String> strings = map.getOrDefault(key, new ArrayList<>());
                strings.add(str);
                map.put(key, strings);
            }
            return new ArrayList<>(map.values());
        }
    }

    // https://leetcode.cn/problems/powx-n
    static class Solution50 {
        public static double myPow(double x, int n) {
            if (n == 0) {
                return 1D;
            }
            // n范围是整形的最大最小，我们统一求一个数的正次幂，如果n<0，再用1除以求出来的结果就可
            int pow = Math.abs(n == Integer.MIN_VALUE ? n + 1 : n);
            double t = x;
            double ans = 1D;
            while (pow != 0) {
                if ((pow & 1) != 0) {
                    ans *= t;
                }
                pow >>= 1;
                t = t * t;
            }
            if (n == Integer.MIN_VALUE) {
                ans *= x;
            }
            return n < 0 ? (1D / ans) : ans;
        }
    }

    // 整数对的个数
    // 给定一个数组，求出数组中两数字和为sum的整数对的个数
    static class Test {
        static public long countPairs(ArrayList<Integer> A, int n, int sum) {
            Integer[] array = new Integer[n];
            A.toArray(array);
            Arrays.sort(array);
            int left = 0;
            int right = n - 1;
            long counts = 0;
            while (left < right) {
                if (array[left] + array[right] == sum) {
                    // 出现这种情况 22222222222 sum=4
                    if (array[left].equals(array[right])) {
                        long x = right - left + 1;
                        counts += x * (x - 1) / 2;
                        break;
                    } else {
                        // 这种情况 1112345555 sum=6
                        int k1 = left + 1;
                        int k2 = right - 1;
                        while (k1 <= right && array[k1].equals(array[left])) {
                            k1++;
                        }
                        while (k2 >= left && array[k2].equals(array[right])) {
                            k2--;
                        }
                        counts += (long) (k1 - left) * (right - k2);
                        left = k1;
                        right = k2;
                    }
                } else if (array[left] + array[right] > sum) {
                    right--;
                } else {
                    left++;
                }
            }
            return counts;
        }
    }

    // https://leetcode.cn/problems/n-queens

    // https://leetcode.cn/problems/n-queens-ii

    // https://leetcode.cn/problems/maximum-subarray
    static class Solution53 {
        static public int maxSubArray(int[] nums) {
            if (nums.length < 2) {
                return nums[0];
            }
            int max = nums[0];
            int ans = nums[0];
            for (int i = 1; i < nums.length; i++) {
                max = Math.max(nums[i], (max + nums[i]));
                ans = Math.max(ans, max);
            }
            return ans;
        }
    }

    // https://leetcode.cn/problems/spiral-matrix
    static class Solution54 {
        public List<Integer> spiralOrder(int[][] matrix) {
            if (matrix.length == 0)
                return new ArrayList<>();
            int l = 0;// 左边界
            int r = matrix[0].length - 1;// 右边界
            int t = 0;// 上边界
            int b = matrix.length - 1;// 下边界
            int x = 0;
            Integer[] res = new Integer[(r + 1) * (b + 1)];
            while (true) {
                for (int i = l; i <= r; i++)
                    res[x++] = matrix[t][i]; // left to right
                if (++t > b)
                    break;
                for (int i = t; i <= b; i++)
                    res[x++] = matrix[i][r]; // top to bottom
                if (l > --r)
                    break;
                for (int i = r; i >= l; i--)
                    res[x++] = matrix[b][i]; // right to left
                if (t > --b)
                    break;
                for (int i = b; i >= t; i--)
                    res[x++] = matrix[i][l]; // bottom to top
                if (++l > r)
                    break;
            }
            return Arrays.asList(res);
        }
    }

    // https://leetcode.cn/problems/jump-game
    static class Solution55 {
        public boolean canJump(int[] nums) {
            int step = 0;
            for (int i = 0; i < nums.length; i++) {
                if (i > step) {
                    return false;
                }
                step = Math.max(step, i + nums[i]);
            }
            return true;
        }
    }

    // https://leetcode.cn/problems/merge-intervals
    static class Solution56 {
        public int[][] merge(int[][] intervals) {
            if (intervals.length == 0) {
                return new int[0][0];
            }
            Arrays.sort(intervals, (a, b) -> a[0] - b[0]);
            int begin = intervals[0][0];
            int end = intervals[0][1];
            int size = 0;
            for (int i = 1; i < intervals.length; i++) {
                if (intervals[i][0] > end) {
                    intervals[size][0] = begin;
                    intervals[size++][1] = end;
                    begin = intervals[i][0];
                    end = intervals[i][1];
                } else {
                    // 如果小于等于代表可以合并
                    end = Math.max(end, intervals[i][1]);
                }
            }
            intervals[size][0] = begin;
            intervals[size++][1] = end;
            return Arrays.copyOf(intervals, size);
        }
    }

    // https://leetcode.cn/problems/insert-interval
    static class Solution57 {

        public int[][] insert(int[][] intervals, int[] newInterval) {
            int left = newInterval[0];
            int right = newInterval[1];
            // 用于判断newInterval是否已经被插入正确位置且合并了
            boolean placed = false;
            List<int[]> ansList = new ArrayList<int[]>();
            for (int[] interval : intervals) {
                if (interval[0] > right) {
                    // 在插入区间的右侧且无交集
                    if (!placed) {
                        ansList.add(new int[]{left, right});
                        placed = true;
                    }
                    ansList.add(interval);
                } else if (interval[1] < left) {
                    // 在插入区间的左侧且无交集
                    ansList.add(interval);
                } else {
                    // 与插入区间有交集，计算它们的并集
                    left = Math.min(left, interval[0]);
                    right = Math.max(right, interval[1]);
                }
            }
            if (!placed) {
                ansList.add(new int[]{left, right});
            }
            int[][] ans = new int[ansList.size()][2];
            for (int i = 0; i < ansList.size(); ++i) {
                ans[i] = ansList.get(i);
            }
            return ans;
        }
    }

    // https://leetcode.cn/problems/length-of-last-word
    static class Solution58 {
        public int lengthOfLastWord(String s) {
            // 反向遍历
            int index = s.length() - 1;
            while (index >= 0 && s.charAt(index) == ' ') {
                index--;
            }
            int lastWordLength = 0;
            while (index >= 0 && s.charAt(index) != ' ') {
                lastWordLength++;
                index--;
            }
            return lastWordLength;
        }
    }

    // https://leetcode.cn/problems/spiral-matrix-ii
    static class Solution59 {
        public int[][] generateMatrix(int n) {
            if (n == 1) {
                return new int[][]{{1}};
            }
            int[][] matrix = new int[n][n];
            int l = 0;// 左边界
            int r = n - 1;// 右边界
            int t = 0;// 上边界
            int b = n - 1;// 下边界
            int num = 1;
            int end = n * n;
            while (num <= end) {
                // 从左往右
                for (int i = l; i <= r; i++) {
                    matrix[t][i] = num;
                    num++;
                }
                t++;
                // 从上往下
                for (int i = t; i <= b; i++) {
                    matrix[i][r] = num;
                    num++;
                }
                r--;
                // 从右往左
                for (int i = r; i >= l; i--) {
                    matrix[b][i] = num;
                    num++;
                }
                b--;
                // 从上往下
                for (int i = b; i >= t; i--) {
                    matrix[i][l] = num;
                    num++;
                }
                l++;
            }
            return matrix;
        }
    }

    // https://leetcode.cn/problems/permutation-sequence
    static class Solution61 {

        static public class ListNode {
            int val;
            ListNode next;

            ListNode() {
            }

            ListNode(int val) {
                this.val = val;
            }

            ListNode(int val, ListNode next) {
                this.val = val;
                this.next = next;
            }
        }

        public ListNode rotateRight(ListNode head, int k) {
            // 特殊情况
            if (k == 0 || head == null || head.next == null) {
                return head;
            }
            // 统计长度
            int n = 1;
            ListNode cur = head;
            while (cur.next != null) {
                cur = cur.next;
                n++;
            }
            // 移动的次数为链表长度，相当于没有移动
            if (k % n == 0) {
                return head;
            }
            int need = n - k % n;
            cur.next = head;
            while (need > 0) {
                cur = cur.next;
                need--;
            }
            ListNode ans = cur.next;
            cur.next = null;
            return ans;
        }
    }

    // https://leetcode.cn/problems/permutation-sequence
    static class Solution60 {
        public String getPermutation(int n, int k) {
            int[] num = new int[n];
            for (int i = 0; i < n; i++) {
                num[i] = i + 1;
            }
            for (int i = 0; i < k - 1; i++) {
                nextPermutation(num);
            }
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < n; i++) {
                sb.append(num[i]);
            }
            return sb.toString();
        }

        private void nextPermutation(int[] num) {
            int i = num.length - 2;
            while (i >= 0 && num[i] >= num[i + 1]) {
                i--;
            }
            if (i >= 0) {
                int j = num.length - 1;
                while (j >= 0 && num[i] >= num[j]) {
                    j--;
                }
                swap(num, i, j);
            }
            reverse(num, i + 1);
        }

        private void reverse(int[] num, int begin) {
            int end = num.length - 1;
            while (begin < end) {
                swap(num, begin, end);
                begin++;
                end--;
            }
        }

        private void swap(int[] num, int begin, int end) {
            int temp = num[begin];
            num[begin] = num[end];
            num[end] = temp;
        }
    }

    // https://leetcode.cn/problems/unique-paths
    static class Solution62 {
        public int uniquePaths(int m, int n) {
            int[][] dp = new int[m][n];
            for (int i = n - 1; i >= 0; i--) {
                dp[m - 1][i] = 1;
            }
            for (int i = 0; i < m; i++) {
                dp[i][n - 1] = 1;
            }
            for (int i = m - 2; i >= 0; i--) {
                for (int j = n - 2; j >= 0; j--) {
                    dp[i][j] = dp[i + 1][j] + dp[i][j + 1];
                }
            }
            return dp[0][0];
        }
    }

    // https://leetcode.cn/problems/unique-paths-ii
    static class Solution63 {
        public int uniquePathsWithObstacles(int[][] obstacleGrid) {
            int m = obstacleGrid.length;
            int n = obstacleGrid[0].length;
            if (n == 0 || obstacleGrid[0][0] == 1) return 0;
            int[][] dp = new int[m][n];
            dp[0][0] = 1;
            //i = 0; dp[i][j] = dp[0][j - 1]
            for (int i = 1; i < n; i++) {
                if (obstacleGrid[0][i] != 1) {
                    dp[0][i] = dp[0][i - 1];
                    continue;
                }
                break;
            }
            //j = 0; dp[i][j] = dp[i - 1][j]
            for (int i = 1; i < m; i++) {
                if (obstacleGrid[i][0] != 1) {
                    dp[i][0] = dp[i - 1][0];
                    continue;
                }
                break;
            }
            //dp[i][j] = dp[i - 1][j] + dp[i][j - 1]
            for (int i = 1; i < m; i++) {
                for (int j = 1; j < n; j++) {
                    if (obstacleGrid[i][j] == 1) {
                        continue;
                    }
                    dp[i][j] = dp[i - 1][j] + dp[i][j - 1];
                }
            }
            return dp[m - 1][n - 1];
        }
    }

    // https://leetcode.cn/problems/minimum-path-sum
    static class Solution64 {
        public int minPathSum(int[][] grid) {
            if (grid == null || grid.length == 0 || grid[0].length == 0) {
                return 0;
            }
            int m = grid.length, n = grid[0].length;
            int[][] dp = new int[m][n];
            dp[0][0] = grid[0][0];
            for (int i = 1; i < m; i++) {
                dp[i][0] = dp[i - 1][0] + grid[i][0];
            }
            for (int j = 1; j < n; j++) {
                dp[0][j] = dp[0][j - 1] + grid[0][j];
            }
            for (int i = 1; i < m; i++) {
                for (int j = 1; j < n; j++) {
                    dp[i][j] = Math.min(dp[i - 1][j], dp[i][j - 1]) + grid[i][j];
                }
            }
            return dp[m - 1][n - 1];
        }
    }

    // https://leetcode.cn/problems/valid-number
    static class Solution65 {
        public boolean isNumber(String s) {
            Map[] states = {
                    new HashMap<Character, Integer>() {{
                        put(' ', 0);
                        put('s', 1);
                        put('d', 2);
                        put('.', 4);
                    }}, // 0.
                    new HashMap<Character, Integer>() {{
                        put('d', 2);
                        put('.', 4);
                    }},                           // 1.
                    new HashMap<Character, Integer>() {{
                        put('d', 2);
                        put('.', 3);
                        put('e', 5);
                        put(' ', 8);
                    }}, // 2.
                    new HashMap<Character, Integer>() {{
                        put('d', 3);
                        put('e', 5);
                        put(' ', 8);
                    }},              // 3.
                    new HashMap<Character, Integer>() {{
                        put('d', 3);
                    }},                                        // 4.
                    new HashMap<Character, Integer>() {{
                        put('s', 6);
                        put('d', 7);
                    }},                           // 5.
                    new HashMap<Character, Integer>() {{
                        put('d', 7);
                    }},                                        // 6.
                    new HashMap<Character, Integer>() {{
                        put('d', 7);
                        put(' ', 8);
                    }},                           // 7.
                    new HashMap<Character, Integer>() {{
                        put(' ', 8);
                    }}                                         // 8.
            };
            int p = 0;
            char t;
            for (char c : s.toCharArray()) {
                if (c >= '0' && c <= '9') t = 'd';
                else if (c == '+' || c == '-') t = 's';
                else if (c == 'e' || c == 'E') t = 'e';
                else if (c == '.' || c == ' ') t = c;
                else t = '?';
                if (!states[p].containsKey(t)) return false;
                p = (int) states[p].get(t);
            }
            return p == 2 || p == 3 || p == 7 || p == 8;
        }
    }

    // https://leetcode.cn/problems/plus-one
    static class Solution66 {
        public int[] plusOne(int[] digits) {
            for (int i = digits.length - 1; i >= 0; i--) {
                digits[i]++;
                digits[i] = digits[i] % 10;
                if (digits[i] != 0) return digits;
            }
            digits = new int[digits.length + 1];
            digits[0] = 1;
            return digits;
        }
    }
}


