public class Day4 {

    // 剑指 Offer 17. 打印从1到最大的n位数
    // https://leetcode.cn/problems/da-yin-cong-1dao-zui-da-de-nwei-shu-lcof/
    public int[] printNumbers(int n) {
        int endNumber = (int) Math.pow(10,n);
        int[] res = new int[endNumber - 1];
        for (int i = 1; i < endNumber; i++) {
            res[i - 1] = i;
        }
        return res;
    }

    // 剑指 Offer 18. 删除链表的节点
    // https://leetcode.cn/problems/shan-chu-lian-biao-de-jie-dian-lcof/
    public class ListNode {
    int val;
    ListNode next;
    ListNode(int x) {
        val = x; }
    }
    public ListNode deleteNode(ListNode head, int val) {
        if(head == null){
            return null;
        }
        if(head.val == val){
            return head.next;
        }
        ListNode pre = head;
        ListNode temp = head.next; //可能是删除的节点
        while(temp != null){
            if(temp.val == val){
                pre.next = temp.next;
                return head;
            }
            pre = temp;
            temp = temp.next;
        }
        return head;
    }

    // 剑指 Offer 19. 正则表达式匹配
    // https://leetcode.cn/problems/zheng-ze-biao-da-shi-pi-pei-lcof/
    public boolean isMatch(String s, String p) {
        if(s == null || p ==null){
            return true;
        }
        int n = s.length();
        int m = p.length();
        boolean[][] dp = new boolean[n + 1][m + 1];
        dp[0][0] = true;
        for (int i = 2; i <= m; i++) {
            if (p.charAt(i - 1) == '*') {
                dp[0][i] = dp[0][ i -2];
            }
        }
        for (int i = 1; i <=n ; i++) {
            for (int j = 1; j <=m ; j++) {
                // 不为*号
                if(p.charAt(j - 1) != '*'){
                    // 当p的第j个位置的字符为.号或者与s的第i个字符相等，只需要看dp[i- 1][j - 1]的匹配情况
                    if(p.charAt(j - 1) == '.' || p.charAt(j - 1) == s.charAt(i - 1)){
                        // 没有满足条件的不用设置，默认为false
                        dp[i][j] = dp[i - 1][j - 1];
                    }
                }
                // 为*号
                else{
                    // *号前一个字符和当前s中第i个字符不匹配
                    if(p.charAt(j - 2) != s.charAt(i - 1) && p.charAt(j - 2) != '.'){
                        // *取0次
                        dp[i][j] = dp[i][j - 2];
                    }else {
                        // *取0次或1次或n次
                        dp[i][j] = dp[i][j - 2] || dp[i][j - 1] || dp[i - 1][j];
                    }
                }
            }
        }
        return dp[n][m];
    }

    // 剑指 Offer 20. 表示数值的字符串
    // https://leetcode.cn/problems/biao-shi-shu-zhi-de-zi-fu-chuan-lcof/
    public boolean isNumber(String s) {
        if(s == null){
            return false;
        }
        // 去除首尾空格
        char[] res = s.trim().toCharArray();
        // 全部都是空格
        if(res.length < 1){
            return false;
        }
        boolean is_num = false;
        boolean is_dot = false;
        boolean is_e_or_E = false;
        for (int i = 0; i < res.length; i++) {
            // 数字字符
            if(res[i] >= '0' && res[i] <= '9'){
                is_num = true;
            }
            // .号
            else if(res[i] == '.'){
                // .号前面不能是.号或者出现E/e
                // 1.2.3   5E.2
                if(is_dot || is_e_or_E){
                    return false;
                }
                is_dot = true;
            }
            // E号或者e号
            else if(res[i] == 'E' || res[i] == 'e'){
                // e或E前面不能是e/E或者前面不能不是数字
                // ee/EE e5/E5
                if(!is_num || is_e_or_E){
                    return false;
                }
                is_e_or_E = true;
                // 出现了e/E就还要判断e/E的后面是否至少跟了一个数字，因此要重置
                // 排除0E/0e这种情况
                is_num = false;
            }
            // +/1号
            else if(res[i] == '+' || res[i] == '-'){
                // +/-号只能在第一位和不能在E或e前面
                // +-5     +E/-E
                if(i != 0 && res[i - 1] != 'E' && res[i - 1] != 'e'){
                    return  false;
                }
            }
            // 其他字符
            else {
                // 1a3
                return false;
            }
        }
        return is_num;
    }
}
