import java.util.*;

public class Day1 {
    // 剑指 Offer 03. 数组中重复的数字
    // https://leetcode.cn/problems/shu-zu-zhong-zhong-fu-de-shu-zi-lcof/
    public int findRepeatNumber(int[] nums) {
        for(int i = 0;i < nums.length;i++){
            while(i != nums[i]){
                if(nums[i] == nums[nums[i]]){
                    return nums[i];
                }
                int tmp = nums[nums[i]];
                nums[nums[i]] = nums[i];
                nums[i] = tmp;
            }
        }
        return -1;
    }

    // 剑指 Offer 04. 二维数组中的查找
    // https://leetcode.cn/problems/er-wei-shu-zu-zhong-de-cha-zhao-lcof/
    public boolean findNumberIn2DArray(int[][] matrix, int target) {
        if(matrix == null || matrix.length <=0 || matrix[0].length <= 0){
            return false;
        }
        int cols = matrix[0].length;
        int rows = matrix.length;
        // 从左下角开始搜索
        int col = 0;
        int row = rows - 1;
        while (col < cols && row > -1) {
            if(matrix[row][col] < target){
                col++;
            }else if (matrix[row][col] > target){
                row--;
            }else {
                return true;
            }
        }
        return false;
    }

    // 剑指 Offer 05. 替换空格
    // https://leetcode.cn/problems/ti-huan-kong-ge-lcof/
    public String replaceSpace(String s) {
        // 记录空格个数
        int count = 0;
        for (int i = 0; i < s.length(); i++) {
            if(s.charAt(i) == ' '){
                count++;
            }
        }
        int totalLength = s.length() + count * 2;
        char[] res = new char[totalLength];
        int k = totalLength - 1;
        for (int i = s.length() - 1; i >= 0 ; i--) {
            if(s.charAt(i) == ' '){
                res[k--] = '0';
                res[k--] = '2';
                res[k--] = '%';
            }else{
                res[k--] = s.charAt(i);
            }
        }
        return new String(res);
    }

    // 剑指 Offer 06. 从尾到头打印链表
    // https://leetcode.cn/problems/cong-wei-dao-tou-da-yin-lian-biao-lcof/
    public static class ListNode {
      int val;
     ListNode next;
      ListNode(int x) { val = x; }
    }
    public int[] reversePrint1(ListNode head) {
        if(head == null){
            return new int[0];
        }
        int length = 0;
        ListNode temp = head;
        while(temp != null){
            length++;
            temp = temp.next;
        }
        temp = head;
        int[] array = new int[length];
        int index = length - 1;
        while(temp != null){
            array[index--] = temp.val;
            temp = temp.next;
        }
        return array;
    }

    ArrayList<Integer> tmp = new ArrayList<Integer>();
    public int[] reversePrint(ListNode head) {
        recur(head);
        int[] res = new int[tmp.size()];
        for(int i = 0; i < res.length; i++)
            res[i] = tmp.get(i);
        return res;
    }
    void recur(ListNode head) {
        if(head == null) return;
        recur(head.next);
        tmp.add(head.val);
    }

    // 剑指 Offer 07. 重建二叉树
    // https://leetcode.cn/problems/zhong-jian-er-cha-shu-lcof/
    public static class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;
      TreeNode(int x) { val = x; }
  }
    private Map<Integer, Integer> indexMap;
    public TreeNode buildTree(int[] preorder, int[] inorder) {
        if(preorder == null || inorder == null){
            return null;
        }
        indexMap = new HashMap<>();
        for (int i = 0; i < preorder.length; i++) {
            // 用前序遍历得到的头节点，在inorder中区分左子树和右子树
            indexMap.put(inorder[i],i);
        }
        TreeNode root = build(preorder,0,preorder.length - 1,inorder,0,inorder.length - 1);
        return root;
    }

    // 递归实现，根据头节点，划分左右子树，从上到下依次建立
    private TreeNode build(int[] preorder, int l1, int r1, int[] inorder, int l2, int r2) {
        if(l1 > r1 || l2 > r2){
            return null;
        }
        TreeNode root = new TreeNode(preorder[l1]);
        int newHeadIndex = indexMap.get(preorder[l1]);
        root.left = build(preorder,l1 + 1,l1 + newHeadIndex - l2,inorder,l2,newHeadIndex - 1);
        root.right = build(preorder,l1 + newHeadIndex - l2 + 1,r2,inorder,newHeadIndex + 1,r2);
        return root;
    }
}
