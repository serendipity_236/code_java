import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

public class Day10 {

    // 剑指 Offer 40. 最小的k个数
    // https://leetcode.cn/problems/zui-xiao-de-kge-shu-lcof/
    // 基于快速排序的代码
    public int[] getLeastNumbers(int[] arr, int k) {
        if(arr == null || arr.length == 0 || k == 0){
            return new int[0];
        }
        return quickFind(arr,0,arr.length - 1,k);
    }

    private int[] quickFind(int[] arr, int left, int right, int k) {
        int i = partition(arr,left,right);

        if(i + 1 == k){
            return Arrays.copyOf(arr,k);
        }

        if(i + 1 > k){
            return quickFind(arr,0,i - 1,k);
        }else {
            return quickFind(arr,i + 1,right,k);
        }
    }

    private int partition(int[] arr, int left, int right) {
        int pivot = arr[left];

        int i = left + 1;
        int j = right;

        while(i < j){
            while (i <= j && arr[i] <= pivot)
                i++;
            while (i <= j && arr[j] >= pivot)
                j++;
            if(i >= j){
                break;
            }
            int tmp = arr[i];
            arr[i] = arr[j];
            arr[j] = tmp;
        }

        arr[left] = arr[j];
        arr[j] = pivot;
        return j;
    }

    // 基于堆排序的代码
    public int[] getLeastNumbers2(int[] arr, int k) {
        int[] vec = new int[k];
        if (k == 0) { // 排除 0 的情况
            return vec;
        }
        PriorityQueue<Integer> queue = new PriorityQueue<Integer>(new Comparator<Integer>() {
            public int compare(Integer num1, Integer num2) {
                return num2 - num1;
            }
        });
        for (int i = 0; i < k; ++i) {
            queue.offer(arr[i]);
        }
        for (int i = k; i < arr.length; ++i) {
            if (queue.peek() > arr[i]) {
                queue.poll();
                queue.offer(arr[i]);
            }
        }
        for (int i = 0; i < k; ++i) {
            vec[i] = queue.poll();
        }
        return vec;
    }

    // 剑指 Offer 41. 数据流中的中位数
    // https://leetcode.cn/problems/shu-ju-liu-zhong-de-zhong-wei-shu-lcof/
    PriorityQueue<Integer> min;
    PriorityQueue<Integer> max;
    public Day10() {
        min = new PriorityQueue<>();
        max = new PriorityQueue<>((a,b) -> b-a);
    }

    public void addNum(int num) {
        if(min.size() == max.size()){
            min.add(num);
            max.add(min.poll());
        }else {
            max.add(num);
            min.add(max.poll());
        }
    }

    public double findMedian() {

        if(min.size() == max.size()){
            return (min.peek() + max.peek()) / 2.0;
        }else {
            return max.peek() * 1.0;
        }
    }

    // 剑指 Offer 42. 连续子数组的最大和
    // https://leetcode.cn/problems/lian-xu-zi-shu-zu-de-zui-da-he-lcof/
    // 没有优化的版本
    public int maxSubArray1(int[] nums) {
        int[] dp = new int[nums.length];
        dp[0] = nums[0];
        int max = nums[0];
        for (int i = 1; i < nums.length; i++) {
            dp[i] = Math.max(nums[i],dp[i - 1] + nums[1]);
            max = Math.max(max,dp[i]);
        }
        return max;
    }

    // 剑指 Offer 42. 连续子数组的最大和
    // https://leetcode.cn/problems/lian-xu-zi-shu-zu-de-zui-da-he-lcof/
    // 优化的版本
    public int maxSubArray(int[] nums) {
        int dp = nums[0];
        int max = nums[0];
        // 刷新dp之前，dp代表的就是前几个连续的最大和，即是dp[i- 1]，刷新之后就是dp[i]
        for (int i = 1; i < nums.length; i++) {
            dp = Math.max(nums[i],dp + nums[1]);
            max = Math.max(max,dp);
        }
        return max;
    }

    // 剑指 Offer 43. 1～n 整数中 1 出现的次数
    // https://leetcode.cn/problems/1nzheng-shu-zhong-1chu-xian-de-ci-shu-lcof/
    // 当cur == 1时，例如2314，求十位出现1的个数 high = 2314 / bit / 10，low =2314 % bit，cur = 2314 / bit % 10
    // 出现1的范围：0000~2314
    // 固定十位为1，只看高低位 000~234
    // 出现1的次数为：234 - 0 + 1 = 235 = high * bit + low + 1；
    // 当cur => 1时，例如2324，求十位出现1的个数 high = 2324 / bit / 10，low =2324 % bit，cur = 2324 / bit % 10
    // 出现1的范围：0000~2319
    // 固定十位为1，只看高低位 000~239
    // 出现1的次数为：239 - 0 + 1 = 240 = (high + 1) * bit；
    // 当cur < 1时，例如2304，求十位出现1的个数 high = 2304 / bit / 10，low =2304 % bit，cur = 2304 / bit % 10
    // 出现1的范围：0000~2219
    // 固定十位为1，只看高低位 000~229
    // 出现1的次数为：229 - 0 + 1 = 230 = high * bit；
    public int countDigitOne(int n) {
        long bit = 1;
        long sum = 0;
        while (bit <= n){
            long cur = n / bit % 10;
            long low = n % bit;
            long high = n / bit / 10;

            if(cur > 1){
                sum += (high + 1) *bit;
            }else if(cur == 1){
                sum += (high * bit) + low + 1;
            }else {
                sum += high *bit;
            }
            bit *= 10;
        }
        return (int) sum;
    }

    // 剑指 Offer 44. 数字序列中某一位的数字
    // https://leetcode.cn/problems/shu-zi-xu-lie-zhong-mou-yi-wei-de-shu-zi-lcof/
    public int findNthDigit(int n) {
        if(n == 0){
            return 0;
        }
        long bit = 1;// 代表当前是位数的起始数字
        int i = 1;// 代表当前范围数字的组成个数
        long count = 9;// 代表当前范围数字的总字符数量
        while(count < n){
            n = (int) (n - count);
            bit *= 10;
            i++;
            count = 9 * bit * i;
        }
        // 找到当前是哪个数字：从当前位数的起始数字开始加，看n - 1(排除0的下标位置)能整除几个i，
        int resNum = (int) (bit + (n - 1) / i);

        // 确定是当前数字的第几位  当前位数都是i个字符组成，看n - 1个字符能整除几个i，得到的余数 + 1就是当前位数
        int index = (n - 1) % i + 1;

        // 已知当前数字的位数，求当前数字的第index位数字
        return (int) (resNum  / Math.pow(10,i - index) % 10);
    }
}
