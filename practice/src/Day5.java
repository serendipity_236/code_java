public class Day5 {

    // 剑指 Offer 21. 调整数组顺序使奇数位于偶数前面
    // https://leetcode.cn/problems/diao-zheng-shu-zu-shun-xu-shi-qi-shu-wei-yu-ou-shu-qian-mian-lcof/
    public int[] exchange(int[] nums) {
        if(nums == null){
            return nums;
        }
        int left = 0;
        int right = nums.length - 1;
        while(left < right){
            while (left <right && nums[left] % 2 != 0){
                left++;
            }
            while(left < right && nums[right] % 2 != 1){
                right++;
            }
            int temp = nums[left];
            nums[left] = nums[right];
            nums[right] = temp;
        }
        return nums;
    }

    // 剑指 Offer 22. 链表中倒数第k个节点
    // https://leetcode.cn/problems/lian-biao-zhong-dao-shu-di-kge-jie-dian-lcof/
     public static class ListNode {
          int val;
          ListNode next;
          ListNode(int x) {
              val = x; }
    }
    public ListNode getKthFromEnd(ListNode head, int k) {
        if (head == null){
            return head;
        }
        ListNode low = head;
        ListNode fast = head;
        int count = 0;
        while(count != k){
            if(fast == null){
                return null;
            }
            fast = fast.next;
            count++;
        }
        while(fast!= null){
            fast = fast.next;
            low = low.next;
        }
        return low;
    }

    // 剑指 Offer 24. 反转链表
    // https://leetcode.cn/problems/fan-zhuan-lian-biao-lcof/description/
    // 递归法
    public ListNode reverseList(ListNode head) {
        if(head == null || head.next == null){
            return head;
        }

        ListNode temp = reverseList(head.next);
        head.next.next = head;
        head.next = null;

        return temp;
    }

    // 原地反转
    public ListNode reverseList2(ListNode head) {
        if(head == null || head.next == null){
            return head;
        }
        ListNode pre = null;
        ListNode cur = head;
        ListNode next = null;
        while(cur != null){
            next = cur.next;
            cur.next = pre;
            pre = cur;
            cur = next;
        }
        return pre;
    }

    // 剑指 Offer 25. 合并两个排序的链表
    // https://leetcode.cn/problems/he-bing-liang-ge-pai-xu-de-lian-biao-lcof/
    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        if(l1 == null){
            return l2;
        }
        if(l2 == null){
            return l1;
        }
        ListNode mergerHead = new ListNode(0);
        ListNode temp = mergerHead;
        while(l1 != null && l2 != null){
            if(l1.val <= l2.val){
                temp.next = l1;
                l1 = l1.next;
            }else{
                temp.next = l2;
                l2 = l2.next;
            }
            temp = temp.next;
        }
        if(l1 == null){
            temp.next = l2;
        }
        if(l2 == null){
            temp.next = l1;
        }
        return mergerHead.next;
    }


     public static class TreeNode {
          int val;
          TreeNode left;
          TreeNode right;
          TreeNode(int x) {
              val = x; }
    }
    // 剑指 Offer 26. 树的子结构
    // https://leetcode.cn/problems/shu-de-zi-jie-gou-lcof/
    public boolean isSubStructure(TreeNode A, TreeNode B) {
        if(A == null || B == null){
            return false;
        }
        // 判断B是否是以A的某个节点为头的子树
        if(isSubTree(A,B)){
            return true;
        }
        return isSubStructure(A.left, B) || isSubStructure(A.right, B);
    }

    private boolean isSubTree(TreeNode master, TreeNode sub) {
        if(sub == null){
            return true;
        }
        if(master == null){
            return false;
        }
        if(master.val != sub.val){
            return false;
        }
        return isSubTree(master.left,sub.left) && isSubTree(master.right,sub.right);
    }
}
