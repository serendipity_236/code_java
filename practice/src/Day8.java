import java.util.*;

public class Day8 {
    // 剑指 Offer 32 - I. 从上到下打印二叉树
    // https://leetcode.cn/problems/cong-shang-dao-xia-da-yin-er-cha-shu-lcof/
    public static class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode(int x) {
          val = x;
        }
        TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
        }
    }
    public int[] levelOrder1(TreeNode root) {
        if(root == null){
            return new int[0];
        }
        Queue<TreeNode> queue = new LinkedList<>();
        List<Integer> res = new ArrayList<>();
        queue.offer(root);
        while(!queue.isEmpty()){
            TreeNode cur = queue.poll();
            if(cur != null){
                res.add(cur.val);
                queue.add(cur.left);
                queue.add(cur.right);
            }
        }
        
        int[] result = new int[res.size()];
        for (int i = 0; i < res.size(); i++) {
            result[i] = res.get(i);
        }
        return result;
    }

    // 剑指 Offer 32 - II. 从上到下打印二叉树 II
    // https://leetcode.cn/problems/cong-shang-dao-xia-da-yin-er-cha-shu-ii-lcof/
    // 通过队列中的个数来判断是几层了 每一次都要取完队列中的所有元素
    public List<List<Integer>> levelOrder2(TreeNode root) {
        if(root == null){
            return new ArrayList<>();
        }
        Queue<TreeNode> queue = new LinkedList<>();
        List<List<Integer>> result = new ArrayList<>();
        queue.offer(root);
        while(!queue.isEmpty()){
            int size = queue.size();
            List<Integer> tmp = new ArrayList<>();
            for (int i = 0; i < size; i++) {
                TreeNode cur = queue.poll();
                tmp.add(cur.val);
                if(cur.left != null) {
                    queue.add(cur.left);
                }
                if(cur.right != null) {
                    queue.add(cur.right);
                }
            }
            result.add(tmp);
        }
        return result;
    }

    // 剑指 Offer 32 - III. 从上到下打印二叉树 III
    // https://leetcode.cn/problems/cong-shang-dao-xia-da-yin-er-cha-shu-iii-lcof/
    public List<List<Integer>> levelOrder(TreeNode root) {
        if(root == null){
            return new ArrayList<>();
        }
        Queue<TreeNode> queue = new LinkedList<>();
        List<List<Integer>> result = new ArrayList<>();
        queue.offer(root);
        int flag = 1;// 标志是奇数行还是偶数行
        while(!queue.isEmpty()){
            int size = queue.size();
            LinkedList<Integer> tmp = new LinkedList<>();
            for (int i = 0; i < size; i++) {
                TreeNode cur = queue.poll();
                if(flag % 2 == 1){
                    tmp.add(cur.val);
                }else {
                    tmp.addFirst(cur.val);
                }
                if(cur.left != null) {
                    queue.add(cur.left);
                }
                if(cur.right != null) {
                    queue.add(cur.right);
                }
            }
            result.add(tmp);
            flag++;
        }
        return result;
    }

    // 剑指 Offer 33. 二叉搜索树的后序遍历序列
    // https://leetcode.cn/problems/er-cha-sou-suo-shu-de-hou-xu-bian-li-xu-lie-lcof/
    // 递归方法
    public boolean verifyPostorder1(int[] postorder) {
        if(postorder == null){
            return true;
        }
        return verify2(postorder,0,postorder.length - 1);
    }

    private boolean verify2(int[] postorder, int i, int j) {
        // 只有最后一个节点了 ，可以直接作为根节点，直接返回true即可
        if(i >= j){
            return  true;
        }
        // 找到子树的根节点
        int root = postorder[j];

        // 找到第一个比根节点大的节点
        int firstBig = i;
        while(postorder[firstBig] < root){
            firstBig++;
        }
        for (int k = firstBig + 1; k < j; k++) {
            if(postorder[k] < root){
                return false;
            }
        }

        return verify2(postorder,i,firstBig - 1) && verify2(postorder,firstBig,j - 1);
    }

    public boolean verifyPostorder(int[] postorder) {
        if(postorder == null){
            return true;
        }
        Stack<Integer> stack = new Stack<>();
        int parent = Integer.MAX_VALUE;
        //注意for循环是倒叙遍历的
        for (int i = postorder.length - 1; i >= 0; i--) {
            int cur = postorder[i];
            //当如果前节点小于栈顶元素，说明栈顶元素和当前值构成了倒叙，
            //说明当前节点是前面某个节点的左子节点，我们要找到他的父节点
            while (!stack.isEmpty() && stack.peek() > cur)
                parent = stack.pop();
            //只要遇到了某一个左子节点，才会执行上面的代码，才会更
            //新parent的值，否则parent就是一个非常大的值，也就
            //是说如果一直没有遇到左子节点，那么右子节点可以非常大
            if (cur > parent)
                return false;
            //入栈
            stack.add(cur);
        }
        return true;
    }

    // 剑指 Offer 34. 二叉树中和为某一值的路径
    // https://leetcode.cn/problems/er-cha-shu-zhong-he-wei-mou-yi-zhi-de-lu-jing-lcof/
    List<List<Integer>> result;
    List<Integer> tmp;
    public List<List<Integer>> pathSum(TreeNode root, int target) {
        result = new ArrayList<>();
        tmp = new ArrayList<>();
        dfs(root,target);
        return result;
    }

    private void dfs(TreeNode root, int target) {
        if (root == null) {
            return;
        }
        tmp.add(root.val);
        target = target - root.val;
        // 遍历到叶子节点了，查看target是否为0
        if(root.left == null && root.right == null){
            if(target == 0){
                result.add(new ArrayList<>(tmp));
            }
        }

        dfs(root.left,target);
        dfs(root.right,target);

        tmp.remove(tmp.size() - 1);
    }
}
