public class Day3 {
    // 剑指 Offer 13. 机器人的运动范围
    // https://leetcode.cn/problems/ji-qi-ren-de-yun-dong-fan-wei-lcof/
    int m;
    int n;
    int k;
    boolean[][] visited;
    public int movingCount1(int m, int n, int k) {
        this.m = m;
        this.n = n;
        this.k = k;
        this.visited = new boolean[m][n];
        return dfs1(0,0);
    }

    private int dfs1(int i, int j) {
        // 访问位置越界或者当前位置已经访问过或者不满足该位置行列坐标的数位和大于k
        if(i < 0 || i>=m || j < 0 || j >= n || visited[i][j] || k < (sum1(i) + sum1(j))){
            return 0;
        }
        // 表示已经当前位置已经访问了
        visited[i][j] = true;

        return 1 + dfs1(i + 1,j) + dfs1(i,j + 1) + dfs1(i - 1,j) + dfs1(i,j - 1);
    }

    private int sum1(int i) {
        int res = 0;
        while(i != 0){
            res += i % 10;
            i = i / 10;
        }
        return res;
    }

    // 剑指 Offer 13. 机器人的运动范围 优化版
    // https://leetcode.cn/problems/ji-qi-ren-de-yun-dong-fan-wei-lcof/
    public int movingCount(int m, int n, int k) {
        this.m = m;
        this.n = n;
        this.k = k;
        this.visited = new boolean[m][n];
        return dfs(0,0);
    }

    private int dfs(int i, int j) {
        // 访问位置越界或者当前位置已经访问过或者不满足该位置行列坐标的数位和大于k
        if(i < 0 || i>=m || j < 0 || j >= n || visited[i][j] || k < (sum(i) + sum(j))){
            return 0;
        }
        // 表示已经当前位置已经访问了
        visited[i][j] = true;

        // 可以去看官方解释
        return 1 + dfs(i + 1,j) + dfs(i,j + 1);//dfs(i - 1,j) + dfs(i,j - 1)
    }

    private int sum(int i) {
        int res = 0;
        while(i != 0){
            res += i % 10;
            i = i / 10;
        }
        return res;
    }

    // 剑指 Offer 14- I. 剪绳子
    // https://leetcode.cn/problems/jian-sheng-zi-lcof/
    public int cuttingRope(int n) {
        // 观察可以发现  当绳子的某一段>=5的时候，我们要进行切割，值会变得更大
        if(n <= 2){
            return 1;
        }
        if(n == 3){
            return 2;
        }
        int res = n / 3;
        int mod = n % 3;
        if(mod == 0){
            return (int) Math.pow(3,res);
        }else if (mod == 1){
            return (int) Math.pow(3,res - 1) * 4;
        }
        else {
            return (int) Math.pow(3,res) * 2;
        }
    }

    // 剑指 Offer 14- II. 剪绳子 II
    // https://leetcode.cn/problems/jian-sheng-zi-ii-lcof/
    // (x1 + x2)%p = (x1%p + x2%p)%p
    // (x1 * x2)%p = (x1%p * x2%p)%p
    // 当x1*x2 < p的时候才会成立，反之大于不成立
    public int cuttingRope2(int n) {
        if(n <= 2){
            return 1;
        }
        if(n == 3){
            return 2;
        }
        int p =1000000007;
        int res = n / 3;
        int mod = n % 3;
        if(mod == 0){
            return (int) pow(3,res);
        }else if (mod == 1){
            return (int) ((pow(3,res - 1) * 4) % p);
        }
        else {
            return (int) ((pow(3,res) * 2) % p);
        }
    }

    // 求a ^ n % p
    long pow(int a,int n){
        int res = 1;
        int p = 1000000007;
        for (int i = 1; i <= n; i++) {
            res = (res * a) % p;
        }
        return res;
    }

    // 剑指 Offer 15. 二进制中1的个数
    // https://leetcode.cn/problems/er-jin-zhi-zhong-1de-ge-shu-lcof/
    public int hammingWeight(int n) {
        int count = 0;
        while(n != 0){
            n = n & (n - 1);
            count++;
        }
        return count;
    }

    // 剑指 Offer 16. 数值的整数次方
    // https://leetcode.cn/problems/shu-zhi-de-zheng-shu-ci-fang-lcof/
    // 把次幂转换为二进制，从而进行x * x^2 * x ^ 4....这样来计算 判断二进制中末位是否为1，为1就代表要乘进去
    public double myPow(double x, int n) {
        // 可以得到x的n次方
        boolean flag = true;// 代表n为正数数
        // 这里用long去存储一下n，因为如果是int类型的最小值，把它转换为正整数，int类型存不下
        long y = n;
        if(y < 0){
            y = -y;
            flag = false;// 代表n为正数
        }
        double res = 1;
        while (y > 0){
            if(y % 2 == 1)
                res = res * x;
            x = x * x;
            y = y / 2;
        }
        if(flag){
            return res;
        }else {
            return 1 / res;
        }
    }
}
