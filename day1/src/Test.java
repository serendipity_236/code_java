import java.util.Arrays;

public class Test {

    public static void main(String[] args) {
        int[] array = {1,4,9,15,20,34,55};

        int index = binarySearch0(array,0,array.length,60);
        if(index < 0){
            int[] b = new int[array.length + 1];
            int insert = Math.abs(index + 1);
            System.arraycopy(array,0,b,0,insert);
            b[insert] = 60;
            System.arraycopy(array,insert,b,insert + 1,array.length - insert);
            System.out.println(Arrays.toString(b));
        }
        System.out.println(Arrays.toString(getRange1(array, 0, array.length, 8)));
        System.out.println(Arrays.toString(getRange2(array, 0, array.length, 8)));
        System.out.println(Arrays.toString(getRange3(array, 0, array.length, 8)));
        System.out.println(Arrays.toString(getRange4(array, 0, array.length, 8)));
    }

    /**
     * 二分查找平衡版
     */
    public static int binarySearch(int[] array,int target){
        int i = 0;
        int j = array.length;
        while(1 < j - i){
            int m = (i + j) >>> 1;
            if(target < array[m]){
                j =m;
            }else{
                i = m;
            }
        }
        if(array[i] == target){
            return i;
        }else{
            return  - 1;
        }
    }
    // jdk的util包下binarySearch的实现
    private static int binarySearch0(int[] a, int fromIndex, int toIndex, int key) {
        int low = fromIndex;
        int high = toIndex - 1;

        while (low <= high) {
            int mid = (low + high) >>> 1;
            int midVal = a[mid];

            if (midVal < key)
                low = mid + 1;
            else if (midVal > key)
                high = mid - 1;
            else
                return mid; // key found
        }
        // -插入点-1  该结果加上1取正得到插入点  减1是为了区分0的情况  -0和0是一样的
        return -(low + 1);  // key not found.
    }

    // 查找最左边的数字 二分查找
    private static int findLeftByBinarySearch(int[] a, int fromIndex, int toIndex, int key) {
        int low = fromIndex;
        int high = toIndex - 1;
        int left = -1;
        while (low <= high) {
            int mid = (low + high) >>> 1;
            int midVal = a[mid];

            if (midVal < key)
                low = mid + 1;
            else if (midVal > key)
                high = mid - 1;
            else{
                left = mid;
                high = mid - 1;
            }

        }
        return left;
    }

    // 查找最右边边的数字 二分查找
    private static int findRightByBinarySearch(int[] a, int fromIndex, int toIndex, int key) {
        int low = fromIndex;
        int high = toIndex - 1;
        int right = -1;
        while (low <= high) {
            int mid = (low + high) >>> 1;
            int midVal = a[mid];

            if (midVal < key)
                low = mid + 1;
            else if (midVal > key)
                high = mid - 1;
            else{
                right = mid;
                low = mid + 1;
            }

        }
        return right;
    }

    // 查找最左边的数字2 二分查找
    private static int findLeftByBinarySearch2(int[] a, int fromIndex, int toIndex, int key) {
        int low = fromIndex;
        int high = toIndex - 1;
        while (low <= high) {
            int mid = (low + high) >>> 1;
            int midVal = a[mid];

            if (midVal < key)
                low = mid + 1;
            else
                high = mid - 1;
        }
        // 找到就是目标最靠左的索引位置，没找到就是找到一个比目标大的最靠左的索引位置
        // 即大于等于目标的最靠左边的位置
        return low;
    }

    // 查找最右边的数字2 二分查找
    private static int findRightByBinarySearch2(int[] a, int fromIndex, int toIndex, int key) {
        int low = fromIndex;
        int high = toIndex - 1;
        while (low <= high) {
            int mid = (low + high) >>> 1;
            int midVal = a[mid];

            if (midVal <= key)
                low = mid + 1;
            else
                high = mid - 1;

        }
        // 找到就是目标最靠右的索引位置，没找到就是找到一个比目标小的最靠右的索引位置
        // 即小于等于目标的最靠右边的位置
        return low - 1;
    }

    // 应用
    // 求排名
    private static int getRank(int[] a, int fromIndex, int toIndex, int key) {
        int result = findLeftByBinarySearch2(a, fromIndex, toIndex, key) + 1;
        return result;
    }

    // 求前任
    private static int getPredecessor(int[] a, int fromIndex, int toIndex, int key) {
        int result = findLeftByBinarySearch2(a, fromIndex, toIndex, key) - 1;
        return result;
    }

    // 求后任
    private static int getPostPredecessor(int[] a, int fromIndex, int toIndex, int key) {
        int result = findRightByBinarySearch2(a, fromIndex, toIndex, key) + 1;
        return result;
    }

    // 范围查询
    // 求小于某个数的所有元素
    private static int[] getRange1(int[] a, int fromIndex, int toIndex, int key) {
        int result = findLeftByBinarySearch2(a, fromIndex, toIndex, key);
        int[] array = new int[result];
        System.arraycopy(a, 0, array, 0, array.length);
        return array;
    }

    // 范围查询
    // 求小于等于某个数的所有元素
    private static int[] getRange2(int[] a, int fromIndex, int toIndex, int key) {
        int result = findRightByBinarySearch2(a, fromIndex, toIndex, key) + 1;
        int[] array = new int[result];
        System.arraycopy(a, 0, array, 0, array.length);
        return array;
    }

    // 范围查询
    // 求大于某个数的所有元素
    private static int[] getRange3(int[] a, int fromIndex, int toIndex, int key) {
        int result = findRightByBinarySearch2(a, fromIndex, toIndex, key) + 1;
        int[] array = new int[a.length - result];
        System.arraycopy(a, result, array, 0, array.length);
        return array;
    }

    // 范围查询
    // 求大于等于某个数的所有元素
    private static int[] getRange4(int[] a, int fromIndex, int toIndex, int key) {
        int result = findLeftByBinarySearch2(a, fromIndex, toIndex, key);
        int[] array = new int[a.length - result];
        System.arraycopy(a, result, array, 0, array.length);
        return array;
    }
}



