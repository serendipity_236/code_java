package top.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import top.pojo.Book;

@Mapper
public interface BookMapper extends BaseMapper<Book> {
}
