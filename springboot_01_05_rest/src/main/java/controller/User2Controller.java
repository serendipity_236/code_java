package controller;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users2")
public class User2Controller {
    //@RequestMapping(method = RequestMethod.POST)
    @PostMapping
    //RequestBody用于请求体的参数获取
    public String save(@RequestBody User user) {
        System.out.println("user2 save..." + user);
        return "{'module':'user2 save'}";
    }

    //@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    @DeleteMapping("/{id}")
    //PathVariable作用于路径参数的获取
    public String delete(@PathVariable Integer id) {
        System.out.println("user2 delete..." + id);
        return "{'module':'user2 delete'}";
    }

    //@RequestMapping(method = RequestMethod.PUT)
    @PutMapping
    //RequestBody用于请求体的参数获取
    public String update(@RequestBody User user) {
        System.out.println("user2 update..." + user);
        return "{'module':'user2 update'}";
    }

    //@RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @GetMapping("/{id}")
    //PathVariable作用于路径参数的获取
    public String getById(@PathVariable Integer id) {
        System.out.println("user2 getById..." + id);
        return "{'module':'user2 getById'}";
    }

    //@RequestMapping(method = RequestMethod.GET)
    @GetMapping
    //PathVariable作用于路径参数的获取
    public String getAll() {
        System.out.println("user2 getAll...");
        return "{'module':'user2 getAll'}";
    }
}


// 所谓的restful风格就是一个业务对应一个controller，通过参数的有无和不同方法的请求区分不同的子业务
// get对应查询 post对应新增 put对应修改 delete对应删除
//使用url传参可以直接使用和url路径中同名的参数名接收参数，或者封装对象接收(要提供set方法)
