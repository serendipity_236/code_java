package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;


@Controller
public class User1Controller {
    @RequestMapping(value = "/users1", method = RequestMethod.POST)
    @ResponseBody
    //RequestBody用于请求体的参数获取
    public String save(@RequestBody User user) {
        System.out.println("user1 save...");
        return "{'module':'user1 save'}";
    }

    @RequestMapping(value = "/users1/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    //PathVariable作用于路径参数的获取
    public String delete(@PathVariable Integer id) {
        System.out.println("user1 delete..." + id);
        return "{'module':'user1 delete'}";
    }

    @RequestMapping(value = "/users1", method = RequestMethod.PUT)
    @ResponseBody
    //RequestBody用于请求体的参数获取
    public String update(@RequestBody User user) {
        System.out.println("user1 update..." + user);
        return "{'module':'user1 update'}";
    }

    @RequestMapping(value = "/users1/{id}", method = RequestMethod.GET)
    @ResponseBody
    //PathVariable作用于路径参数的获取
    //路径参数的名字要写在PathVariable仲
    public String getById(@PathVariable Integer id) {
        System.out.println("user1 getById..." + id);
        return "{'module':'user1 getById'}";
    }

    @RequestMapping(value = "/users1", method = RequestMethod.GET)
    @ResponseBody
    public String getAll() {
        System.out.println("user1 getAll...");
        return "{'module':'user1 getAll'}";
    }
}

// 所谓的restful风格就是一个业务对应一个controller，通过参数的有无和不同方法的请求区分不同的子业务
// get对应查询 post对应新增 put对应修改 delete对应删除
//使用url传参可以直接使用和url路径中同名的参数名接收参数，或者封装对象接收(要提供set方法)
