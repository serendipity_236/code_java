
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBoot0105RestApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBoot0105RestApplication.class, args);
    }

}
